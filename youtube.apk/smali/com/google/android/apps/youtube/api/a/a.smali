.class public final Lcom/google/android/apps/youtube/api/a/a;
.super Lcom/google/android/apps/youtube/api/jar/a/a;
.source "SourceFile"


# instance fields
.field private final j:Lcom/google/android/apps/youtube/api/ApiPlayer;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/api/j;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 14

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a;

    invoke-direct {v1, p1}, Lcom/google/android/apps/youtube/api/jar/a;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p3

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/youtube/api/jar/a/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;)V

    const-string v1, "apiEnvironment cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v1, p4

    check-cast v1, Landroid/view/View;

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setVideoView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/youtube/api/ApiPlayer;

    new-instance v3, Lcom/google/android/apps/youtube/api/a/b;

    const/4 v2, 0x0

    invoke-direct {v3, p0, v2}, Lcom/google/android/apps/youtube/api/a/b;-><init>(Lcom/google/android/apps/youtube/api/a/a;B)V

    iget-object v7, p0, Lcom/google/android/apps/youtube/api/a/a;->d:Lcom/google/android/apps/youtube/core/player/overlay/a;

    iget-object v8, p0, Lcom/google/android/apps/youtube/api/a/a;->e:Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;

    iget-object v9, p0, Lcom/google/android/apps/youtube/api/a/a;->f:Lcom/google/android/apps/youtube/core/player/overlay/g;

    iget-object v10, p0, Lcom/google/android/apps/youtube/api/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    iget-object v11, p0, Lcom/google/android/apps/youtube/api/a/a;->g:Lcom/google/android/apps/youtube/core/player/overlay/ak;

    iget-object v12, p0, Lcom/google/android/apps/youtube/api/a/a;->h:Lcom/google/android/apps/youtube/core/player/overlay/be;

    iget-object v13, p0, Lcom/google/android/apps/youtube/api/a/a;->i:Lcom/google/android/apps/youtube/core/player/overlay/br;

    move-object v2, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/youtube/api/ApiPlayer;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/p;Lcom/google/android/apps/youtube/api/j;Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/medialib/player/y;Lcom/google/android/apps/youtube/core/player/overlay/a;Lcom/google/android/apps/youtube/core/player/overlay/bm;Lcom/google/android/apps/youtube/core/player/overlay/g;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;Lcom/google/android/apps/youtube/core/player/overlay/ak;Lcom/google/android/apps/youtube/core/player/overlay/be;Lcom/google/android/apps/youtube/core/player/overlay/br;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/api/j;Z)V
    .locals 2

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-direct {v1, p1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/apps/youtube/api/a/a;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/api/j;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/apps/youtube/medialib/player/y;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->T()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a/a;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/a/a;->g(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a/a;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/a/a;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a/a;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/a/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a/a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/api/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a/a;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/a/a;->k(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->U()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/a/a;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/a/a;->j(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->V()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/a/a;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/a/a;->l(Z)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->W()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->X()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->O()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->Y()V

    return-void
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->Z()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->R()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->aa()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->S()V

    return-void
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->ab()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->S()V

    return-void
.end method

.method static synthetic n(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->ac()V

    return-void
.end method

.method static synthetic o(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->N()V

    return-void
.end method

.method static synthetic p(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->P()V

    return-void
.end method

.method static synthetic q(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/a/a;->Q()V

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->k()V

    return-void
.end method

.method public final B()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->d()Z

    move-result v0

    return v0
.end method

.method public final C()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->e()Z

    move-result v0

    return v0
.end method

.method public final D()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->f()Z

    move-result v0

    return v0
.end method

.method public final E()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->g()V

    return-void
.end method

.method public final F()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->h()V

    return-void
.end method

.method public final G()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->i()I

    move-result v0

    return v0
.end method

.method public final H()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->j()I

    move-result v0

    return v0
.end method

.method public final I()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->o()V

    return-void
.end method

.method public final J()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->p()V

    return-void
.end method

.method public final K()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->m()V

    return-void
.end method

.method public final L()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->l()Z

    move-result v0

    return v0
.end method

.method public final M()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a()V

    return-void
.end method

.method public final a()Lcom/google/android/apps/youtube/api/ApiPlayer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    return-object v0
.end method

.method protected final a([B)Z
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    array-length v0, p1

    invoke-virtual {v1, p1, v2, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    const-class v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    const/4 v0, 0x1

    return v0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public final c(Ljava/lang/String;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Ljava/lang/String;II)V

    return-void
.end method

.method public final c(Ljava/util/List;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Ljava/util/List;II)V

    return-void
.end method

.method public final c(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(Ljava/lang/String;I)V

    return-void
.end method

.method public final d(Ljava/lang/String;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(Ljava/lang/String;II)V

    return-void
.end method

.method public final d(Ljava/util/List;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(Ljava/util/List;II)V

    return-void
.end method

.method public final d(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final e(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(I)V

    return-void
.end method

.method public final f(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(I)V

    return-void
.end method

.method public final f(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(Z)V

    return-void
.end method

.method protected final g(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Z)V

    return-void
.end method

.method public final h(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/ApiPlayer;->c(Z)V

    return-void
.end method

.method public final i(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->n()V

    return-void
.end method

.method protected final x()[B
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->q()Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    move-result-object v0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final y()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b()V

    return-void
.end method

.method public final z()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/a;->j:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->c()V

    return-void
.end method
