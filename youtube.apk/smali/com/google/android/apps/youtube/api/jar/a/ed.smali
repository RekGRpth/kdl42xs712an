.class public final Lcom/google/android/apps/youtube/api/jar/a/ed;
.super Lcom/google/android/apps/youtube/api/jar/a/da;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/be;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/be;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/da;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/be;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ed;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ed;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ed;)Lcom/google/android/apps/youtube/core/player/overlay/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ed;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ed;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ef;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/ef;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ed;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ed;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/eh;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/eh;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ed;F)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ed;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ei;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ei;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ed;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ed;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ee;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ee;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ed;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ed;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/eg;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/eg;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ed;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
