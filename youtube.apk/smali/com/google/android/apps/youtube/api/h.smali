.class final Lcom/google/android/apps/youtube/api/h;
.super Lcom/google/android/apps/youtube/core/converter/http/ay;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/converter/c;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/h;->a:Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/http/ay;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/h;-><init>(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 3

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v1, "DeviceId"

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/h;->a:Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;

    invoke-static {v2}, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->e(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;)[B

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/b;-><init>(Ljava/lang/String;[B)V

    return-object v1
.end method

.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    check-cast p1, Landroid/util/Pair;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    sget-object v2, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->POST:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    const-string v3, "X-GData-Device"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/b;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method protected final a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;
    .locals 3

    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v2, "Error"

    invoke-virtual {v0, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x190

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    const-string v2, "InvalidDeveloper"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis$InvalidDeveloperException;

    const-string v2, "Invalid Developer Key"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis$InvalidDeveloperException;-><init>(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/ay;->a(Lorg/apache/http/HttpResponse;)Lorg/apache/http/client/HttpResponseException;

    move-result-object v0

    goto :goto_0
.end method
