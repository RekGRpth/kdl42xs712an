.class final Lcom/google/android/apps/youtube/api/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/api/p;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/api/a/a;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/api/a/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/api/a/a;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/a/b;-><init>(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->a(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->k(Lcom/google/android/apps/youtube/api/a/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->l(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->i(Lcom/google/android/apps/youtube/api/a/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->j(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/api/a/a;->a(Lcom/google/android/apps/youtube/api/a/a;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IIZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/youtube/api/a/a;->a(Lcom/google/android/apps/youtube/api/a/a;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/api/a/a;->a(Lcom/google/android/apps/youtube/api/a/a;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/api/a/a;->b(Lcom/google/android/apps/youtube/api/a/a;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/api/a/a;->c(Lcom/google/android/apps/youtube/api/a/a;Z)V

    return-void
.end method

.method public final a(ZI)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/api/a/a;->a(Lcom/google/android/apps/youtube/api/a/a;Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->b(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/api/a/a;->a(Lcom/google/android/apps/youtube/api/a/a;I)V

    return-void
.end method

.method public final b(II)V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->c(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->d(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->e(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->f(Lcom/google/android/apps/youtube/api/a/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->g(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->h(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->m(Lcom/google/android/apps/youtube/api/a/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->n(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->o(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->p(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a/b;->a:Lcom/google/android/apps/youtube/api/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/a/a;->q(Lcom/google/android/apps/youtube/api/a/a;)V

    return-void
.end method
