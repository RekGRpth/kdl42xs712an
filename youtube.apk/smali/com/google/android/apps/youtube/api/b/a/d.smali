.class public final Lcom/google/android/apps/youtube/api/b/a/d;
.super Lcom/google/android/apps/youtube/api/b/a/ap;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Lcom/google/android/youtube/api/service/a;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/apps/youtube/api/ApiPlayer;

.field private final c:Lcom/google/android/youtube/api/service/c;

.field private final d:Lcom/google/android/apps/youtube/api/b/a/ae;

.field private final e:Lcom/google/android/apps/youtube/api/b/a/cv;

.field private final f:Lcom/google/android/apps/youtube/api/b/a/de;

.field private final g:Lcom/google/android/apps/youtube/api/b/a/cr;

.field private final h:Lcom/google/android/apps/youtube/api/b/a/bj;

.field private final i:Lcom/google/android/apps/youtube/api/b/a/dl;

.field private final j:Lcom/google/android/apps/youtube/api/b/a/bp;

.field private final k:Lcom/google/android/apps/youtube/api/b/a/bw;

.field private final l:Lcom/google/android/apps/youtube/api/b/a/co;

.field private final m:Lcom/google/android/apps/youtube/api/b/a/cu;

.field private final n:Lcom/google/android/apps/youtube/api/b/a/dq;

.field private final o:Lcom/google/android/apps/youtube/api/b/a/a;

.field private p:Lcom/google/android/apps/youtube/api/jar/a/ck;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/api/service/c;Lcom/google/android/apps/youtube/api/j;Lcom/google/android/apps/youtube/api/jar/a/ck;Lcom/google/android/apps/youtube/api/jar/a/cw;Lcom/google/android/apps/youtube/api/jar/a/dc;Lcom/google/android/apps/youtube/api/jar/a/df;Lcom/google/android/apps/youtube/api/jar/a/ct;Lcom/google/android/apps/youtube/api/jar/a/ce;Lcom/google/android/apps/youtube/api/jar/a/di;Lcom/google/android/apps/youtube/api/jar/a/ch;Lcom/google/android/apps/youtube/api/jar/a/cn;Lcom/google/android/apps/youtube/api/jar/a/cq;Lcom/google/android/apps/youtube/api/jar/a/cz;Lcom/google/android/apps/youtube/api/jar/a/dl;Z)V
    .locals 16

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/api/b/a/ap;-><init>()V

    const-string v3, "context cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "uiHandler cannot be null"

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    const-string v3, "serviceDestroyedNotifier"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/api/service/c;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->c:Lcom/google/android/youtube/api/service/c;

    const-string v3, "apiEnvironment cannot be null"

    move-object/from16 v0, p4

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "apiPlayerClient cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/api/jar/a/ck;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->p:Lcom/google/android/apps/youtube/api/jar/a/ck;

    const-string v3, "playerUiClient cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p17, :cond_0

    const-string v3, "surfaceHolderClient cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const-string v3, "playerSurfaceClient cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "adOverlayClient cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "annotationOverlayClient cannot be null"

    move-object/from16 v0, p12

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "controlsOverlayClient cannot be null"

    move-object/from16 v0, p13

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "liveOverlayClient cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "subtitlesOverlayClient cannot be null"

    move-object/from16 v0, p15

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "thumbnailOverlayClient cannot be null"

    move-object/from16 v0, p16

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/cr;

    move-object/from16 v0, p2

    move-object/from16 v1, p6

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/api/b/a/cr;-><init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/cw;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->g:Lcom/google/android/apps/youtube/api/b/a/cr;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/bj;

    move-object/from16 v0, p2

    move-object/from16 v1, p10

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/api/b/a/bj;-><init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/ce;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->h:Lcom/google/android/apps/youtube/api/b/a/bj;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/dl;

    move-object/from16 v0, p2

    move-object/from16 v1, p11

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/api/b/a/dl;-><init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/di;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->i:Lcom/google/android/apps/youtube/api/b/a/dl;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/bp;

    move-object/from16 v0, p2

    move-object/from16 v1, p12

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/api/b/a/bp;-><init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/ch;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->j:Lcom/google/android/apps/youtube/api/b/a/bp;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/bw;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p13

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/apps/youtube/api/b/a/bw;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/cn;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->k:Lcom/google/android/apps/youtube/api/b/a/bw;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/co;

    move-object/from16 v0, p2

    move-object/from16 v1, p14

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/api/b/a/co;-><init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/cq;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->l:Lcom/google/android/apps/youtube/api/b/a/co;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/cu;

    move-object/from16 v0, p15

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/api/b/a/cu;-><init>(Lcom/google/android/apps/youtube/api/jar/a/cz;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->m:Lcom/google/android/apps/youtube/api/b/a/cu;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/dq;

    move-object/from16 v0, p16

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/api/b/a/dq;-><init>(Lcom/google/android/apps/youtube/api/jar/a/dl;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->n:Lcom/google/android/apps/youtube/api/b/a/dq;

    if-nez p17, :cond_1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->f:Lcom/google/android/apps/youtube/api/b/a/de;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/cv;

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->d(Landroid/content/Context;)Z

    move-result v4

    move-object/from16 v0, p2

    move-object/from16 v1, p7

    invoke-direct {v3, v0, v1, v4}, Lcom/google/android/apps/youtube/api/b/a/cv;-><init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/dc;Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->e:Lcom/google/android/apps/youtube/api/b/a/cv;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/cn;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/b/a/d;->e:Lcom/google/android/apps/youtube/api/b/a/cv;

    move-object/from16 v0, p9

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/youtube/api/b/a/cn;-><init>(Landroid/view/SurfaceHolder;Lcom/google/android/apps/youtube/api/jar/a/ct;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->o:Lcom/google/android/apps/youtube/api/b/a/a;

    :goto_1
    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/ae;

    move-object/from16 v0, p5

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/api/b/a/ae;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ck;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->d:Lcom/google/android/apps/youtube/api/b/a/ae;

    new-instance v3, Lcom/google/android/apps/youtube/api/ApiPlayer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/b/a/d;->d:Lcom/google/android/apps/youtube/api/b/a/ae;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/api/b/a/d;->g:Lcom/google/android/apps/youtube/api/b/a/cr;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/api/b/a/d;->o:Lcom/google/android/apps/youtube/api/b/a/a;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/b/a/d;->h:Lcom/google/android/apps/youtube/api/b/a/bj;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/api/b/a/d;->i:Lcom/google/android/apps/youtube/api/b/a/dl;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/api/b/a/d;->j:Lcom/google/android/apps/youtube/api/b/a/bp;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/api/b/a/d;->k:Lcom/google/android/apps/youtube/api/b/a/bw;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/api/b/a/d;->l:Lcom/google/android/apps/youtube/api/b/a/co;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/youtube/api/b/a/d;->m:Lcom/google/android/apps/youtube/api/b/a/cu;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/api/b/a/d;->n:Lcom/google/android/apps/youtube/api/b/a/dq;

    move-object/from16 v4, p1

    move-object/from16 v6, p4

    invoke-direct/range {v3 .. v15}, Lcom/google/android/apps/youtube/api/ApiPlayer;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/p;Lcom/google/android/apps/youtube/api/j;Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/medialib/player/y;Lcom/google/android/apps/youtube/core/player/overlay/a;Lcom/google/android/apps/youtube/core/player/overlay/bm;Lcom/google/android/apps/youtube/core/player/overlay/g;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;Lcom/google/android/apps/youtube/core/player/overlay/ak;Lcom/google/android/apps/youtube/core/player/overlay/be;Lcom/google/android/apps/youtube/core/player/overlay/br;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->b:Lcom/google/android/apps/youtube/api/ApiPlayer;

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/api/service/c;->a(Lcom/google/android/youtube/api/service/a;)V

    :try_start_0
    invoke-interface/range {p5 .. p5}, Lcom/google/android/apps/youtube/api/jar/a/ck;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-interface {v3, v0, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_0
    const-string v3, "surfaceTextureClient cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->e:Lcom/google/android/apps/youtube/api/b/a/cv;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/de;

    move-object/from16 v0, p2

    move-object/from16 v1, p8

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/api/b/a/de;-><init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/df;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->f:Lcom/google/android/apps/youtube/api/b/a/de;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/dp;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/b/a/d;->f:Lcom/google/android/apps/youtube/api/b/a/de;

    move-object/from16 v0, p9

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/youtube/api/b/a/dp;-><init>(Lcom/google/android/apps/youtube/api/b/a/de;Lcom/google/android/apps/youtube/api/jar/a/ct;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/b/a/d;->o:Lcom/google/android/apps/youtube/api/b/a/a;

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/api/b/a/d;->binderDied()V

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/d;)Lcom/google/android/apps/youtube/api/ApiPlayer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->b:Lcom/google/android/apps/youtube/api/ApiPlayer;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/d;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/b/a/d;->e(Z)V

    return-void
.end method

.method private e(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->b:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->n()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->c:Lcom/google/android/youtube/api/service/c;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/api/service/c;->b(Lcom/google/android/youtube/api/service/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->p:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->p:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->p:Lcom/google/android/apps/youtube/api/jar/a/ck;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->d:Lcom/google/android/apps/youtube/api/b/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/ae;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->e:Lcom/google/android/apps/youtube/api/b/a/cv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->e:Lcom/google/android/apps/youtube/api/b/a/cv;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/cv;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->f:Lcom/google/android/apps/youtube/api/b/a/de;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->f:Lcom/google/android/apps/youtube/api/b/a/de;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/de;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->g:Lcom/google/android/apps/youtube/api/b/a/cr;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/cr;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->h:Lcom/google/android/apps/youtube/api/b/a/bj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/bj;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->k:Lcom/google/android/apps/youtube/api/b/a/bw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/bw;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->l:Lcom/google/android/apps/youtube/api/b/a/co;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/co;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->m:Lcom/google/android/apps/youtube/api/b/a/cu;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/cu;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->n:Lcom/google/android/apps/youtube/api/b/a/dq;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/dq;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->o:Lcom/google/android/apps/youtube/api/b/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/b/a/a;->a()V

    invoke-static {}, Ljava/lang/System;->gc()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/b/a/d;->e(Z)V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/j;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/j;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/s;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/s;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/e;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/e;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/x;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/x;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/util/List;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/z;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/z;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Ljava/util/List;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/g;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a([B)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    array-length v0, p1

    invoke-virtual {v3, p1, v1, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    invoke-virtual {v3, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eq v0, v2, :cond_0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    :try_start_0
    const-class v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/apps/youtube/api/b/a/v;

    invoke-direct {v4, p0, v0, v1}, Lcom/google/android/apps/youtube/api/b/a/v;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;Landroid/os/ConditionVariable;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ab;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/ab;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/k;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/t;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/t;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/p;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/p;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/y;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/y;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Ljava/util/List;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/aa;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/aa;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Ljava/util/List;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/l;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/l;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final binderDied()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/b/a/d;->a(Z)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ac;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/ac;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/m;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/m;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ad;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/ad;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/o;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/o;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e()Z
    .locals 4

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/f;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/apps/youtube/api/b/a/f;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/h;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/i;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/i;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/n;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/q;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/q;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/r;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/r;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k()[B
    .locals 4

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/apps/youtube/api/b/a/u;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/youtube/api/b/a/u;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;Ljava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0
.end method

.method public final l()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/d;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/w;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/w;-><init>(Lcom/google/android/apps/youtube/api/b/a/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
