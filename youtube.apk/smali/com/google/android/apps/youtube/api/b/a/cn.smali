.class public final Lcom/google/android/apps/youtube/api/b/a/cn;
.super Lcom/google/android/apps/youtube/api/b/a/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private final b:Landroid/view/SurfaceHolder;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/view/SurfaceHolder;Lcom/google/android/apps/youtube/api/jar/a/ct;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/api/b/a/a;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ct;)V

    const-string v0, "surfaceHolder cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceHolder;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->b:Landroid/view/SurfaceHolder;

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-interface {p1, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->b:Landroid/view/SurfaceHolder;

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->b:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public final b(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final g()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->b:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->c:Z

    return v0
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->b()V

    :cond_0
    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->c:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->a()V

    :cond_0
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->c:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cn;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->c()V

    :cond_0
    return-void
.end method
