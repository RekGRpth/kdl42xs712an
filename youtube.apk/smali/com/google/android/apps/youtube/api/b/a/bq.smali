.class final Lcom/google/android/apps/youtube/api/b/a/bq;
.super Lcom/google/android/apps/youtube/api/b/a/aj;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/apps/youtube/core/player/overlay/h;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/b/a/aj;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bq;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/bq;)Lcom/google/android/apps/youtube/core/player/overlay/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bq;->b:Lcom/google/android/apps/youtube/core/player/overlay/h;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bq;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/br;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/br;-><init>(Lcom/google/android/apps/youtube/api/b/a/bq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/overlay/h;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bq;->b:Lcom/google/android/apps/youtube/core/player/overlay/h;

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bq;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bt;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/bt;-><init>(Lcom/google/android/apps/youtube/api/b/a/bq;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bq;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bs;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/bs;-><init>(Lcom/google/android/apps/youtube/api/b/a/bq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bq;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bu;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/bu;-><init>(Lcom/google/android/apps/youtube/api/b/a/bq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bq;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bv;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/bv;-><init>(Lcom/google/android/apps/youtube/api/b/a/bq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
