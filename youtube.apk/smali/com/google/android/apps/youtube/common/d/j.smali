.class public final Lcom/google/android/apps/youtube/common/d/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Lcom/google/android/apps/youtube/common/database/d;

.field private final c:Ljava/util/concurrent/ScheduledExecutorService;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;

.field private final e:Lcom/google/android/apps/youtube/common/d/p;

.field private final f:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/database/d;Ljava/util/concurrent/ScheduledExecutorService;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/d/p;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/database/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    iput-object p2, p0, Lcom/google/android/apps/youtube/common/d/j;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/d/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->e:Lcom/google/android/apps/youtube/common/d/p;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->f:Ljava/util/concurrent/Executor;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->a:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/common/d/j;)Lcom/google/android/apps/youtube/common/database/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/a/a/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/common/d/j;->e(Lcom/google/android/apps/youtube/a/a/g;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->a()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Removing task %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/common/database/d;->a(Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->b()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/common/d/j;)V
    .locals 10

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->d()Lcom/google/android/apps/youtube/common/database/e;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Lcom/google/android/apps/youtube/common/database/e;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Lcom/google/android/apps/youtube/common/database/e;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/g;

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/d/j;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/g;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/common/d/i;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "Missing task factory for task type: "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/g;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/common/d/i;->a(Lcom/google/android/apps/youtube/a/a/g;)Lcom/google/android/apps/youtube/common/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/d/h;->d()J

    move-result-wide v6

    cmp-long v1, v3, v6

    if-ltz v1, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Executed scheduled task of type %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/d/h;->c()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/d/j;->f:Ljava/util/concurrent/Executor;

    new-instance v6, Lcom/google/android/apps/youtube/common/d/o;

    invoke-direct {v6, p0, v0}, Lcom/google/android/apps/youtube/common/d/o;-><init>(Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/common/d/h;)V

    invoke-interface {v1, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/d/h;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/d/h;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v5}, Lcom/google/android/apps/youtube/common/database/e;->a()V

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/common/d/j;->a(Ljava/util/List;)V

    return-void
.end method

.method private e(Lcom/google/android/apps/youtube/a/a/g;)V
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->c()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/d/j;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    new-instance v1, Lcom/google/android/apps/youtube/common/d/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/common/d/n;-><init>(Lcom/google/android/apps/youtube/common/d/j;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->g()J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Scheduling task %s with ScheduledExecutorService for repeating execution."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->g()J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Scheduling task %s with ScheduledExecutorService for one time execution."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->c:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/android/apps/youtube/common/d/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/common/d/k;-><init>(Lcom/google/android/apps/youtube/common/d/j;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/d/j;->e:Lcom/google/android/apps/youtube/common/d/p;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/d/p;->x()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/a/a/g;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/common/d/l;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/common/d/l;-><init>(Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/a/a/g;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/common/d/i;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->a:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/apps/youtube/common/d/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/a/a/g;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/common/d/m;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/common/d/m;-><init>(Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/a/a/g;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final declared-synchronized c(Lcom/google/android/apps/youtube/a/a/g;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/common/database/d;->b(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/common/d/j;->d(Lcom/google/android/apps/youtube/a/a/g;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Lcom/google/android/apps/youtube/a/a/g;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/j;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/youtube/common/database/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/common/d/j;->e(Lcom/google/android/apps/youtube/a/a/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
