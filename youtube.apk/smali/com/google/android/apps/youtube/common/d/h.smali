.class public abstract Lcom/google/android/apps/youtube/common/d/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/a/a/g;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/youtube/a/a/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/g;->d()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/common/d/h;->a:Lcom/google/android/apps/youtube/a/a/g;

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/h;->a:Lcom/google/android/apps/youtube/a/a/g;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/g;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final d()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/h;->a:Lcom/google/android/apps/youtube/a/a/g;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/g;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method final e()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/h;->a:Lcom/google/android/apps/youtube/a/a/g;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/g;->g()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
