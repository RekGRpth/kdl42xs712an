.class public final Lcom/google/android/apps/youtube/common/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([BI)[B
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    if-lez p1, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    array-length v0, p0

    rem-int/2addr v0, p1

    if-nez v0, :cond_2

    invoke-virtual {p0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    array-length v1, p0

    sub-int v0, p1, v0

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/a;->b([BI)[B

    move-result-object v0

    goto :goto_2
.end method

.method public static a([BIB)[B
    .locals 3

    const/16 v0, 0xbc

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/a;->a([BI)[B

    move-result-object v1

    array-length v0, p0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    const/4 v2, -0x1

    aput-byte v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static a([BII)[B
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    if-ltz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    if-ltz p2, :cond_2

    :goto_2
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    new-array v0, p2, [B

    invoke-static {p0, p1, v0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public static varargs a([[B)[B
    .locals 5

    const/4 v0, 0x0

    array-length v3, p0

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p0, v1

    array-length v4, v4

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    array-length v2, p0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    return-object v0
.end method

.method public static b([BI)[B
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    if-ltz p1, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    new-array v0, p1, [B

    array-length v1, p0

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
