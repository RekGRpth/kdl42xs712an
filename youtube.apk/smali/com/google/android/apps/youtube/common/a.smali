.class public abstract Lcom/google/android/apps/youtube/common/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/common/a;->a:J

    return-wide v0
.end method

.method protected a(J)V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/common/a;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This instance is already timestamped"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    cmp-long v0, p1, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iput-wide p1, p0, Lcom/google/android/apps/youtube/common/a;->a:J

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
