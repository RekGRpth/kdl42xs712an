.class final Lcom/google/android/apps/youtube/core/player/overlay/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/overlay/c;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/c;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/f;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/c;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t retrieve video info from [uri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v3

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    sget-object v4, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PUBLIC:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-ne v0, v4, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v3, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v3, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerUri:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/core/player/overlay/c;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->b(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/core/player/overlay/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/c;->b(Lcom/google/android/apps/youtube/core/player/overlay/c;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/a;->setAdTitle(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdOwnerUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/e;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/youtube/core/player/overlay/e;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/c;B)V

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/core/player/overlay/c;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->e(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdOwnerUri()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/overlay/c;->c(Lcom/google/android/apps/youtube/core/player/overlay/c;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/f;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/overlay/c;->d(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
