.class public final Lcom/google/android/apps/youtube/core/client/aj;
.super Lcom/google/android/apps/youtube/core/client/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bq;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final h:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const-wide/32 v7, 0x6ddd00

    invoke-direct {p0, p1, p4, p2, p3}, Lcom/google/android/apps/youtube/core/client/m;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;)V

    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/aj;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/client/aj;->e()Lcom/google/android/apps/youtube/common/cache/c;

    move-result-object v1

    const/16 v2, 0x14

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/client/aj;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/core/client/ak;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/apps/youtube/core/client/ak;-><init>(B)V

    new-instance v4, Lcom/google/android/apps/youtube/common/cache/l;

    invoke-direct {v4, v2, v0, v3}, Lcom/google/android/apps/youtube/common/cache/l;-><init>(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/cache/m;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/d;

    invoke-direct {v2, p5, p6}, Lcom/google/android/apps/youtube/core/converter/http/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/apps/youtube/core/converter/http/e;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/core/converter/http/e;-><init>()V

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/youtube/core/client/aj;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v2

    const-wide/32 v5, 0x5265c00

    invoke-virtual {p0, v1, v2, v5, v6}, Lcom/google/android/apps/youtube/core/client/aj;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/client/aj;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/async/i;

    invoke-direct {v2, v1}, Lcom/google/android/apps/youtube/core/async/i;-><init>(Lcom/google/android/apps/youtube/core/async/af;)V

    invoke-virtual {p0, v4, v2, v7, v8}, Lcom/google/android/apps/youtube/core/client/aj;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/aj;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/bt;

    invoke-direct {v1, p5, p6}, Lcom/google/android/apps/youtube/core/converter/http/bt;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/bu;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/bu;-><init>()V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/youtube/core/client/aj;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/client/aj;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/async/i;

    invoke-direct {v2, v1}, Lcom/google/android/apps/youtube/core/async/i;-><init>(Lcom/google/android/apps/youtube/core/async/af;)V

    invoke-virtual {p0, v0, v2, v7, v8}, Lcom/google/android/apps/youtube/core/client/aj;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/aj;->h:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/aj;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
