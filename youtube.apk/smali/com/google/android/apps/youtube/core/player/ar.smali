.class public final Lcom/google/android/apps/youtube/core/player/ar;
.super Lcom/google/android/apps/youtube/core/player/w;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/apps/youtube/core/identity/l;

.field private final e:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final f:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Landroid/content/SharedPreferences;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/w;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->e:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->f:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ar;->b()V

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ar;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/ar;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ar;->b()V

    return-void
.end method

.method private b()V
    .locals 1

    const-string v0, "playability_adult_confirmations"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/ar;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->a:Z

    const-string v0, "playability_content_confirmations"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/ar;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->b:Z

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->f:Landroid/content/SharedPreferences;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/ar;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/ar;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "playability_adult_confirmations"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/ar;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "playability_content_confirmations"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/ar;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->c:Lcom/google/android/apps/youtube/core/player/z;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/ar;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ar;->e:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ar;->c:Lcom/google/android/apps/youtube/core/player/z;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/z;->a()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/player/as;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/as;-><init>(Lcom/google/android/apps/youtube/core/player/ar;Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    goto :goto_0
.end method

.method public final onSignIn(Lcom/google/android/apps/youtube/core/identity/ai;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ar;->b()V

    return-void
.end method

.method public final onSignOut(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ar;->b()V

    return-void
.end method
