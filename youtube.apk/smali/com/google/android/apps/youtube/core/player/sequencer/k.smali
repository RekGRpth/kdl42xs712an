.class Lcom/google/android/apps/youtube/core/player/sequencer/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field protected volatile a:Z

.field final synthetic b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/j;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->c:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/k;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/k;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iput-object p1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/k;Ljava/lang/Exception;)V
    .locals 5

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v3

    instance-of v4, v3, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException;

    if-nez v4, :cond_0

    instance-of v3, v3, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException;

    if-eqz v3, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->NO_STREAMS:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    :cond_1
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/player/sequencer/j;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v4, p1}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4, p1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    :cond_2
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->d(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/n;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/n;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/k;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/sequencer/k;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->SEQUENCE_EMPTY:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->t()Landroid/util/Pair;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/j;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/j;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    new-array v0, v0, [I

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/j;[I)[I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->c(Lcom/google/android/apps/youtube/core/player/sequencer/j;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->d(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/l;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/sequencer/l;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/k;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method protected a(I)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b(I)Z

    return-void
.end method

.method protected final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->c:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/j;I)I

    return-void
.end method

.method protected final b(I)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->e(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->k(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineNoMediaException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineNoMediaException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a(Ljava/lang/Exception;)V

    move v0, v1

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->d(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/core/player/sequencer/m;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/m;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/k;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v0, 0x1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a(Ljava/lang/Exception;)V

    move v0, v1

    goto :goto_0
.end method

.method public final run()V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->c:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-boolean v1, v1, Lcom/google/android/apps/youtube/core/player/sequencer/j;->k:Z

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->b:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/j;)[I

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/k;->c:I

    aget v0, v0, v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a(I)V

    return-void
.end method
