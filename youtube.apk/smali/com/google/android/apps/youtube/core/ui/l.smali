.class public Lcom/google/android/apps/youtube/core/ui/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/youtube/common/a/b;
.implements Lcom/google/android/apps/youtube/common/fromguava/d;
.implements Lcom/google/android/apps/youtube/core/ui/j;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final b:Lcom/google/android/apps/youtube/core/aw;

.field protected final c:Landroid/app/Activity;

.field final d:Landroid/os/Handler;

.field protected final e:Lcom/google/android/apps/youtube/core/ui/PagedView;

.field protected final f:Lcom/google/android/apps/youtube/core/a/j;

.field private final g:Lcom/google/android/apps/youtube/common/a/a;

.field private final h:Lcom/google/android/apps/youtube/core/a/a;

.field private final i:Lcom/google/android/apps/youtube/core/ui/k;

.field private j:Ljava/util/LinkedList;

.field private k:Lcom/google/android/apps/youtube/core/async/GDataRequest;

.field private l:I

.field private m:Z

.field private n:Landroid/net/Uri;

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->c:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/PagedView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/a/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->h:Lcom/google/android/apps/youtube/core/a/a;

    invoke-interface {p2}, Lcom/google/android/apps/youtube/core/ui/PagedView;->h()Lcom/google/android/apps/youtube/core/ui/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->i:Lcom/google/android/apps/youtube/core/ui/k;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->i:Lcom/google/android/apps/youtube/core/ui/k;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/k;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcom/google/android/apps/youtube/core/a/j;->a(Lcom/google/android/apps/youtube/core/a/a;Landroid/view/View;Z)Lcom/google/android/apps/youtube/core/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->setOnScrollListener(Lcom/google/android/apps/youtube/core/ui/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->setOnRetryClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->i:Lcom/google/android/apps/youtube/core/ui/k;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/core/ui/k;->a(Landroid/view/View$OnClickListener;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->j:Ljava/util/LinkedList;

    invoke-static {p1, p0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->g:Lcom/google/android/apps/youtube/common/a/a;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/m;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/ui/m;-><init>(Lcom/google/android/apps/youtube/core/ui/l;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->d:Landroid/os/Handler;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/async/GDataRequest;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->m:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/ui/l;->k:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->i:Lcom/google/android/apps/youtube/core/ui/k;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/k;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->d()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->a:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->g:Lcom/google/android/apps/youtube/common/a/a;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->e()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/ui/l;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/l;->i()V

    return-void
.end method

.method private a(Ljava/lang/String;ZZ)V
    .locals 1

    if-nez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->i:Lcom/google/android/apps/youtube/core/ui/k;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/ui/k;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/j;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->d()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->h:Lcom/google/android/apps/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/a;->a()V

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/ui/PagedView;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/ui/PagedView;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private i()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->m:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/l;->j()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->k:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/ui/PagedView;->a()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->k:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    goto :goto_0

    :cond_3
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/l;->n_()V

    goto :goto_0
.end method

.method private j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/l;->n_()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->o:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->c()V

    return-void
.end method

.method public final a(III)V
    .locals 2

    add-int v0, p1, p2

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "required_items_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->p:I

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V
    .locals 10

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/apps/youtube/core/ui/l;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->k:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->a()I

    move-result v2

    iget v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->totalResults:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->l:I

    iget-object v3, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->o:I

    iget v5, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->startIndex:I

    sub-int/2addr v0, v5

    add-int/lit8 v0, v0, 0x1

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    move v9, v0

    move v0, v1

    move v1, v9

    :goto_1
    if-ge v1, v5, :cond_3

    if-ge v0, v2, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    iget v7, p0, Lcom/google/android/apps/youtube/core/ui/l;->r:I

    if-lez v7, :cond_2

    iget v6, p0, Lcom/google/android/apps/youtube/core/ui/l;->r:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/google/android/apps/youtube/core/ui/l;->r:I

    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v6}, Lcom/google/android/apps/youtube/core/ui/l;->a(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    iget v6, p0, Lcom/google/android/apps/youtube/core/ui/l;->s:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/apps/youtube/core/ui/l;->s:I

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->o:I

    iget v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->startIndex:I

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->o:I

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->q:I

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->q:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Received "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " entries; after filtering "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; realLastIndex = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->f(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/a/j;->a(Ljava/lang/Iterable;)V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/util/List;)V

    :goto_3
    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->q:I

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/l;->a()V

    :goto_4
    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->s:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/ui/PagedView;->b()I

    move-result v1

    if-lt v0, v1, :cond_4

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->q:I

    iget v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->p:I

    if-ge v0, v1, :cond_7

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/l;->i()V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/l;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->d()V

    goto :goto_4

    :cond_7
    iput v8, p0, Lcom/google/android/apps/youtube/core/ui/l;->s:I

    goto/16 :goto_0
.end method

.method public a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/ui/l;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/j;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v2, 0x193

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->c:Landroid/app/Activity;

    sget v2, Lcom/google/android/youtube/p;->bm:I

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a(Ljava/lang/String;ZZ)V

    :goto_0
    return-void

    :cond_0
    instance-of v0, p2, Ljava/lang/IllegalArgumentException;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->i:Lcom/google/android/apps/youtube/core/ui/k;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/k;->c()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a(Ljava/lang/String;ZZ)V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/ui/i;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/ui/PagedView;->setOnPagedViewStateChangeListener(Lcom/google/android/apps/youtube/core/ui/i;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V

    return-void
.end method

.method public final varargs a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V
    .locals 3

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "requests cannot be empty"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/l;->c()V

    :goto_1
    array-length v0, p1

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->j:Ljava/util/LinkedList;

    aget-object v2, p1, v1

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/l;->j()V

    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/core/a/j;->a(ILjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->r:I

    return-void
.end method

.method public c()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->n:Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/ui/l;->k:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->o:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->l:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->r:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->s:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->q:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/j;->a()V

    return-void
.end method

.method public final d()Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "required_items_count"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/ui/PagedView;->i()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method public final e()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/l;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->e()V

    return-void
.end method

.method public final f()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/l;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->c()V

    return-void
.end method

.method protected final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->f:Lcom/google/android/apps/youtube/core/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/j;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->i:Lcom/google/android/apps/youtube/core/ui/k;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/ui/k;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->i:Lcom/google/android/apps/youtube/core/ui/k;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/k;->d()V

    return-void
.end method

.method public final h()Lcom/google/android/apps/youtube/core/ui/PagedView$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->e:Lcom/google/android/apps/youtube/core/ui/PagedView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->f()Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    move-result-object v0

    return-object v0
.end method

.method protected n_()V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->k:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/l;->k:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    :cond_0
    return-void
.end method
