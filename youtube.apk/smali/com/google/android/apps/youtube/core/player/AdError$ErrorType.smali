.class public final enum Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

.field public static final enum AD_SURVEY_PARSING_ERROR:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

.field public static final enum NONE:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

.field public static final enum UNSUPPORTED_VIDEO_FORMAT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

.field public static final enum VIDEO_PLAYBACK_ERROR_CANNOT_CONNECT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

.field public static final enum VIDEO_PLAYBACK_ERROR_NO_NETWORK:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

.field public static final enum VIDEO_PLAYBACK_ERROR_TIMEOUT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

.field public static final enum VIDEO_PLAYBACK_ERROR_UNKNOWN_HOST:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

.field public static final enum VIDEO_PLAYBACK_UNKNOWN_ERROR:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;


# instance fields
.field public final internalError:I

.field public final monetizationError:I

.field public final vastError:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const-string v1, "NONE"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->NONE:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const-string v1, "VIDEO_PLAYBACK_ERROR_NO_NETWORK"

    const/4 v2, 0x1

    const/16 v3, 0xa

    const/4 v4, 0x7

    const/16 v5, 0x195

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_NO_NETWORK:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const-string v1, "VIDEO_PLAYBACK_ERROR_UNKNOWN_HOST"

    const/4 v2, 0x2

    const/16 v3, 0xb

    const/4 v4, 0x7

    const/16 v5, 0x191

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_UNKNOWN_HOST:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const-string v1, "VIDEO_PLAYBACK_ERROR_CANNOT_CONNECT"

    const/4 v2, 0x3

    const/16 v3, 0xc

    const/4 v4, 0x7

    const/16 v5, 0x191

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_CANNOT_CONNECT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const-string v1, "VIDEO_PLAYBACK_ERROR_TIMEOUT"

    const/4 v2, 0x4

    const/16 v3, 0xd

    const/4 v4, 0x3

    const/16 v5, 0x192

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_TIMEOUT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const-string v1, "VIDEO_PLAYBACK_UNKNOWN_ERROR"

    const/4 v2, 0x5

    const/16 v3, 0xe

    const/4 v4, 0x3

    const/16 v5, 0x195

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_UNKNOWN_ERROR:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const-string v1, "UNSUPPORTED_VIDEO_FORMAT"

    const/4 v2, 0x6

    const/16 v3, 0xf

    const/4 v4, 0x6

    const/16 v5, 0x193

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->UNSUPPORTED_VIDEO_FORMAT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const-string v1, "AD_SURVEY_PARSING_ERROR"

    const/4 v2, 0x7

    const/16 v3, 0x14

    const/16 v4, 0xa

    const/16 v5, 0x384

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->AD_SURVEY_PARSING_ERROR:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->NONE:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_NO_NETWORK:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_UNKNOWN_HOST:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_CANNOT_CONNECT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_TIMEOUT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_UNKNOWN_ERROR:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->UNSUPPORTED_VIDEO_FORMAT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->AD_SURVEY_PARSING_ERROR:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->$VALUES:[Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->internalError:I

    iput p4, p0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->monetizationError:I

    iput p5, p0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->vastError:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->$VALUES:[Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    return-object v0
.end method
