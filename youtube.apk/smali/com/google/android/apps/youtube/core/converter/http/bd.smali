.class final Lcom/google/android/apps/youtube/core/converter/http/bd;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "review_count"

    invoke-interface {p2, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/e/m;->b(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    const-string v2, "app_name"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "price"

    invoke-interface {p2, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    move-result-object v0

    const-string v1, "rating"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;F)F

    move-result v1

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a(F)Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    const-string v1, "rating_image"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/l;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    return-void

    :catch_0
    move-exception v1

    const-string v1, "Badly formed rating image uri - ignoring"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 2

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    return-void
.end method
