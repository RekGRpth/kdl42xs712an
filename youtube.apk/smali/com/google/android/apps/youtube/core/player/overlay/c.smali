.class public final Lcom/google/android/apps/youtube/core/player/overlay/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/b;
.implements Lcom/google/android/apps/youtube/core/player/overlay/y;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/a;

.field private final b:Lcom/google/android/apps/youtube/core/player/ae;

.field private final c:Lcom/google/android/apps/youtube/core/client/bc;

.field private final d:Lcom/google/android/apps/youtube/core/client/bj;

.field private final e:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/youtube/core/Analytics;

.field private final g:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final h:Landroid/content/Context;

.field private final i:Landroid/os/Handler;

.field private final j:Lcom/google/android/apps/youtube/common/e/b;

.field private k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private l:Z

.field private m:Z

.field private n:Lcom/google/android/apps/youtube/common/a/d;

.field private o:Lcom/google/android/apps/youtube/common/a/d;

.field private p:Lcom/google/android/apps/youtube/common/a/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/a;Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/datalib/d/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->a:Lcom/google/android/apps/youtube/core/player/overlay/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->h:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->d:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/core/player/ae;->a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->e:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iput-object p6, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->f:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->g:Lcom/google/android/apps/youtube/datalib/d/a;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->i:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->j:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {p1, p0}, Lcom/google/android/apps/youtube/core/player/overlay/a;->setListener(Lcom/google/android/apps/youtube/core/player/overlay/b;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/c;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->n:Lcom/google/android/apps/youtube/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/c;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object p1
.end method

.method private static a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdOwnerName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/c;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->p:Lcom/google/android/apps/youtube/common/a/d;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/core/player/overlay/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->a:Lcom/google/android/apps/youtube/core/player/overlay/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/c;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/overlay/c;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->i:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/common/a/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->n:Lcom/google/android/apps/youtube/common/a/d;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->c:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method private e()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->o:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->o:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->o:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->n:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->n:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->n:Lcom/google/android/apps/youtube/common/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->p:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->p:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->p:Lcom/google/android/apps/youtube/common/a/d;

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->a:Lcom/google/android/apps/youtube/core/player/overlay/a;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/a;->d()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/common/a/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->p:Lcom/google/android/apps/youtube/common/a/d;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/core/client/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->d:Lcom/google/android/apps/youtube/core/client/bj;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->f:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "LearnMore"

    const-string v2, "Menu"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->b()V

    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->e:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->t()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->e:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->g:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/google/a/a/a/a/kz;

    invoke-direct {v2}, Lcom/google/a/a/a/a/kz;-><init>()V

    new-instance v3, Lcom/google/a/a/a/a/tu;

    invoke-direct {v3}, Lcom/google/a/a/a/a/tu;-><init>()V

    iput-object v3, v2, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    iget-object v3, v2, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/a/a/a/a/tu;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/datalib/d/a;->a(Lcom/google/a/a/a/a/kz;)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->e:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->k()V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdOwnerUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->e:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->g:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdOwnerUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/p;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/a/a/a/a/kz;

    invoke-direct {v2}, Lcom/google/a/a/a/a/kz;-><init>()V

    new-instance v3, Lcom/google/a/a/a/a/jx;

    invoke-direct {v3}, Lcom/google/a/a/a/a/jx;-><init>()V

    iput-object v3, v2, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    iget-object v3, v2, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    iput-object v1, v3, Lcom/google/a/a/a/a/jx;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/datalib/d/a;->a(Lcom/google/a/a/a/a/kz;)V

    :cond_0
    return-void
.end method

.method public final handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->l:Z

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->onAd()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->e()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/f;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/f;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/c;B)V

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->o:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->c:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->i:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->o:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v4, v5}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->l:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->j:Lcom/google/android/apps/youtube/common/e/b;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPlayAd(Lcom/google/android/apps/youtube/common/e/b;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->m:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->a:Lcom/google/android/apps/youtube/core/player/overlay/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/overlay/c;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isSkippable()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_2

    move v0, v1

    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v0, v5}, Lcom/google/android/apps/youtube/core/player/overlay/a;->a(Ljava/lang/String;ZZLjava/lang/String;)V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->m:Z

    :cond_3
    :goto_0
    return-void

    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->e()V

    goto :goto_0
.end method

.method public final handleVideoTimeEvent(Lcom/google/android/apps/youtube/core/player/event/ae;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/c;->a:Lcom/google/android/apps/youtube/core/player/overlay/a;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->b()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/a;->a(II)V

    :cond_0
    return-void
.end method
