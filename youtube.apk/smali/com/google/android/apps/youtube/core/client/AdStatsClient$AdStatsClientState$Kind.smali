.class public final enum Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

.field public static final enum INSTREAM:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

.field public static final enum TRUEVIEW_INDISPLAY:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;


# instance fields
.field private final kind:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    const-string v1, "INSTREAM"

    const-string v2, "1"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->INSTREAM:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    const-string v1, "TRUEVIEW_INDISPLAY"

    const-string v2, "2"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->TRUEVIEW_INDISPLAY:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->INSTREAM:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->TRUEVIEW_INDISPLAY:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->$VALUES:[Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->kind:Ljava/lang/String;

    return-void
.end method

.method public static fromKindString(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->INSTREAM:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->getKindString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->INSTREAM:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->TRUEVIEW_INDISPLAY:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->getKindString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->TRUEVIEW_INDISPLAY:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->$VALUES:[Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    return-object v0
.end method


# virtual methods
.method public final getKindString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->kind:Ljava/lang/String;

    return-object v0
.end method
