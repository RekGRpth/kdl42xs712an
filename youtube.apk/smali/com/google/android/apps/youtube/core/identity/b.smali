.class public final Lcom/google/android/apps/youtube/core/identity/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/core/identity/as;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Ljava/lang/String;

.field private volatile e:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->a:Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->b:Lcom/google/android/apps/youtube/core/identity/as;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->c:Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/identity/as;Ljava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/as;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->b:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->c:Ljava/util/concurrent/Executor;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->d:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/b;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/b;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/identity/b;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method final a(Lcom/google/android/apps/youtube/core/identity/f;)Ljava/util/concurrent/Future;
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/identity/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->b:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/as;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v3, "Account removed from device."

    invoke-direct {v1, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/identity/a;->b(Ljava/lang/Exception;)Lcom/google/android/apps/youtube/core/identity/a;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/common/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-object v2

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->c:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/apps/youtube/core/identity/c;

    invoke-direct {v3, p0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/c;-><init>(Lcom/google/android/apps/youtube/core/identity/b;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/c;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/core/identity/l;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/identity/l;->g()Lcom/google/android/apps/youtube/core/identity/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/f;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/b;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/b;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/auth/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
