.class public final Lcom/google/android/apps/youtube/core/player/fetcher/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/innertube/ay;

.field private final b:Lcom/google/android/apps/youtube/common/c/a;


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/e;->b:Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a:Lcom/google/android/apps/youtube/datalib/innertube/ay;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/datalib/innertube/ay;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/e;->b:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/ay;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a:Lcom/google/android/apps/youtube/datalib/innertube/ay;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/fetcher/e;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/e;->b:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 7

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getParams()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getClickTrackingParams()[B

    move-result-object v5

    move-object v0, p0

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLcom/google/android/apps/youtube/datalib/a/l;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/e;->b:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/q;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/q;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/fetcher/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p6, v1}, Lcom/google/android/apps/youtube/core/player/fetcher/f;-><init>(Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/datalib/a/l;B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a:Lcom/google/android/apps/youtube/datalib/innertube/ay;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/ay;->a()Lcom/google/android/apps/youtube/datalib/innertube/bb;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/bb;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/bb;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/youtube/datalib/innertube/bb;->a(I)Lcom/google/android/apps/youtube/datalib/innertube/bb;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/bb;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/bb;

    invoke-virtual {v1, p4}, Lcom/google/android/apps/youtube/datalib/innertube/bb;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/bb;

    invoke-virtual {v1, p5}, Lcom/google/android/apps/youtube/datalib/innertube/bb;->a([B)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a:Lcom/google/android/apps/youtube/datalib/innertube/ay;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ay;->a(Lcom/google/android/apps/youtube/datalib/innertube/bb;Lcom/google/android/apps/youtube/datalib/a/l;)V

    return-void
.end method
