.class public Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/apps/youtube/core/player/overlay/g;


# instance fields
.field private final A:Landroid/view/animation/Animation;

.field private final B:Landroid/view/animation/Animation;

.field private final C:Landroid/view/animation/Animation;

.field private final D:Landroid/view/animation/Animation;

.field private final E:Landroid/view/animation/Animation;

.field private final F:Landroid/view/animation/Animation;

.field protected final a:Landroid/widget/TextView;

.field protected final b:Landroid/widget/TextView;

.field protected final c:Landroid/widget/TextView;

.field protected final d:Landroid/widget/TextView;

.field private e:Lcom/google/android/apps/youtube/core/player/overlay/h;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/view/View;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/ImageButton;

.field private final j:Landroid/view/View;

.field private final k:Landroid/widget/ImageView;

.field private final l:Landroid/view/View;

.field private final m:Landroid/view/View;

.field private final n:Landroid/widget/ImageView;

.field private final o:Landroid/widget/ImageButton;

.field private final p:Landroid/view/View;

.field private final q:Landroid/view/View;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/ImageView;

.field private t:Z

.field private final u:I

.field private v:Z

.field private final w:Landroid/view/animation/Animation;

.field private final x:Landroid/view/animation/Animation;

.field private final y:Landroid/view/animation/Animation;

.field private final z:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/b;->c:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->w:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->d:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->x:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->a:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->y:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->b:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->z:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->e:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->A:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->f:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->B:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->e:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->C:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->f:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->D:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->e:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->E:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->f:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->F:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/k;->h:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->w:Landroid/view/animation/Animation;

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->x:Landroid/view/animation/Animation;

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->x:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->B:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->A:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->F:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/l;->e:I

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->bg:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->f:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->bh:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->bk:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->h:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->bi:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->bl:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->bj:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->i:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    sget v0, Lcom/google/android/youtube/j;->L:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->R:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->k:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->O:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    sget v0, Lcom/google/android/youtube/j;->P:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->R:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->n:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->S:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->M:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->N:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->o:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    sget v0, Lcom/google/android/youtube/j;->bM:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->p:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->bN:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->q:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->q:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->bP:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->r:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->q:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->bO:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->s:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    sget v0, Lcom/google/android/youtube/g;->d:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget v2, Lcom/google/android/youtube/g;->c:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v0, v2

    sget v2, Lcom/google/android/youtube/g;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    sget v2, Lcom/google/android/youtube/g;->b:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->u:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->a()V

    return-void
.end method

.method private static a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V
    .locals 1

    if-ne p0, p1, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->t:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->u:I

    :goto_0
    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->t:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->u:I

    :goto_1
    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->i:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->t:Z

    if-eqz v0, :cond_2

    move v0, v3

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->o:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->t:Z

    if-eqz v1, :cond_3

    :goto_3
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_3
.end method

.method private l()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->f:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->p:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->v:Z

    if-nez v2, :cond_2

    :goto_1
    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->setVisibility(I)V

    return v1

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    const/16 v0, 0x8

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x8

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->t:Z

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->v:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->k()V

    return-void
.end method

.method public final a(Z)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->A:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l()Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->B:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->C:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l()Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->y:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l()Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->B:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void
.end method

.method public final i()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->q:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l()Z

    return-void
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->q:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->F:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->x:Landroid/view/animation/Animation;

    invoke-static {p1, v0, p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->z:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->D:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->B:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->F:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->p:Landroid/view/View;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->A:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->B:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/h;

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->g:Landroid/view/View;

    if-ne p1, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/h;->c()V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->i:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/h;->d()V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->m:Landroid/view/View;

    if-ne p1, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/h;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/h;->a(Z)V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->j:Landroid/view/View;

    if-ne p1, v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/h;

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/h;->a(Z)V

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->o:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/h;->b()V

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->p:Landroid/view/View;

    if-ne p1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/h;->e()V

    goto :goto_1
.end method

.method public setAdStyle(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->t:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->k()V

    return-void
.end method

.method public setCallToActionImage(Landroid/graphics/Bitmap;)V
    .locals 4

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->n:Landroid/widget/ImageView;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->k:Landroid/widget/ImageView;

    if-nez p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public setCallToActionText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setFeaturedChannelImage(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setFeaturedVideoImage(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->h:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public setFeaturedVideoTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setInfoCardTeaserImage(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->s:Landroid/widget/ImageView;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setInfoCardTeaserMessage(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setListener(Lcom/google/android/apps/youtube/core/player/overlay/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/h;

    return-void
.end method

.method public setVisible(Z)V
    .locals 1

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->v:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->v:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->x:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->w:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method
