.class public final Lcom/google/android/apps/youtube/core/converter/http/cu;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/apps/youtube/core/converter/f;)V
    .locals 2

    const-string v0, "/entry"

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/da;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/da;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    const-string v0, "/entry"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/converter/http/cu;->c(Lcom/google/android/apps/youtube/core/converter/f;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/core/converter/f;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/cv;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/cv;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/converter/http/cu;->c(Lcom/google/android/apps/youtube/core/converter/f;Ljava/lang/String;)V

    return-void
.end method

.method public static b(Lcom/google/android/apps/youtube/core/converter/f;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/cy;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/http/cy;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/entry"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/cx;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/cx;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/converter/http/cu;->c(Lcom/google/android/apps/youtube/core/converter/f;Ljava/lang/String;)V

    return-void
.end method

.method private static c(Lcom/google/android/apps/youtube/core/converter/f;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/category"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/cw;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/cw;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/content"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/df;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/df;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/yt:username"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/de;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/de;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/yt:playlistTitle"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/dd;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/dd;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/yt:queryString"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/dc;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/dc;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/link"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/db;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/db;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    return-void
.end method
