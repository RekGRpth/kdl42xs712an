.class public final Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Lcom/google/android/apps/youtube/core/client/p;

.field private final d:Lcom/google/android/apps/youtube/core/client/h;

.field private final e:Lcom/google/android/apps/youtube/core/async/u;

.field private final f:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/client/h;)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->b:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/p;

    invoke-direct {v0, p6}, Lcom/google/android/apps/youtube/core/client/p;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->c:Lcom/google/android/apps/youtube/core/client/p;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->d:Lcom/google/android/apps/youtube/core/client/h;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/k;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/google/android/apps/youtube/core/client/k;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/b;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, p7, v2}, Lcom/google/android/apps/youtube/core/converter/http/b;-><init>(Lcom/google/android/apps/youtube/core/client/h;Ljava/util/List;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/gs;

    new-instance v3, Lcom/google/android/apps/youtube/core/converter/http/dj;

    invoke-direct {v3, p4}, Lcom/google/android/apps/youtube/core/converter/http/dj;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    invoke-direct {v2, p4, p5, v3}, Lcom/google/android/apps/youtube/core/converter/http/gs;-><init>(Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/http/dj;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/k;->a(Lcom/google/android/apps/youtube/core/converter/http/b;Lcom/google/android/apps/youtube/core/converter/http/gs;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->e:Lcom/google/android/apps/youtube/core/async/u;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;-><init>()V

    const-string v1, "AdSense"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const-string v1, "http://google.com/fakeimpression"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const-string v1, "http://googleads.g.doubleclick.net/pagead/conversion/?label=clicktracking"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const-string v1, "http://google.com/conversion"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->v(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "http://google.com/fake_ping?x=[I_X]&y=[I_Y]"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sget-object v2, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->SKIPPABLE_INSTREAM:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v3

    invoke-interface {p5}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v3

    const-string v4, "http://www.youtube.com/watch?v=s4ur2BDVyYE"

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->a(Ljava/lang/String;)Lcom/google/a/a/a/a/fj;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/a/a/a/a/fj;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->SKIPPABLE_INSTREAM_WITH_CTA_ANNOTATION:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v3

    invoke-interface {p5}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v3

    const-string v4, "http://www.youtube.com/watch?v=IHLWtUP1sTs#modules"

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->a(Ljava/lang/String;)Lcom/google/a/a/a/a/fj;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/a/a/a/a/fj;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;-><init>()V

    new-instance v3, Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->MULTI_SELECT:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    const-string v5, "Which of the following have you seen advertising for recently? Select all that apply."

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;Ljava/lang/String;)V

    const-string v4, "Coca Cola"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v3

    const-string v4, "Pepsi"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v3

    const-string v4, "Rivella"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v3

    const-string v4, "Sprite"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v3

    const-string v4, "//www.youtube.com/survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v3

    const/16 v4, 0x14

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b(I)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;)Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->BRAND_SURVEY:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->BRAND_SURVEY_NON_SKIPPABLE:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->APP_PROMOTION_COMPANION_CARD:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-interface {p5}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    const-string v4, "Colossatron"

    const-string v5, "$0.99"

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v4, 0x40833333    # 4.1f

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a(F)Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    move-result-object v3

    const-string v4, "http://googleads.g.doubleclick.net/pagead/images/gmob/4_5-stars.png"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    move-result-object v3

    const v4, 0x84d0

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    move-result-object v3

    const-string v4, "https://lh3.ggpht.com/J1ltAGYVyu0G7xwqlE--3pRZ3wSpjltqfbLd5wK2aQ5TtnG6k87q8dvUOJaNABMkRX0=w64-rw"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/youtube/datalib/legacy/model/i;

    const/4 v5, 0x4

    const-string v6, "https://play.google.com/store/apps/details?id=com.halfbrick.colossatron"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const-string v7, "INSTALL"

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/i;-><init>(ILandroid/net/Uri;Ljava/lang/String;)V

    const-string v5, "http://www.google.com/search?q=action"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/i;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/f;-><init>(I)V

    invoke-virtual {v5, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;

    const/4 v5, 0x1

    const-string v6, "http://www.google.com/search?q=impression"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;-><init>(ILandroid/net/Uri;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const-string v3, "http://www.youtube.com/watch?v=s4ur2BDVyYE"

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->a(Ljava/lang/String;)Lcom/google/a/a/a/a/fj;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/a/a/a/a/fj;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->f:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->a:Landroid/content/Context;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Lcom/google/a/a/a/a/fj;
    .locals 1

    new-instance v0, Lcom/google/a/a/a/a/fj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fj;-><init>()V

    iput-object p0, v0, Lcom/google/a/a/a/a/fj;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;Lcom/google/android/apps/youtube/core/converter/http/c;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->c:Lcom/google/android/apps/youtube/core/client/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/p;->b()Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/converter/http/c;->a:Ljava/lang/String;

    new-instance v2, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;-><init>()V

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->d:Lcom/google/android/apps/youtube/core/client/h;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/h;->a(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->e:Lcom/google/android/apps/youtube/core/async/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/u;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/converter/http/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/o;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/core/client/o;-><init>(Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;Lcom/google/android/apps/youtube/core/converter/http/c;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
