.class final Lcom/google/android/apps/youtube/core/converter/http/fr;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 3

    invoke-static {p3}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    const-class v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->publishedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->getUploadedDate()Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    :cond_0
    return-void
.end method
