.class public Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/be;


# instance fields
.field private final a:Landroid/util/SparseArray;

.field private final b:Landroid/util/SparseArray;

.field private c:F

.field private d:F

.field private e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/g;->Q:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->d:F

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultBackgroundColorValue()I

    move-result v1

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultWindowColorValue()I

    move-result v2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultEdgeColorValue()I

    move-result v3

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->getDefaultEdgeTypeValue()I

    move-result v4

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultTextColorValue()I

    move-result v5

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->getDefaultFontValue()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->setVisibility(I)V

    return-void
.end method

.method private a(II)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->c:F

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/Context;FII)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->d:F

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a(Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;)V
    .locals 3

    const/16 v2, 0xa

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->d:F

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setTextBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getWindowColor()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getForegroundColor()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getTypeface()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->getTypefaceFromFontValue(ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getEdgeColor()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setEdgeColor(I)V

    invoke-virtual {p1, v2, v2, v2, v2}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getEdgeType()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setEdgeType(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 7

    const/4 v2, 0x0

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move v0, v2

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;

    iget v1, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->windowId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    iget v5, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v1, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    iget-boolean v5, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->visible:Z

    if-nez v5, :cond_3

    :cond_1
    if-eqz v1, :cond_2

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setVisibility(I)V

    :cond_2
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    iget v6, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v5, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    new-instance v5, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a(Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;)V

    invoke-virtual {v5, v1}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setWindowSnapshot(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;)V

    invoke-virtual {p0, v5}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {v1, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    :cond_4
    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getTag()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setWindowSnapshot(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;)V

    :cond_5
    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_3

    :cond_7
    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->setVisibility(I)V

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final d()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method public final e()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->setVisibility(I)V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->requestLayout()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    sub-int v0, p4, p2

    sub-int v1, p5, p3

    mul-int/lit8 v2, v0, 0xf

    div-int/lit8 v2, v2, 0x64

    div-int/lit8 v4, v2, 0x2

    mul-int/lit8 v2, v1, 0xf

    div-int/lit8 v2, v2, 0x64

    div-int/lit8 v5, v2, 0x2

    mul-int/lit8 v0, v0, 0x55

    div-int/lit8 v6, v0, 0x64

    mul-int/lit8 v0, v1, 0x55

    div-int/lit8 v7, v0, 0x64

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getMeasuredWidth()I

    move-result v8

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getMeasuredHeight()I

    move-result v9

    iget-object v10, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    iget v11, v10, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorPoint:I

    iget v1, v10, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorHorizontalPos:I

    mul-int/2addr v1, v6

    div-int/lit8 v3, v1, 0x64

    iget v1, v10, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorVerticalPos:I

    mul-int/2addr v1, v7

    div-int/lit8 v1, v1, 0x64

    iget-boolean v10, v10, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->rollUp:Z

    if-nez v10, :cond_3

    and-int/lit8 v10, v11, 0x1

    if-eqz v10, :cond_1

    :goto_1
    and-int/lit8 v10, v11, 0x8

    if-eqz v10, :cond_4

    :goto_2
    add-int/2addr v3, v4

    add-int/2addr v1, v5

    add-int/2addr v8, v3

    add-int/2addr v9, v1

    invoke-virtual {v0, v3, v1, v8, v9}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->layout(IIII)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    and-int/lit8 v10, v11, 0x2

    if-eqz v10, :cond_2

    div-int/lit8 v10, v8, 0x2

    sub-int/2addr v3, v10

    goto :goto_1

    :cond_2
    and-int/lit8 v10, v11, 0x4

    if-eqz v10, :cond_3

    sub-int/2addr v3, v8

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    and-int/lit8 v10, v11, 0x10

    if-eqz v10, :cond_5

    div-int/lit8 v10, v9, 0x2

    sub-int/2addr v1, v10

    goto :goto_2

    :cond_5
    and-int/lit8 v10, v11, 0x20

    if-eqz v10, :cond_6

    sub-int/2addr v1, v9

    goto :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    :cond_7
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->setMeasuredDimension(II)V

    invoke-direct {p0, v5, v6}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a(II)V

    move v2, v3

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->b:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    iget v7, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorPoint:I

    iget v4, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorHorizontalPos:I

    mul-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x64

    iget v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorVerticalPos:I

    mul-int/2addr v1, v6

    div-int/lit8 v1, v1, 0x64

    and-int/lit8 v8, v7, 0x1

    if-eqz v8, :cond_3

    sub-int v4, v5, v4

    :cond_0
    :goto_1
    and-int/lit8 v8, v7, 0x8

    if-eqz v8, :cond_5

    sub-int v1, v6, v1

    :cond_1
    :goto_2
    invoke-static {v4, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/youtube/core/ui/SubtitleWindowView;->measure(II)V

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    and-int/lit8 v8, v7, 0x2

    if-eqz v8, :cond_4

    sub-int v8, v5, v4

    invoke-static {v4, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    goto :goto_1

    :cond_4
    and-int/lit8 v8, v7, 0x4

    if-nez v8, :cond_0

    move v4, v3

    goto :goto_1

    :cond_5
    and-int/lit8 v8, v7, 0x10

    if-eqz v8, :cond_6

    sub-int v7, v6, v1

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    goto :goto_2

    :cond_6
    and-int/lit8 v7, v7, 0x20

    if-nez v7, :cond_1

    move v1, v3

    goto :goto_2

    :cond_7
    return-void
.end method

.method public setFontScale(F)V
    .locals 2

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->c:F

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a(II)V

    return-void
.end method

.method public setSubtitlesStyle(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;->a(II)V

    return-void
.end method
