.class public final enum Lcom/google/android/apps/youtube/core/client/AdFormat;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/client/AdFormat;

.field public static final enum DOUBLECLICK_VIDEO_MASTHEAD:Lcom/google/android/apps/youtube/core/client/AdFormat;


# instance fields
.field private final adFormatString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/client/AdFormat;

    const-string v1, "DOUBLECLICK_VIDEO_MASTHEAD"

    const-string v2, "1_8"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/core/client/AdFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/AdFormat;->DOUBLECLICK_VIDEO_MASTHEAD:Lcom/google/android/apps/youtube/core/client/AdFormat;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/client/AdFormat;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/AdFormat;->DOUBLECLICK_VIDEO_MASTHEAD:Lcom/google/android/apps/youtube/core/client/AdFormat;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/youtube/core/client/AdFormat;->$VALUES:[Lcom/google/android/apps/youtube/core/client/AdFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/AdFormat;->adFormatString:Ljava/lang/String;

    return-void
.end method

.method public static fromAdFormatString(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdFormat;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/AdFormat;->DOUBLECLICK_VIDEO_MASTHEAD:Lcom/google/android/apps/youtube/core/client/AdFormat;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/AdFormat;->getAdFormatString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/client/AdFormat;->DOUBLECLICK_VIDEO_MASTHEAD:Lcom/google/android/apps/youtube/core/client/AdFormat;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromOrdinal(I)Lcom/google/android/apps/youtube/core/client/AdFormat;
    .locals 1

    if-ltz p0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/client/AdFormat;->values()[Lcom/google/android/apps/youtube/core/client/AdFormat;

    move-result-object v0

    array-length v0, v0

    if-lt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/android/apps/youtube/core/client/AdFormat;->values()[Lcom/google/android/apps/youtube/core/client/AdFormat;

    move-result-object v0

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdFormat;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/client/AdFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/AdFormat;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/client/AdFormat;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/AdFormat;->$VALUES:[Lcom/google/android/apps/youtube/core/client/AdFormat;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/client/AdFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/client/AdFormat;

    return-object v0
.end method


# virtual methods
.method public final getAdFormatString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/AdFormat;->adFormatString:Ljava/lang/String;

    return-object v0
.end method
