.class public final Lcom/google/android/apps/youtube/core/identity/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final b:Lcom/google/android/apps/youtube/core/async/af;

.field private final c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Lcom/google/android/apps/youtube/core/identity/b;


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->a:Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->b:Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->d:Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->e:Lcom/google/android/apps/youtube/core/identity/b;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/utils/ai;Lcom/google/android/apps/youtube/core/converter/n;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->d:Ljava/util/concurrent/Executor;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->e:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/a/p;

    instance-of v0, v0, Lcom/google/android/apps/youtube/core/identity/k;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "IdentityClient must not have an authentication header decorator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;->V_2_1:Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    invoke-static {v0, p4, p5, p7, v1}, Lcom/google/android/apps/youtube/core/converter/http/ao;->a(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/utils/ai;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;)Lcom/google/android/apps/youtube/core/converter/http/ao;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/async/u;

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/bv;

    invoke-direct {v2, p8}, Lcom/google/android/apps/youtube/core/converter/http/bv;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    invoke-direct {v1, p2, v0, v2}, Lcom/google/android/apps/youtube/core/async/u;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/identity/h;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/u;

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/dx;

    invoke-direct {v2, p8}, Lcom/google/android/apps/youtube/core/converter/http/dx;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    invoke-direct {v1, p2, v0, v2}, Lcom/google/android/apps/youtube/core/async/u;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/identity/h;->b:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/identity/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->e:Lcom/google/android/apps/youtube/core/identity/b;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->a:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->b:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method


# virtual methods
.method final a(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/identity/f;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/i;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/core/identity/i;-><init>(Lcom/google/android/apps/youtube/core/identity/h;Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/h;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/j;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/core/identity/j;-><init>(Lcom/google/android/apps/youtube/core/identity/h;Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
