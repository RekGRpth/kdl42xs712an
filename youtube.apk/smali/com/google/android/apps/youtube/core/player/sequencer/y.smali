.class final Lcom/google/android/apps/youtube/core/player/sequencer/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

.field private final b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

.field private volatile c:Z

.field private volatile d:Z

.field private volatile e:Ljava/lang/Runnable;

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/u;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->d:Z

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->f:Z

    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/aa;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/aa;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/y;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/y;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/sequencer/y;)Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    return-object v0
.end method

.method private b(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/ad;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/ad;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/y;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->d:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->c:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    monitor-exit p0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->e:Ljava/lang/Runnable;

    const/4 v0, 0x1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final run()V
    .locals 9

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/r;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/r;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Lcom/google/android/apps/youtube/core/player/fetcher/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Lcom/google/android/apps/youtube/datalib/a/l;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->f:Z

    if-eqz v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v8}, Lcom/google/android/apps/youtube/datalib/a/k;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getVideoId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->f:Z

    if-eqz v0, :cond_3

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getClickTrackingParams()[B

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlayerParams()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v5

    const/4 v6, -0x1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->d:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Landroid/os/Handler;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/z;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/z;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/y;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_6

    :goto_0
    :try_start_3
    invoke-virtual {v8}, Lcom/google/android/apps/youtube/datalib/a/k;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/core/player/sequencer/ac;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/ac;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/y;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/ab;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/sequencer/ab;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/y;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_5

    :cond_1
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->d:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v1, v7

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :catch_3
    move-exception v0

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Ljava/lang/Exception;)V

    move-object v1, v7

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_5
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b(Ljava/lang/Exception;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_6
    move-exception v0

    move-object v7, v1

    goto :goto_3

    :catch_7
    move-exception v0

    goto :goto_2
.end method
