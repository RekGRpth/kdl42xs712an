.class final Lcom/google/android/apps/youtube/core/client/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/a/e;


# instance fields
.field protected final a:Ljava/util/Set;

.field final synthetic b:Lcom/google/android/apps/youtube/core/client/a/h;

.field private final c:Lcom/google/android/apps/youtube/core/client/a/b;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/client/a/h;Lcom/google/android/apps/youtube/core/client/a/b;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/a/j;->b:Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/j;->a:Ljava/util/Set;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/a/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/j;->c:Lcom/google/android/apps/youtube/core/client/a/b;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/client/a/h;Lcom/google/android/apps/youtube/core/client/a/b;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/client/a/j;-><init>(Lcom/google/android/apps/youtube/core/client/a/h;Lcom/google/android/apps/youtube/core/client/a/b;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Lcom/google/android/apps/youtube/core/client/a/e;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/j;->a:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/client/a/g;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/j;->c:Lcom/google/android/apps/youtube/core/client/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/j;->b:Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/a/b;->a(Lcom/google/android/apps/youtube/core/client/a/c;)Lcom/google/android/apps/youtube/core/client/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/j;->a:Ljava/util/Set;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/client/a/a;->a(Lcom/google/android/apps/youtube/core/client/a/g;Ljava/util/Set;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/j;->b:Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/client/a/h;->a(Lcom/google/android/apps/youtube/core/client/a/h;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
