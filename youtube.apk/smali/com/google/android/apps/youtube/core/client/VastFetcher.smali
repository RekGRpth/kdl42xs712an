.class public final Lcom/google/android/apps/youtube/core/client/VastFetcher;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J

.field private static final b:Ljava/util/Set;

.field private static final c:Ljava/util/Set;

.field private static final d:Ljava/util/Set;


# instance fields
.field private final e:Lcom/google/android/apps/youtube/core/async/af;

.field private final f:Lcom/google/android/apps/youtube/common/e/b;

.field private final g:Lcom/google/android/apps/youtube/core/player/fetcher/d;

.field private final h:Lcom/google/android/apps/youtube/core/client/h;

.field private final i:Lcom/google/android/apps/youtube/common/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a:J

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "YT:ADSENSE"

    aput-object v1, v0, v3

    const-string v1, "ADSENSE"

    aput-object v1, v0, v4

    const-string v1, "ADSENSE/ADX"

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/c;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->b:Ljava/util/Set;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "YT:DOUBLECLICK"

    aput-object v1, v0, v3

    const-string v1, "GDFP"

    aput-object v1, v0, v4

    const-string v1, "DART"

    aput-object v1, v0, v5

    const-string v1, "DART_DFA"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "DART_DFP"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/c;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->c:Ljava/util/Set;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "YT:FREEWHEEL"

    aput-object v1, v0, v3

    const-string v1, "FREEWHEEL"

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/c;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->d:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/h;Lcom/google/android/apps/youtube/core/player/fetcher/d;Lcom/google/android/apps/youtube/core/async/af;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->f:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->i:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->h:Lcom/google/android/apps/youtube/core/client/h;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->g:Lcom/google/android/apps/youtube/core/player/fetcher/d;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->e:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/h;Lcom/google/android/apps/youtube/core/player/fetcher/d;Lcom/google/android/apps/youtube/core/client/k;Lcom/google/android/apps/youtube/core/converter/n;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->f:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->i:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->h:Lcom/google/android/apps/youtube/core/client/h;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->g:Lcom/google/android/apps/youtube/core/player/fetcher/d;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/a;

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/core/client/h;->a()Lcom/google/android/apps/youtube/core/utils/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/a;-><init>(Lcom/google/android/apps/youtube/core/utils/a;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/el;

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/dj;

    invoke-direct {v2, p6}, Lcom/google/android/apps/youtube/core/converter/http/dj;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    invoke-direct {v1, p6, p1, v2}, Lcom/google/android/apps/youtube/core/converter/http/el;-><init>(Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/http/dj;)V

    invoke-virtual {p5, v0, v1}, Lcom/google/android/apps/youtube/core/client/k;->a(Lcom/google/android/apps/youtube/core/converter/http/a;Lcom/google/android/apps/youtube/core/converter/http/el;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->e:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->ADSENSE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdWrapperUri()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    sget-object v4, Lcom/google/android/apps/youtube/core/client/VastFetcher;->c:Ljava/util/Set;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v3, v1

    :goto_2
    if-eqz v3, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->DOUBLECLICK:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".doubleclick.net"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    sget-object v4, Lcom/google/android/apps/youtube/core/client/VastFetcher;->d:Ljava/util/Set;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->FREEWHEEL:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v3, ".fwmrm.net"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;JI)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->i:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/g;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapperCount()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v2, p5, v3}, Lcom/google/android/apps/youtube/core/player/event/g;-><init>(II)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPrefetchedAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPrefetchedAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->f:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v2

    cmp-long v4, v2, p3

    if-ltz v4, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->e:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v4, p1, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/client/AdsClient$VastException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/client/AdsClient$VastException;-><init>(Ljava/lang/Exception;)V

    throw v1

    :cond_3
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;JJI)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 10

    move-wide v3, p4

    move-object v2, p1

    move-object v0, p0

    :goto_0
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isVastWrapper()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdWrapperUri()Landroid/net/Uri;

    move-result-object v1

    move/from16 v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;JI)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    :cond_0
    :goto_1
    if-nez v1, :cond_4

    move-object v2, v1

    :cond_1
    return-object v2

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v1

    move/from16 v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;JI)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "dfaexp=1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPlaybackTracking()Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPlayerConfig()Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdOwnerName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo()Z

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v5

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-nez v1, :cond_5

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_6

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v1

    :goto_2
    invoke-virtual {v5, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    :cond_5
    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    move-wide v1, p2

    goto :goto_2
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 9

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    move-object v1, p1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0

    :cond_0
    move-object v1, p1

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v0

    :goto_1
    if-eqz v1, :cond_8

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->DOUBLECLICK:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    if-ne v4, v0, :cond_8

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v2

    :goto_2
    if-eqz v1, :cond_9

    sget-object v7, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->ADSENSE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    if-ne v7, v0, :cond_9

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_9

    move v0, v2

    :goto_3
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->DOUBLECLICK:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v7

    if-eq v1, v7, :cond_2

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->ADSENSE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v7

    if-eq v1, v7, :cond_2

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->FREEWHEEL:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v7

    if-ne v1, v7, :cond_a

    :cond_2
    move v1, v2

    :goto_4
    if-nez v1, :cond_3

    if-nez v4, :cond_3

    if-eqz v0, :cond_4

    :cond_3
    move v3, v2

    :cond_4
    invoke-virtual {v6, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->partnerId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "_2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isSkippable()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "_1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_5

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_8
    move v4, v3

    goto/16 :goto_2

    :cond_9
    move v0, v3

    goto/16 :goto_3

    :cond_a
    move v1, v3

    goto :goto_4

    :cond_b
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 8

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->g:Lcom/google/android/apps/youtube/core/player/fetcher/d;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/fetcher/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/Exception;)V

    throw v0
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    const-string v1, "Error retrieving streams for ad video"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/Exception;)V

    throw v1

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a:[B

    const-string v3, ""

    const-string v4, ""

    const/4 v5, -0x1

    const/4 v6, -0x1

    sget-object v7, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->PLAYER_SERVICE_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlaybackTracking()Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->SERVER_EXPERIMENT:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayerConfig(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getLengthSeconds()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getExpandedAnnotation()Lcom/google/a/a/a/a/ne;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/a/a/a/a/ne;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getInfoCardCollection()Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    return-object v0

    :catch_1
    move-exception v0

    const-string v1, "Error retrieving streams for ad video"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/Exception;)V

    throw v1
.end method

.method private static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/al;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/al;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/utils/al;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unable to find video id in watch uri from VastAd "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/youtube/core/utils/al;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unable to parse watch uri from VastAd "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    sget-object v1, Lcom/google/android/apps/youtube/core/client/VastFetcher;->b:Ljava/util/Set;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->h:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/h;->a()Lcom/google/android/apps/youtube/core/utils/a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/utils/a;->c(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;JLjava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 11

    const/4 v8, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->f:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    sget-wide v2, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a:J

    add-long v4, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->f:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    add-long v2, v0, p2

    const/4 v6, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    add-int/lit8 v7, v6, 0x1

    move-object v0, p0

    :try_start_0
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;JJI)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    if-nez v0, :cond_0

    move v6, v7

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isForecastingAd()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->DOUBLECLICK:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v6

    if-ne v1, v6, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback()Z
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-nez v0, :cond_6

    :cond_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v8

    :cond_2
    :goto_1
    return-object v0

    :cond_3
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v1

    sget-object v6, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->EMPTY:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    if-ne v1, v6, :cond_4

    move v6, v7

    goto :goto_0

    :cond_4
    invoke-direct {p0, v0, v9, p1}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->firstStreamUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isYouTubeHostedUri(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VastFetcher;->i:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v6, Lcom/google/android/apps/youtube/core/player/event/h;

    invoke-direct {v6}, Lcom/google/android/apps/youtube/core/player/event/h;-><init>()V

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->firstStreamUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    new-instance v1, Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException;

    const/4 v6, 0x0

    invoke-direct {v1, v0, v6}, Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/Exception;)V

    throw v1
    :try_end_1
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VastException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_0
    move-exception v0

    const-string v1, "Error resolving VAST Wrapper"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v6, v7

    goto :goto_0

    :cond_5
    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    :try_end_2
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VastException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/apps/youtube/core/client/VastFetcher$VideoInfoException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_1

    :cond_6
    move v6, v7

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Error retrieving ad video info"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v6, v7

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-direct {p0, v0, v9, p1}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_1
.end method
