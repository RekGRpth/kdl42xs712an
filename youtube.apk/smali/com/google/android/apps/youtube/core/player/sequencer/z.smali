.class final Lcom/google/android/apps/youtube/core/player/sequencer/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field final synthetic b:Lcom/google/android/apps/youtube/core/player/sequencer/y;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/y;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Lcom/google/android/apps/youtube/core/player/sequencer/y;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/z;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    goto :goto_0
.end method
