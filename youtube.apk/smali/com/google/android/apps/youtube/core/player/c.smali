.class public final Lcom/google/android/apps/youtube/core/player/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lcom/google/android/apps/youtube/common/e/b;

.field private static c:Lcom/google/android/apps/youtube/core/client/cf;

.field private static d:Ljava/util/Random;

.field private static e:Lcom/google/android/apps/youtube/common/network/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Lcom/google/android/apps/youtube/core/player/c;->a:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/n;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/c;->b:Lcom/google/android/apps/youtube/common/e/b;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/youtube/core/player/c;->c:Lcom/google/android/apps/youtube/core/client/cf;

    return-void
.end method

.method public static a()Lcom/google/android/apps/youtube/core/player/b;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/core/player/b;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/c;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/c;->b:Lcom/google/android/apps/youtube/common/e/b;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/c;->d:Ljava/util/Random;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/apps/youtube/core/player/c;->d:Ljava/util/Random;

    :goto_0
    sget-object v4, Lcom/google/android/apps/youtube/core/player/c;->c:Lcom/google/android/apps/youtube/core/client/cf;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/c;->e:Lcom/google/android/apps/youtube/common/network/h;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/b;-><init>(Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Ljava/util/Random;Lcom/google/android/apps/youtube/core/client/cf;Lcom/google/android/apps/youtube/common/network/h;)V

    return-object v0

    :cond_0
    new-instance v3, Ljava/util/Random;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/c;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Random;-><init>(J)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/youtube/core/player/c;->b:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/youtube/core/player/c;->e:Lcom/google/android/apps/youtube/common/network/h;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/core/client/cf;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/youtube/core/player/c;->c:Lcom/google/android/apps/youtube/core/client/cf;

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/youtube/core/player/c;->a:Ljava/lang/String;

    return-void
.end method
