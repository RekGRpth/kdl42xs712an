.class final Lcom/google/android/apps/youtube/core/transfer/e;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:Lcom/google/android/apps/youtube/core/transfer/d;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/google/android/apps/youtube/core/transfer/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/e;->a:Ljava/io/InputStream;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/transfer/e;->b:Lcom/google/android/apps/youtube/core/transfer/d;

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/e;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public final read()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/e;->b:Lcom/google/android/apps/youtube/core/transfer/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/e;->a:Ljava/io/InputStream;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/d;->a(Lcom/google/android/apps/youtube/core/transfer/d;Ljava/io/InputStream;)I

    move-result v0

    return v0
.end method

.method public final read([B)I
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/e;->b:Lcom/google/android/apps/youtube/core/transfer/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/e;->a:Ljava/io/InputStream;

    const/4 v2, 0x0

    array-length v3, p1

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/d;->a(Lcom/google/android/apps/youtube/core/transfer/d;Ljava/io/InputStream;[BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/e;->b:Lcom/google/android/apps/youtube/core/transfer/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/e;->a:Ljava/io/InputStream;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/apps/youtube/core/transfer/d;->a(Lcom/google/android/apps/youtube/core/transfer/d;Ljava/io/InputStream;[BII)I

    move-result v0

    return v0
.end method

.method public final skip(J)J
    .locals 2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "skip not supported"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
