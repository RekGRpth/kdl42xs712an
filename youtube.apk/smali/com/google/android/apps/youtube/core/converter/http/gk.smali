.class final Lcom/google/android/apps/youtube/core/converter/http/gk;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V
    .locals 4

    const-class v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    const-string v1, "yt:name"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "url"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "default"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->defaultThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "mqdefault"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->mqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_0

    :cond_2
    const-string v3, "hqdefault"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->hqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_0

    :cond_3
    const-string v3, "sddefault"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->sdThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_0
.end method
