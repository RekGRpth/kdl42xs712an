.class public final Lcom/google/android/apps/youtube/core/transfer/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/transfer/h;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/google/android/apps/youtube/core/client/bc;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/util/Map;

.field private final g:Ljava/util/Map;

.field private final h:Ljava/util/Map;

.field private i:Lcom/google/android/apps/youtube/core/transfer/z;

.field private j:Lcom/google/android/apps/youtube/core/utils/w;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->d:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->e:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->f:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->g:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->h:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->a:Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/w;->b:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->c:Lcom/google/android/apps/youtube/core/client/bc;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/w;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->h:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/w;Lcom/google/android/apps/youtube/core/transfer/x;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/transfer/w;->b(Lcom/google/android/apps/youtube/core/transfer/x;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/transfer/x;)Z
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->f:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/k;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v2, "metadata_updated"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->i:Lcom/google/android/apps/youtube/core/transfer/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/transfer/z;->c()V

    :cond_0
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/k;->a()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->e:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x40

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/k;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/transfer/w;->b(Lcom/google/android/apps/youtube/core/transfer/x;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/transfer/w;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->f:Ljava/util/Map;

    return-object v0
.end method

.method private b(Lcom/google/android/apps/youtube/core/transfer/x;)V
    .locals 10

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/w;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->f:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->c(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->c(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->b(Lcom/google/android/apps/youtube/core/transfer/x;)Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/k;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->d(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->e(Lcom/google/android/apps/youtube/core/transfer/x;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->f(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->g(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->h(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->i(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/x;->j(Lcom/google/android/apps/youtube/core/transfer/x;)Landroid/util/Pair;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/transfer/w;->b:Landroid/app/Activity;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/transfer/w;->b:Landroid/app/Activity;

    new-instance v9, Lcom/google/android/apps/youtube/core/transfer/y;

    invoke-direct {v9, p0, p1}, Lcom/google/android/apps/youtube/core/transfer/y;-><init>(Lcom/google/android/apps/youtube/core/transfer/w;Lcom/google/android/apps/youtube/core/transfer/x;)V

    invoke-static {v8, v9}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v8

    :goto_1
    invoke-interface/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :cond_2
    new-instance v8, Lcom/google/android/apps/youtube/core/transfer/y;

    invoke-direct {v8, p0, p1}, Lcom/google/android/apps/youtube/core/transfer/y;-><init>(Lcom/google/android/apps/youtube/core/transfer/w;Lcom/google/android/apps/youtube/core/transfer/x;)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/transfer/w;)Lcom/google/android/apps/youtube/core/transfer/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->i:Lcom/google/android/apps/youtube/core/transfer/z;

    return-object v0
.end method

.method private d()V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/x;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/w;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/apps/youtube/datalib/model/transfer/a;Z)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "filename of the video being uploaded was not provided."

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/apps/youtube/core/transfer/x;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/x;-><init>(B)V

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;

    if-nez p10, :cond_0

    sget-object p3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    :cond_0
    invoke-static {v1, p3}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-static {v1, p4}, Lcom/google/android/apps/youtube/core/transfer/x;->b(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v1, p5}, Lcom/google/android/apps/youtube/core/transfer/x;->c(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/x;->d(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v1, p7}, Lcom/google/android/apps/youtube/core/transfer/x;->e(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v1, p8}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;Landroid/util/Pair;)Landroid/util/Pair;

    invoke-static {v1, p1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;Landroid/net/Uri;)Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/x;->f(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->h:Ljava/util/Map;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/x;->g(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v1, p9}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/transfer/x;->b(Lcom/google/android/apps/youtube/core/transfer/x;)Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    move-result-object v0

    const-string v2, "metadata_updated"

    invoke-virtual {v0, v2, p10}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/w;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/transfer/w;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Z

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/transfer/z;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/w;->i:Lcom/google/android/apps/youtube/core/transfer/z;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/w;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/w;->m_()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->a:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->f:Ljava/util/Map;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/x;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/w;->g:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/w;->i:Lcom/google/android/apps/youtube/core/transfer/z;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v3, "metadata_updated"

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/transfer/z;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/w;->d()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/w;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/k;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v3, "metadata_updated"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/transfer/k;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/k;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/w;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/utils/w;->b(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->j:Lcom/google/android/apps/youtube/core/utils/w;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :goto_1
    return-void

    :cond_2
    iput-boolean v4, p0, Lcom/google/android/apps/youtube/core/transfer/w;->k:Z

    goto :goto_1
.end method

.method public final c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 4

    iget-wide v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->e:J

    iget-wide v2, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->i:Lcom/google/android/apps/youtube/core/transfer/z;

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->g:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->g:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final e(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/x;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/w;->b(Lcom/google/android/apps/youtube/core/transfer/x;)V

    :cond_0
    return-void
.end method

.method public final m_()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->k:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/w;->c()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/w;->d()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/w;->i:Lcom/google/android/apps/youtube/core/transfer/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/transfer/z;->b()V

    goto :goto_0
.end method
