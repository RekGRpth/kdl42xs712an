.class final Lcom/google/android/apps/youtube/core/offline/store/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/offline/store/aa;

.field private final b:Landroid/database/Cursor;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/offline/store/aa;Landroid/database/Cursor;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->a:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    const-string v0, "id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->c:I

    const-string v0, "title"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->d:I

    const-string v0, "summary"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->e:I

    const-string v0, "author"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->f:I

    const-string v0, "updated_date"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->g:I

    const-string v0, "content_uri"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->h:I

    const-string v0, "size"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->i:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/offline/store/aa;Landroid/database/Cursor;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/ab;-><init>(Lcom/google/android/apps/youtube/core/offline/store/aa;Landroid/database/Cursor;)V

    return-void
.end method

.method private a()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->a:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Lcom/google/android/apps/youtube/core/offline/store/aa;)Lcom/google/android/apps/youtube/core/offline/store/l;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/l;->g(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->id(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->d:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->title(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->e:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->summary(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->f:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->author(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->g:I

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/utils/i;->a(Landroid/database/Cursor;I)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->updated(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->h:I

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/utils/i;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->contentUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->i:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->size(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->thumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->hqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->sdThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/offline/store/ab;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/offline/store/ab;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/offline/store/ab;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ab;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/offline/store/ab;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
