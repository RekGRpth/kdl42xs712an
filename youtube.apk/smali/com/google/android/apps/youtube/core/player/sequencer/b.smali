.class final Lcom/google/android/apps/youtube/core/player/sequencer/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/sequencer/a;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/b;->a:Lcom/google/android/apps/youtube/core/player/sequencer/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/a;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/b;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/a;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/b;->a:Lcom/google/android/apps/youtube/core/player/sequencer/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/b;->a:Lcom/google/android/apps/youtube/core/player/sequencer/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/b;->a:Lcom/google/android/apps/youtube/core/player/sequencer/a;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/player/sequencer/a;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v4, p2}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, p2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/b;->a:Lcom/google/android/apps/youtube/core/player/sequencer/a;

    iput-object p2, v0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/b;->a:Lcom/google/android/apps/youtube/core/player/sequencer/a;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    return-void
.end method
