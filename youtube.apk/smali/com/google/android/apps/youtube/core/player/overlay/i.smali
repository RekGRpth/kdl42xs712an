.class public final Lcom/google/android/apps/youtube/core/player/overlay/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/u;


# instance fields
.field private A:I

.field private B:Lcom/google/a/a/a/a/be;

.field private C:Ljava/util/List;

.field private D:I

.field private E:Lcom/google/a/a/a/a/hx;

.field private F:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

.field private G:Ljava/util/List;

.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field private final h:Landroid/content/Context;

.field private final i:Lcom/google/android/apps/youtube/core/player/overlay/g;

.field private final j:Lcom/google/android/apps/youtube/core/client/bj;

.field private final k:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final l:Lcom/google/android/apps/youtube/core/player/am;

.field private final m:Landroid/os/Handler;

.field private final n:Lcom/google/android/apps/youtube/core/player/ae;

.field private final o:Lcom/google/android/apps/youtube/datalib/innertube/t;

.field private final p:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

.field private final q:Lcom/google/android/apps/youtube/core/player/s;

.field private r:Z

.field private s:Lcom/google/android/apps/youtube/common/a/d;

.field private t:Lcom/google/android/apps/youtube/common/a/d;

.field private u:Lcom/google/android/apps/youtube/common/a/d;

.field private v:Lcom/google/android/apps/youtube/common/a/d;

.field private w:Lcom/google/a/a/a/a/ne;

.field private x:Z

.field private y:[Z

.field private z:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/overlay/g;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/datalib/innertube/t;Landroid/support/v4/app/l;)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->h:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/g;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->j:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->k:Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->l:Lcom/google/android/apps/youtube/core/player/am;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->m:Landroid/os/Handler;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ae;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->n:Lcom/google/android/apps/youtube/core/player/ae;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->o:Lcom/google/android/apps/youtube/datalib/innertube/t;

    invoke-virtual {p6}, Lcom/google/android/apps/youtube/core/player/ae;->a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->p:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    if-eqz p8, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->m:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->p:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-object v2, p4

    move-object v3, p3

    move-object v5, p7

    move-object v6, p0

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/s;-><init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/datalib/innertube/t;Lcom/google/android/apps/youtube/core/player/u;Landroid/support/v4/app/l;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->q:Lcom/google/android/apps/youtube/core/player/s;

    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->A:I

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/o;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/i;B)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setListener(Lcom/google/android/apps/youtube/core/player/overlay/h;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->q:Lcom/google/android/apps/youtube/core/player/s;

    goto :goto_0
.end method

.method private a(Lcom/google/a/a/a/a/sx;Lcom/google/android/apps/youtube/core/player/overlay/m;)Lcom/google/android/apps/youtube/common/a/d;
    .locals 4

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/sx;->b:[Lcom/google/a/a/a/a/sy;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return-object v1

    :cond_1
    iget-object v0, p1, Lcom/google/a/a/a/a/sx;->b:[Lcom/google/a/a/a/a/sy;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/a/a/a/a/sy;->b:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->j:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->m:Landroid/os/Handler;

    invoke-static {v3, v1}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/core/player/overlay/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    return-object v0
.end method

.method private a(Lcom/google/a/a/a/a/ne;Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->b()V

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->r:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->g:Z

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setAdStyle(Z)V

    iget-object v2, p1, Lcom/google/a/a/a/a/ne;->b:Lcom/google/a/a/a/a/fc;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/a/a/a/a/ne;->b:Lcom/google/a/a/a/a/fc;

    iget-object v2, v2, Lcom/google/a/a/a/a/fc;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    iget-object v3, p1, Lcom/google/a/a/a/a/ne;->b:Lcom/google/a/a/a/a/fc;

    iget-object v3, v3, Lcom/google/a/a/a/a/fc;->f:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setFeaturedVideoTitle(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p1, Lcom/google/a/a/a/a/ne;->d:[Lcom/google/a/a/a/a/be;

    array-length v2, v2

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/a/a/a/a/ne;->d:[Lcom/google/a/a/a/a/be;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->C:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->C:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v3, v2, [Z

    iput-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->y:[Z

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->z:[Z

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->q:Lcom/google/android/apps/youtube/core/player/s;

    if-eqz v2, :cond_5

    :goto_0
    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->F:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    iget-object v0, v0, Lcom/google/a/a/a/a/ne;->c:Lcom/google/a/a/a/a/ez;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/google/a/a/a/a/ez;->g:Lcom/google/a/a/a/a/sx;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/k;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/k;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/i;B)V

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/a/a/a/a/sx;Lcom/google/android/apps/youtube/core/player/overlay/m;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->s:Lcom/google/android/apps/youtube/common/a/d;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    iget-object v0, v0, Lcom/google/a/a/a/a/ne;->b:Lcom/google/a/a/a/a/fc;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/google/a/a/a/a/fc;->e:Lcom/google/a/a/a/a/sx;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/l;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/l;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/i;B)V

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/a/a/a/a/sx;Lcom/google/android/apps/youtube/core/player/overlay/m;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->t:Lcom/google/android/apps/youtube/common/a/d;

    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/i;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->p:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->p:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c(I)V

    :cond_0
    iget-object v0, v0, Lcom/google/a/a/a/a/rm;->h:[B

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a([B)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->e:Z

    if-eq v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->d:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->e:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/g;->a(Z)V

    :cond_1
    return-void
.end method

.method private a([B)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->o:Lcom/google/android/apps/youtube/datalib/innertube/t;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->o:Lcom/google/android/apps/youtube/datalib/innertube/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/u;->b([B)Lcom/google/android/apps/youtube/datalib/innertube/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->o:Lcom/google/android/apps/youtube/datalib/innertube/t;

    const-class v2, Lcom/google/a/a/a/a/il;

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/a/m;->a(Ljava/lang/Class;)Lcom/google/android/apps/youtube/datalib/a/l;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/t;->a(Lcom/google/android/apps/youtube/datalib/innertube/u;Lcom/google/android/apps/youtube/datalib/a/l;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/i;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->x:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/ne;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    return-object v0
.end method

.method private b()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->s:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->s:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->s:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->t:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->t:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->t:Lcom/google/android/apps/youtube/common/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->u:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->u:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->u:Lcom/google/android/apps/youtube/common/a/d;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->v:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->v:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->v:Lcom/google/android/apps/youtube/common/a/d;

    :cond_3
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->r:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->a()V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->b:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->c:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->d:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->e:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->x:Z

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->y:[Z

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->z:[Z

    iput v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->A:I

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->E:Lcom/google/a/a/a/a/hx;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->F:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    iput v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->D:I

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/i;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/datalib/d/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->k:Lcom/google/android/apps/youtube/datalib/d/a;

    return-object v0
.end method

.method private c()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->c:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->g()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e()V

    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->d:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->h()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e()V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/overlay/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->c()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/be;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    return-object v0
.end method

.method private e()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->l:Lcom/google/android/apps/youtube/core/player/am;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/player/am;->a(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->a:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/player/overlay/i;)[Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->z:[Z

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/player/overlay/i;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->A:I

    return v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/core/player/overlay/i;)[Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->y:[Z

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/core/player/overlay/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->d()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/core/player/overlay/i;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->D:I

    return v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/core/player/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->n:Lcom/google/android/apps/youtube/core/player/ae;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->F:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/core/player/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->q:Lcom/google/android/apps/youtube/core/player/s;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->n:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->i()V

    return-void
.end method

.method public final handleInfoCardTeaserClickEvent(Lcom/google/android/apps/youtube/core/player/event/b;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->n:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->q:Lcom/google/android/apps/youtube/core/player/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->F:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/s;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;I)V

    return-void
.end method

.method public final handleVideoControlsVisibilityEvent(Lcom/google/android/apps/youtube/core/player/event/aa;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/event/aa;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setVisible(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final handleVideoFullscreenEvent(Lcom/google/android/apps/youtube/core/player/event/ab;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ab;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->a:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->f()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->h()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->f()V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->e:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/g;->a(Z)V

    goto :goto_0
.end method

.method public final handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->onAd()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->g:Z

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getExpandedAnnotation()Lcom/google/a/a/a/a/ne;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getInfoCardCollection()Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/a/a/a/a/ne;Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->r:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getExpandedAnnotation()Lcom/google/a/a/a/a/ne;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getInfoCardCollection()Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/a/a/a/a/ne;Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldShowCtaAnnotations()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adAnnotations:Lcom/google/a/a/a/a/ne;

    :goto_1
    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/a/a/a/a/ne;Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->b()V

    goto :goto_0
.end method

.method public final handleVideoTimeEvent(Lcom/google/android/apps/youtube/core/player/event/ae;)V
    .locals 12
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->a()I

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    iget-object v0, v0, Lcom/google/a/a/a/a/ne;->c:Lcom/google/a/a/a/a/ez;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->b:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    iget-object v0, v0, Lcom/google/a/a/a/a/ne;->b:Lcom/google/a/a/a/a/fc;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->x:Z

    if-nez v1, :cond_3

    iget-wide v1, v0, Lcom/google/a/a/a/a/fc;->b:J

    int-to-long v5, v4

    cmp-long v1, v1, v5

    if-gtz v1, :cond_3

    int-to-long v1, v4

    iget-wide v5, v0, Lcom/google/a/a/a/a/fc;->c:J

    cmp-long v0, v1, v5

    if-gez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->c:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->f()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->w:Lcom/google/a/a/a/a/ne;

    iget-object v0, v0, Lcom/google/a/a/a/a/ne;->d:[Lcom/google/a/a/a/a/be;

    array-length v0, v0

    if-eqz v0, :cond_9

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->C:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/be;

    iget-wide v5, v0, Lcom/google/a/a/a/a/be;->b:J

    int-to-long v7, v4

    cmp-long v5, v5, v7

    if-gtz v5, :cond_15

    iget-wide v5, v0, Lcom/google/a/a/a/a/be;->c:J

    int-to-long v7, v4

    cmp-long v5, v5, v7

    if-lez v5, :cond_15

    if-eqz v3, :cond_2

    iget-wide v5, v0, Lcom/google/a/a/a/a/be;->b:J

    iget-wide v7, v3, Lcom/google/a/a/a/a/be;->b:J

    cmp-long v5, v5, v7

    if-lez v5, :cond_15

    :cond_2
    move-object v1, v0

    move v0, v2

    :goto_2
    add-int/lit8 v2, v2, 0x1

    move-object v3, v1

    move v1, v0

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->c()V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->A:I

    if-eq v1, v0, :cond_5

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->A:I

    iput-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    iget-object v1, v1, Lcom/google/a/a/a/a/be;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    iget-object v2, v2, Lcom/google/a/a/a/a/be;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setCallToActionText(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setCallToActionImage(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/google/a/a/a/a/be;->j:Lcom/google/a/a/a/a/sx;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/j;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/j;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/i;B)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/a/a/a/a/sx;Lcom/google/android/apps/youtube/core/player/overlay/m;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->u:Lcom/google/android/apps/youtube/common/a/d;

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->d()V

    :cond_6
    :goto_3
    return-void

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->y:[Z

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->A:I

    aget-boolean v1, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->z:[Z

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->A:I

    aget-boolean v0, v0, v2

    if-nez v0, :cond_8

    int-to-long v2, v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->B:Lcom/google/a/a/a/a/be;

    iget-wide v5, v0, Lcom/google/a/a/a/a/be;->d:J

    cmp-long v0, v2, v5

    if-gez v0, :cond_b

    :cond_8
    const/4 v0, 0x1

    :goto_4
    if-eqz v1, :cond_c

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->d()V

    :cond_9
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/hx;

    iget-wide v6, v0, Lcom/google/a/a/a/a/hx;->b:J

    iget-wide v8, v0, Lcom/google/a/a/a/a/hx;->d:J

    add-long/2addr v6, v8

    iget-wide v8, v0, Lcom/google/a/a/a/a/hx;->b:J

    int-to-long v10, v4

    cmp-long v8, v8, v10

    if-gtz v8, :cond_14

    int-to-long v8, v4

    cmp-long v6, v6, v8

    if-lez v6, :cond_14

    if-eqz v1, :cond_a

    iget-wide v6, v0, Lcom/google/a/a/a/a/hx;->b:J

    iget-wide v8, v1, Lcom/google/a/a/a/a/hx;->b:J

    cmp-long v6, v6, v8

    if-gez v6, :cond_14

    :cond_a
    move v1, v3

    :goto_8
    move v2, v1

    move-object v1, v0

    goto :goto_7

    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    :cond_c
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Z)V

    goto :goto_5

    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_e
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->D:I

    if-ne v2, v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->E:Lcom/google/a/a/a/a/hx;

    if-eq v1, v0, :cond_12

    :cond_f
    iput v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->D:I

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->E:Lcom/google/a/a/a/a/hx;

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->D:I

    if-ltz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->D:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a()Lcom/google/a/a/a/a/rn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setInfoCardTeaserImage(Landroid/graphics/Bitmap;)V

    if-eqz v0, :cond_10

    iget-object v1, v0, Lcom/google/a/a/a/a/rn;->b:Lcom/google/a/a/a/a/sx;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/n;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/core/player/overlay/n;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/i;B)V

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/a/a/a/a/sx;Lcom/google/android/apps/youtube/core/player/overlay/m;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->v:Lcom/google/android/apps/youtube/common/a/d;

    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    iget-object v0, v0, Lcom/google/a/a/a/a/rn;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setInfoCardTeaserMessage(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->D:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->G:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a()Lcom/google/a/a/a/a/rn;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->p:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->p:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b(I)V

    :cond_11
    iget-object v0, v0, Lcom/google/a/a/a/a/rn;->e:[B

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a([B)V

    :cond_12
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->D:I

    if-gez v0, :cond_13

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->f:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->j()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e()V

    goto/16 :goto_3

    :cond_13
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->f:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->f:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/i;->i:Lcom/google/android/apps/youtube/core/player/overlay/g;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/g;->i()V

    goto/16 :goto_3

    :cond_14
    move-object v0, v1

    move v1, v2

    goto/16 :goto_8

    :cond_15
    move v0, v1

    move-object v1, v3

    goto/16 :goto_2
.end method
