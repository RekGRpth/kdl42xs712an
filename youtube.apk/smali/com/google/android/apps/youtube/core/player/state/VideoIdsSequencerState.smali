.class public Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final clickTrackingParams:[B

.field public final currentPlayerResponse:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field public final index:I

.field public final loop:Z

.field public final pendingIndex:I

.field public final playerParams:Ljava/lang/String;

.field public final videoIds:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/state/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->videoIds:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->videoIds:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->clickTrackingParams:[B

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->clickTrackingParams:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->playerParams:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->index:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->pendingIndex:I

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->currentPlayerResponse:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->loop:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Ljava/lang/String;[BLjava/lang/String;IILcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->videoIds:[Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->clickTrackingParams:[B

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->playerParams:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->index:I

    iput p5, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->pendingIndex:I

    iput-object p6, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->currentPlayerResponse:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-boolean p7, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->loop:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->videoIds:[Ljava/lang/String;

    array-length v1, v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->videoIds:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->clickTrackingParams:[B

    array-length v1, v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->clickTrackingParams:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->playerParams:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->index:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->pendingIndex:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->currentPlayerResponse:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->loop:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
