.class final Lcom/google/android/apps/youtube/core/player/sequencer/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/ah;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget v1, v1, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->q:I

    iput v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->p:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v4, p2}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, p2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iput-object p2, v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget v1, v1, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->q:I

    iput v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->p:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->b(Lcom/google/android/apps/youtube/core/player/sequencer/ae;)Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/ai;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/ai;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/ah;Landroid/os/Handler;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
