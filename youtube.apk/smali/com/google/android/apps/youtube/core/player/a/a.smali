.class public final Lcom/google/android/apps/youtube/core/player/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/fromguava/e;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/fromguava/e;

.field private final b:Lcom/google/android/apps/youtube/common/fromguava/e;

.field private final c:Ljava/security/Key;

.field private d:Lcom/google/android/exoplayer/upstream/cache/a;

.field private e:Lcom/google/android/exoplayer/upstream/cache/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/fromguava/e;Lcom/google/android/apps/youtube/common/fromguava/e;Ljava/security/Key;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/a/a;->a:Lcom/google/android/apps/youtube/common/fromguava/e;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/a/a;->b:Lcom/google/android/apps/youtube/common/fromguava/e;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/a/a;->c:Ljava/security/Key;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/exoplayer/upstream/i;
    .locals 14

    const/4 v11, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/a;->a:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/upstream/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/a;->b:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/upstream/cache/a;

    if-eqz v1, :cond_0

    new-instance v4, Lcom/google/android/exoplayer/upstream/a/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/a;->c:Ljava/security/Key;

    invoke-interface {v0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    const/16 v3, 0x1000

    new-array v3, v3, [B

    new-instance v7, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;

    const-wide/32 v8, 0x500000

    invoke-direct {v7, v1, v8, v9}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;-><init>(Lcom/google/android/exoplayer/upstream/cache/a;J)V

    invoke-direct {v4, v0, v3, v7}, Lcom/google/android/exoplayer/upstream/a/a;-><init>([B[BLcom/google/android/exoplayer/upstream/h;)V

    new-instance v3, Lcom/google/android/exoplayer/upstream/a/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/a;->c:Ljava/security/Key;

    invoke-interface {v0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    new-instance v7, Lcom/google/android/exoplayer/upstream/FileDataSource;

    invoke-direct {v7}, Lcom/google/android/exoplayer/upstream/FileDataSource;-><init>()V

    invoke-direct {v3, v0, v7}, Lcom/google/android/exoplayer/upstream/a/b;-><init>([BLcom/google/android/exoplayer/upstream/i;)V

    new-instance v0, Lcom/google/android/exoplayer/upstream/cache/b;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/cache/b;-><init>(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/h;ZZ)V

    move-object v2, v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/a;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/exoplayer/upstream/cache/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/a;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    new-instance v3, Lcom/google/android/exoplayer/upstream/a/b;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/a/a;->c:Ljava/security/Key;

    invoke-interface {v4}, Ljava/security/Key;->getEncoded()[B

    move-result-object v4

    new-instance v7, Lcom/google/android/exoplayer/upstream/FileDataSource;

    invoke-direct {v7}, Lcom/google/android/exoplayer/upstream/FileDataSource;-><init>()V

    invoke-direct {v3, v4, v7}, Lcom/google/android/exoplayer/upstream/a/b;-><init>([BLcom/google/android/exoplayer/upstream/i;)V

    move-object v4, v11

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/cache/b;-><init>(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/h;ZZ)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/a;->e:Lcom/google/android/exoplayer/upstream/cache/a;

    if-eqz v1, :cond_1

    new-instance v7, Lcom/google/android/exoplayer/upstream/cache/b;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/a/a;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    new-instance v10, Lcom/google/android/exoplayer/upstream/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/a;->c:Ljava/security/Key;

    invoke-interface {v1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v1

    new-instance v2, Lcom/google/android/exoplayer/upstream/FileDataSource;

    invoke-direct {v2}, Lcom/google/android/exoplayer/upstream/FileDataSource;-><init>()V

    invoke-direct {v10, v1, v2}, Lcom/google/android/exoplayer/upstream/a/b;-><init>([BLcom/google/android/exoplayer/upstream/i;)V

    move-object v9, v0

    move v12, v5

    move v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/exoplayer/upstream/cache/b;-><init>(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/h;ZZ)V

    :goto_0
    return-object v7

    :cond_1
    move-object v7, v0

    goto :goto_0

    :cond_2
    move-object v7, v2

    goto :goto_0
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/a/a;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/a/a;->e:Lcom/google/android/exoplayer/upstream/cache/a;

    return-void
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/a/a;->a()Lcom/google/android/exoplayer/upstream/i;

    move-result-object v0

    return-object v0
.end method
