.class public final Lcom/google/android/apps/youtube/core/player/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/apps/youtube/core/player/ap;

.field private final d:Lcom/google/android/apps/youtube/core/client/ce;

.field private final e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/util/List;

.field private m:Ljava/lang/String;

.field private n:Lcom/google/android/apps/youtube/common/a/d;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/player/ap;Lcom/google/android/apps/youtube/core/client/ce;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->a:Landroid/os/Handler;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->b:Landroid/content/SharedPreferences;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/ce;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->d:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ap;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->c:Lcom/google/android/apps/youtube/core/player/ap;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/ao;->e:Ljava/lang/String;

    return-void
.end method

.method private c()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->l:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->h:Z

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->n:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->d:Lcom/google/android/apps/youtube/core/client/ce;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ao;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ao;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ao;->n:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/ce;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method private d()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->i:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->c:Lcom/google/android/apps/youtube/core/player/ap;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/ap;->a()V

    :cond_0
    return-void
.end method

.method private e()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->i:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->c:Lcom/google/android/apps/youtube/core/player/ap;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/ap;->b()V

    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ao;->l:Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ao;->e:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->createDisableSubtitleOption(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ao;->c:Lcom/google/android/apps/youtube/core/player/ap;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/player/ap;->a(Ljava/util/List;)V

    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private g()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->h:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->e()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->c:Lcom/google/android/apps/youtube/core/player/ap;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/ap;->c()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->g:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->c()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 5

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getCaptionTracksUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ao;->b()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ao;->m:Ljava/lang/String;

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/ao;->j:Z

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ao;->b:Landroid/content/SharedPreferences;

    const-string v4, "subtitles_language_code"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->f:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->c()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_language_code"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_language_code"

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "subtitles_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "error retrieving subtitle tracks"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->g()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    const/4 v1, 0x0

    const/4 v6, 0x0

    check-cast p2, Ljava/util/List;

    iput-boolean v6, p0, Lcom/google/android/apps/youtube/core/player/ao;->h:Z

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SubtitleTrack response was empty"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->j:Z

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->d()V

    :cond_3
    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/ao;->l:Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->f:Z

    if-eqz v0, :cond_7

    iput-boolean v6, p0, Lcom/google/android/apps/youtube/core/player/ao;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ao;->k:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v2, v0

    goto :goto_1

    :cond_4
    if-nez v1, :cond_8

    const-string v4, "en"

    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_5
    if-nez v2, :cond_6

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->j:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->l:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-object v2, v0

    :cond_6
    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->c:Lcom/google/android/apps/youtube/core/player/ap;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/player/ap;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->g:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/google/android/apps/youtube/core/player/ao;->g:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->f()V

    goto :goto_0

    :cond_8
    move-object v0, v1

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ao;->m:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ao;->l:Ljava/util/List;

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->g:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->h:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ao;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->n:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ao;->n:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ao;->n:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    return-void
.end method
