.class public abstract Lcom/google/android/apps/youtube/core/transfer/TransferService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/transfer/r;
.implements Lcom/google/android/apps/youtube/core/transfer/v;


# instance fields
.field protected a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

.field private b:Lcom/google/android/apps/youtube/core/transfer/i;

.field private c:Ljava/util/Map;

.field private d:Z

.field private e:Lcom/google/android/apps/youtube/core/transfer/k;

.field private f:Ljava/util/Set;

.field private g:I

.field private h:Lcom/google/android/apps/youtube/core/transfer/d;

.field private i:Landroid/content/SharedPreferences;

.field private j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/TransferService;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->g:I

    return p1
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method protected static a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/g;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/youtube/core/transfer/g;-><init>(Ljava/lang/Class;Lcom/google/android/apps/youtube/core/transfer/h;)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/utils/w;->a(Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/TransferService;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->c:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/TransferService;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->f:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/TransferService;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->d:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/transfer/TransferService;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/transfer/TransferService;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->g:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/transfer/TransferService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->m()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/transfer/TransferService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->n()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/transfer/TransferService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->o()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/transfer/TransferService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->p()V

    return-void
.end method

.method private m()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v0, Lcom/google/android/youtube/p;->hm:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b(Z)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->g:I

    return-void
.end method

.method private n()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Z)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->g:I

    return-void
.end method

.method private o()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->h()I

    move-result v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->g:I

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->h()I

    move-result v0

    goto :goto_0
.end method

.method private p()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    const v2, 0x7fffffff

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->h:Lcom/google/android/apps/youtube/core/transfer/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/d;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a()I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Lcom/google/android/apps/youtube/core/transfer/i;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/i;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/youtube/core/transfer/i;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/i;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/transfer/h;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->f:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->d:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/youtube/core/transfer/h;->m_()V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b()I
    .locals 1

    const/16 v0, 0x14

    return v0
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/youtube/core/transfer/i;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/i;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final b(Lcom/google/android/apps/youtube/core/transfer/h;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/youtube/core/transfer/i;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/i;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected abstract d()Ljava/lang/String;
.end method

.method public final d(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/youtube/core/transfer/i;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/i;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected abstract e()Ljava/lang/String;
.end method

.method public final e(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/youtube/core/transfer/i;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/i;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method public final f(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/youtube/core/transfer/i;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/i;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected abstract g()Ljava/lang/String;
.end method

.method protected h()I
    .locals 1

    const v0, 0x7fffffff

    return v0
.end method

.method protected abstract i()Ljava/lang/String;
.end method

.method protected final j()Ljava/util/Map;
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->c:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final l()Lcom/google/android/apps/youtube/core/transfer/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->h:Lcom/google/android/apps/youtube/core/transfer/d;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->e:Lcom/google/android/apps/youtube/core/transfer/k;

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "creating transfer service"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/i;-><init>(Lcom/google/android/apps/youtube/core/transfer/TransferService;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b:Lcom/google/android/apps/youtube/core/transfer/i;

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->c()Z

    move-result v6

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/transfer/r;Lcom/google/android/apps/youtube/core/transfer/v;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->f:Ljava/util/Set;

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/k;-><init>(Lcom/google/android/apps/youtube/core/transfer/TransferService;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->e:Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/transfer/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->h:Lcom/google/android/apps/youtube/core/transfer/d;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->B()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/transfer/j;-><init>(Lcom/google/android/apps/youtube/core/transfer/TransferService;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->m()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->n()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->o()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->p()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->g:I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->i:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->d()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
