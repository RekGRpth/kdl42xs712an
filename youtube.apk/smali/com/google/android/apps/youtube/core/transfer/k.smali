.class public final Lcom/google/android/apps/youtube/core/transfer/k;
.super Landroid/os/Binder;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/transfer/TransferService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/transfer/TransferService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->j()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Lcom/google/android/apps/youtube/core/transfer/TransferService;I)I

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Lcom/google/android/apps/youtube/core/transfer/TransferService;I)I

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Lcom/google/android/apps/youtube/core/transfer/TransferService;I)I

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Lcom/google/android/apps/youtube/core/transfer/TransferService;I)I

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/transfer/h;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Lcom/google/android/apps/youtube/core/transfer/h;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 3

    const-string v0, "Transfer binder: pause all transfers"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Lcom/google/android/apps/youtube/core/transfer/TransferService;I)I

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Transfer binder: restore transfers for account ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Lcom/google/android/apps/youtube/core/transfer/TransferService;I)I

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/core/transfer/h;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->b(Lcom/google/android/apps/youtube/core/transfer/h;)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/k;->a:Lcom/google/android/apps/youtube/core/transfer/TransferService;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
