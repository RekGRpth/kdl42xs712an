.class public final enum Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

.field public static final enum DEPRESSED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

.field public static final enum DROP_SHADOW:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

.field public static final enum NONE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

.field public static final enum RAISED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

.field public static final enum UNIFORM:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

.field private static edgeTypeEntryStrings:[Ljava/lang/String;

.field private static edgeTypeValueStrings:[Ljava/lang/String;


# instance fields
.field private edgeTypeStringId:I

.field private edgeTypeValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    const-string v1, "NONE"

    sget v2, Lcom/google/android/youtube/p;->eo:I

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->NONE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    const-string v1, "UNIFORM"

    sget v2, Lcom/google/android/youtube/p;->eD:I

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->UNIFORM:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    const-string v1, "DROP_SHADOW"

    sget v2, Lcom/google/android/youtube/p;->ee:I

    invoke-direct {v0, v1, v5, v2, v5}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->DROP_SHADOW:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    const-string v1, "RAISED"

    sget v2, Lcom/google/android/youtube/p;->er:I

    invoke-direct {v0, v1, v6, v2, v6}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->RAISED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    const-string v1, "DEPRESSED"

    sget v2, Lcom/google/android/youtube/p;->ed:I

    invoke-direct {v0, v1, v7, v2, v7}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->DEPRESSED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->NONE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->UNIFORM:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->DROP_SHADOW:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->RAISED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->DEPRESSED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeStringId:I

    iput p4, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeValue:I

    return-void
.end method

.method public static getDefaultEdgeTypeValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeValue:I

    return v0
.end method

.method public static getEdgeTypeEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeEntryStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeEntryStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeEntryStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeStringId:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeEntryStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getEdgeTypeValueStrings()[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeValueStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeValueStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeValueStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeValue:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->edgeTypeValueStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;

    return-object v0
.end method
