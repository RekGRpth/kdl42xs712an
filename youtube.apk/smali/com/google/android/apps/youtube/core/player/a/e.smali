.class public final Lcom/google/android/apps/youtube/core/player/a/e;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/RandomAccessFile;

.field private b:J

.field private c:J

.field private final d:[B

.field private final e:Ljava/security/Key;

.field private final f:Ljavax/crypto/spec/IvParameterSpec;


# direct methods
.method public constructor <init>(Ljava/io/File;JJLjava/security/Key;)V
    .locals 6

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    cmp-long v0, p2, p4

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "begin must be less than or equal to end"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sub-long v2, p4, p2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    iput-wide p2, p0, Lcom/google/android/apps/youtube/core/player/a/e;->b:J

    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v0, p1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->a:Ljava/io/RandomAccessFile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p2, p3}, Ljava/io/RandomAccessFile;->seek(J)V

    iput-object p6, p0, Lcom/google/android/apps/youtube/core/player/a/e;->e:Ljava/security/Key;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/h;->a(Ljava/lang/String;)Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->f:Ljavax/crypto/spec/IvParameterSpec;

    new-array v0, v1, [B

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->d:[B

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 5

    const-string v0, "Stream closed with %d bytes left unread"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    return-void
.end method

.method public final read()I
    .locals 10

    const-wide/16 v8, 0x1

    const/4 v0, -0x1

    const/4 v7, 0x0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/e;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->read()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->d:[B

    int-to-byte v1, v1

    aput-byte v1, v0, v7

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->d:[B

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/a/e;->e:Ljava/security/Key;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/a/e;->f:Ljavax/crypto/spec/IvParameterSpec;

    iget-wide v5, p0, Lcom/google/android/apps/youtube/core/player/a/e;->b:J

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/utils/h;->b([BIILjava/security/Key;Ljavax/crypto/spec/IvParameterSpec;J)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->b:J

    add-long/2addr v0, v8

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->b:J

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    sub-long/2addr v0, v8

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->d:[B

    aget-byte v0, v0, v7

    and-int/lit16 v0, v0, 0xff

    :cond_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final read([BII)I
    .locals 7

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->a:Ljava/io/RandomAccessFile;

    int-to-long v1, p3

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v0, p1, p2, v1}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v2

    if-lez v2, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/a/e;->e:Ljava/security/Key;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/a/e;->f:Ljavax/crypto/spec/IvParameterSpec;

    iget-wide v5, p0, Lcom/google/android/apps/youtube/core/player/a/e;->b:J

    move-object v0, p1

    move v1, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/utils/h;->b([BIILjava/security/Key;Ljavax/crypto/spec/IvParameterSpec;J)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    int-to-long v3, v2

    sub-long/2addr v0, v3

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->c:J

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->b:J

    int-to-long v3, v2

    add-long/2addr v0, v3

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/e;->b:J

    :cond_0
    :goto_0
    return v2

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const/4 v2, -0x1

    goto :goto_0
.end method
