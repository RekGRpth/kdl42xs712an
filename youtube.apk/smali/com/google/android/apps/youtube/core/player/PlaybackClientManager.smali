.class public final Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Z

.field private final a:Ljava/util/Random;

.field private final b:Lcom/google/android/apps/youtube/common/network/h;

.field private final c:Lcom/google/android/apps/youtube/core/client/d;

.field private d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

.field private e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private final f:Lcom/google/android/apps/youtube/core/client/bf;

.field private g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

.field private final h:Lcom/google/android/apps/youtube/core/client/bs;

.field private i:Lcom/google/android/apps/youtube/core/client/br;

.field private final j:Lcom/google/android/apps/youtube/core/client/bv;

.field private k:Lcom/google/android/apps/youtube/core/client/bu;

.field private final l:Lcom/google/android/apps/youtube/core/client/bx;

.field private m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

.field private final n:Lcom/google/android/apps/youtube/core/client/cc;

.field private o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

.field private final p:Lcom/google/android/apps/youtube/core/client/ch;

.field private q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

.field private w:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

.field private x:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Ljava/util/Random;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/d;Lcom/google/android/apps/youtube/core/client/bf;Lcom/google/android/apps/youtube/core/client/bs;Lcom/google/android/apps/youtube/core/client/bv;Lcom/google/android/apps/youtube/core/client/bx;Lcom/google/android/apps/youtube/core/client/cc;Lcom/google/android/apps/youtube/core/client/ch;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a:Ljava/util/Random;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c:Lcom/google/android/apps/youtube/core/client/d;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bf;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->f:Lcom/google/android/apps/youtube/core/client/bf;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bs;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->h:Lcom/google/android/apps/youtube/core/client/bs;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bv;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->j:Lcom/google/android/apps/youtube/core/client/bv;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bx;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->l:Lcom/google/android/apps/youtube/core/client/bx;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/cc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->n:Lcom/google/android/apps/youtube/core/client/cc;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/ch;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->p:Lcom/google/android/apps/youtube/core/client/ch;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->t:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "PlaybackClientManager "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    const-string v1, "RESTORED"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": videoId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->t:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->u:Ljava/lang/String;

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "NEW"

    goto :goto_1
.end method

.method private final n()Ljava/lang/String;
    .locals 2

    const/16 v0, 0xc

    new-array v0, v0, [B

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/br;->c()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->h()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->f()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;
    .locals 10

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v8

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->u:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-nez v4, :cond_2

    move-object v4, v8

    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    if-nez v5, :cond_3

    move-object v5, v8

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    if-nez v6, :cond_4

    move-object v6, v8

    :goto_3
    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-nez v7, :cond_5

    move-object v7, v8

    :goto_4
    iget-object v9, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-nez v9, :cond_6

    :goto_5
    iget-object v9, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->j()Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    move-result-object v4

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->b()Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    move-result-object v5

    goto :goto_2

    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->a()Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    move-result-object v6

    goto :goto_3

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->f()Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    move-result-object v7

    goto :goto_4

    :cond_6
    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->e()Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    move-result-object v8

    goto :goto_5
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(I)V

    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(II)V

    :cond_0
    return-void
.end method

.method public final a(IIILjava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_NO_NETWORK:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/AdError;-><init>(Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->b(Lcom/google/android/apps/youtube/core/player/AdError;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(IIILjava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    invoke-static {p1, p2}, Lcom/google/android/apps/youtube/core/player/AdError;->a(II)Lcom/google/android/apps/youtube/core/player/AdError;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(IIIZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(III)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(I)V

    :cond_2
    return-void
.end method

.method public final a(J)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    long-to-int v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->k:Lcom/google/android/apps/youtube/core/client/bu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->k:Lcom/google/android/apps/youtube/core/client/bu;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/bu;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->a(J)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(J)V

    :cond_4
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/client/bi;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->f:Lcom/google/android/apps/youtube/core/client/bf;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/bf;->a(Lcom/google/android/apps/youtube/core/client/bi;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->w:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->x:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->y:Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->y:Z

    if-eqz v0, :cond_0

    const-string v0, "Suppressed an extra call to onResetVideoFlow. See b/12133789"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->y:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->z:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->A:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adCpn:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$000(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoCpn:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$100(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$200(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->t:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->trueViewInDisplayAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$300(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->w:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->x:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c:Lcom/google/android/apps/youtube/core/client/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->w:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->x:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adState:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;
    invoke-static {v5}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$400(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->heartbeatState:Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$500(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->playbackOffsetState:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$600(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->qoeState:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$700(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->vss2State:Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$800(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    move-result-object v0

    if-nez v0, :cond_6

    move-object v0, v1

    :goto_5
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    :goto_6
    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->k:Lcom/google/android/apps/youtube/core/client/bu;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->u:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c:Lcom/google/android/apps/youtube/core/client/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adState:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;
    invoke-static {v4}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$400(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    goto :goto_1

    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->f:Lcom/google/android/apps/youtube/core/client/bf;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->heartbeatState:Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;
    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$500(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/client/bf;->a(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    move-result-object v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->l:Lcom/google/android/apps/youtube/core/client/bx;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->playbackOffsetState:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;
    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$600(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/client/bx;->a(Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    move-result-object v0

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->n:Lcom/google/android/apps/youtube/core/client/cc;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->qoeState:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;
    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$700(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/client/cc;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;)Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    move-result-object v0

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->p:Lcom/google/android/apps/youtube/core/client/ch;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->v:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    # getter for: Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->vss2State:Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;
    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->access$800(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/google/android/apps/youtube/core/client/ch;->a(Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    move-result-object v0

    goto :goto_5

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->t:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    goto :goto_6
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Lcom/google/android/apps/youtube/core/client/WatchFeature;ZZLcom/google/android/apps/youtube/core/player/al;Ljava/lang/String;)V
    .locals 13

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->z:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->z:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->y:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    const-string v2, "onPlayVideo called before onResetVideoFlow"

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlaybackTracking()Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    move-result-object v4

    invoke-direct {p0, v5}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->f:Lcom/google/android/apps/youtube/core/client/bf;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getHeartbeatParams()Lcom/google/a/a/a/a/gr;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/google/android/apps/youtube/core/client/bf;->a(Lcom/google/a/a/a/a/gr;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g:Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->j:Lcom/google/android/apps/youtube/core/client/bv;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/youtube/core/client/bv;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/bu;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->k:Lcom/google/android/apps/youtube/core/client/bu;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->l:Lcom/google/android/apps/youtube/core/client/bx;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getPlaybackTrackingUrls()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/bx;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->n:Lcom/google/android/apps/youtube/core/client/cc;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getQoeTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->isLive()Z

    move-result v6

    invoke-virtual {v1, v2, v3, v5, v6}, Lcom/google/android/apps/youtube/core/client/cc;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->p:Lcom/google/android/apps/youtube/core/client/ch;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getVss2PlaybackTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getVss2DelayplayTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getVss2WatchtimeTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getLengthSeconds()I

    move-result v8

    move-object/from16 v7, p6

    move/from16 v9, p3

    move/from16 v10, p4

    move-object v11, p2

    move-object/from16 v12, p5

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/apps/youtube/core/client/ch;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVastProto()Lcom/google/a/a/a/a/tz;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c:Lcom/google/android/apps/youtube/core/client/d;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/d;->a()Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/converter/http/ek;->a(Lcom/google/a/a/a/a/tz;Lcom/google/android/apps/youtube/common/e/b;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getLengthSeconds()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c:Lcom/google/android/apps/youtube/core/client/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    move-object/from16 v0, p5

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->c()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->h:Lcom/google/android/apps/youtube/core/client/bs;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/client/bs;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/br;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;I)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 11

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->A:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->A:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->y:Z

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_1
    const-string v2, "onPlayAd called before onResetVideoFlow"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPlaybackTracking()Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->l:Lcom/google/android/apps/youtube/core/client/bx;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getPlaybackTrackingUrls()Ljava/util/List;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/apps/youtube/core/client/bx;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->m:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->n:Lcom/google/android/apps/youtube/core/client/cc;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getQoeTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v4, v5, v1}, Lcom/google/android/apps/youtube/core/client/cc;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->p:Lcom/google/android/apps/youtube/core/client/ch;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getVss2PlaybackTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v1

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getVss2DelayplayTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->getVss2WatchtimeTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v7

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdFormat()Ljava/lang/String;

    move-result-object v8

    move-object v9, p2

    move-object v10, p3

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/apps/youtube/core/client/ch;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->h:Lcom/google/android/apps/youtube/core/client/bs;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/bs;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/br;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a()V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c:Lcom/google/android/apps/youtube/core/client/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a()V

    sget-object v1, Lcom/google/android/apps/youtube/core/player/AdError;->a:Lcom/google/android/apps/youtube/core/player/AdError;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(Lcom/google/android/apps/youtube/core/player/AdError;)V

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->b()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c:Lcom/google/android/apps/youtube/core/client/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p3}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->c()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/aw;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/aw;)V

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/br;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Z)V

    :cond_1
    return-void
.end method

.method public final a(ZI)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->b(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->c(J)V

    :cond_1
    return-void
.end method

.method public final b()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o()V

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->b(I)V

    :cond_0
    return-void
.end method

.method public final b(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->f()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->e()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->b(J)V

    :cond_2
    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    :cond_1
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->c(I)V

    :cond_0
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final d(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->d(I)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->h()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->b()V

    :cond_2
    return-void
.end method

.method public final e(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->e(I)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->g()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->h()V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->i()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/br;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a()V

    :cond_3
    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->c()V

    :cond_1
    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i:Lcom/google/android/apps/youtube/core/client/br;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/br;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->q:Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d()V

    :cond_2
    return-void
.end method

.method public final j()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/AdError;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->UNSUPPORTED_VIDEO_FORMAT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/player/AdError;-><init>(Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->b(Lcom/google/android/apps/youtube/core/player/AdError;)V

    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->d()V

    :cond_0
    return-void
.end method

.method public final l()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->e()V

    :cond_0
    return-void
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d:Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient;->k()V

    :cond_0
    return-void
.end method
