.class public Lcom/google/android/apps/youtube/core/a/l;
.super Lcom/google/android/apps/youtube/core/a/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/a/f;


# instance fields
.field private final b:[Lcom/google/android/apps/youtube/core/a/e;

.field private final c:[I

.field private final d:Z


# direct methods
.method public varargs constructor <init>([Lcom/google/android/apps/youtube/core/a/e;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a/e;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    array-length v4, p1

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, p1, v0

    if-eqz v5, :cond_0

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/a/e;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/a/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a/l;->c:[I

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/youtube/core/a/l;->a(IZ)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    invoke-virtual {v6, p0}, Lcom/google/android/apps/youtube/core/a/e;->a(Lcom/google/android/apps/youtube/core/a/f;)V

    if-eqz v0, :cond_2

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/core/a/e;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/a/l;->d:Z

    return-void
.end method

.method private a(IZ)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/a/l;->c(I)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    array-length v1, v1

    if-ge p1, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a/e;->g()I

    move-result v1

    add-int/2addr v0, v1

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->c:[I

    aget v1, v1, p1

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->c:[I

    aput v0, v1, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c(I)I
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a/l;->c:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method private d(I)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a/l;->c:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->c:[I

    aget v1, v1, v0

    if-eq v1, p1, :cond_0

    :goto_0
    return v0

    :cond_1
    xor-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/l;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/a/l;->d(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    aget-object v1, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/a/l;->c(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/apps/youtube/core/a/e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/l;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/a/l;->d(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    aget-object v1, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/a/l;->c(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/a/e;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/a/e;)V
    .locals 3

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    aget-object v2, v2, v0

    if-ne p1, v2, :cond_1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/a/l;->a(IZ)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/l;->f()V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected final a(Ljava/util/Set;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/a/e;->a(Ljava/util/Set;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/a/l;->c(I)I

    move-result v0

    return v0
.end method

.method public b(I)Z
    .locals 2

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/l;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/a/l;->d(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    aget-object v1, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/a/l;->c(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/a/e;->b(I)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/a/l;->d:Z

    return v0
.end method

.method public final e(I)Lcom/google/android/apps/youtube/core/a/g;
    .locals 2

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/l;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/a/l;->d(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    aget-object v1, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/a/l;->c(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/a/e;->e(I)Lcom/google/android/apps/youtube/core/a/g;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)J
    .locals 2

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/l;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "position out of bounds"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/a/l;->d:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/a/l;->d(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a/l;->b:[Lcom/google/android/apps/youtube/core/a/e;

    aget-object v1, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/a/l;->c(I)I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/a/e;->f(I)J

    move-result-wide v0

    :goto_1
    return-wide v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    int-to-long v0, p1

    goto :goto_1
.end method
