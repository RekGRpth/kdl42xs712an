.class public final Lcom/google/android/apps/youtube/core/player/fetcher/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;IIZ)Lcom/google/a/a/a/a/ag;
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_2

    if-ltz p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p2, v0, :cond_3

    add-int/lit8 v1, p2, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v0, v3, v1, p4}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/a/a/a/a/kz;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-lez p2, :cond_4

    add-int/lit8 v2, p2, -0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v0, v3, v2, p4}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/a/a/a/a/kz;

    move-result-object v0

    :goto_2
    new-instance v2, Lcom/google/a/a/a/a/ag;

    invoke-direct {v2}, Lcom/google/a/a/a/a/ag;-><init>()V

    iput p3, v2, Lcom/google/a/a/a/a/ag;->b:I

    if-eqz v1, :cond_0

    iput-object v1, v2, Lcom/google/a/a/a/a/ag;->d:Lcom/google/a/a/a/a/kz;

    iput-object v1, v2, Lcom/google/a/a/a/a/ag;->c:Lcom/google/a/a/a/a/kz;

    :cond_0
    if-eqz v0, :cond_1

    iput-object v0, v2, Lcom/google/a/a/a/a/ag;->e:Lcom/google/a/a/a/a/kz;

    :cond_1
    return-object v2

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_5

    const/4 v0, 0x3

    if-ne p3, v0, :cond_5

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v0, v3, v1, p4}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/a/a/a/a/kz;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method

.method private static a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/fk;
    .locals 4

    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/a/a/a/a/sc;

    invoke-direct {v1}, Lcom/google/a/a/a/a/sc;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/a/a/a/a/sc;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    :cond_0
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/a/a/a/a/kz;
    .locals 2

    new-instance v0, Lcom/google/a/a/a/a/me;

    invoke-direct {v0}, Lcom/google/a/a/a/a/me;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object p0, v0, Lcom/google/a/a/a/a/me;->b:Ljava/lang/String;

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object p1, v0, Lcom/google/a/a/a/a/me;->c:Ljava/lang/String;

    :cond_1
    const/4 v1, -0x1

    if-eq p2, v1, :cond_2

    iput p2, v0, Lcom/google/a/a/a/a/me;->d:I

    :cond_2
    new-instance v1, Lcom/google/a/a/a/a/kz;

    invoke-direct {v1}, Lcom/google/a/a/a/a/kz;-><init>()V

    iput-object v0, v1, Lcom/google/a/a/a/a/kz;->w:Lcom/google/a/a/a/a/me;

    return-object v1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/a/a/a/a/kz;
    .locals 2

    if-eqz p3, :cond_0

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/a/a/a/a/kz;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/a/a/a/a/wb;

    invoke-direct {v1}, Lcom/google/a/a/a/a/wb;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p0, v1, Lcom/google/a/a/a/a/wb;->b:Ljava/lang/String;

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-object p1, v1, Lcom/google/a/a/a/a/wb;->c:Ljava/lang/String;

    :cond_2
    const/4 v0, -0x1

    if-eq p2, v0, :cond_3

    iput p2, v1, Lcom/google/a/a/a/a/wb;->d:I

    :cond_3
    new-instance v0, Lcom/google/a/a/a/a/kz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kz;-><init>()V

    iput-object v1, v0, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/wg;
    .locals 17

    new-instance v2, Lcom/google/a/a/a/a/wg;

    invoke-direct {v2}, Lcom/google/a/a/a/a/wg;-><init>()V

    move-object/from16 v0, p1

    iput-object v0, v2, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    new-instance v1, Lcom/google/a/a/a/a/wk;

    invoke-direct {v1}, Lcom/google/a/a/a/a/wk;-><init>()V

    iput-object v1, v2, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iget-object v1, v2, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    new-instance v3, Lcom/google/a/a/a/a/rp;

    invoke-direct {v3}, Lcom/google/a/a/a/a/rp;-><init>()V

    iput-object v3, v1, Lcom/google/a/a/a/a/wk;->b:Lcom/google/a/a/a/a/rp;

    iget-object v1, v2, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iget-object v1, v1, Lcom/google/a/a/a/a/wk;->b:Lcom/google/a/a/a/a/rp;

    new-instance v3, Lcom/google/a/a/a/a/rt;

    invoke-direct {v3}, Lcom/google/a/a/a/a/rt;-><init>()V

    iput-object v3, v1, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    iget-object v1, v2, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iget-object v1, v1, Lcom/google/a/a/a/a/wk;->b:Lcom/google/a/a/a/a/rp;

    iget-object v1, v1, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    new-instance v3, Lcom/google/a/a/a/a/qq;

    invoke-direct {v3}, Lcom/google/a/a/a/a/qq;-><init>()V

    iput-object v3, v1, Lcom/google/a/a/a/a/rt;->b:Lcom/google/a/a/a/a/qq;

    iget-object v1, v2, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iget-object v1, v1, Lcom/google/a/a/a/a/wk;->b:Lcom/google/a/a/a/a/rp;

    iget-object v1, v1, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    iget-object v1, v1, Lcom/google/a/a/a/a/rt;->b:Lcom/google/a/a/a/a/qq;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/a/a/a/a/qt;

    const/4 v4, 0x0

    new-instance v5, Lcom/google/a/a/a/a/qt;

    invoke-direct {v5}, Lcom/google/a/a/a/a/qt;-><init>()V

    aput-object v5, v3, v4

    iput-object v3, v1, Lcom/google/a/a/a/a/qq;->b:[Lcom/google/a/a/a/a/qt;

    iget-object v1, v2, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iget-object v1, v1, Lcom/google/a/a/a/a/wk;->b:Lcom/google/a/a/a/a/rp;

    iget-object v1, v1, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    iget-object v1, v1, Lcom/google/a/a/a/a/rt;->b:Lcom/google/a/a/a/a/qq;

    iget-object v1, v1, Lcom/google/a/a/a/a/qq;->b:[Lcom/google/a/a/a/a/qt;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    new-instance v4, Lcom/google/a/a/a/a/it;

    invoke-direct {v4}, Lcom/google/a/a/a/a/it;-><init>()V

    const/4 v1, 0x2

    new-array v5, v1, [Lcom/google/a/a/a/a/iv;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->b(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/fk;

    move-result-object v7

    const/4 v1, 0x0

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

    if-eqz v8, :cond_0

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

    :cond_0
    new-instance v8, Lcom/google/a/a/a/a/uw;

    invoke-direct {v8}, Lcom/google/a/a/a/a/uw;-><init>()V

    iput-object v7, v8, Lcom/google/a/a/a/a/uw;->c:Lcom/google/a/a/a/a/fk;

    new-instance v7, Lcom/google/a/a/a/a/sx;

    invoke-direct {v7}, Lcom/google/a/a/a/a/sx;-><init>()V

    iput-object v7, v8, Lcom/google/a/a/a/a/uw;->b:Lcom/google/a/a/a/a/sx;

    iget-object v7, v8, Lcom/google/a/a/a/a/uw;->b:Lcom/google/a/a/a/a/sx;

    const/4 v9, 0x1

    new-array v9, v9, [Lcom/google/a/a/a/a/sy;

    const/4 v10, 0x0

    new-instance v11, Lcom/google/a/a/a/a/sy;

    invoke-direct {v11}, Lcom/google/a/a/a/a/sy;-><init>()V

    aput-object v11, v9, v10

    iput-object v9, v7, Lcom/google/a/a/a/a/sx;->b:[Lcom/google/a/a/a/a/sy;

    iget-object v7, v8, Lcom/google/a/a/a/a/uw;->b:Lcom/google/a/a/a/a/sx;

    iget-object v7, v7, Lcom/google/a/a/a/a/sx;->b:[Lcom/google/a/a/a/a/sy;

    const/4 v9, 0x0

    aget-object v7, v7, v9

    iput-object v1, v7, Lcom/google/a/a/a/a/sy;->b:Ljava/lang/String;

    new-instance v1, Lcom/google/a/a/a/a/iv;

    invoke-direct {v1}, Lcom/google/a/a/a/a/iv;-><init>()V

    iput-object v8, v1, Lcom/google/a/a/a/a/iv;->q:Lcom/google/a/a/a/a/uw;

    aput-object v1, v5, v6

    const/4 v1, 0x1

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/fk;

    move-result-object v6

    new-instance v7, Lcom/google/a/a/a/a/fk;

    invoke-direct {v7}, Lcom/google/a/a/a/a/fk;-><init>()V

    new-instance v8, Lcom/google/a/a/a/a/sc;

    invoke-direct {v8}, Lcom/google/a/a/a/a/sc;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/youtube/p;->hb:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p2

    iget-wide v13, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    const/4 v9, 0x1

    new-array v9, v9, [Lcom/google/a/a/a/a/sc;

    const/4 v10, 0x0

    aput-object v8, v9, v10

    iput-object v9, v7, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    move-object/from16 v0, p2

    iget-wide v8, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->likesCount:J

    invoke-static {v8, v9}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(J)Lcom/google/a/a/a/a/fk;

    move-result-object v8

    move-object/from16 v0, p2

    iget-wide v9, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->likesCount:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    invoke-static {v9, v10}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(J)Lcom/google/a/a/a/a/fk;

    move-result-object v9

    move-object/from16 v0, p2

    iget-wide v10, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->dislikesCount:J

    invoke-static {v10, v11}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(J)Lcom/google/a/a/a/a/fk;

    move-result-object v10

    move-object/from16 v0, p2

    iget-wide v11, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->dislikesCount:J

    const-wide/16 v13, 0x1

    add-long/2addr v11, v13

    invoke-static {v11, v12}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(J)Lcom/google/a/a/a/a/fk;

    move-result-object v11

    new-instance v12, Lcom/google/a/a/a/a/fk;

    invoke-direct {v12}, Lcom/google/a/a/a/a/fk;-><init>()V

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->description:Ljava/lang/String;

    if-eqz v13, :cond_1

    new-instance v13, Lcom/google/a/a/a/a/sc;

    invoke-direct {v13}, Lcom/google/a/a/a/a/sc;-><init>()V

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->description:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    const/4 v14, 0x1

    new-array v14, v14, [Lcom/google/a/a/a/a/sc;

    const/4 v15, 0x0

    aput-object v13, v14, v15

    iput-object v14, v12, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    :cond_1
    new-instance v13, Lcom/google/a/a/a/a/fk;

    invoke-direct {v13}, Lcom/google/a/a/a/a/fk;-><init>()V

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->uploadedDate:Ljava/util/Date;

    if-eqz v14, :cond_2

    new-instance v14, Lcom/google/a/a/a/a/sc;

    invoke-direct {v14}, Lcom/google/a/a/a/a/sc;-><init>()V

    const/4 v15, 0x1

    iput-boolean v15, v14, Lcom/google/a/a/a/a/sc;->c:Z

    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v15

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->uploadedDate:Ljava/util/Date;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    const/4 v15, 0x1

    new-array v15, v15, [Lcom/google/a/a/a/a/sc;

    const/16 v16, 0x0

    aput-object v14, v15, v16

    iput-object v15, v13, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    :cond_2
    new-instance v14, Lcom/google/a/a/a/a/iv;

    invoke-direct {v14}, Lcom/google/a/a/a/a/iv;-><init>()V

    new-instance v15, Lcom/google/a/a/a/a/uv;

    invoke-direct {v15}, Lcom/google/a/a/a/a/uv;-><init>()V

    iput-object v15, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iget-object v15, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isActionable()Z

    move-result v16

    move/from16 v0, v16

    iput-boolean v0, v15, Lcom/google/a/a/a/a/uv;->j:Z

    iget-object v15, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isActionable()Z

    move-result v16

    move/from16 v0, v16

    iput-boolean v0, v15, Lcom/google/a/a/a/a/uv;->i:Z

    iget-object v15, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iput-object v12, v15, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    iget-object v12, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iput-object v13, v12, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    iget-object v12, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iput-object v7, v12, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    iget-object v7, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iput-object v6, v7, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    iget-object v6, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iput-object v10, v6, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    iget-object v6, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iput-object v11, v6, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    iget-object v6, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iput-object v8, v6, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    iget-object v6, v14, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    iput-object v9, v6, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    aput-object v14, v5, v1

    iput-object v5, v4, Lcom/google/a/a/a/a/it;->b:[Lcom/google/a/a/a/a/iv;

    iput-object v4, v3, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    return-object v2
.end method

.method public static a(Landroid/content/Context;Lcom/google/a/a/a/a/wg;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;I)Lcom/google/a/a/a/a/wg;
    .locals 6

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Landroid/content/Context;Lcom/google/a/a/a/a/wg;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;IZ)Lcom/google/a/a/a/a/wg;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/a/a/a/a/wg;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;IZ)Lcom/google/a/a/a/a/wg;
    .locals 9

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iget-object v3, v0, Lcom/google/a/a/a/a/wk;->b:Lcom/google/a/a/a/a/rp;

    new-instance v4, Lcom/google/a/a/a/a/ol;

    invoke-direct {v4}, Lcom/google/a/a/a/a/ol;-><init>()V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/a/a/a/a/ol;->e:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/a/a/a/a/ol;->b:Ljava/lang/String;

    iput p4, v4, Lcom/google/a/a/a/a/ol;->d:I

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v4, Lcom/google/a/a/a/a/ol;->f:I

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/a/a/a/a/om;

    iput-object v0, v4, Lcom/google/a/a/a/a/ol;->c:[Lcom/google/a/a/a/a/om;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    if-ne v1, p4, :cond_1

    const/4 v0, 0x1

    move v2, v0

    :goto_1
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    new-instance v5, Lcom/google/a/a/a/a/oo;

    invoke-direct {v5}, Lcom/google/a/a/a/a/oo;-><init>()V

    iput-boolean v2, v5, Lcom/google/a/a/a/a/oo;->g:Z

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/fk;

    move-result-object v2

    iput-object v2, v5, Lcom/google/a/a/a/a/oo;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->b(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/fk;

    move-result-object v2

    iput-object v2, v5, Lcom/google/a/a/a/a/oo;->l:Lcom/google/a/a/a/a/fk;

    add-int/lit8 v2, v1, 0x1

    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(J)Lcom/google/a/a/a/a/fk;

    move-result-object v2

    iput-object v2, v5, Lcom/google/a/a/a/a/oo;->f:Lcom/google/a/a/a/a/fk;

    new-instance v2, Lcom/google/a/a/a/a/fk;

    invoke-direct {v2}, Lcom/google/a/a/a/a/fk;-><init>()V

    new-instance v6, Lcom/google/a/a/a/a/sc;

    invoke-direct {v6}, Lcom/google/a/a/a/a/sc;-><init>()V

    iget v7, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->duration:I

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/youtube/common/e/m;->a(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/a/a/a/a/sc;

    const/4 v8, 0x0

    aput-object v6, v7, v8

    iput-object v7, v2, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    iput-object v2, v5, Lcom/google/a/a/a/a/oo;->e:Lcom/google/a/a/a/a/fk;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v6, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v2, v6, v1, p5}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/a/a/a/a/kz;

    move-result-object v2

    iput-object v2, v5, Lcom/google/a/a/a/a/oo;->h:Lcom/google/a/a/a/a/kz;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v2, v6, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v2, v6, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_3

    new-instance v2, Lcom/google/a/a/a/a/sx;

    invoke-direct {v2}, Lcom/google/a/a/a/a/sx;-><init>()V

    iput-object v2, v5, Lcom/google/a/a/a/a/oo;->d:Lcom/google/a/a/a/a/sx;

    iget-object v2, v5, Lcom/google/a/a/a/a/oo;->d:Lcom/google/a/a/a/a/sx;

    const/4 v6, 0x1

    new-array v6, v6, [Lcom/google/a/a/a/a/sy;

    const/4 v7, 0x0

    new-instance v8, Lcom/google/a/a/a/a/sy;

    invoke-direct {v8}, Lcom/google/a/a/a/a/sy;-><init>()V

    aput-object v8, v6, v7

    iput-object v6, v2, Lcom/google/a/a/a/a/sx;->b:[Lcom/google/a/a/a/a/sy;

    iget-object v2, v5, Lcom/google/a/a/a/a/oo;->d:Lcom/google/a/a/a/a/sx;

    iget-object v2, v2, Lcom/google/a/a/a/a/sx;->b:[Lcom/google/a/a/a/a/sy;

    const/4 v6, 0x0

    aget-object v2, v2, v6

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/a/a/a/a/sy;->b:Ljava/lang/String;

    :goto_3
    iget-object v0, v4, Lcom/google/a/a/a/a/ol;->c:[Lcom/google/a/a/a/a/om;

    new-instance v2, Lcom/google/a/a/a/a/om;

    invoke-direct {v2}, Lcom/google/a/a/a/a/om;-><init>()V

    aput-object v2, v0, v1

    iget-object v0, v4, Lcom/google/a/a/a/a/ol;->c:[Lcom/google/a/a/a/a/om;

    aget-object v0, v0, v1

    iput-object v5, v0, Lcom/google/a/a/a/a/om;->b:Lcom/google/a/a/a/a/oo;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    new-instance v2, Lcom/google/a/a/a/a/fk;

    invoke-direct {v2}, Lcom/google/a/a/a/a/fk;-><init>()V

    new-instance v6, Lcom/google/a/a/a/a/sc;

    invoke-direct {v6}, Lcom/google/a/a/a/a/sc;-><init>()V

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->explanationId:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/a/a/a/a/sc;

    const/4 v7, 0x0

    aput-object v6, v0, v7

    iput-object v0, v2, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    iput-object v2, v5, Lcom/google/a/a/a/a/oo;->j:Lcom/google/a/a/a/a/fk;

    goto :goto_3

    :cond_4
    new-instance v0, Lcom/google/a/a/a/a/rs;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rs;-><init>()V

    iput-object v4, v0, Lcom/google/a/a/a/a/rs;->b:Lcom/google/a/a/a/a/ol;

    iput-object v0, v3, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    new-instance v0, Lcom/google/a/a/a/a/rq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rq;-><init>()V

    new-instance v1, Lcom/google/a/a/a/a/af;

    invoke-direct {v1}, Lcom/google/a/a/a/a/af;-><init>()V

    iput-object v1, v0, Lcom/google/a/a/a/a/rq;->b:Lcom/google/a/a/a/a/af;

    iget-object v1, v0, Lcom/google/a/a/a/a/rq;->b:Lcom/google/a/a/a/a/af;

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/a/a/a/a/ag;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {p2, p3, p4, v5, p5}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;IIZ)Lcom/google/a/a/a/a/ag;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x3

    invoke-static {p2, p3, p4, v5, p5}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;IIZ)Lcom/google/a/a/a/a/ag;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-static {p2, p3, p4, v5, p5}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;IIZ)Lcom/google/a/a/a/a/ag;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const/4 v5, 0x4

    invoke-static {p2, p3, p4, v5, p5}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;IIZ)Lcom/google/a/a/a/a/ag;

    move-result-object v5

    aput-object v5, v2, v4

    iput-object v2, v1, Lcom/google/a/a/a/a/af;->b:[Lcom/google/a/a/a/a/ag;

    iput-object v0, v3, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;I)Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;
    .locals 6

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    const/4 v5, 0x1

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/a/a/a/a/kz;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Landroid/content/Context;Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/wg;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Landroid/content/Context;Lcom/google/a/a/a/a/wg;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;IZ)Lcom/google/a/a/a/a/wg;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;-><init>(Lcom/google/a/a/a/a/wg;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/a/a/a/a/kz;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Landroid/content/Context;Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/wg;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;-><init>(Lcom/google/a/a/a/a/wg;)V

    return-object v1
.end method

.method private static b(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/a/a/a/a/fk;
    .locals 4

    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    new-instance v1, Lcom/google/a/a/a/a/sc;

    invoke-direct {v1}, Lcom/google/a/a/a/a/sc;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/a/a/a/a/sc;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    return-object v0
.end method
