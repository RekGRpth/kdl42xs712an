.class public final Lcom/google/android/apps/youtube/core/player/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Random;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private h:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

.field private i:I

.field private j:I

.field private k:Lcom/google/android/apps/youtube/core/player/al;

.field private final l:Lcom/google/android/apps/youtube/core/client/cf;

.field private final m:Lcom/google/android/apps/youtube/common/network/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "(\\[[a-zA-Z_]+\\])"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/player/b;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Ljava/util/Random;Lcom/google/android/apps/youtube/core/client/cf;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 3

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->k:Lcom/google/android/apps/youtube/core/player/al;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/b;->c:Ljava/util/Random;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "a."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/b;->d:Lcom/google/android/apps/youtube/common/e/b;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/player/b;->l:Lcom/google/android/apps/youtube/core/client/cf;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/b;->m:Lcom/google/android/apps/youtube/common/network/h;

    iput v2, p0, Lcom/google/android/apps/youtube/core/player/b;->i:I

    iput v2, p0, Lcom/google/android/apps/youtube/core/player/b;->j:I

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)Landroid/net/Uri;
    .locals 6

    const/4 v5, 0x1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/b;->a:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v0

    if-ne v0, v5, :cond_0

    invoke-virtual {v2, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/youtube/core/player/d;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, ""

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->firstStreamUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->firstStreamUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->c:Ljava/util/Random;

    const v3, 0x55d4a7f

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    const v3, 0x989680

    add-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const-string v0, "00:00:00.000"

    goto :goto_1

    :pswitch_3
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/player/AdError;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_4
    const-string v0, ","

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOfflineAdFormatExclusionReasons()Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdIds()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, "0"

    goto :goto_1

    :pswitch_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdSystems()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    const-string v0, ""

    goto/16 :goto_1

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_9
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/player/AdError;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "1"

    goto/16 :goto_1

    :cond_7
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_a
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->value()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_b
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->m:Lcom/google/android/apps/youtube/common/network/h;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->m:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_c
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_d
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->f:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->f:Ljava/lang/String;

    goto/16 :goto_1

    :cond_a
    const-string v0, ""

    goto/16 :goto_1

    :pswitch_e
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->e:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->e:Ljava/lang/String;

    goto/16 :goto_1

    :cond_b
    const-string v0, ""

    goto/16 :goto_1

    :pswitch_f
    if-eqz p2, :cond_c

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/player/AdError;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_c
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_10
    const-string v0, ""

    goto/16 :goto_1

    :pswitch_11
    const-string v0, "detailpage"

    goto/16 :goto_1

    :pswitch_12
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->partnerId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_d
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_13
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdFormatSubType()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->subType:Ljava/lang/String;

    goto/16 :goto_1

    :cond_e
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_14
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_f

    const-string v0, "2"

    goto/16 :goto_1

    :cond_f
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_15
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/b;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_16
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/b;->j:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_17
    if-eqz p2, :cond_10

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/player/AdError;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_10
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_18
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->l:Lcom/google/android/apps/youtube/core/client/cf;

    if-nez v0, :cond_11

    const-string v0, "userPresenceTracker is not supported and should not expect receiving LACT macro"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    const-string v0, "-1"

    goto/16 :goto_1

    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->l:Lcom/google/android/apps/youtube/core/client/cf;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/cf;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_19
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_1a
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_1b
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->k:Lcom/google/android/apps/youtube/core/player/al;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->k:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/al;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_12
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_1c
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_1d
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getRequestTimes()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getRequestTimes()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_13
    const-string v0, ""

    goto/16 :goto_1

    :pswitch_1e
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->b:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_1f
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_20
    const-string v0, "0"

    goto/16 :goto_1

    :pswitch_21
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/b;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_14
    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/ConverterException;

    const-string v2, "Failed to convert URI"

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/core/converter/ConverterException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method

.method public final a(II)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/b;->i:I

    iput p2, p0, Lcom/google/android/apps/youtube/core/player/b;->j:I

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/b;->k:Lcom/google/android/apps/youtube/core/player/al;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/b;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/b;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/b;->e:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/b;->f:Ljava/lang/String;

    return-void
.end method
