.class final Lcom/google/android/apps/youtube/core/async/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lcom/google/android/apps/youtube/core/async/d;

.field private d:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/async/d;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/e;->c:Lcom/google/android/apps/youtube/core/async/d;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/async/e;->a:Lcom/google/android/apps/youtube/common/a/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/async/e;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/async/e;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/e;->c:Lcom/google/android/apps/youtube/core/async/d;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/d;->a(Lcom/google/android/apps/youtube/core/async/d;)Lcom/google/android/apps/youtube/core/async/f;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/f;->a(Ljava/lang/Object;Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/async/e;->d:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/e;->c:Lcom/google/android/apps/youtube/core/async/d;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/d;->b(Lcom/google/android/apps/youtube/core/async/d;)Lcom/google/android/apps/youtube/core/identity/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/b;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/e;->c:Lcom/google/android/apps/youtube/core/async/d;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/d;->c(Lcom/google/android/apps/youtube/core/async/d;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/e;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/e;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/e;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/e;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
