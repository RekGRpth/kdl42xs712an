.class public final Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/ad;


# instance fields
.field private a:Lcom/google/android/apps/youtube/common/c/a;

.field private b:Lcom/google/android/apps/youtube/core/Analytics;

.field private volatile c:Lcom/google/android/apps/youtube/core/player/al;

.field private d:Lcom/google/android/apps/youtube/core/player/i;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/Analytics;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->a:Lcom/google/android/apps/youtube/common/c/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->b:Lcom/google/android/apps/youtube/core/Analytics;

    return-void
.end method

.method private handlePlayerGeometry(Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->c:Lcom/google/android/apps/youtube/core/player/al;

    return-void
.end method

.method private i()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->b:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "PlaybackMonitor SequenceError"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "PlaybackMonitor queried outside playback sequence"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->b()V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/player/i;-><init>(Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->a:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->a:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()I
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->i()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/i;->a(Lcom/google/android/apps/youtube/core/player/i;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->i()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/i;->b(Lcom/google/android/apps/youtube/core/player/i;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->i()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/i;->c(Lcom/google/android/apps/youtube/core/player/i;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Lcom/google/android/apps/youtube/core/player/al;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->c:Lcom/google/android/apps/youtube/core/player/al;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->i()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;->d:Lcom/google/android/apps/youtube/core/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/i;->d(Lcom/google/android/apps/youtube/core/player/i;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
