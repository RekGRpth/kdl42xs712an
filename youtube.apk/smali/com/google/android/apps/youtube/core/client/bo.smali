.class public final Lcom/google/android/apps/youtube/core/client/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/ag;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/bp;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/bp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bp;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bo;->a:Lcom/google/android/apps/youtube/core/client/bp;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bo;->a:Lcom/google/android/apps/youtube/core/client/bp;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bp;->y()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "tvlite"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->j(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    :goto_0
    return-void

    :cond_0
    const-string v1, "xbox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->j(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    goto :goto_0

    :cond_1
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->j(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    goto :goto_0
.end method
