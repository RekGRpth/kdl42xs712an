.class public final Lcom/google/android/apps/youtube/core/client/cj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private b:Lcom/google/android/apps/youtube/common/e/b;

.field private c:Lcom/google/android/apps/youtube/core/converter/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/core/async/af;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->b:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/b;

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/dj;

    invoke-direct {v1, p2}, Lcom/google/android/apps/youtube/core/converter/http/dj;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    invoke-direct {v0, p2, p1, v1}, Lcom/google/android/apps/youtube/core/converter/b;-><init>(Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/http/dj;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->c:Lcom/google/android/apps/youtube/core/converter/b;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/core/client/k;Lcom/google/android/apps/youtube/core/converter/http/b;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->b:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/dj;

    invoke-direct {v1, p2}, Lcom/google/android/apps/youtube/core/converter/http/dj;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/http/b;

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/gs;

    invoke-direct {v2, p2, p1, v1}, Lcom/google/android/apps/youtube/core/converter/http/gs;-><init>(Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/http/dj;)V

    invoke-virtual {p3, v0, v2}, Lcom/google/android/apps/youtube/core/client/k;->a(Lcom/google/android/apps/youtube/core/converter/http/b;Lcom/google/android/apps/youtube/core/converter/http/gs;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/b;

    invoke-direct {v0, p2, p1, v1}, Lcom/google/android/apps/youtube/core/converter/b;-><init>(Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/http/dj;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->c:Lcom/google/android/apps/youtube/core/converter/b;

    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/bf;JJ)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;
    .locals 11

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isVastWrapper()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v1, v7, v9

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v1, v7, v9

    if-lez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v0

    :goto_2
    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    :cond_0
    invoke-virtual {v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    goto :goto_1

    :cond_1
    add-long v0, p2, p4

    goto :goto_2

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/bg;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;->a()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getTrackingDecoration()Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    move-result-object v0

    :goto_3
    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;)Lcom/google/android/apps/youtube/datalib/legacy/model/bg;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    move-result-object v0

    return-object v0

    :cond_4
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->NO_TRACKING_AUTH_SETTINGS:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;J)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;
    .locals 6

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVmapXml()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->c:Lcom/google/android/apps/youtube/core/converter/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/converter/b;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/cj;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v2

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/cj;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/bf;JJ)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/youtube/core/converter/ParserException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException;-><init>(Ljava/lang/Exception;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;J)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;
    .locals 6

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v1

    new-instance v4, Lcom/google/android/apps/youtube/core/converter/http/c;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v4, v2, v0}, Lcom/google/android/apps/youtube/core/converter/http/c;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/cj;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, v4, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/cj;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/bf;JJ)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method
