.class public final Lcom/google/android/apps/youtube/core/converter/http/ao;
.super Lcom/google/android/apps/youtube/core/converter/http/bw;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/apps/youtube/core/utils/ai;

.field public final b:Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

.field private final g:Lcom/google/android/apps/youtube/core/identity/ar;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;Lcom/google/android/apps/youtube/core/identity/ar;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/converter/http/bw;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->a:Lcom/google/android/apps/youtube/core/utils/ai;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->b:Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->g:Lcom/google/android/apps/youtube/core/identity/ar;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/utils/ai;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/converter/http/bw;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;)V

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/ai;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->a:Lcom/google/android/apps/youtube/core/utils/ai;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->b:Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->g:Lcom/google/android/apps/youtube/core/identity/ar;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/utils/ai;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;Lcom/google/android/apps/youtube/core/identity/ar;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/converter/http/bw;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;)V

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/ai;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->a:Lcom/google/android/apps/youtube/core/utils/ai;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->b:Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->g:Lcom/google/android/apps/youtube/core/identity/ar;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/utils/ai;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;Lcom/google/android/apps/youtube/core/identity/ar;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/converter/http/bw;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;)V

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/ai;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->a:Lcom/google/android/apps/youtube/core/utils/ai;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->b:Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->g:Lcom/google/android/apps/youtube/core/identity/ar;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/utils/ai;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;)Lcom/google/android/apps/youtube/core/converter/http/ao;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/ao;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/converter/http/ao;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/utils/ai;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;)V

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/bw;->b(Lcom/google/android/apps/youtube/core/async/w;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    const-string v1, "GData-Version"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->b:Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;->headerValue:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/ao;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Lcom/google/android/apps/youtube/core/async/w;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->a:Lcom/google/android/apps/youtube/core/utils/ai;

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->g:Lcom/google/android/apps/youtube/core/identity/ar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->g:Lcom/google/android/apps/youtube/core/identity/ar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/identity/ar;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->e:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ao;->a:Lcom/google/android/apps/youtube/core/utils/ai;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ai;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic b(Lcom/google/android/apps/youtube/core/async/w;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/ao;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method
