.class final Lcom/google/android/apps/youtube/core/player/overlay/j;
.super Lcom/google/android/apps/youtube/core/player/overlay/m;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/overlay/i;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/i;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/j;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/m;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/i;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/i;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/j;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/i;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/graphics/Bitmap;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Got CallToAction image from [uri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/j;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/core/player/overlay/g;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/player/overlay/g;->setCallToActionImage(Landroid/graphics/Bitmap;)V

    return-void
.end method
