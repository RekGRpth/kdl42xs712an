.class public final Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final engagedViewPinged:Z

.field public final impressionPinged:Z

.field public final kind:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

.field public final lastProgressEventMillis:I

.field public final nextQuartile:I

.field public final pingedCustomConversionTypes:Ljava/util/List;

.field public final skipAdShownPinged:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/client/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/client/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IZZZLjava/util/List;ILcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->nextQuartile:I

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->engagedViewPinged:Z

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->impressionPinged:Z

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->skipAdShownPinged:Z

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->pingedCustomConversionTypes:Ljava/util/List;

    iput p6, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->lastProgressEventMillis:I

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->kind:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->nextQuartile:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->engagedViewPinged:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->impressionPinged:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->skipAdShownPinged:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->readStringList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->pingedCustomConversionTypes:Ljava/util/List;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->lastProgressEventMillis:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->fromKindString(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->kind:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private readStringList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AdsStatsClient.AdsStatsClientState{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " nextQuartile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->nextQuartile:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " engagedViewPinged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->engagedViewPinged:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " impressionPinged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->impressionPinged:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pingedCustomConversionTypes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->pingedCustomConversionTypes:Ljava/util/List;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " kind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->kind:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->nextQuartile:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->engagedViewPinged:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->impressionPinged:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->skipAdShownPinged:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->pingedCustomConversionTypes:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->lastProgressEventMillis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->kind:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->kind:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->getKindString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
