.class final Lcom/google/android/apps/youtube/core/player/ui/d;
.super Landroid/support/v4/view/aj;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;Ljava/util/List;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->a:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    invoke-direct {p0}, Landroid/support/v4/view/aj;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->d:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->c:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    const/4 v0, -0x2

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v3

    iget-object v1, v3, Lcom/google/a/a/a/a/rm;->b:Lcom/google/a/a/a/a/sx;

    if-eqz v1, :cond_0

    iget-object v1, v3, Lcom/google/a/a/a/a/rm;->e:Lcom/google/a/a/a/a/ht;

    if-eqz v1, :cond_0

    iget-object v1, v3, Lcom/google/a/a/a/a/rm;->c:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c()Lcom/google/a/a/a/a/rk;

    move-result-object v7

    new-instance v0, Lcom/google/android/apps/youtube/core/player/ui/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->a:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->a:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    iget-object v4, v3, Lcom/google/a/a/a/a/rm;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v4}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v4

    iget-object v3, v3, Lcom/google/a/a/a/a/rm;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v3}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v5

    if-eqz v7, :cond_2

    iget-object v3, v7, Lcom/google/a/a/a/a/rk;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v3}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v6

    :cond_2
    move v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/ui/f;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/ui/h;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ui/f;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->c:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ui/f;->a(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->d:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ui/f;->a()Landroid/view/View;

    move-result-object v6

    goto :goto_0
.end method

.method public final a(ILandroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->c:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ui/f;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/player/ui/f;->a(Landroid/graphics/Bitmap;)V

    :cond_1
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/d;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ui/f;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ui/f;->a(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/d;->b()V

    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
