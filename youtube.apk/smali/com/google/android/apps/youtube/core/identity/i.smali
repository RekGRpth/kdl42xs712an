.class final Lcom/google/android/apps/youtube/core/identity/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/identity/f;

.field final synthetic b:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic c:Lcom/google/android/apps/youtube/core/identity/h;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/h;Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/i;->c:Lcom/google/android/apps/youtube/core/identity/h;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/i;->a:Lcom/google/android/apps/youtube/core/identity/f;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/i;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/i;->c:Lcom/google/android/apps/youtube/core/identity/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/h;->a(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/identity/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/i;->a:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/f;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/i;->c:Lcom/google/android/apps/youtube/core/identity/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/identity/h;->c(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/i;->c:Lcom/google/android/apps/youtube/core/identity/h;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/identity/h;->b(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->d()Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/util/Pair;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/i;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/i;->b:Lcom/google/android/apps/youtube/common/a/b;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Authentication unsuccessful."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/i;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, v4, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/i;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, v4, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
