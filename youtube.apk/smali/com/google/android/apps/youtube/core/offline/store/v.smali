.class public final Lcom/google/android/apps/youtube/core/offline/store/v;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "language_code"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "source_language_code"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "language_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "track_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "format"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "subtitles_path"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "track_vss_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/offline/store/v;->a:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/v;->b:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ljava/util/List;
    .locals 9

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/v;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "subtitles"

    sget-object v2, Lcom/google/android/apps/youtube/core/offline/store/v;->a:[Ljava/lang/String;

    const-string v3, "video_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/w;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/w;-><init>(Landroid/database/Cursor;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/offline/store/w;->a(Lcom/google/android/apps/youtube/core/offline/store/w;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/v;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "subtitles"

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->offlineSubtitlesPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    if-eqz p1, :cond_0

    const-string v4, "video_id"

    iget-object v5, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "language_code"

    iget-object v5, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "source_language_code"

    iget-object v5, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "language_name"

    iget-object v5, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "track_name"

    iget-object v5, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "format"

    iget v5, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->format:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "subtitles_path"

    iget-object v5, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->offlineSubtitlesPath:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "track_vss_id"

    iget-object v5, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->vssId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Error inserting subtitle track"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method final b(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/v;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "subtitles"

    const-string v2, "video_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method
