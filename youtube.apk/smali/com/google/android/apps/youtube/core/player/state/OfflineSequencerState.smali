.class public Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final clickTrackingParams:[B

.field public final currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field public final currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

.field public final index:I

.field public final loop:Z

.field public final pendingIndex:I

.field public final playlistId:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/state/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->videoId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->playlistId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->clickTrackingParams:[B

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->clickTrackingParams:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->index:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->pendingIndex:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->loop:Z

    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;IIZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->videoId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->playlistId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->clickTrackingParams:[B

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput p6, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->index:I

    iput p7, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->pendingIndex:I

    iput-boolean p8, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->loop:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->videoId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->videoId:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->playlistId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->playlistId:Ljava/lang/String;

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->clickTrackingParams:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->clickTrackingParams:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->index:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->pendingIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->loop:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method
