.class public final Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Set;

.field private static final c:Ljava/util/HashSet;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/async/t;

.field private final d:I

.field private final e:Lcom/google/android/apps/youtube/core/utils/v;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x10

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "zh-TW"

    aput-object v2, v1, v4

    const-string v2, "cs-CZ"

    aput-object v2, v1, v5

    const-string v2, "nl-NL"

    aput-object v2, v1, v6

    const-string v2, "en-GB"

    aput-object v2, v1, v7

    const-string v2, "en-US"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "fr-FR"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "de-DE"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "it-IT"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "ja-JP"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "ko-KR"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "pl-PL"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "pt-BR"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "ru-RU"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "es-ES"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "es-MX"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "sv-SE"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->c:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x26

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "AR"

    aput-object v2, v1, v4

    const-string v2, "AU"

    aput-object v2, v1, v5

    const-string v2, "BE"

    aput-object v2, v1, v6

    const-string v2, "BR"

    aput-object v2, v1, v7

    const-string v2, "CA"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "CL"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "CO"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "CZ"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "EG"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "FR"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "DE"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "GB"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "HK"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "HU"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "IN"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "IE"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "IL"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "IT"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "JP"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "JO"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "MY"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "MX"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "MA"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "NL"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "NZ"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "PE"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "PH"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string v3, "PL"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "RU"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "SA"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string v3, "SG"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "ZA"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string v3, "KR"

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string v3, "ES"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-string v3, "SE"

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-string v3, "TW"

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string v3, "AE"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string v3, "US"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/t;ILcom/google/android/apps/youtube/core/utils/v;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "gdataHostnameProvider must be provided"

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    if-lez p2, :cond_3

    move v0, v1

    :goto_1
    const-string v3, "resultsPerPage must be > 0"

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "countryCodeRestrict must be empty or a two letter country code (given: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    iput p2, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->e:Lcom/google/android/apps/youtube/core/utils/v;

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->f:Ljava/lang/String;

    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p4, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->f:Ljava/lang/String;

    goto :goto_2
.end method

.method private a(Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 2

    const-string v0, "start-index"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "max-results"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public static a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$UpcomingTimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "charts"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "live"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "events"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b(Landroid/net/Uri$Builder;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 4

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:media=\'http://search.yahoo.com/mrss/\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\' xmlns:gd=\'http://schemas.google.com/g/2005\' xmlns:gml=\'http://www.opengis.net/gml\' xmlns:georss=\'http://www.georss.org/georss\'><media:group><media:title type=\'plain\'>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x3c

    const-string v2, ""

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</media:title><media:description type=\'plain\'>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x1388

    const-string v2, ""

    invoke-static {p1, v1, v2}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</media:description><media:category label=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x64

    const-string v2, ""

    invoke-static {p3, v1, v2}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' scheme=\'http://gdata.youtube.com/schemas/2007/categories.cat"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p2, ""

    :cond_0
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</media:category><media:keywords>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x1f4

    const-string v2, ""

    invoke-static {p4, v1, v2}, Lcom/google/android/apps/youtube/core/utils/Util;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</media:keywords>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-ne p5, v0, :cond_3

    const-string v0, "<yt:private/>"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</media:group>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez p6, :cond_1

    new-instance p6, Ljava/util/LinkedHashMap;

    invoke-direct {p6}, Ljava/util/LinkedHashMap;-><init>()V

    :cond_1
    if-eqz p5, :cond_2

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PUBLIC:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-ne p5, v1, :cond_4

    const-string v1, "list"

    const-string v2, "allowed"

    invoke-interface {p6, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_1
    invoke-interface {p6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "<yt:accessControl action=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' permission=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    :cond_3
    const-string v0, ""

    goto :goto_0

    :cond_4
    const-string v1, "list"

    const-string v2, "denied"

    invoke-interface {p6, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_5
    if-eqz p7, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<yt:location>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</yt:location>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    if-eqz p8, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<georss:where><gml:Point><gml:pos>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</gml:pos></gml:Point></georss:where>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</entry>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {p9, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0

    :cond_7
    move-object v0, v2

    goto :goto_3
.end method

.method private a(Landroid/net/Uri$Builder;)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;I)V

    return-void
.end method

.method private a(Landroid/net/Uri$Builder;I)V
    .locals 2

    const-string v0, "format"

    const-string v1, "2,3,8,9"

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const/4 v0, 0x1

    invoke-static {p1, v0, p2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->e:Lcom/google/android/apps/youtube/core/utils/v;

    if-eqz v0, :cond_0

    const-string v0, "safeSearch"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->e:Lcom/google/android/apps/youtube/core/utils/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/utils/v;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "restriction"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    return-void
.end method

.method private static a(Landroid/net/Uri$Builder;II)V
    .locals 2

    const-string v0, "start-index"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v0, "max-results"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    return-void
.end method

.method private b(Landroid/net/Uri$Builder;)V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    return-void
.end method

.method public static c(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "true"

    const-string v1, "inline"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public static i(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public static j(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public static k(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public static l(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public static m(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method private p(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static q(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0
.end method

.method private t()Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/feeds/api/users/default/uploads"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private u()Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/feeds/api/users/default/subscriptions"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/feeds/api/users/default/suggestion"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "channel"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "channelstandardfeeds"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "type"

    const-string v2, "channel"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->t()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->i(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/util/Pair;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "managedByMe"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Ljava/util/Map;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/util/Pair;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "default"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "on-behalf-of"

    invoke-virtual {v0, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :cond_0
    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Ljava/util/Map;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;I)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 4

    iget v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "standardfeeds"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {p3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :goto_0
    if-eqz p4, :cond_1

    const-string v0, "time"

    invoke-virtual {p4}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;I)V

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feeds/api/videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ComplaintReason;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "complaints"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ComplaintReason;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ComplaintReason;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><summary>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1f4

    const-string v4, ""

    invoke-static {p3, v3, v4}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</summary><category scheme=\'http://gdata.youtube.com/schemas/2007/complaint-reasons.cat\' "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "term=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'/></entry>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 5

    const-string v0, "filename may not be empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v0, "Slug"

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "<?xml version=\'1.0\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:media=\'http://search.yahoo.com/mrss/\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\' xmlns:gml=\'http://www.opengis.net/gml\' xmlns:georss=\'http://www.georss.org/georss\'><media:group><media:title type=\'plain\'>"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x3c

    invoke-static {p3, v2, p1}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</media:title>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "<media:description type=\'plain\'>"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1388

    const-string v4, ""

    invoke-static {p4, v3, v4}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</media:description>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "<media:category scheme=\'http://gdata.youtube.com/schemas/2007/categories.cat\'>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string p5, "People"

    :cond_0
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</media:category><media:keywords>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x1f4

    const-string v3, ""

    invoke-static {p6, v2, v3}, Lcom/google/android/apps/youtube/core/utils/Util;->b(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</media:keywords>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-ne p2, v0, :cond_2

    const-string v0, "<yt:private/>"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</media:group>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p7, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "<georss:where><gml:Point><gml:pos>"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p7, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p7, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "</gml:pos></gml:Point></georss:where>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</entry>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/async/t;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "/resumable/feeds/api/users/default/uploads"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Ljava/util/Map;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, ""

    goto/16 :goto_0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "<yt:accessControl action=\'list\' permission=\'"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PUBLIC:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-ne p2, v0, :cond_3

    const-string v0, "allowed"

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'/>"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const-string v0, "denied"

    goto :goto_3

    :cond_4
    const-string v0, ""

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 4

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "playlists"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><title type=\'text\'>"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x3c

    const-string v3, ""

    invoke-static {p1, v2, v3}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</title><summary></summary>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_0

    const-string v0, "<yt:private/>"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</entry>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final b()Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/feeds/api/users/default/favorites"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    const/4 v2, 0x1

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feeds/player/videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "make"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "model"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "batch"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/feeds/api/users/default/watch_later"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    const-string v0, "channelId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "channels"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "thefeed"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/feeds/api/users/default/watch_history"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b(Landroid/net/Uri$Builder;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "username cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->o()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    const-string v0, "videoId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->t()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    const-string v0, "default"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->p(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final f(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "playlists"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final g(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "playlists"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    const-string v0, "live_now"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$UpcomingTimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final h(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 4

    const-string v0, "userId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->u()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "fields"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "entry[yt:username=\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\']"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x32

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "channelstandardfeeds"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    const-string v0, "most_viewed"

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    const-string v0, "recently_broadcasted"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$UpcomingTimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final j(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "partners"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "branding"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->c(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final k(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "favorites"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\'><id>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</id></entry>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final l(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "watch_later"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><id>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</id></entry>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "/feeds/api/users/default/playlists"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "watch_history"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><id>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</id></entry>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->u()Landroid/net/Uri;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "playlists"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "/feeds/api/users/default/newsubscriptionvideos"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;I)V

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final o(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    const-string v0, "username can\'t be empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "subscriptions"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'><category scheme=\'http://gdata.youtube.com/schemas/2007/subscriptiontypes.cat\' term=\'user\'/><yt:username>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</yt:username></entry>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/feeds/api/users/default/subtivity"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final q()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/feeds/api/users/default/recommendations"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri$Builder;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "users"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "watch_history"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "actions"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "clear"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\'></entry>"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b:Lcom/google/android/apps/youtube/core/async/t;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/async/t;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "channels"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "<?xml version=\'1.0\' encoding=\'UTF-8\'?><entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'></entry>"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method
