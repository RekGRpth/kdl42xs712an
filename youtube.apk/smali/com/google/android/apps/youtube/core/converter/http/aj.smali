.class final Lcom/google/android/apps/youtube/core/converter/http/aj;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/converter/http/ae;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/converter/http/aj;->a:Lcom/google/android/apps/youtube/core/converter/http/ae;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 3

    const-string v0, "http://gdata.youtube.com/schemas/2007/userevents.cat"

    const-string v1, "scheme"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "term"

    invoke-interface {p2, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    const-string v1, "BULLETIN_POSTED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->BULLETIN_POSTED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_a

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->action(Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v1, "VIDEO_UPLOADED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_UPLOADED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_2
    const-string v1, "VIDEO_RATED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_LIKED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_3
    const-string v1, "VIDEO_SHARED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_SHARED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_4
    const-string v1, "VIDEO_FAVORITED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_FAVORITED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_5
    const-string v1, "VIDEO_COMMENTED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_COMMENTED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_6
    const-string v1, "VIDEO_RECOMMENDED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_RECOMMENDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_7
    const-string v1, "VIDEO_ADDED_TO_PLAYLIST"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_ADDED_TO_PLAYLIST:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_8
    const-string v1, "FRIEND_ADDED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->FRIEND_ADDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_9
    const-string v1, "USER_SUBSCRIPTION_ADDED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->USER_SUBSCRIPTION_ADDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    move-object v1, v0

    goto :goto_0

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected event action "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_b
    move-object v1, v0

    goto/16 :goto_0
.end method
