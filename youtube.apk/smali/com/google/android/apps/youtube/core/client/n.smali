.class public final Lcom/google/android/apps/youtube/core/client/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/ag;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/n;->a:Lcom/google/android/apps/youtube/core/client/h;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/n;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->l()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/n;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->a()Lcom/google/android/apps/youtube/core/utils/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/n;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->a()Lcom/google/android/apps/youtube/core/utils/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/utils/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/n;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/n;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->i()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/network/h;->i()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->c(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/n;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->h()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->b(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    monitor-enter v1

    :try_start_0
    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/ad;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/ad;->e()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->e(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/ad;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->h(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/ad;->f()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->f(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/ad;->g()Lcom/google/android/apps/youtube/core/player/al;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/ad;->g()Lcom/google/android/apps/youtube/core/player/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/al;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->d(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/n;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->k()Lcom/google/android/apps/youtube/core/client/cf;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/cf;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a(J)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
