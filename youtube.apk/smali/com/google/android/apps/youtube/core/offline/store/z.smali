.class final Lcom/google/android/apps/youtube/core/offline/store/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/offline/store/x;

.field private final b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

.field private c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

.field private d:J

.field private e:J

.field private f:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

.field private g:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field private h:J

.field private i:J

.field private j:J

.field private k:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

.field private l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

.field private m:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/offline/store/x;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->a:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->m:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/offline/store/x;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/z;-><init>(Lcom/google/android/apps/youtube/core/offline/store/x;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    return-void
.end method


# virtual methods
.method final a()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    return-object v0
.end method

.method final declared-synchronized a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->j:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(JJ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->h:J

    iput-wide p3, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->i:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;JJ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iput-wide p2, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->d:J

    iput-wide p4, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->e:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->f:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->m:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c()Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized e()Lcom/google/android/apps/youtube/datalib/legacy/model/v;
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getOfflineState()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->d:J

    iget-wide v5, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->e:J

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;-><init>(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/v;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/z;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized f()Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    .locals 17

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    if-nez v1, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/core/offline/store/z;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v9

    :cond_0
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->f:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->f:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v11, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    :cond_1
    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v4, v4, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v4, v4, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->a:Lcom/google/android/apps/youtube/core/offline/store/x;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v6, v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/youtube/core/offline/store/x;->f(Ljava/lang/String;)Z

    move-result v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->j:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->h:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->i:J

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->m:Z

    move/from16 v16, v0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;ZJLcom/google/android/apps/youtube/datalib/legacy/model/v;Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;JJZ)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/core/offline/store/z;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v4, v4, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
