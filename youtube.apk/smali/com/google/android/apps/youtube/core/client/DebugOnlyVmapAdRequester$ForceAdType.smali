.class public final enum Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

.field public static final enum APP_PROMOTION_COMPANION_CARD:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

.field public static final enum BRAND_SURVEY:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

.field public static final enum BRAND_SURVEY_NON_SKIPPABLE:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

.field public static final enum SKIPPABLE_INSTREAM:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

.field public static final enum SKIPPABLE_INSTREAM_WITH_CTA_ANNOTATION:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;


# instance fields
.field public final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    const-string v1, "SKIPPABLE_INSTREAM"

    const-string v2, "Skippable instream"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->SKIPPABLE_INSTREAM:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    const-string v1, "SKIPPABLE_INSTREAM_WITH_CTA_ANNOTATION"

    const-string v2, "CTA annotation in ad"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->SKIPPABLE_INSTREAM_WITH_CTA_ANNOTATION:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    const-string v1, "BRAND_SURVEY"

    const-string v2, "Brand ad survey"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->BRAND_SURVEY:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    const-string v1, "BRAND_SURVEY_NON_SKIPPABLE"

    const-string v2, "Unskippable ad survey"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->BRAND_SURVEY_NON_SKIPPABLE:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    const-string v1, "APP_PROMOTION_COMPANION_CARD"

    const-string v2, "App promotion"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->APP_PROMOTION_COMPANION_CARD:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->SKIPPABLE_INSTREAM:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->SKIPPABLE_INSTREAM_WITH_CTA_ANNOTATION:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->BRAND_SURVEY:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->BRAND_SURVEY_NON_SKIPPABLE:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->APP_PROMOTION_COMPANION_CARD:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->$VALUES:[Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->title:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->$VALUES:[Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->title:Ljava/lang/String;

    return-object v0
.end method
