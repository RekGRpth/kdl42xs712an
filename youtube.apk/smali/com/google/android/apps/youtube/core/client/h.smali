.class public final Lcom/google/android/apps/youtube/core/client/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/utils/a;

.field private final b:Lcom/google/android/apps/youtube/common/e/b;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/youtube/core/client/ax;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Lcom/google/android/apps/youtube/common/network/h;

.field private final h:Lcom/google/android/apps/youtube/datalib/offline/n;

.field private final i:Lcom/google/android/apps/youtube/core/player/w;

.field private final j:Landroid/content/SharedPreferences;

.field private final k:Ljava/lang/String;

.field private final l:Lcom/google/android/apps/youtube/core/client/cf;

.field private final m:Lcom/google/android/apps/youtube/core/player/ad;

.field private n:J


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/client/i;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/utils/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->a:Lcom/google/android/apps/youtube/core/utils/a;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->b(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->c(Lcom/google/android/apps/youtube/core/client/i;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->c:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->d(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/client/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->d:Lcom/google/android/apps/youtube/core/client/ax;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->e(Lcom/google/android/apps/youtube/core/client/i;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->e:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->f(Lcom/google/android/apps/youtube/core/client/i;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/h;->f:Z

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->g(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->g:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->h(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/datalib/offline/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->h:Lcom/google/android/apps/youtube/datalib/offline/n;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->i(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->i:Lcom/google/android/apps/youtube/core/player/w;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->j(Lcom/google/android/apps/youtube/core/client/i;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->j:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->k(Lcom/google/android/apps/youtube/core/client/i;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->k:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->l(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/client/cf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->l:Lcom/google/android/apps/youtube/core/client/cf;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/i;->m(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->m:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/h;->j:Landroid/content/SharedPreferences;

    const-string v3, "last_ad_time"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/h;->n:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/client/i;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/h;-><init>(Lcom/google/android/apps/youtube/core/client/i;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/utils/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->a:Lcom/google/android/apps/youtube/core/utils/a;

    return-object v0
.end method

.method public final a(J)V
    .locals 2

    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/client/h;->n:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->j:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_ad_time"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->j:Landroid/content/SharedPreferences;

    const-string v1, "user_channel_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->d:Lcom/google/android/apps/youtube/core/client/ax;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->d:Lcom/google/android/apps/youtube/core/client/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/ax;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/h;->f:Z

    return v0
.end method

.method public final g()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/h;->n:J

    return-wide v0
.end method

.method public final h()I
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v0, 0x0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/client/h;->n:J

    cmp-long v1, v1, v5

    if-gtz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/h;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/client/h;->n:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    const-wide/32 v3, 0x7fffffff

    cmp-long v3, v1, v3

    if-gtz v3, :cond_0

    cmp-long v3, v1, v5

    if-lez v3, :cond_0

    long-to-int v0, v1

    goto :goto_0
.end method

.method public final i()Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->g:Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/youtube/core/client/cf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->l:Lcom/google/android/apps/youtube/core/client/cf;

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/youtube/core/player/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/h;->m:Lcom/google/android/apps/youtube/core/player/ad;

    return-object v0
.end method
