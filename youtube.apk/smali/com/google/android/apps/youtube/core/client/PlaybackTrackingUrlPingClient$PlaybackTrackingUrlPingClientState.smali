.class public Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cpn:Ljava/lang/String;

.field private final trackingUrls:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/client/by;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/client/by;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->trackingUrls:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->cpn:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/PriorityQueue;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {p1, v0}, Ljava/util/PriorityQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->trackingUrls:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->cpn:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;)[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->trackingUrls:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->cpn:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->trackingUrls:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->cpn:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
