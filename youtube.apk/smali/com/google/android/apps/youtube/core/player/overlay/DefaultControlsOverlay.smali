.class public Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;
.implements Lcom/google/android/apps/youtube/core/player/overlay/av;


# instance fields
.field private final A:Lcom/google/android/apps/youtube/core/player/overlay/aa;

.field private final B:Lcom/google/android/apps/youtube/core/player/overlay/z;

.field private C:Landroid/view/animation/Animation;

.field private D:Landroid/view/animation/Animation;

.field private E:I

.field private F:I

.field private G:Landroid/view/animation/Animation;

.field private H:Landroid/view/animation/Animation;

.field private I:Landroid/view/animation/Animation;

.field private J:Landroid/view/animation/Animation;

.field private K:Landroid/view/animation/Animation;

.field private L:Landroid/view/animation/Animation;

.field private M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

.field private N:Z

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

.field private a:Lcom/google/android/apps/youtube/core/player/overlay/p;

.field private final aa:Landroid/widget/FrameLayout;

.field private b:Lcom/google/android/apps/youtube/core/player/overlay/ad;

.field private c:Lcom/google/android/apps/youtube/core/player/overlay/y;

.field private d:Lcom/google/android/apps/youtube/core/player/overlay/ac;

.field private final e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

.field private final f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Landroid/widget/LinearLayout;

.field private final k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final n:Landroid/widget/TextView;

.field private final o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private final t:Lcom/google/android/apps/youtube/core/player/overlay/bb;

.field private final u:Landroid/widget/RelativeLayout;

.field private final v:Landroid/widget/ProgressBar;

.field private final w:Landroid/widget/TextView;

.field private final x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

.field private final y:Lcom/google/android/apps/youtube/core/player/overlay/at;

.field private final z:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->z:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/at;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/ab;

    invoke-direct {v1, p0, v4}, Lcom/google/android/apps/youtube/core/player/overlay/ab;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;B)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/at;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/au;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->y:Lcom/google/android/apps/youtube/core/player/overlay/at;

    sget v0, Lcom/google/android/youtube/b;->d:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->C:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->C:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    sget v0, Lcom/google/android/youtube/b;->c:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->D:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->a:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->I:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->b:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->J:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->g:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->K:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->h:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->L:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/k;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->E:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/k;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->F:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->D:Landroid/view/animation/Animation;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->E:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    sget v0, Lcom/google/android/youtube/b;->c:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->G:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->d:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->H:Landroid/view/animation/Animation;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/k;->j:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->G:Landroid/view/animation/Animation;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->H:Landroid/view/animation/Animation;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->H:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->YOUTUBE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/bb;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/bb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->t:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setClipToPadding(Z)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/aa;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/youtube/core/player/overlay/aa;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->A:Lcom/google/android/apps/youtube/core/player/overlay/aa;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/z;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/youtube/core/player/overlay/z;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->B:Lcom/google/android/apps/youtube/core/player/overlay/z;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->N:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    sget v1, Lcom/google/android/youtube/l;->O:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->aF:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->u:Landroid/widget/RelativeLayout;

    sget v0, Lcom/google/android/youtube/j;->fC:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->A:Lcom/google/android/apps/youtube/core/player/overlay/aa;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setListener(Lcom/google/android/apps/youtube/core/player/overlay/bu;)V

    sget v0, Lcom/google/android/youtube/j;->br:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->ci:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_LIGHT:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/utils/Typefaces;->toTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget v0, Lcom/google/android/youtube/j;->G:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->fI:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->fD:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->j:Landroid/widget/LinearLayout;

    sget v0, Lcom/google/android/youtube/j;->dz:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->v:Landroid/widget/ProgressBar;

    sget v0, Lcom/google/android/youtube/j;->dw:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->w:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->du:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->dv:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->dt:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->dr:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->dB:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->dy:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_LIGHT:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/utils/Typefaces;->toTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    sget v0, Lcom/google/android/youtube/j;->ds:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->bL:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->dA:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->cZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->B:Lcom/google/android/apps/youtube/core/player/overlay/z;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setListener(Lcom/google/android/apps/youtube/core/player/overlay/af;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    sget v0, Lcom/google/android/youtube/j;->dq:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->aa:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    return-object v0
.end method

.method private a(Z)V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->C:Landroid/view/animation/Animation;

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->E:I

    int-to-long v0, v0

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->L:Landroid/view/animation/Animation;

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->E:I

    int-to-long v0, v0

    :goto_1
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->J:Landroid/view/animation/Animation;

    if-eqz p1, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->E:I

    int-to-long v0, v0

    :goto_2
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->alwaysVisibleTimeBar:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->aa:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Landroid/view/View;)V

    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->F:I

    int-to-long v0, v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->F:I

    int-to-long v0, v0

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->F:I

    int-to-long v0, v0

    goto :goto_2
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->J:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i:Landroid/view/View;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->L:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->C:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b:Lcom/google/android/apps/youtube/core/player/overlay/ad;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->I:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i:Landroid/view/View;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->K:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->D:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->G:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->u:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->j()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->A:Lcom/google/android/apps/youtube/core/player/overlay/aa;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    return-object v0
.end method

.method private j()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->N:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->z:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->V:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->z:Landroid/os/Handler;

    const-wide/16 v1, 0x9c4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void
.end method

.method private k()V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->z:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    sget v3, Lcom/google/android/youtube/h;->at:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/p;->b:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->w:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v3

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->v:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v4, :cond_7

    :cond_0
    move v0, v2

    :goto_1
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->N:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->aa:Landroid/widget/FrameLayout;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->alwaysVisibleTimeBar:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isOnVideo()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_2
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->alwaysVisibleTimeBar:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isOnVideo()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isReady()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    move v1, v2

    :cond_4
    invoke-static {p0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    :goto_3
    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    sget v3, Lcom/google/android/youtube/h;->as:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/p;->a:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    sget v3, Lcom/google/android/youtube/h;->au:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/p;->c:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Q:Z

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->R:Z

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->AD:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    if-eq v0, v4, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isOnVideo()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v2

    :goto_4
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->AD:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    if-eq v0, v4, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isOnVideo()Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v2

    :goto_5
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->AD:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    if-eq v0, v4, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isOnVideo()Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v2

    :goto_6
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->W:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->AD:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    if-ne v0, v4, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isOnVideo()Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v2

    :goto_7
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->aa:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v2

    :goto_8
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsTimeBar:Z

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->REMOTE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    if-eq v0, v4, :cond_10

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Q:Z

    if-eqz v0, :cond_10

    move v0, v2

    :goto_9
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->LIVE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    if-ne v0, v4, :cond_11

    move v0, v2

    :goto_a
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isOnVideo()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->alwaysVisibleTimeBar:Z

    if-nez v0, :cond_12

    move v0, v2

    :goto_b
    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isReady()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v0, :cond_13

    move v0, v1

    :goto_c
    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsNextPrevious:Z

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->T:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->U:Z

    if-eqz v0, :cond_14

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v0, v3, :cond_14

    move v0, v2

    :goto_d
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->T:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->U:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->u:Landroid/widget/RelativeLayout;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    invoke-static {p0, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/view/View;Z)V

    goto/16 :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_4

    :cond_c
    move v0, v1

    goto/16 :goto_5

    :cond_d
    move v0, v1

    goto/16 :goto_6

    :cond_e
    move v0, v1

    goto/16 :goto_7

    :cond_f
    move v0, v1

    goto/16 :goto_8

    :cond_10
    move v0, v1

    goto/16 :goto_9

    :cond_11
    move v0, v1

    goto :goto_a

    :cond_12
    move v0, v1

    goto :goto_b

    :cond_13
    const/4 v0, 0x4

    goto :goto_c

    :cond_14
    move v0, v1

    goto :goto_d
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->alwaysVisibleTimeBar:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->aa:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c(Landroid/view/View;)V

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->aa:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->t:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/x;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/overlay/x;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/player/overlay/bb;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/overlay/bd;)V

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->S:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->N:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->j()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->j()V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->N:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->k()V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->t:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bb;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->d()V

    return-void
.end method

.method public final h()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->b()I

    move-result v0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Z)V

    :goto_0
    return v0

    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->z:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->aa:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->clearAnimation()V

    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->C:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->H:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->u:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->N:Z

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->p:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->T:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsNextPrevious:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->d()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b:Lcom/google/android/apps/youtube/core/player/overlay/ad;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->l:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b:Lcom/google/android/apps/youtube/core/player/overlay/ad;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ad;->d()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c:Lcom/google/android/apps/youtube/core/player/overlay/y;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->n:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c:Lcom/google/android/apps/youtube/core/player/overlay/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/y;->a()V

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->q:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->U:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsNextPrevious:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->e()V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->o:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v1, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->m()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->b()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->a()V

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->r:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v1, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->d:Lcom/google/android/apps/youtube/core/player/overlay/ac;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ac;->H()V

    goto :goto_0

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->s:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v1, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->i()V

    goto :goto_0

    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v1, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->u:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->H:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_b

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->P:Z

    if-eqz v1, :cond_c

    :cond_b
    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->O:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->O:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/p;->b(Z)V

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto :goto_2

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->m:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b:Lcom/google/android/apps/youtube/core/player/overlay/ad;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ad;->b()V

    goto/16 :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i()V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/overlay/at;->b(I)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e()V

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->RECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v3, v4, :cond_6

    if-eqz v2, :cond_6

    const/16 v2, 0x14

    if-eq p1, v2, :cond_2

    const/16 v2, 0x15

    if-eq p1, v2, :cond_2

    const/16 v2, 0x16

    if-eq p1, v2, :cond_2

    const/16 v2, 0x13

    if-ne p1, v2, :cond_5

    :cond_2
    move v2, v0

    :goto_1
    if-nez v2, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/overlay/p;->l()V

    :cond_3
    :goto_2
    return v0

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->y:Lcom/google/android/apps/youtube/core/player/overlay/at;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/at;->a(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_2
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->y:Lcom/google/android/apps/youtube/core/player/overlay/at;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/at;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    const/16 v2, 0xa

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    int-to-float v0, p1

    const v1, 0x3e2aaaab

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->w:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->S:Z

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->RECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/overlay/p;->l()V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->N:Z

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a()V

    goto :goto_0

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->V:Z

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Z)V

    goto :goto_0
.end method

.method public setAdActionsListener(Lcom/google/android/apps/youtube/core/player/overlay/y;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->c:Lcom/google/android/apps/youtube/core/player/overlay/y;

    return-void
.end method

.method public setAlwaysShowControls(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->V:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    return-void
.end method

.method public setAndShowEnded()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setBufferedPercent(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e()V

    goto :goto_0
.end method

.method public setAndShowPaused()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e()V

    goto :goto_0
.end method

.method public setAudioOnlyEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setAudioOnlyEnabled(Z)V

    return-void
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setCC(Z)V

    return-void
.end method

.method public setErrorAndShowMessage(IZ)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setErrorAndShowMessage(Ljava/lang/String;Z)V

    return-void
.end method

.method public setErrorAndShowMessage(Ljava/lang/String;Z)V
    .locals 5

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->RECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->touchscreen:I

    if-eq v0, v1, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->fZ:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->w:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e()V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->UNRECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->am:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    const-string v0, ""

    goto :goto_3
.end method

.method public setFullscreen(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->O:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setSelected(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Z)V

    :cond_0
    return-void
.end method

.method public setHQ(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setHQ(Z)V

    return-void
.end method

.method public setHQisHD(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setHQisHD(Z)V

    return-void
.end method

.method public setHasAudioOnly(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setHasAudioOnly(Z)V

    return-void
.end method

.method public setHasCc(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setHasCC(Z)V

    return-void
.end method

.method public setHasInfoCard(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->R:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    return-void
.end method

.method public setHasNext(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->T:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    return-void
.end method

.method public setHasPrevious(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->U:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    return-void
.end method

.method public setInitial()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    goto :goto_0
.end method

.method public setLearnMoreEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->W:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    return-void
.end method

.method public setListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a:Lcom/google/android/apps/youtube/core/player/overlay/p;

    return-void
.end method

.method public setLoading()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->j()V

    goto :goto_0
.end method

.method public setLockedInFullscreen(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->P:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->O:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setFullscreen(Z)V

    return-void
.end method

.method public setMenuActionsListener(Lcom/google/android/apps/youtube/core/player/overlay/ad;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b:Lcom/google/android/apps/youtube/core/player/overlay/ad;

    return-void
.end method

.method public setMinimized(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->S:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->S:Z

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->V:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isReady()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    goto :goto_0
.end method

.method public setPlaying()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->M:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->j()V

    goto :goto_0
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setEnabled(Z)V

    return-void
.end method

.method public setShowAudioOnly(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setShowAudioOnly(Z)V

    return-void
.end method

.method public setShowFullscreen(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Q:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    return-void
.end method

.method public setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V
    .locals 4

    const/16 v3, 0xb

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->Z:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget v1, p1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->progressColor:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setProgressColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsBuffered:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setShowBuffered(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsShowTime:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setShowTimes(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsScrubber:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setShowScrubber(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->AD:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    if-ne p1, v1, :cond_0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->j()V

    return-void

    :cond_0
    invoke-virtual {v0, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->x:Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setHasQuality(Z)V

    return-void
.end method

.method public setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setTime(III)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->y:Lcom/google/android/apps/youtube/core/player/overlay/at;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/at;->a(II)V

    return-void
.end method

.method public setUiListener(Lcom/google/android/apps/youtube/core/player/overlay/ac;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->d:Lcom/google/android/apps/youtube/core/player/overlay/ac;

    return-void
.end method
