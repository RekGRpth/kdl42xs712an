.class public final Lcom/google/android/apps/youtube/core/async/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final b:Lcom/google/android/apps/youtube/common/cache/a;

.field private final c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/as;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/as;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/as;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/as;->d:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/as;Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/util/Set;)V
    .locals 6

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/as;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/as;->b:Lcom/google/android/apps/youtube/common/cache/a;

    new-instance v3, Lcom/google/android/apps/youtube/core/async/Timestamped;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/async/as;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v4

    invoke-direct {v3, v1, v4, v5}, Lcom/google/android/apps/youtube/core/async/Timestamped;-><init>(Ljava/lang/Object;J)V

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/at;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/core/async/at;-><init>(Lcom/google/android/apps/youtube/core/async/as;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/as;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
