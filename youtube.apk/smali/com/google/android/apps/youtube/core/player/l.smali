.class final Lcom/google/android/apps/youtube/core/player/l;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/Director;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/l;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/Director;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/l;-><init>(Lcom/google/android/apps/youtube/core/player/Director;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/l;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/l;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->r(Lcom/google/android/apps/youtube/core/player/Director;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.media.AUDIO_BECOMING_NOISY"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/l;->b:Z

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/l;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/l;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->r(Lcom/google/android/apps/youtube/core/player/Director;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/l;->b:Z

    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const-string v0, "Audio becoming noisy. Pausing if needed"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/l;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->t(Lcom/google/android/apps/youtube/core/player/Director;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/l;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/l;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->c()V

    :cond_0
    return-void
.end method
