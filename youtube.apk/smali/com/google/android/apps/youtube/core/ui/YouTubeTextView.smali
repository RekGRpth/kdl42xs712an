.class public Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;
.super Landroid/widget/TextView;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, v0, v0}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->a(Landroid/content/Context;IIZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/google/android/youtube/r;->M:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/TypedArray;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/google/android/youtube/r;->M:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->a(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/TypedArray;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private a(Landroid/content/Context;IIZ)V
    .locals 1

    packed-switch p2, :pswitch_data_0

    const-string v0, "Missing or invalid font preference on a RobotoTextView."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_LIGHT:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/utils/Typefaces;->toTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0, p3}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_0
    invoke-virtual {p0, p4}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setAllCaps(Z)V

    return-void

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_REGULAR:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/TypedArray;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, -0x1

    invoke-virtual {p3, v3, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    const-string v1, "android"

    const-string v2, "textStyle"

    invoke-interface {p2, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p3, v4, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->a(Landroid/content/Context;IIZ)V

    const/4 v0, 0x2

    invoke-virtual {p3, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->b:Z

    invoke-static {}, Lcom/google/android/apps/youtube/core/ui/ab;->a()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :goto_0
    return-void

    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->b:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->getDefaultMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->a:Z

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->a:Z

    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->a:Z

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
