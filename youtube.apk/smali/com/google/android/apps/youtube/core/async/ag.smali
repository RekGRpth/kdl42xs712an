.class public final Lcom/google/android/apps/youtube/core/async/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/async/af;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/ah;

    const/16 v1, 0x32

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/ah;-><init>(Lcom/google/android/apps/youtube/core/async/af;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/ag;->a:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method static synthetic a(Landroid/net/Uri;II)Landroid/net/Uri;
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "start-index"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "max-results"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/ag;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/async/ag;

    const/16 v1, 0x32

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/async/ag;-><init>(Lcom/google/android/apps/youtube/core/async/af;I)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ag;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/aj;

    invoke-direct {v1, p2}, Lcom/google/android/apps/youtube/core/async/aj;-><init>(Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
