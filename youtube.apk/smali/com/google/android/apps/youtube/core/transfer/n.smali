.class final Lcom/google/android/apps/youtube/core/transfer/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private c:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/n;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/transfer/n;->b:Ljava/lang/String;

    return-void
.end method

.method private static d(Lcom/google/android/apps/youtube/core/transfer/t;)Landroid/content/ContentValues;
    .locals 4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "file_path"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "network_uri"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "status"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "status_reason"

    iget v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "bytes_transferred"

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "bytes_total"

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "extras"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v1, "output_extras"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v1, "accountname"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 26

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "transfers"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    move-object v4, v3

    :goto_0
    :try_start_0
    const-string v3, "file_path"

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    const-string v5, "network_uri"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    const-string v6, "status"

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    const-string v7, "status_reason"

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const-string v8, "bytes_transferred"

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    const-string v9, "bytes_total"

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    const-string v10, "extras"

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const-string v12, "output_extras"

    invoke-interface {v4, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    const-string v13, "accountname"

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->values()[Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    move-result-object v16

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    aget-object v16, v16, v17

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    new-instance v22, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>([B)V

    new-instance v23, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-interface {v4, v12}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>([B)V

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    new-instance v25, Lcom/google/android/apps/youtube/core/transfer/t;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v14, v15, v2}, Lcom/google/android/apps/youtube/core/transfer/t;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    move/from16 v0, v17

    move-object/from16 v1, v25

    iput v0, v1, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    move-wide/from16 v0, v20

    move-object/from16 v2, v25

    iput-wide v0, v2, Lcom/google/android/apps/youtube/core/transfer/t;->f:J

    move-wide/from16 v0, v18

    move-object/from16 v2, v25

    iput-wide v0, v2, Lcom/google/android/apps/youtube/core/transfer/t;->e:J

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/google/android/apps/youtube/core/transfer/t;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "transfers"

    const/4 v5, 0x0

    const-string v6, "accountname=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_0

    :cond_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    return-object v11
.end method

.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/o;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/n;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/n;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/transfer/o;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/o;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/transfer/t;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "transfers"

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/n;->d(Lcom/google/android/apps/youtube/core/transfer/t;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method public final b()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "transfers"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/core/transfer/t;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "transfers"

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/n;->d(Lcom/google/android/apps/youtube/core/transfer/t;)Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "file_path = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "transfers"

    const-string v2, "file_path = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/n;->c:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method public final c(Lcom/google/android/apps/youtube/core/transfer/t;)V
    .locals 1

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Ljava/lang/String;)V

    return-void
.end method
