.class public final Lcom/google/android/apps/youtube/core/ui/g;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Lcom/google/android/apps/youtube/core/player/overlay/ay;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I


# instance fields
.field private final e:Landroid/view/Window;

.field private final f:Landroid/app/ActionBar;

.field private final g:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

.field private final h:Lcom/google/android/apps/youtube/core/ui/h;

.field private i:Landroid/graphics/Rect;

.field private final j:I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private final r:Z

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x3

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    const/4 v1, 0x7

    const/16 v2, 0x600

    const/16 v0, 0x100

    :goto_0
    sput v1, Lcom/google/android/apps/youtube/core/ui/g;->a:I

    sput v2, Lcom/google/android/apps/youtube/core/ui/g;->c:I

    sput v0, Lcom/google/android/apps/youtube/core/ui/g;->d:I

    const/4 v0, 0x1

    sput v0, Lcom/google/android/apps/youtube/core/ui/g;->b:I

    return-void

    :cond_0
    move v2, v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/apps/youtube/core/ui/h;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const-string v0, "window cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    const-string v0, "playerOverlaysLayout cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->g:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {p3, p0}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemWindowInsetsListener(Lcom/google/android/apps/youtube/core/player/overlay/ay;)V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/ui/g;->f:Landroid/app/ActionBar;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/ui/g;->h:Lcom/google/android/apps/youtube/core/ui/h;

    invoke-virtual {p3, p0}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->getSystemUiVisibility()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->k:I

    invoke-virtual {p3, p0}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setfullscreenUiVisibilityHelper(Lcom/google/android/apps/youtube/core/ui/g;)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->n:Z

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v1, [I

    const v3, 0x10102eb    # android.R.attr.actionBarSize

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->j:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    :goto_1
    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->r:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/ui/g;->s:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iput v2, p0, Lcom/google/android/apps/youtube/core/ui/g;->j:I

    goto :goto_1
.end method

.method private b()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/core/ui/g;->removeMessages(I)V

    iget v3, p0, Lcom/google/android/apps/youtube/core/ui/g;->k:I

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->l:Z

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/core/ui/g;->m:Z

    and-int/2addr v4, v0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->s:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    and-int/2addr v4, v0

    sget v0, Lcom/google/android/apps/youtube/core/ui/g;->a:I

    and-int/2addr v0, v3

    sget v5, Lcom/google/android/apps/youtube/core/ui/g;->a:I

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_1
    iget-boolean v5, p0, Lcom/google/android/apps/youtube/core/ui/g;->l:Z

    iget-boolean v6, p0, Lcom/google/android/apps/youtube/core/ui/g;->m:Z

    and-int/2addr v5, v6

    iget-boolean v6, p0, Lcom/google/android/apps/youtube/core/ui/g;->s:Z

    and-int/2addr v5, v6

    sget v6, Lcom/google/android/apps/youtube/core/ui/g;->b:I

    and-int/2addr v3, v6

    sget v6, Lcom/google/android/apps/youtube/core/ui/g;->b:I

    if-ne v3, v6, :cond_4

    if-nez v0, :cond_4

    move v3, v1

    :goto_2
    if-ne v4, v0, :cond_0

    if-eq v5, v3, :cond_5

    :cond_0
    move v0, v1

    :goto_3
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/ui/g;->p:Z

    if-nez v3, :cond_6

    if-eqz v0, :cond_6

    :goto_4
    if-eqz v1, :cond_1

    const-wide/16 v0, 0x9c4

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4
.end method

.method private c()V
    .locals 4

    const/16 v1, 0x400

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->b()V

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/ui/g;->r:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/ui/g;->l:Z

    if-eqz v3, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v2, v0, v1}, Landroid/view/Window;->setFlags(II)V

    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->l:Z

    if-eqz v1, :cond_2

    sget v2, Lcom/google/android/apps/youtube/core/ui/g;->c:I

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->m:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->s:Z

    if-eqz v1, :cond_3

    sget v1, Lcom/google/android/apps/youtube/core/ui/g;->b:I

    :goto_1
    or-int/2addr v1, v2

    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    :goto_3
    or-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->g:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemUiVisibility(I)V

    goto :goto_0

    :cond_3
    sget v1, Lcom/google/android/apps/youtube/core/ui/g;->a:I

    goto :goto_1

    :cond_4
    sget v0, Lcom/google/android/apps/youtube/core/ui/g;->d:I

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method private d()Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->r:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->q:Z

    if-eqz v1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->f:Landroid/app/ActionBar;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/view/Window;->hasFeature(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 9

    const/16 v8, 0x10

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->g:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildCount()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->g:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->l:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->i:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/ui/g;->i:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/ui/g;->i:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/ui/g;->i:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v0, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->n:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    const/16 v5, 0x9

    invoke-virtual {v0, v5}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->j:I

    invoke-virtual {v4, v2, v0, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_4
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    :cond_5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/g;->removeMessages(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->p:Z

    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->f:Landroid/app/ActionBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/google/android/apps/youtube/core/ui/g;->j:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->i:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/ui/g;->i:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->e()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->n:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->c()V

    return-void
.end method

.method public final b(Z)V
    .locals 4

    const/16 v2, 0x400

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->l:Z

    if-eq v0, p1, :cond_4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->o:Z

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/ui/g;->l:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->c()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->e()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    if-nez p1, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/ui/g;->o:Z

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->n:Z

    if-eqz v0, :cond_4

    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_1
.end method

.method public final c(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/ui/g;->m:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/g;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->c()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->e:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/ui/g;->q:Z

    return-void
.end method

.method public final e(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->s:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/ui/g;->s:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->c()V

    :cond_0
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onSystemUiVisibilityChange(I)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->g:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->getSystemUiVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->g:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setSystemUiVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->h:Lcom/google/android/apps/youtube/core/ui/h;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->k:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->s:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/g;->h:Lcom/google/android/apps/youtube/core/ui/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/ui/h;->f_()V

    :cond_1
    iput p1, p0, Lcom/google/android/apps/youtube/core/ui/g;->k:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/g;->b()V

    return-void
.end method
