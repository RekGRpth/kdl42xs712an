.class final Lcom/google/android/apps/youtube/core/player/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/notification/j;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/g;-><init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->p()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/ae;->q()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(ZZ)V

    return-void
.end method

.method public final a(J)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->c(I)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->i()V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->i()V

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)J

    move-result-wide v0

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->r()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->c(I)V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->s()V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->E()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopSelf()V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/g;->a:Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->j()V

    return-void
.end method
