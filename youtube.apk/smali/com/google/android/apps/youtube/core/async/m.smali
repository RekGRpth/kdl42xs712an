.class public Lcom/google/android/apps/youtube/core/async/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/fromguava/d;

.field private final b:Lcom/google/android/apps/youtube/common/cache/a;

.field private final c:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->c:Lcom/google/android/apps/youtube/core/async/af;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->a:Lcom/google/android/apps/youtube/common/fromguava/d;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/common/fromguava/d;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/fromguava/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->a:Lcom/google/android/apps/youtube/common/fromguava/d;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->c:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/m;)Lcom/google/android/apps/youtube/common/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->b:Lcom/google/android/apps/youtube/common/cache/a;

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/android/apps/youtube/common/fromguava/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->a:Lcom/google/android/apps/youtube/common/fromguava/d;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/m;->c:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/r;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/core/async/r;-><init>(Lcom/google/android/apps/youtube/core/async/m;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
