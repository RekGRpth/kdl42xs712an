.class public final Lcom/google/android/apps/youtube/core/client/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/ag;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->d(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->partnerId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->e(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->f(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isSkippable()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->g(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->f(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
