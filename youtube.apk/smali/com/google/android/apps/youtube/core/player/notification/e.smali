.class final Lcom/google/android/apps/youtube/core/player/notification/e;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/notification/j;

.field final synthetic b:Lcom/google/android/apps/youtube/core/player/notification/d;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/notification/d;Lcom/google/android/apps/youtube/core/player/notification/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/notification/e;->b:Lcom/google/android/apps/youtube/core/player/notification/d;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/notification/e;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string v0, "com.google.android.youtube.action.controller_notification_prev"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/e;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->e()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "com.google.android.youtube.action.controller_notification_play_pause"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/e;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->d()V

    goto :goto_0

    :cond_2
    const-string v0, "com.google.android.youtube.action.controller_notification_next"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/e;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->f()V

    goto :goto_0

    :cond_3
    const-string v0, "com.google.android.youtube.action.controller_notification_close"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/e;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->g()V

    goto :goto_0

    :cond_4
    const-string v0, "com.google.android.youtube.action.controller_notification_replay"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/e;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->h()V

    goto :goto_0
.end method
