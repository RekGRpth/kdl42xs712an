.class public abstract Lcom/google/android/apps/youtube/core/async/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/f;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/identity/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/l;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/x;->a:Lcom/google/android/apps/youtube/core/identity/l;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/youtube/core/async/w;Ljava/lang/Exception;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/x;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v1, :cond_0

    check-cast p2, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {p2}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x191

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/w;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/x;->a(Lcom/google/android/apps/youtube/core/async/w;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method
