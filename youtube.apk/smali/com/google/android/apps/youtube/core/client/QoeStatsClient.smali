.class public final Lcom/google/android/apps/youtube/core/client/QoeStatsClient;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/a/b;


# static fields
.field private static final a:Landroid/util/SparseIntArray;

.field private static final b:Landroid/util/SparseArray;


# instance fields
.field private final c:Landroid/net/Uri;

.field private final d:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Z

.field private final i:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

.field private final j:Lcom/google/android/apps/youtube/common/e/b;

.field private final k:Lcom/google/android/apps/youtube/common/network/h;

.field private final l:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

.field private final m:Lcom/google/android/apps/youtube/medialib/a/a;

.field private final n:Ljava/util/Map;

.field private o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

.field private p:I

.field private q:J

.field private r:J

.field private s:I

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    const/16 v1, -0x3ea

    const/16 v2, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    const/16 v1, -0x3eb

    const/16 v2, 0x136

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    const/16 v1, -0x3ec

    const/16 v2, 0x78

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    const/16 v1, -0x3ed

    const/16 v2, 0x137

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    const/16 v1, -0x3f2

    const/16 v2, 0x12d

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    const/16 v1, 0x21

    const/16 v2, 0x12e

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->a:I

    const/16 v2, 0x138

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->b:I

    const/16 v2, 0x12f

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->c:I

    const/16 v2, 0x130

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->d:I

    const/16 v2, 0x131

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->e:I

    const/16 v2, 0x132

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->f:I

    const/16 v2, 0x133

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->g:I

    const/16 v2, 0x134

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->h:I

    const/16 v2, 0x135

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->b:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "i"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->b:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "m"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->b:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "a"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/medialib/a/a;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;JZ)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->j:Lcom/google/android/apps/youtube/common/e/b;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->d:Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->k:Lcom/google/android/apps/youtube/common/network/h;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->l:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->m:Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c:Landroid/net/Uri;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->f:Ljava/lang/String;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->g:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "vps"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "vfs"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "error"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "error_info"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "bwm"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "df"

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v1, 0x0

    cmp-long v1, p10, v1

    if-gez v1, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->e:J

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "vps"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0.000:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    :goto_0
    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->h:Z

    invoke-interface {p1}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v1

    const-wide/16 v3, 0x7530

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->q:J

    return-void

    :cond_0
    iput-wide p10, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->e:J

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->PAUSED:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->p:I

    return p1
.end method

.method private a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v1, "vps"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    goto :goto_0
.end method

.method private c(Z)V
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->j:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v1

    if-nez p1, :cond_0

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->q:J

    cmp-long v0, v1, v3

    if-lez v0, :cond_3

    :cond_0
    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->r:J

    cmp-long v0, v3, v6

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->r:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->s:I

    int-to-float v3, v3

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v4, "bwm"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-wide v6, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->r:J

    iput v5, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->s:I

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->t:I

    if-lez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->t:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v4, "df"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput v5, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->t:I

    :cond_2
    const-wide/16 v3, 0x7530

    add-long v0, v1, v3

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->q:J

    :cond_3
    return-void
.end method

.method private i()Ljava/lang/String;
    .locals 8

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.3f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->j:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->e:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private j()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const-string v0, "No ping as there is nothing new to report"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v2

    const-string v0, "event"

    const-string v1, "streamingstats"

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "cpn"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "ns"

    const-string v3, "yt"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "docid"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "conn"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->k:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/common/network/h;->i()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/common/e/o;

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->p:I

    if-lez v0, :cond_3

    const-string v0, "fmt"

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->p:I

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/common/e/o;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->isLive()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "live"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->l:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->a(Lcom/google/android/apps/youtube/common/e/o;)Lcom/google/android/apps/youtube/common/e/o;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, ","

    invoke-static {v4, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ",:"

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Pinging "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->d:Lcom/google/android/apps/youtube/datalib/e/b;

    const-string v1, "qoe"

    const v2, 0x323467f

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->d:Lcom/google/android/apps/youtube/datalib/e/b;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/a/b;->a:Lcom/android/volley/n;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->ENDED:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->j()V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->h:Z

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->t:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->t:I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c(Z)V

    return-void
.end method

.method public final a(III)V
    .locals 3

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->p:I

    if-eq v0, p1, :cond_0

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-lez p2, :cond_2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->p:I

    if-lez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "vfs"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput p1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->p:I

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_2
.end method

.method public final a(IIILjava/lang/Object;)V
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->ERROR:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    const/16 v0, 0x6b

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v1

    if-ltz v1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    move v1, v0

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit16 v2, p3, 0x3e8

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v3, "error"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p4}, Lcom/google/android/apps/youtube/core/client/cb;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->n:Ljava/util/Map;

    const-string v2, "error_info"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->j()V

    return-void

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->r:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->r:J

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->s:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->s:I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c(Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->PAUSED:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->PAUSED_BUFFERING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->BUFFERING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->PAUSED_BUFFERING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->PAUSED:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->PLAYING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->j()V

    return-void
.end method

.method public final b(Z)V
    .locals 1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->SEEKING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->PAUSED:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    return-void
.end method

.method public final d()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->h:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->j()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->h:Z

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->PLAYING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    return-void
.end method

.method public final f()Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->i:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    iget v5, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->p:I

    iget-wide v6, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->e:J

    iget-boolean v8, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->h:Z

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;IJZ)V

    return-object v0
.end method

.method final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->m:Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/medialib/a/a;->a(Lcom/google/android/apps/youtube/medialib/a/b;)V

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->m:Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/medialib/a/a;->b(Lcom/google/android/apps/youtube/medialib/a/b;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->c(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->o:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    if-eq v0, v1, :cond_0

    const-string v0, "QoE client released unexpectedly"

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;->NOT_PLAYING:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$PlayerState;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->j()V

    return-void
.end method
