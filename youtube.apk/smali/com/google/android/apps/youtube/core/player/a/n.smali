.class public final Lcom/google/android/apps/youtube/core/player/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/protocol/HttpRequestHandler;


# instance fields
.field private final a:Ljava/security/Key;

.field private final b:Lcom/google/android/apps/youtube/common/e/b;

.field private c:Lcom/google/android/exoplayer/upstream/cache/a;

.field private d:Lcom/google/android/exoplayer/upstream/cache/a;


# direct methods
.method public constructor <init>(Ljava/security/Key;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/a/n;->a:Ljava/security/Key;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/n;->b:Lcom/google/android/apps/youtube/common/e/b;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/n;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/n;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/a/n;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/a/n;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    return-void
.end method

.method public final handle(Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 11

    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/http/MethodNotSupportedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Method is not supported: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/apache/http/HttpException;

    const-string v2, "Internal error while handling an exo request."

    invoke-direct {v1, v2, v0}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    :try_start_1
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "v"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "i"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v2, "l"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "e"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "m"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/a/n;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v7}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v7

    cmp-long v2, v2, v7

    if-gez v2, :cond_1

    const-string v0, "Offline URL has expired. Not allowed to access content."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    const/16 v0, 0x193

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    :goto_0
    return-void

    :cond_1
    const-string v2, "Range"

    invoke-interface {p1, v2}, Lorg/apache/http/HttpRequest;->getLastHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v7

    const-wide/16 v2, 0x0

    if-eqz v7, :cond_3

    const-string v8, "bytes=(\\d*)-(\\d*)"

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v8

    invoke-interface {v7}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sub-long/2addr v4, v2

    :cond_2
    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sub-long/2addr v4, v2

    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    :cond_3
    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/a/n;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    if-nez v8, :cond_4

    const/16 v0, 0x194

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_4
    :try_start_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    invoke-static {v1, v6, v0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/google/android/exoplayer/upstream/j;

    const/4 v1, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    new-instance v2, Lcom/google/android/exoplayer/upstream/FileDataSource;

    invoke-direct {v2}, Lcom/google/android/exoplayer/upstream/FileDataSource;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/n;->a:Ljava/security/Key;

    if-eqz v1, :cond_7

    new-instance v1, Lcom/google/android/exoplayer/upstream/a/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/a/n;->a:Ljava/security/Key;

    invoke-interface {v3}, Ljava/security/Key;->getEncoded()[B

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lcom/google/android/exoplayer/upstream/a/b;-><init>([BLcom/google/android/exoplayer/upstream/i;)V

    :goto_1
    new-instance v2, Lcom/google/android/apps/youtube/core/player/a/m;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/a/n;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/a/n;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/youtube/core/player/a/m;-><init>(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/i;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/player/a/f;

    const-string v3, "video/mp4"

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/youtube/core/player/a/f;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Ljava/lang/String;)V

    invoke-interface {p2, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    if-nez v7, :cond_5

    const/16 v0, 0xc8

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v1, "Exception while trying to construct a offline data source."

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0x1f4

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    :cond_5
    const/16 v0, 0xce

    :try_start_4
    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    goto/16 :goto_0

    :cond_6
    const/16 v0, 0x194

    invoke-interface {p2, v0}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    :cond_7
    move-object v1, v2

    goto :goto_1
.end method
