.class final Lcom/google/android/apps/youtube/core/player/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/s;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/s;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/t;->a:Lcom/google/android/apps/youtube/core/player/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/apps/youtube/core/player/t;->b:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    check-cast p1, Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t retrieve info card image from [uri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/t;->a:Lcom/google/android/apps/youtube/core/player/s;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/t;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/s;->a(Lcom/google/android/apps/youtube/core/player/s;ILandroid/graphics/Bitmap;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/t;->a:Lcom/google/android/apps/youtube/core/player/s;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/t;->b:I

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/youtube/core/player/s;->a(Lcom/google/android/apps/youtube/core/player/s;ILandroid/graphics/Bitmap;)V

    return-void
.end method
