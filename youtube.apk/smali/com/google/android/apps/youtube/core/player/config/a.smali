.class public final Lcom/google/android/apps/youtube/core/player/config/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig;


# static fields
.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/au;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/config/a;->a:Ljava/util/HashMap;

    const-string v1, "12"

    sget-object v2, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->SIMULATE_AD_LOAD:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/config/a;->a:Ljava/util/HashMap;

    const-string v1, "13"

    sget-object v2, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->PRECACHE_AD_METADATA:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/config/a;->a:Ljava/util/HashMap;

    const-string v1, "14"

    sget-object v2, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->PRECACHE_AD_STREAM:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/au;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/config/a;->b:Lcom/google/android/apps/youtube/core/au;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/config/a;->b:Lcom/google/android/apps/youtube/core/au;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/au;->E()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/config/a;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->NONE:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/core/player/config/a;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    goto :goto_0
.end method
