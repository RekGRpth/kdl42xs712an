.class public Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;
.super Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/e;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/state/e;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;[BLjava/lang/String;IILcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;Z)V
    .locals 8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;-><init>([Ljava/lang/String;[BLjava/lang/String;IILcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Z)V

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
