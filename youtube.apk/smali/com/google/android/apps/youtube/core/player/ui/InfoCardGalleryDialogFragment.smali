.class public Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/ui/h;


# instance fields
.field private Y:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

.field private Z:Lcom/google/android/apps/youtube/core/player/ui/c;

.field private aa:I

.field private ab:Landroid/content/res/Resources;

.field private ac:Landroid/support/v4/view/ViewPager;

.field private ad:Lcom/google/android/apps/youtube/core/player/ui/d;

.field private ae:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private E()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Z:Lcom/google/android/apps/youtube/core/player/ui/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;I)Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "infoCardCollection"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "selectedCardIndex"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->g(Landroid/os/Bundle;)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;)Lcom/google/android/apps/youtube/core/player/ui/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Z:Lcom/google/android/apps/youtube/core/player/ui/c;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;)Landroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ac:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ab:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/g;->N:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ad:Lcom/google/android/apps/youtube/core/player/ui/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ui/d;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ac:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz p1, :cond_0

    :goto_0
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ac:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->q()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->q()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void

    :cond_0
    mul-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/DialogFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->E()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v3

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ab:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/g;->O:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget v0, Lcom/google/android/youtube/l;->ae:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    sget v0, Lcom/google/android/youtube/j;->dd:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Y:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->setPageCount(I)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->setCurrentPage(I)V

    sget v1, Lcom/google/android/youtube/j;->gg:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ac:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ac:Landroid/support/v4/view/ViewPager;

    neg-int v5, v5

    invoke-virtual {v1, v5}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ac:Landroid/support/v4/view/ViewPager;

    new-instance v5, Lcom/google/android/apps/youtube/core/player/ui/e;

    invoke-direct {v5, p0, v0}, Lcom/google/android/apps/youtube/core/player/ui/e;-><init>(Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;)V

    invoke-virtual {v1, v5}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bx;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/ui/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Y:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/player/ui/d;-><init>(Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ad:Lcom/google/android/apps/youtube/core/player/ui/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ac:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ad:Lcom/google/android/apps/youtube/core/player/ui/d;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/aj;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ac:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->aa:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    sget v0, Lcom/google/android/youtube/j;->aw:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/ui/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/ui/a;-><init>(Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->au:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/ui/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/ui/b;-><init>(Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ae:Landroid/util/SparseArray;

    if-eqz v0, :cond_2

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ae:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ad:Lcom/google/android/apps/youtube/core/player/ui/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ae:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ae:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v5, v0}, Lcom/google/android/apps/youtube/core/player/ui/d;->a(ILandroid/graphics/Bitmap;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iput-object v3, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ae:Landroid/util/SparseArray;

    :cond_2
    move-object v0, v4

    goto/16 :goto_0
.end method

.method public final a(ILandroid/graphics/Bitmap;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->q()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ad:Lcom/google/android/apps/youtube/core/player/ui/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/ui/d;->a(ILandroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ae:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ae:Landroid/util/SparseArray;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ae:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->a(Landroid/app/Activity;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ab:Landroid/content/res/Resources;

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x2

    const v1, 0x1030010    # android.R.style.Theme_Translucent_NoTitleBar

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->a(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "infoCardCollection"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Y:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selectedCardIndex"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->aa:I

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/ui/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Z:Lcom/google/android/apps/youtube/core/player/ui/c;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/ui/f;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Z:Lcom/google/android/apps/youtube/core/player/ui/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Z:Lcom/google/android/apps/youtube/core/player/ui/c;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/ui/f;->b()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/ui/c;->b(I)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->ab:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->b(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Z:Lcom/google/android/apps/youtube/core/player/ui/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->Z:Lcom/google/android/apps/youtube/core/player/ui/c;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/ui/c;->a()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->q()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->b(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->r()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->E()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->a()V

    :cond_0
    return-void
.end method
