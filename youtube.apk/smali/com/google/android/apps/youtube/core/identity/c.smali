.class final Lcom/google/android/apps/youtube/core/identity/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/youtube/common/a/c;

.field final synthetic c:Lcom/google/android/apps/youtube/core/identity/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/b;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/c;->c:Lcom/google/android/apps/youtube/core/identity/b;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/c;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/c;->b:Lcom/google/android/apps/youtube/common/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/c;->c:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/b;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/c;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/c;->c:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/identity/b;->b(Lcom/google/android/apps/youtube/core/identity/b;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/auth/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/c;->c:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/b;Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/c;->b:Lcom/google/android/apps/youtube/common/a/c;

    const/4 v2, 0x0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/identity/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/common/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/c;->b:Lcom/google/android/apps/youtube/common/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a(Landroid/content/Intent;)Lcom/google/android/apps/youtube/core/identity/a;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/youtube/common/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/c;->b:Lcom/google/android/apps/youtube/common/a/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/a;->b(Ljava/lang/Exception;)Lcom/google/android/apps/youtube/core/identity/a;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/youtube/common/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/c;->b:Lcom/google/android/apps/youtube/common/a/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a(Ljava/lang/Exception;)Lcom/google/android/apps/youtube/core/identity/a;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/youtube/common/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
