.class final Lcom/google/android/apps/youtube/core/p;
.super Lcom/google/android/apps/youtube/common/e/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/p;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/common/e/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 9

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/p;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->au()Lcom/google/android/apps/youtube/core/au;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/apps/youtube/core/au;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Lcom/google/android/apps/youtube/core/au;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Lcom/google/android/apps/youtube/core/au;->E()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/p;->a:Lcom/google/android/apps/youtube/core/a;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/android/apps/youtube/core/client/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/p;->a:Lcom/google/android/apps/youtube/core/a;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/p;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/a;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "@"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v8}, Lcom/google/android/apps/youtube/core/au;->f()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v8}, Lcom/google/android/apps/youtube/core/au;->z()I

    move-result v6

    invoke-interface {v8}, Lcom/google/android/apps/youtube/core/au;->A()I

    move-result v7

    invoke-interface {v8}, Lcom/google/android/apps/youtube/core/au;->B()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/client/s;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/p;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/a;->a(Lcom/google/android/apps/youtube/core/client/l;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/core/client/az;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/client/az;-><init>()V

    goto :goto_1
.end method
