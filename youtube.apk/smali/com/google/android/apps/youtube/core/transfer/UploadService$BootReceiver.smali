.class public Lcom/google/android/apps/youtube/core/transfer/UploadService$BootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const-string v0, "boot completed, starting upload service"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
