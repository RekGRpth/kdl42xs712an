.class public final Lcom/google/android/apps/youtube/core/client/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/core/utils/a;

.field private final b:Lcom/google/android/apps/youtube/common/e/b;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/apps/youtube/core/client/ax;

.field private e:Ljava/lang/String;

.field private f:Z

.field private final g:Lcom/google/android/apps/youtube/common/network/h;

.field private h:Lcom/google/android/apps/youtube/datalib/offline/n;

.field private i:Lcom/google/android/apps/youtube/core/player/w;

.field private final j:Landroid/content/SharedPreferences;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/android/apps/youtube/core/client/cf;

.field private m:Lcom/google/android/apps/youtube/core/player/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->k:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->j:Landroid/content/SharedPreferences;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->g:Lcom/google/android/apps/youtube/common/network/h;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/i;->f:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/utils/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->a:Lcom/google/android/apps/youtube/core/utils/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->b:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/client/i;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/client/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->d:Lcom/google/android/apps/youtube/core/client/ax;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/client/i;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/client/i;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/i;->f:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->g:Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/datalib/offline/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->h:Lcom/google/android/apps/youtube/datalib/offline/n;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/player/w;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->i:Lcom/google/android/apps/youtube/core/player/w;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/core/client/i;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->j:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/core/client/i;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/client/cf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->l:Lcom/google/android/apps/youtube/core/client/cf;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/core/client/i;)Lcom/google/android/apps/youtube/core/player/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->m:Lcom/google/android/apps/youtube/core/player/ad;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/client/h;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->a:Lcom/google/android/apps/youtube/core/utils/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/client/h;-><init>(Lcom/google/android/apps/youtube/core/client/i;B)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/client/ax;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/i;->d:Lcom/google/android/apps/youtube/core/client/ax;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/client/cf;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/i;->l:Lcom/google/android/apps/youtube/core/client/cf;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/ad;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/i;->m:Lcom/google/android/apps/youtube/core/player/ad;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/w;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/i;->i:Lcom/google/android/apps/youtube/core/player/w;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/utils/a;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->a:Lcom/google/android/apps/youtube/core/utils/a;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/offline/n;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/i;->h:Lcom/google/android/apps/youtube/datalib/offline/n;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/i;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/youtube/core/client/i;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/client/i;->f:Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/i;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/i;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/i;->k:Ljava/lang/String;

    return-object p0
.end method
