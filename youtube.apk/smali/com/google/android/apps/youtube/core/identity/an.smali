.class final Lcom/google/android/apps/youtube/core/identity/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Landroid/app/Activity;

.field final synthetic d:Lcom/google/android/apps/youtube/core/identity/al;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/al;Ljava/util/List;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/an;->d:Lcom/google/android/apps/youtube/core/identity/al;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/an;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/an;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/identity/an;->c:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    check-cast p1, Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/an;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->plusUserId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/an;->d:Lcom/google/android/apps/youtube/core/identity/al;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/an;->d:Lcom/google/android/apps/youtube/core/identity/al;

    new-instance v2, Lcom/google/android/apps/youtube/core/identity/f;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/identity/an;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/youtube/core/identity/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Lcom/google/android/apps/youtube/core/identity/f;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/an;->c:Landroid/app/Activity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/an;->d:Lcom/google/android/apps/youtube/core/identity/al;

    const-string v1, "No +Page Delegate"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/an;->d:Lcom/google/android/apps/youtube/core/identity/al;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/f;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/an;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/identity/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Lcom/google/android/apps/youtube/core/identity/f;)V

    goto :goto_0
.end method
