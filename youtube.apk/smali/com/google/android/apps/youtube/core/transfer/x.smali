.class final Lcom/google/android/apps/youtube/core/transfer/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Landroid/util/Pair;

.field private j:Ljava/lang/String;

.field private k:Lcom/google/android/apps/youtube/datalib/model/transfer/a;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->k:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/x;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/x;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->a:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/x;Landroid/util/Pair;)Landroid/util/Pair;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->i:Landroid/util/Pair;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/x;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->d:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/x;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)Lcom/google/android/apps/youtube/datalib/model/transfer/a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->k:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/transfer/x;)Lcom/google/android/apps/youtube/datalib/model/transfer/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->k:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->e:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->f:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/transfer/x;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->d:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->h:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/x;->j:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/core/transfer/x;)Landroid/util/Pair;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->i:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/core/transfer/x;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/x;->a:Landroid/net/Uri;

    return-object v0
.end method
