.class public abstract Lcom/google/android/apps/youtube/core/async/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/cache/a;

.field private final b:Lcom/google/android/apps/youtube/core/async/af;

.field private final c:Lcom/google/android/apps/youtube/common/e/b;

.field private final d:J


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/common/e/b;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/an;->a:Lcom/google/android/apps/youtube/common/cache/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/async/an;->b:Lcom/google/android/apps/youtube/core/async/af;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/async/an;->c:Lcom/google/android/apps/youtube/common/e/b;

    iput-wide p4, p0, Lcom/google/android/apps/youtube/core/async/an;->d:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/an;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/an;->c:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/common/e/b;J)Lcom/google/android/apps/youtube/core/async/an;
    .locals 6

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x9a7ec800L

    cmp-long v0, p3, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "time to live must be >=0 and <= 2592000000"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/async/ap;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/async/ap;-><init>(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/common/e/b;J)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/async/an;)Lcom/google/android/apps/youtube/common/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/an;->a:Lcom/google/android/apps/youtube/common/cache/a;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public final a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 7

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/async/an;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/an;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/async/an;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/Timestamped;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/an;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v1

    if-eqz v0, :cond_0

    iget-wide v3, v0, Lcom/google/android/apps/youtube/core/async/Timestamped;->timestamp:J

    cmp-long v3, v1, v3

    if-ltz v3, :cond_0

    iget-wide v3, v0, Lcom/google/android/apps/youtube/core/async/Timestamped;->timestamp:J

    iget-wide v5, p0, Lcom/google/android/apps/youtube/core/async/an;->d:J

    add-long/2addr v3, v5

    cmp-long v1, v3, v1

    if-ltz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/async/Timestamped;->element:Ljava/lang/Object;

    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/an;->b:Lcom/google/android/apps/youtube/core/async/af;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/an;->b:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/ao;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/core/async/ao;-><init>(Lcom/google/android/apps/youtube/core/async/an;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/core/async/NotFoundException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/async/NotFoundException;-><init>()V

    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
