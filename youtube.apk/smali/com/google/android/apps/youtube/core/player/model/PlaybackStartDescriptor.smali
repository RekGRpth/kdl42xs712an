.class public Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final NO_PLAYLIST_ID:Ljava/lang/String; = ""

.field public static final NO_PLAYLIST_INDEX:I = -0x1

.field public static final NO_VIDEO_ID:Ljava/lang/String; = ""

.field public static final NO_VIDEO_START_TIME:I


# instance fields
.field private final localProto:Lcom/google/android/apps/youtube/a/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/model/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/a/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v2, v0, Lcom/google/a/a/a/a/wb;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v2, v0, Lcom/google/a/a/a/a/wb;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget v2, v0, Lcom/google/a/a/a/a/wb;->d:I

    iget-object v3, v0, Lcom/google/a/a/a/a/wb;->c:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->valueOrMissingPlaylistIndex(ILjava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->a(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v2, v0, Lcom/google/a/a/a/a/wb;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v2, v0, Lcom/google/a/a/a/a/wb;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/wb;->g:Z

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/d;->b(Z)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/a/a/d;->a(Z)Lcom/google/android/apps/youtube/a/a/d;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->b:[B

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->b:[B

    invoke-static {v0}, Lcom/google/protobuf/micro/a;->a([B)Lcom/google/protobuf/micro/a;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/d;->a(Lcom/google/protobuf/micro/a;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->b(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/a/a/d;->c(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/a/a/d;->c(Z)Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->validate()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->u:Lcom/google/a/a/a/a/wl;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->u:Lcom/google/a/a/a/a/wl;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v2, v0, Lcom/google/a/a/a/a/wl;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget v2, v0, Lcom/google/a/a/a/a/wl;->c:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->a(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, v0, Lcom/google/a/a/a/a/wl;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/a/a/d;->b(Z)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/a/a/d;->a(Z)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->w:Lcom/google/a/a/a/a/me;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->w:Lcom/google/a/a/a/a/me;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v2, v0, Lcom/google/a/a/a/a/me;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v2, v0, Lcom/google/a/a/a/a/me;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget v2, v0, Lcom/google/a/a/a/a/me;->d:I

    iget-object v3, v0, Lcom/google/a/a/a/a/me;->c:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->valueOrMissingPlaylistIndex(ILjava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/a/a/d;->a(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, v0, Lcom/google/a/a/a/a/me;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/a/a/d;->b(Z)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/a/a/d;->a(Z)Lcom/google/android/apps/youtube/a/a/d;

    goto/16 :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Navigation endpoint does not contain watch data"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    sget-object v0, Lcom/google/protobuf/micro/a;->a:Lcom/google/protobuf/micro/a;

    goto/16 :goto_1
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/a/a/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/a/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/d;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/a/a/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/a/a/d;->a(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {p5}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->b(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/youtube/a/a/d;->c(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    sget-object v1, Lcom/google/protobuf/micro/a;->a:Lcom/google/protobuf/micro/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->a(Lcom/google/protobuf/micro/a;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/a/a/d;->b(Z)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/a/a/d;->a(Z)Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->validate()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void

    :cond_0
    const-string p1, ""

    goto :goto_0

    :cond_1
    const-string p2, ""

    goto :goto_1
.end method

.method public constructor <init>(Ljava/util/List;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/a/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/a/a/d;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/a/a/d;->a(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/a/a/d;->c(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {p4}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->b(I)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    sget-object v1, Lcom/google/protobuf/micro/a;->a:Lcom/google/protobuf/micro/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/d;->a(Lcom/google/protobuf/micro/a;)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/a/a/d;->b(Z)Lcom/google/android/apps/youtube/a/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/a/a/d;->a(Z)Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->validate()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void
.end method

.method private valueOrMissingPlaylistIndex(ILjava/lang/String;)I
    .locals 1

    if-nez p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, -0x1

    :cond_0
    return p1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getClickTrackingParams()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->i()Lcom/google/protobuf/micro/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/micro/a;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public getParams()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlaybackStartDescriptorProto()Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    return-object v0
.end method

.method public getPlayerParams()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlaylistId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlaylistIndex()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->g()I

    move-result v0

    return v0
.end method

.method public getVideoId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoIds()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->c()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->b()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoStartTime()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->n()I

    move-result v0

    return v0
.end method

.method public getWatchFeature()Lcom/google/android/apps/youtube/core/client/WatchFeature;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->m()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->fromOrdinal(I)Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v0

    return-object v0
.end method

.method public isFromRemoteQueue()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "RQ"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInnerTube()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->l()Z

    move-result v0

    return v0
.end method

.method public isOffline()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->j()Z

    move-result v0

    return v0
.end method

.method public isScriptedPlay()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->r()Z

    move-result v0

    return v0
.end method

.method public isWatchNextDisabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->q()Z

    move-result v0

    return v0
.end method

.method public setOffline(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/d;->a(Z)Lcom/google/android/apps/youtube/a/a/d;

    return-void
.end method

.method public setScriptedPlay(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/d;->f(Z)Lcom/google/android/apps/youtube/a/a/d;

    return-void
.end method

.method public setStartPaused(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/d;->d(Z)Lcom/google/android/apps/youtube/a/a/d;

    return-void
.end method

.method public setWatchNextDisabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/d;->e(Z)Lcom/google/android/apps/youtube/a/a/d;

    return-void
.end method

.method public shouldContinuePlayback()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->k()Z

    move-result v0

    return v0
.end method

.method public shouldStartPaused()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/d;->p()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v1, "PlaybackStartDescriptor:\n  VideoId:%s\n  PlaylistId:%s\n  Index:%d\n  VideoIds:%s"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method validate()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid PlaybackStartDescriptor\n"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaybackStartDescriptorProto()Lcom/google/android/apps/youtube/a/a/d;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/micro/c;)V

    return-void
.end method
