.class final Lcom/google/android/apps/youtube/core/player/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;

.field private b:Ljava/lang/String;

.field private volatile c:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

.field private volatile d:I

.field private volatile e:Z

.field private volatile f:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/i;->a:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;->USER_PLAY:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->f:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/i;-><init>(Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/i;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/i;->d:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/i;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->c:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->AUTOPLAY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/i;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/i;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->f:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;->SCRIPTED_PLAY:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/i;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method private handlePlaybackScriptedOperationEvent(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/core/player/h;->c:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;->SCRIPTED_NAVIGATION_PENDING:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->f:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;->SCRIPTED_PLAY:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->f:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleSequenceChangedEvent(Lcom/google/android/apps/youtube/core/player/event/s;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/s;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->b:Ljava/lang/String;

    return-void
.end method

.method private handleSequencerNavigationRequestEvent(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/h;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/i;->c:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->f:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;->SCRIPTED_NAVIGATION_PENDING:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;->SCRIPTED_PLAY:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->f:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    :goto_1
    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/i;->e:Z

    goto :goto_0

    :pswitch_1
    iput v2, p0, Lcom/google/android/apps/youtube/core/player/i;->d:I

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/i;->e:Z

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;->USER_PLAY:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/i;->f:Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor$ScriptedPlayState;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/h;->b:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/i;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/i;->d:I

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/i;->e:Z

    goto :goto_0

    :pswitch_1
    iput v2, p0, Lcom/google/android/apps/youtube/core/player/i;->d:I

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/i;->e:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
