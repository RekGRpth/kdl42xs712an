.class final Lcom/google/android/apps/youtube/core/converter/http/gl;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V
    .locals 4

    const-class v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;-><init>()V

    const-string v2, "type"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->mimeType(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    const-string v2, "url"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    const-string v2, "yt:format"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x1

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/e/m;->b(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->legacyGdataFormat(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->addStream(Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    return-void
.end method
