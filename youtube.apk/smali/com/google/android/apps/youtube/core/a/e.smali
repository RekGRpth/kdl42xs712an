.class public abstract Lcom/google/android/apps/youtube/core/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/apps/youtube/core/a/g;


# instance fields
.field private b:Lcom/google/android/apps/youtube/core/a/f;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/a/g;

    const-string v1, "IGNORE_VIEW_TYPE"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/a/e;->a:Lcom/google/android/apps/youtube/core/a/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/a/e;->c:Z

    return-void
.end method


# virtual methods
.method public abstract a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract a(I)Ljava/lang/Object;
.end method

.method final a(Lcom/google/android/apps/youtube/core/a/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/a/e;->b:Lcom/google/android/apps/youtube/core/a/f;

    return-void
.end method

.method protected abstract a(Ljava/util/Set;)V
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/a/e;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/a/e;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/a/e;->c:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/e;->f()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract b()I
.end method

.method public b(I)Z
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/e;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " out of range 0.."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/e;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract e(I)Lcom/google/android/apps/youtube/core/a/g;
.end method

.method public f(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method protected final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a/e;->b:Lcom/google/android/apps/youtube/core/a/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a/e;->b:Lcom/google/android/apps/youtube/core/a/f;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/core/a/f;->a(Lcom/google/android/apps/youtube/core/a/e;)V

    :cond_0
    return-void
.end method

.method final g()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/a/e;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a/e;->b()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
