.class final Lcom/google/android/apps/youtube/core/converter/http/ep;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 4

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    const-string v1, "event"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, "Custom tracking tag missing event attribute - ignoring"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    :goto_1
    const-string v3, "skip"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Badly formed custom tracking uri - ignoring"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string v3, "engagedView"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->h(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_4
    const-string v3, "skipShown"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_5
    const-string v3, "videoTitleClicked"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->p(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_6
    const-string v3, "vast2tracking"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
