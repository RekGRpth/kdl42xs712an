.class public final Lcom/google/android/apps/youtube/core/converter/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/converter/d;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/converter/n;

.field private final b:Lcom/google/android/apps/youtube/core/converter/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/http/dj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/b;->a:Lcom/google/android/apps/youtube/core/converter/n;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/f;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/f;-><init>()V

    invoke-static {v0, p2, p3}, Lcom/google/android/apps/youtube/core/converter/http/gt;->a(Lcom/google/android/apps/youtube/core/converter/f;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/http/dj;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/converter/f;->a()Lcom/google/android/apps/youtube/core/converter/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/b;->b:Lcom/google/android/apps/youtube/core/converter/e;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;
    .locals 4

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVmapXml()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/b;->a:Lcom/google/android/apps/youtube/core/converter/n;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const-string v3, "UTF-8"

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/b;->b:Lcom/google/android/apps/youtube/core/converter/e;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/core/converter/n;->a(Ljava/io/InputStream;Lcom/google/android/apps/youtube/core/converter/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic a_(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/converter/b;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    move-result-object v0

    return-object v0
.end method
