.class final Lcom/google/android/apps/youtube/core/offline/store/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "itag"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "format_stream_proto"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "duration_millis"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "audio_only"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "bytes_transferred"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bytes_total"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/offline/store/u;->a:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/u;->b:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method private static a(Landroid/database/Cursor;Lcom/google/android/apps/youtube/core/offline/store/t;)Ljava/util/List;
    .locals 17

    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    const-string v1, "video_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    const-string v1, "itag"

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    const-string v1, "format_stream_proto"

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    const-string v1, "duration_millis"

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    const-string v1, "audio_only"

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v13

    const-string v1, "bytes_total"

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    const-string v1, "bytes_transferred"

    move-object/from16 v0, p0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    :goto_0
    invoke-interface/range {p0 .. p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    :try_start_0
    new-instance v16, Lcom/google/a/a/a/a/fj;

    invoke-direct/range {v16 .. v16}, Lcom/google/a/a/a/a/fj;-><init>()V

    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    move-object/from16 v0, v16

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    if-eqz p1, :cond_0

    move-object/from16 v0, v16

    iget-wide v6, v0, Lcom/google/a/a/a/a/fj;->l:J

    move-object/from16 v1, p1

    invoke-interface/range {v1 .. v7}, Lcom/google/android/apps/youtube/core/offline/store/t;->a(Ljava/lang/String;IJJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    iput-object v1, v0, Lcom/google/a/a/a/a/fj;->c:Ljava/lang/String;

    :cond_0
    move-object/from16 v0, p0

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-object/from16 v0, v16

    invoke-direct {v5, v0, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;-><init>(Lcom/google/a/a/a/a/fj;Ljava/lang/String;J)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v13, v3}, Lcom/google/android/apps/youtube/core/utils/i;->a(Landroid/database/Cursor;IZ)Z

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct {v1, v5, v3, v6, v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;ZJ)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error reading stream for video "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    return-object v8
.end method


# virtual methods
.method final a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;
    .locals 9

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/u;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "streams"

    sget-object v2, Lcom/google/android/apps/youtube/core/offline/store/u;->a:[Ljava/lang/String;

    const-string v3, "video_id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/core/offline/store/u;->a(Landroid/database/Cursor;Lcom/google/android/apps/youtube/core/offline/store/t;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;-><init>(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method final a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)V
    .locals 5

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)V

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "video_id"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "itag"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "format_stream_proto"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getFormatStreamProto()Lcom/google/a/a/a/a/fj;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v2, "duration_millis"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getVideoDurationMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "audio_only"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->d()Z

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/utils/i;->a(Z)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "bytes_total"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->f()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "bytes_transferred"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->e()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/u;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "streams"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/u;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "streams"

    const-string v2, "video_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method final a(Ljava/lang/String;I)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/u;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "streams"

    const-string v2, "video_id = ? AND itag = ?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    new-instance v2, Landroid/database/SQLException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Delete stream affected "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rows"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    return-void
.end method

.method final a(Ljava/lang/String;IJ)V
    .locals 7

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "bytes_transferred"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/u;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "streams"

    const-string v3, "video_id = ? AND itag = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    new-instance v2, Landroid/database/SQLException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Update stream bytes_transferred affected "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " rows"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    return-void
.end method
