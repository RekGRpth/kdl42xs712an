.class final Lcom/google/android/apps/youtube/core/converter/http/af;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/converter/http/ae;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/converter/http/af;->a:Lcom/google/android/apps/youtube/core/converter/http/ae;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 3

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->target(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    const-string v1, "display"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->displayUsername(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    :cond_0
    return-void
.end method
