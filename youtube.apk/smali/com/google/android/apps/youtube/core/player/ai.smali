.class final Lcom/google/android/apps/youtube/core/player/ai;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/ae;

.field private b:Landroid/os/Handler;

.field private c:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/ai;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/ae;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/ai;-><init>(Lcom/google/android/apps/youtube/core/player/ae;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->b:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->b:Landroid/os/Handler;

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->c:Z

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ai;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/ae;->e(Lcom/google/android/apps/youtube/core/player/ae;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->c:Z

    :cond_1
    return-void
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ae;->e(Lcom/google/android/apps/youtube/core/player/ae;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->c:Z

    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ae;->d(Lcom/google/android/apps/youtube/core/player/ae;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ae;->d(Lcom/google/android/apps/youtube/core/player/ae;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/aj;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/aj;-><init>(Lcom/google/android/apps/youtube/core/player/ai;)V

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ai;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method
