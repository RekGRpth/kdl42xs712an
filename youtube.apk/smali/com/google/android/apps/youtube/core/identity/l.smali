.class public final Lcom/google/android/apps/youtube/core/identity/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private volatile b:Lcom/google/android/apps/youtube/core/identity/f;

.field private volatile c:Lcom/google/android/apps/youtube/core/identity/z;

.field private volatile d:Z


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->a:Landroid/content/SharedPreferences;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/core/identity/f;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->a:Landroid/content/SharedPreferences;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/l;)Lcom/google/android/apps/youtube/core/identity/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    return-object v0
.end method

.method public static a()Lcom/google/android/apps/youtube/core/identity/l;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/identity/l;

    sget-object v1, Lcom/google/android/apps/youtube/core/identity/f;->a:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/identity/l;-><init>(Lcom/google/android/apps/youtube/core/identity/f;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/z;)Lcom/google/android/apps/youtube/core/identity/z;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/l;->c:Lcom/google/android/apps/youtube/core/identity/z;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/identity/l;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->a:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/identity/l;)Lcom/google/android/apps/youtube/core/identity/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->c:Lcom/google/android/apps/youtube/core/identity/z;

    return-object v0
.end method

.method private declared-synchronized i()V
    .locals 6

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/l;->a:Landroid/content/SharedPreferences;

    const-string v2, "user_account"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/l;->a:Landroid/content/SharedPreferences;

    const-string v3, "user_identity"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "No +Page Delegate"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v1, v0

    :cond_1
    new-instance v3, Lcom/google/android/apps/youtube/core/identity/f;

    invoke-direct {v3, v2, v1}, Lcom/google/android/apps/youtube/core/identity/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/l;->a:Landroid/content/SharedPreferences;

    const-string v2, "profile_display_name"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "profile_display_email"

    const-string v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "profile_thumbnail_uri"

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    :goto_1
    new-instance v1, Lcom/google/android/apps/youtube/core/identity/z;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/youtube/core/identity/z;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/identity/l;->c:Lcom/google/android/apps/youtube/core/identity/z;

    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/apps/youtube/core/identity/f;->a:Lcom/google/android/apps/youtube/core/identity/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/l;->i()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/m;

    invoke-direct {v1, p0, v0, p2}, Lcom/google/android/apps/youtube/core/identity/m;-><init>(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/c/a;)V

    invoke-interface {p1, v1}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/core/identity/z;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/z;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->c:Lcom/google/android/apps/youtube/core/identity/z;

    return-void
.end method

.method public final b()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/l;->i()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    sget-object v1, Lcom/google/android/apps/youtube/core/identity/f;->a:Lcom/google/android/apps/youtube/core/identity/f;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/l;->i()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/f;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/youtube/core/identity/z;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/l;->i()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->c:Lcom/google/android/apps/youtube/core/identity/z;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/l;->i()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/f;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final f()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/l;->i()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/f;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final g()Lcom/google/android/apps/youtube/core/identity/f;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/l;->i()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    return-object v0
.end method

.method final h()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->d:Z

    sget-object v0, Lcom/google/android/apps/youtube/core/identity/f;->a:Lcom/google/android/apps/youtube/core/identity/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->b:Lcom/google/android/apps/youtube/core/identity/f;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/l;->c:Lcom/google/android/apps/youtube/core/identity/z;

    return-void
.end method
