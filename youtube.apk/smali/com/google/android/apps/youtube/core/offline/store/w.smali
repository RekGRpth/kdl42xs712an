.class final Lcom/google/android/apps/youtube/core/offline/store/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I


# direct methods
.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    const-string v0, "video_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->b:I

    const-string v0, "language_code"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->c:I

    const-string v0, "source_language_code"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->d:I

    const-string v0, "language_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->e:I

    const-string v0, "track_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->f:I

    const-string v0, "format"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->g:I

    const-string v0, "subtitles_path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->h:I

    const-string v0, "track_vss_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->i:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/offline/store/w;-><init>(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/offline/store/w;)Ljava/util/List;
    .locals 9

    new-instance v7, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    iget v2, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->e:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->f:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    iget v4, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->b:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    iget v5, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->g:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    iget v6, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->h:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->a:Landroid/database/Cursor;

    iget v8, p0, Lcom/google/android/apps/youtube/core/offline/store/w;->i:I

    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->createOffline(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v7
.end method
