.class public final Lcom/google/android/apps/youtube/core/client/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Lorg/apache/http/client/HttpClient;

.field private final c:Lcom/google/android/apps/youtube/core/converter/n;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;

.field private final e:Lcom/google/android/apps/youtube/common/c/a;

.field private f:Lcom/google/android/apps/youtube/core/player/fetcher/d;

.field private g:Ljava/util/List;

.field private h:Lcom/google/android/apps/youtube/core/async/af;

.field private i:Lcom/google/android/apps/youtube/core/async/af;

.field private j:Lcom/google/android/apps/youtube/core/converter/http/b;

.field private k:Lcom/google/android/apps/youtube/core/client/h;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->a:Ljava/util/concurrent/Executor;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->b:Lorg/apache/http/client/HttpClient;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->c:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->e:Lcom/google/android/apps/youtube/common/c/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->d:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/client/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->k:Lcom/google/android/apps/youtube/core/client/h;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/player/fetcher/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->f:Lcom/google/android/apps/youtube/core/player/fetcher/d;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/client/r;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/client/r;)Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->b:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/converter/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->c:Lcom/google/android/apps/youtube/core/converter/n;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->h:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/converter/http/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->j:Lcom/google/android/apps/youtube/core/converter/http/b;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->i:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->e:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/client/q;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->h:Lcom/google/android/apps/youtube/core/async/af;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->j:Lcom/google/android/apps/youtube/core/converter/http/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/r;->k:Lcom/google/android/apps/youtube/core/client/h;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/r;->g:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/http/b;-><init>(Lcom/google/android/apps/youtube/core/client/h;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->j:Lcom/google/android/apps/youtube/core/converter/http/b;

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/client/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/client/q;-><init>(Lcom/google/android/apps/youtube/core/client/r;B)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/client/r;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/r;->h:Lcom/google/android/apps/youtube/core/async/af;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/client/h;)Lcom/google/android/apps/youtube/core/client/r;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/r;->k:Lcom/google/android/apps/youtube/core/client/h;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/fetcher/d;)Lcom/google/android/apps/youtube/core/client/r;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->f:Lcom/google/android/apps/youtube/core/player/fetcher/d;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/a/p;)Lcom/google/android/apps/youtube/core/client/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->g:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->g:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/r;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
