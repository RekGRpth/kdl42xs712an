.class public final Lcom/google/android/apps/youtube/core/player/DirectorSavedState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field public final adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

.field public final adStartPositionMillis:I

.field public final canToggleFullscreen:Z

.field public final contentVideoState:Lcom/google/android/apps/youtube/core/player/r;

.field public final feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public final isInAudioOnlyMode:Z

.field public final isInBackgroundMode:Z

.field public final playbackClientManagerState:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

.field public final playbackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field public final streamSelectionHint:I

.field public final userInitiatedPlayback:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/q;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/q;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/youtube/core/player/r;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-ne v3, v1, :cond_1

    move v3, v1

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-direct {v5, v0, v3, v6}, Lcom/google/android/apps/youtube/core/player/r;-><init>(ZZI)V

    iput-object v5, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->contentVideoState:Lcom/google/android/apps/youtube/core/player/r;

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackClientManagerState:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->fromOrdinal(I)Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->streamSelectionHint:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->canToggleFullscreen:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->userInitiatedPlayback:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->isInBackgroundMode:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->isInAudioOnlyMode:Z

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adStartPositionMillis:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/r;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/core/client/WatchFeature;IZZZZLcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->contentVideoState:Lcom/google/android/apps/youtube/core/player/r;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackClientManagerState:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iput p5, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->streamSelectionHint:I

    iput-boolean p6, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->canToggleFullscreen:Z

    iput-boolean p7, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->userInitiatedPlayback:Z

    iput-boolean p8, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->isInBackgroundMode:Z

    iput-boolean p9, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->isInAudioOnlyMode:Z

    iput-object p10, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iput-object p11, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput p12, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adStartPositionMillis:I

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DirectorSavedState{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " contentVideoState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->contentVideoState:Lcom/google/android/apps/youtube/core/player/r;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " playbackClientManagerState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackClientManagerState:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " playbackPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " feature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " streamSelectionHint="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->streamSelectionHint:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " canToggleFullscreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->canToggleFullscreen:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " userInitiatedPlayback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->userInitiatedPlayback:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isInBackgroundMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->isInBackgroundMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isInAudioOnlyMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->isInAudioOnlyMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adBreak="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ad="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adStartPositionMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adStartPositionMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->contentVideoState:Lcom/google/android/apps/youtube/core/player/r;

    iget-boolean v0, v3, Lcom/google/android/apps/youtube/core/player/r;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, v3, Lcom/google/android/apps/youtube/core/player/r;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, v3, Lcom/google/android/apps/youtube/core/player/r;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackClientManagerState:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->streamSelectionHint:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->canToggleFullscreen:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->userInitiatedPlayback:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->isInBackgroundMode:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->isInAudioOnlyMode:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adStartPositionMillis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method
