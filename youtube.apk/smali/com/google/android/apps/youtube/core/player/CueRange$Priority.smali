.class public final enum Lcom/google/android/apps/youtube/core/player/CueRange$Priority;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

.field public static final enum AD_MODULE:Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

.field public static final enum DEFAULT:Lcom/google/android/apps/youtube/core/player/CueRange$Priority;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;->DEFAULT:Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    const-string v1, "AD_MODULE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;->AD_MODULE:Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;->DEFAULT:Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;->AD_MODULE:Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;->$VALUES:[Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/CueRange$Priority;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/CueRange$Priority;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/CueRange$Priority;->$VALUES:[Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/CueRange$Priority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/CueRange$Priority;

    return-object v0
.end method
