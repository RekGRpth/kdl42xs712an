.class public final Lcom/google/android/apps/youtube/core/offline/store/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

.field public final e:J

.field public final f:I

.field public final g:I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;JII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/g;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/g;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/offline/store/g;->c:Ljava/lang/String;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/g;->d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    iput-wide p5, p0, Lcom/google/android/apps/youtube/core/offline/store/g;->e:J

    iput p7, p0, Lcom/google/android/apps/youtube/core/offline/store/g;->f:I

    iput p8, p0, Lcom/google/android/apps/youtube/core/offline/store/g;->g:I

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Landroid/database/Cursor;)Lcom/google/android/apps/youtube/core/offline/store/g;
    .locals 9

    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "original_video_id"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    const-string v0, "ad_break_id"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    const-string v0, "ad_video_id"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    const-string v0, "vast_type"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    const-string v0, "expiry_timestamp"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    const-string v0, "asset_frequency_cap"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    const-string v0, "vast_playback_count"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/g;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->fromValue(I)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    move-result-object v4

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/offline/store/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;JII)V

    goto :goto_0
.end method
