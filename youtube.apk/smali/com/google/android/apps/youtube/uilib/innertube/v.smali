.class public final Lcom/google/android/apps/youtube/uilib/innertube/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/uilib/a/h;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

.field private c:Lcom/google/android/apps/youtube/uilib/innertube/u;

.field private d:Landroid/view/View$OnClickListener;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/uilib/a/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->e:Z

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/u;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/innertube/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/w;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/uilib/innertube/w;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/v;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->d:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private a()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->e:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->e()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->j()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/u;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/u;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->e()Lcom/google/a/a/a/a/kz;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/u;->a(Lcom/google/a/a/a/a/kz;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/uilib/innertube/v;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/v;->a()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/ar;)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->h()I

    move-result v1

    if-gt v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/v;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->e:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->g()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->h()I

    move-result v2

    invoke-interface {v1, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->i()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->i()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/u;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/u;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/u;->a(Lcom/google/a/a/a/a/kz;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/v;->c:Lcom/google/android/apps/youtube/uilib/innertube/u;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
