.class public final Lcom/google/android/apps/youtube/uilib/innertube/t;
.super Lcom/google/android/apps/youtube/uilib/innertube/h;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/google/android/apps/youtube/uilib/innertube/q;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/uilib/innertube/i;

.field private final b:Lcom/google/android/apps/youtube/core/aw;

.field private final c:Landroid/widget/ListView;

.field private final d:Lcom/google/android/apps/youtube/uilib/innertube/p;

.field private final e:Lcom/google/android/apps/youtube/common/c/a;

.field private final f:Lcom/google/android/apps/youtube/uilib/a/b;

.field private final g:Lcom/google/android/apps/youtube/uilib/innertube/j;

.field private h:Landroid/widget/AbsListView$OnScrollListener;

.field private i:Lcom/google/android/apps/youtube/uilib/a/h;

.field private j:Lcom/google/android/apps/youtube/uilib/innertube/o;

.field private k:Z

.field private l:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/uilib/innertube/i;Landroid/widget/ListView;Lcom/google/android/apps/youtube/uilib/innertube/p;Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/uilib/innertube/j;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/c/a;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p4, p5, v0, p7}, Lcom/google/android/apps/youtube/uilib/innertube/h;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Ljava/lang/Object;Lcom/google/android/apps/youtube/core/aw;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->a:Lcom/google/android/apps/youtube/uilib/innertube/i;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    iput-object p3, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->e:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/innertube/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->g:Lcom/google/android/apps/youtube/uilib/innertube/j;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->b:Lcom/google/android/apps/youtube/core/aw;

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->l:I

    return-void
.end method

.method private c(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V
    .locals 10

    const/4 v7, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/b;->a()I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/uilib/a/b;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/b;->a(I)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b()Lcom/google/a/a/a/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Lcom/google/a/a/a/a/dp;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/b;->getViewTypeCount()I

    move-result v8

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_3
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    instance-of v0, v6, Lcom/google/android/apps/youtube/datalib/innertube/model/o;

    if-eqz v0, :cond_4

    new-instance v1, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->g:Lcom/google/android/apps/youtube/uilib/innertube/j;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/j;->a(Lcom/google/android/apps/youtube/uilib/a/h;)V

    new-instance v2, Lcom/google/android/apps/youtube/uilib/innertube/k;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->h()Lcom/google/android/apps/youtube/datalib/innertube/i;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->e:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/google/android/apps/youtube/uilib/innertube/k;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    move-object v0, v6

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/uilib/innertube/k;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/o;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/e;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;)V

    goto :goto_1

    :cond_4
    instance-of v0, v6, Lcom/google/android/apps/youtube/datalib/innertube/model/p;

    if-eqz v0, :cond_5

    new-instance v2, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->g:Lcom/google/android/apps/youtube/uilib/innertube/j;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/uilib/innertube/j;->b(Lcom/google/android/apps/youtube/uilib/a/h;)V

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/m;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->h()Lcom/google/android/apps/youtube/datalib/innertube/i;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->a:Lcom/google/android/apps/youtube/uilib/innertube/i;

    iget-object v4, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->e:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v5, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/uilib/innertube/m;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/uilib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    check-cast v6, Lcom/google/android/apps/youtube/datalib/innertube/model/p;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/uilib/innertube/m;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/p;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/uilib/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/e;)V

    goto :goto_1

    :cond_5
    instance-of v0, v6, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->g:Lcom/google/android/apps/youtube/uilib/innertube/j;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/uilib/innertube/j;->c(Lcom/google/android/apps/youtube/uilib/a/h;)V

    new-instance v1, Lcom/google/android/apps/youtube/uilib/innertube/v;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/uilib/innertube/v;-><init>(Lcom/google/android/apps/youtube/uilib/a/h;)V

    check-cast v6, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/uilib/innertube/v;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ar;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/uilib/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/e;)V

    goto :goto_1

    :cond_6
    instance-of v0, v6, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->g:Lcom/google/android/apps/youtube/uilib/innertube/j;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/uilib/innertube/j;->d(Lcom/google/android/apps/youtube/uilib/a/h;)V

    new-instance v1, Lcom/google/android/apps/youtube/uilib/innertube/r;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->h()Lcom/google/android/apps/youtube/datalib/innertube/i;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->e:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/apps/youtube/uilib/innertube/r;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    check-cast v6, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/uilib/innertube/r;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ag;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/uilib/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/e;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->c()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;)V

    goto/16 :goto_1

    :cond_7
    instance-of v0, v6, Lcom/google/a/a/a/a/da;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->i:Lcom/google/android/apps/youtube/uilib/a/h;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->i:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/e;)V

    goto/16 :goto_1

    :cond_8
    instance-of v0, v6, Lcom/google/android/apps/youtube/uilib/a/m;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->g:Lcom/google/android/apps/youtube/uilib/innertube/j;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/uilib/innertube/j;->e(Lcom/google/android/apps/youtube/uilib/a/h;)V

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/uilib/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/e;)V

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->c()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->d()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->j:Lcom/google/android/apps/youtube/uilib/innertube/o;

    if-eqz v0, :cond_c

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->j:Lcom/google/android/apps/youtube/uilib/innertube/o;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1, p0, p0}, Lcom/google/android/apps/youtube/uilib/innertube/o;-><init>(Ljava/lang/Object;Landroid/view/View$OnClickListener;Lcom/google/android/apps/youtube/uilib/innertube/q;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->j:Lcom/google/android/apps/youtube/uilib/innertube/o;

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/e;)V

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/b;->getViewTypeCount()I

    move-result v0

    if-eq v0, v8, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    invoke-virtual {v2, v1, v0}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto/16 :goto_0

    :cond_d
    move v0, v7

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->h:Landroid/widget/AbsListView$OnScrollListener;

    return-void
.end method

.method protected final a(Lcom/google/a/a/a/a/dq;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/innertube/h;->a(Lcom/google/a/a/a/a/dq;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/dq;->b:Lcom/google/a/a/a/a/qq;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    iget-object v1, p1, Lcom/google/a/a/a/a/dq;->b:Lcom/google/a/a/a/a/qq;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;-><init>(Lcom/google/a/a/a/a/qq;)V

    sget-object v1, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;->RELOAD:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

    if-ne p2, v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->k:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->c(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->l:I

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->k:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/b;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->g()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/t;->c(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/b;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/uilib/a/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->i:Lcom/google/android/apps/youtube/uilib/a/h;

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->j()V

    return-void
.end method

.method public final handleContentEvent(Lcom/google/android/apps/youtube/uilib/innertube/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->j:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/p;->a(Lcom/google/android/apps/youtube/uilib/innertube/o;)V

    goto :goto_0
.end method

.method public final handleErrorEvent(Lcom/google/android/apps/youtube/uilib/innertube/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->j:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->j:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/p;->a(Lcom/google/android/apps/youtube/uilib/innertube/o;)V

    goto :goto_0
.end method

.method public final handleLoadingEvent(Lcom/google/android/apps/youtube/uilib/innertube/d;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->j:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->d:Lcom/google/android/apps/youtube/uilib/innertube/p;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->j:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/p;->a(Lcom/google/android/apps/youtube/uilib/innertube/o;)V

    goto :goto_0
.end method

.method public final j()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->k:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->k:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/b;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->f()V

    goto :goto_0
.end method

.method public final k()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->c:Landroid/widget/ListView;

    return-object v0
.end method

.method public final k_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->i()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->e()V

    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    add-int v0, p2, p3

    if-ne v0, p4, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->l:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/uilib/a/b;->getCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->f:Lcom/google/android/apps/youtube/uilib/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/b;->getCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->l:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->h:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->h:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    :cond_1
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->h:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/t;->h:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    :cond_0
    return-void
.end method
