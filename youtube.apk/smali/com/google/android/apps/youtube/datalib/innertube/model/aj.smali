.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/aj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/qq;

.field private b:Ljava/util/List;

.field private c:Lcom/google/a/a/a/a/dp;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/qq;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/qq;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->a:Lcom/google/a/a/a/a/qq;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b:Ljava/util/List;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->a:Lcom/google/a/a/a/a/qq;

    iget-object v1, v1, Lcom/google/a/a/a/a/qq;->b:[Lcom/google/a/a/a/a/qt;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->a:Lcom/google/a/a/a/a/qq;

    iget-object v1, v0, Lcom/google/a/a/a/a/qq;->b:[Lcom/google/a/a/a/a/qt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/o;

    iget-object v3, v3, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/o;-><init>(Lcom/google/a/a/a/a/it;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, v3, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/p;

    iget-object v3, v3, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/p;-><init>(Lcom/google/a/a/a/a/iq;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v4, v3, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;

    iget-object v3, v3, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;-><init>(Lcom/google/a/a/a/a/os;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v4, v3, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b:Ljava/util/List;

    iget-object v3, v3, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v4, v3, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    if-eqz v4, :cond_0

    iget-object v3, v3, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    iget-object v4, v3, Lcom/google/a/a/a/a/rd;->f:Lcom/google/a/a/a/a/re;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/google/a/a/a/a/rd;->f:Lcom/google/a/a/a/a/re;

    iget-object v4, v4, Lcom/google/a/a/a/a/re;->e:Lcom/google/a/a/a/a/ue;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;-><init>(Lcom/google/a/a/a/a/rd;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lcom/google/a/a/a/a/dp;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->c:Lcom/google/a/a/a/a/dp;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/a/a/a/a/dp;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dp;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->c:Lcom/google/a/a/a/a/dp;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->a:Lcom/google/a/a/a/a/qq;

    iget-object v1, v0, Lcom/google/a/a/a/a/qq;->c:[Lcom/google/a/a/a/a/qs;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/qs;->b:Lcom/google/a/a/a/a/li;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->c:Lcom/google/a/a/a/a/dp;

    iget-object v3, v3, Lcom/google/a/a/a/a/qs;->b:Lcom/google/a/a/a/a/li;

    iput-object v3, v4, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, v3, Lcom/google/a/a/a/a/qs;->d:Lcom/google/a/a/a/a/pr;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->c:Lcom/google/a/a/a/a/dp;

    iget-object v3, v3, Lcom/google/a/a/a/a/qs;->d:Lcom/google/a/a/a/a/pr;

    iput-object v3, v4, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;->c:Lcom/google/a/a/a/a/dp;

    return-object v0
.end method
