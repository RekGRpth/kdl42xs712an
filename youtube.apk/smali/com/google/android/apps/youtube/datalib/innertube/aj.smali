.class public final Lcom/google/android/apps/youtube/datalib/innertube/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/f;


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Landroid/content/SharedPreferences;

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->OFF:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->name()Ljava/lang/String;

    move-result-object v1

    const v2, 0xdc502

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->NON_ADAPTIVE:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->name()Ljava/lang/String;

    move-result-object v1

    const v2, 0xe6b04

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->ADAPTIVE:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->name()Ljava/lang/String;

    move-result-object v1

    const v2, 0xe6b05

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/aj;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/aj;->b:Landroid/content/SharedPreferences;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/datalib/innertube/aj;->c:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/ii;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/aj;->b:Landroid/content/SharedPreferences;

    const-string v1, "ExoPlayer"

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/aj;->c:Z

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->getDefaultValue(Z)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/aj;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->SERVER_EXPERIMENT:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    move-object v1, v0

    :goto_1
    iget-object v3, v1, Lcom/google/a/a/a/a/ct;->e:[I

    const/4 v0, 0x1

    new-array v4, v0, [I

    const/4 v5, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/aj;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/common/e/c;->a([I[I)[I

    move-result-object v0

    iput-object v0, v1, Lcom/google/a/a/a/a/ct;->e:[I

    iput-object v1, p1, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/a/a/a/a/ct;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ct;-><init>()V

    move-object v1, v0

    goto :goto_1
.end method
