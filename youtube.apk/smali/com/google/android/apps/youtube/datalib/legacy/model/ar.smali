.class public Lcom/google/android/apps/youtube/datalib/legacy/model/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/a/a/a/b;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a:Lcom/google/android/apps/youtube/a/a/a/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a:Lcom/google/android/apps/youtube/a/a/a/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->b:Ljava/util/List;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/a/a/a/c;

    iput-object v0, v1, Lcom/google/android/apps/youtube/a/a/a/b;->b:[Lcom/google/android/apps/youtube/a/a/a/c;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/b;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a:Lcom/google/android/apps/youtube/a/a/a/b;

    invoke-static {v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/a/b;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;-><init>(Lcom/google/android/apps/youtube/a/a/a/b;Lcom/google/android/apps/youtube/datalib/legacy/model/aq;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->EMPTY:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;)Lcom/google/android/apps/youtube/datalib/legacy/model/ar;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getSurveyQuestionProto()Lcom/google/android/apps/youtube/a/a/a/c;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ar;
    .locals 2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;)Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    goto :goto_0

    :cond_0
    return-object p0
.end method
