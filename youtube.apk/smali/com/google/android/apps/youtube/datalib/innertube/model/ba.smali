.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/ba;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/ww;

.field private b:Ljava/lang/CharSequence;

.field private c:Lcom/google/a/a/a/a/kz;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ww;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->a:Lcom/google/a/a/a/a/ww;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->b:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->a:Lcom/google/a/a/a/a/ww;

    iget-object v0, v0, Lcom/google/a/a/a/a/ww;->d:Lcom/google/a/a/a/a/pb;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/google/a/a/a/a/pb;->c:Lcom/google/a/a/a/a/xa;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/a/a/a/a/pb;->c:Lcom/google/a/a/a/a/xa;

    iget-object v1, v0, Lcom/google/a/a/a/a/xa;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/a/a/a/a/xa;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->b:Ljava/lang/CharSequence;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->b:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final b()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->c:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->c:Lcom/google/a/a/a/a/kz;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->a:Lcom/google/a/a/a/a/ww;

    iget-object v0, v0, Lcom/google/a/a/a/a/ww;->d:Lcom/google/a/a/a/a/pb;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->c:Lcom/google/a/a/a/a/kz;

    goto :goto_0
.end method
