.class public Lcom/google/android/apps/youtube/datalib/innertube/model/a/f;
.super Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;
.source "SourceFile"


# instance fields
.field private c:Landroid/text/Spanned;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ge;)V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/ge;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/f;->a:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/ge;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/f;->c:Landroid/text/Spanned;

    iget-object v1, p1, Lcom/google/a/a/a/a/ge;->c:[Lcom/google/a/a/a/a/gf;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    iget-object v5, v4, Lcom/google/a/a/a/a/gf;->b:Lcom/google/a/a/a/a/gc;

    if-eqz v5, :cond_0

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    iget-object v4, v4, Lcom/google/a/a/a/a/gf;->b:Lcom/google/a/a/a/a/gc;

    invoke-direct {v5, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;-><init>(Lcom/google/a/a/a/a/gc;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/f;->b:Ljava/util/List;

    return-void
.end method
