.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/ar;

.field private b:Ljava/util/List;

.field private c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ar;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->a:Lcom/google/a/a/a/a/ar;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/a/a/a/a/dq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->a:Lcom/google/a/a/a/a/ar;

    iget-object v0, v0, Lcom/google/a/a/a/a/ar;->e:Lcom/google/a/a/a/a/dq;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->b:Ljava/util/List;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->a:Lcom/google/a/a/a/a/ar;

    iget-object v0, v0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->b:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/a/a/a/a/as;->d:Lcom/google/a/a/a/a/ro;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->b:Ljava/util/List;

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/google/a/a/a/a/ro;->b:[Lcom/google/a/a/a/a/av;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/av;->b:Lcom/google/a/a/a/a/sp;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/an;

    iget-object v3, v3, Lcom/google/a/a/a/a/av;->b:Lcom/google/a/a/a/a/sp;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/an;-><init>(Lcom/google/a/a/a/a/sp;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->b:Ljava/util/List;

    goto :goto_0
.end method

.method public final c()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->c:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->a:Lcom/google/a/a/a/a/ar;

    iget-object v0, v0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->a:Lcom/google/a/a/a/a/ar;

    iget-object v0, v0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    iget-object v1, v0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/c;

    iget-object v0, v0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/c;-><init>(Lcom/google/a/a/a/a/ba;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->c:Ljava/lang/Object;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->c:Ljava/lang/Object;

    return-object v0

    :cond_1
    iget-object v1, v0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/j;

    iget-object v0, v0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/j;-><init>(Lcom/google/a/a/a/a/fh;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->c:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    iget-object v0, v0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;-><init>(Lcom/google/a/a/a/a/oi;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->c:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->a:Lcom/google/a/a/a/a/ar;

    iget-object v0, v0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
