.class public final Lcom/google/android/apps/youtube/datalib/e/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

.field private final c:Lcom/android/volley/l;

.field private final d:Lcom/google/android/apps/youtube/datalib/offline/j;

.field private final e:Lcom/google/android/apps/youtube/common/e/b;

.field private final f:Lcom/google/android/apps/youtube/core/au;

.field private final g:Lcom/google/android/apps/youtube/datalib/config/c;

.field private final h:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Lcom/android/volley/l;Lcom/google/android/apps/youtube/datalib/offline/j;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/au;Lcom/google/android/apps/youtube/datalib/config/c;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->a:Ljava/util/List;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->b:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->c:Lcom/android/volley/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->d:Lcom/google/android/apps/youtube/datalib/offline/j;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/au;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->f:Lcom/google/android/apps/youtube/core/au;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->g:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->h:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/e/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/youtube/datalib/e/f;-><init>(Ljava/lang/String;IB)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V
    .locals 13

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->a()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/b;->h:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/apps/youtube/datalib/e/c;

    invoke-direct {v2, p0, p2, v1}, Lcom/google/android/apps/youtube/datalib/e/c;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/android/volley/n;Landroid/net/Uri;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/e/a;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->f()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->d()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->c()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->g()Lcom/google/android/apps/youtube/datalib/e/d;

    move-result-object v7

    iget-object v9, p0, Lcom/google/android/apps/youtube/datalib/e/b;->a:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/apps/youtube/datalib/e/b;->b:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    iget-object v11, p0, Lcom/google/android/apps/youtube/datalib/e/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v12, p0, Lcom/google/android/apps/youtube/datalib/e/b;->g:Lcom/google/android/apps/youtube/datalib/config/c;

    move-object v8, p2

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/datalib/e/a;-><init>(Ljava/lang/String;ZLjava/lang/String;IJLcom/google/android/apps/youtube/datalib/e/d;Lcom/android/volley/n;Ljava/util/List;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;)V

    const-string v1, "Sending from HTTP204 service"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/e/b;->f:Lcom/google/android/apps/youtube/core/au;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/au;->P()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->f()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/e/b;->c:Lcom/android/volley/l;

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/e/b;->d:Lcom/google/android/apps/youtube/datalib/offline/j;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/datalib/offline/j;->a(Lcom/google/android/apps/youtube/datalib/offline/l;)V

    goto :goto_1
.end method
