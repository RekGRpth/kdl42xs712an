.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/apps/youtube/datalib/legacy/model/al;

.field private final c:Lcom/google/android/apps/youtube/datalib/legacy/model/ah;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->a:I

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/al;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/al;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ah;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ah;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/ah;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;
    .locals 5

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->a:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/al;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/al;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/ah;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ah;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;-><init>(ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;Lcom/google/android/apps/youtube/datalib/legacy/model/ac;)V

    return-object v0
.end method

.method public final a(ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;)Lcom/google/android/apps/youtube/datalib/legacy/model/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/ah;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ah;->a(ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;)Lcom/google/android/apps/youtube/datalib/legacy/model/ah;

    return-object p0
.end method

.method public final a(Ljava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/al;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/al;->a(Ljava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/al;

    return-object p0
.end method

.method public final b(Ljava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/al;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/al;->b(Ljava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/al;

    return-object p0
.end method

.method public final synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ad;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;

    move-result-object v0

    return-object v0
.end method
