.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;
.implements Ljava/io/Serializable;


# instance fields
.field private action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field private displayUsername:Ljava/lang/String;

.field private groupId:Ljava/lang/String;

.field private subject:Ljava/lang/String;

.field private subjectUri:Landroid/net/Uri;

.field private target:Ljava/lang/String;

.field private targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

.field private when:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subject:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subjectUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->target:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->displayUsername:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->when:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->groupId:Ljava/lang/String;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Event;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subject:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subjectUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->target:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->displayUsername:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->when:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->groupId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final action(Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    return-object p0
.end method

.method public final build()Lcom/google/android/apps/youtube/datalib/legacy/model/Event;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subject:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subjectUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->target:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->displayUsername:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->when:Ljava/util/Date;

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->groupId:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;-><init>(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V

    return-object v0
.end method

.method public final bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Event;

    move-result-object v0

    return-object v0
.end method

.method public final displayUsername(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->displayUsername:Ljava/lang/String;

    return-object p0
.end method

.method public final groupId(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->groupId:Ljava/lang/String;

    return-object p0
.end method

.method public final subject(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subject:Ljava/lang/String;

    return-object p0
.end method

.method public final subjectUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subjectUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final target(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->target:Ljava/lang/String;

    return-object p0
.end method

.method public final targetVideo(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    return-object p0
.end method

.method public final when(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->when:Ljava/util/Date;

    return-object p0
.end method
