.class final Lcom/google/android/apps/youtube/datalib/innertube/model/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;
    .locals 3

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readByteArray([B)V

    :try_start_0
    new-instance v2, Lcom/google/a/a/a/a/hv;

    invoke-direct {v2}, Lcom/google/a/a/a/a/hv;-><init>()V

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-static {v2, v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/hv;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;-><init>(Lcom/google/a/a/a/a/hv;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/n;->a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    return-object v0
.end method
