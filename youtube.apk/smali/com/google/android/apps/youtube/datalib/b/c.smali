.class public final Lcom/google/android/apps/youtube/datalib/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/common/csi/lib/Sender;


# instance fields
.field final a:Lcom/google/android/apps/youtube/datalib/a/e;

.field private final b:Lcom/android/volley/l;

.field private final c:Lcom/google/android/apps/youtube/common/network/h;

.field private final d:Lcom/google/android/apps/youtube/core/client/DeviceClassification;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/l;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/b/c;->b:Lcom/android/volley/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/b/c;->c:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/b/c;->d:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    invoke-static {}, Lcom/google/android/apps/common/csi/lib/k;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/common/csi/lib/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Lcom/google/android/apps/common/csi/lib/k;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "%s %s/%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p1, v2, v0

    const/4 v0, 0x2

    aput-object p2, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/b/d;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/b/d;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/b/c;->a:Lcom/google/android/apps/youtube/datalib/a/e;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4

    invoke-static {p1, p2}, Lcom/google/android/apps/common/csi/lib/k;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/b/c;->d:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->a(Lcom/google/android/apps/youtube/common/e/o;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/b/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/b/c;->a:Lcom/google/android/apps/youtube/datalib/a/e;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/a/b;->a:Lcom/android/volley/n;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/b/b;-><init>(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/a/e;Lcom/android/volley/n;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/b/c;->c:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Pinging "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/b/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/b/c;->b:Lcom/android/volley/l;

    invoke-virtual {v0, v1}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :cond_0
    return-void
.end method
