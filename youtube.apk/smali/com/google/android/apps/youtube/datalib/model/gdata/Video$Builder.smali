.class public final Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;
.implements Ljava/io/Serializable;


# instance fields
.field private accessControl:Ljava/util/Map;

.field private adultContent:Z

.field private captionTracksUri:Landroid/net/Uri;

.field private categoryLabel:Ljava/lang/String;

.field private categoryTerm:Ljava/lang/String;

.field private defaultThumbnailUri:Landroid/net/Uri;

.field private description:Ljava/lang/String;

.field private dislikesCount:J

.field private duration:I

.field private editUri:Landroid/net/Uri;

.field private hqThumbnailUri:Landroid/net/Uri;

.field private id:Ljava/lang/String;

.field private is3d:Z

.field private isInOfflineStore:Z

.field private isUpsell:Z

.field private likesCount:J

.field private liveEventUri:Landroid/net/Uri;

.field private location:Ljava/lang/String;

.field private monetize:Z

.field private monetizeExceptionCountries:Ljava/util/Set;

.field private mqThumbnailUri:Landroid/net/Uri;

.field private offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

.field private owner:Ljava/lang/String;

.field private ownerDisplayName:Ljava/lang/String;

.field private ownerUri:Landroid/net/Uri;

.field private paidContent:Z

.field private privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

.field private publishedDate:Ljava/util/Date;

.field private sdThumbnailUri:Landroid/net/Uri;

.field private state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field private streams:Ljava/util/Set;

.field private tags:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private uploadedDate:Ljava/util/Date;

.field private viewCount:J

.field private watchUri:Landroid/net/Uri;

.field private where:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->id:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->watchUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->defaultThumbnailUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->mqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->hqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->sdThumbnailUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->editUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->captionTracksUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->viewCount:J

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->likesCount:J

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->dislikesCount:J

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->owner:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->publishedDate:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryTerm:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryLabel:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->tags:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->description:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->accessControl:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->location:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->where:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->adultContent:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetize:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetizeExceptionCountries:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->paidContent:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isUpsell:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->liveEventUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->is3d:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isInOfflineStore:Z

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->watchUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->defaultThumbnailUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->mqThumbnailUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->hqThumbnailUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->sdThumbnailUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->editUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->captionTracksUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->viewCount:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->likesCount:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->dislikesCount:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->owner:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->publishedDate:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryTerm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->tags:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->accessControl:Ljava/util/Map;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->location:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->where:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->adultContent:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetize:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetizeExceptionCountries:Ljava/util/Set;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->paidContent:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isUpsell:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->liveEventUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->is3d:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isInOfflineStore:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final accessControl(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->accessControl:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->accessControl:Ljava/util/Map;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->accessControl:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final accessControl(Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->accessControl:Ljava/util/Map;

    return-object p0
.end method

.method public final addStream(Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final adultContent(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->adultContent:Z

    return-object p0
.end method

.method public final build()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 44

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    :goto_0
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->watchUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->defaultThumbnailUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->mqThumbnailUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->hqThumbnailUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->sdThumbnailUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->editUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->captionTracksUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->title:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration:I

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->viewCount:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->likesCount:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->dislikesCount:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->owner:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerUri:Landroid/net/Uri;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate:Ljava/util/Date;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->publishedDate:Ljava/util/Date;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryTerm:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryLabel:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->tags:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->description:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->accessControl:Ljava/util/Map;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->location:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->where:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->adultContent:Z

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetize:Z

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetizeExceptionCountries:Ljava/util/Set;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->paidContent:Z

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isUpsell:Z

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->liveEventUri:Landroid/net/Uri;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->is3d:Z

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerDisplayName:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isInOfflineStore:Z

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    invoke-direct/range {v2 .. v43}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;-><init>(Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;IJJJLjava/lang/String;Landroid/net/Uri;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/youtube/datalib/model/gdata/Video$State;ZLjava/util/Set;ZZLandroid/net/Uri;ZLjava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/c;)V

    return-object v2

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    return-object v0
.end method

.method public final captionTracksUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->captionTracksUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final categoryLabel(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryLabel:Ljava/lang/String;

    return-object p0
.end method

.method public final categoryTerm(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryTerm:Ljava/lang/String;

    return-object p0
.end method

.method public final defaultThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->defaultThumbnailUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final description(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public final dislikesCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->dislikesCount:J

    return-object p0
.end method

.method public final duration(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration:I

    return-object p0
.end method

.method public final editUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->editUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final getUploadedDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate:Ljava/util/Date;

    return-object v0
.end method

.method public final hqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->hqThumbnailUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final id(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public final is3d(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->is3d:Z

    return-object p0
.end method

.method public final isInOfflineStore(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isInOfflineStore:Z

    return-object p0
.end method

.method public final isUpsell(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isUpsell:Z

    return-object p0
.end method

.method public final likesCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->likesCount:J

    return-object p0
.end method

.method public final liveEventUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->liveEventUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final location(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->location:Ljava/lang/String;

    return-object p0
.end method

.method public final monetize(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetize:Z

    return-object p0
.end method

.method public final monetizeExceptionCountries(Ljava/util/Set;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetizeExceptionCountries:Ljava/util/Set;

    return-object p0
.end method

.method public final mqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->mqThumbnailUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final offlineChannelAvatarThumbnailUrl(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

    return-object p0
.end method

.method public final owner(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->owner:Ljava/lang/String;

    return-object p0
.end method

.method public final ownerDisplayName(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerDisplayName:Ljava/lang/String;

    return-object p0
.end method

.method public final ownerUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final paidContent(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->paidContent:Z

    return-object p0
.end method

.method public final privacy(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-eq v0, v1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    :cond_0
    return-object p0
.end method

.method public final publishedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->publishedDate:Ljava/util/Date;

    return-object p0
.end method

.method public final sdThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->sdThumbnailUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    return-object p0
.end method

.method public final streams(Ljava/util/Set;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams:Ljava/util/Set;

    return-object p0
.end method

.method public final tags(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->tags:Ljava/lang/String;

    return-object p0
.end method

.method public final title(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public final uploadedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate:Ljava/util/Date;

    return-object p0
.end method

.method public final viewCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->viewCount:J

    return-object p0
.end method

.method public final watchUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->watchUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final where(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->where:Ljava/lang/String;

    return-object p0
.end method
