.class public final Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private height:I

.field private itag:I

.field private mimeType:Ljava/lang/String;

.field private sizeInBytes:J

.field private uri:Landroid/net/Uri;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->itag:I

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->itag:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->uri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->videoId:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->height:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->mimeType:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->sizeInBytes:J

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->itag:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->height:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->mimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->sizeInBytes:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    return-void
.end method


# virtual methods
.method public final build()Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->itag:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->videoId:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->height:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->mimeType:Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->sizeInBytes:J

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;JLcom/google/android/apps/youtube/datalib/model/gdata/a;)V

    return-object v0
.end method

.method public final height(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->height:I

    return-object p0
.end method

.method public final itag(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->itag:I

    return-object p0
.end method

.method public final legacyGdataFormat(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    # getter for: Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->GDATA_TO_ITAG:Landroid/util/SparseIntArray;
    invoke-static {}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->access$000()Landroid/util/SparseIntArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->itag(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mimeType(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->mimeType:Ljava/lang/String;

    return-object p0
.end method

.method public final sizeInBytes(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->sizeInBytes:J

    return-object p0
.end method

.method public final uri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->uri:Landroid/net/Uri;

    return-object p0
.end method

.method public final videoId(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->videoId:Ljava/lang/String;

    return-object p0
.end method
