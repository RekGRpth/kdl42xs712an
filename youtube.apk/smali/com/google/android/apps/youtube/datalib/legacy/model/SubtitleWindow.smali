.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final id:I

.field final settingsTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;

.field final textTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;


# direct methods
.method private constructor <init>(ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->id:I

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->textTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->settingsTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;

    return-void
.end method

.method synthetic constructor <init>(ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;Lcom/google/android/apps/youtube/datalib/legacy/model/ac;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;-><init>(ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;)V

    return-void
.end method


# virtual methods
.method public final getId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->id:I

    return v0
.end method

.method public final getRollUpTextAt(I)Landroid/util/Pair;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->textTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;->getRollUpTextAt(I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final getSettingsAt(I)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->settingsTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;->getSettingsAt(I)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    move-result-object v0

    return-object v0
.end method

.method public final getSubtitleWindowSnapshotAt(I)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;
    .locals 6

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->getSettingsAt(I)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-boolean v0, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->rollUp:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->getRollUpTextAt(I)Landroid/util/Pair;

    move-result-object v0

    move-object v2, v0

    :goto_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->id:I

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;-><init>(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;)V

    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->getTextAt(I)Landroid/util/Pair;

    move-result-object v0

    move-object v2, v0

    goto :goto_0
.end method

.method public final getTextAt(I)Landroid/util/Pair;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->textTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;->getTextAt(I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "id: %d text: %s settings: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->textTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowTextTimeline;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindow;->settingsTimeline:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettingsTimeline;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
