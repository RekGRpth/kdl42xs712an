.class public final Lcom/google/android/apps/youtube/datalib/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/n;


# instance fields
.field private final a:Lcom/android/volley/n;

.field private final b:Lcom/google/android/apps/youtube/datalib/config/a;


# direct methods
.method public constructor <init>(Lcom/android/volley/n;Lcom/google/android/apps/youtube/datalib/config/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/i;->a:Lcom/android/volley/n;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/i;->b:Lcom/google/android/apps/youtube/datalib/config/a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/i;->b:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/config/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/j;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/j;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Full response from error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/String;

    iget-object v0, v0, Lcom/android/volley/j;->b:[B

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/i;->a:Lcom/android/volley/n;

    invoke-interface {v0, p1}, Lcom/android/volley/n;->a(Lcom/android/volley/VolleyError;)V

    return-void
.end method
