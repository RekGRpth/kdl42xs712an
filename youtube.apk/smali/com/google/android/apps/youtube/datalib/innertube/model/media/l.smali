.class final Lcom/google/android/apps/youtube/datalib/innertube/model/media/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;
    .locals 9

    const/4 v7, 0x1

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Lcom/google/a/a/a/a/sb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sb;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/j;->b(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/sb;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    :goto_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v5

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-ne v8, v7, :cond_0

    :goto_1
    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;-><init>(Lcom/google/a/a/a/a/sb;Ljava/lang/String;JJZ)V

    return-object v0

    :catch_0
    move-exception v0

    const-string v2, "Error reading streaming data"

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/l;->a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    return-object v0
.end method
