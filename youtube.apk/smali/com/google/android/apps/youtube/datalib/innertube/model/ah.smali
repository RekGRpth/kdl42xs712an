.class public Lcom/google/android/apps/youtube/datalib/innertube/model/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private final a:Lcom/google/a/a/a/a/pk;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private h:Ljava/lang/CharSequence;

.field private i:Lcom/google/a/a/a/a/kz;

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:Lcom/google/a/a/a/a/vb;

.field private m:Lcom/google/a/a/a/a/w;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/pk;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/pk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, p1, Lcom/google/a/a/a/a/pk;->k:Lcom/google/a/a/a/a/pj;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/pk;->k:Lcom/google/a/a/a/a/pj;

    iget-object v0, v0, Lcom/google/a/a/a/a/pj;->b:Lcom/google/a/a/a/a/vb;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->l:Lcom/google/a/a/a/a/vb;

    iget-object v0, p1, Lcom/google/a/a/a/a/pk;->k:Lcom/google/a/a/a/a/pj;

    iget-object v0, v0, Lcom/google/a/a/a/a/pj;->c:Lcom/google/a/a/a/a/w;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->m:Lcom/google/a/a/a/a/w;

    :cond_0
    return-void
.end method

.method public static a([Ljava/lang/String;)Ljava/util/List;
    .locals 4

    array-length v0, p0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p0, v1

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v1, v1, Lcom/google/a/a/a/a/pk;->c:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->j:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->f:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v1, v1, Lcom/google/a/a/a/a/pk;->h:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->h:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->i:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->h:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final j()Lcom/google/a/a/a/a/kz;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->i:Lcom/google/a/a/a/a/kz;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->l:Lcom/google/a/a/a/a/vb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->l:Lcom/google/a/a/a/a/vb;

    iget-object v0, v0, Lcom/google/a/a/a/a/vb;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->l:Lcom/google/a/a/a/a/vb;

    iget-object v0, v0, Lcom/google/a/a/a/a/vb;->b:Lcom/google/a/a/a/a/fk;

    iget-object v0, v0, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    aget-object v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->l:Lcom/google/a/a/a/a/vb;

    iget-object v0, v0, Lcom/google/a/a/a/a/vb;->b:Lcom/google/a/a/a/a/fk;

    iget-object v0, v0, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/a/a/a/a/sc;->f:Lcom/google/a/a/a/a/kz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->i:Lcom/google/a/a/a/a/kz;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->i:Lcom/google/a/a/a/a/kz;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->m:Lcom/google/a/a/a/a/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->m:Lcom/google/a/a/a/a/w;

    iget-object v0, v0, Lcom/google/a/a/a/a/w;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->m:Lcom/google/a/a/a/a/w;

    iget-object v0, v0, Lcom/google/a/a/a/a/w;->b:Lcom/google/a/a/a/a/fk;

    iget-object v0, v0, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->m:Lcom/google/a/a/a/a/w;

    iget-object v0, v0, Lcom/google/a/a/a/a/w;->b:Lcom/google/a/a/a/a/fk;

    iget-object v0, v0, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/a/a/a/a/sc;->f:Lcom/google/a/a/a/a/kz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->i:Lcom/google/a/a/a/a/kz;

    goto :goto_0
.end method

.method public final k()Lcom/google/a/a/a/a/vb;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->l:Lcom/google/a/a/a/a/vb;

    return-object v0
.end method

.method public final l()Lcom/google/a/a/a/a/w;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->m:Lcom/google/a/a/a/a/w;

    return-object v0
.end method

.method public final m()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->j:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->l:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->j:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->j:Ljava/util/List;

    return-object v0
.end method

.method public final n()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->k:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->m:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->k:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->k:Ljava/util/List;

    return-object v0
.end method
