.class public final Lcom/google/android/apps/youtube/datalib/innertube/ac;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private c:Lcom/google/a/a/a/a/lm;

.field private d:Lcom/google/a/a/a/a/lp;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    new-instance v0, Lcom/google/a/a/a/a/lm;

    invoke-direct {v0}, Lcom/google/a/a/a/a/lm;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ac;->c:Lcom/google/a/a/a/a/lm;

    new-instance v0, Lcom/google/a/a/a/a/lp;

    invoke-direct {v0}, Lcom/google/a/a/a/a/lp;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ac;->d:Lcom/google/a/a/a/a/lp;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ac;->c:Lcom/google/a/a/a/a/lm;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ac;->d:Lcom/google/a/a/a/a/lp;

    iput-object v1, v0, Lcom/google/a/a/a/a/lm;->b:Lcom/google/a/a/a/a/lp;

    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ac;->a([B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/ac;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ac;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ac;->d:Lcom/google/a/a/a/a/lp;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/lp;->b:[B

    return-object p0
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ac;->c:Lcom/google/a/a/a/a/lm;

    iget-object v0, v0, Lcom/google/a/a/a/a/lm;->b:Lcom/google/a/a/a/a/lp;

    iget-object v0, v0, Lcom/google/a/a/a/a/lp;->b:[B

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "notification_registration/set_registration"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/ac;->b()V

    new-instance v0, Lcom/google/a/a/a/a/qz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/qz;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/ac;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/qz;->b:Lcom/google/a/a/a/a/ii;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ac;->c:Lcom/google/a/a/a/a/lm;

    iput-object v1, v0, Lcom/google/a/a/a/a/qz;->c:Lcom/google/a/a/a/a/lm;

    return-object v0
.end method
