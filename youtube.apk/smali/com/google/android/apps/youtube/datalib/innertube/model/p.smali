.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/iq;

.field private b:Ljava/util/List;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/iq;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->a:Lcom/google/a/a/a/a/iq;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->b:Ljava/util/List;

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->a:Lcom/google/a/a/a/a/iq;

    iget-object v1, v1, Lcom/google/a/a/a/a/iq;->b:[Lcom/google/a/a/a/a/is;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->a:Lcom/google/a/a/a/a/iq;

    iget-object v1, v0, Lcom/google/a/a/a/a/iq;->b:[Lcom/google/a/a/a/a/is;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/e;

    iget-object v6, v3, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    invoke-direct {v5, v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;-><init>(Lcom/google/a/a/a/a/db;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v4, v3, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/f;

    iget-object v6, v3, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    invoke-direct {v5, v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/f;-><init>(Lcom/google/a/a/a/a/de;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v4, v3, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/h;

    iget-object v6, v3, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    invoke-direct {v5, v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/h;-><init>(Lcom/google/a/a/a/a/dj;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v4, v3, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/g;

    iget-object v3, v3, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/g;-><init>(Lcom/google/a/a/a/a/df;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lcom/google/a/a/a/a/qu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->a:Lcom/google/a/a/a/a/iq;

    iget-object v0, v0, Lcom/google/a/a/a/a/iq;->d:Lcom/google/a/a/a/a/ir;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->a:Lcom/google/a/a/a/a/iq;

    iget-object v0, v0, Lcom/google/a/a/a/a/iq;->d:Lcom/google/a/a/a/a/ir;

    iget-object v0, v0, Lcom/google/a/a/a/a/ir;->b:Lcom/google/a/a/a/a/qu;

    goto :goto_0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->a:Lcom/google/a/a/a/a/iq;

    iget-object v0, v0, Lcom/google/a/a/a/a/iq;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->a:Lcom/google/a/a/a/a/iq;

    iget-object v0, v0, Lcom/google/a/a/a/a/iq;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->c:Ljava/lang/CharSequence;

    return-object v0
.end method
