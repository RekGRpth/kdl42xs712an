.class final Lcom/google/android/apps/youtube/datalib/innertube/model/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;
    .locals 3

    :try_start_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    new-instance v0, Lcom/google/android/apps/youtube/a/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/h;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/j;->b(Landroid/os/Parcel;Lcom/google/protobuf/micro/c;)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/h;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Lcom/google/android/apps/youtube/a/a/h;)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    const-string v1, ""

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/aq;->a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    return-object v0
.end method
