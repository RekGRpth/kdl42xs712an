.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/Optional;
.implements Ljava/io/Serializable;


# instance fields
.field public final channelUri:Landroid/net/Uri;

.field public final editUri:Landroid/net/Uri;

.field public final title:Ljava/lang/String;

.field public final type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

.field public final uri:Landroid/net/Uri;

.field public final username:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->editUri:Landroid/net/Uri;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->title:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->uri:Landroid/net/Uri;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->username:Ljava/lang/String;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->channelUri:Landroid/net/Uri;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;->title(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;->type(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->editUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;->editUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;->username(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->channelUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;->channelUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->username:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final get()Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;
    .locals 0

    return-object p0
.end method

.method public final bridge synthetic get()Ljava/io/Serializable;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->get()Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final isUserRelated()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;->CHANNEL:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;->FAVORITES:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;->USER:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->title:Ljava/lang/String;

    return-object v0
.end method
