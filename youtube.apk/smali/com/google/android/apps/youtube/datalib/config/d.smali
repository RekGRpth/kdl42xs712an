.class public final Lcom/google/android/apps/youtube/datalib/config/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/config/c;


# instance fields
.field private final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/d;->a:Landroid/content/ContentResolver;

    return-void
.end method

.method private final a(Ljava/lang/String;I)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/d;->a:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "youtube:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    const-string v0, "offline_http_max_queue_size"

    const/16 v1, 0x3e8

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/config/d;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 6

    const-string v0, "offline_http_max_age_days"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/config/d;->a:Landroid/content/ContentResolver;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "youtube:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()I
    .locals 2

    const-string v0, "offline_http_batch_size"

    const/16 v1, 0x64

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/config/d;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 2

    const-string v0, "offline_http_report_cap_hours"

    const/16 v1, 0x18

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/config/d;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 2

    const-string v0, "ping_request_timeout_seconds"

    const/16 v1, 0x3c

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/config/d;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 2

    const-string v0, "offline_dispatch_maximum_errors"

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/config/d;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    const-string v0, "offline_retry_backoff_factor"

    const/16 v1, 0x8

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/config/d;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
