.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private final a:Lcom/google/a/a/a/a/uy;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/uy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/uy;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, p1, Lcom/google/a/a/a/a/uy;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/uy;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/uy;->j:Lcom/google/a/a/a/a/kz;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, v0, Lcom/google/a/a/a/a/uy;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v1, v1, Lcom/google/a/a/a/a/uy;->c:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, v0, Lcom/google/a/a/a/a/uy;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, v0, Lcom/google/a/a/a/a/uy;->r:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, v0, Lcom/google/a/a/a/a/uy;->j:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, v0, Lcom/google/a/a/a/a/uy;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, v0, Lcom/google/a/a/a/a/uy;->i:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->f:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, v0, Lcom/google/a/a/a/a/uy;->t:Lcom/google/a/a/a/a/uz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v0, v0, Lcom/google/a/a/a/a/uy;->t:Lcom/google/a/a/a/a/uz;

    iget-object v0, v0, Lcom/google/a/a/a/a/uz;->b:Lcom/google/a/a/a/a/mf;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->a:Lcom/google/a/a/a/a/uy;

    iget-object v1, v1, Lcom/google/a/a/a/a/uy;->t:Lcom/google/a/a/a/a/uz;

    iget-object v1, v1, Lcom/google/a/a/a/a/uz;->b:Lcom/google/a/a/a/a/mf;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;-><init>(Lcom/google/a/a/a/a/mf;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    return-object v0
.end method
