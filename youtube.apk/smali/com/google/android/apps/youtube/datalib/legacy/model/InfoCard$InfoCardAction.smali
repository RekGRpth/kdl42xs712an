.class public Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/datalib/legacy/a/a;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final TYPE_APPLICATION_STORE:I = 0x4

.field public static final TYPE_ATTRIBUTION:I = 0x5

.field public static final TYPE_CHANNEL:I = 0x2

.field public static final TYPE_COLLAPSE:I = 0x11

.field public static final TYPE_EXTERNAL:I = 0x10

.field public static final TYPE_FUNDRAISING:I = 0xa

.field public static final TYPE_HEADLINE:I = 0x1

.field public static final TYPE_HELPOUTS:I = 0xf

.field public static final TYPE_INTERNAL:I = 0xd

.field public static final TYPE_IRDB:I = 0x8

.field public static final TYPE_LETTERMAN:I = 0xe

.field public static final TYPE_MERCH:I = 0x9

.field public static final TYPE_NONE:I = 0x0

.field public static final TYPE_PLAYLIST:I = 0x7

.field public static final TYPE_RESPONSE:I = 0xc

.field public static final TYPE_RESULTS:I = 0xb

.field public static final TYPE_SUBSCRIPTION:I = 0x6

.field public static final TYPE_VIDEO:I = 0x3

.field public static final TYPE_VOTE:I = 0x12


# instance fields
.field private final actionTrackingUris:Ljava/util/List;

.field private final linkUrl:Landroid/net/Uri;

.field private final title:Ljava/lang/String;

.field private final type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/h;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/net/Uri;Ljava/lang/String;Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->type:I

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->linkUrl:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->title:Ljava/lang/String;

    if-eqz p4, :cond_0

    :goto_0
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->actionTrackingUris:Ljava/util/List;

    return-void

    :cond_0
    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getLinkUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getLinkUrl()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getActionTrackingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getActionTrackingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getActionTrackingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->actionTrackingUris:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getConverter()Lcom/google/android/apps/youtube/datalib/legacy/a/b;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/j;

    move-result-object v0

    return-object v0
.end method

.method public getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/j;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/j;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)V

    return-object v0
.end method

.method public getLinkUrl()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->linkUrl:Landroid/net/Uri;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->type:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getType()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getLinkUrl()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getActionTrackingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    return-void
.end method
