.class public Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final EDGE_TYPE_DEPRESSED:I = 0x4

.field public static final EDGE_TYPE_DROP_SHADOW:I = 0x2

.field public static final EDGE_TYPE_NONE:I = 0x0

.field public static final EDGE_TYPE_RAISED:I = 0x3

.field public static final EDGE_TYPE_UNIFORM:I = 0x1

.field public static final FONT_CASUAL:I = 0x4

.field public static final FONT_CURSIVE:I = 0x5

.field public static final FONT_MONOSPACED_SANS_SERIF:I = 0x2

.field public static final FONT_MONOSPACED_SERIF:I = 0x0

.field public static final FONT_PROPORTIONAL_SANS_SERIF:I = 0x3

.field public static final FONT_PROPORTIONAL_SERIF:I = 0x1

.field public static final FONT_SMALL_CAPITALS:I = 0x6


# instance fields
.field private final backgroundColor:I

.field private final edgeColor:I

.field private final edgeType:I

.field private final foregroundColor:I

.field private final typeface:I

.field private final windowColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ap;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->backgroundColor:I

    iput p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->windowColor:I

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeColor:I

    iput p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeType:I

    iput p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->foregroundColor:I

    iput p6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->typeface:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->backgroundColor:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->windowColor:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeColor:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->foregroundColor:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->typeface:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/youtube/datalib/legacy/model/ao;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/accessibility/CaptioningManager$CaptionStyle;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p1, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->backgroundColor:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->backgroundColor:I

    iget v0, p1, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->edgeColor:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeColor:I

    iget v0, p1, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->edgeType:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeType:I

    iget v0, p1, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->foregroundColor:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->foregroundColor:I

    iput p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->windowColor:I

    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-virtual {p1}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->typeface:I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-virtual {p1}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->typeface:I

    goto :goto_0

    :cond_1
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {p1}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->typeface:I

    goto :goto_0
.end method

.method private static final getColorStringForColor(I)Ljava/lang/String;
    .locals 4

    const-string v0, "#%06X"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0xffffff

    and-int/2addr v3, p0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static final getOpacityStringForColor(I)Ljava/lang/String;
    .locals 5

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x406fe00000000000L    # 255.0

    div-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    const-string v0, "0.0"

    :goto_0
    return-object v0

    :cond_0
    const-string v2, "%.2f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->backgroundColor:I

    return v0
.end method

.method public getEdgeColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeColor:I

    return v0
.end method

.method public getEdgeType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeType:I

    return v0
.end method

.method public getForegroundColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->foregroundColor:I

    return v0
.end method

.method public getMdxFormat(F)Ljava/util/HashMap;
    .locals 6

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v0, "background"

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->backgroundColor:I

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getColorStringForColor(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "backgroundOpacity"

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->backgroundColor:I

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getOpacityStringForColor(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "color"

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->foregroundColor:I

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getColorStringForColor(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "textOpacity"

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->foregroundColor:I

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getOpacityStringForColor(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "fontSizeRelative"

    const-string v2, "%.2f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "windowColor"

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->windowColor:I

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getColorStringForColor(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "windowOpacity"

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->windowColor:I

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getOpacityStringForColor(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeType:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "none"

    :goto_0
    const-string v2, "charEdgeStyle"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, ""

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->typeface:I

    packed-switch v2, :pswitch_data_1

    :goto_1
    const-string v2, "fontFamilyOption"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1

    :pswitch_0
    const-string v0, "uniform"

    goto :goto_0

    :pswitch_1
    const-string v0, "dropShadow"

    goto :goto_0

    :pswitch_2
    const-string v0, "depressed"

    goto :goto_0

    :pswitch_3
    const-string v0, "raised"

    goto :goto_0

    :pswitch_4
    const-string v0, "monoSerif"

    goto :goto_1

    :pswitch_5
    const-string v0, "propSerif"

    goto :goto_1

    :pswitch_6
    const-string v0, "monoSans"

    goto :goto_1

    :pswitch_7
    const-string v0, "propSans"

    goto :goto_1

    :pswitch_8
    const-string v0, "casual"

    goto :goto_1

    :pswitch_9
    const-string v0, "cursive"

    goto :goto_1

    :pswitch_a
    const-string v0, "smallCaps"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public getTypeface()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->typeface:I

    return v0
.end method

.method public getWindowColor()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->windowColor:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->backgroundColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->windowColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->edgeType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->foregroundColor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->typeface:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
