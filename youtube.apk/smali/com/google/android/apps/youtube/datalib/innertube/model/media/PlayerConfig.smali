.class public Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final exoPlayerActivationType:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

.field private final innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/h;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    new-instance v0, Lcom/google/a/a/a/a/nh;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nh;-><init>()V

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->SERVER_EXPERIMENT:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>(Lcom/google/a/a/a/a/nh;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/nh;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/nh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->exoPlayerActivationType:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/a/a/f;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/a/a/a/a/nh;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nh;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/f;->a()Lcom/google/protobuf/micro/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/micro/a;->b()[B

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/f;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/f;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->exoPlayerActivationType:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->SERVER_EXPERIMENT:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public bandwidthFraction()F
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->l:F

    :goto_0
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/high16 v0, 0x3f400000    # 0.75f

    goto :goto_1
.end method

.method public bufferChunkSizeKb()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->m:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x40

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;Lcom/google/protobuf/nano/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->exoPlayerActivationType:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->exoPlayerActivationType:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public highPoolLoad()F
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->k:F

    :goto_0
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const v0, 0x3f4ccccd    # 0.8f

    goto :goto_1
.end method

.method public highWatermarkMs()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->i:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x7530

    goto :goto_1
.end method

.method public httpConnectTimeoutMs()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->n:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x1f40

    goto :goto_1
.end method

.method public httpReadTimeoutMs()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->o:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x1f40

    goto :goto_1
.end method

.method public isAdaptiveBitrateEnabled()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :cond_1
    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/media/i;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->exoPlayerActivationType:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v2, v2, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v2, v2, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget-boolean v2, v2, Lcom/google/a/a/a/a/ep;->c:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isExoPlayerEnabled()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :cond_1
    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/media/i;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->exoPlayerActivationType:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v2, v2, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v2, v2, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget-boolean v2, v2, Lcom/google/a/a/a/a/ep;->b:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public lowPoolLoad()F
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->j:F

    :goto_0
    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const v0, 0x3e4ccccd    # 0.2f

    goto :goto_1
.end method

.method public lowWatermarkMs()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->h:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x3a98

    goto :goto_1
.end method

.method public maxDurationForQualityDecreaseMs()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->f:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x61a8

    goto :goto_1
.end method

.method public maxInitialByteRate()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->d:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const v0, 0x186a0

    goto :goto_1
.end method

.method public minBufferMs()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->r:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x1f4

    goto :goto_1
.end method

.method public minDurationForQualityIncreaseMs()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->e:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x2710

    goto :goto_1
.end method

.method public minDurationToRetainAfterDiscardMs()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    iget-object v0, v0, Lcom/google/a/a/a/a/nh;->f:Lcom/google/a/a/a/a/ep;

    iget v0, v0, Lcom/google/a/a/a/a/ep;->g:I

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0x61a8

    goto :goto_1
.end method

.method public toPlayerConfigProto()Lcom/google/android/apps/youtube/a/a/f;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/a/a/f;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/f;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->innertubePlayerConfigProto:Lcom/google/a/a/a/a/nh;

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/micro/a;->a([B)Lcom/google/protobuf/micro/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/f;->a(Lcom/google/protobuf/micro/a;)Lcom/google/android/apps/youtube/a/a/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->exoPlayerActivationType:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/f;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->toPlayerConfigProto()Lcom/google/android/apps/youtube/a/a/f;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/micro/c;)V

    return-void
.end method
