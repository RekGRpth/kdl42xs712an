.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final NO_SUBTITLES_VSS_ID:Ljava/lang/String; = "-"


# instance fields
.field public final format:I

.field public final languageCode:Ljava/lang/String;

.field public final languageName:Ljava/lang/String;

.field public final offlineSubtitlesPath:Ljava/lang/String;

.field public final sourceLanguageCode:Ljava/lang/String;

.field public final trackName:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;

.field public final vssId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ab;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ab;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 9

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/youtube/datalib/legacy/model/aa;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "languageCode cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    const-string v0, "sourceLanguageCode cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageName:Ljava/lang/String;

    const-string v0, "trackName cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->format:I

    iput-object p7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->offlineSubtitlesPath:Ljava/lang/String;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->vssId:Ljava/lang/String;

    return-void
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 9

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createDisableSubtitleOption(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    const-string v1, ""

    const-string v2, ""

    const-string v4, ""

    const-string v5, ""

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string v8, "-"

    move-object v3, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createIncomplete(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 9

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v6, p3

    move-object v7, v5

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createMdx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    const/4 v7, 0x0

    const-string v8, "-"

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createOffline(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 9

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createTranslated(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 9

    const/4 v3, 0x0

    const-string v0, "fromTrack cannot be null"

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    const-string v1, "fromTrack cannot be an incomplete subtitle"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->format:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->vssId:Ljava/lang/String;

    move-object v1, p1

    move-object v7, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final createForOffline(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->format:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->vssId:Ljava/lang/String;

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public final isAutoTranslated()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDisableOption()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->isAutoTranslated()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->format:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->offlineSubtitlesPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->vssId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
