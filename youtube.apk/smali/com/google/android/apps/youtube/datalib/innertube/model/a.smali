.class public Lcom/google/android/apps/youtube/datalib/innertube/model/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/a/a/a/a/ab;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Lcom/google/android/apps/youtube/datalib/innertube/model/aw;

.field private e:Ljava/lang/CharSequence;

.field private f:Z

.field private g:Ljava/util/List;

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/util/List;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ab;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/ab;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-object v0, v0, Lcom/google/a/a/a/a/ab;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/ab;->i:Z

    return v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-object v0, v0, Lcom/google/a/a/a/a/ab;->j:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/innertube/model/aw;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->d:Lcom/google/android/apps/youtube/datalib/innertube/model/aw;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/aw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-object v1, v1, Lcom/google/a/a/a/a/ab;->f:Lcom/google/a/a/a/a/vm;

    iget-object v1, v1, Lcom/google/a/a/a/a/vm;->b:Lcom/google/a/a/a/a/vn;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/aw;-><init>(Lcom/google/a/a/a/a/vn;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->d:Lcom/google/android/apps/youtube/datalib/innertube/model/aw;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->d:Lcom/google/android/apps/youtube/datalib/innertube/model/aw;

    return-object v0
.end method

.method public final e()V
    .locals 10

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-object v3, v0, Lcom/google/a/a/a/a/ab;->h:[Lcom/google/a/a/a/a/vq;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    iget-object v5, v0, Lcom/google/a/a/a/a/vq;->c:Lcom/google/a/a/a/a/wa;

    if-eqz v5, :cond_0

    iget-object v0, v0, Lcom/google/a/a/a/a/vq;->c:Lcom/google/a/a/a/a/wa;

    iget-object v5, v0, Lcom/google/a/a/a/a/wa;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v5}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->e:Ljava/lang/CharSequence;

    iget-object v5, v0, Lcom/google/a/a/a/a/wa;->c:[Lcom/google/a/a/a/a/vz;

    new-instance v0, Ljava/util/ArrayList;

    array-length v6, v5

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->g:Ljava/util/List;

    array-length v6, v5

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->g:Ljava/util/List;

    new-instance v9, Lcom/google/android/apps/youtube/datalib/innertube/model/ay;

    invoke-direct {v9, v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/ay;-><init>(Lcom/google/a/a/a/a/vz;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v5, v0, Lcom/google/a/a/a/a/vq;->b:Lcom/google/a/a/a/a/vk;

    if-eqz v5, :cond_1

    iget-object v0, v0, Lcom/google/a/a/a/a/vq;->b:Lcom/google/a/a/a/a/vk;

    iget-object v5, v0, Lcom/google/a/a/a/a/vk;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v5}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->h:Ljava/lang/CharSequence;

    iget-object v5, v0, Lcom/google/a/a/a/a/vk;->c:[Lcom/google/a/a/a/a/vj;

    new-instance v0, Ljava/util/ArrayList;

    array-length v6, v5

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->i:Ljava/util/List;

    array-length v6, v5

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->i:Ljava/util/List;

    new-instance v9, Lcom/google/android/apps/youtube/datalib/innertube/model/av;

    invoke-direct {v9, v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/av;-><init>(Lcom/google/a/a/a/a/vj;)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->f:Z

    return-void
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->g:Ljava/util/List;

    return-object v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->i:Ljava/util/List;

    return-object v0
.end method

.method public final j()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->j:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-object v0, v0, Lcom/google/a/a/a/a/ab;->g:Lcom/google/a/a/a/a/vv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-object v0, v0, Lcom/google/a/a/a/a/ab;->g:Lcom/google/a/a/a/a/vv;

    iget-object v0, v0, Lcom/google/a/a/a/a/vv;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->j:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->k:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-object v0, v0, Lcom/google/a/a/a/a/ab;->g:Lcom/google/a/a/a/a/vv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a:Lcom/google/a/a/a/a/ab;

    iget-object v0, v0, Lcom/google/a/a/a/a/ab;->g:Lcom/google/a/a/a/a/vv;

    iget-object v1, v0, Lcom/google/a/a/a/a/vv;->c:[Lcom/google/a/a/a/a/vw;

    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->k:Ljava/util/List;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->k:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/ax;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/ax;-><init>(Lcom/google/a/a/a/a/vw;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->k:Ljava/util/List;

    return-object v0
.end method
