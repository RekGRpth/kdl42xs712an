.class public final enum Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

.field public static final enum REASON_CLIENT_OFFLINE_AD_ASSET_EXPIRED:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

.field public static final enum REASON_CLIENT_OFFLINE_AD_ASSET_FREQUENCY_CAP:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

.field public static final enum REASON_CLIENT_OFFLINE_AD_ASSET_NOT_READY:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

.field public static final enum REASON_CLIENT_OFFLINE_INACTIVE_USER:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

.field public static final enum REASON_CLIENT_OFFLINE_INSTREAM_FREQUENCY_CAP:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;


# instance fields
.field public final reasonValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    const-string v1, "REASON_CLIENT_OFFLINE_INSTREAM_FREQUENCY_CAP"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_INSTREAM_FREQUENCY_CAP:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    const-string v1, "REASON_CLIENT_OFFLINE_AD_ASSET_FREQUENCY_CAP"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_FREQUENCY_CAP:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    const-string v1, "REASON_CLIENT_OFFLINE_AD_ASSET_EXPIRED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_EXPIRED:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    const-string v1, "REASON_CLIENT_OFFLINE_INACTIVE_USER"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_INACTIVE_USER:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    const-string v1, "REASON_CLIENT_OFFLINE_AD_ASSET_NOT_READY"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_NOT_READY:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_INSTREAM_FREQUENCY_CAP:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_FREQUENCY_CAP:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_EXPIRED:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_INACTIVE_USER:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_NOT_READY:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->reasonValue:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->reasonValue:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
