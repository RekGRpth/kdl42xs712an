.class public final Lcom/google/android/apps/youtube/datalib/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;
.implements Ljava/util/concurrent/Future;


# instance fields
.field private final a:Lcom/android/volley/toolbox/v;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/android/volley/toolbox/v;->a()Lcom/android/volley/toolbox/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/k;->a:Lcom/android/volley/toolbox/v;

    return-void
.end method

.method public static a()Lcom/google/android/apps/youtube/datalib/a/k;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/a/k;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/a/k;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/k;->a:Lcom/android/volley/toolbox/v;

    invoke-virtual {v0, p1}, Lcom/android/volley/toolbox/v;->a(Lcom/android/volley/VolleyError;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/k;->a:Lcom/android/volley/toolbox/v;

    invoke-virtual {v0, p1}, Lcom/android/volley/toolbox/v;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/k;->a:Lcom/android/volley/toolbox/v;

    invoke-virtual {v0, p1}, Lcom/android/volley/toolbox/v;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/k;->a:Lcom/android/volley/toolbox/v;

    invoke-virtual {v0}, Lcom/android/volley/toolbox/v;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/k;->a:Lcom/android/volley/toolbox/v;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/volley/toolbox/v;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final isCancelled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/k;->a:Lcom/android/volley/toolbox/v;

    invoke-virtual {v0}, Lcom/android/volley/toolbox/v;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public final isDone()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/k;->a:Lcom/android/volley/toolbox/v;

    invoke-virtual {v0}, Lcom/android/volley/toolbox/v;->isDone()Z

    move-result v0

    return v0
.end method
