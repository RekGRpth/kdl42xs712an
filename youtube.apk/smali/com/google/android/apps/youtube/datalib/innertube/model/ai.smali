.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/qm;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/qm;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->a:Lcom/google/a/a/a/a/qm;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/aj;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->a:Lcom/google/a/a/a/a/qm;

    iget-object v0, v0, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/a/a/a/a/qn;->b:Lcom/google/a/a/a/a/qq;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    iget-object v0, v0, Lcom/google/a/a/a/a/qn;->b:Lcom/google/a/a/a/a/qq;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;-><init>(Lcom/google/a/a/a/a/qq;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    return-object v0
.end method

.method public final b()Lcom/google/a/a/a/a/dq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->a:Lcom/google/a/a/a/a/qm;

    iget-object v0, v0, Lcom/google/a/a/a/a/qm;->g:Lcom/google/a/a/a/a/dq;

    return-object v0
.end method

.method public final c()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->a:Lcom/google/a/a/a/a/qm;

    iget-object v1, v1, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/a/a/a/a/qn;->b:Lcom/google/a/a/a/a/qq;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, v1, Lcom/google/a/a/a/a/qn;->b:Lcom/google/a/a/a/a/qq;

    iget-object v1, v1, Lcom/google/a/a/a/a/qq;->b:[Lcom/google/a/a/a/a/qt;

    array-length v1, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
