.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

.field private final c:J

.field private final d:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/v;JJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    iput-wide p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->c:J

    iput-wide p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->d:J

    return-void
.end method

.method private j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/innertube/model/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    return-object v0
.end method

.method public final c()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->c:J

    return-wide v0
.end method

.method public final d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->d:J

    return-wide v0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->j()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->h()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()J
    .locals 6

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->d:J

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->h()I

    move-result v2

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final i()Lcom/google/android/apps/youtube/datalib/legacy/model/w;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/v;)Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a(J)Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->d:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v0

    return-object v0
.end method
