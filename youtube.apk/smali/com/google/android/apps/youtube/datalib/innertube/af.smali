.class public final Lcom/google/android/apps/youtube/datalib/innertube/af;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private c:Ljava/util/Collection;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->c:Ljava/util/Collection;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->d:Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/af;->a([B)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/af;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/util/Collection;)Lcom/google/android/apps/youtube/datalib/innertube/af;
    .locals 0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->c:Ljava/util/Collection;

    return-object p0
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "player/refresh"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/af;->b()V

    new-instance v1, Lcom/google/a/a/a/a/ma;

    invoke-direct {v1}, Lcom/google/a/a/a/a/ma;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/af;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v0

    iput-object v0, v1, Lcom/google/a/a/a/a/ma;->b:Lcom/google/a/a/a/a/ii;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->d:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/a/a/a/a/ma;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/af;->c:Ljava/util/Collection;

    iget-object v2, v1, Lcom/google/a/a/a/a/ma;->c:[Lcom/google/a/a/a/a/mm;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/a/a/a/a/mm;

    iput-object v0, v1, Lcom/google/a/a/a/a/ma;->c:[Lcom/google/a/a/a/a/mm;

    return-object v1
.end method
