.class public Lcom/google/android/apps/youtube/datalib/distiller/model/b;
.super Lcom/google/android/apps/youtube/datalib/distiller/model/c;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/c;-><init>(Lorg/json/JSONObject;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->b:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->b:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->a:Ljava/lang/String;

    return-object v0
.end method
