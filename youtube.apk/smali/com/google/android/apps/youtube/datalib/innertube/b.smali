.class public abstract Lcom/google/android/apps/youtube/datalib/innertube/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/s;


# static fields
.field public static final a:[B


# instance fields
.field b:Ljava/util/Map;

.field private c:[B

.field private d:Z

.field private final e:Lcom/google/android/apps/youtube/datalib/innertube/p;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/b;->a:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->d:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->e:Lcom/google/android/apps/youtube/datalib/innertube/p;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->b:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->b:Ljava/util/Map;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->b:Ljava/util/Map;

    return-object v0
.end method

.method public final a([B)V
    .locals 0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->c:[B

    return-void
.end method

.method public final b()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/b;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->c:[B

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must set clickTrackingParams."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected abstract c()V
.end method

.method public final d()Lcom/google/a/a/a/a/ii;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->e:Lcom/google/android/apps/youtube/datalib/innertube/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/p;->a()Lcom/google/a/a/a/a/ii;

    move-result-object v0

    new-instance v1, Lcom/google/a/a/a/a/cs;

    invoke-direct {v1}, Lcom/google/a/a/a/a/cs;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->c:[B

    iput-object v2, v1, Lcom/google/a/a/a/a/cs;->b:[B

    iput-object v1, v0, Lcom/google/a/a/a/a/ii;->f:Lcom/google/a/a/a/a/cs;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/b;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    const-string v2, "OVERRIDE"

    iput-object v2, v1, Lcom/google/a/a/a/a/ct;->d:Ljava/lang/String;

    :cond_0
    return-object v0
.end method
