.class final Lcom/google/android/apps/youtube/datalib/legacy/model/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 3

    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;-><init>(Landroid/os/Parcel;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to parse VastAd\'s annotations parcel"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/az;->a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method
