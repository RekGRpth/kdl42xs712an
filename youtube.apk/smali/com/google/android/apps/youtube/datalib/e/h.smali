.class final Lcom/google/android/apps/youtube/datalib/e/h;
.super Lcom/google/android/apps/youtube/datalib/a/r;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/a/a/c;

.field private final b:Lcom/google/android/apps/youtube/common/e/b;

.field private final c:Lcom/google/android/apps/youtube/datalib/offline/m;

.field private final d:Lcom/google/android/apps/youtube/datalib/a/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/a/a/c;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;Lcom/google/android/apps/youtube/datalib/offline/m;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/c;->c()I

    move-result v1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0, p2}, Lcom/google/android/apps/youtube/datalib/a/r;-><init>(ILjava/lang/String;Lcom/android/volley/n;)V

    new-instance v0, Lcom/android/volley/d;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p4}, Lcom/google/android/apps/youtube/datalib/config/c;->e()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    long-to-int v1, v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, Lcom/android/volley/d;-><init>(IIF)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/e/h;->a(Lcom/android/volley/q;)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/youtube/datalib/e/h;->a(Z)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/a/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->d:Lcom/google/android/apps/youtube/datalib/a/l;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->c:Lcom/google/android/apps/youtube/datalib/offline/m;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/j;)Lcom/android/volley/m;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, v0}, Lcom/android/volley/m;->a(Ljava/lang/Object;Lcom/android/volley/b;)Lcom/android/volley/m;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->c:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/offline/m;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->d:Lcom/google/android/apps/youtube/datalib/a/l;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/datalib/a/l;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/android/volley/VolleyError;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/a/b;->a(Lcom/android/volley/VolleyError;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->c:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/offline/m;->d()V

    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/datalib/a/r;->b(Lcom/android/volley/VolleyError;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->c:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/offline/m;->e()V

    goto :goto_0
.end method

.method public final h()Ljava/util/Map;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/h;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/c;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/b;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/b;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/b;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const-string v0, "X-Goog-Request-Time"

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/e/h;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "X-Goog-Event-Time"

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/e/h;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/a/a/c;->i()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1
.end method
