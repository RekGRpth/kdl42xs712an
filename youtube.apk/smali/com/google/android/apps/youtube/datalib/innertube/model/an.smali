.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private final a:Lcom/google/a/a/a/a/sp;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/sp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/sp;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->a:Lcom/google/a/a/a/a/sp;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->a:Lcom/google/a/a/a/a/sp;

    iget-object v0, v0, Lcom/google/a/a/a/a/sp;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->a:Lcom/google/a/a/a/a/sp;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/sp;->d:Z

    return v0
.end method

.method public final c()Lcom/google/android/apps/youtube/datalib/innertube/model/aj;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->a:Lcom/google/a/a/a/a/sp;

    iget-object v0, v0, Lcom/google/a/a/a/a/sp;->e:Lcom/google/a/a/a/a/so;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/a/a/a/a/so;->b:Lcom/google/a/a/a/a/qq;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    iget-object v0, v0, Lcom/google/a/a/a/a/so;->b:Lcom/google/a/a/a/a/qq;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;-><init>(Lcom/google/a/a/a/a/qq;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->a:Lcom/google/a/a/a/a/sp;

    iget-object v0, v0, Lcom/google/a/a/a/a/sp;->b:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method
