.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/bd;
.super Lcom/google/android/apps/youtube/datalib/legacy/a/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bd;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lorg/json/JSONObject;I)Lcom/google/android/apps/youtube/datalib/legacy/a/a;
    .locals 4

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    const-string v1, "offset"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "isPercentageOffset"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "pingUri"

    invoke-static {p1, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/bd;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;-><init>(IZLandroid/net/Uri;)V

    return-object v0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 2

    const-string v0, "offset"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bd;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getOffset()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "isPercentageOffset"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bd;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->isPercentageOffset()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "pingUri"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bd;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getPingUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bd;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method
