.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/md;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/md;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->a:Lcom/google/a/a/a/a/md;

    iget-object v0, p1, Lcom/google/a/a/a/a/md;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->c:Ljava/lang/String;

    iget v0, p1, Lcom/google/a/a/a/a/md;->e:I

    if-eqz v0, :cond_1

    iget v0, p1, Lcom/google/a/a/a/a/md;->e:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    :goto_0
    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    if-ne v0, v3, :cond_3

    iput v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->d:I

    iput v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->e:I

    :goto_1
    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    if-ne v0, v2, :cond_4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->f:Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iput v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    goto :goto_0

    :cond_2
    iput v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    goto :goto_0

    :cond_3
    iget v0, p1, Lcom/google/a/a/a/a/md;->c:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->d:I

    iget v0, p1, Lcom/google/a/a/a/a/md;->d:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->e:I

    goto :goto_1

    :cond_4
    iget-object v0, p1, Lcom/google/a/a/a/a/md;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/google/a/a/a/a/md;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->f:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/md;->g:Lcom/google/a/a/a/a/lz;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "OfflineState.offline_refresh_message cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "OfflineState.short_message cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/a/a/a/a/md;
    .locals 2

    new-instance v0, Lcom/google/a/a/a/a/md;

    invoke-direct {v0}, Lcom/google/a/a/a/a/md;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->a:Lcom/google/a/a/a/a/md;

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/md;

    return-object v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    return v0
.end method

.method public final c()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->d:I

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->e:I

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lcom/google/a/a/a/a/lz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->a:Lcom/google/a/a/a/a/md;

    iget-object v0, v0, Lcom/google/a/a/a/a/md;->g:Lcom/google/a/a/a/a/lz;

    return-object v0
.end method
