.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final fullText:Ljava/lang/String;

.field public final settings:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

.field public final snapshotTimeMillis:I

.field public final text:Ljava/lang/String;

.field public final windowId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/aj;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/aj;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->windowId:I

    iput p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->snapshotTimeMillis:I

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    iput-object p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->fullText:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;-><init>(IILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/youtube/datalib/legacy/model/ai;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "id: %d time: %d text: %s fullText: %s settings: %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->windowId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->snapshotTimeMillis:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->fullText:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->windowId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->snapshotTimeMillis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->text:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->fullText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSnapshot;->settings:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
