.class public Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/datalib/legacy/a/a;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final NO_TRACKING_AUTH_SETTINGS:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;


# instance fields
.field private final shouldAddUserAuth:Z

.field private final shouldAddVisitorId:Z

.field private final urlMatchPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    const-string v1, "^invalidurl$"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;-><init>(Ljava/lang/String;ZZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->NO_TRACKING_AUTH_SETTINGS:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ax;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ax;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v1, :cond_1

    :goto_1
    invoke-direct {p0, v3, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;-><init>(Ljava/lang/String;ZZ)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "urlMatchRegex cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->urlMatchPattern:Ljava/util/regex/Pattern;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddUserAuth:Z

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddVisitorId:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->getUrlMatchPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->getUrlMatchPattern()Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddVisitorId()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddVisitorId()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddUserAuth()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddUserAuth()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic getConverter()Lcom/google/android/apps/youtube/datalib/legacy/a/b;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/ay;

    move-result-object v0

    return-object v0
.end method

.method public getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/ay;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ay;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ay;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;)V

    return-object v0
.end method

.method public getUrlMatchPattern()Ljava/util/regex/Pattern;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->urlMatchPattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public shouldAddUserAuth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddUserAuth:Z

    return v0
.end method

.method public shouldAddVisitorId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddVisitorId:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->getUrlMatchPattern()Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddVisitorId()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddUserAuth()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
