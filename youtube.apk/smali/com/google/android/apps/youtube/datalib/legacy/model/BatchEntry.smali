.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:I


# direct methods
.method private constructor <init>(Ljava/lang/Object;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->b:I

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->a:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;-><init>(Ljava/lang/Object;I)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->b:I

    iget v3, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->a:Ljava/lang/Object;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->a:Ljava/lang/Object;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->a:Ljava/lang/Object;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->b:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
