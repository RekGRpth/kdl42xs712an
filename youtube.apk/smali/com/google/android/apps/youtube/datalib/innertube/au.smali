.class public final Lcom/google/android/apps/youtube/datalib/innertube/au;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/a/a/a/a/fi;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    new-instance v0, Lcom/google/a/a/a/a/fi;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fi;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/au;->a([B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/au;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/utils/v;)Lcom/google/android/apps/youtube/datalib/innertube/au;
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/utils/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    const/4 v1, 0x3

    iput v1, v0, Lcom/google/a/a/a/a/fi;->l:I

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/a/a/a/a/fi;->l:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;)Lcom/google/android/apps/youtube/datalib/innertube/au;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->getType()I

    move-result v1

    iput v1, v0, Lcom/google/a/a/a/a/fi;->c:I

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;)Lcom/google/android/apps/youtube/datalib/innertube/au;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->getType()I

    move-result v1

    iput v1, v0, Lcom/google/a/a/a/a/fi;->b:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/au;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/au;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->d:Ljava/lang/String;

    return-object p0
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "search"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/au;->b()V

    new-instance v0, Lcom/google/a/a/a/a/ql;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ql;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/au;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/ql;->b:Lcom/google/a/a/a/a/ii;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    iget v1, v1, Lcom/google/a/a/a/a/fi;->c:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    iget v1, v1, Lcom/google/a/a/a/a/fi;->b:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    iget v1, v1, Lcom/google/a/a/a/a/fi;->l:I

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->e:Lcom/google/a/a/a/a/fi;

    iput-object v1, v0, Lcom/google/a/a/a/a/ql;->g:Lcom/google/a/a/a/a/fi;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/ql;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/au;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/ql;->f:Ljava/lang/String;

    goto :goto_0
.end method
