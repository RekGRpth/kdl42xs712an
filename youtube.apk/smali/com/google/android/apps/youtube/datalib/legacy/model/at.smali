.class final Lcom/google/android/apps/youtube/datalib/legacy/model/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/c;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/j;->b(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/a/c;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;-><init>(Lcom/google/android/apps/youtube/a/a/a/c;Lcom/google/android/apps/youtube/datalib/legacy/model/at;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/at;->a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    return-object v0
.end method
