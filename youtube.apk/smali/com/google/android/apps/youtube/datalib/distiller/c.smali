.class public final Lcom/google/android/apps/youtube/datalib/distiller/c;
.super Lcom/google/android/apps/youtube/datalib/distiller/i;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/distiller/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/distiller/c;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/youtube/datalib/distiller/c;
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->b:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/c;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "streams/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/distiller/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/activities/filtered"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/c;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final b()Ljava/util/Map;
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "maxComments"

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "pageToken"

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method protected final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->b:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/c;->b:I

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
