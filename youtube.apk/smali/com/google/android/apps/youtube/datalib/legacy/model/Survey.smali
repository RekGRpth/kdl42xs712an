.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/datalib/legacy/a/a;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final EMPTY:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;


# instance fields
.field private questions:Ljava/util/List;

.field private final surveyProto:Lcom/google/android/apps/youtube/a/a/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->EMPTY:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/aq;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/aq;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->surveyProto:Lcom/google/android/apps/youtube/a/a/a/b;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/a/a/a/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/apps/youtube/a/a/a/b;->b:[Lcom/google/android/apps/youtube/a/a/a/c;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->surveyProto:Lcom/google/android/apps/youtube/a/a/a/b;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/a/a/a/b;Lcom/google/android/apps/youtube/datalib/legacy/model/aq;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;-><init>(Lcom/google/android/apps/youtube/a/a/a/b;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Lcom/google/android/apps/youtube/a/a/a/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->surveyProto:Lcom/google/android/apps/youtube/a/a/a/b;

    return-object v0
.end method


# virtual methods
.method public final buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ar;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestions()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic getConverter()Lcom/google/android/apps/youtube/datalib/legacy/a/b;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/as;

    move-result-object v0

    return-object v0
.end method

.method public final getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/as;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/as;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/as;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)V

    return-object v0
.end method

.method public final getQuestion(I)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    return-object v0
.end method

.method public final getQuestions()Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->questions:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->questions:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->surveyProto:Lcom/google/android/apps/youtube/a/a/a/b;

    iget-object v1, v0, Lcom/google/android/apps/youtube/a/a/a/b;->b:[Lcom/google/android/apps/youtube/a/a/a/c;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->questions:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;-><init>(Lcom/google/android/apps/youtube/a/a/a/c;)V

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->questions:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->questions:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->questions:Ljava/util/List;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Survey "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->surveyProto:Lcom/google/android/apps/youtube/a/a/a/b;

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)V

    return-void
.end method
