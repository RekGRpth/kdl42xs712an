.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/j;
.super Lcom/google/android/apps/youtube/datalib/legacy/a/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/j;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lorg/json/JSONObject;I)Lcom/google/android/apps/youtube/datalib/legacy/a/a;
    .locals 5

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    const-string v1, "type"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "linkUrl"

    invoke-static {p1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/j;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "trackingUris"

    invoke-static {p1, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/j;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;-><init>(ILandroid/net/Uri;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 2

    const-string v0, "type"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/j;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "linkUrl"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/j;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getLinkUrl()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "title"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/j;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "trackingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/j;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getActionTrackingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/j;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method
