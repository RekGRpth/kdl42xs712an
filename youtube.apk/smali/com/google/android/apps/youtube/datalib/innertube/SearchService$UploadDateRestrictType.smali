.class public final enum Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

.field public static final enum UPLOAD_DATE_ANY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

.field public static final enum UPLOAD_DATE_THIS_MONTH:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

.field public static final enum UPLOAD_DATE_THIS_WEEK:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

.field public static final enum UPLOAD_DATE_TODAY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;


# instance fields
.field private final type:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    const-string v1, "UPLOAD_DATE_ANY"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_ANY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    const-string v1, "UPLOAD_DATE_TODAY"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_TODAY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    const-string v1, "UPLOAD_DATE_THIS_WEEK"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_THIS_WEEK:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    const-string v1, "UPLOAD_DATE_THIS_MONTH"

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_THIS_MONTH:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    new-array v0, v6, [Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_ANY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_TODAY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_THIS_WEEK:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_THIS_MONTH:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->type:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    return-object v0
.end method


# virtual methods
.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->type:I

    return v0
.end method
