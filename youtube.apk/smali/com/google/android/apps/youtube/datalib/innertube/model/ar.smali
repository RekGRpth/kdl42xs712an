.class public Lcom/google/android/apps/youtube/datalib/innertube/model/ar;
.super Lcom/google/android/apps/youtube/datalib/innertube/model/ak;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/rd;

.field private final b:Lcom/google/a/a/a/a/ue;

.field private c:Ljava/util/List;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/rd;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;-><init>(Lcom/google/a/a/a/a/rd;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/rd;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->a:Lcom/google/a/a/a/a/rd;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->a:Lcom/google/a/a/a/a/rd;

    iget-object v0, v0, Lcom/google/a/a/a/a/rd;->f:Lcom/google/a/a/a/a/re;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->a:Lcom/google/a/a/a/a/rd;

    iget-object v0, v0, Lcom/google/a/a/a/a/rd;->f:Lcom/google/a/a/a/a/re;

    iget-object v0, v0, Lcom/google/a/a/a/a/re;->e:Lcom/google/a/a/a/a/ue;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final g()Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->c:Ljava/util/List;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    iget-object v1, v1, Lcom/google/a/a/a/a/ue;->b:[Lcom/google/a/a/a/a/uf;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->c:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    iget-object v1, v0, Lcom/google/a/a/a/a/ue;->b:[Lcom/google/a/a/a/a/uf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/uf;->c:Lcom/google/a/a/a/a/de;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->c:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/f;

    iget-object v3, v3, Lcom/google/a/a/a/a/uf;->c:Lcom/google/a/a/a/a/de;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/f;-><init>(Lcom/google/a/a/a/a/de;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, v3, Lcom/google/a/a/a/a/uf;->b:Lcom/google/a/a/a/a/dj;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->c:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/h;

    iget-object v3, v3, Lcom/google/a/a/a/a/uf;->b:Lcom/google/a/a/a/a/dj;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/h;-><init>(Lcom/google/a/a/a/a/dj;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v4, v3, Lcom/google/a/a/a/a/uf;->d:Lcom/google/a/a/a/a/db;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->c:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/e;

    iget-object v3, v3, Lcom/google/a/a/a/a/uf;->d:Lcom/google/a/a/a/a/db;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;-><init>(Lcom/google/a/a/a/a/db;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->c:Ljava/util/List;

    return-object v0
.end method

.method public final h()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    iget v0, v0, Lcom/google/a/a/a/a/ue;->c:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    iget v0, v0, Lcom/google/a/a/a/a/ue;->c:I

    goto :goto_0
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    iget-object v0, v0, Lcom/google/a/a/a/a/ue;->d:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    iget-object v0, v0, Lcom/google/a/a/a/a/ue;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final j()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    iget-object v0, v0, Lcom/google/a/a/a/a/ue;->e:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->b:Lcom/google/a/a/a/a/ue;

    iget-object v0, v0, Lcom/google/a/a/a/a/ue;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;->e:Ljava/lang/CharSequence;

    return-object v0
.end method
