.class public Lcom/google/android/apps/youtube/datalib/innertube/model/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private final a:Lcom/google/a/a/a/a/uw;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private final d:Lcom/google/a/a/a/a/kz;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Lcom/google/android/apps/youtube/datalib/innertube/model/am;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/uw;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/uw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v0, p1, Lcom/google/a/a/a/a/uw;->e:Lcom/google/a/a/a/a/kz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    iget-object v0, v0, Lcom/google/a/a/a/a/am;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    iget-object v0, v0, Lcom/google/a/a/a/a/am;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->b:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    iget-object v0, v0, Lcom/google/a/a/a/a/jx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    iget-object v0, v0, Lcom/google/a/a/a/a/jx;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->b:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v0, v0, Lcom/google/a/a/a/a/uw;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v0, v0, Lcom/google/a/a/a/a/uw;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->f:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v1, v1, Lcom/google/a/a/a/a/uw;->b:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/innertube/model/am;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v0, v0, Lcom/google/a/a/a/a/uw;->g:Lcom/google/a/a/a/a/ux;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v0, v0, Lcom/google/a/a/a/a/uw;->g:Lcom/google/a/a/a/a/ux;

    iget-object v0, v0, Lcom/google/a/a/a/a/ux;->b:Lcom/google/a/a/a/a/sd;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v1, v1, Lcom/google/a/a/a/a/uw;->g:Lcom/google/a/a/a/a/ux;

    iget-object v1, v1, Lcom/google/a/a/a/a/ux;->b:Lcom/google/a/a/a/a/sd;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;-><init>(Lcom/google/a/a/a/a/sd;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v0, v0, Lcom/google/a/a/a/a/uw;->d:Lcom/google/a/a/a/a/si;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a:Lcom/google/a/a/a/a/uw;

    iget-object v0, v0, Lcom/google/a/a/a/a/uw;->d:Lcom/google/a/a/a/a/si;

    iget v1, v0, Lcom/google/a/a/a/a/si;->b:I

    const/4 v0, 0x2

    if-eq v1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-instance v2, Lcom/google/a/a/a/a/sd;

    invoke-direct {v2}, Lcom/google/a/a/a/a/sd;-><init>()V

    iput-boolean v0, v2, Lcom/google/a/a/a/a/sd;->e:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/a/a/a/a/sd;->h:Ljava/lang/String;

    iput v1, v2, Lcom/google/a/a/a/a/sd;->g:I

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;-><init>(Lcom/google/a/a/a/a/sd;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method
