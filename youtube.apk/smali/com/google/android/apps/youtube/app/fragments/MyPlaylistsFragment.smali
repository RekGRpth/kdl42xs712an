.class public Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/dq;


# instance fields
.field private Y:Ljava/util/concurrent/atomic/AtomicReference;

.field private Z:Ljava/util/concurrent/Executor;

.field private a:Landroid/content/res/Resources;

.field private aa:Lcom/google/android/apps/youtube/app/ui/do;

.field private ab:Lcom/google/android/apps/youtube/app/adapter/af;

.field private ac:Lcom/google/android/apps/youtube/app/ui/v;

.field private ad:Lcom/google/android/apps/youtube/core/identity/o;

.field private ae:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;

.field private af:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

.field private b:Lcom/google/android/apps/youtube/core/async/af;

.field private d:Lcom/google/android/apps/youtube/common/c/a;

.field private e:Lcom/google/android/apps/youtube/core/client/bc;

.field private f:Lcom/google/android/apps/youtube/core/client/bj;

.field private g:Lcom/google/android/apps/youtube/core/identity/l;

.field private h:Lcom/google/android/apps/youtube/core/aw;

.field private i:Lcom/google/android/apps/youtube/app/ui/et;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->a:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/k;->n:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->af:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/am;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/youtube/app/fragments/am;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->n(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ae:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->Y:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ab:Lcom/google/android/apps/youtube/app/adapter/af;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/ui/do;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/do;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const/4 v3, 0x0

    sget v0, Lcom/google/android/youtube/l;->ap:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ad;->b(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ac:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ac:Lcom/google/android/apps/youtube/app/ui/v;

    sget v1, Lcom/google/android/youtube/p;->aK:I

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/ak;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/ak;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ac:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Z)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ab:Lcom/google/android/apps/youtube/app/adapter/af;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ab:Lcom/google/android/apps/youtube/app/adapter/af;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    sget v0, Lcom/google/android/youtube/j;->cI:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/ui/PagedView;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/do;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->b:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/ui/do;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/dq;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/do;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/do;

    const-string v1, "playlists_helper"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/do;->a(Landroid/os/Bundle;)V

    :cond_0
    return-object v7
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    sget v0, Lcom/google/android/youtube/p;->ag:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->j()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->g:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/client/bc;->o()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->k()Lcom/google/android/apps/youtube/core/identity/o;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ad:Lcom/google/android/apps/youtube/core/identity/o;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->Y:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aI()Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->Z:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M()Lcom/google/android/apps/youtube/app/compat/o;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/m;->e:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/o;->a(ILcom/google/android/apps/youtube/app/compat/j;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->contentUri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/am;->b(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/q;)Z
    .locals 3

    invoke-interface {p1}, Lcom/google/android/apps/youtube/app/compat/q;->e()I

    move-result v0

    sget v1, Lcom/google/android/youtube/j;->cq:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ad:Lcom/google/android/apps/youtube/core/identity/o;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/al;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/al;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/o;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/v;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/q;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_playlist"

    return-object v0
.end method

.method public final d()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->L()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Landroid/support/v4/app/l;

    move-result-object v1

    const-string v0, "CreatePlaylistDialogFragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ae:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ae:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ae:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ae:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;->a(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    const-string v0, "DeletePlaylistDialogFragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->af:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->af:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->af:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->af:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->a(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/do;

    if-eqz v0, :cond_0

    const-string v0, "playlists_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/do;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/do;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->ac:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/v;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->L()V

    return-void
.end method

.method public final r()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->g:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/do;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->m()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/do;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    goto :goto_0
.end method
