.class final Lcom/google/android/apps/youtube/app/ui/ht;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/hj;

.field final synthetic b:Lcom/google/android/apps/youtube/app/ui/hr;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/hr;Lcom/google/android/apps/youtube/app/ui/hj;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ht;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->b(Lcom/google/android/apps/youtube/app/ui/hr;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->b(Lcom/google/android/apps/youtube/app/ui/hr;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->b(Lcom/google/android/apps/youtube/app/ui/hr;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->a(Lcom/google/android/apps/youtube/app/ui/hr;)Lcom/google/android/apps/youtube/app/adapter/ad;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/app/adapter/ad;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->a(Lcom/google/android/apps/youtube/app/ui/hr;)Lcom/google/android/apps/youtube/app/adapter/ad;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/app/adapter/ad;->d(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->f(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "WatchLater"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->e(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/hr;->c(Lcom/google/android/apps/youtube/app/ui/hr;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/id;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v4, v4, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v4, v4, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v5, v5, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v5}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/id;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->k(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->a(Lcom/google/android/apps/youtube/app/ui/hr;)Lcom/google/android/apps/youtube/app/adapter/ad;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/app/adapter/ad;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->h(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/identity/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/ho;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v3, v3, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/ui/hr;->c(Lcom/google/android/apps/youtube/app/ui/hr;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/ho;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/o;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/v;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->a(Lcom/google/android/apps/youtube/app/ui/hr;)Lcom/google/android/apps/youtube/app/adapter/ad;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/app/adapter/ad;->e(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->i(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/app/ui/hp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/hr;->c(Lcom/google/android/apps/youtube/app/ui/hr;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hp;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->a(Lcom/google/android/apps/youtube/app/ui/hr;)Lcom/google/android/apps/youtube/app/adapter/ad;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/app/adapter/ad;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->h(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/identity/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/hu;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/hu;-><init>(Lcom/google/android/apps/youtube/app/ui/ht;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/o;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/v;)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/hj;->f(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "SaveToPlaylist"

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ht;->b:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/hr;->c(Lcom/google/android/apps/youtube/app/ui/hr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    goto/16 :goto_0
.end method
