.class public final Lcom/google/android/apps/youtube/app/ui/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/widget/ListView;

.field private final b:I

.field private final c:Ljava/util/List;

.field private d:Lcom/google/android/apps/youtube/app/ui/u;

.field private e:Z

.field private f:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/app/aw;Landroid/view/View;Landroid/widget/ListView;ILcom/google/android/apps/youtube/core/player/ae;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->a:Landroid/widget/ListView;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->e:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->c:Ljava/util/List;

    invoke-virtual {p5}, Lcom/google/android/apps/youtube/app/aw;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->c:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/b;

    sget v2, Lcom/google/android/youtube/j;->t:I

    invoke-virtual {p6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual/range {p9 .. p9}, Lcom/google/android/apps/youtube/core/player/ae;->a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v3

    invoke-direct {v1, p1, p3, v2, v3}, Lcom/google/android/apps/youtube/app/ui/b;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Landroid/view/View;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/s;->c:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gt;

    sget v1, Lcom/google/android/youtube/j;->f:I

    invoke-virtual {p6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/gt;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/am;Landroid/view/View;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/s;->b()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/s;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/u;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/ui/u;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    :cond_1
    return-void
.end method

.method private handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/t;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/s;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/s;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eq v1, v0, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/s;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/u;->b()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/u;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/u;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/s;->a:Landroid/widget/ListView;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/s;->b:I

    neg-int v0, v0

    const/16 v3, 0x190

    invoke-virtual {v1, v2, v0, v3}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(III)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/u;->d()V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->e:Z

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/u;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/u;->c()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->d:Lcom/google/android/apps/youtube/app/ui/u;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/s;->e:Z

    return-void
.end method
