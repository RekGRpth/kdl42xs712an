.class public Lcom/google/android/apps/youtube/app/YouTubeApplication;
.super Lcom/google/android/apps/youtube/core/BaseApplication;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/offline/store/r;


# instance fields
.field private a:I

.field private b:Lcom/google/android/apps/youtube/app/ax;

.field private final c:Lcom/google/android/apps/youtube/common/c/d;

.field private final d:Lcom/google/android/apps/youtube/common/c/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/app/as;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/as;-><init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c:Lcom/google/android/apps/youtube/common/c/d;

    new-instance v0, Lcom/google/android/apps/youtube/app/at;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/at;-><init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d:Lcom/google/android/apps/youtube/common/c/d;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/YouTubeApplication;)Lcom/google/android/apps/youtube/app/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    return-object v0
.end method

.method private a(I)V
    .locals 4

    const/4 v3, 0x2

    iput p1, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->a:I

    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    const-string v0, "Signed out"

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "Engagement"

    invoke-interface {v1, v3, v2, v0, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(ILjava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    const-string v0, "Signed in"

    goto :goto_0

    :cond_1
    if-nez p1, :cond_2

    const-string v0, "No subscriptions"

    goto :goto_0

    :cond_2
    const-string v0, "Subscriptions"

    goto :goto_0
.end method


# virtual methods
.method protected final a()Z
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.google.android.youtube.api.service.START"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v3, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    iget-object v5, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz v5, :cond_0

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v3, Landroid/content/pm/ServiceInfo;->processName:Ljava/lang/String;

    :goto_0
    if-nez v3, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v6, v4, :cond_4

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method protected final synthetic b()Lcom/google/android/apps/youtube/core/a;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/ax;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ax;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected final c()V
    .locals 13

    const/4 v12, 0x3

    const/4 v11, 0x0

    const/4 v10, 0x1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ax;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aZ()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/ytremote/a/d/a;->a(Ljava/lang/String;)V

    sput-object v2, Lcom/google/android/apps/youtube/core/async/u;->a:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v6

    const-class v3, Lcom/google/android/apps/youtube/core/identity/aj;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c:Lcom/google/android/apps/youtube/common/c/d;

    invoke-virtual {v6, p0, v3, v5}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    const-class v3, Lcom/google/android/apps/youtube/core/identity/ai;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d:Lcom/google/android/apps/youtube/common/c/d;

    invoke-virtual {v6, p0, v3, v5}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    new-instance v3, Lcom/google/android/apps/youtube/app/b/e;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/b/e;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->K()Lcom/google/android/apps/youtube/core/client/a/c;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/youtube/app/b/e;->a(Lcom/google/android/apps/youtube/core/client/a/c;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/b/n;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/b/n;-><init>()V

    invoke-virtual {v6, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->ac()Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->ad()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->T()Lcom/google/android/apps/youtube/app/notification/b;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aa()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->D()Lcom/google/android/apps/youtube/core/client/ax;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->P()Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->Q()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->R()Lcom/google/android/apps/youtube/app/remote/bw;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->X()Lcom/google/android/apps/youtube/app/remote/aj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aM()Lorg/apache/http/impl/client/AbstractHttpClient;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->D()Lcom/google/android/apps/youtube/core/client/ax;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/client/ax;->a()Lcom/google/android/apps/youtube/core/utils/am;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/http/impl/client/AbstractHttpClient;->setCookieStore(Lorg/apache/http/client/CookieStore;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/youtube/common/network/h;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Landroid/content/ComponentName;

    const-string v5, "com.google.android.youtube.ManageNetworkUsageActivity"

    invoke-direct {v3, p0, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v7

    if-eq v7, v10, :cond_0

    const-string v7, "Enabling network usage management"

    invoke-static {v7}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v10, v10}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->z()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v5, "download_only_while_charging"

    invoke-interface {v3, v5, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v5, "transfer_max_connections"

    invoke-interface {v3, v5, v12}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v5, "identity_version"

    invoke-interface {v3, v5, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Ljava/lang/String;Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/core/utils/Util$StartupType;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/google/android/youtube/p;->bX:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v7

    const-string v8, "FormFactor"

    const/4 v9, 0x2

    invoke-interface {v7, v12, v8, v5, v9}, Lcom/google/android/apps/youtube/core/Analytics;->a(ILjava/lang/String;Ljava/lang/String;I)V

    const-string v5, "Startup"

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/utils/Util$StartupType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7, v5, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->z()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->av()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    new-instance v5, Lcom/google/android/apps/youtube/app/au;

    invoke-direct {v5, p0, v3}, Lcom/google/android/apps/youtube/app/au;-><init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;[Ljava/io/File;)V

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/au;->start()V

    :cond_2
    const/4 v3, -0x2

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->a(I)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v3

    new-instance v5, Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-direct {v5, p0}, Lcom/google/android/apps/youtube/app/remote/bb;-><init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;)V

    invoke-virtual {v3, v5}, Lcom/google/android/apps/youtube/app/remote/an;->a(Lcom/google/android/apps/youtube/app/remote/ap;)V

    const-string v3, "1001680686"

    const-string v5, "4dahCKKczAYQrt7R3QM"

    const-string v7, "<Android_YT_Open_App>"

    const/4 v8, 0x0

    invoke-static {p0, v3, v5, v7, v8}, Lcom/google/ads/conversiontracking/GoogleConversionPing;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->l()Lcom/android/volley/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/volley/l;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->z()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "pending_notification_registration"

    invoke-interface {v2, v3, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->T()Lcom/google/android/apps/youtube/app/notification/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/notification/b;->a()V

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "pending_notification_registration"

    invoke-interface {v2, v3, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_4
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bi()Lcom/google/android/apps/youtube/common/d/d;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/datalib/offline/h;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->n()Lcom/google/android/apps/youtube/datalib/offline/j;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/google/android/apps/youtube/datalib/offline/h;-><init>(Lcom/google/android/apps/youtube/datalib/offline/j;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/d/d;->a(Lcom/google/android/apps/youtube/common/d/c;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bh()Lcom/google/android/apps/youtube/common/d/j;

    move-result-object v3

    new-instance v2, Lcom/google/android/apps/youtube/datalib/offline/p;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->n()Lcom/google/android/apps/youtube/datalib/offline/j;

    move-result-object v5

    invoke-direct {v2, v5, v1}, Lcom/google/android/apps/youtube/datalib/offline/p;-><init>(Lcom/google/android/apps/youtube/datalib/offline/j;Lcom/google/android/apps/youtube/common/network/h;)V

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/common/d/j;->a(Lcom/google/android/apps/youtube/common/d/i;)V

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/google/android/apps/youtube/datalib/offline/o;->a(J)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/common/d/j;->b(Lcom/google/android/apps/youtube/a/a/g;)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/c;->a(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/player/c;->a(Lcom/google/android/apps/youtube/common/e/b;)V

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/c;->a(Lcom/google/android/apps/youtube/common/network/h;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->L()Lcom/google/android/apps/youtube/core/client/cf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/c;->a(Lcom/google/android/apps/youtube/core/client/cf;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/youtube/app/offline/a/a;

    if-eqz v2, :cond_5

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/a/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/identity/ai;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/identity/ai;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/offline/a/a;->handleSignInEvent(Lcom/google/android/apps/youtube/core/identity/ai;)V

    :cond_5
    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/sync/e;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->r()Lcom/google/android/apps/youtube/app/offline/sync/b;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/youtube/app/offline/sync/e;-><init>(Lcom/google/android/apps/youtube/app/offline/sync/b;Lcom/google/android/apps/youtube/common/network/h;)V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/common/d/j;->a(Lcom/google/android/apps/youtube/common/d/i;)V

    :cond_6
    new-instance v0, Lcom/google/android/apps/youtube/datalib/offline/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->B()Lcom/google/android/apps/youtube/datalib/e/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->S()Lcom/google/android/apps/youtube/datalib/offline/m;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->i()Lcom/google/android/apps/youtube/datalib/config/c;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/offline/s;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/datalib/offline/m;Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;)V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/common/d/j;->a(Lcom/google/android/apps/youtube/common/d/i;)V

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->i()Lcom/google/android/apps/youtube/datalib/config/c;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/offline/s;->a(JLcom/google/android/apps/youtube/datalib/config/c;)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/common/d/j;->a(Lcom/google/android/apps/youtube/a/a/g;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->I()Lcom/google/android/apps/youtube/core/client/ca;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/google/android/apps/youtube/core/client/ca;->a(Lcom/google/android/apps/youtube/common/c/a;)V

    return-void

    :cond_7
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "pending_notification_registration"

    invoke-interface {v2, v3, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0
.end method

.method public final d()Lcom/google/android/apps/youtube/app/ax;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ax;

    return-object v0
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "session_watch_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "session_watch_count"

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final f()Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    const-string v0, "YouTube"

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/youtube/app/offline/sync/b;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->r()Lcom/google/android/apps/youtube/app/offline/sync/b;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/youtube/core/client/bj;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/youtube/core/client/at;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->O()Lcom/google/android/apps/youtube/core/client/at;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/youtube/app/remote/bk;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->P()Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->Q()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/youtube/app/remote/bw;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->R()Lcom/google/android/apps/youtube/app/remote/bw;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/youtube/app/notification/b;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->T()Lcom/google/android/apps/youtube/app/notification/b;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/google/android/apps/youtube/datalib/c/f;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->V()Lcom/google/android/apps/youtube/datalib/c/f;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/google/android/apps/youtube/app/remote/an;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    return-object v0
.end method

.method public final q()Lcom/google/android/apps/youtube/core/offline/store/q;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/google/android/apps/youtube/core/player/ae;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ac()Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 4

    sget-object v2, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a:Ljava/util/Set;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "country"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v1, v0

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/aw;->V()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()V
    .locals 1

    const-string v0, "User signed in"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->i()V

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->a(I)V

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/widget/WidgetProvider;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->O()Lcom/google/android/apps/youtube/core/client/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/at;->f()V

    return-void
.end method

.method public final u()V
    .locals 2

    const-string v0, "User signed out"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->i()V

    const/4 v0, -0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/av;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/av;-><init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/widget/WidgetProvider;->a(Landroid/content/Context;)V

    return-void
.end method

.method public final v()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->a:I

    return v0
.end method

.method public final w()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->al()Z

    move-result v0

    return v0
.end method

.method public final x()Lcom/google/android/apps/youtube/datalib/innertube/ag;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/YouTubeApplication;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aq()Lcom/google/android/apps/youtube/datalib/innertube/ag;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic y()Lcom/google/android/apps/youtube/core/a;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ax;

    return-object v0
.end method
