.class public final Lcom/google/android/apps/youtube/app/offline/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/common/c/a;

.field private final c:Lcom/google/android/apps/youtube/core/client/bj;

.field private final d:Landroid/app/NotificationManager;

.field private final e:Lcom/google/android/apps/youtube/common/network/h;

.field private f:Ljava/util/HashMap;

.field private g:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/offline/c;->b:Lcom/google/android/apps/youtube/common/c/a;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/offline/c;->c:Lcom/google/android/apps/youtube/core/client/bj;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->C()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->e:Lcom/google/android/apps/youtube/common/network/h;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->g:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->f:Ljava/util/HashMap;

    invoke-virtual {p2, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/c;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-double v2, v1

    const-wide v4, 0x3ffccccccccccccdL    # 1.8

    div-double/2addr v2, v4

    double-to-int v3, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/16 v4, 0x40

    invoke-static {v2, v4}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v4, v3

    div-float/2addr v2, v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v2, v2, v6, v6}, Landroid/graphics/Matrix;->setScale(FFFF)V

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v3

    div-int/lit8 v2, v0, 0x2

    const/4 v6, 0x0

    move-object v0, p1

    move v4, v3

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/c;Ljava/lang/String;)Landroid/support/v4/app/al;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/c;->b(Ljava/lang/String;)Landroid/support/v4/app/al;

    move-result-object v0

    return-object v0
.end method

.method private static a(J)Ljava/lang/String;
    .locals 7

    const-wide/32 v0, 0x100000

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const-string v0, "%.1f"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    long-to-double v3, p0

    const-wide/high16 v5, 0x4130000000000000L    # 1048576.0

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lcom/google/android/apps/youtube/core/utils/Util;->a(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/c;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->g:Ljava/util/Set;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Z)V
    .locals 4

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/c;->c:Lcom/google/android/apps/youtube/core/client/bj;

    new-instance v3, Lcom/google/android/apps/youtube/app/offline/e;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/youtube/app/offline/e;-><init>(Lcom/google/android/apps/youtube/app/offline/c;Ljava/lang/String;)V

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;Z)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->c()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/c;->c:Lcom/google/android/apps/youtube/core/client/bj;

    new-instance v3, Lcom/google/android/apps/youtube/app/offline/d;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/youtube/app/offline/d;-><init>(Lcom/google/android/apps/youtube/app/offline/c;Ljava/lang/String;)V

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/offline/c;)Landroid/app/NotificationManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Landroid/support/v4/app/al;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v4/app/al;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v4/app/al;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/al;->a(J)Landroid/support/v4/app/al;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private handleOfflinePlaylistDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/s;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/s;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3ec

    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method private handleOfflinePlaylistProgressEvent(Lcom/google/android/apps/youtube/app/offline/a/t;)V
    .locals 14
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/app/offline/a/t;->b:Z

    if-eqz v0, :cond_0

    iget-object v3, p1, Lcom/google/android/apps/youtube/app/offline/a/t;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v4

    sget v0, Lcom/google/android/youtube/h;->af:I

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->db:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/h;->N:I

    :goto_0
    new-instance v3, Landroid/content/Intent;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    const-class v6, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v5, 0x4000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    const-string v5, "pane"

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/offline/c;->b(Ljava/lang/String;)Landroid/support/v4/app/al;

    move-result-object v5

    iget-object v6, v4, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/support/v4/app/al;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/support/v4/app/al;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v1

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/support/v4/app/al;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/al;->a(I)Landroid/support/v4/app/al;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v6, v7}, Landroid/support/v4/app/al;->a(IIZ)Landroid/support/v4/app/al;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->a(Z)Landroid/support/v4/app/al;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->b(Z)Landroid/support/v4/app/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v1, v6, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/al;

    const/4 v0, 0x1

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/youtube/app/offline/c;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    const/16 v1, 0x3ec

    invoke-virtual {v5}, Landroid/support/v4/app/al;->a()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/youtube/p;->da:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "pane"

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->d()I

    move-result v2

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->c()I

    move-result v6

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->f()I

    move-result v7

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/offline/c;->e:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v8}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->dy:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_2
    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/offline/c;->b(Ljava/lang/String;)Landroid/support/v4/app/al;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v8

    iget-object v8, v8, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/support/v4/app/al;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    sget v10, Lcom/google/android/youtube/p;->dK:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/support/v4/app/al;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/support/v4/app/al;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v2

    sget v8, Lcom/google/android/youtube/h;->ad:I

    invoke-virtual {v2, v8}, Landroid/support/v4/app/al;->a(I)Landroid/support/v4/app/al;

    move-result-object v2

    const/16 v8, 0x64

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v7, v9}, Landroid/support/v4/app/al;->a(IIZ)Landroid/support/v4/app/al;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/al;->a(Z)Landroid/support/v4/app/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/al;->b(Z)Landroid/support/v4/app/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v7, 0x8000000

    invoke-static {v1, v2, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/al;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/offline/c;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    const/16 v1, 0x3ec

    invoke-virtual {v6}, Landroid/support/v4/app/al;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_1

    :cond_3
    iget-object v8, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/google/android/youtube/o;->i:I

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v10, v11

    const/4 v6, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v6

    invoke-virtual {v8, v9, v2, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method private handleOfflineVideoCompleteEvent(Lcom/google/android/apps/youtube/app/offline/a/ac;)V
    .locals 9
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v8, 0x1

    const/4 v4, 0x0

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/ac;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v6, p1, Lcom/google/android/apps/youtube/app/offline/a/ac;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const-string v2, ""

    const/4 v3, -0x1

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->OFFLINE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    const-class v5, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "watch"

    new-instance v5, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/offline/c;->b(Ljava/lang/String;)Landroid/support/v4/app/al;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    sget v5, Lcom/google/android/youtube/p;->dd:I

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/al;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/support/v4/app/al;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v3

    sget v5, Lcom/google/android/youtube/h;->af:I

    invoke-virtual {v3, v5}, Landroid/support/v4/app/al;->a(I)Landroid/support/v4/app/al;

    move-result-object v3

    invoke-virtual {v3, v4, v4, v4}, Landroid/support/v4/app/al;->a(IIZ)Landroid/support/v4/app/al;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/support/v4/app/al;->a(Z)Landroid/support/v4/app/al;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/support/v4/app/al;->b(Z)Landroid/support/v4/app/al;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v4, v5, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/al;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/al;

    invoke-direct {p0, v6, v8}, Lcom/google/android/apps/youtube/app/offline/c;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    const/16 v3, 0x3eb

    invoke-virtual {v2}, Landroid/support/v4/app/al;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :cond_0
    return-void
.end method

.method private handleOfflineVideoDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/ad;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/ad;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3eb

    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method private handleOfflineVideoStatusUpdateEvent(Lcom/google/android/apps/youtube/app/offline/a/ae;)V
    .locals 16
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/a/ae;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->m()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x3eb

    invoke-virtual {v2, v1, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p1

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/app/offline/a/ae;->b:Z

    if-eqz v1, :cond_0

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/offline/a/ae;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v8

    new-instance v1, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "pane"

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->i()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v9

    const/16 v6, 0x64

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->o()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/youtube/p;->dK:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget v1, Lcom/google/android/youtube/h;->ad:I

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    invoke-virtual {v7, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    sget v1, Lcom/google/android/youtube/h;->N:I

    const/4 v6, 0x0

    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/apps/youtube/app/offline/c;->b(Ljava/lang/String;)Landroid/support/v4/app/al;

    move-result-object v11

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/support/v4/app/al;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v12

    invoke-virtual {v12, v5}, Landroid/support/v4/app/al;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/support/v4/app/al;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/support/v4/app/al;->a(I)Landroid/support/v4/app/al;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v6, v10, v4}, Landroid/support/v4/app/al;->a(IIZ)Landroid/support/v4/app/al;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/support/v4/app/al;->a(Z)Landroid/support/v4/app/al;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/al;->b(Z)Landroid/support/v4/app/al;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v9, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/al;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/al;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v1}, Lcom/google/android/apps/youtube/app/offline/c;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/c;->d:Landroid/app/NotificationManager;

    const/16 v2, 0x3eb

    invoke-virtual {v11}, Landroid/support/v4/app/al;->a()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v8, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/offline/c;->e:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v4

    if-nez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/youtube/p;->dy:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/offline/c;->a:Landroid/content/Context;

    sget v11, Lcom/google/android/youtube/p;->dc:I

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->i()J

    move-result-wide v14

    invoke-static {v14, v15}, Lcom/google/android/apps/youtube/app/offline/c;->a(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->j()J

    move-result-wide v14

    invoke-static {v14, v15}, Lcom/google/android/apps/youtube/app/offline/c;->a(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v4, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/c;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/al;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/al;->a(J)Landroid/support/v4/app/al;

    :cond_0
    return-void
.end method
