.class final Lcom/google/android/apps/youtube/app/offline/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/identity/ah;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

.field final synthetic c:Lcom/google/android/apps/youtube/app/offline/v;

.field final synthetic d:Lcom/google/android/apps/youtube/app/offline/r;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/offline/r;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/s;->d:Lcom/google/android/apps/youtube/app/offline/r;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/offline/s;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/offline/s;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/offline/s;->c:Lcom/google/android/apps/youtube/app/offline/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/s;->d:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/s;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/s;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/s;->c:Lcom/google/android/apps/youtube/app/offline/v;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/offline/r;->a(Lcom/google/android/apps/youtube/app/offline/r;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V

    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/s;->d:Lcom/google/android/apps/youtube/app/offline/r;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/r;->a(Lcom/google/android/apps/youtube/app/offline/r;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/s;->c:Lcom/google/android/apps/youtube/app/offline/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/s;->c:Lcom/google/android/apps/youtube/app/offline/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/s;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->CANNOT_ADD:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/v;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/s;->d:Lcom/google/android/apps/youtube/app/offline/r;

    sget-object v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->CANNOT_ADD:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/offline/r;->a(Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    return-void
.end method
