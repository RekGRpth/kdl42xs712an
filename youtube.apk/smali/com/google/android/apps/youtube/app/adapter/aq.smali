.class public final Lcom/google/android/apps/youtube/app/adapter/aq;
.super Lcom/google/android/apps/youtube/app/adapter/g;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/g;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/ar;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/adapter/ar;-><init>(Lcom/google/android/apps/youtube/app/adapter/aq;Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aq;->a:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/aq;)Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aq;->a:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/apps/youtube/app/adapter/aq;
    .locals 2

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/aq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/apps/youtube/app/adapter/aq;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/as;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/as;-><init>(Lcom/google/android/apps/youtube/app/adapter/aq;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method
