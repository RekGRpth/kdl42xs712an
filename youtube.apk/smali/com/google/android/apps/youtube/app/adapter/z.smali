.class public final Lcom/google/android/apps/youtube/app/adapter/z;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/innertube/p;


# instance fields
.field private a:Lcom/google/android/apps/youtube/app/ui/presenter/am;

.field private b:Lcom/google/android/apps/youtube/uilib/innertube/o;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/presenter/am;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/player/internal/b/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/presenter/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/z;->a:Lcom/google/android/apps/youtube/app/ui/presenter/am;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/uilib/innertube/o;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/z;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/z;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/adapter/z;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/z;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/z;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/z;->a:Lcom/google/android/apps/youtube/app/ui/presenter/am;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/z;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/uilib/innertube/o;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
