.class Lcom/google/android/apps/youtube/app/ui/du;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public c:Landroid/widget/ImageView;

.field public d:Landroid/widget/TextView;

.field final synthetic e:Lcom/google/android/apps/youtube/app/ui/ds;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/ds;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/du;->e:Lcom/google/android/apps/youtube/app/ui/ds;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/google/android/youtube/j;->bE:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/du;->c:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->bW:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/du;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/du;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/du;->c:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->ai:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/du;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/du;->d:Landroid/widget/TextView;

    iget v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->nameId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/du;->c:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->aj:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
