.class public final Lcom/google/android/apps/youtube/app/ui/presenter/ba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/j;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/common/c/a;

.field private final c:Lcom/google/android/apps/youtube/common/network/h;

.field private final d:Lcom/google/android/apps/youtube/app/am;

.field private final e:Lcom/google/android/apps/youtube/core/player/w;

.field private final f:Lcom/google/android/apps/youtube/app/ui/hh;

.field private final g:Lcom/google/android/apps/youtube/app/offline/r;

.field private final h:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final i:Lcom/google/android/apps/youtube/core/client/bj;

.field private final j:Lcom/google/android/apps/youtube/app/offline/p;

.field private final k:Ljava/lang/String;

.field private final l:Lcom/google/android/apps/youtube/app/ui/v;

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/hh;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/offline/p;Ljava/lang/String;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->b:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->c:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/am;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->d:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/w;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->e:Lcom/google/android/apps/youtube/core/player/w;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/hh;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->f:Lcom/google/android/apps/youtube/app/ui/hh;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/offline/r;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->g:Lcom/google/android/apps/youtube/app/offline/r;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->h:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->i:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->j:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->k:Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/bb;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bb;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/bc;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bc;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/bd;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bd;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)V

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/presenter/be;

    invoke-direct {v4, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/be;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)V

    new-instance v5, Lcom/google/android/apps/youtube/app/ui/v;

    check-cast p1, Landroid/app/Activity;

    invoke-direct {v5, p1}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    iput-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    new-instance v6, Lcom/google/android/apps/youtube/app/ui/presenter/bf;

    invoke-direct {v6, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bf;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)V

    invoke-virtual {v5, v6}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/ac;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    sget v6, Lcom/google/android/youtube/p;->dJ:I

    invoke-virtual {v5, v6, v3}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->m:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    sget v5, Lcom/google/android/youtube/p;->fr:I

    invoke-virtual {v3, v5, v4}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->n:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    sget v4, Lcom/google/android/youtube/p;->ft:I

    invoke-virtual {v3, v4, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->o:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    sget v3, Lcom/google/android/youtube/p;->fa:I

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->p:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->h:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/ba;IZ)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/v;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/v;->b(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)Lcom/google/android/apps/youtube/app/offline/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->g:Lcom/google/android/apps/youtube/app/offline/r;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->m:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->n:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->o:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/presenter/ba;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->p:I

    return v0
.end method


# virtual methods
.method public final synthetic a()Lcom/google/android/apps/youtube/uilib/a/g;
    .locals 13

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/p;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/p;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->c:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->d:Lcom/google/android/apps/youtube/app/am;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->e:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->f:Lcom/google/android/apps/youtube/app/ui/hh;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->g:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->h:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->i:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->j:Lcom/google/android/apps/youtube/app/offline/p;

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->k:Ljava/lang/String;

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->l:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/app/ui/presenter/ay;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/hh;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/offline/p;Ljava/lang/String;Lcom/google/android/apps/youtube/app/ui/v;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;->b:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-object v0
.end method
