.class public final Lcom/google/android/apps/youtube/app/ui/presenter/cz;
.super Lcom/google/android/apps/youtube/uilib/a/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/uilib/a/i;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final e:Landroid/view/View;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Lcom/google/android/apps/youtube/app/ui/fw;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/ja;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/datalib/d/a;)V
    .locals 10

    move-object/from16 v0, p10

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/youtube/uilib/a/a;-><init>(Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/uilib/a/i;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/l;->bA:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->c:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->c:Landroid/widget/LinearLayout;

    sget v2, Lcom/google/android/youtube/j;->dc:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/gr;

    move-object/from16 v0, p7

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->c:Landroid/widget/LinearLayout;

    sget v2, Lcom/google/android/youtube/j;->ap:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->c:Landroid/widget/LinearLayout;

    sget v2, Lcom/google/android/youtube/j;->ao:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->c:Landroid/widget/LinearLayout;

    sget v2, Lcom/google/android/youtube/j;->eZ:I

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->e:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/fw;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->e:Landroid/view/View;

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p9

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/youtube/app/ui/fw;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/ja;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->h:Lcom/google/android/apps/youtube/app/ui/fw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->c:Landroid/widget/LinearLayout;

    invoke-interface {p2, v1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/at;)Landroid/view/View;
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/a/a;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->f:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->e()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->b()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->g:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->b()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->h()Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v3, Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->a:Landroid/app/Activity;

    sget v5, Lcom/google/android/youtube/p;->gn:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;->a()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-virtual {v4, v5, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->a:Landroid/app/Activity;

    const v4, 0x1040013    # android.R.string.yes

    invoke-virtual {v2, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->a:Landroid/app/Activity;

    const v5, 0x1040009    # android.R.string.no

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v2, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/d;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/d;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->h:Lcom/google/android/apps/youtube/app/ui/fw;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/fw;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/gr;->a()Landroid/widget/ImageView;

    move-result-object v0

    sget v3, Lcom/google/android/youtube/h;->am:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->g:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/at;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/at;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/at;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/cz;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/at;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
