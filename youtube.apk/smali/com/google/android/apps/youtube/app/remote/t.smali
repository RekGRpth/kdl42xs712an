.class public final Lcom/google/android/apps/youtube/app/remote/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/bw;


# static fields
.field private static final a:Landroid/util/Pair;


# instance fields
.field private final b:Ljava/util/Map;

.field private final c:Landroid/content/res/Resources;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Lcom/google/android/apps/ytremote/backend/a/e;

.field private final f:Lcom/google/android/apps/ytremote/backend/a/j;

.field private final g:Lcom/google/android/apps/ytremote/backend/logic/a;

.field private final h:Lcom/google/android/apps/youtube/app/remote/bk;

.field private final i:Lcom/google/android/apps/ytremote/backend/a/l;

.field private final j:Lcom/google/android/apps/ytremote/backend/a/a;

.field private final k:Lcom/google/android/apps/youtube/core/identity/l;

.field private final l:Lcom/google/android/apps/youtube/core/identity/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, ""

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/t;->a:Landroid/util/Pair;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Landroid/content/res/Resources;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/b;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->d:Ljava/util/concurrent/Executor;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->c:Landroid/content/res/Resources;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->h:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->k:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->l:Lcom/google/android/apps/youtube/core/identity/b;

    new-instance v0, Lcom/google/android/apps/ytremote/backend/a/l;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/a/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->i:Lcom/google/android/apps/ytremote/backend/a/l;

    new-instance v0, Lcom/google/android/apps/ytremote/backend/a/e;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->e:Lcom/google/android/apps/ytremote/backend/a/e;

    new-instance v0, Lcom/google/android/apps/ytremote/backend/a/j;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->f:Lcom/google/android/apps/ytremote/backend/a/j;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->b:Ljava/util/Map;

    new-instance v0, Lcom/google/android/apps/ytremote/backend/a/a;

    new-instance v1, Lcom/google/android/apps/ytremote/backend/a/g;

    const-string v2, "remote_id"

    invoke-static {p2, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/ytremote/backend/a/g;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/ytremote/backend/a/a;-><init>(Lcom/google/android/apps/ytremote/backend/logic/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->j:Lcom/google/android/apps/ytremote/backend/a/a;

    new-instance v0, Lcom/google/android/apps/ytremote/backend/a/d;

    new-instance v1, Lcom/google/android/apps/ytremote/backend/a/n;

    invoke-direct {v1, p2}, Lcom/google/android/apps/ytremote/backend/a/n;-><init>(Landroid/content/SharedPreferences;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/t;->j:Lcom/google/android/apps/ytremote/backend/a/a;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/a/d;-><init>(Lcom/google/android/apps/ytremote/backend/logic/a;Lcom/google/android/apps/ytremote/backend/logic/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->g:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-virtual {p5, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a()Landroid/util/Pair;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/t;->a:Landroid/util/Pair;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/a/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->i:Lcom/google/android/apps/ytremote/backend/a/l;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/t;Ljava/util/List;Lcom/google/android/apps/ytremote/model/CloudScreen;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/apps/ytremote/model/ScreenId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v1

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x2

    move-object v1, v2

    :goto_2
    invoke-static {p1, v1}, Lcom/google/android/apps/ytremote/backend/a/c;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/t;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 7

    const/4 v1, 0x1

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/t;->c:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/p;->fx:I

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/google/android/apps/ytremote/backend/a/c;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v3

    if-nez v3, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/t;Ljava/util/List;)Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    new-instance v3, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private b()Landroid/util/Pair;
    .locals 3

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->k:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SIGNED_OUT_USER"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->k:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->e()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->l:Lcom/google/android/apps/youtube/core/identity/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/t;->k:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/l;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/a;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/t;->a:Landroid/util/Pair;

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/t;->a:Landroid/util/Pair;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/t;->a:Landroid/util/Pair;

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/t;)Landroid/util/Pair;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/t;->b()Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/logic/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->g:Lcom/google/android/apps/ytremote/backend/logic/a;

    return-object v0
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->j:Lcom/google/android/apps/ytremote/backend/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->j:Lcom/google/android/apps/ytremote/backend/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/backend/a/a;->a()V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/youtube/app/remote/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->h:Lcom/google/android/apps/youtube/app/remote/bk;

    return-object v0
.end method

.method private d(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/z;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/z;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/remote/t;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/a/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->e:Lcom/google/android/apps/ytremote/backend/a/e;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/a/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->f:Lcom/google/android/apps/ytremote/backend/a/j;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/v;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p1}, Lcom/google/android/apps/youtube/app/remote/v;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Ljava/lang/Void;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/ab;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/youtube/app/remote/ab;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/aa;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/aa;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/ScreenId;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/y;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/youtube/app/remote/y;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/ytremote/model/ScreenId;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/x;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/apps/youtube/app/remote/x;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/t;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/u;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/google/android/apps/youtube/app/remote/u;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p1}, Lcom/google/android/apps/youtube/app/remote/w;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Ljava/lang/Void;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final c(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/t;->c()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/remote/t;->b(Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final onSignIn(Lcom/google/android/apps/youtube/core/identity/ai;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/t;->c()V

    return-void
.end method

.method public final onSignOut(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/t;->c()V

    return-void
.end method
