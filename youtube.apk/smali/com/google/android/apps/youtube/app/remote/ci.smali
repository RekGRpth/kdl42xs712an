.class final Lcom/google/android/apps/youtube/app/remote/ci;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/ci;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/ci;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error on retrieving app status for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ci;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ci;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ci;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/apps/ytremote/model/AppStatus;

    invoke-virtual {p2}, Lcom/google/android/apps/ytremote/model/AppStatus;->getStatus()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ci;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "App status for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/ci;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/lang/String;)V

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ci;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ci;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    :cond_0
    return-void
.end method
