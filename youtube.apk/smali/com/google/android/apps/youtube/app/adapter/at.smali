.class public Lcom/google/android/apps/youtube/app/adapter/at;
.super Lcom/google/android/apps/youtube/core/a/a;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/res/Resources;

.field private final b:Landroid/view/LayoutInflater;

.field private final d:I

.field private final e:Ljava/util/Map;

.field private final f:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a/a;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->b:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->a:Landroid/content/res/Resources;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->e:Ljava/util/Map;

    iput p2, p0, Lcom/google/android/apps/youtube/app/adapter/at;->d:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->a:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/fv;->a(Landroid/content/res/Resources;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->f:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->b:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/at;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/au;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/au;-><init>(Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    return-object p1
.end method

.method protected final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/at;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/au;

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/adapter/au;->a:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/adapter/au;->b:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/youtube/h;->J:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    return-object v1
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/a/a;->a()V

    return-void
.end method

.method protected final synthetic a(ILjava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->e:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/at;->f:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/core/a/a;->a(ILjava/lang/Object;)V

    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/at;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/at;->f:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/a/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/at;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/at;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
