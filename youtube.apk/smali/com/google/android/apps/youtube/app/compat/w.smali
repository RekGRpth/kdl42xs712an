.class public Lcom/google/android/apps/youtube/app/compat/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/compat/q;


# instance fields
.field protected final a:Landroid/view/MenuItem;

.field private final b:Landroid/content/Context;

.field private c:I

.field private d:Lcom/google/android/apps/youtube/app/compat/y;

.field private e:Landroid/view/View;

.field private f:Lcom/google/android/apps/youtube/app/compat/s;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/MenuItem;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->b:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->c:I

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/compat/w;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->e:Landroid/view/View;

    return-object p0
.end method

.method public a(Lcom/google/android/apps/youtube/app/compat/r;)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 0

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/s;)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/compat/w;->f:Lcom/google/android/apps/youtube/app/compat/s;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    new-instance v1, Lcom/google/android/apps/youtube/app/compat/x;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/compat/x;-><init>(Lcom/google/android/apps/youtube/app/compat/w;Lcom/google/android/apps/youtube/app/compat/s;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public final a(Z)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    return-object p0
.end method

.method public final b(Z)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    invoke-interface {v1, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->d:Lcom/google/android/apps/youtube/app/compat/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->d:Lcom/google/android/apps/youtube/app/compat/y;

    :cond_0
    return-object p0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/app/compat/w;->c:I

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c(I)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->e:Landroid/view/View;

    return-object v0
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    return v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/compat/w;->c:I

    return v0
.end method
