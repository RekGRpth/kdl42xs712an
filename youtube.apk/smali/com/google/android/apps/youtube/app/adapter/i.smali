.class final Lcom/google/android/apps/youtube/app/adapter/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Z

.field public b:Z

.field final synthetic c:Lcom/google/android/apps/youtube/app/adapter/h;

.field private final d:Lcom/google/android/apps/youtube/app/adapter/h;

.field private e:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/h;Lcom/google/android/apps/youtube/app/adapter/h;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->b:Z

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/adapter/h;Lcom/google/android/apps/youtube/app/adapter/h;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/i;-><init>(Lcom/google/android/apps/youtube/app/adapter/h;Lcom/google/android/apps/youtube/app/adapter/h;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/i;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Lcom/google/android/apps/youtube/app/adapter/h;Lcom/google/android/apps/youtube/app/adapter/i;)Lcom/google/android/apps/youtube/app/adapter/i;

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/i;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->b:Z

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/i;->e:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->b(Lcom/google/android/apps/youtube/app/adapter/h;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Lcom/google/android/apps/youtube/app/adapter/h;Z)Z

    invoke-static {}, Lcom/google/android/apps/youtube/app/adapter/h;->a()Landroid/os/Handler;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/app/adapter/h;->a()Landroid/os/Handler;

    move-result-object v1

    invoke-static {v1, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;Ljava/lang/Runnable;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/adapter/i;->run()V

    goto :goto_0
.end method

.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/i;

    move-result-object v0

    if-ne v0, p0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Lcom/google/android/apps/youtube/app/adapter/h;Lcom/google/android/apps/youtube/app/adapter/i;)Lcom/google/android/apps/youtube/app/adapter/i;

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/h;->c(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/i;->e:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/h;->d(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/h;->e(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/j;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/h;->e(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/j;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/i;->e:Landroid/graphics/Bitmap;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/adapter/j;->a()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/h;->d(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v1

    sget-object v2, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v1, v2, :cond_1

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/adapter/h;->d(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Landroid/graphics/Matrix;Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->d(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->f(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->f(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/h;->g(Lcom/google/android/apps/youtube/app/adapter/h;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->d:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->d(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/h;->f(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->e(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/j;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/h;->e(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/i;->c:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/adapter/j;->b()V

    goto :goto_0
.end method
