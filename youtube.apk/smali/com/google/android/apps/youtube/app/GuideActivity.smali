.class public abstract Lcom/google/android/apps/youtube/app/GuideActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/compat/d;
.implements Lcom/google/android/apps/youtube/app/honeycomb/ui/f;
.implements Lcom/google/android/apps/youtube/app/ui/ft;
.implements Lcom/google/android/apps/youtube/app/ui/gj;


# instance fields
.field private A:Lcom/google/android/apps/youtube/core/identity/aa;

.field private B:Lcom/google/android/apps/youtube/core/identity/l;

.field private C:Lcom/google/android/apps/youtube/core/aw;

.field private D:Lcom/google/android/apps/youtube/core/Analytics;

.field private E:Ljava/lang/String;

.field private F:Lcom/google/android/gms/c/a;

.field private G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

.field private H:Landroid/view/View;

.field private I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

.field private J:Landroid/app/ProgressDialog;

.field private K:Lcom/google/android/apps/youtube/core/async/af;

.field private L:Lcom/google/android/apps/youtube/core/async/af;

.field private M:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private N:Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

.field private O:Z

.field private P:Z

.field private Q:Lcom/google/android/apps/youtube/app/search/SearchType;

.field private R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

.field private S:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

.field private T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

.field private U:Z

.field private V:Lcom/google/android/apps/youtube/app/aj;

.field private W:Lcom/google/android/apps/youtube/app/remote/an;

.field private X:Z

.field private Y:Z

.field private final Z:Lcom/google/android/apps/youtube/common/c/d;

.field private final aa:Lcom/google/android/apps/youtube/common/c/d;

.field private ab:Lcom/google/android/apps/youtube/common/c/e;

.field private ac:Lcom/google/android/apps/youtube/common/c/e;

.field private ad:Lcom/google/android/apps/youtube/common/c/e;

.field protected n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field protected o:Landroid/content/res/Resources;

.field protected p:Lcom/google/android/apps/youtube/common/network/h;

.field protected q:Landroid/support/v4/app/l;

.field protected r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

.field protected s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

.field protected t:Lcom/google/android/apps/youtube/app/compat/q;

.field protected u:Z

.field protected v:Z

.field protected w:Z

.field private x:Lcom/google/android/apps/youtube/app/ax;

.field private y:Lcom/google/android/apps/youtube/common/c/a;

.field private z:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Q:Lcom/google/android/apps/youtube/app/search/SearchType;

    new-instance v0, Lcom/google/android/apps/youtube/app/ae;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ae;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Z:Lcom/google/android/apps/youtube/common/c/d;

    new-instance v0, Lcom/google/android/apps/youtube/app/af;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/af;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->aa:Lcom/google/android/apps/youtube/common/c/d;

    return-void
.end method

.method private C()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->p:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->i()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    new-instance v1, Lcom/google/a/a/a/a/kz;

    invoke-direct {v1}, Lcom/google/a/a/a/a/kz;-><init>()V

    new-instance v2, Lcom/google/a/a/a/a/ls;

    invoke-direct {v2}, Lcom/google/a/a/a/a/ls;-><init>()V

    iput-object v2, v1, Lcom/google/a/a/a/a/kz;->s:Lcom/google/a/a/a/a/ls;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/youtube/app/GuideActivity;->D()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    goto :goto_0
.end method

.method private static D()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 4

    const-string v0, "FEwhat_to_watch"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->b(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    new-instance v1, Lcom/google/a/a/a/a/kz;

    invoke-direct {v1}, Lcom/google/a/a/a/a/kz;-><init>()V

    new-instance v2, Lcom/google/a/a/a/a/am;

    invoke-direct {v2}, Lcom/google/a/a/a/a/am;-><init>()V

    iput-object v2, v1, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    iget-object v2, v1, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    const-string v3, "FEwhat_to_watch"

    iput-object v3, v2, Lcom/google/a/a/a/a/am;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    return-object v0
.end method

.method private E()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->U:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->NONE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->b()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->F()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->UP:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;Z)V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->u:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->NONE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;Z)V

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->DRAWER_TOGGLE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->r()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d()V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->e()V

    goto :goto_0
.end method

.method private F()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->isTaskRoot()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->X:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/youtube/app/honeycomb/Shell$HomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private H()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    sget v0, Lcom/google/android/youtube/j;->bv:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    const-string v2, "GuideFragment"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->c()Landroid/support/v4/app/l;

    move-result-object v0

    const-string v1, "PaneFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private T()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->newFragment()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->di:I

    const-string v2, "PaneFragment"

    const/4 v3, 0x0

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private U()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->H:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->H:Landroid/view/View;

    const v1, 0x3f333333    # 0.7f

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Landroid/view/View;F)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->H:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private V()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->w:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->b(Lcom/google/android/apps/youtube/app/compat/SupportActionBar;)V

    :cond_1
    sget v0, Lcom/google/android/youtube/p;->A:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(I)V

    goto :goto_0
.end method

.method private W()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->u:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->t()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->V()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->t()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->V()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->u()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->V()V

    goto :goto_0
.end method

.method private a(Landroid/support/v4/app/Fragment$SavedState;)Landroid/support/v4/app/Fragment$SavedState;
    .locals 2

    if-eqz p1, :cond_0

    :try_start_0
    const-class v0, Landroid/support/v4/app/Fragment$SavedState;

    const-string v1, "mState"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-object p1

    :catch_0
    move-exception v0

    const-string v1, "Set class loader hack failed."

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    const/4 p1, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Set class loader hack failed."

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "Set class loader hack failed."

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;
    .locals 6

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    const-string v3, "pane"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pane"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->BROWSE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->X:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v3, Lcom/google/android/apps/youtube/app/b/m;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/b/m;-><init>()V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Y:Z

    :goto_1
    return-object v0

    :cond_2
    const-string v3, "watch"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "watch"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->WATCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto :goto_0

    :cond_3
    const-string v3, "alias"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "alias"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-class v5, Lcom/google/android/apps/youtube/app/honeycomb/Shell$HomeActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->X:Z

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->BROWSE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    :cond_4
    :goto_2
    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    if-ne v0, v1, :cond_0

    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Landroid/content/Intent;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->SEARCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto :goto_0

    :cond_5
    const-class v5, Lcom/google/android/apps/youtube/app/honeycomb/Shell$ResultsActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Landroid/content/Intent;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->SEARCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto :goto_2

    :cond_6
    const-class v5, Lcom/google/android/apps/youtube/app/honeycomb/Shell$ChannelActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v4

    :cond_7
    :goto_3
    if-eqz v4, :cond_8

    invoke-virtual {p0, v4}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    move v1, v2

    :cond_8
    if-eqz v1, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->BROWSE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto :goto_2

    :cond_9
    const-string v3, "username"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "username"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v4

    goto :goto_3

    :cond_a
    const-class v5, Lcom/google/android/apps/youtube/app/honeycomb/Shell$WatchActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "m.youtube.com"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "m.youtube.com"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "watch"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    move v3, v2

    :goto_4
    if-nez v3, :cond_c

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/utils/m;->b(Landroid/app/Activity;Landroid/net/Uri;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto/16 :goto_1

    :cond_b
    move v3, v1

    goto :goto_4

    :cond_c
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_e

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/utils/q;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/q;

    move-result-object v4

    if-eqz v4, :cond_e

    iget-object v5, v4, Lcom/google/android/apps/youtube/core/utils/q;->a:Ljava/lang/String;

    if-eqz v5, :cond_e

    iget-object v1, v4, Lcom/google/android/apps/youtube/core/utils/q;->a:Ljava/lang/String;

    invoke-direct {p0, v3, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v1

    :cond_d
    :goto_5
    if-eqz v1, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->WATCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto/16 :goto_2

    :cond_e
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->createFromExternalIntent(Landroid/content/Intent;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    move-result-object v3

    if-eqz v3, :cond_d

    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    move v1, v2

    goto :goto_5

    :cond_f
    const-class v5, Lcom/google/android/apps/youtube/app/honeycomb/Shell$PlaylistActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_12

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/q;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/q;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/utils/q;->a:Ljava/lang/String;

    :goto_6
    if-eqz v0, :cond_11

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/utils/q;->b:Z

    :goto_7
    if-nez v3, :cond_13

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto/16 :goto_2

    :cond_10
    move-object v3, v4

    goto :goto_6

    :cond_11
    move v0, v1

    goto :goto_7

    :cond_12
    const-string v0, "playlist_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "playlist_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/q;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/q;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/utils/q;->a:Ljava/lang/String;

    move-object v3, v0

    move v0, v1

    goto :goto_7

    :cond_13
    if-eqz v0, :cond_14

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->createFromPlaylistId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->WATCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto/16 :goto_2

    :cond_14
    invoke-static {v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Lcom/google/android/apps/youtube/datalib/innertube/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->b(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->BROWSE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto/16 :goto_2

    :cond_15
    const-class v4, Lcom/google/android/apps/youtube/app/honeycomb/Shell$MediaSearchActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->createFromMediaSearchIntent(Landroid/content/Intent;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    move-result-object v3

    if-nez v3, :cond_16

    :goto_8
    if-eqz v1, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->WATCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    goto/16 :goto_2

    :cond_16
    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    move v1, v2

    goto :goto_8

    :cond_17
    move v0, v1

    move-object v3, v4

    goto :goto_7
.end method

.method private a(F)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget v0, Lcom/google/android/youtube/j;->bv:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_0
    return-void
.end method

.method private a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->q:Landroid/support/v4/app/l;

    invoke-virtual {v0}, Landroid/support/v4/app/l;->a()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/v;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/v;

    invoke-virtual {v0, p4}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/v;

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()I

    return-void
.end method

.method private static a(Landroid/view/View;F)V
    .locals 2

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/GuideActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->W()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/core/identity/ai;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->P:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->c(Z)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/GuideActivity;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x408

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->dismissDialog(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->p:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->C:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->f()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->NEW:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    if-ne v0, v1, :cond_1

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/app/fragments/PaneFragment;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;Z)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->popAll()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->q:Landroid/support/v4/app/l;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/l;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->push(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;Landroid/support/v4/app/Fragment$SavedState;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->mustAuthenticate()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->setAuthenticated()V

    :cond_2
    sget v0, Lcom/google/android/youtube/j;->di:I

    const-string v1, "PaneFragment"

    const/16 v2, 0x2002

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/app/fragments/navigation/a;)V
    .locals 4

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/a;->a:Landroid/os/Parcelable;

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->newFragment()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v1

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/a;->b:Landroid/os/Parcelable;

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Landroid/support/v4/app/Fragment$SavedState;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/support/v4/app/Fragment$SavedState;)V

    :cond_0
    sget v0, Lcom/google/android/youtube/j;->di:I

    const-string v2, "PaneFragment"

    const/16 v3, 0x1001

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V

    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->m()V

    return-void
.end method

.method private b(Z)V
    .locals 6

    const/4 v5, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->v:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->J()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->o:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/i;->c:I

    invoke-virtual {v0, v1, v5, v5}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    float-to-double v1, v0

    const-wide v3, 0x3fa999999999999aL    # 0.05

    cmpl-double v1, v1, v3

    if-lez v1, :cond_2

    float-to-double v1, v0

    const-wide v3, 0x3fee666666666666L    # 0.95

    cmpg-double v1, v1, v3

    if-gez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->u:Z

    if-nez v1, :cond_1

    iput-boolean v5, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->u:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(F)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(F)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->u:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->u:Z

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(I)V

    :cond_3
    :goto_1
    const v0, 0x3f733333    # 0.95f

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(F)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->i()V

    goto :goto_1
.end method

.method private b(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/search/SearchType;->fromQuery(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/search/SearchType;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Q:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/search/SearchType;->removeModifiersFromQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "selected_time_filter"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    const/4 v0, 0x1

    return v0
.end method

.method private b(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/al;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/al;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/utils/al;->a:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->createFromPlaylistId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "invalid intercepted Uri: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->C:Lcom/google/android/apps/youtube/core/aw;

    sget v3, Lcom/google/android/youtube/p;->bn:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->d(Z)V

    move v0, v2

    goto :goto_0
.end method

.method private c(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a()V

    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->P:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x408

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->dismissDialog(I)V

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->SIGNED_IN:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->H()V

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->isAuthenticated()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    const/4 v0, 0x2

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    or-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-nez v1, :cond_6

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->C()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v1

    or-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/b/s;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/b/s;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    goto :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->W()V

    goto :goto_0
.end method


# virtual methods
.method public A()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->w:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->V:Lcom/google/android/apps/youtube/app/aj;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/aj;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final B()Lcom/google/android/apps/youtube/app/remote/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->W:Lcom/google/android/apps/youtube/app/remote/an;

    return-object v0
.end method

.method protected final a(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    const/16 v0, 0x408

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->J:Landroid/app/ProgressDialog;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->t()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->V()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->ALL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setTouchInterceptArea(Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->U()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->u()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->V()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->LEFT_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setTouchInterceptArea(Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 3

    if-eq p1, p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->H:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    sub-int v1, p2, p1

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(F)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->H:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sub-int v0, p2, p1

    int-to-float v0, v0

    int-to-float v1, p2

    div-float/2addr v0, v1

    const v1, 0x3f333333    # 0.7f

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->H:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->a(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->a(Landroid/net/Uri;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected abstract a(Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;Z)V
.end method

.method public final a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->P:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->NEW:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    goto :goto_0
.end method

.method protected a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_1

    move v4, v1

    :goto_0
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_2

    move v3, v1

    :goto_1
    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->isGuideEntry()Z

    move-result v5

    if-nez v0, :cond_4

    if-eqz v5, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->F()Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-eqz v6, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->G()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "pane"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_3
    return-void

    :cond_1
    move v4, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    or-int/2addr v0, v5

    or-int/2addr v3, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_4
    if-eqz v3, :cond_8

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->popAll()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    :cond_5
    :goto_5
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->U:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setEnabled(Z)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->U:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->t()V

    :cond_6
    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->z:Landroid/content/SharedPreferences;

    const-string v3, "show_channel_store_turorial"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->p()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    if-nez v3, :cond_9

    if-nez v0, :cond_5

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->mustAuthenticate()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    goto :goto_3

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->newFragment()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    invoke-direct {p0, v0, p1, v3}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/PaneFragment;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;Z)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->c(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    goto :goto_5

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->u()V

    goto :goto_6

    :cond_c
    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->h()V

    goto :goto_3
.end method

.method public final a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->P:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->S:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->S:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/apps/youtube/app/remote/ap;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/ap;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/ap;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Q:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->h()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Q:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    goto :goto_0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->I()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->M()Lcom/google/android/apps/youtube/app/compat/o;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/m;->b:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/o;->a(ILcom/google/android/apps/youtube/app/compat/j;)V

    sget v0, Lcom/google/android/youtube/j;->cA:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->t:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->t:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/j;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->O:Z

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->O:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->onSearchRequested()Z

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/q;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/google/android/apps/youtube/app/compat/q;->e()I

    move-result v1

    sget v2, Lcom/google/android/youtube/j;->cA:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->D:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "SignOut"

    const-string v3, "Menu"

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->A:Lcom/google/android/apps/youtube/core/identity/aa;

    const-string v2, "User action in action bar menu"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->D:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "SignIn"

    const-string v3, "Menu"

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/GuideActivity;->D()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/q;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a_()V
    .locals 3

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a_()V

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->P:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->c(Z)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->s()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->E:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->E:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->T()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->S:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->S:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->A:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/aa;->a()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->m()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->f()V

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->b(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method protected abstract b(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V
.end method

.method public b(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->b(Lcom/google/android/apps/youtube/app/compat/j;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->t:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->t:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/youtube/p;->cF:I

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/compat/q;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->t:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->p:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    sget v0, Lcom/google/android/youtube/p;->cE:I

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_home"

    return-object v0
.end method

.method public final d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->pop()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/a;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->h()V

    :goto_1
    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->finish()V

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->C()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V

    goto :goto_0
.end method

.method protected e()I
    .locals 1

    sget v0, Lcom/google/android/youtube/l;->Y:I

    return v0
.end method

.method public final e(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->v:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->v:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->w()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->y()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->E()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->v:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x400

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->b()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->closeOptionsMenu()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->x()V

    :cond_1
    return-void
.end method

.method protected f()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->P:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x408

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->dismissDialog(I)V

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->SIGNED_OUT:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->H()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->isAuthenticated()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->mustAuthenticate()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    or-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V

    :goto_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->R:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->isAuthenticated()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-nez v1, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->C()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v1

    or-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/b/t;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/b/t;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->T()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->W()V

    goto :goto_2
.end method

.method public final f(Z)V
    .locals 1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->V()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->w:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->d()V

    goto :goto_0
.end method

.method public final g()Z
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->popCheckpoint()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/a;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->E()V

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->F()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->G()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->v:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->r()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->V:Lcom/google/android/apps/youtube/app/aj;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/aj;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->h()V

    goto :goto_0
.end method

.method protected final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->V:Lcom/google/android/apps/youtube/app/aj;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/aj;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected final i()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    return-void
.end method

.method protected final j()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final k()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->SIGNED_IN:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final l()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->SIGNED_OUT:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->NEW:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/b/u;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/b/u;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    const/16 v0, 0x408

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->showDialog(I)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ah;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ah;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->A:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/aa;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->A:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->A:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v1, p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->b(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    goto :goto_0
.end method

.method public final n()V
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->I()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->E()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->getSearchType()Lcom/google/android/apps/youtube/app/search/SearchType;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Q:Lcom/google/android/apps/youtube/app/search/SearchType;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-nez v0, :cond_3

    if-nez v2, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/GuideActivity;->c(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->E()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final o()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->u:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->h()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->K()Z

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->d(Z)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->w()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->e()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->R()V

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->o:Landroid/content/res/Resources;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->c()Landroid/support/v4/app/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->q:Landroid/support/v4/app/l;

    new-instance v0, Lcom/google/android/apps/youtube/app/aj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/aj;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->V:Lcom/google/android/apps/youtube/app/aj;

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->P:Z

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->NEW:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->I:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    new-instance v0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->N:Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->z:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->A:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->B:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->C:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->D:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->E:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->W:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->p:Lcom/google/android/apps/youtube/common/network/h;

    if-eqz p1, :cond_1

    const-string v0, "back_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    const-string v0, "current_descriptor"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->x:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->t()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->K:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->k()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->L:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->M:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->e()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/f;->e:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    sget v0, Lcom/google/android/youtube/j;->eP:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->LEFT_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setTouchInterceptArea(Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setListener(Lcom/google/android/apps/youtube/app/ui/ft;)V

    sget v0, Lcom/google/android/youtube/j;->fK:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->H:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->c()Landroid/support/v4/app/l;

    move-result-object v0

    const-string v1, "GuideFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->s:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->w()V

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/apps/youtube/app/compat/d;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->H:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/ag;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ag;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-nez v0, :cond_0

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->U:Z

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/LoadingFragment;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/LoadingFragment;-><init>()V

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/PaneFragment;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->i()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setEnabled(Z)V

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/GuideActivity;->c(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    :cond_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->J:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->J:Landroid/app/ProgressDialog;

    sget v1, Lcom/google/android/youtube/p;->fC:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->J:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    new-instance v0, Lcom/google/android/gms/c/a;

    const/16 v1, 0xb

    invoke-direct {v0, p0, v1, v3, v3}, Lcom/google/android/gms/c/a;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/gms/c/b;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->F:Lcom/google/android/gms/c/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->F:Lcom/google/android/gms/c/a;

    invoke-virtual {v0}, Lcom/google/android/gms/c/a;->a()V

    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->F:Lcom/google/android/gms/c/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->F:Lcom/google/android/gms/c/a;

    invoke-virtual {v0}, Lcom/google/android/gms/c/a;->b()V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Landroid/view/KeyEvent$Callback;

    if-eqz v2, :cond_2

    check-cast v0, Landroid/view/KeyEvent$Callback;

    invoke-interface {v0, p1, p2}, Landroid/view/KeyEvent$Callback;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->J()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Landroid/view/KeyEvent$Callback;

    if-eqz v2, :cond_2

    check-cast v0, Landroid/view/KeyEvent$Callback;

    invoke-interface {v0, p1, p2}, Landroid/view/KeyEvent$Callback;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Y:Z

    const-string v0, "com.google.android.youtube.action.search"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->onSearchRequested()Z

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->SEARCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Landroid/content/Intent;)Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;Z)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->P:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    new-array v1, v5, [Lcom/google/android/apps/youtube/common/c/e;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->ab:Lcom/google/android/apps/youtube/common/c/e;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->ad:Lcom/google/android/apps/youtube/common/c/e;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a([Lcom/google/android/apps/youtube/common/c/e;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->V:Lcom/google/android/apps/youtube/app/aj;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/aj;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->V:Lcom/google/android/apps/youtube/app/aj;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/app/aj;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->V:Lcom/google/android/apps/youtube/app/aj;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/aj;->removeMessages(I)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onPostCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/b/m;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/b/m;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    const-string v0, "has_handled_intent"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Y:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Y:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    const-string v1, "com.google.android.youtube.action.search"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->O:Z

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->SEARCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    :goto_0
    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;Z)V

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Landroid/content/Intent;)Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    move-result-object v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->F:Lcom/google/android/gms/c/a;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/c/a;->a(JLjava/lang/String;[B[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    const-class v1, Lcom/google/android/apps/youtube/core/identity/aj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Z:Lcom/google/android/apps/youtube/common/c/d;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->ab:Lcom/google/android/apps/youtube/common/c/e;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    const-class v1, Lcom/google/android/apps/youtube/core/identity/ai;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->aa:Lcom/google/android/apps/youtube/common/c/d;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->ad:Lcom/google/android/apps/youtube/common/c/e;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->w:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->V()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "back_stack"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->G:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "current_descriptor"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->T:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "has_handled_intent"

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->Y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onStart()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    const-class v1, Lcom/google/android/apps/youtube/common/network/a;

    new-instance v2, Lcom/google/android/apps/youtube/app/ai;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/app/ai;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;B)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->ac:Lcom/google/android/apps/youtube/common/c/e;

    return-void
.end method

.method protected onStop()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->y:Lcom/google/android/apps/youtube/common/c/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/common/c/e;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->ac:Lcom/google/android/apps/youtube/common/c/e;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a([Lcom/google/android/apps/youtube/common/c/e;)V

    return-void
.end method

.method public p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Lcom/google/android/apps/youtube/datalib/d/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->N:Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    return-object v0
.end method

.method public final r()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(I)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public final s()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->u:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method protected t()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->w:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->E()V

    :cond_0
    return-void
.end method

.method protected u()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->w:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->E()V

    :cond_0
    return-void
.end method

.method public v()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->t()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->V()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->U()V

    return-void
.end method

.method protected w()V
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->v:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a()I

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v2, v1, v0, v1, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setPadding(IIII)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected x()V
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->v:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->i()V

    :cond_0
    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Z)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->U:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected abstract y()V
.end method

.method public z()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/GuideActivity;->w:Z

    return-void
.end method
