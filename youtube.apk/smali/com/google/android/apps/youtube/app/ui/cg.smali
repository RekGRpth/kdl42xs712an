.class final Lcom/google/android/apps/youtube/app/ui/cg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/cc;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/cc;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/cc;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/cg;-><init>(Lcom/google/android/apps/youtube/app/ui/cc;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cc;->e(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->cP:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cc;->c(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cc;->d(Lcom/google/android/apps/youtube/app/ui/cc;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cc;->e(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->cP:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cc;->f(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/app/ui/cf;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/cf;->a(I)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cc;->c(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cc;->e(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cg;->a:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/cc;->d(Lcom/google/android/apps/youtube/app/ui/cc;)Ljava/util/HashSet;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
