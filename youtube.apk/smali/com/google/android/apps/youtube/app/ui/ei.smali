.class public final Lcom/google/android/apps/youtube/app/ui/ei;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/aq;
.implements Lcom/google/android/apps/youtube/core/player/overlay/p;


# static fields
.field private static final a:Ljava/util/Set;


# instance fields
.field private final A:Lcom/google/android/apps/youtube/app/am;

.field private B:Z

.field private C:Ljava/lang/String;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/google/android/apps/youtube/core/client/ce;

.field private final d:Lcom/google/android/apps/youtube/core/Analytics;

.field private final e:Lcom/google/android/apps/youtube/app/ui/gr;

.field private f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

.field private final g:Lcom/google/android/apps/youtube/core/player/ae;

.field private final h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

.field private final i:Landroid/os/Handler;

.field private final j:Lcom/google/android/apps/youtube/app/ui/eq;

.field private final k:Lcom/google/android/apps/youtube/app/ui/ep;

.field private l:Lcom/google/android/apps/youtube/common/a/d;

.field private m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

.field private n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:I

.field private v:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

.field private w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

.field private final x:Landroid/widget/FrameLayout;

.field private final y:Landroid/view/View;

.field private z:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x3

    new-array v2, v0, [Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v0, v2, v1

    const/4 v0, 0x1

    sget-object v3, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PAUSED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    sget-object v3, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v3, v2, v0

    if-eqz v2, :cond_0

    array-length v0, v2

    if-nez v0, :cond_2

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/ei;->a:Ljava/util/Set;

    return-void

    :cond_2
    new-instance v0, Ljava/util/HashSet;

    array-length v3, v2

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(I)V

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;Landroid/view/View;Lcom/google/android/apps/youtube/app/ui/ep;Lcom/google/android/apps/youtube/app/ui/eq;Lcom/google/android/apps/youtube/app/am;Landroid/view/View;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->t:Z

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->y:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ae;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->g:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->b:Landroid/app/Activity;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/ce;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->c:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->d:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ep;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->k:Lcom/google/android/apps/youtube/app/ui/ep;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/eq;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->j:Lcom/google/android/apps/youtube/app/ui/eq;

    invoke-static {p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->A:Lcom/google/android/apps/youtube/app/am;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/en;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/en;-><init>(Lcom/google/android/apps/youtube/app/ui/ei;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/em;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/em;-><init>(Lcom/google/android/apps/youtube/app/ui/ei;)V

    invoke-virtual {p7, p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-direct {v2, p3, p7, v0, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;Lcom/google/android/apps/youtube/core/ui/v;Landroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    move-object v0, p12

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->x:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->x:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    const/16 v0, 0x8

    invoke-virtual {p12, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ej;

    invoke-virtual {p3}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p3}, Lcom/google/android/apps/youtube/app/ui/ej;-><init>(Lcom/google/android/apps/youtube/app/ui/ei;Landroid/os/Looper;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a()Landroid/widget/ImageView;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    return-void
.end method

.method private A()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private C()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    :cond_0
    return-void
.end method

.method private D()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setHasNext(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setHasPrevious(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private E()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setScrubbingEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getLengthSeconds()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->t()D

    move-result-wide v1

    double-to-int v1, v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->u:I

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setScrubbingEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->u:I

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setTimes(III)V

    goto :goto_0

    :cond_1
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->u:I

    goto :goto_1

    :cond_2
    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->u:I

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ei;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->C()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ei;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ei;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ei;)Z
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/ei;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ei;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->r:Z

    return v0
.end method

.method private b(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->b:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->dN:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->y()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne p1, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->b:Landroid/app/Activity;

    sget v2, Lcom/google/android/youtube/p;->ay:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setPlayingOnText(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/ei;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->E()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/ei;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    return-object v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->w()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->x()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->w()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->o:Z

    if-eqz v0, :cond_1

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->u:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->D()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto :goto_0
.end method

.method private d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setMinimized(Z)V

    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->C()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/ei;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/ei;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/ei;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->r:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/ui/RemoteControlView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/ui/ei;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->d:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/ui/eq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->j:Lcom/google/android/apps/youtube/app/ui/eq;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/app/ui/ei;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->B()Z

    move-result v0

    return v0
.end method

.method private w()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->v:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->z:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->u:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->e()V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->q:Z

    return-void
.end method

.method private x()V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->g:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->G()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->y:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->x:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->j:Lcom/google/android/apps/youtube/app/ui/eq;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/app/ui/eq;->h(Z)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->t:Z

    :cond_0
    return-void
.end method

.method private y()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/bg;->getScreenName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private z()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->B()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "RemotePlay"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "RemotePlayVideo"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->r:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getLengthSeconds()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->e()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->v:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v0

    if-nez v0, :cond_1

    iput-object v5, p0, Lcom/google/android/apps/youtube/app/ui/ei;->v:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->B()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ei;->b(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq p1, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-virtual {v0, p1, v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;Lcom/google/android/apps/youtube/app/remote/ar;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_3
    :goto_1
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq p1, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne p1, v0, :cond_7

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    invoke-static {v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->E()V

    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/eo;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->q:Z

    if-nez v0, :cond_2

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->q:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getThumbnailDetails()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    :cond_2
    :goto_2
    :pswitch_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getCaptionTracksUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setHasCc(Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_3

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->t:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq p1, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v3, "RemoteStateChange"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->t:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->g:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->F()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->x:Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->y:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->t:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->j:Lcom/google/android/apps/youtube/app/ui/eq;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/app/ui/eq;->h(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->i:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->A:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;ZLcom/google/android/apps/youtube/core/client/WatchFeature;)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v3, "RemoteError"

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c()Lcom/google/android/apps/youtube/app/remote/as;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/youtube/app/remote/as;->a:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->x()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c()V

    goto/16 :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->b(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->x()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c()Lcom/google/android/apps/youtube/app/remote/as;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/bg;->getScreenName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/apps/youtube/app/remote/as;Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-ne v0, p1, :cond_0

    const-string v0, "Already connected to the same remote control."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->o()V

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->s:Z

    if-eqz v0, :cond_2

    invoke-interface {p1, p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Lcom/google/android/apps/youtube/app/remote/aq;)V

    invoke-interface {p1, p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(Lcom/google/android/apps/youtube/app/remote/aq;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->C()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/client/WatchFeature;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v5, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->l:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->l:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->l:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    if-nez p1, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->q:Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->q:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->e()V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->C:Ljava/lang/String;

    :cond_3
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->o:Z

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->b:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/ek;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/ek;-><init>(Lcom/google/android/apps/youtube/app/ui/ei;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->l:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a:[B

    const-string v3, ""

    const-string v4, ""

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/ei;->l:Lcom/google/android/apps/youtube/common/a/d;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->z:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->setCcEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 7

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Video changed received for a non connected remote. Will ignore"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->A()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->w()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->C:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->A:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->q()I

    move-result v3

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    if-eqz v2, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/ei;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    :goto_1
    const/4 v6, 0x1

    move-object v2, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V

    goto :goto_0

    :cond_3
    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    goto :goto_1

    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ei;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 14

    const/4 v6, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-nez v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->C:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->A:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v1

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move v4, v3

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/ei;->A:Lcom/google/android/apps/youtube/app/am;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->q()I

    move-result v10

    sget-object v12, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move v11, v3

    move v13, v6

    invoke-interface/range {v7 .. v13}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->D()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->B:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ei;->d(Z)V

    return-void
.end method

.method public final a_(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "RemotePause"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PAUSED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->o:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->p:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->C:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->k:Lcom/google/android/apps/youtube/app/ui/ep;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ep;->d(Z)V

    return-void
.end method

.method public final c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->f()V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->C:Ljava/lang/String;

    return-void
.end method

.method public final c(Z)V
    .locals 1

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->B:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->d(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->k()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->A:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->q()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->m()V

    goto :goto_0
.end method

.method public final e()V
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->h()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->A:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->q()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->i()V

    goto :goto_0
.end method

.method public final f()V
    .locals 0

    return-void
.end method

.method public final g()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->c:Lcom/google/android/apps/youtube/core/client/ce;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ei;->b:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/el;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/ui/el;-><init>(Lcom/google/android/apps/youtube/app/ui/ei;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/ce;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final h()V
    .locals 0

    return-void
.end method

.method public final i()V
    .locals 0

    return-void
.end method

.method public final j()V
    .locals 0

    return-void
.end method

.method public final k()V
    .locals 0

    return-void
.end method

.method public final l()V
    .locals 0

    return-void
.end method

.method public final l_()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->w:Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->m:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public final m()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->e()V

    :cond_0
    return-void
.end method

.method public final n()V
    .locals 0

    return-void
.end method

.method public final o()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->l:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->l:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->l:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(Lcom/google/android/apps/youtube/app/remote/aq;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    :cond_1
    return-void
.end method

.method public final p()V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->s:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "cannot call onResume() multiple times, need to call onPause() first"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/ei;->s:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(Lcom/google/android/apps/youtube/app/remote/aq;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ei;->C()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->s:Z

    const-string v1, "cannot call onPause() multiple times, need to call onResume() first"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->s:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(Lcom/google/android/apps/youtube/app/remote/aq;)V

    :cond_0
    return-void
.end method

.method public final r()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->t:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->u:I

    return v0
.end method

.method public final u()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->v:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method public final v()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ei;->z:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    return-object v0
.end method
