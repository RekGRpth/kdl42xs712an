.class final Lcom/google/android/apps/youtube/app/fragments/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->b(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v2, Lcom/google/android/youtube/p;->cL:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->F()V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->bio:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->b(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v2, Lcom/google/android/youtube/p;->cL:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->d(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Lcom/google/android/apps/youtube/app/ui/dw;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/youtube/app/ui/dw;->a(Landroid/app/Activity;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/b;-><init>(Lcom/google/android/apps/youtube/app/fragments/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->b(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->bio:Ljava/lang/String;

    const-string v2, "[\\r\\n]+"

    const-string v3, "\r\n\r\n"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->b(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/a;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)V

    goto :goto_0
.end method
