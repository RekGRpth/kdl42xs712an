.class public Lcom/google/android/apps/youtube/app/ui/ie;
.super Lcom/google/android/apps/youtube/core/ui/l;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private final a:Z

.field private final b:Lcom/google/android/apps/youtube/app/ui/ck;

.field private final g:Lcom/google/android/apps/youtube/core/a/a;

.field private final h:Lcom/google/android/apps/youtube/app/ui/et;

.field private final i:Lcom/google/android/apps/youtube/app/ui/ig;

.field private j:Lcom/google/android/apps/youtube/app/ui/ih;

.field private final k:Lcom/google/android/apps/youtube/app/ui/ii;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;)V
    .locals 13

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/app/ui/ie;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;Lcom/google/android/apps/youtube/app/ui/ii;)V

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;Lcom/google/android/apps/youtube/app/ui/ii;)V
    .locals 10

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/if;

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/am;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    move/from16 v2, p8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/if;-><init>(Lcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;)V

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move-object v8, v0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/youtube/app/ui/ie;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/ui/ig;Lcom/google/android/apps/youtube/app/ui/ii;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/ui/ig;)V
    .locals 9

    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/app/ui/ie;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/ui/ig;Lcom/google/android/apps/youtube/app/ui/ii;)V

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/ui/ig;Lcom/google/android/apps/youtube/app/ui/ii;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/youtube/core/ui/l;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;)V

    iput-boolean p6, p0, Lcom/google/android/apps/youtube/app/ui/ie;->a:Z

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ck;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/ui/ck;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->b:Lcom/google/android/apps/youtube/app/ui/ck;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ig;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->i:Lcom/google/android/apps/youtube/app/ui/ig;

    iput-object p8, p0, Lcom/google/android/apps/youtube/app/ui/ie;->k:Lcom/google/android/apps/youtube/app/ui/ii;

    instance-of v0, p3, Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    check-cast p3, Lcom/google/android/apps/youtube/app/ui/et;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/ie;->h:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->h:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/et;->b()Lcom/google/android/apps/youtube/core/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->g:Lcom/google/android/apps/youtube/core/a/a;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->h:Lcom/google/android/apps/youtube/app/ui/et;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/ie;->g:Lcom/google/android/apps/youtube/core/a/a;

    invoke-interface {p2, p0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->elementsPerPage:I

    if-ge v0, v1, :cond_1

    iget v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->startIndex:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->k:Lcom/google/android/apps/youtube/app/ui/ii;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->k:Lcom/google/android/apps/youtube/app/ui/ii;

    :cond_0
    return-void

    :cond_1
    iget v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->totalResults:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .locals 1

    instance-of v0, p2, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->k:Lcom/google/android/apps/youtube/app/ui/ii;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->k:Lcom/google/android/apps/youtube/app/ui/ii;

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->b:Lcom/google/android/apps/youtube/app/ui/ck;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ck;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final n_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->k:Lcom/google/android/apps/youtube/app/ui/ii;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->k:Lcom/google/android/apps/youtube/app/ui/ii;

    :cond_0
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ie;->i:Lcom/google/android/apps/youtube/app/ui/ig;

    invoke-interface {v1, v0, p3}, Lcom/google/android/apps/youtube/app/ui/ig;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->g:Lcom/google/android/apps/youtube/core/a/a;

    goto :goto_0
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->j:Lcom/google/android/apps/youtube/app/ui/ih;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/ih;->a()Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ie;->g:Lcom/google/android/apps/youtube/core/a/a;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
