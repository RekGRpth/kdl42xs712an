.class public abstract Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field protected final stack:Lcom/google/android/apps/youtube/common/e/l;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->readEntryFromParcel(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public pop()Lcom/google/android/apps/youtube/app/fragments/navigation/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    return-object v0
.end method

.method public popAll()Lcom/google/android/apps/youtube/app/fragments/navigation/a;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/l;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public final push(Lcom/google/android/apps/youtube/app/fragments/navigation/a;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method protected abstract readEntryFromParcel(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/app/fragments/navigation/a;
.end method

.method protected abstract writeEntryToParcel(Lcom/google/android/apps/youtube/app/fragments/navigation/a;Landroid/os/Parcel;I)V
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->iterator()Ljava/util/Iterator;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->size()I

    move-result v1

    new-array v4, v1, [Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    move v0, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    aput-object v0, v4, v2

    move v0, v2

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    aget-object v2, v4, v0

    invoke-virtual {p0, v2, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->writeEntryToParcel(Lcom/google/android/apps/youtube/app/fragments/navigation/a;Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
