.class public Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# instance fields
.field private n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private o:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private p:Lcom/google/android/apps/youtube/core/ui/PagedListView;

.field private q:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->e()V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Landroid/support/v4/app/FragmentActivity;)V
    .locals 5

    const/4 v4, 0x1

    sget v0, Lcom/google/android/youtube/p;->aZ:I

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v4}, Lcom/google/android/apps/youtube/core/utils/ah;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;)Landroid/widget/ArrayAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->q:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->o:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->q:Landroid/widget/ArrayAdapter;

    new-instance v3, Lcom/google/android/apps/youtube/app/honeycomb/phone/ad;

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ad;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/compat/j;->a()V

    const/4 v0, 0x1

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public handleSignOutEvent(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->l()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->o:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    sget v0, Lcom/google/android/youtube/l;->aX:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->setContentView(I)V

    sget v0, Lcom/google/android/youtube/p;->dW:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->b(I)V

    new-instance v5, Lcom/google/android/apps/youtube/app/honeycomb/phone/o;

    invoke-direct {v5, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/o;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;)V

    sget v0, Lcom/google/android/youtube/j;->ep:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->p:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ac;

    sget v3, Lcom/google/android/youtube/l;->aW:I

    sget v4, Lcom/google/android/youtube/j;->eq:I

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ac;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;Landroid/content/Context;IILandroid/view/View$OnClickListener;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->q:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->p:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->q:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->p:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/q;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/q;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->E()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->E()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->e()V

    return-void
.end method
