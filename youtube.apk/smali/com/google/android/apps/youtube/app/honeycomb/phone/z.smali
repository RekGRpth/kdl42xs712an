.class final Lcom/google/android/apps/youtube/app/honeycomb/phone/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

.field private final b:Landroid/widget/EditText;

.field private final c:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;Landroid/widget/EditText;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->b:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->c:Landroid/os/Bundle;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;Landroid/widget/EditText;Landroid/os/Bundle;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;Landroid/widget/EditText;Landroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->c:Landroid/os/Bundle;

    const-string v1, "YouTubeScreen"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->b:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->l()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->m()Lcom/google/android/apps/youtube/app/remote/bw;

    move-result-object v7

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->a(Ljava/lang/String;Landroid/support/v4/app/FragmentActivity;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/youtube/app/honeycomb/phone/y;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-static {v5}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;)Lcom/google/android/apps/youtube/app/honeycomb/phone/x;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lcom/google/android/apps/youtube/app/honeycomb/phone/y;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/x;Landroid/support/v4/app/FragmentActivity;)V

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    invoke-virtual {v4, v2, v1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;)Lcom/google/android/apps/youtube/app/honeycomb/phone/x;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;-><init>(Ljava/lang/String;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/honeycomb/phone/x;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Landroid/support/v4/app/FragmentActivity;B)V

    invoke-static {v8, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    invoke-interface {v7, v0}, Lcom/google/android/apps/youtube/app/remote/bw;->b(Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
