.class public final Lcom/google/android/apps/youtube/app/offline/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/a/a;

.field private final b:Lcom/google/android/apps/youtube/core/offline/store/i;

.field private final c:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/a/a;Lcom/google/android/apps/youtube/core/offline/store/i;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->b:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/a/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->a:Lcom/google/android/apps/youtube/app/a/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->c:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->a:Lcom/google/android/apps/youtube/app/a/a;

    invoke-interface {v1, p1}, Lcom/google/android/apps/youtube/app/a/a;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isForecastingAd()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error loading non-YouTube-hosted ad video [request="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading vast ad [originalVideoId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    :try_end_1
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VastException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->b:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->b:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/offline/store/i;->t(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VastException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_3
    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error saving vast ad [originalVideoId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VastException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method private b(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->a:Lcom/google/android/apps/youtube/app/a/a;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v2}, Lcom/google/android/apps/youtube/app/a/a;->a(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->b:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;Ljava/util/List;)Z

    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->b:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading ad breaks for ad [originalVideoId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error saving ad breaks for ad [originalVideoId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->b:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->r(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/c;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->firstPrerollAdBreak(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->b:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/google/android/apps/youtube/core/offline/store/i;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/offline/a/c;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/util/Map;
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->b:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->e()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/h;

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/offline/store/g;->a:Ljava/lang/String;

    new-instance v4, Lcom/google/a/a/a/a/mk;

    invoke-direct {v4}, Lcom/google/a/a/a/a/mk;-><init>()V

    sget-object v5, Lcom/google/android/apps/youtube/app/offline/a/d;->a:[I

    iget-object v6, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget-object v6, v6, Lcom/google/android/apps/youtube/core/offline/store/g;->d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :goto_1
    new-instance v5, Lcom/google/a/a/a/a/ml;

    invoke-direct {v5}, Lcom/google/a/a/a/a/ml;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/a/a/a/a/mk;

    aput-object v4, v0, v10

    iput-object v0, v5, Lcom/google/a/a/a/a/ml;->b:[Lcom/google/a/a/a/a/mk;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_0
    iput v10, v4, Lcom/google/a/a/a/a/mk;->b:I

    iput v10, v4, Lcom/google/a/a/a/a/mk;->d:I

    goto :goto_1

    :pswitch_1
    const/4 v5, 0x2

    iput v5, v4, Lcom/google/a/a/a/a/mk;->b:I

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget-wide v6, v6, Lcom/google/android/apps/youtube/core/offline/store/g;->e:J

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v8}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v5

    invoke-static {v11, v12, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    long-to-int v5, v5

    iput v5, v4, Lcom/google/a/a/a/a/mk;->d:I

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget v5, v5, Lcom/google/android/apps/youtube/core/offline/store/g;->f:I

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget v0, v0, Lcom/google/android/apps/youtube/core/offline/store/g;->g:I

    sub-int v0, v5, v0

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v4, Lcom/google/a/a/a/a/mk;->e:I

    goto :goto_1

    :pswitch_2
    iget-object v5, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->b:Lcom/google/android/apps/youtube/core/offline/store/e;

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->b:Lcom/google/android/apps/youtube/core/offline/store/e;

    iget-object v5, v5, Lcom/google/android/apps/youtube/core/offline/store/e;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v6, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-eq v5, v6, :cond_2

    :cond_1
    const/4 v5, 0x3

    iput v5, v4, Lcom/google/a/a/a/a/mk;->b:I

    :goto_2
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget-wide v6, v6, Lcom/google/android/apps/youtube/core/offline/store/g;->e:J

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/offline/a/c;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v8}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v5

    invoke-static {v11, v12, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    long-to-int v5, v5

    iput v5, v4, Lcom/google/a/a/a/a/mk;->d:I

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget-object v5, v5, Lcom/google/android/apps/youtube/core/offline/store/g;->c:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/a/a/a/a/mk;->c:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget v5, v5, Lcom/google/android/apps/youtube/core/offline/store/g;->f:I

    iget-object v6, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->b:Lcom/google/android/apps/youtube/core/offline/store/e;

    iget v6, v6, Lcom/google/android/apps/youtube/core/offline/store/e;->b:I

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->a:Lcom/google/android/apps/youtube/core/offline/store/g;

    iget v0, v0, Lcom/google/android/apps/youtube/core/offline/store/g;->g:I

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int v0, v5, v0

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v4, Lcom/google/a/a/a/a/mk;->e:I

    goto/16 :goto_1

    :cond_2
    const/4 v5, 0x4

    iput v5, v4, Lcom/google/a/a/a/a/mk;->b:I

    goto :goto_2

    :cond_3
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
