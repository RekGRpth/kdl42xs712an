.class public final Lcom/google/android/apps/youtube/app/player/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/t;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/ax;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ax;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/player/b;->a:Lcom/google/android/apps/youtube/app/ax;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)Lcom/google/android/apps/youtube/medialib/player/n;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/player/b;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bd()Lcom/google/android/apps/youtube/core/player/a/l;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isFile()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/a/o;

    new-instance v2, Lcom/google/android/apps/youtube/medialib/player/m;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/medialib/player/m;-><init>()V

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/youtube/core/player/a/o;-><init>(Lcom/google/android/apps/youtube/medialib/player/n;Lcom/google/android/apps/youtube/core/player/a/l;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "Cannot create ProxyPlayer because MediaServer is null"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/player/b;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aj()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getVideoId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "Using Regular Player with ExoProxy."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/a/g;

    new-instance v2, Lcom/google/android/apps/youtube/medialib/player/m;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/medialib/player/m;-><init>()V

    invoke-direct {v0, v2, v1, p1}, Lcom/google/android/apps/youtube/core/player/a/g;-><init>(Lcom/google/android/apps/youtube/medialib/player/n;Lcom/google/android/apps/youtube/core/player/a/l;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    goto :goto_0

    :cond_2
    const-string v0, "Using Regular Player."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/m;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/medialib/player/m;-><init>()V

    goto :goto_0
.end method
