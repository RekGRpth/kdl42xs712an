.class public final Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;
.super Lcom/google/android/apps/youtube/app/adapter/g;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Ljava/util/WeakHashMap;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/apps/youtube/core/identity/l;

.field private final e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private final f:Lcom/google/android/apps/youtube/common/c/a;

.field private final g:Lcom/google/android/apps/youtube/core/Analytics;

.field private final h:Lcom/google/android/apps/youtube/core/client/bc;

.field private final i:Lcom/google/android/apps/youtube/app/am;

.field private final j:Lcom/google/android/apps/youtube/core/aw;

.field private final k:Lcom/google/android/apps/youtube/app/adapter/m;

.field private final l:Ljava/util/Map;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Lcom/google/android/apps/youtube/app/ui/gj;

.field private final p:Landroid/app/Activity;

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/YouTubeApplication;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/gj;Landroid/app/Activity;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/g;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/apps/youtube/core/client/bc;

    iput-object p8, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->j:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/apps/youtube/core/identity/l;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->f:Lcom/google/android/apps/youtube/common/c/a;

    iput-object p7, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->i:Lcom/google/android/apps/youtube/app/am;

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/adapter/m;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Lcom/google/android/apps/youtube/app/adapter/m;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Ljava/util/Map;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    iput-object p9, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->o:Lcom/google/android/apps/youtube/app/ui/gj;

    iput-object p10, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->p:Landroid/app/Activity;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Landroid/net/Uri;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/adapter/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->k:Lcom/google/android/apps/youtube/app/adapter/m;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->o:Lcom/google/android/apps/youtube/app/ui/gj;

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gj;->b(Landroid/net/Uri;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->selfUri:Landroid/net/Uri;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/app/adapter/s;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/app/adapter/s;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/adapter/s;->b(Lcom/google/android/apps/youtube/app/adapter/s;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v0

    if-ne v0, p2, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/adapter/s;->a(Lcom/google/android/apps/youtube/app/adapter/s;)Landroid/view/View;

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b()V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/app/adapter/s;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-static {p2}, Lcom/google/android/apps/youtube/app/adapter/s;->a(Lcom/google/android/apps/youtube/app/adapter/s;)Landroid/view/View;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private b(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->channelUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/s;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/s;->a(Lcom/google/android/apps/youtube/app/adapter/s;)Landroid/view/View;

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->selfUri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->r:Z

    return v0
.end method

.method private static c(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    const-string v2, "http"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    return-object v0
.end method

.method private c()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->w()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->n()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->p:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/o;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/youtube/app/adapter/o;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;B)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c:Landroid/content/Context;

    return-object v0
.end method

.method private d(Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/s;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/s;->b(Lcom/google/android/apps/youtube/app/adapter/s;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/s;->a(Lcom/google/android/apps/youtube/app/adapter/s;)Landroid/view/View;

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/identity/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/apps/youtube/core/identity/l;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->p:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/YouTubeApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->g:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->m:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->f:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/ui/gj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->o:Lcom/google/android/apps/youtube/app/ui/gj;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->j:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->i:Lcom/google/android/apps/youtube/app/am;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/s;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/s;->b(Lcom/google/android/apps/youtube/app/adapter/s;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/s;->b(Lcom/google/android/apps/youtube/app/adapter/s;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v2, v3, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/s;->b(Lcom/google/android/apps/youtube/app/adapter/s;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/app/adapter/s;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->r:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/s;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->q:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c()V

    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/s;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/s;->b(Lcom/google/android/apps/youtube/app/adapter/s;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->r:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c()V

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/core/identity/UserProfile;Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->h:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/p;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/p;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v1

    invoke-interface {v0, p4, v1}, Lcom/google/android/apps/youtube/core/client/bc;->e(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->n:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l:Ljava/util/Map;

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Landroid/net/Uri;)V

    return-void
.end method
