.class public Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/adapter/af;

.field private Z:Lcom/google/android/apps/youtube/app/ui/et;

.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private aa:Lcom/google/android/apps/youtube/app/ui/v;

.field private ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private ac:Lcom/google/android/apps/youtube/app/remote/an;

.field private b:Landroid/content/res/Resources;

.field private d:Lcom/google/android/apps/youtube/core/async/af;

.field private e:Lcom/google/android/apps/youtube/core/client/bc;

.field private f:Lcom/google/android/apps/youtube/core/client/bj;

.field private g:Lcom/google/android/apps/youtube/core/identity/l;

.field private h:Lcom/google/android/apps/youtube/core/aw;

.field private i:Lcom/google/android/apps/youtube/app/ui/ie;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/k;->m:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->Y:Lcom/google/android/apps/youtube/app/adapter/af;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/cf;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/fragments/cf;-><init>(Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->editUri:Landroid/net/Uri;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/core/client/bc;->c(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 14

    sget v1, Lcom/google/android/youtube/l;->bK:I

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    sget v2, Lcom/google/android/youtube/p;->fg:I

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/ce;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/ce;-><init>(Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/youtube/app/adapter/ag;->d(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->Y:Lcom/google/android/apps/youtube/app/adapter/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->Y:Lcom/google/android/apps/youtube/app/adapter/af;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v3, Lcom/google/android/youtube/j;->gn:I

    invoke-virtual {v13, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/core/ui/PagedView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->d:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v8

    const/4 v9, 0x1

    sget-object v10, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WATCH_LATER:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->WatchLater:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/youtube/app/ui/ie;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->i:Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->ac:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WATCH_LATER:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->i:Lcom/google/android/apps/youtube/app/ui/ie;

    const-string v2, "watch_later_helper"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Landroid/os/Bundle;)V

    :cond_0
    return-object v13
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    sget v0, Lcom/google/android/youtube/p;->aj:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->b:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->g:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->ac:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->q()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->d:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_your_channel"

    return-object v0
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->L()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->i:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz v0, :cond_0

    const-string v0, "watch_later_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->i:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ie;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->L()V

    return-void
.end method

.method public final r()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->g:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->i:Lcom/google/android/apps/youtube/app/ui/ie;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->k()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ie;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    return-void
.end method
