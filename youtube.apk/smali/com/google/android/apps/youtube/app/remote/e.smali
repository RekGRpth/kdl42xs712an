.class public abstract Lcom/google/android/apps/youtube/app/remote/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/RemoteControl;


# instance fields
.field private final a:Ljava/util/Set;

.field private final b:Ljava/util/Set;

.field private final c:Ljava/util/Set;

.field private final d:Ljava/util/Set;

.field private final e:Landroid/os/Handler;

.field private f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

.field private g:Lcom/google/android/apps/youtube/app/remote/as;

.field private final h:Lcom/google/android/apps/youtube/core/identity/l;

.field private final i:Lcom/google/android/apps/youtube/core/client/bc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/client/bc;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->i:Lcom/google/android/apps/youtube/core/client/bc;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->d:Ljava/util/Set;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/e;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->d:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/e;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->a:Ljava/util/Set;

    return-object v0
.end method

.method private b(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/k;-><init>(Lcom/google/android/apps/youtube/app/remote/e;Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/e;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->c:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/o;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/remote/o;-><init>(Lcom/google/android/apps/youtube/app/remote/e;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/n;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/n;-><init>(Lcom/google/android/apps/youtube/app/remote/e;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/m;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/m;-><init>(Lcom/google/android/apps/youtube/app/remote/e;Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq p1, v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v1, "use toErrorState for ERROR state"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/e;->b(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/youtube/app/remote/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/g;-><init>(Lcom/google/android/apps/youtube/app/remote/e;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/youtube/app/remote/aq;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Lcom/google/android/apps/youtube/app/remote/as;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    const-string v0, "Remote control trying to move to ERROR state while OFFLINE"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/e;->g:Lcom/google/android/apps/youtube/app/remote/as;

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/e;->b(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/au;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/f;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/f;-><init>(Lcom/google/android/apps/youtube/app/remote/e;Lcom/google/android/apps/youtube/app/remote/au;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/youtube/app/remote/aw;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/p;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/p;-><init>(Lcom/google/android/apps/youtube/app/remote/e;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/j;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/j;-><init>(Lcom/google/android/apps/youtube/app/remote/e;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected abstract a(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected final a(Ljava/util/List;)V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/l;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/app/remote/l;-><init>(Lcom/google/android/apps/youtube/app/remote/e;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/bg;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/e;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->f:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    return-object v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/youtube/app/remote/aq;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/youtube/app/remote/au;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/i;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/i;-><init>(Lcom/google/android/apps/youtube/app/remote/e;Lcom/google/android/apps/youtube/app/remote/au;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/youtube/app/remote/aw;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->i:Lcom/google/android/apps/youtube/core/client/bc;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/remote/h;-><init>(Lcom/google/android/apps/youtube/app/remote/e;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/client/bc;->l(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    return-void
.end method

.method public final c()Lcom/google/android/apps/youtube/app/remote/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->g:Lcom/google/android/apps/youtube/app/remote/as;

    return-object v0
.end method

.method public final declared-synchronized c(Lcom/google/android/apps/youtube/app/remote/aq;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "listener not added, cannot be made active"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/e;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/e;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/e;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract d()V
.end method
