.class final Lcom/google/android/apps/youtube/app/ui/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/be;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/be;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/bf;->a:Lcom/google/android/apps/youtube/app/ui/be;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error creating playlist"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bf;->a:Lcom/google/android/apps/youtube/app/ui/be;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/be;->d:Lcom/google/android/apps/youtube/app/ui/bd;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bd;->c(Lcom/google/android/apps/youtube/app/ui/bd;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bf;->a:Lcom/google/android/apps/youtube/app/ui/be;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/be;->d:Lcom/google/android/apps/youtube/app/ui/bd;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bd;->b(Lcom/google/android/apps/youtube/app/ui/bd;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/cp;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/ui/cp;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bf;->a:Lcom/google/android/apps/youtube/app/ui/be;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/be;->c:Lcom/google/android/apps/youtube/app/ui/bg;

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/app/ui/bg;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bf;->a:Lcom/google/android/apps/youtube/app/ui/be;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/be;->d:Lcom/google/android/apps/youtube/app/ui/bd;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bd;->a(Lcom/google/android/apps/youtube/app/ui/bd;)Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->aC:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    return-void
.end method
