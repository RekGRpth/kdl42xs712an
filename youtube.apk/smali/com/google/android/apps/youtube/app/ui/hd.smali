.class public final Lcom/google/android/apps/youtube/app/ui/hd;
.super Lcom/google/android/apps/youtube/core/ui/l;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/youtube/core/transfer/h;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/ui/ck;

.field private final b:Lcom/google/android/apps/youtube/app/adapter/ba;

.field private final g:Lcom/google/android/apps/youtube/app/ui/et;

.field private final h:Lcom/google/android/apps/youtube/core/identity/ak;

.field private final i:Lcom/google/android/apps/youtube/core/client/bc;

.field private final j:Lcom/google/android/apps/youtube/core/Analytics;

.field private k:Lcom/google/android/apps/youtube/core/utils/w;

.field private final l:Lcom/google/android/apps/youtube/app/am;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/app/adapter/ba;Lcom/google/android/apps/youtube/app/ui/et;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;)V
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/he;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/ui/he;-><init>()V

    invoke-static {p6, v0}, Lcom/google/android/apps/youtube/core/async/g;->a(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/d;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p5

    move-object v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/ui/l;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->i:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->h:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->l:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/et;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->g:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->g:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->j:Lcom/google/android/apps/youtube/core/Analytics;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ck;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/ui/ck;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->a:Lcom/google/android/apps/youtube/app/ui/ck;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/ha;)Lcom/google/android/apps/youtube/app/ui/ha;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/adapter/ba;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/ba;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ha;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ha;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hd;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 6

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v1, "upload_file_duration"

    iget v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->duration:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    sub-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->buildUpon()Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hd;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ha;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Lcom/google/android/apps/youtube/app/ui/ha;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hd;->a(Lcom/google/android/apps/youtube/app/ui/ha;)Lcom/google/android/apps/youtube/app/ui/ha;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/ui/ha;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/android/apps/youtube/app/ui/ha;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/app/adapter/ba;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private g(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hd;->h:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/ak;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/hd;->h(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    :cond_0
    return-void
.end method

.method private h(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ha;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Lcom/google/android/apps/youtube/app/ui/ha;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hd;->a(Lcom/google/android/apps/youtube/app/ui/ha;)Lcom/google/android/apps/youtube/app/ui/ha;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/app/adapter/ba;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/hg;

    invoke-direct {v1, p1}, Lcom/google/android/apps/youtube/app/ui/hg;-><init>(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/ba;->a(Lcom/google/android/apps/youtube/common/fromguava/d;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/adapter/ba;->c(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/ui/l;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    :cond_0
    return-void
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/util/List;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/app/ui/ha;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/ha;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->a:Lcom/google/android/apps/youtube/app/ui/ck;

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/ui/ha;->a:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ck;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/hd;->g(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    return-void
.end method

.method public final c()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/ui/l;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hd;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/utils/w;->b(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/ba;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/hd;->h(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 4

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hd;->c:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/hf;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lcom/google/android/apps/youtube/app/ui/hf;-><init>(Lcom/google/android/apps/youtube/app/ui/hd;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;B)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hd;->i:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/youtube/core/client/bc;->f(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/hd;->g(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    goto :goto_0
.end method

.method public final e(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ha;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Lcom/google/android/apps/youtube/app/ui/ha;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/ba;->b(Ljava/lang/Object;)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-eq v0, v1, :cond_0

    iget v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->d:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->c:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->M:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    :cond_0
    return-void
.end method

.method public final f(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/k;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final m_()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->k:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/k;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hd;->g(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/adapter/ba;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hd;->b:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/app/adapter/ba;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ha;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ha;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hd;->j:Lcom/google/android/apps/youtube/core/Analytics;

    sget-object v2, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->Uploads:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    invoke-interface {v1, v2, p3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hd;->l:Lcom/google/android/apps/youtube/app/am;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/ha;->a:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->MY_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;ZLcom/google/android/apps/youtube/core/client/WatchFeature;)V

    :cond_0
    return-void
.end method
