.class public final Lcom/google/android/apps/youtube/app/ui/dw;
.super Lcom/google/android/apps/youtube/app/ui/fm;
.source "SourceFile"


# instance fields
.field private final f:Lcom/google/android/apps/youtube/core/client/bc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/ui/fp;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/fm;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/fp;)V

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dw;->f:Lcom/google/android/apps/youtube/core/client/bc;

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/apps/youtube/app/ui/fq;Ljava/lang/Object;)V
    .locals 6

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/ui/fq;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/dw;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->B:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dw;->b:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->id:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->artistTape:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/MusicVideo;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/MusicVideo;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dw;->f:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->x()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/dx;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/youtube/app/ui/dx;-><init>(Lcom/google/android/apps/youtube/app/ui/dw;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
