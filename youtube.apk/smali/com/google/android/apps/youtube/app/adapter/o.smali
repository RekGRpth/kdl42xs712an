.class final Lcom/google/android/apps/youtube/app/adapter/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/o;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/o;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/o;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->q(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/o;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->p(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/o;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->p(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/o;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/o;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->q(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    return-void
.end method
