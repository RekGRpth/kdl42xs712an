.class public final Lcom/google/android/apps/youtube/app/offline/sync/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/d/i;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/offline/sync/b;

.field private final b:Lcom/google/android/apps/youtube/common/network/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/offline/sync/b;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/sync/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/e;->a:Lcom/google/android/apps/youtube/app/offline/sync/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/e;->b:Lcom/google/android/apps/youtube/common/network/h;

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/a/a/g;)Lcom/google/android/apps/youtube/common/d/h;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/sync/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/e;->a:Lcom/google/android/apps/youtube/app/offline/sync/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/sync/e;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/youtube/app/offline/sync/d;-><init>(Lcom/google/android/apps/youtube/a/a/g;Lcom/google/android/apps/youtube/app/offline/sync/b;Lcom/google/android/apps/youtube/common/network/h;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/app/offline/sync/d;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
