.class public final Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private watchState:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/iz;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/ui/iz;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->getWatchState(I)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->watchState:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/youtube/app/ui/iv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->watchState:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->watchState:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-object p1
.end method

.method private static getWatchState(I)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->values()[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget v4, v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->intValue:I

    if-ne v4, p0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    goto :goto_1
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->watchState:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    iget v0, v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->intValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
