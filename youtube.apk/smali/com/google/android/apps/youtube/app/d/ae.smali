.class public final Lcom/google/android/apps/youtube/app/d/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/core/identity/l;

.field private final d:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final e:Lcom/google/android/apps/youtube/core/aw;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/ImageView;

.field private k:Lcom/google/android/apps/youtube/app/d/ai;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/ax;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->a:Landroid/app/Activity;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->d:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->e:Lcom/google/android/apps/youtube/core/aw;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/d/ae;)Lcom/google/android/apps/youtube/app/d/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->k:Lcom/google/android/apps/youtube/app/d/ai;

    return-object v0
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/d/ae;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/d/ae;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->e:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/d/ae;)Lcom/google/android/apps/youtube/core/identity/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->d:Lcom/google/android/apps/youtube/core/identity/aa;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->d()Lcom/google/android/apps/youtube/core/identity/z;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/ae;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/z;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/d/ae;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/ae;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/z;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/d/ae;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/z;->c()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/ae;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/d/ae;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/z;->c()Landroid/net/Uri;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/d/ae;->j:Landroid/widget/ImageView;

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/youtube/app/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->j:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->am:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/j;->bw:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/youtube/j;->bz:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->f:Landroid/view/View;

    sget v3, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v3, Lcom/google/android/youtube/p;->cE:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->f:Landroid/view/View;

    sget v3, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v3, Lcom/google/android/youtube/h;->D:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->f:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/d/af;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/d/af;-><init>(Lcom/google/android/apps/youtube/app/d/ae;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->bu:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->j:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->g:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/d/ag;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/d/ag;-><init>(Lcom/google/android/apps/youtube/app/d/ae;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/d/ai;)V
    .locals 0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/ae;->k:Lcom/google/android/apps/youtube/app/d/ai;

    return-void
.end method

.method public final b()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ae;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
