.class public Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/app/remote/bg;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

.field private final screen:Lcom/google/android/apps/ytremote/model/CloudScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/bv;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/remote/bv;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    iget-object v3, p1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    if-nez v2, :cond_6

    iget-object v2, p1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    iget-object v3, p1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/ytremote/model/CloudScreen;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/SsdpId;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    return-object v0
.end method

.method public getScreenName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hasDevice()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasScreen()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public mustShowLockScreenControls()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "YouTubeTvScreen [device="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cloudScreen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->device:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->screen:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1
.end method
