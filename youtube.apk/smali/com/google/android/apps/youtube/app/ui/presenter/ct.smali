.class public final Lcom/google/android/apps/youtube/app/ui/presenter/ct;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/j;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/app/ui/presenter/f;

.field private final d:Lcom/google/android/apps/youtube/app/ui/presenter/cu;

.field private final e:Lcom/google/android/apps/youtube/app/ui/presenter/g;

.field private final f:Lcom/google/android/apps/youtube/app/d/r;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/presenter/f;Lcom/google/android/apps/youtube/app/ui/presenter/cu;Lcom/google/android/apps/youtube/app/ui/presenter/g;Lcom/google/android/apps/youtube/app/d/r;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/presenter/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->c:Lcom/google/android/apps/youtube/app/ui/presenter/f;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/presenter/cu;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->d:Lcom/google/android/apps/youtube/app/ui/presenter/cu;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/presenter/g;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->e:Lcom/google/android/apps/youtube/app/ui/presenter/g;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/d/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->f:Lcom/google/android/apps/youtube/app/d/r;

    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/android/apps/youtube/uilib/a/g;
    .locals 7

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->c:Lcom/google/android/apps/youtube/app/ui/presenter/f;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->d:Lcom/google/android/apps/youtube/app/ui/presenter/cu;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->e:Lcom/google/android/apps/youtube/app/ui/presenter/g;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ct;->f:Lcom/google/android/apps/youtube/app/d/r;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/ui/presenter/cq;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/presenter/f;Lcom/google/android/apps/youtube/app/ui/presenter/cu;Lcom/google/android/apps/youtube/app/ui/presenter/g;Lcom/google/android/apps/youtube/app/d/r;)V

    return-object v0
.end method
