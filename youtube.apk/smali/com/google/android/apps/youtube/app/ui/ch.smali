.class public final Lcom/google/android/apps/youtube/app/ui/ch;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final c:Lcom/google/android/apps/youtube/app/offline/r;

.field private final d:Lcom/google/android/apps/youtube/common/c/a;

.field private final e:Lcom/google/android/apps/youtube/app/am;

.field private final f:Lcom/google/android/apps/youtube/core/client/bj;

.field private final g:Lcom/google/android/apps/youtube/common/network/h;

.field private final h:Lcom/google/android/apps/youtube/app/offline/p;

.field private final i:Lcom/google/android/apps/youtube/core/player/w;

.field private final j:Lcom/google/android/apps/youtube/app/ui/ci;

.field private k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private l:Landroid/widget/ListView;

.field private m:Lcom/google/android/apps/youtube/uilib/a/h;

.field private final n:Lcom/google/android/apps/youtube/app/ui/hh;

.field private o:Lcom/google/android/apps/youtube/common/a/d;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/app/ui/hh;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/ci;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->e:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->c:Lcom/google/android/apps/youtube/app/offline/r;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/hh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->n:Lcom/google/android/apps/youtube/app/ui/hh;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->f:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->g:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->h:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/w;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->i:Lcom/google/android/apps/youtube/core/player/w;

    invoke-static {p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ci;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->j:Lcom/google/android/apps/youtube/app/ui/ci;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ch;)Lcom/google/android/apps/youtube/uilib/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->m:Lcom/google/android/apps/youtube/uilib/a/h;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/ch;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->o:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->o:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->o:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/cj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/cj;-><init>(Lcom/google/android/apps/youtube/app/ui/ch;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->o:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ch;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ch;->o:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/ch;)Lcom/google/android/apps/youtube/app/ui/ci;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->j:Lcom/google/android/apps/youtube/app/ui/ci;

    return-object v0
.end method

.method private handleOfflineVideoAddEvent(Lcom/google/android/apps/youtube/app/offline/a/aa;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ch;->b()V

    return-void
.end method

.method private handleOfflineVideoDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/ad;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ch;->b()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ch;->b()V

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 12

    sget v0, Lcom/google/android/youtube/j;->cm:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    sget v0, Lcom/google/android/youtube/j;->gf:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->l:Landroid/widget/ListView;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/ba;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ch;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ch;->d:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ch;->g:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ch;->e:Lcom/google/android/apps/youtube/app/am;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/ch;->i:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/ch;->n:Lcom/google/android/apps/youtube/app/ui/hh;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/ch;->c:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/ch;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/ch;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/ch;->h:Lcom/google/android/apps/youtube/app/offline/p;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/app/ui/presenter/ba;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/hh;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/offline/p;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ch;->m:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ch;->m:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ch;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ch;->m:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
