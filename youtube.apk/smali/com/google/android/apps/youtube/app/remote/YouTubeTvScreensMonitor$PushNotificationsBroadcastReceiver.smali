.class public Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/remote/bw;

.field private final b:Lcom/google/android/apps/youtube/core/identity/l;

.field private final c:Lcom/google/android/apps/youtube/app/remote/bk;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/remote/bk;)V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->a:Lcom/google/android/apps/youtube/app/remote/bw;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->b:Lcom/google/android/apps/youtube/core/identity/l;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->c:Lcom/google/android/apps/youtube/app/remote/bk;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "sm"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "\\"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "notificationType"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "automaticPairing"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "sm"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "\\"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "pairingData"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "pairingCode"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "tvName"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "tvName"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "tv_name"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v3, "isAvailable"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v3, "pairing_code"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->c:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->c:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->a:Lcom/google/android/apps/youtube/app/remote/bw;

    invoke-static {p1, v1, v0}, Lcom/google/android/apps/youtube/app/remote/a;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/bw;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Automatic Pairing Notification parsing failed"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;->a:Lcom/google/android/apps/youtube/app/remote/bw;

    invoke-static {p1, v1, v0}, Lcom/google/android/apps/youtube/app/remote/a;->b(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/bw;Landroid/os/Bundle;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
