.class final Lcom/google/android/apps/youtube/app/adapter/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/a;

.field private final b:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/adapter/a;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/b;->a:Lcom/google/android/apps/youtube/app/adapter/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/b;->b:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error retrieving user profile"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/b;->b:Lcom/google/android/apps/youtube/common/a/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/b;->a:Lcom/google/android/apps/youtube/app/adapter/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/a;->a(Lcom/google/android/apps/youtube/app/adapter/a;)Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/b;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->c(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
