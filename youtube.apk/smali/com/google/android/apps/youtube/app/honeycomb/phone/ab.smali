.class final Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

.field private final c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final d:Landroid/support/v4/app/FragmentActivity;

.field private final e:Lcom/google/android/apps/youtube/app/honeycomb/phone/x;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/honeycomb/phone/x;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->e:Lcom/google/android/apps/youtube/app/honeycomb/phone/x;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->d:Landroid/support/v4/app/FragmentActivity;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/honeycomb/phone/x;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Landroid/support/v4/app/FragmentActivity;B)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;-><init>(Ljava/lang/String;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/honeycomb/phone/x;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Landroid/support/v4/app/FragmentActivity;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->d:Landroid/support/v4/app/FragmentActivity;

    sget v1, Lcom/google/android/youtube/p;->bv:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->d:Landroid/support/v4/app/FragmentActivity;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->a:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->d:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->a(Ljava/lang/String;Landroid/support/v4/app/FragmentActivity;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->d:Landroid/support/v4/app/FragmentActivity;

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/y;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->e:Lcom/google/android/apps/youtube/app/honeycomb/phone/x;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->d:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/app/honeycomb/phone/y;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/x;Landroid/support/v4/app/FragmentActivity;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ab;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
