.class final Lcom/google/android/apps/youtube/app/ui/gx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/gt;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/gt;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/gx;->a:Lcom/google/android/apps/youtube/app/ui/gt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/gt;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/gx;-><init>(Lcom/google/android/apps/youtube/app/ui/gt;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error retrieving video info"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gx;->a:Lcom/google/android/apps/youtube/app/ui/gt;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/gt;->a(Lcom/google/android/apps/youtube/app/ui/gt;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PUBLIC:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-ne v0, v1, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gx;->a:Lcom/google/android/apps/youtube/app/ui/gt;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/gt;->a(Lcom/google/android/apps/youtube/app/ui/gt;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gx;->a:Lcom/google/android/apps/youtube/app/ui/gt;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/gt;->b(Lcom/google/android/apps/youtube/app/ui/gt;)Lcom/google/android/apps/youtube/common/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gx;->a:Lcom/google/android/apps/youtube/app/ui/gt;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/ui/gt;->a(Lcom/google/android/apps/youtube/app/ui/gt;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gx;->a:Lcom/google/android/apps/youtube/app/ui/gt;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/gt;->c(Lcom/google/android/apps/youtube/app/ui/gt;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerUri:Landroid/net/Uri;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/core/client/bc;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
