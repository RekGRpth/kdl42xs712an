.class final Lcom/google/android/apps/youtube/app/remote/bz;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/network/h;

.field final synthetic b:Lcom/google/android/apps/youtube/app/remote/bw;

.field final synthetic c:Lcom/google/android/apps/youtube/app/remote/bk;

.field final synthetic d:Lcom/google/android/apps/youtube/core/async/b;

.field final synthetic e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/core/async/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/bz;->a:Lcom/google/android/apps/youtube/common/network/h;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/remote/bz;->b:Lcom/google/android/apps/youtube/app/remote/bw;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/remote/bz;->c:Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/remote/bz;->d:Lcom/google/android/apps/youtube/core/async/b;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->a:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->b:Lcom/google/android/apps/youtube/app/remote/bw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/common/a/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bw;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    const-string v1, "Network unavailable. Removing all screens."

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->f(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->g(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x5

    if-ge v4, v5, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/bz;->c:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bg;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RemoteControl connected/connecting to "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " . Will not remove the screen from the list of available devices even though it timed out. Time out count: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->g(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Screen "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " timed out. Will check the app status."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bz;->d:Lcom/google/android/apps/youtube/core/async/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppUri()Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/youtube/app/remote/ci;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-direct {v4, v5, v0}, Lcom/google/android/apps/youtube/app/remote/ci;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/youtube/core/async/b;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto/16 :goto_2

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/ytremote/model/SsdpId;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/ck;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->h(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/ck;->a()V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bz;->e:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->h(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
