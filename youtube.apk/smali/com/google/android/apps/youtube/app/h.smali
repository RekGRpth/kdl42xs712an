.class final Lcom/google/android/apps/youtube/app/h;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/h;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->c(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    const/16 v2, 0x8

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->d(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "No offline videos"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->d(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->e(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->d(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->f(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->f(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/h;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->f(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    goto :goto_0
.end method
