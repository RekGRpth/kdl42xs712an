.class public Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# instance fields
.field private A:Landroid/os/AsyncTask;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/Button;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/ListView;

.field private u:Landroid/widget/TextView;

.field private v:Lcom/google/android/apps/youtube/core/identity/as;

.field private w:Landroid/accounts/Account;

.field private x:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private y:Lcom/google/android/apps/youtube/app/v;

.field private z:Landroid/os/AsyncTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)Landroid/os/AsyncTask;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->A:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)Landroid/os/AsyncTask;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->z:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->e()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->x:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->e()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->i()Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->x:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/v;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private e()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->y:Lcom/google/android/apps/youtube/app/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/v;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->y:Lcom/google/android/apps/youtube/app/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->w:Landroid/accounts/Account;

    const-string v2, "com.google.android.youtube.provider"

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->getPeriodicSyncs(Landroid/accounts/Account;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/v;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->s:Landroid/widget/TextView;

    const-string v1, "Saved resync interval: %d seconds"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->x:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->i()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->finish()V

    :cond_0
    sget v0, Lcom/google/android/youtube/l;->G:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->setContentView(I)V

    sget v0, Lcom/google/android/youtube/j;->cT:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->n:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->o:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->aG:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->s:Landroid/widget/TextView;

    new-instance v0, Lcom/google/android/apps/youtube/app/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/v;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->y:Lcom/google/android/apps/youtube/app/v;

    sget v0, Lcom/google/android/youtube/j;->dj:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->t:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->t:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->y:Lcom/google/android/apps/youtube/app/v;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v0, Lcom/google/android/youtube/j;->ej:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->p:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->p:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/youtube/app/q;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/q;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->bc:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->q:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->q:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/youtube/app/r;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/r;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->dY:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->r:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->r:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/youtube/app/s;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/s;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->cM:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aQ()Lcom/google/android/apps/youtube/core/identity/as;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->v:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aV()Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/ak;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->o:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Signed in as "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->v:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/identity/as;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->w:Landroid/accounts/Account;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->x:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->e()V

    new-instance v0, Lcom/google/android/apps/youtube/app/t;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/t;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->A:Landroid/os/AsyncTask;

    new-instance v0, Lcom/google/android/apps/youtube/app/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/u;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->z:Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineResyncActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    const-string v1, "Offline Resync"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Ljava/lang/CharSequence;)V

    return-void
.end method
