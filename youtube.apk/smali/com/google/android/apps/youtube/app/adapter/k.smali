.class public abstract Lcom/google/android/apps/youtube/app/adapter/k;
.super Lcom/google/android/apps/youtube/app/adapter/g;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/g;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/k;->a:Landroid/content/Context;

    iput p2, p0, Lcom/google/android/apps/youtube/app/adapter/k;->b:I

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/k;->a:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/apps/youtube/app/adapter/k;->b:I

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/google/android/apps/youtube/app/adapter/l;-><init>(Lcom/google/android/apps/youtube/app/adapter/k;Landroid/content/Context;Landroid/view/View;I)V

    return-object v0
.end method

.method protected a(Landroid/graphics/Matrix;Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 0

    return-void
.end method

.method protected abstract a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
.end method
