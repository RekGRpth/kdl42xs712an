.class final Lcom/google/android/apps/youtube/app/d/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/youtube/app/d/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/d/l;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/d/m;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->h(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->NO_MORE_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;->a(Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->b(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->a(Lcom/google/android/apps/youtube/datalib/distiller/model/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/m;->b:Lcom/google/android/apps/youtube/app/d/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->j(Lcom/google/android/apps/youtube/app/d/f;)V

    :cond_0
    return-void
.end method
