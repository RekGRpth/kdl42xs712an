.class final Lcom/google/android/apps/youtube/app/ui/presenter/bq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field final synthetic b:Lcom/google/android/apps/youtube/app/offline/j;

.field final synthetic c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/app/offline/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->a:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->b:Lcom/google/android/apps/youtube/app/offline/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->b(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->a:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->f(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/app/offline/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->b:Lcom/google/android/apps/youtube/app/offline/j;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/app/offline/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/j;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->g(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->g(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->f(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/app/offline/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->g(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/offline/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bq;->c:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->f(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/app/offline/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/f;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
