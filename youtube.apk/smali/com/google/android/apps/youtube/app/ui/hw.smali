.class final Lcom/google/android/apps/youtube/app/ui/hw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/hj;

.field private b:Landroid/view/View;

.field private c:Landroid/app/Dialog;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/hw;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->d:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hw;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/hw;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->c:Landroid/app/Dialog;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->X:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aC:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->bq:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    invoke-static {}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ComplaintReason;->values()[Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ComplaintReason;

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    new-instance v6, Landroid/widget/RadioButton;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/hw;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v7, v7, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-direct {v6, v7}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    iget v7, v5, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ComplaintReason;->stringId:I

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setText(I)V

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/hw;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v7, v7, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/youtube/f;->d:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setTextColor(I)V

    invoke-virtual {v6, v5}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hx;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/hx;-><init>(Lcom/google/android/apps/youtube/app/ui/hw;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hw;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/youtube/p;->bQ:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/ui/aa;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hw;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->bP:I

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->K:I

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->c:Landroid/app/Dialog;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error flagging"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hw;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    sget v1, Lcom/google/android/youtube/p;->gI:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;I)V

    return-void
.end method
