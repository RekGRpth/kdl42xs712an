.class final Lcom/google/android/apps/youtube/app/ui/cb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/bz;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/bz;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/bz;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/cb;-><init>(Lcom/google/android/apps/youtube/app/ui/bz;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bz;->e(Lcom/google/android/apps/youtube/app/ui/bz;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/bz;->d(Lcom/google/android/apps/youtube/app/ui/bz;)Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->cV:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Landroid/util/Pair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bz;->c(Lcom/google/android/apps/youtube/app/ui/bz;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    if-eqz p2, :cond_0

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bz;->e(Lcom/google/android/apps/youtube/app/ui/bz;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/bz;->d(Lcom/google/android/apps/youtube/app/ui/bz;)Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->cV:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/bz;->f(Lcom/google/android/apps/youtube/app/ui/bz;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/ui/bz;->f(Lcom/google/android/apps/youtube/app/ui/bz;)Ljava/util/Set;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/bz;->c(Lcom/google/android/apps/youtube/app/ui/bz;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cb;->a:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bz;->e(Lcom/google/android/apps/youtube/app/ui/bz;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b()V

    goto :goto_0
.end method
