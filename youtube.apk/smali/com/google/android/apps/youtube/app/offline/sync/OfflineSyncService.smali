.class public Lcom/google/android/apps/youtube/app/offline/sync/OfflineSyncService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/apps/youtube/app/offline/sync/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/sync/OfflineSyncService;->a:Lcom/google/android/apps/youtube/app/offline/sync/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const-string v0, "PUDL binding sync adapter"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/offline/sync/OfflineSyncService;->a:Lcom/google/android/apps/youtube/app/offline/sync/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/sync/a;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized onCreate()V
    .locals 3

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/youtube/app/offline/sync/OfflineSyncService;->a:Lcom/google/android/apps/youtube/app/offline/sync/a;

    if-nez v0, :cond_0

    const-string v0, "PUDL creating sync service for the 1st time"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/sync/a;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/sync/OfflineSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/sync/a;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/sync/OfflineSyncService;->a:Lcom/google/android/apps/youtube/app/offline/sync/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
