.class final Lcom/google/android/apps/youtube/app/adapter/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/aa;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/google/android/apps/youtube/app/adapter/ae;

.field private final e:Landroid/text/SpannableStringBuilder;

.field private final f:Landroid/text/style/StyleSpan;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/aa;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/google/android/youtube/j;->aY:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fw:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->c:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/adapter/aa;->a(Lcom/google/android/apps/youtube/app/adapter/aa;)Lcom/google/android/apps/youtube/app/adapter/ai;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->b:Landroid/view/View;

    invoke-interface {v0, v1, p3}, Lcom/google/android/apps/youtube/app/adapter/ai;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->d:Lcom/google/android/apps/youtube/app/adapter/ae;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->e:Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->f:Landroid/text/style/StyleSpan;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/adapter/aa;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/ac;-><init>(Lcom/google/android/apps/youtube/app/adapter/aa;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 7

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->subject:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/youtube/app/adapter/ab;->a:[I

    iget-object v4, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    const-string v3, "%1$s"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    if-eqz v0, :cond_0

    const-string v4, "%1$s"

    invoke-virtual {v1, v4, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->e:Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->f:Landroid/text/style/StyleSpan;

    const/16 v5, 0x21

    invoke-virtual {v1, v4, v3, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->d:Lcom/google/android/apps/youtube/app/adapter/ae;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/ae;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/g;->P:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->b:Landroid/view/View;

    return-object v0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bD:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bE:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bJ:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bL:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bI:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bH:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bM:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bG:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->bF:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->a:Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/aa;->b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/youtube/p;->bK:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ac;->e:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
