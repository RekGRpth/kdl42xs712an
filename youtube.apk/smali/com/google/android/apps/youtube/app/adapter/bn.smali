.class final Lcom/google/android/apps/youtube/app/adapter/bn;
.super Lcom/google/android/apps/youtube/app/adapter/bk;
.source "SourceFile"


# instance fields
.field private final f:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/bk;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bn;->c:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->z:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bn;->f:Landroid/widget/TextView;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/bn;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;
    .locals 9

    const/4 v0, 0x1

    const/4 v7, 0x2

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/bn;->b:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v7, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/bn;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/utils/l;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/bn;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setLines(I)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/bn;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/bn;->b:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/youtube/o;->b:I

    iget-wide v5, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    long-to-int v5, v5

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    aput-object v7, v6, v1

    iget-wide v7, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v2, v3, :cond_2

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v2, v3, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/bn;->c:Landroid/view/View;

    sget v3, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_4

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/bk;->a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/bn;->a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
