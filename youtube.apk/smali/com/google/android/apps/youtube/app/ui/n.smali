.class public final Lcom/google/android/apps/youtube/app/ui/n;
.super Lcom/google/android/apps/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/a/c;

.field private final c:Lcom/google/android/apps/youtube/app/adapter/br;

.field private final d:Lcom/google/android/apps/youtube/app/YouTubeApplication;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/a/c;Lcom/google/android/apps/youtube/core/a/l;Lcom/google/android/apps/youtube/app/adapter/br;Landroid/content/res/Resources;Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/a/l;-><init>([Lcom/google/android/apps/youtube/core/a/e;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->d:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    const-string v0, "listAdapterOutline cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/a/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->b:Lcom/google/android/apps/youtube/core/a/c;

    const-string v0, "loadingOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/br;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->c:Lcom/google/android/apps/youtube/app/adapter/br;

    sget v0, Lcom/google/android/youtube/k;->d:I

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sget v1, Lcom/google/android/youtube/k;->c:I

    invoke-virtual {p6, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/2addr v0, v1

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/n;->d:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v1

    invoke-static {p1, p0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/o;->a:[I

    invoke-virtual {p7}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/client/bc;->e(ILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/n;->d:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->s()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(ILjava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_2
    sget-object v3, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_VIEWED:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-interface {v1, v3, v0, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_3
    sget-object v3, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_SUBSCRIBED:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-interface {v1, v3, v0, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_4
    sget-object v3, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;->NOTEWORTHY:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-interface {v1, v3, v0, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->b:Lcom/google/android/apps/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/c;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/u;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/u;->a(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->b:Lcom/google/android/apps/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/c;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/u;->a(Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->b:Lcom/google/android/apps/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/c;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/u;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/u;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->c:Lcom/google/android/apps/youtube/app/adapter/br;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/br;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->b:Lcom/google/android/apps/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/c;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/af;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/af;->b(Ljava/lang/Iterable;)V

    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/n;->b:Lcom/google/android/apps/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/c;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/u;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/u;->b(Landroid/net/Uri;)V

    return-void
.end method
