.class final enum Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

.field public static final enum DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

.field public static final enum OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

.field public static final enum RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    const-string v1, "DISPLACE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    const-string v1, "RESIZE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    const-string v1, "OCCLUDE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    return-object v0
.end method
