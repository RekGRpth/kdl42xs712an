.class public Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

.field private Z:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->Z:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->Y:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->Z:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->Z:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->Y:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->Y:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->Y:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "upload"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->Y:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/au;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/au;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/ui/aa;

    invoke-direct {v2, v0}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/google/android/youtube/p;->aO:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040013    # android.R.string.yes

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040009    # android.R.string.no

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
