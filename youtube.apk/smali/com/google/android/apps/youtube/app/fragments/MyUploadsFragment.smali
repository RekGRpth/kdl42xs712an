.class public Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/ui/hd;

.field private Z:Lcom/google/android/apps/youtube/app/adapter/ba;

.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private aa:Lcom/google/android/apps/youtube/app/ui/et;

.field private ab:Lcom/google/android/apps/youtube/app/ui/v;

.field private ac:Lcom/google/android/apps/youtube/app/ui/v;

.field private ad:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;

.field private ae:Lcom/google/android/apps/youtube/app/remote/an;

.field private af:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private b:Lcom/google/android/apps/youtube/core/async/af;

.field private d:Lcom/google/android/apps/youtube/core/client/bc;

.field private e:Lcom/google/android/apps/youtube/core/client/bj;

.field private f:Lcom/google/android/apps/youtube/core/identity/ak;

.field private g:Lcom/google/android/apps/youtube/core/identity/l;

.field private h:Lcom/google/android/apps/youtube/core/aw;

.field private i:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->i:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/k;->o:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 5

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/av;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/av;-><init>(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->editUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v4, Lcom/google/android/apps/youtube/app/fragments/at;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/youtube/app/fragments/at;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;Lcom/google/android/apps/youtube/app/fragments/av;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/core/client/bc;->f(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)Lcom/google/android/apps/youtube/app/ui/hd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/hd;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)Lcom/google/android/apps/youtube/app/adapter/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Z:Lcom/google/android/apps/youtube/app/adapter/ba;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    const/4 v4, 0x0

    sget v0, Lcom/google/android/youtube/l;->aq:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/v;

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/aq;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/aq;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/v;

    sget v2, Lcom/google/android/youtube/p;->bb:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/ar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/ar;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/v;

    sget v2, Lcom/google/android/youtube/p;->aN:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ac:Lcom/google/android/apps/youtube/app/ui/v;

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/as;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/as;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ac:Lcom/google/android/apps/youtube/app/ui/v;

    sget v2, Lcom/google/android/youtube/p;->L:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->e:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ac:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Z:Lcom/google/android/apps/youtube/app/adapter/ba;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Z:Lcom/google/android/apps/youtube/app/adapter/ba;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->b(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/et;

    sget v0, Lcom/google/android/youtube/j;->fR:I

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    sget v0, Lcom/google/android/youtube/l;->n:I

    invoke-virtual {p1, v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    sget v0, Lcom/google/android/youtube/l;->m:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->b(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->f:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Z:Lcom/google/android/apps/youtube/app/adapter/ba;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->b:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/youtube/app/ui/hd;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/app/adapter/ba;Lcom/google/android/apps/youtube/app/ui/et;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/hd;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ae:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    sget-object v4, Lcom/google/android/apps/youtube/core/client/WatchFeature;->MY_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->af:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/hd;

    const-string v1, "uploads_helper"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hd;->a(Landroid/os/Bundle;)V

    :cond_0
    return-object v11
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    sget v0, Lcom/google/android/youtube/p;->ah:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aV()Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->f:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->g:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->e:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->m()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->i:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ae:Lcom/google/android/apps/youtube/app/remote/an;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M()Lcom/google/android/apps/youtube/app/compat/o;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/m;->f:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/o;->a(ILcom/google/android/apps/youtube/app/compat/j;)V

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_upload"

    return-object v0
.end method

.method public final d()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->L()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Landroid/support/v4/app/l;

    move-result-object v0

    const-string v1, "DeleteUploadDialogFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->ad:Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;->a(Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment$DeleteUploadDialogFragment;Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/hd;

    if-eqz v0, :cond_0

    const-string v0, "uploads_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/hd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/hd;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->af:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->L()V

    return-void
.end method

.method public final r()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->g:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/hd;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->g()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hd;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->af:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    goto :goto_0
.end method

.method public final s()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->s()V

    return-void
.end method
