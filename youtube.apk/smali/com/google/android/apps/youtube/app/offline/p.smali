.class public final Lcom/google/android/apps/youtube/app/offline/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

.field private static final b:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;


# instance fields
.field private final c:Landroid/content/SharedPreferences;

.field private final d:Ljava/util/List;

.field private final e:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->SD:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/p;->a:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    sget-object v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->SD:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/p;->b:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/au;Landroid/content/SharedPreferences;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/p;->c:Landroid/content/SharedPreferences;

    invoke-static {p1, p2}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/au;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/p;->a(Z)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/offline/p;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/offline/p;->b:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/p;->e:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/offline/p;->a:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    goto :goto_0
.end method

.method private static a(Z)Ljava/util/List;
    .locals 7

    invoke-static {}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->values()[Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    if-nez p0, :cond_0

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I

    move-result v5

    const/16 v6, 0x2d0

    if-ge v5, v6, :cond_1

    :cond_0
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/p;->c:Landroid/content/SharedPreferences;

    const-string v1, "offline_quality"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-ne v3, v1, :cond_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/p;->d:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/p;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "offline_quality"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/offline/p;->b(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v2

    if-eqz p1, :cond_2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->b()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getFormatType()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/p;->b()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/p;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/p;->e:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/p;->b(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/p;->c:Landroid/content/SharedPreferences;

    const-string v1, "offline_policy"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
