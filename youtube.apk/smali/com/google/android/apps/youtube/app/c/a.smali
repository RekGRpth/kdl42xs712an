.class public final Lcom/google/android/apps/youtube/app/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/honeycomb/phone/ao;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/innertube/bc;

.field private final b:Lcom/google/android/apps/youtube/core/identity/l;

.field private final c:Lcom/google/android/apps/youtube/core/identity/as;

.field private final d:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/android/apps/youtube/app/c/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->d:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->a:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/as;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->c:Lcom/google/android/apps/youtube/core/identity/as;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/c/a;Lcom/google/android/apps/youtube/datalib/innertube/model/ba;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->g:Lcom/google/android/apps/youtube/app/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->g:Lcom/google/android/apps/youtube/app/c/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/c/a;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/app/c/d;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ba;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/c/a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/c/a;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/c/a;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/c/a;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->c:Lcom/google/android/apps/youtube/core/identity/as;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/c/a;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/as;->a(Lcom/google/android/apps/youtube/core/identity/l;)Landroid/accounts/Account;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/c/a;->d:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v1, v2}, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->setBuyerAccount(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/ia/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->setEnvironment(I)Lcom/google/android/gms/wallet/ia/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->setJwt(Ljava/lang/String;)Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->setTheme(I)Lcom/google/android/gms/wallet/ia/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/c/a;->d:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    const/16 v2, 0x38a

    invoke-virtual {v1, v0, v2, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a(Landroid/content/Intent;ILcom/google/android/apps/youtube/app/honeycomb/phone/ao;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->g:Lcom/google/android/apps/youtube/app/c/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->g:Lcom/google/android/apps/youtube/app/c/d;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/c/d;->a(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/lq;Ljava/lang/String;)V
    .locals 3

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/c/a;->e:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->f:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/lq;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/lq;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->a:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/bc;->a()Lcom/google/android/apps/youtube/datalib/innertube/bf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/bf;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/bf;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/bf;->a:[B

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/bf;->a([B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/c/a;->a:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    new-instance v2, Lcom/google/android/apps/youtube/app/c/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/c/b;-><init>(Lcom/google/android/apps/youtube/app/c/a;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/bc;->a(Lcom/google/android/apps/youtube/datalib/innertube/bf;Lcom/google/android/apps/youtube/datalib/a/l;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/c/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/c/a;->g:Lcom/google/android/apps/youtube/app/c/d;

    return-void
.end method

.method public final a(IILandroid/content/Intent;)Z
    .locals 3

    const/16 v0, 0x38a

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/c/a;->a(Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    const-string v0, "com.google.android.libraries.inapp.EXTRA_RESPONSE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "orderId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/c/a;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/c/a;->a:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/bc;->b()Lcom/google/android/apps/youtube/datalib/innertube/be;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/be;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/be;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/be;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/be;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/be;->a:[B

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/be;->a([B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/c/a;->a:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    new-instance v2, Lcom/google/android/apps/youtube/app/c/c;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/c/c;-><init>(Lcom/google/android/apps/youtube/app/c/a;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/bc;->a(Lcom/google/android/apps/youtube/datalib/innertube/be;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->d:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Ljava/lang/Error;

    sget v2, Lcom/google/android/youtube/p;->hg:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/c/a;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->g:Lcom/google/android/apps/youtube/app/c/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/c/a;->g:Lcom/google/android/apps/youtube/app/c/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/c/d;->a()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
