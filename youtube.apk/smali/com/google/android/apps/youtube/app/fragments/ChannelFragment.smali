.class public Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;
.super Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/gf;
.implements Lcom/google/android/apps/youtube/app/ui/j;


# instance fields
.field private Y:Lcom/google/android/apps/youtube/core/async/af;

.field private Z:Lcom/google/android/apps/youtube/core/Analytics;

.field private aa:Lcom/google/android/apps/youtube/app/am;

.field private ab:Lcom/google/android/apps/youtube/core/identity/ak;

.field private ac:Lcom/google/android/apps/youtube/app/fragments/q;

.field private ad:Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;

.field private ae:Z

.field private af:Z

.field private ag:Lcom/google/android/apps/youtube/app/ui/et;

.field private ah:Lcom/google/android/apps/youtube/app/ui/et;

.field private ai:Lcom/google/android/apps/youtube/app/ui/et;

.field private aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

.field private ak:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

.field private al:Lcom/google/android/apps/youtube/app/ui/ie;

.field private am:Lcom/google/android/apps/youtube/app/ui/do;

.field private an:Landroid/view/View;

.field private ao:Lcom/google/android/apps/youtube/app/ui/g;

.field private ap:Lcom/google/android/apps/youtube/app/ui/f;

.field private aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

.field private ar:Lcom/google/android/apps/youtube/app/ui/gj;

.field private as:Z

.field private at:Lcom/google/android/apps/youtube/app/remote/an;

.field private au:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private av:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private aw:Landroid/os/Bundle;

.field private ax:Lcom/google/android/apps/youtube/app/remote/ax;

.field private b:Ljava/lang/String;

.field private d:Landroid/content/res/Resources;

.field private e:Lcom/google/android/apps/youtube/app/ax;

.field private f:Lcom/google/android/apps/youtube/core/client/bj;

.field private g:Lcom/google/android/apps/youtube/core/aw;

.field private h:Lcom/google/android/apps/youtube/core/client/bc;

.field private i:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;-><init>()V

    return-void
.end method

.method private P()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/s;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/youtube/app/fragments/s;-><init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;B)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->h(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method private Q()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadsUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ie;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/m;-><init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Lcom/google/android/apps/youtube/core/ui/i;)V

    return-void
.end method

.method private R()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/identity/UserProfile;->playlistsUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->f(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/do;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/n;-><init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/do;->a(Lcom/google/android/apps/youtube/core/ui/i;)V

    return-void
.end method

.method private S()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/identity/UserProfile;->activityUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/f;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/p;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/p;-><init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/f;->a(Lcom/google/android/apps/youtube/core/ui/i;)V

    return-void
.end method

.method private T()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/k;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ag:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ag:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ah:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ah:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ai:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ai:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_2
    return-void
.end method

.method private U()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ae:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->af:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->N()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Z:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;Lcom/google/android/apps/youtube/core/identity/UserProfile;)Lcom/google/android/apps/youtube/core/identity/UserProfile;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ak:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;Lcom/google/android/apps/youtube/core/ui/PagedView$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->a(Lcom/google/android/apps/youtube/core/ui/PagedView$State;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ae:Z

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ae:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ao:Lcom/google/android/apps/youtube/app/ui/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ao:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/g;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->U()V

    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/g;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->d()Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->c()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/g;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;Z)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/ui/PagedView$State;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->af:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->LOADING:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->af:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->U()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aa:Lcom/google/android/apps/youtube/app/am;

    return-object v0
.end method

.method private b(Lcom/google/android/apps/youtube/app/ui/g;)V
    .locals 4

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/e;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->d:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/i;->a:I

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/g;->a(ZF)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/identity/UserProfile;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/ie;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Q()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/do;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->R()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->S()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->an:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ao:Lcom/google/android/apps/youtube/app/ui/g;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/fragments/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/q;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ak:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/core/ui/PagedListView;I)Lcom/google/android/apps/youtube/core/a/a;
    .locals 14

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->cU:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setEmptyText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v13

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {v0, v1, v13}, Lcom/google/android/apps/youtube/app/adapter/ag;->b(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v12

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v12}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ag:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->T()V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ag:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->i:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aa:Lcom/google/android/apps/youtube/app/am;

    const/4 v8, 0x0

    sget-object v9, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Z:Lcom/google/android/apps/youtube/core/Analytics;

    sget-object v11, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    move-object v2, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/app/ui/ie;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aw:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aw:Landroid/os/Bundle;

    const-string v2, "uploads_helper"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Q()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->at:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ag:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    sget-object v4, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Z:Lcom/google/android/apps/youtube/core/Analytics;

    move-object v1, v13

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->au:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->au:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c()V

    move-object v0, v12

    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ie;->f()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->cR:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setEmptyText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Z)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v7}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ah:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->T()V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/do;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ah:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Y:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aa:Lcom/google/android/apps/youtube/app/am;

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/ui/do;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aw:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aw:Landroid/os/Bundle;

    const-string v2, "playlists_helper"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/do;->a(Landroid/os/Bundle;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->R()V

    :goto_2
    move-object v0, v7

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/do;->f()V

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->cN:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setEmptyText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->b(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {v0, v1, v2, v9}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->Z:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/g;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aa:Lcom/google/android/apps/youtube/app/am;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/g;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/g;->a(Lcom/google/android/apps/youtube/app/ui/j;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->b(Lcom/google/android/apps/youtube/app/ui/g;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/g;->a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ae:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/g;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;)V

    :cond_4
    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->an:Landroid/view/View;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/g;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ao:Lcom/google/android/apps/youtube/app/ui/g;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->an:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v8}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ai:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->T()V

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/o;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ai:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    const/4 v7, 0x1

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/fragments/o;-><init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Z)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aw:Landroid/os/Bundle;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aw:Landroid/os/Bundle;

    const-string v2, "events_helper"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/f;->a(Landroid/os/Bundle;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->S()V

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->at:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ai:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Z:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {v0, v9, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->av:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->av:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c()V

    move-object v0, v8

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/f;->f()V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ak:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ak:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;->title:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    const/4 v10, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "channel_username"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->d:Landroid/content/res/Resources;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->l()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->i:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->p()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Y:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Z:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aa:Lcom/google/android/apps/youtube/app/am;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aV()Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ab:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->at:Lcom/google/android/apps/youtube/app/remote/an;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ax;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->at:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aa:Lcom/google/android/apps/youtube/app/am;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/aw;->k()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/remote/ax;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/identity/l;Z)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ax:Lcom/google/android/apps/youtube/app/remote/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ax:Lcom/google/android/apps/youtube/app/remote/ax;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYLISTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->Z:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ab:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g:Lcom/google/android/apps/youtube/core/aw;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v8

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/ui/gf;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    const-string v1, "ChannelLayer"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ar:Lcom/google/android/apps/youtube/app/ui/gj;

    iput-boolean v10, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->as:Z

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/q;

    invoke-direct {v0, p0, v10}, Lcom/google/android/apps/youtube/app/fragments/q;-><init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/q;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;

    iput-boolean v10, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ae:Z

    iput-boolean v10, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->af:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aw:Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->P()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->as:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->as:Z

    move v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ao:Lcom/google/android/apps/youtube/app/ui/g;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ao:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->a(Lcom/google/android/apps/youtube/app/ui/g;)V

    :cond_2
    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v1, :cond_4

    :cond_3
    :goto_0
    return-void

    :cond_4
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ar:Lcom/google/android/apps/youtube/app/ui/gj;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ar:Lcom/google/android/apps/youtube/app/ui/gj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gj;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ar:Lcom/google/android/apps/youtube/app/ui/gj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gj;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method protected final b(I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->b(I)V

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ie;->h()Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->a(Lcom/google/android/apps/youtube/core/ui/PagedView$State;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/do;->h()Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->a(Lcom/google/android/apps/youtube/core/ui/PagedView$State;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/f;->h()Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->a(Lcom/google/android/apps/youtube/core/ui/PagedView$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ao:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->a(Lcom/google/android/apps/youtube/app/ui/g;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final c(I)Ljava/lang/String;
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->dD:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->dC:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->dB:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->T()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aj:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a()V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ae:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->M()V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->P()V

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz v0, :cond_0

    const-string v0, "uploads_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->al:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ie;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    if-eqz v0, :cond_1

    const-string v0, "playlists_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->am:Lcom/google/android/apps/youtube/app/ui/do;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/do;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    if-eqz v0, :cond_2

    const-string v0, "events_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ap:Lcom/google/android/apps/youtube/app/ui/f;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/f;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aw:Landroid/os/Bundle;

    return-void
.end method

.method public final i_()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->as:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->aq:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b()V

    return-void
.end method

.method protected final j_()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->T()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->ao:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->b(Lcom/google/android/apps/youtube/app/ui/g;)V

    return-void
.end method

.method public final r()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->au:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->au:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->av:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->av:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    :cond_1
    return-void
.end method

.method public final t()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->au:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->au:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->av:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->av:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    :cond_1
    return-void
.end method
