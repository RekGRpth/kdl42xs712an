.class public final Lcom/google/android/apps/youtube/app/ui/presenter/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/j;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/r;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/r;->b:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/android/apps/youtube/uilib/a/g;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/r;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/r;->b:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter;-><init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method
