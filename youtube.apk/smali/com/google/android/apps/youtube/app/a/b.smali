.class public final Lcom/google/android/apps/youtube/app/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/a/a;
.implements Lcom/google/android/apps/youtube/core/client/AdsClient;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/q;

.field private final b:Lcom/google/android/apps/youtube/core/client/h;

.field private final c:Lcom/google/android/apps/youtube/core/identity/l;

.field private final d:Lcom/google/android/apps/youtube/core/offline/store/r;

.field private final e:Lcom/google/android/apps/youtube/common/e/b;

.field private final f:J

.field private final g:I

.field private final h:J

.field private final i:J

.field private final j:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/q;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/r;Lcom/google/android/apps/youtube/core/client/h;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/au;)V
    .locals 4

    const-wide/16 v2, 0x3e8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->b:Lcom/google/android/apps/youtube/core/client/h;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->d:Lcom/google/android/apps/youtube/core/offline/store/r;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p6}, Lcom/google/android/apps/youtube/core/au;->K()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/a/b;->f:J

    invoke-interface {p6}, Lcom/google/android/apps/youtube/core/au;->L()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/a/b;->g:I

    invoke-interface {p6}, Lcom/google/android/apps/youtube/core/au;->M()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/a/b;->h:J

    invoke-interface {p6}, Lcom/google/android/apps/youtube/core/au;->N()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/a/b;->i:J

    invoke-interface {p6}, Lcom/google/android/apps/youtube/core/au;->O()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/a/b;->j:J

    return-void
.end method

.method private a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->d:Lcom/google/android/apps/youtube/core/offline/store/r;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/r;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/a/b;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/a/b;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 8

    const/4 v6, -0x1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    const-string v2, ""

    iget-wide v3, p0, Lcom/google/android/apps/youtube/app/a/b;->h:J

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/q;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;JLjava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v0

    if-eq v0, v6, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    move-object v7, v0

    move v0, v2

    move-object v2, v7

    :goto_1
    if-ne v0, v6, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    move-object v2, v0

    move v0, v3

    goto :goto_1

    :cond_3
    if-ne v0, v6, :cond_4

    iget v0, p0, Lcom/google/android/apps/youtube/app/a/b;->g:I

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(I)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 11

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/a/b;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->b:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->i()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isForOffline()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/a/b;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/q;->a()J

    move-result-wide v2

    const-wide/16 v7, 0x0

    cmp-long v0, v2, v7

    if-lez v0, :cond_2

    iget-wide v2, p0, Lcom/google/android/apps/youtube/app/a/b;->f:J

    const-wide/16 v7, 0x0

    cmp-long v0, v2, v7

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/q;->a()J

    move-result-wide v2

    iget-wide v7, p0, Lcom/google/android/apps/youtube/app/a/b;->f:J

    add-long/2addr v2, v7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v7

    cmp-long v0, v2, v7

    if-lez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_INSTREAM_FREQUENCY_CAP:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->b:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->k()Lcom/google/android/apps/youtube/core/client/cf;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->b:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->k()Lcom/google/android/apps/youtube/core/client/cf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/cf;->b()J

    move-result-wide v7

    const-wide/16 v2, -0x1

    cmp-long v0, v7, v2

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-wide v2, p0, Lcom/google/android/apps/youtube/app/a/b;->i:J

    :goto_1
    const-wide/16 v9, 0x0

    cmp-long v0, v2, v9

    if-lez v0, :cond_3

    cmp-long v0, v7, v2

    if-lez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_INACTIVE_USER:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdBreakId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v2, v3}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v2

    if-lt v0, v2, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_FREQUENCY_CAP:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->hasExpired(Lcom/google/android/apps/youtube/common/e/b;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_EXPIRED:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->p(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-eq v0, v2, :cond_6

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->REASON_CLIENT_OFFLINE_AD_ASSET_NOT_READY:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v1

    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/a/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v5

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/a/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v7}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v7

    sub-long/2addr v5, v7

    add-long/2addr v2, v5

    invoke-interface {v4, v1, v2, v3}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->b(Ljava/lang/String;J)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/a/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getFormatStreamProto()Lcom/google/a/a/a/a/fj;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/a/a/a/a/fj;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getFormatStreamProto()Lcom/google/a/a/a/a/fj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/a/a/a/a/fj;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    iget-wide v2, p0, Lcom/google/android/apps/youtube/app/a/b;->j:J

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->o(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_2

    :cond_b
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v1

    :goto_4
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExclusionReasonPingUris()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_4

    :cond_c
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto/16 :goto_3

    :cond_d
    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/client/q;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;)Ljava/util/List;
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/a/b;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/a/b;->b:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->i()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->isForOffline()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1, v3}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->l(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->d(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/q;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Ljava/util/List;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/converter/http/b;->a(Z)Ljava/util/Map;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/youtube/app/a/b;->h:J

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/q;->a(Ljava/lang/String;Ljava/util/Map;J)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/q;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/a/b;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isOfflineShouldCountPlayback()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdBreakId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->n(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/q;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;)Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/b;->a:Lcom/google/android/apps/youtube/core/client/q;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/q;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
