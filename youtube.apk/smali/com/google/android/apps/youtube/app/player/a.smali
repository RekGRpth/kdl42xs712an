.class public final Lcom/google/android/apps/youtube/app/player/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/h;


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:Lcom/google/android/apps/youtube/app/aw;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/app/aw;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/player/a;->a:Landroid/content/SharedPreferences;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/player/a;->b:Lcom/google/android/apps/youtube/app/aw;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/player/a;->a:Landroid/content/SharedPreferences;

    const-string v1, "show_exo_player_debug_messages"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/player/a;->b:Lcom/google/android/apps/youtube/app/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/aw;->u()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/player/a;->b:Lcom/google/android/apps/youtube/app/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/aw;->v()I

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/player/a;->b:Lcom/google/android/apps/youtube/app/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/aw;->w()I

    move-result v0

    return v0
.end method
