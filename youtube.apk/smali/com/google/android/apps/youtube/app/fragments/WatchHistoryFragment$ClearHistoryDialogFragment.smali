.class public Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# instance fields
.field private Y:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;->Y:Ljava/lang/ref/WeakReference;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;->Y:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;->Y:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;->Y:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment;->d(Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/cd;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/cd;-><init>(Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment$ClearHistoryDialogFragment;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/ui/aa;

    invoke-direct {v2, v0}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/google/android/youtube/p;->hh:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040013    # android.R.string.yes

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040009    # android.R.string.no

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
