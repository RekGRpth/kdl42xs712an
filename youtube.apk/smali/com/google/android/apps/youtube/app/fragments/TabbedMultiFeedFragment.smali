.class public abstract Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private Y:Landroid/view/animation/Animation;

.field private Z:J

.field protected a:Z

.field private aa:[I

.field private b:Landroid/widget/FrameLayout;

.field private d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

.field private e:[Lcom/google/android/apps/youtube/core/a/a;

.field private f:[Lcom/google/android/apps/youtube/core/ui/PagedListView;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d(I)V

    return-void
.end method

.method private d(I)V
    .locals 3

    add-int/lit8 v0, p1, -0x1

    :goto_0
    add-int/lit8 v1, p1, 0x1

    if-gt v0, v1, :cond_2

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->j_()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->e:[Lcom/google/android/apps/youtube/core/a/a;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->f:[Lcom/google/android/apps/youtube/core/ui/PagedListView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->e:[Lcom/google/android/apps/youtube/core/a/a;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->a(Lcom/google/android/apps/youtube/core/ui/PagedListView;I)Lcom/google/android/apps/youtube/core/a/a;

    move-result-object v1

    aput-object v1, v2, v0

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a/a;->notifyDataSetChanged()V

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method protected final L()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->b:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method protected final M()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->i:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setVisibility(I)V

    return-void
.end method

.method protected final N()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->i:Z

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->i:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->h:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->Y:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->Y:Landroid/view/animation/Animation;

    iget-wide v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->Z:J

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->Y:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->b(I)V

    :cond_0
    return-void
.end method

.method protected final O()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b()I

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->j_()I

    move-result v2

    new-array v0, v2, [Lcom/google/android/apps/youtube/core/a/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->e:[Lcom/google/android/apps/youtube/core/a/a;

    new-array v0, v2, [Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->f:[Lcom/google/android/apps/youtube/core/ui/PagedListView;

    sget v0, Lcom/google/android/youtube/l;->bn:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->b:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->b:Landroid/widget/FrameLayout;

    sget v3, Lcom/google/android/youtube/j;->ft:I

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/bt;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/bt;-><init>(Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setOnTabSelectedListener(Lcom/google/android/apps/youtube/app/ui/go;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    new-instance v4, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v6, Lcom/google/android/youtube/f;->m:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    sget v7, Lcom/google/android/youtube/l;->ah:I

    sget v8, Lcom/google/android/youtube/p;->cV:I

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/apps/youtube/core/ui/PagedListView;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->aa:[I

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setListViewId(I)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->c(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Ljava/lang/CharSequence;Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->f:[Lcom/google/android/apps/youtube/core/ui/PagedListView;

    aput-object v4, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->b:Landroid/widget/FrameLayout;

    sget v2, Lcom/google/android/youtube/j;->co:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->g:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->cp:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->g:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->i:Z

    if-eqz p3, :cond_3

    const-string v0, "selected_feed_index"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    move v2, v0

    :goto_1
    if-ltz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->j_()I

    move-result v0

    if-ge v2, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    const-string v1, "Feed index out of range."

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(I)V

    if-eq v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->i:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->b(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x10a0001    # android.R.anim.fade_out

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->Y:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->Y:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0001    # android.R.integer.config_mediumAnimTime

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->Z:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->b:Landroid/widget/FrameLayout;

    return-object v0

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method protected abstract a(Lcom/google/android/apps/youtube/core/ui/PagedListView;I)Lcom/google/android/apps/youtube/core/a/a;
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->j_()I

    move-result v2

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->aa:[I

    if-eqz p1, :cond_1

    const-string v0, "paged_list_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v3

    if-eqz v3, :cond_1

    array-length v0, v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->aa:[I

    array-length v4, v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->aa:[I

    invoke-static {v3, v1, v4, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->aa:[I

    invoke-static {}, Lcom/google/android/apps/youtube/core/utils/ah;->a()I

    move-result v3

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected b(I)V
    .locals 0

    return-void
.end method

.method protected abstract c(I)Ljava/lang/String;
.end method

.method public d()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->a:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->e:[Lcom/google/android/apps/youtube/core/a/a;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->e:[Lcom/google/android/apps/youtube/core/a/a;

    aget-object v0, v1, v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a/a;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->a:Z

    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    const-string v0, "selected_feed_index"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "paged_list_ids"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->aa:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    return-void
.end method

.method protected abstract j_()I
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->invalidate()V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
