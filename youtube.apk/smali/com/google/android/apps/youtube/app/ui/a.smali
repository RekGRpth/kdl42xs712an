.class public final Lcom/google/android/apps/youtube/app/ui/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final b:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/a;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/a;->b:Ljava/util/Set;

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 4

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/a;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    const-string v2, "adtracking"

    const v3, 0x323467f

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Z)Lcom/google/android/apps/youtube/datalib/e/f;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/a;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/a/b;->b:Lcom/android/volley/n;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/a;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/app/ui/a;->b(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/a;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/a;->b(Ljava/util/List;)V

    return-void
.end method
