.class public Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/Button;

.field private h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

.field private i:Lcom/google/android/apps/youtube/app/ui/bt;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->NEW:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    sget v0, Lcom/google/android/youtube/l;->al:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Landroid/content/Context;I)V

    sget v0, Lcom/google/android/youtube/l;->ai:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b(Landroid/content/Context;I)V

    sget v0, Lcom/google/android/youtube/l;->ak:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->NEW:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Landroid/content/Context;I)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->NEW:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    sget-object v0, Lcom/google/android/youtube/r;->p:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    sget v2, Lcom/google/android/youtube/l;->al:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Landroid/content/Context;I)V

    sget v1, Lcom/google/android/youtube/l;->ai:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b(Landroid/content/Context;I)V

    const/4 v1, 0x2

    sget v2, Lcom/google/android/youtube/l;->ak:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c(Landroid/content/Context;I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->LOADING:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;)V

    return-void
.end method

.method private a(Landroid/content/Context;I)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;)V
    .locals 6

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    if-eq v0, p1, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->CONTENT:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->getChildCount()I

    move-result v4

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->f:Landroid/view/View;

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->LOADING:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    if-ne p1, v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Landroid/view/View;

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->ERROR:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    if-ne p1, v0, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d:Landroid/view/View;

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->EMPTY:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    if-ne p1, v3, :cond_6

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->i:Lcom/google/android/apps/youtube/app/ui/bt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->i:Lcom/google/android/apps/youtube/app/ui/bt;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/ui/bt;->a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;)V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4
.end method

.method private b(Landroid/content/Context;I)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aS:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private c(Landroid/content/Context;I)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aW:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aX:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->g:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->e:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->LOADING:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;)V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->d:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->EMPTY:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;)V

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->g:Landroid/widget/Button;

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->ERROR:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->CONTENT:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;)V

    return-void
.end method

.method public final c()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;->NEW:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout$State;)V

    return-void
.end method

.method public setOnRetryClickListener(Lcom/google/android/apps/youtube/uilib/innertube/q;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->g:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/bs;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/ui/bs;-><init>(Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;Lcom/google/android/apps/youtube/uilib/innertube/q;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnStateChangeListener(Lcom/google/android/apps/youtube/app/ui/bt;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->i:Lcom/google/android/apps/youtube/app/ui/bt;

    return-void
.end method
