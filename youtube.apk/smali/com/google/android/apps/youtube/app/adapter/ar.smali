.class final Lcom/google/android/apps/youtube/app/adapter/ar;
.super Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/aq;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/adapter/aq;Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/ar;->a:Lcom/google/android/apps/youtube/app/adapter/aq;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->thumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->sdThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic c(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic d(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method
