.class public final Lcom/google/android/apps/youtube/app/offline/sync/a;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/app/offline/sync/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->h()Lcom/google/android/apps/youtube/app/offline/sync/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/a;->a:Lcom/google/android/apps/youtube/app/offline/sync/b;

    return-void
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 1

    const-string v0, "OfflineSyncAdapter.onPerformSync() called!"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/a;->a:Lcom/google/android/apps/youtube/app/offline/sync/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a()V

    return-void
.end method
