.class final Lcom/google/android/apps/youtube/app/fragments/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/s;-><init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->a(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;Lcom/google/android/apps/youtube/core/identity/UserProfile;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->d(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->e(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/ie;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->f(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->g(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/do;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->h(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->i(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/f;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->j(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->k(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->l(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/ui/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/g;->a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->n(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->m(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/fragments/q;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->n(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/identity/UserProfile;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/r;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/s;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/app/fragments/r;-><init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;B)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->i(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
