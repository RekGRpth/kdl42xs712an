.class public final Lcom/google/android/apps/youtube/app/ui/presenter/cq;
.super Lcom/google/android/apps/youtube/app/ui/presenter/b;
.source "SourceFile"


# instance fields
.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/view/View;

.field private final h:Lcom/google/android/apps/youtube/app/ui/presenter/cu;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/presenter/f;Lcom/google/android/apps/youtube/app/ui/presenter/cu;Lcom/google/android/apps/youtube/app/ui/presenter/g;Lcom/google/android/apps/youtube/app/d/r;)V
    .locals 7

    sget v0, Lcom/google/android/youtube/l;->U:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/ui/presenter/b;-><init>(Landroid/view/View;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/presenter/f;Lcom/google/android/apps/youtube/app/ui/presenter/g;Lcom/google/android/apps/youtube/app/d/r;)V

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/presenter/cu;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->h:Lcom/google/android/apps/youtube/app/ui/presenter/cu;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eJ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->cl:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->f:Landroid/widget/TextView;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)Landroid/view/View;
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/b;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->f:Landroid/widget/TextView;

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/presenter/cs;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->n()Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->a:Landroid/view/View;

    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->e:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->au:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->e:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/cr;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/cr;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/cq;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_0
    sget v0, Lcom/google/android/youtube/h;->L:I

    goto :goto_1

    :pswitch_1
    sget v0, Lcom/google/android/youtube/h;->C:I

    goto :goto_1

    :pswitch_2
    sget v0, Lcom/google/android/youtube/h;->F:I

    goto :goto_1

    :pswitch_3
    sget v0, Lcom/google/android/youtube/h;->B:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/cq;)Lcom/google/android/apps/youtube/app/ui/presenter/cu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->h:Lcom/google/android/apps/youtube/app/ui/presenter/cu;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/cq;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
