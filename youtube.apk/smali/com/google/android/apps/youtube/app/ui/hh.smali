.class public final Lcom/google/android/apps/youtube/app/ui/hh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final c:Lcom/google/android/apps/youtube/core/Analytics;

.field private final d:Lcom/google/android/apps/youtube/datalib/innertube/t;

.field private e:Landroid/app/AlertDialog;

.field private f:Landroid/app/AlertDialog;

.field private g:Lcom/google/a/a/a/a/kz;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/datalib/innertube/t;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->c:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/t;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->d:Lcom/google/android/apps/youtube/datalib/innertube/t;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hh;)Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->g:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method private a(Lcom/google/a/a/a/a/ee;)V
    .locals 1

    iget-object v0, p1, Lcom/google/a/a/a/a/ee;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/a/a/a/a/kx;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->f:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hi;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/hi;-><init>(Lcom/google/android/apps/youtube/app/ui/hh;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hh;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/youtube/p;->aR:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/ui/aa;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->ck:I

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->f:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p1, Lcom/google/a/a/a/a/kx;->c:Lcom/google/a/a/a/a/kz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->g:Lcom/google/a/a/a/a/kz;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->d:Lcom/google/android/apps/youtube/datalib/innertube/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/u;

    move-result-object v0

    iget-object v1, p1, Lcom/google/a/a/a/a/kx;->d:[B

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/u;->b([B)Lcom/google/android/apps/youtube/datalib/innertube/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hh;->d:Lcom/google/android/apps/youtube/datalib/innertube/t;

    const-class v2, Lcom/google/a/a/a/a/il;

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/a/m;->a(Ljava/lang/Class;)Lcom/google/android/apps/youtube/datalib/a/l;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/t;->a(Lcom/google/android/apps/youtube/datalib/innertube/u;Lcom/google/android/apps/youtube/datalib/a/l;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->c:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "MusicUpsellDialogShown"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->e:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hh;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->dA:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/ui/aa;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->e:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/hh;)Lcom/google/android/apps/youtube/datalib/d/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hh;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->ds:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/a/a/a/a/ai;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/ai;->c:Lcom/google/a/a/a/a/ee;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/ai;->c:Lcom/google/a/a/a/a/ee;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/ee;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/a/a/a/a/ai;->b:Lcom/google/a/a/a/a/kx;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/a/a/a/a/ai;->b:Lcom/google/a/a/a/a/kx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/kx;)V

    goto :goto_0

    :cond_1
    const-string v0, "background_message_seekrit doesn\'t contain dismissable_dialog_renderer or more_info_dialog_renderer"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/a/a/a/a/lv;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/lv;->c:Lcom/google/a/a/a/a/ee;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/lv;->c:Lcom/google/a/a/a/a/ee;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/ee;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/a/a/a/a/lv;->b:Lcom/google/a/a/a/a/kx;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/a/a/a/a/lv;->b:Lcom/google/a/a/a/a/kx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/kx;)V

    goto :goto_0

    :cond_1
    const-string v0, "offline_message doesn\'t contain dismissable_dialog_renderer or music_upsell_dialog_renderer"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/a/a/a/a/lz;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/lz;->c:Lcom/google/a/a/a/a/ee;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/lz;->c:Lcom/google/a/a/a/a/ee;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/ee;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/a/a/a/a/lz;->b:Lcom/google/a/a/a/a/kx;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/a/a/a/a/lz;->b:Lcom/google/a/a/a/a/kx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/kx;)V

    goto :goto_0

    :cond_1
    const-string v0, "offline_refresh_message doesn\'t contain dismissable_dialog_renderer or music_upsell_dialog_renderer"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/a/a/a/a/mh;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/mh;->c:Lcom/google/a/a/a/a/ee;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/mh;->c:Lcom/google/a/a/a/a/ee;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/ee;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/a/a/a/a/mh;->b:Lcom/google/a/a/a/a/kx;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/a/a/a/a/mh;->b:Lcom/google/a/a/a/a/kx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/kx;)V

    goto :goto_0

    :cond_1
    const-string v0, "offlineability_renderer.info_renderer doesn\'t contain dissmissiable_dialog_renderer or music_upsell_dialog_renderer"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
