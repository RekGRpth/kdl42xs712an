.class public final Lcom/google/android/apps/youtube/app/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;)V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/d/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/d/b;-><init>()V

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/youtube/app/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p3}, Landroid/widget/ImageView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/app/d/c;

    invoke-direct {v1, p0, p3, p4}, Lcom/google/android/apps/youtube/app/d/c;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
