.class final Lcom/google/android/apps/youtube/app/compat/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/compat/j;

.field private final b:Landroid/view/SubMenu;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/compat/k;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/app/compat/j;->a(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    return-void
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v1, p1}, Landroid/view/SubMenu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/compat/j;->a(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/SubMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/compat/j;->a(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/compat/j;->a(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v1, p1}, Landroid/view/SubMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/compat/j;->a(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Landroid/view/SubMenu;->addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v2, p1}, Landroid/view/SubMenu;->addSubMenu(I)Landroid/view/SubMenu;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/compat/k;-><init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v2, p1, p2, p3, p4}, Landroid/view/SubMenu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/compat/k;-><init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v2, p1, p2, p3, p4}, Landroid/view/SubMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/compat/k;-><init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v2, p1}, Landroid/view/SubMenu;->addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/compat/k;-><init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->clear()V

    return-void
.end method

.method public final clearHeader()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->clearHeader()V

    return-void
.end method

.method public final close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->close()V

    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final getItem()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/SubMenu;->isShortcutKey(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final performIdentifierAction(II)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/SubMenu;->performIdentifierAction(II)Z

    move-result v0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/SubMenu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->removeGroup(I)V

    return-void
.end method

.method public final removeItem(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->removeItem(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/compat/j;->a(Lcom/google/android/apps/youtube/app/compat/j;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/SubMenu;->setGroupCheckable(IZZ)V

    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/SubMenu;->setGroupEnabled(IZ)V

    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/SubMenu;->setGroupVisible(IZ)V

    return-void
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderIcon(I)Landroid/view/SubMenu;

    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderTitle(I)Landroid/view/SubMenu;

    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;

    return-object p0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setIcon(I)Landroid/view/SubMenu;

    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    return-object p0
.end method

.method public final setQwertyMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setQwertyMode(Z)V

    return-void
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/k;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->size()I

    move-result v0

    return v0
.end method
