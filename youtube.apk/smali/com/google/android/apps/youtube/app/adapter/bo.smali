.class public final Lcom/google/android/apps/youtube/app/adapter/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;
.implements Lcom/google/android/apps/youtube/app/adapter/j;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/google/android/apps/youtube/app/adapter/h;

.field private c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/view/View;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/adapter/h;Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->a:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->b:Lcom/google/android/apps/youtube/app/adapter/h;

    sget v0, Lcom/google/android/youtube/j;->fM:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->c:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->bt:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->d:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1, p0}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Lcom/google/android/apps/youtube/app/adapter/j;)V

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/bp;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/adapter/bp;-><init>(Lcom/google/android/apps/youtube/app/adapter/bo;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->f:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/bo;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->e:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    const/16 v3, 0x8

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->e:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/h;->h:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->b:Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/h;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v0, v2, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v0, v2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->a:Landroid/view/View;

    return-object v0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->c:Landroid/widget/ImageView;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->a:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->fN:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->c:Landroid/widget/ImageView;

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bo;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
