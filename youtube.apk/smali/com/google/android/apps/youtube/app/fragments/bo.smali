.class final Lcom/google/android/apps/youtube/app/fragments/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/bo;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bo;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/bo;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->d(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bo;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/bo;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->cS:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bo;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bo;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/uilib/innertube/t;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ai;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/t;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V

    goto :goto_0
.end method
