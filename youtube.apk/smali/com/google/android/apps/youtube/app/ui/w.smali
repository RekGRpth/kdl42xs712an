.class final Lcom/google/android/apps/youtube/app/ui/w;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/v;

.field private final b:Ljava/util/ArrayList;

.field private final c:Landroid/util/SparseArray;

.field private final d:Ljava/util/ArrayList;

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/w;->a:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->e:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->b:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->c:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->d:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/w;ILjava/lang/String;ILcom/google/android/apps/youtube/app/ui/ab;)I
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/x;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/w;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/w;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/x;-><init>(Lcom/google/android/apps/youtube/app/ui/w;Ljava/lang/Integer;Ljava/lang/String;ILcom/google/android/apps/youtube/app/ui/ab;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/w;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/w;->notifyDataSetChanged()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->a()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final a(ILcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/x;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/w;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/w;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2, p3}, Lcom/google/android/apps/youtube/app/ui/x;-><init>(Lcom/google/android/apps/youtube/app/ui/w;Ljava/lang/Integer;Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/w;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/w;->notifyDataSetChanged()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->a()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/apps/youtube/app/ui/ab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->e()Lcom/google/android/apps/youtube/app/ui/ab;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/x;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/x;->a(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/w;->notifyDataSetChanged()V

    return-void
.end method

.method public final c(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/x;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/x;->a(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/w;->notifyDataSetChanged()V

    return-void
.end method

.method public final d(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/x;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/w;->notifyDataSetChanged()V

    return-void
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->a()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v2, 0x0

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->a:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/v;->b(Lcom/google/android/apps/youtube/app/ui/v;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->B:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/z;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/app/ui/z;-><init>(Lcom/google/android/apps/youtube/app/ui/w;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/x;

    if-nez v0, :cond_2

    move-object p2, v2

    :cond_0
    :goto_1
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/z;

    move-object v1, v0

    goto :goto_0

    :cond_2
    iget-object v3, v1, Lcom/google/android/apps/youtube/app/ui/z;->a:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, v1, Lcom/google/android/apps/youtube/app/ui/z;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/google/android/apps/youtube/app/ui/z;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    :goto_2
    iget-object v3, v1, Lcom/google/android/apps/youtube/app/ui/z;->b:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->c()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v2, v1, Lcom/google/android/apps/youtube/app/ui/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, v1, Lcom/google/android/apps/youtube/app/ui/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    iget-object v3, v1, Lcom/google/android/apps/youtube/app/ui/z;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/google/android/apps/youtube/app/ui/z;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_5
    iget-object v0, v1, Lcom/google/android/apps/youtube/app/ui/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, v1, Lcom/google/android/apps/youtube/app/ui/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final notifyDataSetChanged()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/x;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/w;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/w;->a:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/v;)V

    return-void
.end method
