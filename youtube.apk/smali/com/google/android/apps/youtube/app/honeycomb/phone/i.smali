.class public final Lcom/google/android/apps/youtube/app/honeycomb/phone/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/honeycomb/ui/f;


# instance fields
.field private a:Lcom/google/android/apps/youtube/app/compat/q;

.field private b:Z

.field private c:Z

.field private final d:Landroid/support/v7/media/s;

.field private final e:Landroid/support/v7/media/u;

.field private final f:Lcom/google/android/apps/youtube/app/honeycomb/phone/j;

.field private g:Landroid/support/v7/app/MediaRouteButton;

.field private h:Landroid/support/v7/app/MediaRouteButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->c:Z

    invoke-static {p1}, Landroid/support/v7/media/u;->a(Landroid/content/Context;)Landroid/support/v7/media/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->e:Landroid/support/v7/media/u;

    invoke-static {}, Lcom/google/android/apps/youtube/app/remote/an;->i()Landroid/support/v7/media/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->d:Landroid/support/v7/media/s;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/j;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/j;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/i;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/j;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a(Landroid/content/Context;)Landroid/support/v7/app/MediaRouteButton;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->h:Landroid/support/v7/app/MediaRouteButton;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->f()V

    return-void
.end method

.method private f()V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a:Lcom/google/android/apps/youtube/app/compat/q;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->e:Landroid/support/v7/media/u;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->d:Landroid/support/v7/media/s;

    invoke-static {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;I)Z

    move-result v0

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->b:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->c:Z

    if-nez v2, :cond_3

    :cond_1
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/app/compat/q;->a(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->h:Landroid/support/v7/app/MediaRouteButton;

    if-eqz v0, :cond_4

    :goto_2
    invoke-virtual {v2, v1}, Landroid/support/v7/app/MediaRouteButton;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    const/16 v1, 0x8

    goto :goto_2
.end method


# virtual methods
.method public final A()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->b:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->f()V

    return-void
.end method

.method public final a(Landroid/content/Context;)Landroid/support/v7/app/MediaRouteButton;
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->aE:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteButton;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->d:Landroid/support/v7/media/s;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->setRouteSelector(Landroid/support/v7/media/s;)V

    return-object v0
.end method

.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->f()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 2

    sget v0, Lcom/google/android/youtube/j;->cr:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->d()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/MediaRouteButton;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->g:Landroid/support/v7/app/MediaRouteButton;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->g:Landroid/support/v7/app/MediaRouteButton;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->d:Landroid/support/v7/media/s;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->setRouteSelector(Landroid/support/v7/media/s;)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->c:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->f()V

    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->e:Landroid/support/v7/media/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->d:Landroid/support/v7/media/s;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/j;

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;I)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->e:Landroid/support/v7/media/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/j;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/v;)V

    return-void
.end method

.method public final d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->g:Landroid/support/v7/app/MediaRouteButton;

    return-object v0
.end method

.method public final e()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->h:Landroid/support/v7/app/MediaRouteButton;

    return-object v0
.end method

.method public final z()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->b:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->f()V

    return-void
.end method
