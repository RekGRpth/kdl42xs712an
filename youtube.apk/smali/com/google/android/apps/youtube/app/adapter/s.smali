.class final Lcom/google/android/apps/youtube/app/adapter/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/ImageButton;

.field private final f:Landroid/widget/ProgressBar;

.field private g:Lcom/google/android/apps/youtube/core/identity/UserProfile;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/s;->b:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->am:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->d:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->an:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->c:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dS:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->f:Landroid/widget/ProgressBar;

    sget v0, Lcom/google/android/youtube/j;->eZ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->e:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->d:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/adapter/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/s;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Landroid/view/View;)V

    return-void
.end method

.method private a()Landroid/view/View;
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->d:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->e:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->e:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/p;->d:I

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v6, v6, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/s;->e:Landroid/widget/ImageButton;

    sget-object v4, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-eq v0, v4, :cond_1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/s;->e:Landroid/widget/ImageButton;

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-ne v0, v1, :cond_2

    const/4 v1, 0x4

    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/s;->e:Landroid/widget/ImageButton;

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-ne v0, v1, :cond_3

    sget v1, Lcom/google/android/youtube/h;->v:I

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/s;->f:Landroid/widget/ProgressBar;

    sget-object v3, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->WORKING:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-ne v0, v3, :cond_4

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->b:Landroid/view/View;

    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    sget v1, Lcom/google/android/youtube/h;->w:I

    goto :goto_2

    :cond_4
    const/16 v2, 0x8

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/s;)Landroid/view/View;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/s;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/s;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/adapter/s;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->e(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Ignoring click due to not authenticated"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->g(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->f(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/r;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-direct {v3, v4, v5, p0, p1}, Lcom/google/android/apps/youtube/app/adapter/r;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/app/adapter/s;Z)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/s;)Lcom/google/android/apps/youtube/core/identity/UserProfile;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0, p2, p0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/app/adapter/s;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/s;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->h(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->v()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->c(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->i(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v2, "ChannelStore"

    invoke-static {v0, v2, v1, v4}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/core/Analytics;Ljava/lang/String;IZ)V

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/t;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/adapter/t;-><init>(Lcom/google/android/apps/youtube/app/adapter/s;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/youtube/p;->gn:I

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/adapter/s;->g:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v5, v5, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013    # android.R.string.yes

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009    # android.R.string.no

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/s;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->i(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v2, "ChannelStore"

    invoke-static {v0, v2, v1, v6}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/core/Analytics;Ljava/lang/String;IZ)V

    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/adapter/s;->a(Z)V

    goto :goto_0
.end method
