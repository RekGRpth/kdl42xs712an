.class final Lcom/google/android/apps/youtube/app/honeycomb/ui/d;
.super Lcom/google/android/apps/youtube/app/honeycomb/ui/a;
.source "SourceFile"


# instance fields
.field private final i:Lcom/google/android/apps/youtube/app/honeycomb/ui/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;Lcom/google/android/apps/youtube/app/compat/o;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;Lcom/google/android/apps/youtube/app/compat/o;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/e;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/d;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->i:Lcom/google/android/apps/youtube/app/honeycomb/ui/e;

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->a:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->a:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->c()Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a(Lcom/google/android/apps/youtube/app/compat/j;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v2, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->i:Lcom/google/android/apps/youtube/app/honeycomb/ui/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->a:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->i:Lcom/google/android/apps/youtube/app/honeycomb/ui/e;

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/app/compat/q;->a(Lcom/google/android/apps/youtube/app/compat/r;)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v2, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->b:Landroid/widget/SearchView;

    const v2, 0x2000003

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setImeOptions(I)V

    :cond_1
    return v0
.end method

.method public final d()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->a:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->a:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;->a:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->b()Z

    goto :goto_0
.end method
