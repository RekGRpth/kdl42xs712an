.class final Lcom/google/android/apps/youtube/app/offline/a/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/offline/store/j;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/offline/a/f;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/offline/a/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/offline/a/f;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/p;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(J)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 7

    const-wide/16 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getOfflineState()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->g()I

    move-result v0

    int-to-long v0, v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/offline/a/f;->i()J

    move-result-wide v2

    cmp-long v4, v0, v5

    if-lez v4, :cond_0

    cmp-long v4, v2, v5

    if-eqz v4, :cond_2

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(J)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->g(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/core/offline/store/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/l;->f(Ljava/lang/String;)V

    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->g(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/core/offline/store/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/l;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->k(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bp()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bd()Lcom/google/android/apps/youtube/core/player/a/l;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bn()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bo()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/a/l;->a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bj()Lcom/google/android/apps/youtube/core/player/a/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bn()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bo()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/core/player/a/a;->a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->h(Lcom/google/android/apps/youtube/app/offline/a/f;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->h(Lcom/google/android/apps/youtube/app/offline/a/f;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->d(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/offline/a/af;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/af;->b(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->g(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/core/offline/store/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/l;->h(Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->g(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/core/offline/store/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/l;->c(Ljava/lang/String;)V

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v1, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->r(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v1, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->b(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->r(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/b;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/exoplayer/upstream/cache/a;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/offline/a/f;->i(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/p;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/offline/a/f;->j(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/offline/b;-><init>([Lcom/google/android/exoplayer/upstream/cache/a;)V

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/b;->a(Ljava/lang/String;)V

    return-void
.end method
