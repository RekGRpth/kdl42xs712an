.class public final Lcom/google/android/apps/youtube/app/ui/ja;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/youtube/app/c/d;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/app/c/a;

.field private final c:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final d:Lcom/google/android/apps/youtube/common/c/a;

.field private final e:Lcom/google/android/apps/youtube/core/aw;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/ListView;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/TextView;

.field private j:Lcom/google/android/apps/youtube/uilib/a/h;

.field private k:Landroid/app/AlertDialog;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/c/a;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->b:Lcom/google/android/apps/youtube/app/c/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->e:Lcom/google/android/apps/youtube/core/aw;

    sget v0, Lcom/google/android/youtube/l;->bQ:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->f:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->cP:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->g:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->f:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->dR:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->f:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aK:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->i:Landroid/widget/TextView;

    invoke-virtual {p2, p0}, Lcom/google/android/apps/youtube/app/c/a;->a(Lcom/google/android/apps/youtube/app/c/d;)V

    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->g:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ja;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->e:Lcom/google/android/apps/youtube/core/aw;

    sget v1, Lcom/google/android/youtube/p;->eJ:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/ba;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ja;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/p;

    invoke-direct {v1, p2}, Lcom/google/android/apps/youtube/app/ui/p;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->b()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ba;->b()Lcom/google/a/a/a/a/kz;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/datalib/d/a;->a(Lcom/google/a/a/a/a/kz;)V

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/bb;Ljava/lang/String;)V
    .locals 4

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ja;->l:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->j:Lcom/google/android/apps/youtube/uilib/a/h;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ja;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ja;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->k:Landroid/app/AlertDialog;

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->j:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->j:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v1, Lcom/google/android/apps/youtube/datalib/innertube/model/al;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/cm;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ja;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/cm;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->g:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ja;->j:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->i:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/bb;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->j:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->j:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/bb;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ja;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->k:Landroid/app/AlertDialog;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/bb;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/ja;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ja;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ja;->h:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ja;->g:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ja;->b:Lcom/google/android/apps/youtube/app/c/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->d()Lcom/google/a/a/a/a/lq;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ja;->l:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/app/c/a;->a(Lcom/google/a/a/a/a/lq;Ljava/lang/String;)V

    return-void
.end method
