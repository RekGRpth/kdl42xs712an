.class public final Lcom/google/android/apps/youtube/app/remote/bb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/ap;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

.field private final b:Lcom/google/android/apps/youtube/app/remote/aq;

.field private final c:Landroid/content/res/Resources;

.field private d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/YouTubeApplication;)V
    .locals 14

    const/4 v13, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->af()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v2

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v3

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v4

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v5

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v6

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aY()Ljava/lang/String;

    move-result-object v7

    const-class v8, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v9, Lcom/google/android/apps/youtube/app/remote/be;

    invoke-direct {v9, p0, v13}, Lcom/google/android/apps/youtube/app/remote/be;-><init>(Lcom/google/android/apps/youtube/app/remote/bb;B)V

    const/4 v10, 0x0

    sget v11, Lcom/google/android/youtube/h;->M:I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/common/network/h;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;ILcom/google/android/apps/youtube/core/player/notification/c;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bb;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/bf;

    invoke-direct {v0, p0, v13}, Lcom/google/android/apps/youtube/app/remote/bf;-><init>(Lcom/google/android/apps/youtube/app/remote/bb;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bb;->b:Lcom/google/android/apps/youtube/app/remote/aq;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bb;->c:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bb;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b()V

    sget v0, Lcom/google/android/exoplayer/e/k;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bb;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/bc;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/remote/bc;-><init>(Lcom/google/android/apps/youtube/app/remote/bb;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bb;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bb;->b:Lcom/google/android/apps/youtube/app/remote/aq;

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bb;->b:Lcom/google/android/apps/youtube/app/remote/aq;

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bb;->b:Lcom/google/android/apps/youtube/app/remote/aq;

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->b:Lcom/google/android/apps/youtube/app/remote/aq;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/app/remote/aq;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bb;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/bb;->c:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/youtube/p;->de:I

    new-array v6, v0, [Ljava/lang/Object;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/bg;->getScreenName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bb;->d:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/bg;->mustShowLockScreenControls()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bb;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Z)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method
