.class final Lcom/google/android/apps/youtube/app/ui/ge;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ge;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ge;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ge;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ERROR:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ge;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->d(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/core/async/Optional;

    invoke-interface {p2}, Lcom/google/android/apps/youtube/core/async/Optional;->get()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ge;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ge;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->d(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ge;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->editUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ge;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ge;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    goto :goto_0
.end method
