.class final Lcom/google/android/apps/youtube/app/aj;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/GuideActivity;)V
    .locals 2

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/aj;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/GuideActivity;

    if-eqz v0, :cond_0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, v0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    goto :goto_0

    :pswitch_1
    iget-object v0, v0, Lcom/google/android/apps/youtube/app/GuideActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    goto :goto_0

    :pswitch_2
    invoke-static {v0}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/GuideActivity;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
