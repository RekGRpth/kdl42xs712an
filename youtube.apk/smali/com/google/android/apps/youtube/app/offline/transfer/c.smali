.class public final Lcom/google/android/apps/youtube/app/offline/transfer/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/common/fromguava/e;

.field private final c:Lcom/google/android/exoplayer/upstream/cache/a;

.field private final d:Ljava/io/File;

.field private final e:Ljava/security/Key;

.field private final f:Lcom/google/android/apps/youtube/common/fromguava/e;

.field private g:Lcom/google/android/apps/youtube/app/offline/transfer/d;

.field private final h:Ljava/lang/Object;

.field private volatile i:Z

.field private volatile j:Z

.field private final k:Lcom/google/android/apps/youtube/common/e/b;

.field private l:J

.field private m:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/common/fromguava/e;Lcom/google/android/exoplayer/upstream/cache/a;Ljava/io/File;Ljava/security/Key;Lcom/google/android/apps/youtube/common/fromguava/e;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->i:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->j:Z

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/fromguava/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->b:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->d:Ljava/io/File;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/Key;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->e:Ljava/security/Key;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/fromguava/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->f:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->k:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->h:Ljava/lang/Object;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->l:J

    return-void
.end method

.method private a(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;J)V
    .locals 10

    const/16 v0, 0x1000

    new-array v2, v0, [B

    invoke-interface {p1, p2}, Lcom/google/android/exoplayer/upstream/i;->a(Lcom/google/android/exoplayer/upstream/j;)J

    :try_start_0
    iget-wide v0, p2, Lcom/google/android/exoplayer/upstream/j;->d:J

    :cond_0
    const/4 v3, 0x0

    const/16 v4, 0x1000

    invoke-interface {p1, v2, v3, v4}, Lcom/google/android/exoplayer/upstream/i;->a([BII)I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    add-long v4, p4, v0

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->l:J

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->k:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v7, 0x1e

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v7, v8, v9}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->m:J

    sub-long/2addr v4, v8

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferTimedOutException;

    const-string v1, "Transfer timed out."

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferTimedOutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Lcom/google/android/exoplayer/upstream/i;->a()V

    throw v0

    :cond_1
    :try_start_1
    iput-wide v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->l:J

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->k:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v6}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->m:J

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->g:Lcom/google/android/apps/youtube/app/offline/transfer/d;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->g:Lcom/google/android/apps/youtube/app/offline/transfer/d;

    invoke-interface {v6, p3, v4, v5}, Lcom/google/android/apps/youtube/app/offline/transfer/d;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;J)V

    :cond_2
    if-lez v3, :cond_3

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->i:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_0

    :cond_3
    invoke-interface {p1}, Lcom/google/android/exoplayer/upstream/i;->a()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->b:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;)Ljava/util/SortedSet;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/upstream/cache/d;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/upstream/cache/a;->b(Lcom/google/android/exoplayer/upstream/cache/d;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/offline/transfer/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->g:Lcom/google/android/apps/youtube/app/offline/transfer/d;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;JJ)V
    .locals 16

    sget-object v9, Lcom/google/android/apps/youtube/app/offline/transfer/c;->a:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getLastModified()J

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v4

    invoke-static {v1, v4, v2, v3}, Lcom/google/android/apps/youtube/core/utils/p;->a(Ljava/lang/String;IJ)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->f:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer/upstream/i;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->b:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/upstream/cache/a;

    if-eqz v2, :cond_b

    new-instance v4, Lcom/google/android/exoplayer/upstream/a/b;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->e:Ljava/security/Key;

    invoke-interface {v1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v1

    new-instance v5, Lcom/google/android/exoplayer/upstream/FileDataSource;

    invoke-direct {v5}, Lcom/google/android/exoplayer/upstream/FileDataSource;-><init>()V

    invoke-direct {v4, v1, v5}, Lcom/google/android/exoplayer/upstream/a/b;-><init>([BLcom/google/android/exoplayer/upstream/i;)V

    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/b;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/upstream/cache/b;-><init>(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/h;ZZ)V

    :goto_0
    new-instance v2, Lcom/google/android/exoplayer/upstream/a/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->e:Ljava/security/Key;

    invoke-interface {v3}, Ljava/security/Key;->getEncoded()[B

    move-result-object v3

    const/16 v4, 0x1000

    new-array v4, v4, [B

    new-instance v5, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    const-wide/32 v7, 0x500000

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;-><init>(Lcom/google/android/exoplayer/upstream/cache/a;J)V

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/exoplayer/upstream/a/a;-><init>([B[BLcom/google/android/exoplayer/upstream/h;)V

    new-instance v13, Lcom/google/android/exoplayer/upstream/q;

    invoke-direct {v13, v1, v2}, Lcom/google/android/exoplayer/upstream/q;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/h;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getUri()Landroid/net/Uri;

    move-result-object v2

    new-instance v14, Ljava/util/LinkedList;

    invoke-direct {v14}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v1, v12}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;)Ljava/util/SortedSet;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v1, v12}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;)Ljava/util/SortedSet;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    new-instance v1, Lcom/google/android/exoplayer/upstream/j;

    const-wide/16 v3, 0x0

    invoke-virtual {v12}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    move-wide v5, v10

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_1
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer/upstream/j;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v4, v3, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    iget-wide v5, v3, Lcom/google/android/exoplayer/upstream/j;->d:J

    invoke-interface {v1, v4, v5, v6}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    move-object/from16 v1, p0

    move-object v2, v13

    move-object/from16 v4, p1

    move-wide/from16 v5, p2

    :try_start_2
    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/youtube/app/offline/transfer/c;->a(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;J)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v1, v7}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v2, 0x0

    :try_start_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->i:Z

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->j:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_3

    :try_start_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v4, v3, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    iget-wide v5, v3, Lcom/google/android/exoplayer/upstream/j;->d:J

    invoke-interface {v1, v4, v5, v6}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v1

    iget-boolean v3, v1, Lcom/google/android/exoplayer/upstream/cache/d;->d:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v3, v1}, Lcom/google/android/exoplayer/upstream/cache/a;->b(Lcom/google/android/exoplayer/upstream/cache/d;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_3
    :goto_2
    :try_start_5
    monitor-exit v9

    :goto_3
    return-void

    :cond_4
    new-instance v15, Ljava/util/TreeSet;

    invoke-direct {v15}, Ljava/util/TreeSet;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v1, v12}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;)Ljava/util/SortedSet;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    const-wide/16 v3, 0x0

    :cond_5
    :goto_4
    invoke-virtual {v15}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v15}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/d;

    move-object v8, v0

    invoke-virtual {v15, v8}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    iget-wide v5, v8, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    cmp-long v1, v5, v3

    if-nez v1, :cond_6

    iget-wide v5, v8, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    add-long/2addr v3, v5

    goto :goto_4

    :cond_6
    iget-wide v5, v8, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    cmp-long v1, v5, v3

    if-ltz v1, :cond_5

    iget-wide v5, v8, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    cmp-long v1, v5, v3

    if-lez v1, :cond_5

    new-instance v1, Lcom/google/android/exoplayer/upstream/j;

    iget-wide v5, v8, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    sub-long/2addr v5, v3

    move-object v7, v12

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-wide v3, v8, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    iget-wide v5, v8, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    add-long/2addr v3, v5

    goto :goto_4

    :cond_7
    cmp-long v1, v3, v10

    if-gez v1, :cond_1

    new-instance v1, Lcom/google/android/exoplayer/upstream/j;

    sub-long v5, v10, v3

    move-object v7, v12

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-virtual {v14, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v9

    throw v1

    :cond_8
    :try_start_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v3, v1}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_2

    :cond_9
    :try_start_7
    invoke-virtual {v12}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/offline/transfer/c;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    monitor-exit v9

    goto :goto_3

    :catchall_1
    move-exception v1

    :goto_5
    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v3, v2}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V

    :cond_a
    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_2
    move-exception v1

    move-object v2, v7

    goto :goto_5

    :cond_b
    move-object v1, v3

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->h:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->i:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->j:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final a(J)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/c;->d:Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/e;->c(Ljava/io/File;)J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
