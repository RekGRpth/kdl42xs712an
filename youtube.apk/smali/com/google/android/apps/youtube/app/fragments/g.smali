.class final Lcom/google/android/apps/youtube/app/fragments/g;
.super Lcom/google/android/apps/youtube/app/adapter/af;
.source "SourceFile"


# instance fields
.field public a:Z

.field final synthetic b:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/g;->b:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/g;->a()V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->relatedArtists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ArtistSnippet;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/g;->d(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/g;->a:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/fragments/g;->a:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/g;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 3

    const/4 v0, 0x3

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/adapter/af;->getCount()I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/fragments/g;->a:Z

    if-eqz v2, :cond_0

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
