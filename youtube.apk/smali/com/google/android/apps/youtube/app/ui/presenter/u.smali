.class public final Lcom/google/android/apps/youtube/app/ui/presenter/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/j;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/app/d/o;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/d/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/u;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/u;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/d/o;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/u;->c:Lcom/google/android/apps/youtube/app/d/o;

    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/android/apps/youtube/uilib/a/g;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/u;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/u;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/u;->c:Lcom/google/android/apps/youtube/app/d/o;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/s;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/d/o;)V

    return-object v0
.end method
