.class final Lcom/google/android/apps/youtube/app/offline/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/core/offline/store/l;

.field private final c:Lcom/google/android/apps/youtube/core/client/bc;

.field private final d:Lcom/google/android/apps/youtube/core/client/ce;

.field private final e:Lcom/google/android/apps/youtube/app/offline/a/f;

.field private final f:Lcom/google/android/apps/youtube/core/offline/store/i;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/offline/store/l;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/app/offline/a/f;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/ce;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->d:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/a/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {p5}, Lcom/google/android/apps/youtube/app/offline/a/f;->d()Lcom/google/android/apps/youtube/core/offline/store/i;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/l;->h(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/l;->c(Ljava/lang/String;)V

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->b(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 4

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->d:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/youtube/core/client/ce;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/youtube/core/offline/store/l;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->createForOffline(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/util/List;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed saving video subtitles "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/l;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->owner:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    return-void

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed saving video thumbnails for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed saving avatar for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->owner:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/e;->d(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed removing video "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->i(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/e;->g(Ljava/lang/String;)Landroid/util/Pair;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    iget v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_4

    const-string v2, "Playlist size doesn\'t match number of playlist videos"

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->size(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    move-object v2, v0

    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/offline/store/l;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    iget-object v0, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, v1, v0, v3}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Ljava/util/List;Ljava/util/List;Ljava/util/HashSet;)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I

    move-result v5

    invoke-virtual {v4, v2, v1, v5}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;I)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Failed inserting playlist "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to database"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->x(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed requesting playlist "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for offline"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->x(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed saving playlist thumbnail for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_2
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed saving avatar for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/Collection;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    goto :goto_4

    :cond_4
    move-object v2, v0

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, p3}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isMonetized(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;Ljava/util/HashSet;)V
    .locals 5

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/core/offline/store/i;->q(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {p3, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v3

    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/l;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->p(Ljava/lang/String;)Z

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->owner:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/l;->c(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->i(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->y(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->f(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getOfflineStreamQualityForQualityValue(I)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v3

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/e;->g(Ljava/lang/String;)Landroid/util/Pair;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    iget v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-eq v2, v4, :cond_5

    const-string v2, "Playlist size doesn\'t match number of playlist videos"

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->size(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    move-object v2, v0

    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/offline/store/l;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, v1, v0, v4}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Ljava/util/List;Ljava/util/List;Ljava/util/HashSet;)V

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v6, v2, v1, v5}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;Ljava/util/List;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->d(Ljava/lang/String;)V

    goto :goto_3

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed requesting playlist "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for offline"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->y(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed saving playlist thumbnail for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Failed syncing playlist "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to database"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->y(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/apps/youtube/app/offline/a/f;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/Collection;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0, p1, v0, v3}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    goto :goto_4

    :cond_5
    move-object v2, v0

    goto/16 :goto_1
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->z(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed removing playlist "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->w(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->w(Ljava/lang/String;)V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/l;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/core/client/bc;->h(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/youtube/core/offline/store/l;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private f(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->e(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getOfflineStreamQualityForQualityValue(I)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    return-void
.end method

.method private g(Ljava/lang/String;)Landroid/util/Pair;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/core/client/bc;->e(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    return-object v0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 7

    const/4 v3, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v3

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->h()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    sget-object v5, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I

    move-result v6

    invoke-virtual {v4, v1, v5, v6}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;I)Z

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->n(Ljava/lang/String;)Z

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    goto :goto_0

    :cond_4
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v4, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v4, v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v5, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v4, v5, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Failed to offline video because video is not in PLAYABLE state: "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->t(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Failed requesting video "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for offline"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->t(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;I)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Failed inserting video "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " to database"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/offline/a/e;->b(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->t(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->n(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->s(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->t(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->PAUSED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->q(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->f(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->j(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->e(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;I)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->s(Ljava/lang/String;)V

    :cond_7
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->f(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->f()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->u(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->e:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->c(Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/e;->f:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/e;->a(Ljava/lang/String;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
