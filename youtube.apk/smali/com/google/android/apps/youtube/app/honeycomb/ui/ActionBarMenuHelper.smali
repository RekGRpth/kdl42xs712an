.class public abstract Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;
.super Lcom/google/android/apps/youtube/app/ui/bu;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;Lcom/google/android/apps/youtube/app/compat/o;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/ui/bu;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;Lcom/google/android/apps/youtube/app/compat/o;)V

    return-void
.end method

.method public static final a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->M()Lcom/google/android/apps/youtube/app/compat/o;

    move-result-object v1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/d;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;Lcom/google/android/apps/youtube/app/compat/o;)V

    return-object v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V
.end method

.method public abstract a(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V
.end method

.method public abstract a(Ljava/lang/String;Z)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()V
.end method

.method public abstract b(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V
.end method

.method public abstract c()Z
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method
