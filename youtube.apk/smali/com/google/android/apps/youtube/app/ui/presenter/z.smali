.class public final Lcom/google/android/apps/youtube/app/ui/presenter/z;
.super Lcom/google/android/apps/youtube/uilib/a/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final f:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

.field private final g:Lcom/google/android/apps/youtube/app/d/e;

.field private final h:Landroid/content/Context;

.field private final i:Lcom/google/android/apps/youtube/uilib/a/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/datalib/d/a;)V
    .locals 3

    invoke-direct {p0, p4, p3}, Lcom/google/android/apps/youtube/uilib/a/a;-><init>(Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/uilib/a/i;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->h:Landroid/content/Context;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->i:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->w:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->an:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fV:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fe:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ah:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-direct {v1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->C:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->f:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/aa;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/aa;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/z;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->g:Lcom/google/android/apps/youtube/app/d/e;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a:Landroid/view/View;

    invoke-interface {p3, v0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/e;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/a/a;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->f:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/k;->g:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->b()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->g:Lcom/google/android/apps/youtube/app/d/e;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;Lcom/google/android/apps/youtube/app/d/e;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->i:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a()V

    goto :goto_1
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/z;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/gr;->a()Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/h;->am:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/z;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/e;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/e;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/e;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/z;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/e;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
