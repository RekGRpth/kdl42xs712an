.class abstract Lcom/google/android/apps/youtube/app/adapter/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Landroid/content/res/Resources;

.field protected final c:Landroid/view/View;

.field protected final d:Landroid/widget/TextView;

.field protected final e:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->b:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->c:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->c:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->e:Landroid/widget/TextView;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/bk;-><init>(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->d:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->e:Landroid/widget/TextView;

    iget v1, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->duration:I

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/e/m;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bk;->c:Landroid/view/View;

    return-object v0
.end method

.method public bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/bk;->a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
