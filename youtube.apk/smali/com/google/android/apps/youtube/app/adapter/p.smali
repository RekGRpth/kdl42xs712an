.class public final Lcom/google/android/apps/youtube/app/adapter/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

.field private final b:Lcom/google/android/apps/youtube/app/adapter/s;

.field private final c:Lcom/google/android/apps/youtube/core/identity/UserProfile;

.field private final d:Lcom/google/android/apps/youtube/core/identity/UserProfile;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/adapter/p;->c:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/adapter/p;->d:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/p;->b:Lcom/google/android/apps/youtube/app/adapter/s;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 4

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x194

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->d(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->x:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/p;->b:Lcom/google/android/apps/youtube/app/adapter/s;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/p;->c:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->m(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/ui/gj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/p;->c:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->UNSUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/gj;->a(Landroid/net/Uri;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->n(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/p;->b:Lcom/google/android/apps/youtube/app/adapter/s;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/p;->c:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->m(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/ui/gj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/p;->c:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory$State;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/gj;->a(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/p;->b:Lcom/google/android/apps/youtube/app/adapter/s;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/p;->c:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->l(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/q;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/ui/q;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/p;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->m(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/app/ui/gj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/p;->d:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gj;->a(Landroid/net/Uri;)V

    return-void
.end method
