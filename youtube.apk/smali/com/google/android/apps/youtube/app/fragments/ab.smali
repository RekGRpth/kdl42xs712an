.class final Lcom/google/android/apps/youtube/app/fragments/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->j(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;Ljava/lang/String;Z)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 6

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/l;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/l;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;)Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->f(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->h(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/a;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;->a()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->h(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    instance-of v1, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-static {v5}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->h(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->h(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->g(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ab;->a:Lcom/google/android/apps/youtube/app/fragments/GuideFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->i(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    return-void
.end method
