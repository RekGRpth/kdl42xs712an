.class public final Lcom/google/android/apps/youtube/app/ui/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final A:J

.field private B:Z

.field private C:Z

.field private final D:I

.field private E:Lcom/google/android/apps/youtube/core/identity/UserProfile;

.field private F:Lcom/google/android/apps/youtube/app/ui/j;

.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/app/ui/k;

.field private final d:Lcom/google/android/apps/youtube/app/ui/i;

.field private final e:Lcom/google/android/apps/youtube/core/aw;

.field private final f:Lcom/google/android/apps/youtube/app/am;

.field private final g:Landroid/content/res/Resources;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/view/View;

.field private final p:Landroid/view/View;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/ProgressBar;

.field private final s:Landroid/widget/ImageView;

.field private final t:Landroid/widget/ImageView;

.field private final u:F

.field private final v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

.field private final w:Landroid/widget/ImageView;

.field private x:Z

.field private y:Z

.field private final z:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;)V
    .locals 7

    sget v6, Lcom/google/android/youtube/p;->eG:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/ui/g;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;I)V

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->a:Landroid/app/Activity;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->f:Lcom/google/android/apps/youtube/app/am;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/k;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/k;-><init>(Lcom/google/android/apps/youtube/app/ui/g;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->c:Lcom/google/android/apps/youtube/app/ui/k;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/i;-><init>(Lcom/google/android/apps/youtube/app/ui/g;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->d:Lcom/google/android/apps/youtube/app/ui/i;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    sget v0, Lcom/google/android/youtube/j;->ak:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->h:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->al:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->ah:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->ap:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->ar:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->aq:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->l:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->ao:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->n:Landroid/widget/TextView;

    iput p6, p0, Lcom/google/android/apps/youtube/app/ui/g;->D:I

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/g;->x:Z

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/g;->y:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->h:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->aj:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    sget v2, Lcom/google/android/youtube/j;->ai:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const-string v2, "banner may not be null if bannerContainer is set"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->w:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->x:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    :goto_1
    const/high16 v0, 0x10a0000    # android.R.anim.fade_in

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->z:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    const/high16 v2, 0x10e0000    # android.R.integer.config_shortAnimTime

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/google/android/apps/youtube/app/ui/g;->A:J

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->B:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->C:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fH:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->o:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eZ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fc:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->q:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fb:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fa:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->t:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->i:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fd:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->s:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/i;->b:I

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->u:F

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->w:Landroid/widget/ImageView;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    return-object v0
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/h;->am:I

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/g;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->C:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/g;->a()V

    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/g;->E:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->k:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->k:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->a:Landroid/app/Activity;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->D:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->m:Landroid/widget/TextView;

    const-string v1, "%1$,d"

    new-array v2, v5, [Ljava/lang/Object;

    iget-wide v3, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadViewsCount:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->l:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->l:Landroid/widget/TextView;

    const-string v1, "%1$,d"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadedCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->n:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/o;->c:I

    iget v3, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->subscribersCount:I

    new-array v4, v5, [Ljava/lang/Object;

    iget v5, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->subscribersCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/g;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/g;->c:Lcom/google/android/apps/youtube/app/ui/k;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/g;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/g;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->B:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->w:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/g;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->x:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/g;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->B:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->z:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/g;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->A:J

    return-wide v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;Z)V
    .locals 7

    const/4 v6, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    const/16 v5, 0x8

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/app/ui/h;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_1
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->u:F

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/h;->ay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->q:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/p;->fO:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->q:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/h;->ax:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->q:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/p;->fN:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->t:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->t:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/g;->F:Lcom/google/android/apps/youtube/app/ui/j;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/g;->b(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/g;->w:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_2

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/g;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/g;->g:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/youtube/e;->b:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v2, :sswitch_data_0

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerTabletMediumUri:Landroid/net/Uri;

    :goto_0
    if-eqz v2, :cond_4

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->y:Z

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/g;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/g;->a:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/g;->d:Lcom/google/android/apps/youtube/app/ui/i;

    invoke-static {v4, v5}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/g;->x:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/g;->C:Z

    if-nez v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->B:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    :cond_0
    :goto_2
    return-void

    :sswitch_0
    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerTabletLowUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_1
    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerTabletHdUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_2
    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerTabletExtraHdUri:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v2, :sswitch_data_1

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerMobileMediumUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_3
    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerMobileLowUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_4
    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerMobileMediumHdUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_5
    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerMobileHdUri:Landroid/net/Uri;

    goto :goto_0

    :sswitch_6
    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Branding;->channelBannerMobileExtraHdUri:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->y:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xf0 -> :sswitch_1
        0x140 -> :sswitch_2
        0x1e0 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x78 -> :sswitch_3
        0xf0 -> :sswitch_4
        0x140 -> :sswitch_5
        0x1e0 -> :sswitch_6
    .end sparse-switch
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error retrieving user profile"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->e:Lcom/google/android/apps/youtube/core/aw;

    sget v1, Lcom/google/android/youtube/p;->af:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/g;->a()V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/app/ui/g;->b(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    return-void
.end method

.method public final a(ZF)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/g;->x:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->x:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->y:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;->setAspectRatio(F)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->w:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->w:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->v:Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/FixedAspectRatioFrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->F:Lcom/google/android/apps/youtube/app/ui/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->o:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->f:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/g;->E:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/am;->a(Landroid/net/Uri;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->p:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/g;->F:Lcom/google/android/apps/youtube/app/ui/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/j;->i_()V

    goto :goto_0
.end method
