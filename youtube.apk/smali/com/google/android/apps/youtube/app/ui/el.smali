.class final Lcom/google/android/apps/youtube/app/ui/el;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/ei;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/ei;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/el;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    const-string v0, "error retrieving subtitle tracks"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/el;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->i(Lcom/google/android/apps/youtube/app/ui/ei;)Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->cT:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/el;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->i(Lcom/google/android/apps/youtube/app/ui/ei;)Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->cT:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/el;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/ei;->i(Lcom/google/android/apps/youtube/app/ui/ei;)Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->gk:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->createDisableSubtitleOption(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/el;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/ei;->h(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/util/List;)V

    goto :goto_0
.end method
