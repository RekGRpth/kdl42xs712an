.class public abstract Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/compat/i;
.implements Lcom/google/android/apps/youtube/app/remote/ap;


# instance fields
.field private A:Lcom/google/android/apps/youtube/app/remote/an;

.field private B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

.field private C:Z

.field private D:Lcom/google/android/apps/youtube/app/compat/l;

.field private E:Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

.field private F:Lcom/google/android/apps/youtube/core/client/cf;

.field private n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private o:Lcom/google/android/apps/youtube/app/ax;

.field private p:Landroid/content/SharedPreferences;

.field private q:Lcom/google/android/apps/youtube/core/Analytics;

.field private r:Lcom/google/android/apps/youtube/app/am;

.field private s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

.field private t:Lcom/google/android/apps/youtube/app/compat/o;

.field private u:Lcom/google/android/apps/youtube/app/compat/j;

.field private v:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

.field private w:Z

.field private x:Landroid/util/SparseArray;

.field private y:Z

.field private z:Lcom/google/android/apps/youtube/app/ui/es;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->w:Z

    return v0
.end method

.method private e()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/compat/a;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    return-void
.end method

.method private f()V
    .locals 3

    const/16 v2, 0x11

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/android/gms/common/e;->a(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-static {v0, p0, v2}, Lcom/google/android/gms/common/e;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/an;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/an;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {v0, p0, v2}, Lcom/google/android/gms/common/e;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/am;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/am;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public B()Lcom/google/android/apps/youtube/app/remote/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/apps/youtube/app/remote/an;

    return-object v0
.end method

.method public final K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/h;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    return-object v0
.end method

.method public final L()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final M()Lcom/google/android/apps/youtube/app/compat/o;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/apps/youtube/app/compat/o;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/o;

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/compat/o;-><init>(Landroid/content/Context;Landroid/view/MenuInflater;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/apps/youtube/app/compat/o;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/apps/youtube/app/compat/o;

    return-object v0
.end method

.method public final N()Lcom/google/android/apps/youtube/app/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/apps/youtube/app/am;

    return-object v0
.end method

.method public final O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    return-object v0
.end method

.method public final P()Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method public final Q()Lcom/google/android/apps/youtube/app/honeycomb/phone/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    return-object v0
.end method

.method protected final R()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->C:Z

    goto :goto_0
.end method

.method public final S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    return-object v0
.end method

.method protected a(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aD()Lcom/google/android/apps/youtube/core/identity/al;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/google/android/apps/youtube/core/identity/al;->a(Landroid/app/Activity;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/content/Intent;ILcom/google/android/apps/youtube/app/honeycomb/phone/ao;)V
    .locals 2

    const/16 v1, 0x38a

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    invoke-virtual {v0, v1, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/apps/youtube/app/ui/es;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/es;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/apps/youtube/app/ui/es;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/es;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/android/apps/youtube/app/compat/q;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected a_(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->k()Lcom/google/android/apps/youtube/core/identity/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/identity/o;->a(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->k()Lcom/google/android/apps/youtube/core/identity/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/identity/o;->b(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->b()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->w:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->w:Z

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/al;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/al;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(I)V

    return-void
.end method

.method protected b(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method protected e_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g()Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ancestor_classname"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x24000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->finish()V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v1, "Target Activity class for Up event not found"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/am;->c()V

    goto :goto_0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method protected j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ao;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ao;->a(IILandroid/content/Intent;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->x:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x11

    if-ne p1, v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->f()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/apps/youtube/app/compat/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/apps/youtube/app/compat/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/l;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/apps/youtube/app/compat/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/l;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->c()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x3

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->f()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->setDefaultKeyMode(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->setVolumeControlStream(I)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->y:Z

    sget v0, Lcom/google/android/youtube/l;->bl:I

    invoke-super {p0, v0}, Landroid/support/v4/app/FragmentActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->UP:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->p:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/a;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/apps/youtube/app/am;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/apps/youtube/app/am;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/es;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/aw;->q()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/es;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/apps/youtube/app/ui/es;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/apps/youtube/app/remote/an;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/e;->c:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->p:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->Q()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->L()Lcom/google/android/apps/youtube/core/client/cf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/apps/youtube/core/client/cf;

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected final onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    if-eqz p2, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/google/android/apps/youtube/app/compat/l;->a(Landroid/app/Activity;)Lcom/google/android/apps/youtube/app/compat/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/apps/youtube/app/compat/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/apps/youtube/app/compat/l;

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a_(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x405
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/j;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/app/compat/j;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/apps/youtube/app/compat/j;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e_()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a(Lcom/google/android/apps/youtube/app/compat/j;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->d()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a(Landroid/view/View;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/apps/youtube/app/compat/j;)Z

    move-result v1

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-static {}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->f()Z

    move-result v1

    or-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    sget v2, Lcom/google/android/youtube/j;->cr:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->c()V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/apps/youtube/app/ui/es;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/ui/es;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-gt v1, v2, :cond_1

    const/16 v1, 0x52

    if-ne p1, v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    const/16 v1, 0x405

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->showDialog(I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/apps/youtube/app/ui/es;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/es;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c    # android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->g()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/apps/youtube/app/compat/q;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/apps/youtube/app/compat/q;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->d()V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->b()V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/apps/youtube/app/compat/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/youtube/app/compat/l;->a(Lcom/google/android/apps/youtube/app/compat/j;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x405
        :pswitch_0
    .end packed-switch
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e_()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->b(Lcom/google/android/apps/youtube/app/compat/j;)Z

    move-result v1

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-static {}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->g()Z

    move-result v1

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/apps/youtube/app/compat/l;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/apps/youtube/app/compat/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/compat/l;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/apps/youtube/app/compat/l;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->u:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/compat/l;->a(Lcom/google/android/apps/youtube/app/compat/j;)V

    :cond_1
    return v0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 3

    const/4 v0, 0x1

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Z)V

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->j()Lcom/google/android/apps/youtube/core/client/at;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/at;->a(Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Lcom/google/android/apps/youtube/app/remote/ap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/apps/youtube/app/ui/es;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/es;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->l()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/bi;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->b()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/apps/youtube/app/ui/es;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/es;->a()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->e_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/remote/an;->b(Lcom/google/android/apps/youtube/app/remote/ap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->l()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->E:Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/bi;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->B:Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->c()V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/apps/youtube/core/client/cf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->F:Lcom/google/android/apps/youtube/core/client/cf;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/cf;->a()V

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onUserInteraction()V

    return-void
.end method

.method public final setContentView(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public final setContentView(Landroid/view/View;)V
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public final setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
