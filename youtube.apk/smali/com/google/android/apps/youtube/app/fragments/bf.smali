.class final Lcom/google/android/apps/youtube/app/fragments/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/bf;->a:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error retrieving playlist description"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bf;->a:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bf;->a:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->b(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
