.class public final Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private b:Lcom/google/android/apps/youtube/core/aw;

.field private d:Landroid/content/res/Resources;

.field private e:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

.field private f:Lcom/google/android/apps/youtube/uilib/innertube/t;

.field private g:Landroid/widget/ListView;

.field private h:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private i:Lcom/google/android/apps/youtube/app/ui/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lcom/google/android/youtube/l;->ba:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->g:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->g:Landroid/widget/ListView;

    return-object v0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/p;->fS:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->d:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->b:Lcom/google/android/apps/youtube/core/aw;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->C()Lcom/google/android/apps/youtube/datalib/e/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/ui/a;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->i:Lcom/google/android/apps/youtube/app/ui/a;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Lcom/google/a/a/a/a/qu;

    invoke-direct {v1}, Lcom/google/a/a/a/a/qu;-><init>()V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v2, "section_list_without_preview_proto"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/google/a/a/a/a/qu;->b:[Lcom/google/a/a/a/a/qw;

    array-length v0, v0

    if-lez v0, :cond_0

    new-instance v0, Lcom/google/a/a/a/a/qq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/qq;-><init>()V

    iget-object v1, v1, Lcom/google/a/a/a/a/qu;->b:[Lcom/google/a/a/a/a/qw;

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/google/a/a/a/a/qw;->b:Lcom/google/a/a/a/a/it;

    new-instance v2, Lcom/google/a/a/a/a/qt;

    invoke-direct {v2}, Lcom/google/a/a/a/a/qt;-><init>()V

    iput-object v1, v2, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/a/a/a/a/qt;

    aput-object v2, v1, v3

    iput-object v1, v0, Lcom/google/a/a/a/a/qq;->b:[Lcom/google/a/a/a/a/qt;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;-><init>(Lcom/google/a/a/a/a/qq;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 14

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->b:Lcom/google/android/apps/youtube/core/aw;

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->RELATED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->h:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->aQ()Lcom/google/android/apps/youtube/core/identity/as;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->i:Lcom/google/android/apps/youtube/app/ui/a;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->F()Lcom/google/android/apps/youtube/datalib/innertube/bc;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/ax;->G()Lcom/google/android/apps/youtube/datalib/innertube/av;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->b:Lcom/google/android/apps/youtube/core/aw;

    iget-object v13, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->h:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/a;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/datalib/innertube/av;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/am;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/cv;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/ui/presenter/cv;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/am;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/common/c/a;)V

    new-instance v4, Lcom/google/android/apps/youtube/app/adapter/z;

    invoke-direct {v4, v1}, Lcom/google/android/apps/youtube/app/adapter/z;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/am;)V

    new-instance v1, Lcom/google/android/apps/youtube/uilib/innertube/t;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->g:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->t()Lcom/google/android/apps/youtube/datalib/innertube/ay;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v6

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->b:Lcom/google/android/apps/youtube/core/aw;

    move-object v7, v0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/youtube/uilib/innertube/t;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/i;Landroid/widget/ListView;Lcom/google/android/apps/youtube/uilib/innertube/p;Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/uilib/innertube/j;Lcom/google/android/apps/youtube/core/aw;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->f:Lcom/google/android/apps/youtube/uilib/innertube/t;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->f:Lcom/google/android/apps/youtube/uilib/innertube/t;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/t;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V

    return-void
.end method

.method public final r()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->h:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    goto :goto_0
.end method

.method public final t()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;->h:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method
