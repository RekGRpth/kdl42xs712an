.class final Lcom/google/android/apps/youtube/app/ui/gg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/gg;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x190

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->f(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->w:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->c(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->editUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->e(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/p;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/ui/p;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gg;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    return-void
.end method
