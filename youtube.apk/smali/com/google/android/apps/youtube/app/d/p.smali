.class final Lcom/google/android/apps/youtube/app/d/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/d/o;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/d/f;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/d/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/d/f;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/d/p;-><init>(Lcom/google/android/apps/youtube/app/d/f;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/d/p;Lcom/google/android/apps/youtube/core/identity/UserProfile;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V
    .locals 6

    const/4 v0, 0x0

    if-nez p3, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->k(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/ak;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v1, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    new-instance v3, Lcom/google/android/gms/plus/j;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/d/f;->c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/plus/j;-><init>(Landroid/app/Activity;)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/d/f;->k(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/identity/ak;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/j;->d(Ljava/lang/String;)Lcom/google/android/gms/plus/j;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/h;->b:I

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/j;->a(I)Lcom/google/android/gms/plus/j;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/d/f;->c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;

    move-result-object v4

    sget v5, Lcom/google/android/youtube/p;->A:I

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/j;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/j;

    move-result-object v3

    const-string v4, "104"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/j;->h(Ljava/lang/String;)Lcom/google/android/gms/plus/j;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://www.youtube.com/watch?v="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/j;->a(Landroid/net/Uri;)Lcom/google/android/gms/plus/j;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/d/f;->c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;

    move-result-object v4

    sget v5, Lcom/google/android/youtube/p;->as:I

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/plus/j;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/j;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/gms/plus/j;->e(Ljava/lang/String;)Lcom/google/android/gms/plus/j;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/j;->f(Ljava/lang/String;)Lcom/google/android/gms/plus/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/j;->g(Ljava/lang/String;)Lcom/google/android/gms/plus/j;

    move-result-object v0

    const-string v1, "youTubeComments"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/j;->c(Ljava/lang/String;)Lcom/google/android/gms/plus/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/j;->a(Z)Lcom/google/android/gms/plus/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/j;->a()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->k(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/ak;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    move-result-object v2

    instance-of v3, p3, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    if-eqz v3, :cond_5

    check-cast p3, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    :goto_3
    new-instance v0, Lcom/google/android/gms/plus/h;

    invoke-direct {v0}, Lcom/google/android/gms/plus/h;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/d/f;->k(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/identity/ak;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/h;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/h;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/h;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/h;

    move-result-object v0

    const-string v3, "104"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/h;->e(Ljava/lang/String;)Lcom/google/android/gms/plus/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/h;->c(Ljava/lang/String;)Lcom/google/android/gms/plus/h;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "youTubeComments"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/h;->d(Ljava/lang/String;)Lcom/google/android/gms/plus/h;

    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->j()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/plus/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/h;

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/plus/h;->a()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    :cond_5
    move-object p3, v0

    goto :goto_3
.end method

.method private b(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/p;->a:Lcom/google/android/apps/youtube/app/d/f;

    new-instance v1, Lcom/google/android/apps/youtube/app/d/q;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/d/q;-><init>(Lcom/google/android/apps/youtube/app/d/p;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/app/d/f;Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/d/p;->b(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/d/p;->b(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V

    return-void
.end method
