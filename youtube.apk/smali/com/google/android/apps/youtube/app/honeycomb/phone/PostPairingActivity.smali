.class public Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# static fields
.field private static final n:Ljava/util/Map;


# instance fields
.field private o:Lcom/google/android/apps/youtube/core/identity/l;

.field private p:Lcom/google/android/apps/youtube/core/client/bc;

.field private q:Lcom/google/android/apps/youtube/core/client/bj;

.field private r:Lcom/google/android/apps/youtube/common/network/h;

.field private s:Landroid/view/View;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/ImageView;

.field private v:Lcom/google/android/apps/youtube/app/adapter/h;

.field private w:Lcom/google/android/apps/youtube/app/remote/an;

.field private x:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

.field private y:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->n:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->values()[Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->n:Ljava/util/Map;

    iget v5, v3, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->position:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->x:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V
    .locals 6

    const/4 v2, 0x0

    const/16 v3, 0xf

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/m;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v5

    sget-object v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/l;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0, v3, v5}, Lcom/google/android/apps/youtube/core/client/bc;->a(ILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0, v3, v5}, Lcom/google/android/apps/youtube/core/client/bc;->d(ILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0, v3, v5}, Lcom/google/android/apps/youtube/core/client/bc;->b(ILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0, v3, v5}, Lcom/google/android/apps/youtube/core/client/bc;->c(ILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/apps/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->y:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->s()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->TODAY:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/apps/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    const-string v2, "Music"

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->y:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->s()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->TODAY:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/apps/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    sget-object v4, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->TODAY:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    move-object v3, v2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V
    .locals 2

    iget v0, p1, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->position:I

    invoke-static {}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->values()[Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "We run out of feeds! How?"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->n:Ljava/util/Map;

    iget v1, p1, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->position:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->o:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->isAccountFeed:Z

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->POPULAR:Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/apps/youtube/app/adapter/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->v:Lcom/google/android/apps/youtube/app/adapter/h;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->t:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->u:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->z:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public handleSignOutEvent(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->finish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->g()Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/youtube/l;->aL:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->setContentView(I)V

    sget v0, Lcom/google/android/youtube/p;->fR:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->y:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->y:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->o:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->r:Lcom/google/android/apps/youtube/common/network/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->w:Lcom/google/android/apps/youtube/app/remote/an;

    sget v0, Lcom/google/android/youtube/j;->bn:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->t:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->u:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->s:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->dl:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->z:Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/k;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->r:Lcom/google/android/apps/youtube/common/network/h;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/honeycomb/phone/k;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->v:Lcom/google/android/apps/youtube/app/adapter/h;

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->y:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->E()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->y:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->E()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public onStart()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->w:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->x:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->x:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-nez v0, :cond_1

    const-string v0, "Ooops! We should be connected a route but that\'s not the case!"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->o:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->WATCH_LATER:Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->x:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/youtube/j;->dK:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/p;->dI:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->x:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/youtube/app/remote/bg;->getScreenName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->POPULAR:Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    goto :goto_1
.end method
