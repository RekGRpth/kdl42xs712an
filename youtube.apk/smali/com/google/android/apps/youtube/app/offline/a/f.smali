.class public final Lcom/google/android/apps/youtube/app/offline/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/ax;

.field private final b:Lcom/google/android/apps/youtube/app/offline/c;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Lcom/google/android/apps/youtube/common/c/a;

.field private final e:Lcom/google/android/apps/youtube/core/player/a/l;

.field private final f:Ljava/lang/String;

.field private final g:Landroid/accounts/Account;

.field private final h:Lcom/google/android/apps/youtube/common/e/b;

.field private final i:Lcom/google/android/apps/youtube/app/offline/p;

.field private final j:Lcom/google/android/apps/youtube/core/utils/w;

.field private final k:Lcom/google/android/apps/youtube/app/offline/a/x;

.field private final l:Lcom/google/android/apps/youtube/core/offline/store/i;

.field private final m:Lcom/google/android/apps/youtube/core/offline/store/l;

.field private n:Lcom/google/android/apps/youtube/app/offline/a/c;

.field private o:Landroid/os/HandlerThread;

.field private p:Landroid/os/Handler;

.field private q:Lcom/google/android/exoplayer/upstream/cache/a;

.field private r:Lcom/google/android/exoplayer/upstream/cache/a;

.field private s:Lcom/google/android/apps/youtube/app/offline/a/e;

.field private final t:Lcom/google/android/apps/youtube/app/offline/a/af;

.field private final u:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ax;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/a/l;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/core/utils/w;Lcom/google/android/apps/youtube/app/offline/p;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 7

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ax;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/a/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->e:Lcom/google/android/apps/youtube/core/player/a/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->f:Ljava/lang/String;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/w;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->i:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->h:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/a/x;

    invoke-direct {v0, p0, v6}, Lcom/google/android/apps/youtube/app/offline/a/x;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->k:Lcom/google/android/apps/youtube/app/offline/a/x;

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/c;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->af()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v2

    invoke-direct {v0, v1, p2, v2}, Lcom/google/android/apps/youtube/app/offline/c;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->b:Lcom/google/android/apps/youtube/app/offline/c;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->u:Ljava/util/HashMap;

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/a/af;

    invoke-direct {v0, p0, v6}, Lcom/google/android/apps/youtube/app/offline/a/af;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->af()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v2

    invoke-direct {v0, v1, p7, v2, p4}, Lcom/google/android/apps/youtube/core/offline/store/l;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/ce;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->m:Lcom/google/android/apps/youtube/core/offline/store/l;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->af()Landroid/content/Context;

    move-result-object v1

    const-string v2, "."

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "offline"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    aput-object p7, v3, v4

    const/4 v4, 0x2

    const-string v5, "db"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->m:Lcom/google/android/apps/youtube/core/offline/store/l;

    new-instance v4, Lcom/google/android/apps/youtube/app/offline/a/p;

    invoke-direct {v4, p0, v6}, Lcom/google/android/apps/youtube/app/offline/a/p;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;B)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/offline/store/i;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/l;Lcom/google/android/apps/youtube/core/offline/store/j;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->a()V

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/a/c;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->ag()Lcom/google/android/apps/youtube/app/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/offline/a/c;-><init>(Lcom/google/android/apps/youtube/app/a/a;Lcom/google/android/apps/youtube/core/offline/store/i;Lcom/google/android/apps/youtube/common/e/b;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->n:Lcom/google/android/apps/youtube/app/offline/a/c;

    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->o:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->o:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/a/e;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->af()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->m:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v3

    move-object v4, p4

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/offline/a/e;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/offline/store/l;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/app/offline/a/f;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->s:Lcom/google/android/apps/youtube/app/offline/a/e;

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->o:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->s:Lcom/google/android/apps/youtube/app/offline/a/e;

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    return-void
.end method

.method private A(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 9

    const-wide/32 v5, 0x112a880

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineNoMediaException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineNoMediaException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineMediaUnplayableException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineMediaUnplayableException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException$OfflineVideoExpiredPolicyException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException$OfflineVideoExpiredPolicyException;-><init>()V

    throw v0

    :cond_2
    new-instance v0, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException$OfflineVideoDisabledPolicyException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException$OfflineVideoDisabledPolicyException;-><init>()V

    throw v0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->n()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineMediaIncompleteException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineMediaIncompleteException;-><init>()V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/a/w;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v3

    add-long/2addr v3, v5

    const/4 v5, 0x0

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/google/android/apps/youtube/app/offline/a/w;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;JB)V

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->e()Z

    move-result v1

    if-nez v1, :cond_5

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v3

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/32 v6, 0x112a880

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->cloneAndMergeOfflineStreams(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;JJ)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_5
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private B(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s:%s:ad"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private C(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/core/offline/store/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->C(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/accounts/Account;)V
    .locals 2

    const/4 v1, 0x1

    const-string v0, "com.google.android.youtube.provider"

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v0, "com.google.android.youtube.provider"

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    return-void
.end method

.method private static a(Landroid/accounts/Account;J)V
    .locals 2

    const-string v0, "com.google.android.youtube.provider"

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0, v0, v1, p1, p2}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->f:Ljava/lang/String;

    const-string v2, "fake_source"

    invoke-virtual {v0, v1, p1, v2, p2}, Lcom/google/android/apps/youtube/core/transfer/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/core/player/a/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->e:Lcom/google/android/apps/youtube/core/player/a/l;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->B(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/accounts/Account;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "com.google.android.youtube.provider"

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v0, "com.google.android.youtube.provider"

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    const-string v0, "com.google.android.youtube.provider"

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0, v0, v1}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/offline/a/f;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/offline/a/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/offline/a/f;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/offline/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->b:Lcom/google/android/apps/youtube/app/offline/c;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/core/offline/store/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->m:Lcom/google/android/apps/youtube/core/offline/store/l;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/offline/a/f;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->u:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/exoplayer/upstream/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->q:Lcom/google/android/exoplayer/upstream/cache/a;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/exoplayer/upstream/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->r:Lcom/google/android/exoplayer/upstream/cache/a;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->a:Lcom/google/android/apps/youtube/app/ax;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
    .locals 5

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ADDING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ADDING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ALREADY_ADDED:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/4 v2, 0x2

    new-instance v3, Landroid/util/Pair;

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getOfflineStreamQualityForFormatType(Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ADDING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
    .locals 3

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->i(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p2, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->CANNOT_ADD:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ADDING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ALREADY_ADDED:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->i:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/p;->c()Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getFormatType()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bn()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->q:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bo()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->r:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/exoplayer/upstream/cache/a;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->q:Lcom/google/android/exoplayer/upstream/cache/a;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->r:Lcom/google/android/exoplayer/upstream/cache/a;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->a([Lcom/google/android/exoplayer/upstream/cache/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->e:Lcom/google/android/apps/youtube/core/player/a/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->q:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->r:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/a/l;->a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    const-string v0, "PUDL account found, making it syncable"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/a/f;->i()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Landroid/accounts/Account;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Landroid/accounts/Account;J)V

    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v5, 0x0

    cmp-long v0, p1, v5

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    cmp-long v0, p1, v5

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/a/f;->i()J

    move-result-wide v3

    cmp-long v0, v3, v5

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Landroid/accounts/Account;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Landroid/accounts/Account;J)V

    :goto_1
    const-string v0, "offline_resync_interval_%s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->b(Landroid/accounts/Account;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/i;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/i;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/Collection;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/youtube/app/offline/a/af;->a(Lcom/google/android/apps/youtube/app/offline/a/af;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/Collection;)Lcom/google/android/apps/youtube/app/offline/a/ag;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl event playlist "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " add"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/a/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a(Lcom/google/android/apps/youtube/app/offline/a/ag;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/youtube/app/offline/a/q;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/s;B)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b(Lcom/google/android/apps/youtube/app/offline/a/ag;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/af;->b(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;I)V

    return-void
.end method

.method final a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;I)V
    .locals 4

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "pudl event playlist "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " progress: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v3, Lcom/google/android/apps/youtube/app/offline/a/t;

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v3, p1, v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/t;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/s;ZB)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method final a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->h()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    const-wide/16 v3, 0x1f4

    add-long/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/16 v5, 0x8

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v3, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/g;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/offline/a/g;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method final a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->n:Lcom/google/android/apps/youtube/app/offline/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>()V

    const-string v2, "stream_quality"

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;I)V

    const-string v2, "video_id"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ad"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->B(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/k;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/youtube/app/offline/a/k;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V
    .locals 3

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>()V

    const-string v1, "stream_quality"

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;I)V

    const-string v1, "video_id"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v1, "playlist_id"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/app/offline/a/f;->C(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->w(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->e(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getOfflineStreamQualityForQualityValue(I)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Z
    .locals 4

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/16 v2, 0xa

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/v;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->cloneAndReplaceOfflineState(Lcom/google/android/apps/youtube/datalib/innertube/model/v;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->d()J

    move-result-wide v3

    invoke-virtual {v2, v1, v0, v3, v4}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->u(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
    .locals 5

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->i(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ALREADY_ADDED:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/16 v2, 0x9

    new-instance v3, Landroid/util/Pair;

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getOfflineStreamQualityForFormatType(Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ADDING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 3

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/af;->a(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/offline/a/ag;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a(Lcom/google/android/apps/youtube/app/offline/a/ag;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->u:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->u:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->i(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->u:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;J)Lcom/google/android/apps/youtube/datalib/legacy/model/u;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/w;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p2, p3, v2}, Lcom/google/android/apps/youtube/app/offline/a/w;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;JB)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PUDL account close: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->o:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->o:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->o:Landroid/os/HandlerThread;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    const-string v0, "Offline Found account, making it non-syncable"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->g:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->b(Landroid/accounts/Account;)V

    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/j;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/j;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method final b(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/Collection;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/youtube/app/offline/a/af;->a(Lcom/google/android/apps/youtube/app/offline/a/af;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/Collection;)Lcom/google/android/apps/youtube/app/offline/a/ag;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl event playlist "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sync"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/a/u;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a(Lcom/google/android/apps/youtube/app/offline/a/ag;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/youtube/app/offline/a/u;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/s;B)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b(Lcom/google/android/apps/youtube/app/offline/a/ag;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/af;->b(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/l;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/offline/a/l;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final c()Lcom/google/android/apps/youtube/core/transfer/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->k:Lcom/google/android/apps/youtube/app/offline/a/x;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/v;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->k(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/m;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/offline/a/m;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/o;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/offline/a/o;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/i;->f(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final d()Lcom/google/android/apps/youtube/core/offline/store/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->l(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->d()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->m()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final g()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->n:Lcom/google/android/apps/youtube/app/offline/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/a/c;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method final h()V
    .locals 6

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/k;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/k;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, v3, v5}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/app/offline/a/f;->u(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/offline/store/i;->j(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/af;->a(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/offline/a/ag;

    move-result-object v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->i(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    invoke-static {v1, v0, v5}, Lcom/google/android/apps/youtube/app/offline/a/af;->a(Lcom/google/android/apps/youtube/app/offline/a/af;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/Collection;)Lcom/google/android/apps/youtube/app/offline/a/ag;

    move-result-object v0

    :goto_1
    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a(Lcom/google/android/apps/youtube/app/offline/a/ag;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "pudl transfer playlist not in database"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->t:Lcom/google/android/apps/youtube/app/offline/a/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/af;->a(Lcom/google/android/apps/youtube/app/offline/a/af;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->v()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public final h(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->p:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final i()J
    .locals 4

    const-string v0, "offline_resync_interval_%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final i(Ljava/lang/String;)Landroid/util/Pair;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final j(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    return-object v0
.end method

.method public final k(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->A(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l(Ljava/lang/String;)Ljava/util/List;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->r(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/n;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/n;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final n(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/h;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/h;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final o(Ljava/lang/String;)I
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->u(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final p(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->l:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->v(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v0

    return-object v0
.end method

.method final q(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->C(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/transfer/k;->a(Ljava/lang/String;I)V

    return-void
.end method

.method final r(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/transfer/k;->a(Ljava/lang/String;)V

    return-void
.end method

.method final s(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " add: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h()Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/a/aa;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/app/offline/a/aa;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/x;B)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method final t(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl event "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " add_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/ab;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/youtube/app/offline/a/ab;-><init>(Ljava/lang/String;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method final u(Ljava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "pudl event "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " status: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h()Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->i()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->j()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v4, Lcom/google/android/apps/youtube/app/offline/a/ae;

    invoke-direct {v4, v3, v0, v1}, Lcom/google/android/apps/youtube/app/offline/a/ae;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/x;ZB)V

    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->j:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/k;->d()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->C(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method final v(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " complete: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h()Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/a/ac;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/app/offline/a/ac;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/x;B)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method final w(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl event "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delete"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/ad;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/youtube/app/offline/a/ad;-><init>(Ljava/lang/String;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method final x(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl event playlist "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " add_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/r;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/youtube/app/offline/a/r;-><init>(Ljava/lang/String;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method final y(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl event playlist "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sync_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/v;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/youtube/app/offline/a/v;-><init>(Ljava/lang/String;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method final z(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl event playlist "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delete"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/f;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/s;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/youtube/app/offline/a/s;-><init>(Ljava/lang/String;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method
