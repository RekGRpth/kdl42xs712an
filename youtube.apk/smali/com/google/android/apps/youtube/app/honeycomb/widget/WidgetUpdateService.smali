.class public Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;
.super Landroid/widget/RemoteViewsService;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private a:Lcom/google/android/apps/youtube/app/e/b;

.field private b:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private c:Lcom/google/android/apps/youtube/app/honeycomb/widget/b;

.field private d:Ljava/util/List;

.field private e:Z

.field private final f:Ljava/util/concurrent/Semaphore;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/widget/b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/widget/b;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->c:Lcom/google/android/apps/youtube/app/honeycomb/widget/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->d:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->f:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->d:Ljava/util/List;

    return-object v0
.end method

.method private a()V
    .locals 7

    const/4 v0, 0x0

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->a:Lcom/google/android/apps/youtube/app/e/b;

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->e:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->f:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/youtube/app/honeycomb/widget/WidgetProvider;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;

    invoke-direct {v3, v5, v5, p0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/c;->b(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v4

    aget v5, v1, v0

    sget v6, Lcom/google/android/youtube/j;->gx:I

    invoke-virtual {v4, v5, v6, v3}, Landroid/widget/RemoteViews;->setRemoteAdapter(IILandroid/content/Intent;)V

    aget v5, v1, v0

    invoke-virtual {v2, v5, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/a;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;)V
    .locals 9

    const/4 v6, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->e:Z

    if-nez v0, :cond_0

    iput-boolean v6, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->e:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v7

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->s()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/an;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v3

    invoke-direct {v1, v8, v3, v0}, Lcom/google/android/apps/youtube/app/an;-><init>(Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/e/b;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/an;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    const/16 v4, 0x8

    const/16 v5, 0x18

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/e/b;-><init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/client/bj;Ljava/util/concurrent/ConcurrentMap;IIZ)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-interface {v8}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->b:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->a:Lcom/google/android/apps/youtube/app/e/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->b:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->e()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/youtube/app/e/b;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->f:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->a:Lcom/google/android/apps/youtube/app/e/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->b:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/youtube/app/e/b;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error response"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->d:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->a()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->d:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->a()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetUpdateService;->c:Lcom/google/android/apps/youtube/app/honeycomb/widget/b;

    return-object v0
.end method
