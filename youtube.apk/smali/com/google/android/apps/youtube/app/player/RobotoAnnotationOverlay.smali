.class public Lcom/google/android/apps/youtube/app/player/RobotoAnnotationOverlay;
.super Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_REGULAR:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/utils/Typefaces;->toTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_LIGHT:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/utils/Typefaces;->toTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/player/RobotoAnnotationOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/player/RobotoAnnotationOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_0
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/player/RobotoAnnotationOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/player/RobotoAnnotationOverlay;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_1
    return-void
.end method
