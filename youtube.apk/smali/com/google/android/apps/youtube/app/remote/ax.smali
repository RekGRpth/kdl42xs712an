.class public final Lcom/google/android/apps/youtube/app/remote/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/apps/youtube/core/aw;

.field private final c:Lcom/google/android/apps/youtube/core/client/bc;

.field private final d:Lcom/google/android/apps/youtube/app/remote/an;

.field private final e:Lcom/google/android/apps/youtube/app/am;

.field private f:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private final g:Lcom/google/android/apps/youtube/core/identity/l;

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/identity/l;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->a:Landroid/os/Handler;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/an;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->d:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->e:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->g:Lcom/google/android/apps/youtube/core/identity/l;

    iput-boolean p7, p0, Lcom/google/android/apps/youtube/app/remote/ax;->h:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/ax;)Lcom/google/android/apps/youtube/core/client/WatchFeature;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->f:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    return-object v0
.end method

.method private a(Landroid/net/Uri;IZ)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->g:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->j()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    move-object v6, v0

    :goto_0
    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ax;->d:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    const/4 v5, 0x0

    move-object v1, p0

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/remote/ay;-><init>(Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/app/remote/RemoteControl;IZB)V

    new-instance v1, Lcom/google/android/apps/youtube/app/a/c;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ax;->c:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/ax;->a:Landroid/os/Handler;

    invoke-direct {v1, v2, v6, v3, v0}, Lcom/google/android/apps/youtube/app/a/c;-><init>(Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/async/af;Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/a/c;->a(Landroid/net/Uri;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->c()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    move-object v6, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/ax;Ljava/util/List;Ljava/util/List;ILcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V
    .locals 7

    const/4 v1, -0x1

    const/4 v4, 0x0

    if-ltz p3, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p3, v0, :cond_3

    move v3, p3

    :goto_0
    if-ne v3, v1, :cond_0

    const-string v0, "No video to play."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    :cond_0
    if-nez p5, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_6

    :cond_1
    invoke-interface {p4, p1, v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Ljava/util/List;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->e:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {p4}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move v3, p3

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Trying to play an invalid index "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Will try to play a different video."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    if-gez p3, :cond_4

    move v3, v4

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    goto :goto_0

    :cond_5
    move v3, v1

    goto :goto_0

    :cond_6
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {p4, v0, v1, v4}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->e:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {p4}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p4}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->q()I

    move-result v3

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move v6, v4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/ax;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->b:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/client/WatchFeature;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/ax;->f:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->d:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Landroid/net/Uri;IZ)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->d:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->d:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0, p1, p3, p2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Landroid/net/Uri;IZ)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;I)V
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ax;->d:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/remote/ax;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/az;

    const/4 v4, 0x1

    move-object v1, p0

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/remote/az;-><init>(Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/app/remote/RemoteControl;IZB)V

    invoke-static {v6, v0}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ax;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/client/bc;->y()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
