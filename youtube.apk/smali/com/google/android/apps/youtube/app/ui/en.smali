.class final Lcom/google/android/apps/youtube/app/ui/en;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/ui/v;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/ei;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/ei;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v2, v3, :cond_3

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c()Lcom/google/android/apps/youtube/app/remote/as;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/apps/youtube/app/remote/as;->c:Z

    if-eqz v2, :cond_4

    :cond_2
    :goto_2
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->j(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->j(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    const-string v0, "Should be flinging a video, but it\'s not loaded yet"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->l(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/ui/eq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/eq;->I()V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->j(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->m(Lcom/google/android/apps/youtube/app/ui/ei;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->l(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/ui/eq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/eq;->I()V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v0, v2, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->e(Lcom/google/android/apps/youtube/app/ui/ei;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/ui/ei;Ljava/lang/String;)V

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->m(Lcom/google/android/apps/youtube/app/ui/ei;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->j(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v2, :cond_c

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNSTARTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v2, :cond_c

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v2, :cond_e

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->g(Lcom/google/android/apps/youtube/app/ui/ei;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/ui/ei;Z)Z

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->l(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/ui/eq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/eq;->I()V

    goto/16 :goto_0

    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/en;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto/16 :goto_0
.end method
