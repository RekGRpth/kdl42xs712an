.class final Lcom/google/android/apps/youtube/app/e/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/e/b;

.field private final b:Lcom/google/android/apps/youtube/common/a/b;

.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/e/b;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/a/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/e/c;->b:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/e/c;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/c;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    move-object v2, p1

    check-cast v2, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "widget gets "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " videos"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->f(Ljava/lang/String;)V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/e/b;->a(Lcom/google/android/apps/youtube/app/e/b;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/e/b;->b(Lcom/google/android/apps/youtube/app/e/b;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/e/c;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/e/b;->c(Lcom/google/android/apps/youtube/app/e/b;)I

    move-result v1

    if-ne v0, v1, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/e/b;->d(Lcom/google/android/apps/youtube/app/e/b;)I

    move-result v1

    if-ge v0, v1, :cond_4

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/e/b;->e(Lcom/google/android/apps/youtube/app/e/b;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/c;->b:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v1, Ljava/lang/Exception;

    const-string v3, "Unable to load any teasers"

    invoke-direct {v1, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/google/android/apps/youtube/app/e/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/e/c;->b:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/e/c;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/e/c;->c:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v6}, Lcom/google/android/apps/youtube/app/e/b;->b(Lcom/google/android/apps/youtube/app/e/b;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/e/d;-><init>(Lcom/google/android/apps/youtube/app/e/b;Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;ILjava/util/List;Ljava/util/Map;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/e/c;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/e/b;->a(Lcom/google/android/apps/youtube/app/e/b;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/e/b;->f(Lcom/google/android/apps/youtube/app/e/b;)Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    invoke-interface {v3, v1, v0}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/e/c;->a:Lcom/google/android/apps/youtube/app/e/b;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/e/b;->f(Lcom/google/android/apps/youtube/app/e/b;)Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;

    invoke-interface {v3, v1, v0}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_2
.end method
