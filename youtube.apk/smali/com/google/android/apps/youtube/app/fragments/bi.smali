.class final Lcom/google/android/apps/youtube/app/fragments/bi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/Analytics;

.field final synthetic b:Lcom/google/android/apps/youtube/app/remote/ax;

.field final synthetic c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

.field final synthetic d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->a:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->b:Lcom/google/android/apps/youtube/app/remote/ax;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->a:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "MdxModalPlay"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->d(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->b(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->b:Lcom/google/android/apps/youtube/app/remote/ax;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->d(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->e(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Ljava/lang/String;ILjava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->f(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->b:Lcom/google/android/apps/youtube/app/remote/ax;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/bi;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->f(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Ljava/util/List;I)V

    goto :goto_0

    :cond_1
    const-string v0, "No videoIds or playlistId. No idea what to play."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
