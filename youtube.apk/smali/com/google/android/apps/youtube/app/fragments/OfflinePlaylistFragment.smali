.class public Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private d:Lcom/google/android/apps/youtube/common/c/a;

.field private e:Lcom/google/android/apps/youtube/common/network/h;

.field private f:Lcom/google/android/apps/youtube/app/offline/p;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/apps/youtube/app/ui/bz;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 36

    sget v2, Lcom/google/android/youtube/l;->at:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v12

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v12, v2}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v8

    new-instance v17, Lcom/google/android/apps/youtube/app/ui/bv;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->f:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/youtube/app/ui/bv;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/offline/p;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/cl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->x()Lcom/google/android/apps/youtube/datalib/innertube/v;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v9

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/youtube/app/ui/cl;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;)V

    new-instance v9, Lcom/google/android/apps/youtube/app/offline/r;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->f:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v18

    move-object v11, v5

    move-object v13, v6

    move-object v14, v8

    invoke-direct/range {v9 .. v18}, Lcom/google/android/apps/youtube/app/offline/r;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/app/ui/hh;)V

    new-instance v18, Lcom/google/android/apps/youtube/app/offline/f;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->e:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->f:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v25, v0

    move-object/from16 v20, v5

    move-object/from16 v21, v12

    move-object/from16 v22, v6

    move-object/from16 v23, v8

    move-object/from16 v26, v17

    invoke-direct/range {v18 .. v26}, Lcom/google/android/apps/youtube/app/offline/f;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aa()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v29

    new-instance v3, Lcom/google/android/apps/youtube/core/player/z;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v3, v4}, Lcom/google/android/apps/youtube/core/player/z;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/core/player/z;)V

    new-instance v19, Lcom/google/android/apps/youtube/app/ui/bz;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->x()Lcom/google/android/apps/youtube/datalib/innertube/v;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->e:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->f:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->g:Ljava/lang/String;

    move-object/from16 v34, v0

    move-object/from16 v30, v2

    move-object/from16 v31, v9

    move-object/from16 v32, v18

    invoke-direct/range {v19 .. v34}, Lcom/google/android/apps/youtube/app/ui/bz;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/cl;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/app/offline/f;Lcom/google/android/apps/youtube/app/ui/hh;Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->h:Lcom/google/android/apps/youtube/app/ui/bz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->h:Lcom/google/android/apps/youtube/app/ui/bz;

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/app/ui/bz;->a(Landroid/view/LayoutInflater;Landroid/view/View;)V

    return-object v35

    :cond_0
    invoke-interface {v12}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->gh:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->H()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->e:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->Z()Lcom/google/android/apps/youtube/app/offline/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->f:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "playlist_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->g:Ljava/lang/String;

    return-void
.end method

.method public final d()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->h:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/bz;->a()V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->h:Lcom/google/android/apps/youtube/app/ui/bz;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/bz;->b()V

    return-void
.end method

.method public handleOfflinePlaylistDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/s;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/s;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    :cond_0
    return-void
.end method
