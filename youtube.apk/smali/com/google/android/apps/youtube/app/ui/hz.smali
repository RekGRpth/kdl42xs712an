.class final Lcom/google/android/apps/youtube/app/ui/hz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/hj;

.field private final b:Lcom/google/android/apps/youtube/app/ui/LikeAction;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->b:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "Error rating"

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hz;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->d(Lcom/google/android/apps/youtube/app/ui/hj;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    invoke-static {v3, v1, v4}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    invoke-static {v1, v0, v4}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;Landroid/view/View;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/hn;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hz;->b:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/LikeAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hz;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->c(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/app/ui/hy;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hz;->b:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hy;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    :cond_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    sget v1, Lcom/google/android/youtube/p;->eO:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    sget v1, Lcom/google/android/youtube/p;->eL:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hz;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    sget v1, Lcom/google/android/youtube/p;->eQ:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
