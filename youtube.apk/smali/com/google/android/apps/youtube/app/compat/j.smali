.class public final Lcom/google/android/apps/youtube/app/compat/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/Menu;

.field private c:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Menu;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Menu;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->c:Landroid/util/SparseArray;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-interface {p2, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/compat/j;->c:Landroid/util/SparseArray;

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/compat/j;->a(Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/compat/j;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->c:Landroid/util/SparseArray;

    return-object v0
.end method

.method private a(Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/u;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/u;-><init>(Landroid/content/Context;Landroid/view/MenuItem;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->c:Landroid/util/SparseArray;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-object v0

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/t;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/t;-><init>(Landroid/content/Context;Landroid/view/MenuItem;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/app/compat/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/w;-><init>(Landroid/content/Context;Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->a(Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Landroid/view/Menu;->addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/compat/j;->a(Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIII)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/compat/j;->a(Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIILjava/lang/CharSequence;)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/compat/j;->a(Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/compat/j;->a(Landroid/view/MenuItem;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    return-void
.end method

.method public final a(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    return-void
.end method

.method public final a(IZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/Menu;->setGroupCheckable(IZZ)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->setQwertyMode(Z)V

    return-void
.end method

.method public final a(II)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->performIdentifierAction(II)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->isShortcutKey(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/KeyEvent;I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/view/SubMenu;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v1, p1}, Landroid/view/Menu;->addSubMenu(I)Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/compat/k;-><init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final b(IIII)Landroid/view/SubMenu;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/Menu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/compat/k;-><init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final b(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/Menu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/compat/k;-><init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v1, p1}, Landroid/view/Menu;->addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/compat/k;-><init>(Lcom/google/android/apps/youtube/app/compat/j;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->close()V

    return-void
.end method

.method public final b(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    return-void
.end method

.method public final c(I)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/compat/q;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->size()I

    move-result v0

    return v0
.end method

.method public final d(I)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/j;->c:Landroid/util/SparseArray;

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/compat/q;

    goto :goto_0
.end method

.method public final e(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->removeGroup(I)V

    return-void
.end method

.method public final f(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->removeItem(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/j;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    return-void
.end method
