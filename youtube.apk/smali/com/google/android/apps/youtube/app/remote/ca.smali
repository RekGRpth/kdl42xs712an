.class final Lcom/google/android/apps/youtube/app/remote/ca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/logic/c;


# instance fields
.field final synthetic a:Lcom/google/android/apps/ytremote/model/SsdpId;

.field final synthetic b:Lcom/google/android/apps/youtube/app/remote/ck;

.field final synthetic c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/ytremote/model/SsdpId;Lcom/google/android/apps/youtube/app/remote/ck;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/ca;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/ca;->a:Lcom/google/android/apps/ytremote/model/SsdpId;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/remote/ca;->b:Lcom/google/android/apps/youtube/app/remote/ck;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ca;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->h(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ca;->a:Lcom/google/android/apps/ytremote/model/SsdpId;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ca;->a:Lcom/google/android/apps/ytremote/model/SsdpId;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/model/SsdpId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ca;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->h(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ca;->a:Lcom/google/android/apps/ytremote/model/SsdpId;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ca;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    new-instance v2, Landroid/util/Pair;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/ca;->a:Lcom/google/android/apps/ytremote/model/SsdpId;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/ca;->b:Lcom/google/android/apps/youtube/app/remote/ck;

    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ca;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ca;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/cb;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/app/remote/cb;-><init>(Lcom/google/android/apps/youtube/app/remote/ca;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
