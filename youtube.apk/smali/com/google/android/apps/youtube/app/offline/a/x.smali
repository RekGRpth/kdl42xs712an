.class final Lcom/google/android/apps/youtube/app/offline/a/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/transfer/h;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/offline/a/f;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/offline/a/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/offline/a/f;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/x;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl transfer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " added"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 0

    return-void
.end method

.method public final c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl transfer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " progress "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->f:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->e(Lcom/google/android/apps/youtube/app/offline/a/f;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/y;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/y;-><init>(Lcom/google/android/apps/youtube/app/offline/a/x;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl transfer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " status "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->e(Lcom/google/android/apps/youtube/app/offline/a/f;)Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/z;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/z;-><init>(Lcom/google/android/apps/youtube/app/offline/a/x;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final e(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl transfer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->d(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/offline/a/af;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/af;->c(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/a/ag;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b(Lcom/google/android/apps/youtube/app/offline/a/ag;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b(Lcom/google/android/apps/youtube/app/offline/a/ag;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/offline/a/f;->d(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/offline/a/af;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->c(Lcom/google/android/apps/youtube/app/offline/a/ag;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/app/offline/a/af;->b(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final m_()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->c(Lcom/google/android/apps/youtube/app/offline/a/f;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
