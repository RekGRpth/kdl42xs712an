.class public final Lcom/google/android/apps/youtube/app/ui/presenter/cg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/j;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final d:Lcom/google/android/apps/youtube/app/ui/v;

.field private final e:Lcom/google/android/apps/youtube/app/ui/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->d:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->e:Lcom/google/android/apps/youtube/app/ui/a;

    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/android/apps/youtube/uilib/a/g;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->d:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;->e:Lcom/google/android/apps/youtube/app/ui/a;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/presenter/cd;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/a;)V

    return-object v0
.end method
