.class final Lcom/google/android/apps/youtube/app/ui/presenter/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/i;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:I

.field private final c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

.field private final d:Landroid/widget/FrameLayout;

.field private final e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/player/internal/b/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a:Landroid/content/res/Resources;

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/d;->a:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->b:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->g:Z

    sget v0, Lcom/google/android/youtube/l;->o:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    sget v1, Lcom/google/android/youtube/j;->eu:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    sget v1, Lcom/google/android/youtube/j;->T:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->d:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    sget v1, Lcom/google/android/youtube/j;->aD:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->f:Landroid/view/View;

    return-void
.end method

.method private a(FF)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/g;->k:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    float-to-int v3, p1

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/youtube/g;->l:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setPadding(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/g;->g:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v3, v0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/youtube/g;->h:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v0, v4, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setSelectorMargins(IIII)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/youtube/g;->q:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setSelector(I)V

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/uilib/a/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/uilib/a/f;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->d:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/youtube/h;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/g;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a(FF)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setSelectorDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/uilib/a/f;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->d:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/youtube/h;->n:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/g;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a(FF)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/uilib/a/f;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->d:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/youtube/h;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    invoke-direct {p0, v2, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a(FF)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->d:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/youtube/h;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    invoke-direct {p0, v2, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/p;->a(FF)V

    goto :goto_1
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->g:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->c:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->d:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->d:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->f:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->d:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/p;->f:Landroid/view/View;

    return-void
.end method
