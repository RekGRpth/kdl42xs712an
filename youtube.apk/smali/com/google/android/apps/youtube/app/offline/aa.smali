.class public final Lcom/google/android/apps/youtube/app/offline/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/ce;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/ce;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Lcom/google/android/apps/youtube/core/identity/l;

.field private final d:Lcom/google/android/apps/youtube/datalib/offline/n;

.field private final e:Lcom/google/android/apps/youtube/core/offline/store/q;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/ce;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/q;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/ce;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->a:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->b:Ljava/util/concurrent/Executor;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->d:Lcom/google/android/apps/youtube/datalib/offline/n;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->e:Lcom/google/android/apps/youtube/core/offline/store/q;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/aa;)Lcom/google/android/apps/youtube/core/identity/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->c:Lcom/google/android/apps/youtube/core/identity/l;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/offline/aa;)Lcom/google/android/apps/youtube/core/offline/store/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->e:Lcom/google/android/apps/youtube/core/offline/store/q;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->a:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/ce;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->d:Lcom/google/android/apps/youtube/datalib/offline/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/offline/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/ab;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/offline/ab;-><init>(Lcom/google/android/apps/youtube/app/offline/aa;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->a:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/ce;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/aa;->a:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/ce;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
