.class abstract Lcom/google/android/apps/youtube/app/adapter/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field protected final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/e;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/e;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->D:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/e;->b:Landroid/widget/TextView;

    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/e;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/adapter/e;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/e;->b:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/p;->cl:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/e;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/e;->a:Landroid/view/View;

    return-object v0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/adapter/e;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/e;->b:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/p;->ga:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/e;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/e;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/Object;)Z
.end method

.method protected abstract b(Ljava/lang/Object;)Z
.end method
