.class public final Lcom/google/android/apps/youtube/app/ui/gt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/u;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/bc;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/app/am;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Lcom/google/android/apps/youtube/common/a/b;

.field private final h:Lcom/google/android/apps/youtube/common/a/b;

.field private final i:Lcom/google/android/apps/youtube/common/a/b;

.field private final j:Landroid/content/res/Resources;

.field private final k:Lcom/google/android/apps/youtube/common/c/a;

.field private final l:Landroid/view/View;

.field private m:Lcom/google/android/apps/youtube/common/a/d;

.field private n:Lcom/google/android/apps/youtube/core/identity/UserProfile;

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/am;Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->a:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/gt;->c:Lcom/google/android/apps/youtube/app/am;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/ui/gt;->l:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p5, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gv;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/gv;-><init>(Lcom/google/android/apps/youtube/app/ui/gt;B)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->g:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gw;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/gw;-><init>(Lcom/google/android/apps/youtube/app/ui/gt;B)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->h:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gx;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/gx;-><init>(Lcom/google/android/apps/youtube/app/ui/gt;B)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->i:Lcom/google/android/apps/youtube/common/a/b;

    sget v0, Lcom/google/android/youtube/j;->ah:I

    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->d:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->J:I

    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->e:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->fV:I

    invoke-virtual {p5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->f:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->j:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->E()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->k:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gu;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/gu;-><init>(Lcom/google/android/apps/youtube/app/ui/gt;B)V

    invoke-virtual {p5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/gt;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->m:Lcom/google/android/apps/youtube/common/a/d;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/gt;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gt;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/gt;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 7

    const/4 v5, 0x1

    const/4 v6, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->n:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->j:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/p;->hl:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/gt;->g:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->j:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/o;->d:I

    iget v3, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadedCount:I

    new-array v4, v5, [Ljava/lang/Object;

    iget v5, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadedCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gt;->f()V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gt;->e()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/gt;)Lcom/google/android/apps/youtube/common/a/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->h:Lcom/google/android/apps/youtube/common/a/b;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/gt;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->a:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/gt;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method private e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->j:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/h;->am:I

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/gt;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->n:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->k:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/b;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/client/b;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->c:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->n:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/am;->a(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method private f()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->n:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->l:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->i:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->a:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->o:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gt;->f()V

    return-void
.end method

.method public final c()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->m:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->m:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->m:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->o:Z

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gt;->n:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gt;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gt;->f()V

    return-void
.end method
