.class public abstract Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/transfer/l;


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/core/offline/store/i;

.field protected final b:Lcom/google/android/apps/youtube/datalib/innertube/ah;

.field protected final c:Lcom/google/android/apps/youtube/core/player/w;

.field protected final d:Lcom/google/android/apps/youtube/datalib/innertube/ag;

.field protected final e:Lcom/google/android/apps/youtube/common/cache/a;

.field protected final f:Ljava/lang/String;

.field protected final g:Ljava/lang/String;

.field protected final h:Lcom/google/android/apps/youtube/common/e/b;

.field private final i:Lcom/google/android/apps/youtube/app/offline/transfer/c;

.field private final j:Lcom/google/android/apps/youtube/app/offline/transfer/a;

.field private final k:I

.field private final l:Lcom/google/android/apps/youtube/core/async/af;

.field private final m:Ljava/io/File;

.field private n:J

.field private volatile o:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/offline/store/i;Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/datalib/innertube/ag;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/app/offline/transfer/c;ILcom/google/android/apps/youtube/core/async/af;Ljava/io/File;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->c:Lcom/google/android/apps/youtube/core/player/w;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->d:Lcom/google/android/apps/youtube/datalib/innertube/ag;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->e:Lcom/google/android/apps/youtube/common/cache/a;

    iput-object p6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->h:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v0, p7, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-static {p7}, Lcom/google/android/apps/youtube/core/utils/p;->c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->i:Lcom/google/android/apps/youtube/app/offline/transfer/c;

    iput p9, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->k:I

    iput-object p10, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->l:Lcom/google/android/apps/youtube/core/async/af;

    iput-object p11, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->m:Ljava/io/File;

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/transfer/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/offline/transfer/a;-><init>(Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->j:Lcom/google/android/apps/youtube/app/offline/transfer/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->j:Lcom/google/android/apps/youtube/app/offline/transfer/a;

    invoke-virtual {p8, v0}, Lcom/google/android/apps/youtube/app/offline/transfer/c;->a(Lcom/google/android/apps/youtube/app/offline/transfer/d;)V

    return-void
.end method

.method private a(Landroid/net/Uri;)J
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->l:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->buildUpon()Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Landroid/net/Uri;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->a(J)Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/legacy/model/u;ZLcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/legacy/model/t;
    .locals 8

    const/4 v0, 0x0

    if-eqz p2, :cond_5

    if-eqz p3, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->c()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getFormatStreamByItag(I)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getLastModified()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getLastModified()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;I)Z

    :cond_3
    if-eqz p3, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAudioOnlyStream()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p3}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)Z

    goto :goto_1

    :cond_4
    iget v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->k:I

    invoke-virtual {p1, v1, p4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getOfflineVideoStream(ILcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/t;JJ)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->f()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->e()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->i:Lcom/google/android/apps/youtube/app/offline/transfer/c;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/app/offline/transfer/c;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl task["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] start stream<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "> download"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->i:Lcom/google/android/apps/youtube/app/offline/transfer/c;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    const-wide/16 v2, 0x0

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/offline/transfer/c;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;JJ)V

    goto :goto_0
.end method

.method private b(J)V
    .locals 10

    iget-wide v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->n:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->m:Ljava/io/File;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->m:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "offline.log"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s,%s,%s\n"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v5}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->n:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_3
    throw v0

    :catch_3
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v1

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method private c()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->k:I

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->AMODO_ONLY:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->getQualityValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl task["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] cancel"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->o:Z

    and-int/lit16 v1, p1, 0x80

    if-nez v1, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->i:Lcom/google/android/apps/youtube/app/offline/transfer/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/transfer/c;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract a(J)V
.end method

.method protected abstract a(JJ)V
.end method

.method protected abstract a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
.end method

.method protected abstract a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
.end method

.method protected abstract a()Z
.end method

.method protected abstract b()V
.end method

.method public final run()V
    .locals 9

    const-wide/16 v1, 0x0

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "pudl task["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] start"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->e:Lcom/google/android/apps/youtube/common/cache/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-eqz v3, :cond_0

    if-nez v0, :cond_7

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/ah;->a()Lcom/google/android/apps/youtube/datalib/innertube/ak;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->c(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a:[B

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a([B)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->c:Lcom/google/android/apps/youtube/core/player/w;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->d:Lcom/google/android/apps/youtube/datalib/innertube/ag;

    invoke-interface {v3, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ag;->a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ah;->a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    :try_end_1
    .catch Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    :try_end_2
    .catch Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask$PlayerResponseSaveException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl task["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] received actionable playability error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v3, :cond_2

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    const-string v0, "Playability error"

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->CANNOT_OFFLINE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl task["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed to retrieve player response"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "Cannot retrieve player response from the server."

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->NETWORK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl task["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] error while pinning video"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "Error encountered while pinning the video"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->FAILED_UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl task["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed to save player response."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    const-string v1, "Fail to save playerResponse"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->FAILED_UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    goto :goto_1

    :catch_3
    move-exception v0

    const-string v1, "Error trying to write to local disk."

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_WRITE_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getOfflineState()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->c()Z

    move-result v4

    if-nez v4, :cond_6

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pudl task["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] received offline state error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez v3, :cond_5

    const-string v0, "null"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    const-string v0, "Offline state error"

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->CANNOT_OFFLINE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->e:Lcom/google/android/apps/youtube/common/cache/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v3

    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->n:J
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :try_start_5
    sget-object v4, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->SERVER_EXPERIMENT:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayerConfig(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5, v0}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/legacy/model/u;ZLcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->c()Z

    move-result v6

    if-nez v6, :cond_8

    if-nez v5, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v3, "No valid video stream to offline."

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_4
    move-exception v0

    :goto_3
    :try_start_6
    instance-of v3, v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferTimedOutException;

    if-eqz v3, :cond_f

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->NETWORK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v3, v0, v4}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_4
    :try_start_7
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b(J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_1

    :cond_8
    const/4 v6, 0x1

    :try_start_8
    invoke-direct {p0, v3, v4, v6, v0}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/legacy/model/u;ZLcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v0

    new-instance v8, Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    invoke-direct {v8, v5, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/t;Lcom/google/android/apps/youtube/datalib/legacy/model/t;)V

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->f()J

    move-result-wide v3

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->g()J
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-wide v6

    :try_start_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->j:Lcom/google/android/apps/youtube/app/offline/transfer/a;

    invoke-static {v0, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/a;->a(Lcom/google/android/apps/youtube/app/offline/transfer/a;J)J

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(J)V

    invoke-virtual {p0, v3, v4, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(JJ)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->n:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->j:Lcom/google/android/apps/youtube/app/offline/transfer/a;

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/a;->b(Lcom/google/android/apps/youtube/app/offline/transfer/a;J)J

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->c()Z

    move-result v0

    if-nez v0, :cond_9

    if-nez v1, :cond_9

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid video stream to offline."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_5
    move-exception v0

    move-wide v1, v6

    goto :goto_3

    :cond_9
    if-eqz v1, :cond_a

    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->f()J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/t;JJ)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->f()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->j:Lcom/google/android/apps/youtube/app/offline/transfer/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/youtube/app/offline/transfer/a;->b(Lcom/google/android/apps/youtube/app/offline/transfer/a;J)J

    :cond_a
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->o:Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v0, :cond_b

    :try_start_a
    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b(J)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_1

    :cond_b
    :try_start_b
    invoke-virtual {v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    if-nez v1, :cond_c

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid audio stream to offline."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :catch_6
    move-exception v0

    :goto_5
    :try_start_c
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pudl task["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] error while downloading video"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "Error encountered while downloading the video"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_WRITE_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :try_start_d
    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b(J)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1

    goto/16 :goto_1

    :cond_c
    if-eqz v1, :cond_d

    const-wide/16 v2, 0x0

    :try_start_e
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->f()J

    move-result-wide v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/t;JJ)V

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->o:Z
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    if-eqz v0, :cond_e

    :try_start_f
    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b(J)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1

    goto/16 :goto_1

    :cond_e
    :try_start_10
    invoke-virtual {p0, v6, v7, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(JJ)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :try_start_11
    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b(J)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_1

    goto/16 :goto_1

    :cond_f
    :try_start_12
    instance-of v3, v0, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    if-eqz v3, :cond_10

    const-string v3, "Error trying to read from network."

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->NETWORK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v3, v0, v4}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_4

    :catchall_0
    move-exception v0

    move-wide v6, v1

    :goto_6
    :try_start_13
    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->b(J)V

    throw v0
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_1

    :cond_10
    :try_start_14
    instance-of v3, v0, Lcom/google/android/exoplayer/upstream/FileDataSource$FileDataSourceException;

    if-eqz v3, :cond_11

    const-string v3, "Error trying to read from local disk."

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v3, v0, v4}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    goto/16 :goto_4

    :cond_11
    instance-of v3, v0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;

    if-eqz v3, :cond_12

    const-string v3, "Error trying to write to local disk."

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_WRITE_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v3, v0, v4}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    goto/16 :goto_4

    :cond_12
    const-string v3, "Error trying to download video for offline."

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->FAILED_UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {p0, v3, v0, v4}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_4

    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_7
    move-exception v0

    move-wide v6, v1

    goto :goto_5
.end method
