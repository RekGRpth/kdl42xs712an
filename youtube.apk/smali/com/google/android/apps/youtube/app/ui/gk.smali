.class public final Lcom/google/android/apps/youtube/app/ui/gk;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Landroid/content/Context;I[Ljava/lang/CharSequence;I)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/gk;->a:Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/gk;->b:I

    iput p5, p0, Lcom/google/android/apps/youtube/app/ui/gk;->b:I

    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gk;->a:Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a(Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->bi:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gk;->a:Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->b(Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gk;->a:Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->b(Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;)[I

    move-result-object v0

    array-length v0, v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gk;->a:Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->b(Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;)[I

    move-result-object v0

    aget v0, v0, p1

    if-nez v0, :cond_2

    sget v0, Lcom/google/android/youtube/j;->dL:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/h;->av:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    sget v0, Lcom/google/android/youtube/j;->at:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/gk;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/gk;->b:I

    if-ne p1, v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    :cond_1
    return-object v2

    :cond_2
    sget v0, Lcom/google/android/youtube/j;->dL:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gk;->a:Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->b(Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;)[I

    move-result-object v1

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method
