.class public final Lcom/google/android/apps/youtube/app/ui/presenter/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/uilib/a/i;

.field private final d:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final e:Landroid/content/res/Resources;

.field private final f:Landroid/widget/LinearLayout;

.field private final g:Landroid/widget/LinearLayout;

.field private h:Landroid/widget/FrameLayout;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/LinearLayout;

.field private final l:Landroid/view/LayoutInflater;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:I

.field private q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/datalib/d/a;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->c:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->d:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->e:Landroid/content/res/Resources;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->i:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/j;->X:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v0, Lcom/google/android/youtube/j;->V:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->m:Z

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->n:Z

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->o:Z

    invoke-interface {p3, v1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    return-void
.end method

.method private a()V
    .locals 9

    const/4 v5, 0x1

    const/4 v8, -0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->k()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bG:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bE:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v8, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->e:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->e:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/k;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ax;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v6, Lcom/google/android/youtube/l;->bF:I

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ax;->a()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ax;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v7

    invoke-direct {p0, v6, v1, v7}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a(Landroid/view/View;ILcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ax;->c()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/m;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/h;Lcom/google/a/a/a/a/kz;)V

    invoke-virtual {v6, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v8, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Landroid/view/View;ILcom/google/android/apps/youtube/datalib/innertube/model/ap;)V
    .locals 3

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/gr;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v1, p3}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->m:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/h;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->m:Z

    return p1
.end method

.method private b()V
    .locals 11

    const/4 v10, 0x0

    const/16 v2, 0x8

    const/4 v9, 0x5

    const/4 v3, 0x0

    const/4 v8, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->m:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->o:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->g()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bG:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bE:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v9, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v2, v0, :cond_1

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bE:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v6, Lcom/google/android/youtube/l;->bH:I

    invoke-virtual {v1, v6, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ay;->a()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ay;->b()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ay;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v7

    invoke-direct {p0, v6, v1, v7}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a(Landroid/view/View;ILcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ay;->d()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->ga:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v7, Lcom/google/android/apps/youtube/app/ui/presenter/k;

    invoke-direct {v7, p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/k;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/h;Lcom/google/a/a/a/a/kz;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->i()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bG:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->h()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bE:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move v2, v3

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v9, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v2, v0, :cond_3

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bE:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/av;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v6, Lcom/google/android/youtube/l;->bC:I

    invoke-virtual {v1, v6, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/av;->a()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/youtube/j;->gz:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/av;->b()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/av;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v7

    invoke-direct {p0, v6, v1, v7}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a(Landroid/view/View;ILcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/av;->d()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->p:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v7, Lcom/google/android/apps/youtube/app/ui/presenter/l;

    invoke-direct {v7, p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/l;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/h;Lcom/google/a/a/a/a/kz;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a()V

    iput-boolean v8, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->o:Z

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->e:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v8, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->j:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->h:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/youtube/j;->Z:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0, v1, v1, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->i:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->x:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_3
    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->be:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->e:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v8, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->h:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/youtube/j;->Z:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->i:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->E:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->be:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/presenter/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->b()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/presenter/h;)Lcom/google/android/apps/youtube/datalib/d/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->d:Lcom/google/android/apps/youtube/datalib/d/a;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->e:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->p:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->c:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->n:Z

    if-nez v0, :cond_1

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->b()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->m:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v3, Lcom/google/android/youtube/j;->W:I

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->l:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/youtube/l;->bD:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->ab:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->U:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->aa:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->i:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->Y:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->h:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->h:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/i;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/i;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/h;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->f:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->Q:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/aw;

    move-result-object v3

    sget v1, Lcom/google/android/youtube/j;->cc:I

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/aw;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v4

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a(Landroid/view/View;ILcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    sget v1, Lcom/google/android/youtube/j;->fJ:I

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/aw;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v4

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a(Landroid/view/View;ILcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    sget v1, Lcom/google/android/youtube/j;->H:I

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/aw;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v4

    invoke-direct {p0, v0, v1, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a(Landroid/view/View;ILcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    sget v1, Lcom/google/android/youtube/j;->K:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/aw;->d()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/j;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/j;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/h;Lcom/google/android/apps/youtube/datalib/innertube/model/aw;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->n:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->o:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->a()V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/h;->b()V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->n:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->e:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->p:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/h;->c:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto/16 :goto_1
.end method
