.class public abstract Lcom/google/android/apps/youtube/app/ui/presenter/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field protected final a:Landroid/view/View;

.field protected final b:Landroid/app/Activity;

.field protected final c:Lcom/google/android/apps/youtube/core/client/bj;

.field protected final d:Landroid/content/res/Resources;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;

.field private final i:Landroid/view/View;

.field private final j:Lcom/google/android/apps/youtube/app/ui/presenter/f;

.field private final k:Lcom/google/android/apps/youtube/app/ui/presenter/g;

.field private final l:Lcom/google/android/apps/youtube/app/d/r;


# direct methods
.method protected constructor <init>(Landroid/view/View;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/presenter/f;Lcom/google/android/apps/youtube/app/ui/presenter/g;Lcom/google/android/apps/youtube/app/d/r;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->a:Landroid/view/View;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->b:Landroid/app/Activity;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->c:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/presenter/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->j:Lcom/google/android/apps/youtube/app/ui/presenter/f;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/presenter/g;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->k:Lcom/google/android/apps/youtube/app/ui/presenter/g;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/d/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->l:Lcom/google/android/apps/youtube/app/d/r;

    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->d:Landroid/content/res/Resources;

    sget v0, Lcom/google/android/youtube/j;->y:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->e:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->ax:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->f:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->ay:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->g:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->B:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->h:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;

    sget v0, Lcom/google/android/youtube/j;->ba:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->i:Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/b;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/presenter/b;)Lcom/google/android/apps/youtube/app/ui/presenter/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->j:Lcom/google/android/apps/youtube/app/ui/presenter/f;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/presenter/b;)Lcom/google/android/apps/youtube/app/d/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->l:Lcom/google/android/apps/youtube/app/d/r;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/presenter/b;)Lcom/google/android/apps/youtube/app/ui/presenter/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->k:Lcom/google/android/apps/youtube/app/ui/presenter/g;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->e:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/distiller/model/c;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->f:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/distiller/model/c;->f()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->g:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/distiller/model/c;->e()Ljava/util/Date;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->d:Landroid/content/res/Resources;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/utils/ag;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/distiller/model/c;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->i:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/c;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/c;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/b;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->a:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/d;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/d;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/b;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->h:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->h:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/e;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/e;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/b;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/g;->ae:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/c;->a(I)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->h:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->c:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->h:Lcom/google/android/apps/youtube/app/ui/SelectorOnTopImageView;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/apps/youtube/app/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->a:Landroid/view/View;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/b;->i:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/b;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/distiller/model/c;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
