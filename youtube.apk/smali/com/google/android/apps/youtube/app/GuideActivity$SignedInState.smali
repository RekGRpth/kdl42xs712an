.class public final enum Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

.field public static final enum NEW:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

.field public static final enum SIGNED_IN:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

.field public static final enum SIGNED_OUT:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->NEW:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    new-instance v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    const-string v1, "SIGNED_IN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->SIGNED_IN:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    new-instance v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    const-string v1, "SIGNED_OUT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->SIGNED_OUT:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->NEW:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->SIGNED_IN:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->SIGNED_OUT:Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->$VALUES:[Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->$VALUES:[Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/GuideActivity$SignedInState;

    return-object v0
.end method
