.class public final Lcom/google/android/apps/youtube/app/ui/presenter/ca;
.super Lcom/google/android/apps/youtube/uilib/a/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/widget/LinearLayout;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final g:Landroid/widget/RelativeLayout;

.field private final h:Landroid/view/View;

.field private final i:Landroid/content/Context;

.field private final j:Lcom/google/android/apps/youtube/app/ui/v;

.field private final k:Lcom/google/android/apps/youtube/uilib/a/i;

.field private final l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 3

    invoke-direct {p0, p4, p3}, Lcom/google/android/apps/youtube/uilib/a/a;-><init>(Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/uilib/a/i;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->i:Landroid/content/Context;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->k:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->j:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/g;->F:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;F)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->l:I

    sget v0, Lcom/google/android/youtube/l;->z:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->y:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->aK:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-direct {v1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->f:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->g:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->aE:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a:Landroid/widget/LinearLayout;

    invoke-interface {p3, v0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/af;)Landroid/view/View;
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/a/a;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->i:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    sget v2, Lcom/google/android/youtube/k;->g:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->f()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->g()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->f:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->j:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->h:Landroid/view/View;

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->k:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->g()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/af;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/af;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/af;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/ca;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/af;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
