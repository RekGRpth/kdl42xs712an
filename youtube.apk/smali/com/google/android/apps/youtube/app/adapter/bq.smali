.class public Lcom/google/android/apps/youtube/app/adapter/bq;
.super Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/apps/youtube/app/adapter/bq;
    .locals 2

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/bq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/apps/youtube/app/adapter/bq;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 2

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/bo;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/h;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/youtube/app/adapter/bo;-><init>(Lcom/google/android/apps/youtube/app/adapter/h;Landroid/view/View;)V

    return-object v1
.end method

.method protected final a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method protected a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    :goto_1
    const/4 v1, 0x0

    invoke-interface {p3, v0, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;

    goto :goto_1

    :cond_3
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_2
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/bq;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->sdThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic c(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic d(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    return-object v0
.end method
