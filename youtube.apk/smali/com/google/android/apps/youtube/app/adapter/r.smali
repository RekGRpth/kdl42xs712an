.class final Lcom/google/android/apps/youtube/app/adapter/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

.field private final b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

.field private final c:Lcom/google/android/apps/youtube/app/adapter/s;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/app/adapter/s;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/adapter/r;->c:Lcom/google/android/apps/youtube/app/adapter/s;

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/app/adapter/r;->d:Z

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/r;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ignoring click due to an error within processing the user profile request: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/r;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/r;->c:Lcom/google/android/apps/youtube/app/adapter/s;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/r;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/r;->c:Lcom/google/android/apps/youtube/app/adapter/s;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/r;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/r;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->g(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->j(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/n;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/adapter/r;->c:Lcom/google/android/apps/youtube/app/adapter/s;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/adapter/r;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/youtube/app/adapter/n;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->m(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->k(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/r;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->selfUri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/r;->c:Lcom/google/android/apps/youtube/app/adapter/s;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/r;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-virtual {v1, v2, v3, p2, v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/core/identity/UserProfile;Landroid/net/Uri;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->g(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->j(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/q;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/r;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/adapter/r;->c:Lcom/google/android/apps/youtube/app/adapter/s;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/adapter/r;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-direct {v3, v4, v5, v6, p2}, Lcom/google/android/apps/youtube/app/adapter/q;-><init>(Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/apps/youtube/app/adapter/s;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->g(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
