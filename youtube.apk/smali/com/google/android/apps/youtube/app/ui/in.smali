.class final Lcom/google/android/apps/youtube/app/ui/in;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/is;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/ij;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/ij;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/in;->a:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aJ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aH:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eu:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/io;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/ui/io;-><init>(Lcom/google/android/apps/youtube/app/ui/in;Lcom/google/android/apps/youtube/app/ui/ij;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method public final a(ZLcom/google/android/apps/youtube/datalib/legacy/model/x;)V
    .locals 0

    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->a:Lcom/google/android/apps/youtube/app/ui/ij;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/in;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->c(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 8

    const/16 v3, 0x8

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    sget v1, Lcom/google/android/youtube/h;->e:I

    sget v0, Lcom/google/android/youtube/h;->o:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/in;->e:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/in;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    invoke-virtual {v6, v7, v7, v1, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/in;->a:Lcom/google/android/apps/youtube/app/ui/ij;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/in;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->c(Landroid/view/View;)V

    return-void

    :cond_0
    sget v1, Lcom/google/android/youtube/h;->d:I

    sget v0, Lcom/google/android/youtube/h;->m:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/in;->e:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/in;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
