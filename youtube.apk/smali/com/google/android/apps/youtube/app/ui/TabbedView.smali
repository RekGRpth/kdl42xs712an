.class public Lcom/google/android/apps/youtube/app/ui/TabbedView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I

.field private static final e:I


# instance fields
.field private A:Lcom/google/android/apps/youtube/app/ui/gp;

.field private B:I

.field private C:Lcom/google/android/apps/youtube/app/ui/gn;

.field private D:I

.field private E:F

.field private F:Landroid/graphics/Rect;

.field private G:Landroid/graphics/Paint;

.field private f:Landroid/content/Context;

.field private g:Ljava/util/ArrayList;

.field private h:Landroid/widget/HorizontalScrollView;

.field private i:Landroid/widget/LinearLayout;

.field private j:Landroid/support/v4/view/ViewPager;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Landroid/view/View$OnClickListener;

.field private z:Lcom/google/android/apps/youtube/app/ui/go;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lcom/google/android/youtube/f;->t:I

    sput v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:I

    const-string v0, "#e62117"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:I

    const-string v0, "#33000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:I

    const-string v0, "#333333"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:I

    const-string v0, "#E1E1E1"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;F)F
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->E:F

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->D:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 7

    const/4 v6, -0x2

    const/16 v3, 0xa

    const/4 v2, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/16 v1, 0x30

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->l:I

    sget v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a:I

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->m:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->n:I

    sget v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:I

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->o:I

    const/16 v1, 0x30

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->k:I

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->p:I

    sget v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:I

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->q:I

    sget v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:I

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->r:I

    const/16 v1, 0x14

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->s:I

    const/16 v1, 0x10

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->t:I

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->u:I

    sget v1, Lcom/google/android/apps/youtube/app/ui/TabbedView;->e:I

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->v:I

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->w:I

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->x:I

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/gl;-><init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setOrientation(I)V

    new-instance v0, Landroid/widget/HorizontalScrollView;

    invoke-direct {v0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/HorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->setFillViewport(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->l:I

    invoke-direct {v2, v6, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->G:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->G:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Landroid/support/v4/view/ViewPager;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gn;

    invoke-direct {v0, p0, v4}, Lcom/google/android/apps/youtube/app/ui/gn;-><init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->C:Lcom/google/android/apps/youtube/app/ui/gn;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->C:Lcom/google/android/apps/youtube/app/ui/gn;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/aj;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gm;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/gm;-><init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bx;)V

    iput v5, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/TabbedView;Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/gq;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/gq;->a:Landroid/widget/TextView;

    if-ne v0, p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(I)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/TabbedView;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->k:I

    return v0
.end method

.method private b(I)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Lcom/google/android/apps/youtube/app/ui/gp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Lcom/google/android/apps/youtube/app/ui/gp;

    :cond_0
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->z:Lcom/google/android/apps/youtube/app/ui/go;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->z:Lcom/google/android/apps/youtube/app/ui/go;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/ui/go;->a(I)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/TabbedView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b(I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/widget/HorizontalScrollView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/TabbedView;)Landroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method


# virtual methods
.method public final a(II)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Ljava/lang/CharSequence;Landroid/view/View;)V

    return-object v0
.end method

.method public final a()V
    .locals 2

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->D:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->E:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->C:Lcom/google/android/apps/youtube/app/ui/gn;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/gn;->b()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/view/View;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->v:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->u:I

    invoke-direct {v1, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->x:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->w:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->r:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->t:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    sget-object v1, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_REGULAR:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/utils/Typefaces;->toTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setLines(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget v3, Lcom/google/android/youtube/d;->a:I

    invoke-virtual {v2, v3, v1, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->s:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->s:I

    invoke-virtual {v0, v1, v5, v2, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_3

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    :goto_0
    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gq;

    invoke-direct {v1, v0, p2}, Lcom/google/android/apps/youtube/app/ui/gq;-><init>(Landroid/widget/TextView;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->C:Lcom/google/android/apps/youtube/app/ui/gn;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/gn;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_1

    invoke-direct {p0, v5}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->D:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->E:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->invalidate(Landroid/graphics/Rect;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v6, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v5}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->B:I

    return v0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 7

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->D:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/gq;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/gq;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->E:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->D:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/ArrayList;

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->D:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/gq;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/gq;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v3, v2

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->E:F

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->E:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v2

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->G:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->o:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->G:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->G:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->q:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->p:I

    sub-int/2addr v0, v2

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->G:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    return v6

    :cond_1
    move v0, v1

    move v1, v2

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 5

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->F:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->n:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public setOnTabSelectedListener(Lcom/google/android/apps/youtube/app/ui/go;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->z:Lcom/google/android/apps/youtube/app/ui/go;

    return-void
.end method

.method public setOnTabUnselectedListener(Lcom/google/android/apps/youtube/app/ui/gp;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->A:Lcom/google/android/apps/youtube/app/ui/gp;

    return-void
.end method
