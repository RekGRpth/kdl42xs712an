.class public Lcom/google/android/apps/youtube/app/remote/r;
.super Landroid/support/v7/media/j;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/app/remote/bk;

.field private final b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

.field private final c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final d:Landroid/os/Handler;

.field private e:J

.field private f:J

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Landroid/support/v7/media/j;-><init>()V

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->e:J

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->f:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->g:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/remote/s;-><init>(Lcom/google/android/apps/youtube/app/remote/r;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->d:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 7

    const-wide/16 v5, 0xc8

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->d:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/youtube/app/remote/r;->f:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v5

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->f:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/remote/bk;->d(I)V

    :goto_0
    invoke-super {p0, p1}, Landroid/support/v7/media/j;->a(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/r;->d:Landroid/os/Handler;

    invoke-static {v1, v4, p1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final b(I)V
    .locals 8

    const-wide/16 v6, 0xc8

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->d:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->g:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->g:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/youtube/app/remote/r;->e:J

    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->e:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget v1, p0, Lcom/google/android/apps/youtube/app/remote/r;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(I)V

    iput v4, p0, Lcom/google/android/apps/youtube/app/remote/r;->g:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/r;->d:Landroid/os/Handler;

    iget v2, p0, Lcom/google/android/apps/youtube/app/remote/r;->g:I

    invoke-static {v1, v5, v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/r;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/r;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/r;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Z)V

    return-void
.end method
