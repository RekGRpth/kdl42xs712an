.class public Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/hp;
.implements Lcom/google/android/apps/youtube/app/ui/hy;
.implements Lcom/google/android/apps/youtube/app/ui/ir;


# instance fields
.field private Y:Landroid/view/View;

.field private Z:Landroid/view/ViewStub;

.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private aa:Landroid/view/View;

.field private ab:Lcom/google/android/apps/youtube/app/ui/ij;

.field private ac:Lcom/google/android/apps/youtube/app/ui/hj;

.field private ad:Lcom/google/android/apps/youtube/app/ui/s;

.field private ae:Lcom/google/android/apps/youtube/app/offline/r;

.field private af:Lcom/google/android/apps/youtube/core/offline/store/q;

.field private ag:Lcom/google/android/apps/youtube/core/identity/l;

.field private ah:Lcom/google/android/apps/youtube/core/client/bj;

.field private ai:Lcom/google/android/apps/youtube/datalib/distiller/a;

.field private aj:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private al:Lcom/google/android/apps/youtube/app/d/f;

.field private am:Lcom/google/android/apps/youtube/uilib/innertube/t;

.field private an:Lcom/google/android/apps/youtube/app/ui/fh;

.field private ao:Lcom/google/android/apps/youtube/app/ui/df;

.field private b:Lcom/google/android/apps/youtube/app/aw;

.field private c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private d:Landroid/content/res/Resources;

.field private e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

.field private f:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

.field private g:Lcom/google/android/apps/youtube/common/c/a;

.field private h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private i:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private F()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/core/offline/store/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lcom/google/android/apps/youtube/app/d/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Lcom/google/android/apps/youtube/app/d/f;

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method private b(Landroid/content/res/Configuration;)V
    .locals 5

    const/16 v3, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->f:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Landroid/view/View;

    if-eqz v0, :cond_2

    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Landroid/view/View;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lcom/google/android/apps/youtube/app/ui/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/ui/s;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;)Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->f:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    return-object v0
.end method

.method private handleOfflineVideoAddEvent(Lcom/google/android/apps/youtube/app/offline/a/aa;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/aa;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->M()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    :cond_0
    return-void
.end method

.method private handleOfflineVideoAddFailedEvent(Lcom/google/android/apps/youtube/app/offline/a/ab;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/ab;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->M()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    :cond_0
    return-void
.end method

.method private handleOfflineVideoCompleteEvent(Lcom/google/android/apps/youtube/app/offline/a/ac;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/ac;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->M()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->o:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    :cond_0
    return-void
.end method

.method private handleOfflineVideoDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/ad;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/ad;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->M()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    :cond_0
    return-void
.end method

.method private handleOfflineVideoStatusUpdateEvent(Lcom/google/android/apps/youtube/app/offline/a/ae;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v3, 0x1

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/ae;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->M()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->N:I

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->r()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->N:I

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->s()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->p:I

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->t:I

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method private handlePlaybackServiceException(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->d:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/p;->gm:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object v1, Lcom/google/android/apps/youtube/app/fragments/bv;->b:[I

    iget-object v2, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/bx;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/app/fragments/bx;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;B)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setOnRetryClickListener(Lcom/google/android/apps/youtube/uilib/innertube/q;)V

    iget-boolean v1, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->isRetriable:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/bz;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/app/fragments/bz;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;B)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setOnRetryClickListener(Lcom/google/android/apps/youtube/uilib/innertube/q;)V

    iget-boolean v1, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->isRetriable:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a(Ljava/lang/String;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleRequestingWatchDataEvent(Lcom/google/android/apps/youtube/core/player/event/r;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a()V

    return-void
.end method

.method private handleSequencerStageEvent(Lcom/google/android/apps/youtube/core/player/event/v;)V
    .locals 9
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/bv;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->a()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Lcom/google/android/apps/youtube/app/d/f;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/datalib/distiller/model/a;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/s;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Lcom/google/android/apps/youtube/app/d/f;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/datalib/distiller/model/a;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getVideoId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->F()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Lcom/google/android/apps/youtube/datalib/distiller/a;

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/distiller/a;->a()Lcom/google/android/apps/youtube/datalib/distiller/c;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/datalib/distiller/c;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/c;

    const/4 v7, 0x2

    invoke-virtual {v0, v7}, Lcom/google/android/apps/youtube/datalib/distiller/c;->a(I)Lcom/google/android/apps/youtube/datalib/distiller/c;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Lcom/google/android/apps/youtube/datalib/distiller/a;

    new-instance v8, Lcom/google/android/apps/youtube/app/fragments/bu;

    invoke-direct {v8, p0, v5}, Lcom/google/android/apps/youtube/app/fragments/bu;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;Ljava/lang/String;)V

    invoke-virtual {v7, v0, v8}, Lcom/google/android/apps/youtube/datalib/distiller/a;->a(Lcom/google/android/apps/youtube/datalib/distiller/c;Lcom/google/android/apps/youtube/datalib/a/l;)V

    move v0, v1

    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lcom/google/android/apps/youtube/uilib/innertube/t;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getSectionList()Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/youtube/uilib/innertube/t;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getVideoMetadataRenderer()Lcom/google/a/a/a/a/uv;

    move-result-object v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getVideoMetadataRenderer()Lcom/google/a/a/a/a/uv;

    move-result-object v8

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->isLive()Z

    move-result v4

    if-nez v4, :cond_3

    :goto_2
    invoke-virtual {v7, v8, v6, v5, v2}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Lcom/google/a/a/a/a/uv;Lcom/google/android/apps/youtube/datalib/legacy/model/x;Ljava/lang/String;Z)V

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getSectionList()Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    move-result-object v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c()V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final E()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->q()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 26

    sget v2, Lcom/google/android/youtube/l;->by:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v25

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/fh;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->d:Landroid/content/res/Resources;

    move-object/from16 v0, v25

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/app/ui/fh;-><init>(Landroid/view/View;Landroid/content/res/Resources;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lcom/google/android/apps/youtube/app/ui/fh;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lcom/google/android/apps/youtube/app/ui/fh;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/fh;->a(Z)V

    sget v2, Lcom/google/android/youtube/j;->fY:I

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/bw;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/youtube/app/fragments/bw;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;B)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setOnStateChangeListener(Lcom/google/android/apps/youtube/app/ui/bt;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b:Lcom/google/android/apps/youtube/app/aw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aV()Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Lcom/google/android/apps/youtube/core/identity/l;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Lcom/google/android/apps/youtube/core/client/bj;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->w()Lcom/google/android/apps/youtube/datalib/distiller/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Lcom/google/android/apps/youtube/datalib/distiller/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->x()Lcom/google/android/apps/youtube/datalib/innertube/v;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->y()Lcom/google/android/apps/youtube/datalib/innertube/al;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/core/offline/store/q;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->Z()Lcom/google/android/apps/youtube/app/offline/p;

    move-result-object v9

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/r;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Lcom/google/android/apps/youtube/core/identity/l;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->af:Lcom/google/android/apps/youtube/core/offline/store/q;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v8

    new-instance v10, Lcom/google/android/apps/youtube/app/ui/bv;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v10, v11, v9}, Lcom/google/android/apps/youtube/app/ui/bv;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/offline/p;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v11

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/youtube/app/offline/r;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/app/ui/hh;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Lcom/google/android/apps/youtube/app/offline/r;

    new-instance v8, Lcom/google/android/apps/youtube/app/ui/hj;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->k()Lcom/google/android/apps/youtube/core/identity/o;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Lcom/google/android/apps/youtube/core/identity/l;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aI()Ljava/util/concurrent/Executor;

    move-result-object v22

    move-object/from16 v10, v24

    move-object/from16 v11, v18

    move-object/from16 v18, v7

    move-object/from16 v20, p0

    invoke-direct/range {v8 .. v22}, Lcom/google/android/apps/youtube/app/ui/hj;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/datalib/innertube/al;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/ui/hp;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Executor;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/ui/hj;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/ui/hj;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hy;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    sget v3, Lcom/google/android/youtube/j;->go:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->i:Landroid/widget/ListView;

    sget v2, Lcom/google/android/youtube/l;->bL:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->i:Landroid/widget/ListView;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    sget v2, Lcom/google/android/youtube/j;->gl:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/ij;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/ui/hj;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/youtube/app/ui/ij;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/ir;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Y:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Landroid/view/View;)V

    sget v2, Lcom/google/android/youtube/j;->gj:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Landroid/view/ViewStub;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v6

    sget-object v8, Lcom/google/android/apps/youtube/core/client/WatchFeature;->RELATED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object/from16 v9, v17

    move-object/from16 v10, v24

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aj:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-static {v2, v7, v3, v0, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    new-instance v14, Lcom/google/android/apps/youtube/app/ui/s;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Lcom/google/android/apps/youtube/core/client/bj;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b:Lcom/google/android/apps/youtube/app/aw;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->i:Landroid/widget/ListView;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ac()Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v23

    move-object/from16 v16, v24

    invoke-direct/range {v14 .. v23}, Lcom/google/android/apps/youtube/app/ui/s;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/app/aw;Landroid/view/View;Landroid/widget/ListView;ILcom/google/android/apps/youtube/core/player/ae;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/ui/s;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->i:Landroid/widget/ListView;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    new-instance v14, Lcom/google/android/apps/youtube/app/d/f;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->i:Landroid/widget/ListView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ai:Lcom/google/android/apps/youtube/datalib/distiller/a;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Lcom/google/android/apps/youtube/core/client/bj;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Lcom/google/android/apps/youtube/core/identity/l;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v23

    move-object/from16 v19, v24

    move-object/from16 v20, v13

    move-object/from16 v24, v7

    invoke-direct/range {v14 .. v24}, Lcom/google/android/apps/youtube/app/d/f;-><init>(Landroid/app/Activity;Landroid/widget/ListView;Lcom/google/android/apps/youtube/datalib/distiller/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/aw;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Lcom/google/android/apps/youtube/app/d/f;

    new-instance v8, Lcom/google/android/apps/youtube/app/ui/presenter/cc;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->b(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;)Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ah:Lcom/google/android/apps/youtube/core/client/bj;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ag:Lcom/google/android/apps/youtube/core/identity/l;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aQ()Lcom/google/android/apps/youtube/core/identity/as;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v15

    new-instance v16, Lcom/google/android/apps/youtube/app/ui/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->C()Lcom/google/android/apps/youtube/datalib/e/b;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/app/ui/a;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->F()Lcom/google/android/apps/youtube/datalib/innertube/bc;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->G()Lcom/google/android/apps/youtube/datalib/innertube/av;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aj:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-object/from16 v21, v0

    move-object/from16 v20, v7

    invoke-direct/range {v8 .. v21}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/a;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/datalib/innertube/av;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/am;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/presenter/cv;

    invoke-direct {v4}, Lcom/google/android/apps/youtube/app/ui/presenter/cv;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/presenter/am;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/common/c/a;)V

    new-instance v12, Lcom/google/android/apps/youtube/app/adapter/z;

    invoke-direct {v12, v2}, Lcom/google/android/apps/youtube/app/adapter/z;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/am;)V

    new-instance v9, Lcom/google/android/apps/youtube/uilib/innertube/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->i:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->t()Lcom/google/android/apps/youtube/datalib/innertube/ay;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v14

    move-object v15, v8

    move-object/from16 v16, v7

    invoke-direct/range {v9 .. v16}, Lcom/google/android/apps/youtube/uilib/innertube/t;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/i;Landroid/widget/ListView;Lcom/google/android/apps/youtube/uilib/innertube/p;Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/uilib/innertube/j;Lcom/google/android/apps/youtube/core/aw;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lcom/google/android/apps/youtube/uilib/innertube/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lcom/google/android/apps/youtube/uilib/innertube/t;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/by;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/youtube/app/fragments/by;-><init>(Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;B)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->am:Lcom/google/android/apps/youtube/uilib/innertube/t;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Lcom/google/android/apps/youtube/app/d/f;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/d/f;->b()Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Lcom/google/android/apps/youtube/uilib/a/h;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    return-object v25
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->h:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    check-cast p1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->d:Landroid/content/res/Resources;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->M()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->N()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->j()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->k()Lcom/google/a/a/a/a/lv;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/lv;)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->t:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->F()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->v()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Lcom/google/android/apps/youtube/app/offline/r;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/offline/r;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-virtual {v0, v3, p1, v1}, Lcom/google/android/apps/youtube/app/offline/r;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V

    goto :goto_0

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->q()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->t:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    goto :goto_0

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->f()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a()V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->j()Lcom/google/a/a/a/a/lz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/lz;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ae:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-virtual {v0, p1, v3, v1}, Lcom/google/android/apps/youtube/app/offline/r;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V

    goto/16 :goto_0
.end method

.method public final b()Lcom/google/android/apps/youtube/app/ui/hj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ac:Lcom/google/android/apps/youtube/app/ui/hj;

    return-object v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->k()Landroid/support/v4/app/l;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/j;->dx:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/l;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    sget v0, Lcom/google/android/youtube/j;->gm:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/l;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->f:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/df;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->e:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/df;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lcom/google/android/apps/youtube/app/ui/dn;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;Ljava/util/concurrent/atomic/AtomicReference;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lcom/google/android/apps/youtube/app/ui/df;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lcom/google/android/apps/youtube/app/ui/df;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->an:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/df;->a(Lcom/google/android/apps/youtube/app/ui/fh;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->f:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->f:Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/fragments/WatchInfoPanelFragment;->q()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->Z:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ij;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aa:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->b(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public final r()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->d:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aj:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Lcom/google/android/apps/youtube/app/d/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/d/f;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/s;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Lcom/google/android/apps/youtube/app/d/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lcom/google/android/apps/youtube/app/ui/df;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/ui/s;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final s()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->s()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->al:Lcom/google/android/apps/youtube/app/d/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ao:Lcom/google/android/apps/youtube/app/ui/df;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ad:Lcom/google/android/apps/youtube/app/ui/s;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final t()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->aj:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method
