.class final Lcom/google/android/apps/youtube/app/ui/presenter/bt;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/core/client/bj;

.field private Z:Lcom/google/android/apps/youtube/datalib/d/a;

.field private aa:Lcom/google/android/apps/youtube/app/ui/gr;

.field private ab:Landroid/view/View;

.field private ac:Landroid/widget/ImageView;

.field private ad:Landroid/widget/TextView;

.field private ae:Landroid/widget/TextView;

.field private af:Landroid/widget/TextView;

.field private ag:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bt;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/bt;)Lcom/google/android/apps/youtube/datalib/d/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->Z:Lcom/google/android/apps/youtube/datalib/d/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/presenter/bt;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ab:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    sget v0, Lcom/google/android/youtube/l;->aK:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ab:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ac:Landroid/widget/ImageView;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->Y:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ac:Landroid/widget/ImageView;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->aa:Lcom/google/android/apps/youtube/app/ui/gr;

    sget v0, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ad:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->ag:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ae:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->eK:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->af:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->aH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ag:Landroid/widget/TextView;

    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->Y:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->Z:Lcom/google/android/apps/youtube/datalib/d/a;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    return-object v0
.end method

.method public final d()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->d()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->h()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "playlist_header"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->a()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "playlist_header"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/google/a/a/a/a/oi;

    invoke-direct {v1}, Lcom/google/a/a/a/a/oi;-><init>()V

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;-><init>(Lcom/google/a/a/a/a/oi;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ad:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ae:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->h()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->af:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->f()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->g()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ag:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->e()Lcom/google/a/a/a/a/kz;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ae:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/bu;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bu;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bt;Lcom/google/android/apps/youtube/datalib/innertube/model/ab;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->aa:Lcom/google/android/apps/youtube/app/ui/gr;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/bv;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bv;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bt;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;Lcom/google/android/apps/youtube/app/d/e;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->a()V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ag:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bt;->ag:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->g()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
