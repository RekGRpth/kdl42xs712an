.class public final Lcom/google/android/apps/youtube/app/remote/an;
.super Landroid/support/v7/media/v;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/notification/c;


# static fields
.field private static final a:Landroid/support/v7/media/s;


# instance fields
.field private final b:Landroid/support/v7/media/u;

.field private final c:Ljava/util/List;

.field private final d:Lcom/google/android/apps/youtube/app/remote/bk;

.field private final e:Lcom/google/android/apps/youtube/app/ac;

.field private final f:Z

.field private g:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

.field private h:I

.field private i:Lcom/google/android/apps/youtube/common/a/b;

.field private j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/support/v7/media/t;

    invoke-direct {v0}, Landroid/support/v7/media/t;-><init>()V

    const-string v1, "MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    invoke-virtual {v0, v1}, Landroid/support/v7/media/t;->a(Ljava/lang/String;)Landroid/support/v7/media/t;

    invoke-virtual {v0}, Landroid/support/v7/media/t;->a()Landroid/support/v7/media/s;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/an;->a:Landroid/support/v7/media/s;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/bk;Landroid/support/v7/media/f;Lcom/google/android/apps/youtube/app/ac;Z)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/media/v;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->h:I

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->d:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ac;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->e:Lcom/google/android/apps/youtube/app/ac;

    iput-boolean p5, p0, Lcom/google/android/apps/youtube/app/remote/an;->f:Z

    invoke-static {p1}, Landroid/support/v7/media/u;->a(Landroid/content/Context;)Landroid/support/v7/media/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->c:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ao;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/remote/ao;-><init>(Lcom/google/android/apps/youtube/app/remote/an;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/au;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    invoke-static {p3}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/f;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/ad;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    invoke-static {}, Landroid/support/v7/media/u;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ad;

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->m()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->m()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "screen"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/an;)Landroid/support/v7/media/u;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/remote/RemoteControl;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/an;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->f:Z

    if-nez v0, :cond_0

    const-string v0, "true"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/an;->e:Lcom/google/android/apps/youtube/app/ac;

    const-string v2, "enable_mdx_logs"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "YouTube MDX"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/ap;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, v2, p1}, Lcom/google/android/apps/youtube/app/remote/ap;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private static f(Landroid/support/v7/media/ad;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/support/v7/media/ad;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    const-string v2, "MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i()Landroid/support/v7/media/s;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/an;->a:Landroid/support/v7/media/s;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/media/RemoteControlClient;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    invoke-static {p1}, Landroid/support/v7/media/u;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Landroid/support/v7/media/ad;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route added "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->g:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->g:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->m()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "screen"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->n()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->i:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/an;->g:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/remote/an;->g:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/remote/an;->i:Lcom/google/android/apps/youtube/common/a/b;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Selecting mdx route for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/an;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/ad;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Trying to select an unknown route - will ignore"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/an;->g:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/an;->i:Lcom/google/android/apps/youtube/common/a/b;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/media/ad;->n()V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/youtube/app/remote/ap;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Landroid/media/RemoteControlClient;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    invoke-static {p1}, Landroid/support/v7/media/u;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Landroid/support/v7/media/ad;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route removed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/youtube/app/remote/ap;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Landroid/support/v7/media/ad;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route selected "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/remote/an;->f(Landroid/support/v7/media/ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->d:Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    :goto_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/app/remote/an;->g:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Z)V

    return-void

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    goto :goto_0
.end method

.method public final d(Landroid/support/v7/media/ad;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Route unselected "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Z)V

    return-void
.end method

.method public final e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method public final f()V
    .locals 3

    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->h:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/an;->a:Landroid/support/v7/media/s;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, p0, v2}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    invoke-static {}, Landroid/support/v7/media/u;->c()Landroid/support/v7/media/ad;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/an;->f(Landroid/support/v7/media/ad;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/an;->d:Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Z)V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->h:I

    return-void

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/an;->j:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->h:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->h:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/v;)V

    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/an;->b:Landroid/support/v7/media/u;

    invoke-static {}, Landroid/support/v7/media/u;->b()Landroid/support/v7/media/ad;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->n()V

    return-void
.end method
