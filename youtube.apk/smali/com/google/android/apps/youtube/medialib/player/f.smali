.class public final Lcom/google/android/apps/youtube/medialib/player/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/ae;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/network/h;

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/network/h;ZZZ)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/f;-><init>(Lcom/google/android/apps/youtube/common/network/h;ZZZZ)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/common/network/h;ZZZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/f;->a:Lcom/google/android/apps/youtube/common/network/h;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/medialib/player/f;->b:Z

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/medialib/player/f;->c:Z

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/medialib/player/f;->d:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/f;->e:Z

    return-void
.end method

.method private static a(Ljava/util/Collection;[I)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 6

    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget v3, p1, v1

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v5

    if-ne v5, v3, :cond_0

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Ljava/util/Collection;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;
    .locals 11

    const/16 v9, 0x2d0

    const/16 v8, 0x168

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v7

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/f;->b:Z

    if-nez v1, :cond_2

    if-lt v7, v9, :cond_2

    move v1, v2

    :goto_1
    or-int/lit8 v5, v1, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/f;->c:Z

    if-eqz v1, :cond_3

    if-lt v7, v8, :cond_3

    move v1, v2

    :goto_2
    or-int/2addr v5, v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->is3D()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/f;->d:Z

    if-nez v1, :cond_4

    move v1, v2

    :goto_3
    or-int/2addr v5, v1

    if-eq v7, v9, :cond_5

    if-eq v7, v8, :cond_5

    move v1, v2

    :goto_4
    or-int/2addr v5, v1

    :cond_1
    if-eqz p3, :cond_14

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getSimpleMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_5
    or-int/2addr v1, v5

    :goto_6
    if-nez v1, :cond_0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_4

    :cond_6
    move v1, v3

    goto :goto_5

    :cond_7
    invoke-static {v4}, Lcom/google/android/apps/youtube/medialib/player/f;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/f;->d:Z

    if-eqz v1, :cond_13

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_13

    :goto_7
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/f;->e:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/f;->a:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/network/h;->h()Z

    move-result v1

    if-eqz v1, :cond_9

    move-object v1, p2

    :goto_8
    if-nez v1, :cond_8

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/f;->a()[I

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/f;->a(Ljava/util/Collection;[I)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    :cond_8
    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/f;->b(Ljava/util/Collection;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v4

    if-eqz v4, :cond_c

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v5

    if-le v0, v5, :cond_b

    new-instance v3, Lcom/google/android/apps/youtube/medialib/player/ad;

    if-ne p4, v2, :cond_a

    move-object v0, v1

    :goto_9
    invoke-direct {v3, v0, v1, p4, v2}, Lcom/google/android/apps/youtube/medialib/player/ad;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;IZ)V

    move-object v0, v3

    :goto_a
    return-object v0

    :cond_9
    const/4 v1, 0x0

    goto :goto_8

    :cond_a
    move-object v0, v4

    goto :goto_9

    :cond_b
    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/ad;

    invoke-direct {v0, v4, v4, p4, v3}, Lcom/google/android/apps/youtube/medialib/player/ad;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;IZ)V

    goto :goto_a

    :cond_c
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/medialib/player/f;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)[I

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/apps/youtube/medialib/player/f;->a(Ljava/util/Collection;[I)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v4

    if-nez v4, :cond_12

    if-nez v1, :cond_12

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_d

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/MissingStreamException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/medialib/player/MissingStreamException;-><init>()V

    throw v0

    :cond_d
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    :goto_b
    if-nez v0, :cond_e

    move-object v0, v1

    :goto_c
    new-instance v4, Lcom/google/android/apps/youtube/medialib/player/ad;

    if-ne p4, v2, :cond_f

    move-object v5, v0

    :goto_d
    if-eq v0, v1, :cond_10

    :goto_e
    invoke-direct {v4, v5, v0, p4, v2}, Lcom/google/android/apps/youtube/medialib/player/ad;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;IZ)V

    move-object v0, v4

    goto :goto_a

    :cond_e
    if-nez v1, :cond_11

    move-object v1, v0

    goto :goto_c

    :cond_f
    move-object v5, v1

    goto :goto_d

    :cond_10
    move v2, v3

    goto :goto_e

    :cond_11
    move-object v10, v0

    move-object v0, v1

    move-object v1, v10

    goto :goto_c

    :cond_12
    move-object v0, v4

    goto :goto_b

    :cond_13
    move-object v0, v4

    goto :goto_7

    :cond_14
    move v1, v5

    goto/16 :goto_6
.end method

.method private static a(Ljava/util/Collection;)Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->is3D()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private a()[I
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/f;->a:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x168

    aput v2, v0, v1

    goto :goto_0

    :array_0
    .array-data 4
        0x2d0
        0x1e0
        0x195
        0x168
    .end array-data
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)[I
    .locals 4

    const/4 v3, 0x3

    const/4 v2, 0x2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v0

    const/16 v1, 0x168

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/f;->a:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/network/h;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/f;->a:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/network/h;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_3

    new-array v0, v2, [I

    fill-array-data v0, :array_2

    goto :goto_1

    :cond_3
    new-array v0, v3, [I

    fill-array-data v0, :array_3

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    new-array v0, v2, [I

    fill-array-data v0, :array_4

    goto :goto_1

    :cond_5
    new-array v0, v3, [I

    fill-array-data v0, :array_5

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0xf0
        0x90
    .end array-data

    :array_1
    .array-data 4
        0x168
        0xf0
        0x90
    .end array-data

    :array_2
    .array-data 4
        0x90
        0xf0
    .end array-data

    :array_3
    .array-data 4
        0x90
        0xf0
        0x168
    .end array-data

    :array_4
    .array-data 4
        0xf0
        0x90
    .end array-data

    :array_5
    .array-data 4
        0xf0
        0x90
        0x168
    .end array-data
.end method

.method private static b(Ljava/util/Collection;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 3

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isLocal()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a([II)I
    .locals 8

    const/4 v2, 0x0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/f;->a()[I

    move-result-object v0

    :goto_0
    array-length v5, v0

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_3

    aget v1, v0, v4

    array-length v6, p1

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_2

    aget v7, p1, v3

    if-ne v7, v1, :cond_1

    move v0, v1

    :goto_3
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/f;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)[I

    move-result-object v0

    goto :goto_0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getHlsStream()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/MissingStreamException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/medialib/player/MissingStreamException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/ad;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v1, p3, v2}, Lcom/google/android/apps/youtube/medialib/player/ad;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;IZ)V

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getProgressiveFormatStreams()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/google/android/apps/youtube/medialib/player/f;->a(Ljava/util/Collection;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/apps/youtube/medialib/player/f;->a(Ljava/util/Collection;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;

    move-result-object v0

    return-object v0
.end method
