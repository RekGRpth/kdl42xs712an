.class Lcom/google/android/apps/youtube/medialib/player/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic b:Lcom/google/android/apps/youtube/medialib/player/af;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/medialib/player/af;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/ah;->b:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/medialib/player/af;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/ah;-><init>(Lcom/google/android/apps/youtube/medialib/player/af;)V

    return-void
.end method


# virtual methods
.method protected a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 7

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/ah;->a()Z

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/ah;->b:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/af;->b(Lcom/google/android/apps/youtube/medialib/player/af;)Z

    move-result v2

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ah;->b:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/af;->a(Lcom/google/android/apps/youtube/medialib/player/af;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget v3, p1, Landroid/os/Message;->what:I

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0, v3, v4, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    return v1
.end method
