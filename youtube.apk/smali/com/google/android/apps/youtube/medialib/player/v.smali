.class final Lcom/google/android/apps/youtube/medialib/player/v;
.super Landroid/os/HandlerThread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/medialib/player/p;

.field private final b:Ljava/lang/Runnable;

.field private c:Landroid/os/Handler;

.field private d:I

.field private volatile e:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/p;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/v;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    const-string v0, "YouTubePlayer.ProgressDetector"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/w;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/medialib/player/w;-><init>(Lcom/google/android/apps/youtube/medialib/player/v;Lcom/google/android/apps/youtube/medialib/player/p;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->b:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/v;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->u(Lcom/google/android/apps/youtube/medialib/player/p;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/v;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/p;->q(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->e()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/player/v;->d:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v;->c()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/v;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->e:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Retrying MediaPlayer error [retry="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/player/v;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", max=3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    return-void
.end method

.method public final a(I)V
    .locals 4

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/v;->d:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/v;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final b()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->e:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->e:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public final quit()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-super {p0}, Landroid/os/HandlerThread;->quit()Z

    move-result v0

    return v0
.end method

.method public final start()V
    .locals 2

    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v;->c:Landroid/os/Handler;

    return-void
.end method
