.class final Lcom/google/android/apps/youtube/medialib/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/fromguava/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/medialib/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/medialib/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/b;->a:Lcom/google/android/apps/youtube/medialib/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic b()Ljava/lang/Object;
    .locals 6

    new-instance v0, Lcom/google/android/exoplayer/upstream/HttpDataSource;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/b;->a:Lcom/google/android/apps/youtube/medialib/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/a;->a(Lcom/google/android/apps/youtube/medialib/a;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/exoplayer/upstream/HttpDataSource;->a:Lcom/google/android/exoplayer/e/d;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/b;->a:Lcom/google/android/apps/youtube/medialib/a;

    invoke-static {v3}, Lcom/google/android/apps/youtube/medialib/a;->b(Lcom/google/android/apps/youtube/medialib/a;)Lcom/google/android/exoplayer/upstream/c;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/b;->a:Lcom/google/android/apps/youtube/medialib/a;

    invoke-static {v4}, Lcom/google/android/apps/youtube/medialib/a;->c(Lcom/google/android/apps/youtube/medialib/a;)Lcom/google/android/apps/youtube/common/fromguava/e;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->httpConnectTimeoutMs()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/medialib/b;->a:Lcom/google/android/apps/youtube/medialib/a;

    invoke-static {v5}, Lcom/google/android/apps/youtube/medialib/a;->c(Lcom/google/android/apps/youtube/medialib/a;)Lcom/google/android/apps/youtube/common/fromguava/e;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->httpReadTimeoutMs()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer/upstream/HttpDataSource;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/e/d;Lcom/google/android/exoplayer/upstream/l;II)V

    return-object v0
.end method
