.class public final Lcom/google/android/apps/youtube/medialib/player/ak;
.super Lcom/google/android/exoplayer/a/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/e;


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/c;IIIIF)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct/range {p0 .. p6}, Lcom/google/android/exoplayer/a/p;-><init>(Lcom/google/android/exoplayer/upstream/c;IIIIF)V

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/ak;->a:I

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/ak;->b:I

    return-void
.end method


# virtual methods
.method protected final a([Lcom/google/android/exoplayer/a/m;J)Lcom/google/android/exoplayer/a/m;
    .locals 6

    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    const-wide/32 p2, 0x16378

    :cond_0
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_8

    aget-object v1, p1, v0

    iget v2, p0, Lcom/google/android/apps/youtube/medialib/player/ak;->a:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget v2, p0, Lcom/google/android/apps/youtube/medialib/player/ak;->b:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    :cond_1
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_6

    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-eqz v2, :cond_2

    iget v2, v1, Lcom/google/android/exoplayer/a/m;->g:I

    int-to-long v2, v2

    cmp-long v2, v2, p2

    if-gez v2, :cond_6

    :cond_2
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_7

    move-object v0, v1

    :goto_3
    return-object v0

    :cond_3
    iget v2, v1, Lcom/google/android/exoplayer/a/m;->c:I

    int-to-double v2, v2

    const-wide v4, 0x3feb333333333333L    # 0.85

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/google/android/apps/youtube/medialib/player/ak;->a:I

    int-to-double v4, v4

    cmpl-double v2, v2, v4

    if-lez v2, :cond_4

    iget v2, v1, Lcom/google/android/exoplayer/a/m;->d:I

    int-to-double v2, v2

    const-wide v4, 0x3feb333333333333L    # 0.85

    mul-double/2addr v2, v4

    iget v4, p0, Lcom/google/android/apps/youtube/medialib/player/ak;->b:I

    int-to-double v4, v4

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_5

    :cond_4
    const/4 v2, 0x1

    goto :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    goto :goto_3
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 1

    if-nez p1, :cond_0

    instance-of v0, p2, Lcom/google/android/apps/youtube/medialib/player/al;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/apps/youtube/medialib/player/al;

    iget v0, p2, Lcom/google/android/apps/youtube/medialib/player/al;->a:I

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/ak;->a:I

    iget v0, p2, Lcom/google/android/apps/youtube/medialib/player/al;->b:I

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/ak;->b:I

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/d;II)V
    .locals 2

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/al;

    invoke-direct {v1, p2, p3}, Lcom/google/android/apps/youtube/medialib/player/al;-><init>(II)V

    invoke-interface {p1, p0, v0, v1}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method
