.class final Lcom/google/android/apps/youtube/medialib/player/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/z;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/medialib/player/i;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/medialib/player/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/medialib/player/i;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/k;-><init>(Lcom/google/android/apps/youtube/medialib/player/i;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Lcom/google/android/apps/youtube/medialib/player/i;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/i;->b(Lcom/google/android/apps/youtube/medialib/player/i;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/i;->c(Lcom/google/android/apps/youtube/medialib/player/i;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/y;->setVideoSize(II)V

    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->e(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->f(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/exoplayer/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->e(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/i;->f(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/exoplayer/d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/medialib/player/y;->b()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v3}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/youtube/medialib/player/y;->c()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/medialib/player/ak;->a(Lcom/google/android/exoplayer/d;II)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/k;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->g(Lcom/google/android/apps/youtube/medialib/player/i;)V

    return-void
.end method
