.class public final Lcom/google/android/apps/youtube/medialib/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/medialib/a/a;

.field private final b:Lcom/google/android/exoplayer/upstream/c;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/youtube/common/network/h;

.field private final e:Lcom/google/android/apps/youtube/medialib/player/ae;

.field private final f:Lcom/google/android/apps/youtube/common/fromguava/e;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/apps/youtube/common/network/h;ZZZLcom/google/android/apps/youtube/common/fromguava/e;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p7, p0, Lcom/google/android/apps/youtube/medialib/a;->f:Lcom/google/android/apps/youtube/common/fromguava/e;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/medialib/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->a:Lcom/google/android/apps/youtube/medialib/a/a;

    new-instance v1, Lcom/google/android/exoplayer/upstream/c;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/a;->a:Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-direct {v1, v0, v2}, Lcom/google/android/exoplayer/upstream/c;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer/upstream/e;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/a;->b:Lcom/google/android/exoplayer/upstream/c;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->c:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->d:Lcom/google/android/apps/youtube/common/network/h;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/f;

    invoke-direct {v0, p3, p4, p5, p6}, Lcom/google/android/apps/youtube/medialib/player/f;-><init>(Lcom/google/android/apps/youtube/common/network/h;ZZZ)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->e:Lcom/google/android/apps/youtube/medialib/player/ae;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/a;)Lcom/google/android/exoplayer/upstream/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->b:Lcom/google/android/exoplayer/upstream/c;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/a;)Lcom/google/android/apps/youtube/common/fromguava/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->f:Lcom/google/android/apps/youtube/common/fromguava/e;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/medialib/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->a:Lcom/google/android/apps/youtube/medialib/a/a;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)Lcom/google/android/apps/youtube/medialib/player/x;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/p;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/a;->d:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/a;->e:Lcom/google/android/apps/youtube/medialib/player/ae;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/a;->c:Ljava/lang/String;

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/youtube/medialib/player/p;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/youtube/medialib/player/t;Lcom/google/android/apps/youtube/common/fromguava/e;ZLcom/google/android/apps/youtube/medialib/player/h;)Lcom/google/android/apps/youtube/medialib/player/x;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/p;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/a;->d:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/a;->e:Lcom/google/android/apps/youtube/medialib/player/ae;

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/a;->c:Ljava/lang/String;

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/p;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;Lcom/google/android/apps/youtube/medialib/player/t;)V

    if-eqz p4, :cond_0

    new-instance v2, Lcom/google/android/apps/youtube/medialib/m2ts/b;

    invoke-direct {v2, p3}, Lcom/google/android/apps/youtube/medialib/m2ts/b;-><init>(Lcom/google/android/apps/youtube/common/fromguava/e;)V

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/medialib/player/af;-><init>(Lcom/google/android/apps/youtube/medialib/player/x;Lcom/google/android/apps/youtube/medialib/player/x;)V

    move-object v0, v1

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_1

    new-instance v2, Lcom/google/android/apps/youtube/medialib/player/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/a;->b:Lcom/google/android/exoplayer/upstream/c;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/a;->e:Lcom/google/android/apps/youtube/medialib/player/ae;

    invoke-direct {v2, p3, v1, v3, p5}, Lcom/google/android/apps/youtube/medialib/player/i;-><init>(Lcom/google/android/apps/youtube/common/fromguava/e;Lcom/google/android/exoplayer/upstream/c;Lcom/google/android/apps/youtube/medialib/player/ae;Lcom/google/android/apps/youtube/medialib/player/h;)V

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/g;

    invoke-direct {v1, v2, v0, p1, p5}, Lcom/google/android/apps/youtube/medialib/player/g;-><init>(Lcom/google/android/apps/youtube/medialib/player/x;Lcom/google/android/apps/youtube/medialib/player/x;Landroid/content/Context;Lcom/google/android/apps/youtube/medialib/player/h;)V

    move-object v0, v1

    :cond_1
    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/ab;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/medialib/player/ab;-><init>(Lcom/google/android/apps/youtube/medialib/player/x;)V

    return-object v1
.end method

.method public final b()Lcom/google/android/apps/youtube/medialib/player/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/a;->e:Lcom/google/android/apps/youtube/medialib/player/ae;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/common/fromguava/e;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/medialib/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/b;-><init>(Lcom/google/android/apps/youtube/medialib/a;)V

    return-object v0
.end method
