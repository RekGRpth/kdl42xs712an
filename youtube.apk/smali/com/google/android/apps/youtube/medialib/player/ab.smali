.class public final Lcom/google/android/apps/youtube/medialib/player/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/x;


# instance fields
.field private a:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final b:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final c:Lcom/google/android/apps/youtube/medialib/player/x;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/x;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/ac;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/medialib/player/ac;-><init>(Lcom/google/android/apps/youtube/medialib/player/ab;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->c:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(F)V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(I)V

    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/x;->a(II)V

    return-void
.end method

.method public final a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    return-void
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Landroid/os/Handler;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->c:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->b()V

    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->b(Landroid/os/Handler;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->c()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->d()V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->c:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->e()V

    return-void
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->g()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->h()I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->i()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->k()V

    return-void
.end method

.method public final l()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->c:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->l()V

    return-void
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    return-void
.end method

.method public final n()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->c:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ab;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->k()V

    return-void
.end method
