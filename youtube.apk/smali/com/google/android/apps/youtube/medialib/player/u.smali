.class final Lcom/google/android/apps/youtube/medialib/player/u;
.super Landroid/os/HandlerThread;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/google/android/apps/youtube/medialib/player/p;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/p;)V
    .locals 1

    const-string v0, "YouTubePlayer.MediaPlayerThread"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final a(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;Landroid/net/Uri;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    aput-object p2, v0, v3

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final declared-synchronized d()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized handleMessage(Landroid/os/Message;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    :goto_0
    monitor-exit p0

    return v0

    :pswitch_0
    :try_start_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Lcom/google/android/apps/youtube/medialib/player/n;

    const/4 v4, 0x1

    aget-object v0, v0, v4

    check-cast v0, Landroid/net/Uri;

    invoke-static {v3, v1, v0}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/medialib/player/n;Landroid/net/Uri;)V

    move v0, v2

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Lcom/google/android/apps/youtube/medialib/player/p;)V

    move v0, v2

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->c(Lcom/google/android/apps/youtube/medialib/player/p;)V

    move v0, v2

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;I)V

    move v0, v2

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;)V

    move v0, v2

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/u;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->b:Lcom/google/android/apps/youtube/medialib/player/p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final quit()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/u;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final start()V
    .locals 2

    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/u;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/u;->a:Landroid/os/Handler;

    return-void
.end method
