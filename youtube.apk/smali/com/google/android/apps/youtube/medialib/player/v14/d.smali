.class final Lcom/google/android/apps/youtube/medialib/player/v14/d;
.super Landroid/view/TextureView;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-direct {p0, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected final onMeasure(II)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v0

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getDefaultSize(II)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getDefaultSize(II)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v2

    mul-int/2addr v2, v0

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v3}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v3

    mul-int/2addr v3, v1

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    const v3, 0x3c23d70a    # 0.01f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v0

    mul-int/2addr v0, v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v2

    div-int/2addr v0, v2

    :cond_0
    :goto_0
    invoke-static {v1, p1}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->setMeasuredDimension(II)V

    return-void

    :cond_1
    const v3, -0x43dc28f6    # -0.01f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v1

    mul-int/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/v14/d;->a:Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I

    move-result v2

    div-int/2addr v1, v2

    goto :goto_0
.end method
