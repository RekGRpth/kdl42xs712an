.class final Lcom/google/android/apps/youtube/medialib/player/ai;
.super Lcom/google/android/apps/youtube/medialib/player/ah;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/medialib/player/af;

.field private c:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/medialib/player/af;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->a:Lcom/google/android/apps/youtube/medialib/player/af;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/medialib/player/ah;-><init>(Lcom/google/android/apps/youtube/medialib/player/af;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/medialib/player/af;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/ai;-><init>(Lcom/google/android/apps/youtube/medialib/player/af;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->a:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/af;->c(Lcom/google/android/apps/youtube/medialib/player/af;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->a:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/af;->e(Lcom/google/android/apps/youtube/medialib/player/af;)V

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/ah;->handleMessage(Landroid/os/Message;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->a:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/af;->d(Lcom/google/android/apps/youtube/medialib/player/af;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->a:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/af;->f(Lcom/google/android/apps/youtube/medialib/player/af;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v1

    sub-int/2addr v0, v1

    const/16 v2, 0x32

    if-le v0, v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Foreground sync is ahead by "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iput v4, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->c:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->a:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/af;->d(Lcom/google/android/apps/youtube/medialib/player/af;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/x;->c()V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/google/android/apps/youtube/medialib/player/aj;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/medialib/player/aj;-><init>(Lcom/google/android/apps/youtube/medialib/player/ai;)V

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/16 v2, -0x32

    if-ge v0, v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->c:I

    const/4 v3, 0x2

    if-ge v2, v3, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Foreground sync is behind. Retry seek ahead: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->a:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/af;->d(Lcom/google/android/apps/youtube/medialib/player/af;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    add-int/lit16 v1, v1, 0xfa0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(I)V

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Foreground synced with time diff: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", retries: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iput v4, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->c:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/ai;->a:Lcom/google/android/apps/youtube/medialib/player/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/af;->e(Lcom/google/android/apps/youtube/medialib/player/af;)V

    goto :goto_1

    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/ah;->handleMessage(Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0
.end method
