.class public final Lcom/google/android/apps/ytremote/fork/net/async/x;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static b:Ljava/lang/Long;

.field private static final c:Lcom/google/android/apps/ytremote/fork/net/async/ab;

.field private static final d:Lcom/google/android/apps/ytremote/fork/net/async/ab;

.field private static final e:Ljava/util/Collection;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/apps/ytremote/fork/net/async/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/fork/net/async/x;->a:Ljava/util/logging/Logger;

    const-wide/16 v0, 0x7530

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/fork/net/async/x;->b:Ljava/lang/Long;

    new-instance v0, Lcom/google/android/apps/ytremote/fork/net/async/ac;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/ytremote/fork/net/async/ac;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/ytremote/fork/net/async/x;->c:Lcom/google/android/apps/ytremote/fork/net/async/ab;

    new-instance v0, Lcom/google/android/apps/ytremote/fork/net/async/y;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/fork/net/async/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/ytremote/fork/net/async/x;->d:Lcom/google/android/apps/ytremote/fork/net/async/ab;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/fork/net/async/x;->e:Ljava/util/Collection;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/apps/ytremote/fork/net/async/p;
    .locals 8

    sget-object v2, Lcom/google/android/apps/ytremote/fork/net/async/x;->c:Lcom/google/android/apps/ytremote/fork/net/async/ab;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "LoopRunner cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "EventDispatcher"

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    const-class v6, Lcom/google/android/apps/ytremote/fork/net/async/x;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "EventDispatcher created by "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    new-instance v1, Lcom/google/android/apps/ytremote/fork/net/async/z;

    const/4 v3, 0x1

    invoke-direct {v1, v3, v2, v0}, Lcom/google/android/apps/ytremote/fork/net/async/z;-><init>(ZLcom/google/android/apps/ytremote/fork/net/async/ab;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/fork/net/async/z;->start()V

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/fork/net/async/z;->a()Lcom/google/android/apps/ytremote/fork/net/async/p;

    move-result-object v0

    return-object v0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/logging/Logger;
    .locals 1

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/x;->a:Ljava/util/logging/Logger;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/Long;
    .locals 1

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/x;->b:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic d()Ljava/util/Collection;
    .locals 1

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/x;->e:Ljava/util/Collection;

    return-object v0
.end method
