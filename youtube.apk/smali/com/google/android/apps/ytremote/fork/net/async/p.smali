.class public Lcom/google/android/apps/ytremote/fork/net/async/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/fork/net/async/u;


# static fields
.field static final synthetic a:Z

.field private static final b:Lcom/google/android/apps/ytremote/fork/net/async/s;

.field private static final n:Ljava/util/logging/Logger;


# instance fields
.field private final c:Ljava/nio/channels/Selector;

.field private final d:Ljava/lang/Thread;

.field private final e:Lcom/google/android/apps/ytremote/fork/net/async/ak;

.field private final f:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final g:Ljava/util/PriorityQueue;

.field private volatile h:J

.field private volatile i:Z

.field private volatile j:Z

.field private volatile k:Z

.field private volatile l:Z

.field private volatile m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/ytremote/fork/net/async/p;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->a:Z

    new-instance v0, Lcom/google/android/apps/ytremote/fork/net/async/q;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/fork/net/async/q;-><init>()V

    sput-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->b:Lcom/google/android/apps/ytremote/fork/net/async/s;

    const-class v0, Lcom/google/android/apps/ytremote/fork/net/async/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/ytremote/fork/net/async/ak;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/fork/net/async/ak;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->e:Lcom/google/android/apps/ytremote/fork/net/async/ak;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->h:J

    iput-boolean v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->j:Z

    iput-boolean v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    iput-boolean v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->l:Z

    iput-boolean v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->m:Z

    :try_start_0
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->d:Ljava/lang/Thread;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/ytremote/fork/net/async/IORuntimeException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/ytremote/fork/net/async/IORuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Ljava/util/Set;Z)I
    .locals 11

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/SelectionKey;

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->readyOps()I

    move-result v4

    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/ytremote/fork/net/async/t;

    iget-object v5, v1, Lcom/google/android/apps/ytremote/fork/net/async/t;->a:Lcom/google/android/apps/ytremote/fork/net/async/a;

    iget-object v5, v1, Lcom/google/android/apps/ytremote/fork/net/async/t;->b:Lcom/google/android/apps/ytremote/fork/net/async/o;

    iget-object v6, v1, Lcom/google/android/apps/ytremote/fork/net/async/t;->c:Lcom/google/android/apps/ytremote/fork/net/async/af;

    iget-object v7, v1, Lcom/google/android/apps/ytremote/fork/net/async/t;->d:Lcom/google/android/apps/ytremote/fork/net/async/an;

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    and-int/lit8 v1, v4, 0x10

    if-eqz v1, :cond_1

    :try_start_2
    sget-object v1, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v8, "OP_ACCEPT event ready"

    invoke-virtual {v1, v8}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    :cond_1
    and-int/lit8 v1, v4, 0x8

    if-eqz v1, :cond_2

    :try_start_3
    sget-object v1, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v8, "OP_CONNECT event ready"

    invoke-virtual {v1, v8}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    invoke-interface {v5}, Lcom/google/android/apps/ytremote/fork/net/async/o;->a()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    :cond_2
    and-int/lit8 v1, v4, 0x1

    if-eqz v1, :cond_9

    :try_start_4
    sget-object v1, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v5, "OP_READ event ready"

    invoke-virtual {v1, v5}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    invoke-interface {v6}, Lcom/google/android/apps/ytremote/fork/net/async/af;->f()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_3
    add-int/lit8 v1, v2, 0x1

    :goto_4
    and-int/lit8 v2, v4, 0x4

    if-eqz v2, :cond_8

    :try_start_5
    sget-object v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v4, "OP_WRITE event ready"

    invoke-virtual {v2, v4}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    invoke-interface {v7}, Lcom/google/android/apps/ytremote/fork/net/async/an;->g()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    add-int/lit8 v0, v1, 0x1

    :goto_6
    move v2, v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectionKey;)V

    if-eqz p2, :cond_3

    throw v1

    :cond_3
    sget-object v8, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v10, "RuntimeException caught when invoking AcceptHandler.handleAcceptEvent"

    invoke-virtual {v8, v9, v10, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectionKey;)V

    if-eqz p2, :cond_4

    throw v1

    :cond_4
    sget-object v5, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v9, "RuntimeException caught when invoking ConnectHandler.handleConnectEvent"

    invoke-virtual {v5, v8, v9, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_2
    move-exception v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectionKey;)V

    if-eqz p2, :cond_5

    throw v1

    :cond_5
    sget-object v5, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v8, "RuntimeException caught when invoking ReadHandler.handleReadEvent"

    invoke-virtual {v5, v6, v8, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_3
    move-exception v2

    invoke-direct {p0, v0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectionKey;)V

    if-eqz p2, :cond_6

    throw v2

    :cond_6
    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "RuntimeException caught when invoking WriteHandler.handleWriteEvent"

    invoke-virtual {v0, v4, v5, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_7
    return v2

    :cond_8
    move v0, v1

    goto :goto_6

    :cond_9
    move v1, v2

    goto :goto_4
.end method

.method private static a(Ljava/util/Queue;Ljava/util/PriorityQueue;Lcom/google/android/apps/ytremote/fork/net/async/s;Z)J
    .locals 7

    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    if-eqz p3, :cond_0

    throw v0

    :cond_0
    sget-object v3, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "RuntimeException caught when invoking Runnable.run()"

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    monitor-enter p1

    :try_start_1
    invoke-virtual {p1}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    monitor-exit p1

    move-wide v0, v1

    :goto_1
    return-wide v0

    :cond_2
    invoke-virtual {p1}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/fork/net/async/r;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/net/async/r;->a()J

    move-result-wide v3

    invoke-interface {p2}, Lcom/google/android/apps/ytremote/fork/net/async/s;->a()J

    move-result-wide v5

    sub-long/2addr v3, v5

    cmp-long v5, v3, v1

    if-lez v5, :cond_3

    monitor-exit p1

    move-wide v0, v3

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Alarm expired: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/fork/net/async/r;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/google/android/apps/ytremote/fork/net/async/r;->b:Z

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/net/async/r;->b()Lcom/google/android/apps/ytremote/fork/net/async/c;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/google/android/apps/ytremote/fork/net/async/c;->a(Lcom/google/android/apps/ytremote/fork/net/async/b;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    if-eqz p3, :cond_5

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0

    :cond_5
    sget-object v3, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "RuntimeException caught when invoking AlarmHandler.handleAlarmEvent"

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Ljava/nio/channels/SelectableChannel;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {p1, v0}, Ljava/nio/channels/SelectableChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->h()V

    :try_start_0
    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->i()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result v0

    xor-int/lit8 v2, p2, -0x1

    and-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/fork/net/async/t;

    const/16 v2, 0x10

    if-ne p2, v2, :cond_2

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/ytremote/fork/net/async/t;->a:Lcom/google/android/apps/ytremote/fork/net/async/a;

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "OP_ACCEPT event deregistered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    :goto_1
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->i()V

    goto :goto_0

    :cond_2
    const/16 v2, 0x8

    if-ne p2, v2, :cond_3

    const/4 v2, 0x0

    :try_start_3
    iput-object v2, v0, Lcom/google/android/apps/ytremote/fork/net/async/t;->b:Lcom/google/android/apps/ytremote/fork/net/async/o;

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "OP_CONNECT event deregistered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->i()V

    throw v0

    :cond_3
    const/4 v2, 0x1

    if-ne p2, v2, :cond_4

    const/4 v2, 0x0

    :try_start_5
    iput-object v2, v0, Lcom/google/android/apps/ytremote/fork/net/async/t;->c:Lcom/google/android/apps/ytremote/fork/net/async/af;

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "OP_READ event deregistered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const/4 v2, 0x4

    if-ne p2, v2, :cond_5

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/ytremote/fork/net/async/t;->d:Lcom/google/android/apps/ytremote/fork/net/async/an;

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "OP_WRITE event deregistered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a valid op"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private a(Ljava/nio/channels/SelectableChannel;ILcom/google/android/apps/ytremote/fork/net/async/a;Lcom/google/android/apps/ytremote/fork/net/async/o;Lcom/google/android/apps/ytremote/fork/net/async/af;Lcom/google/android/apps/ytremote/fork/net/async/an;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->h()V

    :try_start_0
    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {p1, v0}, Ljava/nio/channels/SelectableChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    if-nez v0, :cond_a

    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    new-instance v1, Lcom/google/android/apps/ytremote/fork/net/async/t;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/google/android/apps/ytremote/fork/net/async/t;-><init>(B)V

    invoke-virtual {p1, v0, p2, v1}, Ljava/nio/channels/SelectableChannel;->register(Ljava/nio/channels/Selector;ILjava/lang/Object;)Ljava/nio/channels/SelectionKey;
    :try_end_2
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    move-object v1, v0

    move v0, v2

    :goto_0
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    sget-boolean v3, Lcom/google/android/apps/ytremote/fork/net/async/p;->a:Z

    if-nez v3, :cond_0

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->i()V

    throw v0

    :catch_0
    move-exception v0

    :try_start_5
    new-instance v1, Lcom/google/android/apps/ytremote/fork/net/async/IORuntimeException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/ytremote/fork/net/async/IORuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit p1

    throw v0

    :cond_0
    monitor-enter v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v0, :cond_1

    :try_start_7
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result v0

    or-int/2addr v0, p2

    invoke-virtual {v1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    :cond_1
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/fork/net/async/t;

    const/16 v3, 0x10

    if-ne p2, v3, :cond_3

    sget-boolean v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->a:Z

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v1

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_2
    const/4 v2, 0x0

    :try_start_9
    iput-object v2, v0, Lcom/google/android/apps/ytremote/fork/net/async/t;->a:Lcom/google/android/apps/ytremote/fork/net/async/a;

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "OP_ACCEPT event registered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    :goto_1
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->i()V

    return-void

    :cond_3
    const/16 v3, 0x8

    if-ne p2, v3, :cond_5

    :try_start_a
    sget-boolean v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->a:Z

    if-nez v2, :cond_4

    if-nez p4, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    iput-object p4, v0, Lcom/google/android/apps/ytremote/fork/net/async/t;->b:Lcom/google/android/apps/ytremote/fork/net/async/o;

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "OP_CONNECT event registered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    if-ne p2, v2, :cond_7

    sget-boolean v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->a:Z

    if-nez v2, :cond_6

    if-nez p5, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_6
    iput-object p5, v0, Lcom/google/android/apps/ytremote/fork/net/async/t;->c:Lcom/google/android/apps/ytremote/fork/net/async/af;

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "OP_READ event registered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const/4 v2, 0x4

    if-ne p2, v2, :cond_9

    sget-boolean v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->a:Z

    if-nez v2, :cond_8

    if-nez p6, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    iput-object p6, v0, Lcom/google/android/apps/ytremote/fork/net/async/t;->d:Lcom/google/android/apps/ytremote/fork/net/async/an;

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "OP_WRITE event registered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a valid op"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :cond_a
    move-object v4, v0

    move v0, v1

    move-object v1, v4

    goto/16 :goto_0
.end method

.method private static a(Ljava/nio/channels/SelectableChannel;Ljava/lang/Object;)V
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Ljava/nio/channels/SelectableChannel;->isBlocking()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/nio/channels/IllegalBlockingModeException;

    invoke-direct {v0}, Ljava/nio/channels/IllegalBlockingModeException;-><init>()V

    throw v0

    :cond_2
    return-void
.end method

.method private a(Ljava/nio/channels/SelectionKey;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->m:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v2, "Closing channel due to RuntimeException thrown by event handler"

    invoke-virtual {v0, v1, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->channel()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/SelectableChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to close channel"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private f()J
    .locals 9

    const-wide/16 v4, 0x0

    const-wide/16 v2, -0x1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move-wide v0, v2

    :goto_0
    return-wide v0

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v6

    move-wide v0, v4

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/fork/net/async/r;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/net/async/r;->a()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v0, v7

    cmp-long v4, v0, v4

    if-lez v4, :cond_2

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_2
    monitor-exit v6

    move-wide v0, v2

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->e:Lcom/google/android/apps/ytremote/fork/net/async/ak;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/net/async/ak;->c()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    return v0

    :catch_0
    move-exception v0

    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method private h()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->e:Lcom/google/android/apps/ytremote/fork/net/async/ak;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/net/async/ak;->a()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/ytremote/fork/net/async/IpV6BugException;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/fork/net/async/IpV6BugException;-><init>()V

    throw v0
.end method

.method private i()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->e:Lcom/google/android/apps/ytremote/fork/net/async/ak;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/net/async/ak;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(JLjava/lang/Object;Lcom/google/android/apps/ytremote/fork/net/async/c;)Lcom/google/android/apps/ytremote/fork/net/async/b;
    .locals 8

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offsetMillis cannot be negative: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    monitor-enter v7

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/ytremote/fork/net/async/r;

    add-long v1, v3, p1

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/ytremote/fork/net/async/r;-><init>(JJLcom/google/android/apps/ytremote/fork/net/async/c;Ljava/lang/Object;)V

    sget-object v1, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Alarm created: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    move-result v1

    sget-boolean v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->a:Z

    if-nez v2, :cond_3

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->e()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    :cond_4
    return-object v0
.end method

.method public final a()V
    .locals 9

    const-wide/16 v7, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->e()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalThreadStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Network thread is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->d:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but the thread "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is trying to loop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->j:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot recursively loop"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->j:Z

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v1, "Start looping"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->f()J

    move-result-wide v0

    :cond_2
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    if-nez v2, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_6

    cmp-long v2, v0, v7

    if-nez v2, :cond_8

    const-wide/32 v0, 0x5265c00

    move-wide v1, v0

    :goto_1
    cmp-long v0, v1, v7

    if-ltz v0, :cond_4

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v0, v1, v2}, Ljava/nio/channels/Selector;->select(J)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->h:J

    iget-object v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v2}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/util/Set;Z)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->h:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->i:Z

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    sget-object v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->b:Lcom/google/android/apps/ytremote/fork/net/async/s;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/util/Queue;Ljava/util/PriorityQueue;Lcom/google/android/apps/ytremote/fork/net/async/s;Z)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-wide v0

    const/4 v2, 0x0

    :try_start_4
    iput-boolean v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->i:Z

    sget-object v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "State at end of loop iteration: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-boolean v6, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->j:Z

    iput-boolean v6, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    sget-object v1, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v2, "Stop looping"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->l:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->close()V

    :cond_3
    throw v0

    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->selectNow()I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Operation not permitted"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v3, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "Ignoring spurious IOException in server loop"

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v0, v1

    goto/16 :goto_0

    :cond_5
    throw v0

    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->i:Z

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_6
    iput-boolean v6, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->j:Z

    iput-boolean v6, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/p;->n:Ljava/util/logging/Logger;

    const-string v1, "Stop looping"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->l:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->close()V

    :cond_7
    return-void

    :cond_8
    move-wide v1, v0

    goto/16 :goto_1
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;I)V

    return-void
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;Lcom/google/android/apps/ytremote/fork/net/async/af;)V
    .locals 7

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;Ljava/lang/Object;)V

    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, p2

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;ILcom/google/android/apps/ytremote/fork/net/async/a;Lcom/google/android/apps/ytremote/fork/net/async/o;Lcom/google/android/apps/ytremote/fork/net/async/af;Lcom/google/android/apps/ytremote/fork/net/async/an;)V

    return-void
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;Lcom/google/android/apps/ytremote/fork/net/async/an;)V
    .locals 7

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;Ljava/lang/Object;)V

    const/4 v2, 0x4

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;ILcom/google/android/apps/ytremote/fork/net/async/a;Lcom/google/android/apps/ytremote/fork/net/async/o;Lcom/google/android/apps/ytremote/fork/net/async/af;Lcom/google/android/apps/ytremote/fork/net/async/an;)V

    return-void
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;Lcom/google/android/apps/ytremote/fork/net/async/o;)V
    .locals 7

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;Ljava/lang/Object;)V

    const/16 v2, 0x8

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;ILcom/google/android/apps/ytremote/fork/net/async/a;Lcom/google/android/apps/ytremote/fork/net/async/o;Lcom/google/android/apps/ytremote/fork/net/async/af;Lcom/google/android/apps/ytremote/fork/net/async/an;)V

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    return-void
.end method

.method public final b(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;I)V

    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    invoke-virtual {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    :cond_0
    return-void
.end method

.method public final c(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->a(Ljava/nio/channels/SelectableChannel;I)V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->close()V

    return-void
.end method

.method public final e()Z
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->d:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/ytremote/fork/net/async/p;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->c:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Alarm set = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->j:Z

    if-eqz v1, :cond_1

    const-string v1, "; Looping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/p;->k:Z

    if-eqz v1, :cond_0

    const-string v1, "; Loop exiting"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    const-string v1, "; Not looping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
