.class public Lcom/google/android/apps/ytremote/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/logic/d;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/ytremote/b/g;

.field private final c:Lcom/google/android/apps/ytremote/b/f;

.field private final d:Lorg/apache/http/client/HttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/ytremote/b/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/b/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/ytremote/backend/logic/b;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/ytremote/b/e;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/ytremote/b/e;-><init>(B)V

    invoke-static {}, Lcom/google/android/apps/ytremote/a/d/a;->b()Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/ytremote/b/d;-><init>(Lcom/google/android/apps/ytremote/backend/logic/b;Lcom/google/android/apps/ytremote/b/f;Lorg/apache/http/client/HttpClient;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/ytremote/backend/logic/b;Lcom/google/android/apps/ytremote/b/f;Lorg/apache/http/client/HttpClient;)V
    .locals 3
    .annotation build Lcom/google/android/apps/ytremote/util/VisibleForTesting;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/ytremote/b/d;->c:Lcom/google/android/apps/ytremote/b/f;

    iput-object p3, p0, Lcom/google/android/apps/ytremote/b/d;->d:Lorg/apache/http/client/HttpClient;

    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lcom/google/android/apps/ytremote/b/g;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, p1, v2}, Lcom/google/android/apps/ytremote/b/g;-><init>(Landroid/os/Looper;Lcom/google/android/apps/ytremote/backend/logic/b;B)V

    iput-object v1, p0, Lcom/google/android/apps/ytremote/b/d;->b:Lcom/google/android/apps/ytremote/b/g;

    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/ytremote/b/d;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/d;->b:Lcom/google/android/apps/ytremote/b/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/b/g;->removeMessages(I)V

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 4

    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    const-string v1, "Origin"

    const-string v2, "package:com.google.android.youtube"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpDelete;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/d;->d:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/ytremote/b/d;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error stopping YouTubeTV. Response code is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-wide/16 v0, 0x3e8

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/ytremote/b/d;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error stopping YouTubeTV."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;JLcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/d;->c:Lcom/google/android/apps/ytremote/b/f;

    invoke-interface {v0}, Lcom/google/android/apps/ytremote/b/f;->a()Lcom/google/android/apps/ytremote/model/PairingCode;

    move-result-object v1

    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Content-Type"

    const-string v4, "text/plain; charset=\"utf-8\""

    invoke-direct {v0, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Lorg/apache/http/Header;)V

    const-string v0, "Origin"

    const-string v3, "package:com.google.android.youtube"

    invoke-virtual {v2, v0, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "pairingCode="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/PairingCode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&v="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&t="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/16 v3, 0x3e8

    div-long v3, p3, v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v3, Lorg/apache/http/entity/StringEntity;

    const-string v4, "UTF-8"

    invoke-direct {v3, v0, v4}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/d;->d:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x4

    invoke-interface {p6, v0}, Lcom/google/android/apps/ytremote/logic/e;->a(I)V

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/d;->b:Lcom/google/android/apps/ytremote/b/g;

    invoke-virtual {v0, v1, p6}, Lcom/google/android/apps/ytremote/b/g;->a(Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)V

    const/4 v0, 0x0

    const-string v1, "LOCATION"

    invoke-interface {v2, v1}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "LOCATION"

    invoke-interface {v2, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_1
    invoke-interface {p6, v0}, Lcom/google/android/apps/ytremote/logic/e;->a(Landroid/net/Uri;)V

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x1

    invoke-interface {p6, v0}, Lcom/google/android/apps/ytremote/logic/e;->a(I)V

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x2

    invoke-interface {p6, v0}, Lcom/google/android/apps/ytremote/logic/e;->a(I)V

    goto :goto_0

    :sswitch_3
    const/4 v0, 0x0

    invoke-interface {p6, v0}, Lcom/google/android/apps/ytremote/logic/e;->a(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0x12f -> :sswitch_1
        0x194 -> :sswitch_2
        0x1f7 -> :sswitch_3
    .end sparse-switch
.end method
