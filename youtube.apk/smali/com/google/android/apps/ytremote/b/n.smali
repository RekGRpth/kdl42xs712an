.class final Lcom/google/android/apps/ytremote/b/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/ytremote/b/j;


# direct methods
.method constructor <init>(Lcom/google/android/apps/ytremote/b/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    const-wide/16 v0, 0x24b8

    iget-object v2, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-static {v2}, Lcom/google/android/apps/ytremote/b/j;->a(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v6}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    const-wide/16 v6, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v4, v8, v4

    sub-long v4, v1, v4

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v0

    move-wide v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/apps/ytremote/b/j;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Error waiting for reading device response task to complete"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/android/apps/ytremote/b/j;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Error waiting for reading device response task to complete"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v4

    invoke-static {}, Lcom/google/android/apps/ytremote/b/j;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Timed out waiting for device response"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->b(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-static {v1}, Lcom/google/android/apps/ytremote/b/j;->b(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    iget-object v4, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-static {v4}, Lcom/google/android/apps/ytremote/b/j;->c(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-static {v2}, Lcom/google/android/apps/ytremote/b/j;->b(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->d(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->c(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->e(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/n;->a:Lcom/google/android/apps/ytremote/b/j;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/ytremote/b/j;->a(Lcom/google/android/apps/ytremote/b/j;Z)Z

    return-void
.end method
