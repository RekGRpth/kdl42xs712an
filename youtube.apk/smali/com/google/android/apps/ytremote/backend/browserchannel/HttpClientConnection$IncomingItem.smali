.class final Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/nio/ByteBuffer;

.field private b:Ljava/lang/Exception;

.field private c:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->CLOSED:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->ERROR:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iput-object p1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->b:Ljava/lang/Exception;

    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->DATA:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iput-object p1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->a:Ljava/nio/ByteBuffer;

    return-void
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->DATA:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "data accessed when state was not DATA"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->a:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public final b()Ljava/lang/Exception;
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->ERROR:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "data accessed when state was not ERROR"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->b:Ljava/lang/Exception;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->CLOSED:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->ERROR:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c:Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
