.class final Lcom/google/android/apps/ytremote/backend/browserchannel/l;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/ytremote/backend/model/a;

.field final synthetic b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;


# direct methods
.method constructor <init>(Lcom/google/android/apps/ytremote/backend/browserchannel/k;Ljava/lang/String;Lcom/google/android/apps/ytremote/backend/model/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    iput-object p3, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->a:Lcom/google/android/apps/ytremote/backend/model/a;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Lcom/google/android/apps/ytremote/backend/browserchannel/k;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->b(Lcom/google/android/apps/ytremote/backend/browserchannel/k;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Lcom/google/android/apps/ytremote/backend/browserchannel/k;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->b(Lcom/google/android/apps/ytremote/backend/browserchannel/k;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Ljava/util/concurrent/CountDownLatch;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->c(Lcom/google/android/apps/ytremote/backend/browserchannel/k;)Lcom/google/android/apps/ytremote/backend/browserchannel/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->a:Lcom/google/android/apps/ytremote/backend/model/a;

    invoke-interface {v1, v2}, Lcom/google/android/apps/ytremote/backend/browserchannel/c;->a(Lcom/google/android/apps/ytremote/backend/model/a;)Lcom/google/android/apps/ytremote/backend/browserchannel/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Lcom/google/android/apps/ytremote/backend/browserchannel/k;Lcom/google/android/apps/ytremote/backend/browserchannel/a;)Lcom/google/android/apps/ytremote/backend/browserchannel/a;

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->d(Lcom/google/android/apps/ytremote/backend/browserchannel/k;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->e(Lcom/google/android/apps/ytremote/backend/browserchannel/k;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v0, v3}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->c(Lcom/google/android/apps/ytremote/backend/browserchannel/k;Z)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->f(Lcom/google/android/apps/ytremote/backend/browserchannel/k;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v1, v3}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->c(Lcom/google/android/apps/ytremote/backend/browserchannel/k;Z)V

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/l;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-static {v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->f(Lcom/google/android/apps/ytremote/backend/browserchannel/k;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
