.class Lcom/google/android/apps/ytremote/backend/a/m;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lorg/json/JSONObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/ytremote/backend/a/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/backend/a/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    const-string v2, "accessType"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-static {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->fromString(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    const-string v3, "name"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    const-string v3, "loungeToken"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    const-string v3, "screenId"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/apps/ytremote/backend/a/m;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "We got a permanent screen without a screen id. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/apps/ytremote/backend/a/m;->a:Ljava/lang/String;

    const-string v3, "Error parsing screen "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_3
    sget-object v3, Lcom/google/android/apps/ytremote/backend/a/m;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown access type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    new-instance v5, Lcom/google/android/apps/ytremote/model/ScreenId;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    const-string v3, "screenId"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/google/android/apps/ytremote/model/ScreenId;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    const-string v3, "loungeToken"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lcom/google/android/apps/ytremote/model/LoungeToken;

    invoke-direct {v1, v2}, Lcom/google/android/apps/ytremote/model/LoungeToken;-><init>(Ljava/lang/String;)V

    move-object v3, v1

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    const-string v2, "clientName"

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    new-instance v1, Lcom/google/android/apps/ytremote/model/ClientName;

    invoke-direct {v1, v2}, Lcom/google/android/apps/ytremote/model/ClientName;-><init>(Ljava/lang/String;)V

    move-object v2, v1

    :goto_2
    new-instance v1, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/google/android/apps/ytremote/model/CloudScreen;-><init>(Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ClientName;Lcom/google/android/apps/ytremote/model/LoungeToken;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    const-string v3, "loungeToken"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/google/android/apps/ytremote/backend/a/m;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "We got a temporary screen without a loungeToken. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/ytremote/backend/a/m;->b:Lorg/json/JSONObject;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    new-instance v3, Lcom/google/android/apps/ytremote/model/TemporaryAccessToken;

    invoke-direct {v3, v2}, Lcom/google/android/apps/ytremote/model/TemporaryAccessToken;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-direct {v1, v3, v4}, Lcom/google/android/apps/ytremote/model/CloudScreen;-><init>(Lcom/google/android/apps/ytremote/model/TemporaryAccessToken;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    move-object v2, v0

    goto :goto_2

    :cond_6
    move-object v3, v0

    goto :goto_1
.end method
