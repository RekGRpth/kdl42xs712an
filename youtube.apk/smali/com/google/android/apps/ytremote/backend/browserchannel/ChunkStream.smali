.class Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/backend/browserchannel/i;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

.field private c:I

.field private final d:Ljava/io/CharArrayWriter;

.field private final e:Ljava/io/CharArrayWriter;

.field private f:Lcom/google/android/apps/ytremote/backend/browserchannel/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;->ST_CHUNK_LENGTH:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    new-instance v0, Ljava/io/CharArrayWriter;

    invoke-direct {v0}, Ljava/io/CharArrayWriter;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->d:Ljava/io/CharArrayWriter;

    new-instance v0, Ljava/io/CharArrayWriter;

    invoke-direct {v0}, Ljava/io/CharArrayWriter;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->e:Ljava/io/CharArrayWriter;

    return-void
.end method

.method private a([CII)I
    .locals 6

    const/16 v2, 0xa

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p3, :cond_1

    add-int v0, p2, v1

    aget-char v0, p1, v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->d:Ljava/io/CharArrayWriter;

    invoke-virtual {v0, p1, p2, v1}, Ljava/io/CharArrayWriter;->write([CII)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->d:Ljava/io/CharArrayWriter;

    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v0, 0xa

    :try_start_0
    invoke-static {v2, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->c:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;->ST_CHUNK_BODY:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->d:Ljava/io/CharArrayWriter;

    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->reset()V

    add-int/lit8 p3, v1, 0x1

    :goto_1
    return p3

    :catch_0
    move-exception v0

    sget-object v3, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error parsing chunk length: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;->ST_CHUNK_LENGTH:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->d:Ljava/io/CharArrayWriter;

    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->reset()V

    add-int/lit8 p3, v1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->d:Ljava/io/CharArrayWriter;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/CharArrayWriter;->write([CII)V

    goto :goto_1
.end method


# virtual methods
.method public final a([C)I
    .locals 6

    const/4 v0, 0x0

    array-length v2, p1

    move v1, v2

    move v3, v0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/d;->a:[I

    iget-object v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    invoke-virtual {v4}, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :cond_0
    :goto_1
    add-int/2addr v3, v0

    sub-int/2addr v1, v0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p1, v3, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->a([CII)I

    move-result v0

    goto :goto_1

    :pswitch_1
    iget v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->c:I

    if-lt v1, v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->c:I

    sget-object v4, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;->ST_CHUNK_LENGTH:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    iput-object v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->e:Ljava/io/CharArrayWriter;

    invoke-virtual {v4, p1, v3, v0}, Ljava/io/CharArrayWriter;->write([CII)V

    iget v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->c:I

    sub-int/2addr v4, v0

    iput v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->c:I

    iget v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->c:I

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->f:Lcom/google/android/apps/ytremote/backend/browserchannel/e;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->f:Lcom/google/android/apps/ytremote/backend/browserchannel/e;

    iget-object v5, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->e:Ljava/io/CharArrayWriter;

    invoke-virtual {v5}, Ljava/io/CharArrayWriter;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/apps/ytremote/backend/browserchannel/e;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->e:Ljava/io/CharArrayWriter;

    invoke-virtual {v4}, Ljava/io/CharArrayWriter;->reset()V

    goto :goto_1

    :cond_2
    sub-int v0, v2, v1

    return v0

    :cond_3
    move v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)V
    .locals 3

    const/16 v0, 0x194

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/google/android/apps/ytremote/logic/exception/NotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/ytremote/logic/exception/NotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v0, 0xc8

    if-eq p1, v0, :cond_1

    new-instance v0, Lcom/google/android/apps/ytremote/backend/browserchannel/UnexpectedReponseCodeException;

    invoke-direct {v0, p1}, Lcom/google/android/apps/ytremote/backend/browserchannel/UnexpectedReponseCodeException;-><init>(I)V

    throw v0

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/backend/browserchannel/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->f:Lcom/google/android/apps/ytremote/backend/browserchannel/e;

    return-void
.end method

.method public final a([BII)V
    .locals 2

    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, p2, p3, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->a([C)I

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This application needs UTF-8 support in order to run"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected finalize()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;->ST_CHUNK_BODY:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->b:Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream$State;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/backend/browserchannel/ChunkStream;->a:Ljava/lang/String;

    const-string v1, "Lost partial chunk"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
