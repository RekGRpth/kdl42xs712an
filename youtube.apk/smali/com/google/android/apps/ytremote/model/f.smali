.class final Lcom/google/android/apps/ytremote/model/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 3

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/apps/ytremote/model/g;

    invoke-direct {v2}, Lcom/google/android/apps/ytremote/model/g;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/ytremote/model/g;->a(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/ytremote/model/g;->b(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/ytremote/model/g;->c(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/SsdpId;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/ytremote/model/g;->a(Lcom/google/android/apps/ytremote/model/SsdpId;)Lcom/google/android/apps/ytremote/model/g;

    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/ytremote/model/g;->a(Landroid/net/Uri;)Lcom/google/android/apps/ytremote/model/g;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/ytremote/model/g;->a(Z)Lcom/google/android/apps/ytremote/model/g;

    const-class v0, Lcom/google/android/apps/ytremote/model/AppStatus;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/AppStatus;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/ytremote/model/g;->a(Lcom/google/android/apps/ytremote/model/AppStatus;)Lcom/google/android/apps/ytremote/model/g;

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/model/g;->a()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    return-object v0
.end method
