.class public abstract Lcom/google/android/exoplayer/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/o;


# instance fields
.field public final a:Lcom/google/android/exoplayer/a/m;

.field public final b:I

.field public final c:J

.field private final d:Lcom/google/android/exoplayer/upstream/i;

.field private final e:Lcom/google/android/exoplayer/upstream/j;

.field private f:Lcom/google/android/exoplayer/upstream/DataSourceStream;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/a/m;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/exoplayer/e/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/i;

    iput-object v0, p0, Lcom/google/android/exoplayer/a/a;->d:Lcom/google/android/exoplayer/upstream/i;

    invoke-static {p2}, Lcom/google/android/exoplayer/e/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/j;

    iput-object v0, p0, Lcom/google/android/exoplayer/a/a;->e:Lcom/google/android/exoplayer/upstream/j;

    invoke-static {p3}, Lcom/google/android/exoplayer/e/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/m;

    iput-object v0, p0, Lcom/google/android/exoplayer/a/a;->a:Lcom/google/android/exoplayer/a/m;

    iput p4, p0, Lcom/google/android/exoplayer/a/a;->b:I

    iget-wide v0, p2, Lcom/google/android/exoplayer/upstream/j;->e:J

    iput-wide v0, p0, Lcom/google/android/exoplayer/a/a;->c:J

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/b;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    new-instance v0, Lcom/google/android/exoplayer/upstream/DataSourceStream;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/a;->d:Lcom/google/android/exoplayer/upstream/i;

    iget-object v2, p0, Lcom/google/android/exoplayer/a/a;->e:Lcom/google/android/exoplayer/upstream/j;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/exoplayer/upstream/DataSourceStream;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/upstream/b;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/google/android/exoplayer/upstream/p;)V
    .locals 0

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->c()Z

    move-result v0

    return v0
.end method

.method public final c()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/a/a;->a(Lcom/google/android/exoplayer/upstream/p;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final e()Lcom/google/android/exoplayer/upstream/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    return-object v0
.end method

.method protected final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a()V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g()V

    return-void
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h()Z

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/a;->f:Lcom/google/android/exoplayer/upstream/DataSourceStream;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->i()V

    return-void
.end method
