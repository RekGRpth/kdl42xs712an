.class public abstract Lcom/google/android/exoplayer/a/t;
.super Lcom/google/android/exoplayer/a/a;
.source "SourceFile"


# instance fields
.field public final d:J

.field public final e:J

.field public final f:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/a/m;IJJI)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/exoplayer/a/a;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/a/m;I)V

    iput-wide p5, p0, Lcom/google/android/exoplayer/a/t;->d:J

    iput-wide p7, p0, Lcom/google/android/exoplayer/a/t;->e:J

    iput p9, p0, Lcom/google/android/exoplayer/a/t;->f:I

    return-void
.end method


# virtual methods
.method public abstract a(JZ)Z
.end method

.method public abstract a(Lcom/google/android/exoplayer/ah;)Z
.end method

.method public final j()Z
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer/a/t;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract k()Lcom/google/android/exoplayer/ag;
.end method

.method public abstract l()Ljava/util/Map;
.end method
