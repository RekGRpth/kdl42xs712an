.class public final Lcom/google/android/exoplayer/a/u;
.super Lcom/google/android/exoplayer/a/t;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/exoplayer/d/a/f;

.field private final h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/a/m;ILcom/google/android/exoplayer/d/a/f;JJIZ)V
    .locals 10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-wide/from16 v5, p6

    move-wide/from16 v7, p8

    move/from16 v9, p10

    invoke-direct/range {v0 .. v9}, Lcom/google/android/exoplayer/a/t;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/a/m;IJJI)V

    iput-object p5, p0, Lcom/google/android/exoplayer/a/u;->g:Lcom/google/android/exoplayer/d/a/f;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/a/u;->h:Z

    return-void
.end method


# virtual methods
.method public final a(JZ)Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer/a/u;->h:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/a/u;->d:J

    sub-long/2addr p1, v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/a/u;->g:Lcom/google/android/exoplayer/d/a/f;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer/d/a/f;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/exoplayer/a/u;->f()V

    :cond_1
    return v0
.end method

.method public final a(Lcom/google/android/exoplayer/ah;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/a/u;->e()Lcom/google/android/exoplayer/upstream/p;

    move-result-object v3

    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/u;->g:Lcom/google/android/exoplayer/d/a/f;

    invoke-virtual {v0, v3, p1}, Lcom/google/android/exoplayer/d/a/f;->a(Lcom/google/android/exoplayer/upstream/p;Lcom/google/android/exoplayer/ah;)I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer/a/u;->h:Z

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-wide v2, p1, Lcom/google/android/exoplayer/ah;->f:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/a/u;->d:J

    add-long/2addr v2, v4

    iput-wide v2, p1, Lcom/google/android/exoplayer/ah;->f:J

    :cond_0
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final k()Lcom/google/android/exoplayer/ag;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/u;->g:Lcom/google/android/exoplayer/d/a/f;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/f;->c()Lcom/google/android/exoplayer/ag;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/u;->g:Lcom/google/android/exoplayer/d/a/f;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/f;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
