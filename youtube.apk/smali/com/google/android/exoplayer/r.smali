.class public final Lcom/google/android/exoplayer/r;
.super Lcom/google/android/exoplayer/w;
.source "SourceFile"


# instance fields
.field private A:J

.field private B:F

.field private C:[B

.field private D:I

.field private E:I

.field private final c:Landroid/os/ConditionVariable;

.field private final d:Lcom/google/android/exoplayer/t;

.field private final e:[J

.field private f:I

.field private g:I

.field private h:J

.field private i:J

.field private j:Z

.field private k:J

.field private l:J

.field private m:J

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:Landroid/media/AudioTrack;

.field private t:Ljava/lang/reflect/Method;

.field private u:I

.field private v:J

.field private w:Z

.field private x:J

.field private y:J

.field private z:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0, v0}, Lcom/google/android/exoplayer/r;-><init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;Landroid/os/Handler;Lcom/google/android/exoplayer/z;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;Landroid/os/Handler;Lcom/google/android/exoplayer/z;)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/exoplayer/w;-><init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;Landroid/os/Handler;Lcom/google/android/exoplayer/z;)V

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->c:Landroid/os/ConditionVariable;

    sget v0, Lcom/google/android/exoplayer/e/k;->a:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/google/android/exoplayer/u;

    invoke-direct {v0}, Lcom/google/android/exoplayer/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->d:Lcom/google/android/exoplayer/t;

    :goto_0
    sget v0, Lcom/google/android/exoplayer/e/k;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    :try_start_0
    const-class v0, Landroid/media/AudioTrack;

    const-string v1, "getLatency"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->t:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/16 v0, 0xa

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->e:[J

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/exoplayer/r;->B:F

    return-void

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer/v;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/v;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->d:Lcom/google/android/exoplayer/t;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/exoplayer/r;)Landroid/os/ConditionVariable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->c:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method private c(J)J
    .locals 4

    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, Lcom/google/android/exoplayer/r;->n:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private v()V
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    if-eqz v0, :cond_1

    iput-wide v1, p0, Lcom/google/android/exoplayer/r;->v:J

    iput v3, p0, Lcom/google/android/exoplayer/r;->E:I

    iput-wide v1, p0, Lcom/google/android/exoplayer/r;->l:J

    iput-wide v1, p0, Lcom/google/android/exoplayer/r;->m:J

    iput-wide v1, p0, Lcom/google/android/exoplayer/r;->x:J

    iput-boolean v3, p0, Lcom/google/android/exoplayer/r;->w:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->w()V

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    iget-object v1, p0, Lcom/google/android/exoplayer/r;->c:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->close()V

    new-instance v1, Lcom/google/android/exoplayer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/android/exoplayer/s;-><init>(Lcom/google/android/exoplayer/r;Landroid/media/AudioTrack;)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/s;->start()V

    :cond_1
    return-void
.end method

.method private w()V
    .locals 3

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    iput-wide v1, p0, Lcom/google/android/exoplayer/r;->h:J

    iput v0, p0, Lcom/google/android/exoplayer/r;->g:I

    iput v0, p0, Lcom/google/android/exoplayer/r;->f:I

    iput-wide v1, p0, Lcom/google/android/exoplayer/r;->i:J

    iput-boolean v0, p0, Lcom/google/android/exoplayer/r;->j:Z

    iput-wide v1, p0, Lcom/google/android/exoplayer/r;->k:J

    return-void
.end method

.method private x()J
    .locals 2

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->y()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/r;->c(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private y()J
    .locals 6

    const-wide v0, 0xffffffffL

    iget-object v2, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v2

    int-to-long v2, v2

    and-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->l:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->m:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer/r;->m:J

    :cond_0
    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->l:J

    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->m:J

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private z()I
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer/r;->v:J

    iget v2, p0, Lcom/google/android/exoplayer/r;->o:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->y()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/r;->B:F

    iget-object v1, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-virtual {v1, v0, v0}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/w;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected final a(J)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/w;->a(J)V

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->v()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->z:J

    return-void
.end method

.method protected final a(JZ)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/exoplayer/w;->a(JZ)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->z:J

    return-void
.end method

.method protected final a(Landroid/media/MediaFormat;)V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->v()V

    const-string v0, "sample-rate"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/r;->n:I

    const-string v0, "channel-count"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported channel count: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v0, 0x4

    :goto_0
    iput v0, p0, Lcom/google/android/exoplayer/r;->p:I

    iget v2, p0, Lcom/google/android/exoplayer/r;->n:I

    const/4 v3, 0x2

    invoke-static {v2, v0, v3}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/r;->q:I

    iget v0, p0, Lcom/google/android/exoplayer/r;->q:I

    mul-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/exoplayer/r;->r:I

    mul-int/lit8 v0, v1, 0x2

    iput v0, p0, Lcom/google/android/exoplayer/r;->o:I

    return-void

    :pswitch_2
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xfc

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(JLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;I)Z
    .locals 8

    iget-boolean v0, p0, Lcom/google/android/exoplayer/r;->w:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    iget-wide v2, p5, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget v4, p5, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget v5, p0, Lcom/google/android/exoplayer/r;->o:I

    div-int/2addr v4, v5

    int-to-long v4, v4

    invoke-direct {p0, v4, v5}, Lcom/google/android/exoplayer/r;->c(J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->x:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/r;->w:Z

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer/r;->E:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->C:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->C:[B

    array-length v0, v0

    iget v1, p5, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-ge v0, v1, :cond_2

    :cond_1
    iget v0, p5, Landroid/media/MediaCodec$BufferInfo;->size:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->C:[B

    :cond_2
    iget v0, p5, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {p4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->C:[B

    const/4 v1, 0x0

    iget v2, p5, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {p4, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/r;->D:I

    iget v0, p5, Landroid/media/MediaCodec$BufferInfo;->size:I

    iput v0, p0, Lcom/google/android/exoplayer/r;->E:I

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->c:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    iget v0, p0, Lcom/google/android/exoplayer/r;->u:I

    if-nez v0, :cond_5

    new-instance v0, Landroid/media/AudioTrack;

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/exoplayer/r;->n:I

    iget v3, p0, Lcom/google/android/exoplayer/r;->p:I

    const/4 v4, 0x2

    iget v5, p0, Lcom/google/android/exoplayer/r;->r:I

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/r;->u:I

    iget v0, p0, Lcom/google/android/exoplayer/r;->u:I

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    iget v1, p0, Lcom/google/android/exoplayer/r;->B:F

    iget v2, p0, Lcom/google/android/exoplayer/r;->B:F

    invoke-virtual {v0, v1, v2}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    invoke-virtual {p0}, Lcom/google/android/exoplayer/r;->p()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->y:J

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    :cond_4
    iget-wide v0, p0, Lcom/google/android/exoplayer/r;->v:J

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->y()J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/exoplayer/r;->o:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iget v1, p0, Lcom/google/android/exoplayer/r;->r:I

    sub-int v0, v1, v0

    if-lez v0, :cond_6

    iget v1, p0, Lcom/google/android/exoplayer/r;->E:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    iget-object v2, p0, Lcom/google/android/exoplayer/r;->C:[B

    iget v3, p0, Lcom/google/android/exoplayer/r;->D:I

    invoke-virtual {v1, v2, v3, v0}, Landroid/media/AudioTrack;->write([BII)I

    iget v1, p0, Lcom/google/android/exoplayer/r;->D:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/exoplayer/r;->D:I

    iget v1, p0, Lcom/google/android/exoplayer/r;->E:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/exoplayer/r;->E:I

    iget-wide v1, p0, Lcom/google/android/exoplayer/r;->v:J

    int-to-long v3, v0

    add-long v0, v1, v3

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->v:J

    iget v0, p0, Lcom/google/android/exoplayer/r;->E:I

    if-nez v0, :cond_6

    const/4 v0, 0x0

    invoke-virtual {p3, p6, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->i:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->i:J

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_5
    new-instance v0, Landroid/media/AudioTrack;

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/exoplayer/r;->n:I

    iget v3, p0, Lcom/google/android/exoplayer/r;->p:I

    const/4 v4, 0x2

    iget v5, p0, Lcom/google/android/exoplayer/r;->r:I

    const/4 v6, 0x1

    iget v7, p0, Lcom/google/android/exoplayer/r;->u:I

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer/e/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/exoplayer/w;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/exoplayer/w;->b()V

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->v()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/r;->u:I

    return-void
.end method

.method protected final b(J)V
    .locals 10

    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/w;->b(J)V

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/exoplayer/r;->w:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/r;->p()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->x()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/exoplayer/r;->i:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x7530

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/exoplayer/r;->e:[J

    iget v5, p0, Lcom/google/android/exoplayer/r;->f:I

    sub-long/2addr v0, v2

    aput-wide v0, v4, v5

    iget v0, p0, Lcom/google/android/exoplayer/r;->f:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0xa

    iput v0, p0, Lcom/google/android/exoplayer/r;->f:I

    iget v0, p0, Lcom/google/android/exoplayer/r;->g:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/exoplayer/r;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/r;->g:I

    :cond_2
    iput-wide v2, p0, Lcom/google/android/exoplayer/r;->i:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->h:J

    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/google/android/exoplayer/r;->g:I

    if-ge v0, v1, :cond_3

    iget-wide v4, p0, Lcom/google/android/exoplayer/r;->h:J

    iget-object v1, p0, Lcom/google/android/exoplayer/r;->e:[J

    aget-wide v6, v1, v0

    iget v1, p0, Lcom/google/android/exoplayer/r;->g:I

    int-to-long v8, v1

    div-long/2addr v6, v8

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/exoplayer/r;->h:J

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-wide v0, p0, Lcom/google/android/exoplayer/r;->k:J

    sub-long v0, v2, v0

    const-wide/32 v4, 0x7a120

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->d:Lcom/google/android/exoplayer/t;

    iget-object v1, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/t;->a(Landroid/media/AudioTrack;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/r;->j:Z

    iget-boolean v0, p0, Lcom/google/android/exoplayer/r;->j:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->d:Lcom/google/android/exoplayer/t;

    invoke-interface {v0}, Lcom/google/android/exoplayer/t;->a()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    iget-wide v4, p0, Lcom/google/android/exoplayer/r;->y:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/r;->j:Z

    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer/r;->t:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/r;->t:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    iget v4, p0, Lcom/google/android/exoplayer/r;->r:I

    iget v5, p0, Lcom/google/android/exoplayer/r;->o:I

    div-int/2addr v4, v5

    int-to-long v4, v4

    invoke-direct {p0, v4, v5}, Lcom/google/android/exoplayer/r;->c(J)J

    move-result-wide v4

    sub-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->A:J

    iget-wide v0, p0, Lcom/google/android/exoplayer/r;->A:J

    const-wide/16 v4, 0x0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->A:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_2
    iput-wide v2, p0, Lcom/google/android/exoplayer/r;->k:J

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/r;->t:Ljava/lang/reflect/Method;

    goto :goto_2
.end method

.method protected final d()J
    .locals 6

    const-wide/16 v4, 0x3e8

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    div-long/2addr v0, v4

    iget-object v2, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/exoplayer/r;->w:Z

    if-nez v2, :cond_2

    :cond_0
    invoke-super {p0}, Lcom/google/android/exoplayer/w;->d()J

    move-result-wide v0

    :cond_1
    :goto_0
    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->z:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->z:J

    return-wide v0

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/exoplayer/r;->j:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer/r;->d:Lcom/google/android/exoplayer/t;

    invoke-interface {v2}, Lcom/google/android/exoplayer/t;->a()J

    move-result-wide v2

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/exoplayer/r;->n:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/exoplayer/r;->d:Lcom/google/android/exoplayer/t;

    invoke-interface {v2}, Lcom/google/android/exoplayer/t;->b()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/r;->c(J)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->x:J

    add-long/2addr v0, v2

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/google/android/exoplayer/r;->g:I

    if-nez v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->x()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->x:J

    add-long/2addr v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer/r;->i()Z

    move-result v2

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->A:J

    sub-long/2addr v0, v2

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->h:J

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/exoplayer/r;->x:J

    add-long/2addr v0, v2

    goto :goto_1
.end method

.method protected final g()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/exoplayer/w;->g()V

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/r;->y:J

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    :cond_0
    return-void
.end method

.method protected final h()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/exoplayer/w;->h()V

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->w()V

    iget-object v0, p0, Lcom/google/android/exoplayer/r;->s:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    :cond_0
    return-void
.end method

.method protected final i()Z
    .locals 4

    invoke-super {p0}, Lcom/google/android/exoplayer/w;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->z()I

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/r;->v:J

    iget v2, p0, Lcom/google/android/exoplayer/r;->q:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final j()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/exoplayer/r;->z()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final k()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
