.class public final Lcom/google/android/exoplayer/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:Ljava/util/List;

.field private h:I

.field private i:I

.field private j:I

.field private k:Landroid/media/MediaFormat;


# direct methods
.method private constructor <init>(Ljava/lang/String;IIIIILjava/util/List;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/exoplayer/ag;->b:I

    iput p3, p0, Lcom/google/android/exoplayer/ag;->c:I

    iput p4, p0, Lcom/google/android/exoplayer/ag;->d:I

    iput p5, p0, Lcom/google/android/exoplayer/ag;->e:I

    iput p6, p0, Lcom/google/android/exoplayer/ag;->f:I

    if-nez p7, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p7

    :cond_0
    iput-object p7, p0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    iput v0, p0, Lcom/google/android/exoplayer/ag;->h:I

    iput v0, p0, Lcom/google/android/exoplayer/ag;->i:I

    return-void
.end method

.method public static a(Ljava/lang/String;IIILjava/util/List;)Lcom/google/android/exoplayer/ag;
    .locals 8

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/exoplayer/ag;

    move-object v1, p0

    move v3, p2

    move v4, p3

    move v5, v2

    move v6, v2

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer/ag;-><init>(Ljava/lang/String;IIIIILjava/util/List;)V

    return-object v0
.end method

.method private final a(Landroid/media/MediaFormat;)V
    .locals 2

    const-string v0, "max-width"

    iget v1, p0, Lcom/google/android/exoplayer/ag;->h:I

    invoke-static {p1, v0, v1}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "max-height"

    iget v1, p0, Lcom/google/android/exoplayer/ag;->i:I

    invoke-static {p1, v0, v1}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    return-void
.end method

.method private static final a(Landroid/media/MediaFormat;Ljava/lang/String;I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    invoke-virtual {p0, p1, p2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;IIILjava/util/List;)Lcom/google/android/exoplayer/ag;
    .locals 8

    const/4 v3, -0x1

    new-instance v0, Lcom/google/android/exoplayer/ag;

    move-object v1, p0

    move v2, p1

    move v4, v3

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer/ag;-><init>(Ljava/lang/String;IIIIILjava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/media/MediaFormat;
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->k:Landroid/media/MediaFormat;

    if-nez v0, :cond_1

    new-instance v2, Landroid/media/MediaFormat;

    invoke-direct {v2}, Landroid/media/MediaFormat;-><init>()V

    const-string v0, "mime"

    iget-object v1, p0, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "max-input-size"

    iget v1, p0, Lcom/google/android/exoplayer/ag;->b:I

    invoke-static {v2, v0, v1}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "width"

    iget v1, p0, Lcom/google/android/exoplayer/ag;->c:I

    invoke-static {v2, v0, v1}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "height"

    iget v1, p0, Lcom/google/android/exoplayer/ag;->d:I

    invoke-static {v2, v0, v1}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "channel-count"

    iget v1, p0, Lcom/google/android/exoplayer/ag;->e:I

    invoke-static {v2, v0, v1}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "sample-rate"

    iget v1, p0, Lcom/google/android/exoplayer/ag;->f:I

    invoke-static {v2, v0, v1}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "csd-"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/media/MediaFormat;->setByteBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;)V

    iput-object v2, p0, Lcom/google/android/exoplayer/ag;->k:Landroid/media/MediaFormat;

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->k:Landroid/media/MediaFormat;

    return-object v0
.end method

.method public final a(II)V
    .locals 1

    iput p1, p0, Lcom/google/android/exoplayer/ag;->h:I

    iput p2, p0, Lcom/google/android/exoplayer/ag;->i:I

    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->k:Landroid/media/MediaFormat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->k:Landroid/media/MediaFormat;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/ag;->a(Landroid/media/MediaFormat;)V

    :cond_0
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-ne p0, p1, :cond_1

    move v3, v4

    :cond_0
    :goto_0
    return v3

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    check-cast p1, Lcom/google/android/exoplayer/ag;

    iget v0, p0, Lcom/google/android/exoplayer/ag;->b:I

    iget v1, p1, Lcom/google/android/exoplayer/ag;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ag;->c:I

    iget v1, p1, Lcom/google/android/exoplayer/ag;->c:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ag;->d:I

    iget v1, p1, Lcom/google/android/exoplayer/ag;->d:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ag;->h:I

    iget v1, p1, Lcom/google/android/exoplayer/ag;->h:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ag;->i:I

    iget v1, p1, Lcom/google/android/exoplayer/ag;->i:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ag;->e:I

    iget v1, p1, Lcom/google/android/exoplayer/ag;->e:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ag;->f:I

    iget v1, p1, Lcom/google/android/exoplayer/ag;->f:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/exoplayer/e/k;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p1, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v2, v3

    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v1, p1, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/exoplayer/ag;->j:I

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "527"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer/ag;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer/ag;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer/ag;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer/ag;->h:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer/ag;->i:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer/ag;->e:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/exoplayer/ag;->f:I

    add-int/2addr v0, v2

    :goto_1
    iget-object v2, p0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iput v0, p0, Lcom/google/android/exoplayer/ag;->j:I

    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer/ag;->j:I

    return v0
.end method
