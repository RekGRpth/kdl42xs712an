.class public Lcom/google/android/exoplayer/upstream/HttpDataSource;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/i;


# static fields
.field public static final a:Lcom/google/android/exoplayer/e/d;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private final d:I

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/exoplayer/e/d;

.field private final h:Ljava/util/HashMap;

.field private final i:Lcom/google/android/exoplayer/upstream/l;

.field private j:Ljava/net/HttpURLConnection;

.field private k:Ljava/io/InputStream;

.field private l:Z

.field private m:J

.field private n:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/exoplayer/upstream/k;

    invoke-direct {v0}, Lcom/google/android/exoplayer/upstream/k;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->a:Lcom/google/android/exoplayer/e/d;

    const-class v0, Lcom/google/android/exoplayer/upstream/HttpDataSource;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b:Ljava/lang/String;

    const-string v0, "^bytes (\\d+)-(\\d+)/(\\d+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/exoplayer/e/d;Lcom/google/android/exoplayer/upstream/l;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->f:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->g:Lcom/google/android/exoplayer/e/d;

    iput-object p3, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->h:Ljava/util/HashMap;

    iput p4, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->d:I

    iput p5, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->e:I

    return-void
.end method

.method private static a(Ljava/net/HttpURLConnection;)J
    .locals 11

    const-wide/16 v2, -0x1

    const-string v0, "Content-Length"

    invoke-virtual {p0, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :try_start_0
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    const-string v4, "Content-Range"

    invoke-virtual {p0, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/google/android/exoplayer/upstream/HttpDataSource;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x2

    :try_start_1
    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v4

    sub-long v4, v8, v4

    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-gez v8, :cond_3

    move-wide v0, v4

    :cond_0
    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to parse content length ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-wide v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected Content-Length ["

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "]"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-wide v0, v2

    goto :goto_0

    :cond_3
    cmp-long v8, v0, v4

    if-eqz v8, :cond_0

    :try_start_2
    sget-object v8, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Inconsistent headers ["

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-wide v0

    goto :goto_1

    :catch_1
    move-exception v4

    sget-object v4, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Unexpected Content-Range ["

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "]"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private b(Lcom/google/android/exoplayer/upstream/j;)Ljava/net/HttpURLConnection;
    .locals 7

    new-instance v0, Ljava/net/URL;

    iget-object v1, p1, Lcom/google/android/exoplayer/upstream/j;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iget v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->d:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    iget v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->e:I

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    iget-object v3, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->h:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->h:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v1, "Accept-Encoding"

    const-string v2, "deflate"

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "User-Agent"

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Range"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "bytes="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p1, Lcom/google/android/exoplayer/upstream/j;->d:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "-"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v3, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p1, Lcom/google/android/exoplayer/upstream/j;->d:J

    iget-wide v5, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    add-long/2addr v3, v5

    const-wide/16 v5, 0x1

    sub-long/2addr v3, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {v0, v2, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    return-object v0
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->k:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-lez v0, :cond_1

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->n:J

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->n:J

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/upstream/l;->a(I)V

    :cond_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_1
    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->m:J

    iget-wide v3, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->n:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    new-instance v1, Lcom/google/android/exoplayer/upstream/UnexpectedLengthException;

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->m:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->n:J

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/exoplayer/upstream/UnexpectedLengthException;-><init>(JJ)V

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/j;)J
    .locals 6

    const-wide/16 v4, -0x1

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->n:J

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b(Lcom/google/android/exoplayer/upstream/j;)Ljava/net/HttpURLConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    const/16 v1, 0xc8

    if-lt v0, v1, :cond_0

    const/16 v1, 0x12b

    if-le v0, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b()V

    new-instance v2, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidResponseCodeException;

    invoke-direct {v2, v0, v1}, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidResponseCodeException;-><init>(ILjava/util/Map;)V

    throw v2

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;-><init>(Ljava/io/IOException;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->g:Lcom/google/android/exoplayer/e/d;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->g:Lcom/google/android/exoplayer/e/d;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/e/d;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b()V

    new-instance v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidContentTypeException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidContentTypeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;

    invoke-static {v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->a(Ljava/net/HttpURLConnection;)J

    move-result-wide v2

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    move-wide v0, v2

    :goto_0
    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->m:J

    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->m:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    new-instance v1, Lcom/google/android/exoplayer/upstream/UnexpectedLengthException;

    invoke-direct {v1, v4, v5, v4, v5}, Lcom/google/android/exoplayer/upstream/UnexpectedLengthException;-><init>(JJ)V

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_3
    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    goto :goto_0

    :cond_4
    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    cmp-long v0, v2, v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b()V

    new-instance v0, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    new-instance v1, Lcom/google/android/exoplayer/upstream/UnexpectedLengthException;

    iget-wide v4, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    invoke-direct {v1, v4, v5, v2, v3}, Lcom/google/android/exoplayer/upstream/UnexpectedLengthException;-><init>(JJ)V

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->j:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->k:Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->l:Z

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/l;->a()V

    :cond_6
    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->m:J

    return-wide v0

    :catch_2
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b()V

    new-instance v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a()V
    .locals 3

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->k:Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->k:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->k:Ljava/io/InputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->l:Z

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->l:Z

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/l;->b()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b()V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;-><init>(Ljava/io/IOException;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    iget-boolean v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->l:Z

    if-eqz v1, :cond_4

    iput-boolean v2, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->l:Z

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/HttpDataSource;->i:Lcom/google/android/exoplayer/upstream/l;

    invoke-interface {v1}, Lcom/google/android/exoplayer/upstream/l;->b()V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->b()V

    :cond_4
    throw v0
.end method
