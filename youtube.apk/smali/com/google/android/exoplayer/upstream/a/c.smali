.class public final Lcom/google/android/exoplayer/upstream/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljavax/crypto/Cipher;

.field private final b:I

.field private final c:[B

.field private final d:[B

.field private e:I


# direct methods
.method public constructor <init>(I[BJJ)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    const-string v0, "AES/CTR/NoPadding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->a:Ljavax/crypto/Cipher;

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->a:Ljavax/crypto/Cipher;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    iget v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->c:[B

    iget v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->d:[B

    iget v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    int-to-long v0, v0

    div-long v0, p5, v0

    iget v2, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    int-to-long v2, v2

    rem-long v2, p5, v2

    long-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/exoplayer/upstream/a/c;->a:Ljavax/crypto/Cipher;

    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v5, p0, Lcom/google/android/exoplayer/upstream/a/c;->a:Ljavax/crypto/Cipher;

    invoke-virtual {v5}, Ljavax/crypto/Cipher;->getAlgorithm()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-direct {v4, p2, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    const/16 v6, 0x10

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6, p3, p4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-direct {v5, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v3, p1, v4, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    if-eqz v2, :cond_0

    new-array v0, v2, [B

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/exoplayer/upstream/a/c;->a([BII)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_3

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b([BII[BI)I
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->a:Ljavax/crypto/Cipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->update([BII[BI)I
    :try_end_0
    .catch Ljavax/crypto/ShortBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a([BII)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer/upstream/a/c;->a([BII[BI)V

    return-void
.end method

.method public final a([BII[BI)V
    .locals 10

    const/4 v6, 0x1

    const/4 v7, 0x0

    move v5, p5

    move v3, p3

    move v2, p2

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->e:I

    if-lez v0, :cond_2

    aget-byte v0, p1, v2

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/a/c;->d:[B

    iget v4, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    iget v8, p0, Lcom/google/android/exoplayer/upstream/a/c;->e:I

    sub-int/2addr v4, v8

    aget-byte v1, v1, v4

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    aput-byte v0, p4, v5

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v2, v2, 0x1

    iget v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->e:I

    add-int/lit8 v3, v3, -0x1

    if-nez v3, :cond_0

    :cond_1
    return-void

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer/upstream/a/c;->b([BII[BI)I

    move-result v1

    if-eq v3, v1, :cond_1

    sub-int v9, v3, v1

    iget v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    if-ge v9, v0, :cond_3

    move v0, v6

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    add-int v8, v5, v1

    iget v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    sub-int/2addr v0, v9

    iput v0, p0, Lcom/google/android/exoplayer/upstream/a/c;->e:I

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/a/c;->c:[B

    iget v3, p0, Lcom/google/android/exoplayer/upstream/a/c;->e:I

    iget-object v4, p0, Lcom/google/android/exoplayer/upstream/a/c;->d:[B

    move-object v0, p0

    move v2, v7

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer/upstream/a/c;->b([BII[BI)I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer/upstream/a/c;->b:I

    if-ne v0, v1, :cond_4

    :goto_1
    invoke-static {v6}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    move v0, v8

    :goto_2
    if-ge v7, v9, :cond_1

    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/a/c;->d:[B

    aget-byte v2, v2, v7

    aput-byte v2, p4, v0

    add-int/lit8 v7, v7, 0x1

    move v0, v1

    goto :goto_2

    :cond_3
    move v0, v7

    goto :goto_0

    :cond_4
    move v6, v7

    goto :goto_1
.end method
