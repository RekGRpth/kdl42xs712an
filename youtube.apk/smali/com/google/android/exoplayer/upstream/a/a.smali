.class public final Lcom/google/android/exoplayer/upstream/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/h;


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/h;

.field private final b:[B

.field private final c:[B

.field private d:Lcom/google/android/exoplayer/upstream/a/c;


# direct methods
.method public constructor <init>([B[BLcom/google/android/exoplayer/upstream/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/exoplayer/upstream/a/a;->a:Lcom/google/android/exoplayer/upstream/h;

    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/a/a;->b:[B

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/a/a;->c:[B

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/exoplayer/upstream/j;)Lcom/google/android/exoplayer/upstream/h;
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->a:Lcom/google/android/exoplayer/upstream/h;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/upstream/h;->a(Lcom/google/android/exoplayer/upstream/j;)Lcom/google/android/exoplayer/upstream/h;

    iget-object v0, p1, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/exoplayer/upstream/a/d;->a(Ljava/lang/String;)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/exoplayer/upstream/a/c;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/a/a;->b:[B

    iget-wide v5, p1, Lcom/google/android/exoplayer/upstream/j;->c:J

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/a/c;-><init>(I[BJJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->d:Lcom/google/android/exoplayer/upstream/a/c;

    return-object p0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->d:Lcom/google/android/exoplayer/upstream/a/c;

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->a:Lcom/google/android/exoplayer/upstream/h;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/h;->a()V

    return-void
.end method

.method public final a([BII)V
    .locals 7

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->c:[B

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->d:Lcom/google/android/exoplayer/upstream/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/a/c;->a([BII)V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->a:Lcom/google/android/exoplayer/upstream/h;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/h;->a([BII)V

    :cond_0
    return-void

    :cond_1
    move v6, v5

    :goto_0
    if-ge v6, p3, :cond_0

    sub-int v0, p3, v6

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/a/a;->c:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->d:Lcom/google/android/exoplayer/upstream/a/c;

    add-int v2, p2, v6

    iget-object v4, p0, Lcom/google/android/exoplayer/upstream/a/a;->c:[B

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/exoplayer/upstream/a/c;->a([BII[BI)V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/a;->a:Lcom/google/android/exoplayer/upstream/h;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/a/a;->c:[B

    invoke-interface {v0, v1, v5, v3}, Lcom/google/android/exoplayer/upstream/h;->a([BII)V

    add-int v0, v6, v3

    move v6, v0

    goto :goto_0
.end method
