.class public final Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/h;


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/cache/a;

.field private final b:J

.field private c:Lcom/google/android/exoplayer/upstream/j;

.field private d:Ljava/io/File;

.field private e:Ljava/io/FileOutputStream;

.field private f:J

.field private g:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/cache/a;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/exoplayer/e/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    const-wide/32 v0, 0x500000

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->b:J

    return-void
.end method

.method private b()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->c:Lcom/google/android/exoplayer/upstream/j;

    iget-object v1, v1, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->c:Lcom/google/android/exoplayer/upstream/j;

    iget-wide v2, v2, Lcom/google/android/exoplayer/upstream/j;->c:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->g:J

    add-long/2addr v2, v4

    iget-object v4, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->c:Lcom/google/android/exoplayer/upstream/j;

    iget-wide v4, v4, Lcom/google/android/exoplayer/upstream/j;->e:J

    iget-wide v6, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->g:J

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->b:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;JJ)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->d:Ljava/io/File;

    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->d:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->e:Ljava/io/FileOutputStream;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->f:J

    return-void
.end method

.method private c()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->e:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->e:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->e:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    iput-object v2, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->e:Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->d:Ljava/io/File;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/io/File;)V

    iput-object v2, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->d:Ljava/io/File;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/exoplayer/upstream/j;)Lcom/google/android/exoplayer/upstream/h;
    .locals 2

    :try_start_0
    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->c:Lcom/google/android/exoplayer/upstream/j;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->g:J

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->b()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a()V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a([BII)V
    .locals 7

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_1

    :try_start_0
    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->f:J

    iget-wide v3, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->c()V

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->b()V

    :cond_0
    sub-int v1, p3, v0

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->b:J

    iget-wide v5, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->f:J

    sub-long/2addr v3, v5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->e:Ljava/io/FileOutputStream;

    add-int v3, p2, v0

    invoke-virtual {v2, p1, v3, v1}, Ljava/io/FileOutputStream;->write([BII)V

    add-int/2addr v0, v1

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->f:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->f:J

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->g:J

    int-to-long v4, v1

    add-long v1, v2, v4

    iput-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink;->g:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_1
    return-void
.end method
