.class public final Lcom/google/android/exoplayer/upstream/cache/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/i;


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/cache/a;

.field private final b:Lcom/google/android/exoplayer/upstream/i;

.field private final c:Lcom/google/android/exoplayer/upstream/i;

.field private final d:Lcom/google/android/exoplayer/upstream/i;

.field private final e:Z

.field private final f:Z

.field private g:Lcom/google/android/exoplayer/upstream/i;

.field private h:Landroid/net/Uri;

.field private i:Ljava/lang/String;

.field private j:J

.field private k:J

.field private l:Lcom/google/android/exoplayer/upstream/cache/d;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/h;ZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object p3, p0, Lcom/google/android/exoplayer/upstream/cache/b;->b:Lcom/google/android/exoplayer/upstream/i;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->e:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->f:Z

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->d:Lcom/google/android/exoplayer/upstream/i;

    if-eqz p4, :cond_0

    new-instance v0, Lcom/google/android/exoplayer/upstream/q;

    invoke-direct {v0, p2, p4}, Lcom/google/android/exoplayer/upstream/q;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/h;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->c:Lcom/google/android/exoplayer/upstream/i;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->c:Lcom/google/android/exoplayer/upstream/i;

    goto :goto_0
.end method

.method private a(Ljava/io/IOException;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->g:Lcom/google/android/exoplayer/upstream/i;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->b:Lcom/google/android/exoplayer/upstream/i;

    if-eq v0, v1, :cond_0

    instance-of v0, p1, Lcom/google/android/exoplayer/upstream/cache/CacheDataSink$CacheDataSinkException;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->m:Z

    :cond_1
    return-void
.end method

.method private b()V
    .locals 9

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->d:Lcom/google/android/exoplayer/upstream/i;

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->g:Lcom/google/android/exoplayer/upstream/i;

    new-instance v0, Lcom/google/android/exoplayer/upstream/j;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->h:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/cache/b;->k:J

    iget-object v6, p0, Lcom/google/android/exoplayer/upstream/cache/b;->i:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->g:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/upstream/i;->a(Lcom/google/android/exoplayer/upstream/j;)J

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->i:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->i:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/upstream/cache/a;->b(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-boolean v1, v0, Lcom/google/android/exoplayer/upstream/cache/d;->d:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/exoplayer/upstream/cache/d;->e:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    iget-wide v4, v0, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    sub-long v7, v2, v4

    iget-wide v2, v0, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    sub-long/2addr v2, v7

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/cache/b;->k:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    new-instance v0, Lcom/google/android/exoplayer/upstream/j;

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    iget-object v6, p0, Lcom/google/android/exoplayer/upstream/cache/b;->i:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->b:Lcom/google/android/exoplayer/upstream/i;

    move-object v2, p0

    :goto_2
    iput-object v1, v2, Lcom/google/android/exoplayer/upstream/cache/b;->g:Lcom/google/android/exoplayer/upstream/i;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    :try_start_1
    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->l:Lcom/google/android/exoplayer/upstream/cache/d;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/cache/d;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/cache/b;->k:J

    :goto_3
    new-instance v0, Lcom/google/android/exoplayer/upstream/j;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->h:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    iget-object v6, p0, Lcom/google/android/exoplayer/upstream/cache/b;->i:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->c:Lcom/google/android/exoplayer/upstream/i;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->c:Lcom/google/android/exoplayer/upstream/i;

    move-object v2, p0

    goto :goto_2

    :cond_4
    iget-wide v0, v0, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->k:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->d:Lcom/google/android/exoplayer/upstream/i;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v2, p0

    goto :goto_2
.end method

.method private c()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->g:Lcom/google/android/exoplayer/upstream/i;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->g:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/i;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->g:Lcom/google/android/exoplayer/upstream/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->l:Lcom/google/android/exoplayer/upstream/cache/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->l:Lcom/google/android/exoplayer/upstream/cache/d;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V

    iput-object v3, p0, Lcom/google/android/exoplayer/upstream/cache/b;->l:Lcom/google/android/exoplayer/upstream/cache/d;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->l:Lcom/google/android/exoplayer/upstream/cache/d;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/cache/b;->l:Lcom/google/android/exoplayer/upstream/cache/d;

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V

    iput-object v3, p0, Lcom/google/android/exoplayer/upstream/cache/b;->l:Lcom/google/android/exoplayer/upstream/cache/d;

    :cond_2
    throw v0
.end method


# virtual methods
.method public final a([BII)I
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->g:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/i;->a([BII)I

    move-result v0

    if-ltz v0, :cond_1

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->k:J

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->k:J

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/b;->c()V

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/b;->k:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/b;->b()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/cache/b;->a([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/upstream/cache/b;->a(Ljava/io/IOException;)V

    throw v0
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/j;)J
    .locals 4

    iget-boolean v0, p1, Lcom/google/android/exoplayer/upstream/j;->b:Z

    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    :try_start_0
    iget-object v0, p1, Lcom/google/android/exoplayer/upstream/j;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->h:Landroid/net/Uri;

    iget-object v0, p1, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->i:Ljava/lang/String;

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->d:J

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->j:J

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/b;->k:J

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/b;->b()V

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/upstream/cache/b;->a(Ljava/io/IOException;)V

    throw v0
.end method

.method public final a()V
    .locals 1

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/b;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/upstream/cache/b;->a(Ljava/io/IOException;)V

    throw v0
.end method
