.class public final Lcom/google/android/exoplayer/upstream/DataSourceStream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/o;
.implements Lcom/google/android/exoplayer/upstream/p;


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/i;

.field private final b:Lcom/google/android/exoplayer/upstream/j;

.field private final c:Lcom/google/android/exoplayer/upstream/b;

.field private d:Lcom/google/android/exoplayer/upstream/a;

.field private e:J

.field private volatile f:Z

.field private volatile g:J

.field private volatile h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/upstream/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a:Lcom/google/android/exoplayer/upstream/i;

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->b:Lcom/google/android/exoplayer/upstream/j;

    iput-object p3, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->c:Lcom/google/android/exoplayer/upstream/b;

    return-void
.end method

.method private a(Ljava/nio/ByteBuffer;[BII)I
    .locals 10

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->e:J

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->b:Lcom/google/android/exoplayer/upstream/j;

    iget-wide v4, v0, Lcom/google/android/exoplayer/upstream/j;->e:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->e:J

    sub-long/2addr v2, v4

    int-to-long v4, p4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v3, v2

    if-nez v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->e:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    iput v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->i:I

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    iput v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->j:I

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/a;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->k:I

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/a;->a()[[B

    move-result-object v4

    move v2, v1

    move v0, p3

    :cond_3
    :goto_1
    if-ge v2, v3, :cond_6

    iget v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->k:I

    sub-int v6, v3, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-eqz p1, :cond_5

    iget v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->i:I

    aget-object v6, v4, v6

    iget v7, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->j:I

    invoke-virtual {p1, v6, v7, v5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    :cond_4
    :goto_2
    iget-wide v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->e:J

    int-to-long v8, v5

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->e:J

    add-int/2addr v2, v5

    iget v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->j:I

    add-int/2addr v6, v5

    iput v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->j:I

    iget v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->k:I

    sub-int v5, v6, v5

    iput v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->k:I

    iget v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->k:I

    if-nez v5, :cond_3

    iget-wide v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->e:J

    iget v7, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v7, v7

    cmp-long v5, v5, v7

    if-gez v5, :cond_3

    iget v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->i:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->i:I

    iget-object v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    iget v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->i:I

    iput v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->j:I

    iget-object v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    iget v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->i:I

    invoke-interface {v5}, Lcom/google/android/exoplayer/upstream/a;->b()I

    move-result v5

    iput v5, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->k:I

    goto :goto_1

    :cond_5
    if-eqz p2, :cond_4

    iget v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->i:I

    aget-object v6, v4, v6

    iget v7, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->j:I

    invoke-static {v6, v7, p2, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v0, v5

    goto :goto_2

    :cond_6
    move v0, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, v1, v1, v0, p1}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a(Ljava/nio/ByteBuffer;[BII)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)I
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a(Ljava/nio/ByteBuffer;[BII)I

    move-result v0

    return v0
.end method

.method public final a([BII)I
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a(Ljava/nio/ByteBuffer;[BII)I

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->e:J

    return-void
.end method

.method public final b()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    return-wide v0
.end method

.method public final c()Z
    .locals 4

    iget v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->e:J

    iget v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/a;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->f:Z

    return-void
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->f:Z

    return v0
.end method

.method public final i()V
    .locals 8

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->f:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    iget v3, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v3, v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->b:Lcom/google/android/exoplayer/upstream/j;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/upstream/i;->a(Lcom/google/android/exoplayer/upstream/j;)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->c:Lcom/google/android/exoplayer/upstream/b;

    iget v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/upstream/b;->a(I)Lcom/google/android/exoplayer/upstream/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    :cond_3
    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->l:I

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->m:I

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/a;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->n:I

    :cond_4
    const v0, 0x7fffffff

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    invoke-interface {v1}, Lcom/google/android/exoplayer/upstream/a;->a()[[B

    move-result-object v1

    :cond_5
    :goto_2
    iget-boolean v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->f:Z

    if-nez v2, :cond_9

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    iget v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_9

    if-lez v0, :cond_9

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-static {v1}, Lcom/google/android/exoplayer/e/k;->a(Lcom/google/android/exoplayer/upstream/i;)V

    throw v0

    :cond_6
    :try_start_1
    new-instance v0, Lcom/google/android/exoplayer/upstream/j;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->b:Lcom/google/android/exoplayer/upstream/j;

    iget-object v1, v1, Lcom/google/android/exoplayer/upstream/j;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->b:Lcom/google/android/exoplayer/upstream/j;

    iget-wide v2, v2, Lcom/google/android/exoplayer/upstream/j;->d:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    add-long/2addr v2, v4

    iget v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v4, v4

    iget-wide v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    sub-long/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->b:Lcom/google/android/exoplayer/upstream/j;

    iget-object v6, v6, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/upstream/i;->a(Lcom/google/android/exoplayer/upstream/j;)J

    goto :goto_1

    :cond_7
    iget v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->n:I

    int-to-long v2, v0

    iget v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v4, v0

    iget-wide v6, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v0, v2

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a:Lcom/google/android/exoplayer/upstream/i;

    iget v3, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->l:I

    aget-object v3, v1, v3

    iget v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->m:I

    invoke-interface {v2, v3, v4, v0}, Lcom/google/android/exoplayer/upstream/i;->a([BII)I

    move-result v0

    if-lez v0, :cond_8

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    iget v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->m:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->m:I

    iget v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->n:I

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->n:I

    iget v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->n:I

    if-nez v2, :cond_5

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    iget v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_5

    iget v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->l:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->l:I

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    iget v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->l:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->m:I

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->d:Lcom/google/android/exoplayer/upstream/a;

    iget v3, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->l:I

    invoke-interface {v2}, Lcom/google/android/exoplayer/upstream/a;->b()I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->n:I

    goto/16 :goto_2

    :cond_8
    iget v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    new-instance v0, Lcom/google/android/exoplayer/upstream/DataSourceStream$DataSourceStreamLoadException;

    new-instance v1, Lcom/google/android/exoplayer/upstream/UnexpectedLengthException;

    iget v2, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->h:I

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->g:J

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/exoplayer/upstream/UnexpectedLengthException;-><init>(JJ)V

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/upstream/DataSourceStream$DataSourceStreamLoadException;-><init>(Ljava/io/IOException;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/DataSourceStream;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-static {v0}, Lcom/google/android/exoplayer/e/k;->a(Lcom/google/android/exoplayer/upstream/i;)V

    goto/16 :goto_0
.end method
