.class public final Lcom/google/android/exoplayer/upstream/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Z

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;JJLjava/lang/String;)V
    .locals 10

    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    move-wide v7, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;JZ)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;JJLjava/lang/String;J)V
    .locals 10

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    move-wide/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;JZ)V

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;JJLjava/lang/String;JZ)V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    cmp-long v0, p2, v3

    if-ltz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->a(Z)V

    cmp-long v0, p7, v3

    if-ltz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->a(Z)V

    cmp-long v0, p4, v3

    if-gtz v0, :cond_0

    const-wide/16 v3, -0x1

    cmp-long v0, p4, v3

    if-nez v0, :cond_5

    :cond_0
    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->a(Z)V

    cmp-long v0, p2, p7

    if-eqz v0, :cond_1

    if-nez p9, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    invoke-static {v2}, Lcom/google/android/exoplayer/e/a;->a(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/j;->a:Landroid/net/Uri;

    iput-boolean p9, p0, Lcom/google/android/exoplayer/upstream/j;->b:Z

    iput-wide p2, p0, Lcom/google/android/exoplayer/upstream/j;->c:J

    iput-wide p7, p0, Lcom/google/android/exoplayer/upstream/j;->d:J

    iput-wide p4, p0, Lcom/google/android/exoplayer/upstream/j;->e:J

    iput-object p6, p0, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DataSpec["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/j;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/exoplayer/upstream/j;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/j;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/j;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/j;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
