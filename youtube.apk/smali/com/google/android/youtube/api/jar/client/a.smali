.class final Lcom/google/android/youtube/api/jar/client/a;
.super Lcom/google/android/apps/youtube/api/jar/a/cl;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/cl;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/api/jar/client/a;-><init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->p(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/j;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/j;-><init>(Lcom/google/android/youtube/api/jar/client/a;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/i;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/i;-><init>(Lcom/google/android/youtube/api/jar/client/a;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->n(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v1, v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNKNOWN:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNKNOWN:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IIZZ)V
    .locals 9

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->f(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v8

    new-instance v0, Lcom/google/android/youtube/api/jar/client/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p5

    move v5, p6

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/api/jar/client/g;-><init>(Lcom/google/android/youtube/api/jar/client/a;Ljava/lang/String;Ljava/lang/String;ZZII)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->x(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/c;-><init>(Lcom/google/android/youtube/api/jar/client/a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->f(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)V

    return-void
.end method

.method public final a(ZI)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->v(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/m;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/api/jar/client/m;-><init>(Lcom/google/android/youtube/api/jar/client/a;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->d(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/n;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/api/jar/client/n;-><init>(Lcom/google/android/youtube/api/jar/client/a;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)V

    return-void
.end method

.method public final b(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->u(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/l;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/l;-><init>(Lcom/google/android/youtube/api/jar/client/a;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->d(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/b;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/b;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->e(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->g(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/h;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/h;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/k;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/k;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->t(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->z(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/d;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/d;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/e;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/e;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/a;->a:Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;

    invoke-static {v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->D(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/api/jar/client/f;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/api/jar/client/f;-><init>(Lcom/google/android/youtube/api/jar/client/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
