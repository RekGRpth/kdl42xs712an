.class public final Lcom/google/android/youtube/api/StandalonePlayerActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/api/m;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/ArrayList;

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/google/android/apps/youtube/api/j;

.field private j:Lcom/google/android/apps/youtube/api/a/a;

.field private k:Lcom/google/android/apps/youtube/api/ac;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/api/j;)V
    .locals 13

    const/4 v12, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->i:Lcom/google/android/apps/youtube/api/j;

    new-instance v0, Lcom/google/android/apps/youtube/api/a/a;

    invoke-direct {v0, p0, p1, v12}, Lcom/google/android/apps/youtube/api/a/a;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/api/j;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/youtube/api/a/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/a/a;->j()Lcom/google/android/youtube/player/internal/dynamic/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/dynamic/d;->a(Lcom/google/android/youtube/player/internal/dynamic/a;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/youtube/api/ac;

    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/api/a/a;->a()Lcom/google/android/apps/youtube/api/ApiPlayer;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->c:Ljava/util/ArrayList;

    iget v7, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->d:I

    iget v8, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->e:I

    iget-boolean v9, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Z

    iget-boolean v10, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Z

    iget-boolean v11, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->h:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/api/ac;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/youtube/api/ApiPlayer;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;IIZZZ)V

    iput-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->k:Lcom/google/android/apps/youtube/api/ac;

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->k:Lcom/google/android/apps/youtube/api/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ac;->show()V

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/a/a;->b(I)V

    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->k:Lcom/google/android/apps/youtube/api/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ac;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/api/a/a;->d(Z)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "initialization_result"

    sget-object v2, Lcom/google/android/youtube/player/YouTubeInitializationResult;->SUCCESS:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-virtual {v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->setResult(ILandroid/content/Intent;)V

    return-void

    :cond_0
    move v0, v12

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "initialization_result"

    invoke-static {p1}, Lcom/google/android/apps/youtube/api/j;->a(Ljava/lang/Exception;)Lcom/google/android/youtube/player/YouTubeInitializationResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->finish()V

    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "developer_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "app_package"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "app_version"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v1, "client_library_version"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    const-string v6, "1.0.0"

    :cond_0
    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a:Ljava/lang/String;

    const-string v1, "playlist_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->b:Ljava/lang/String;

    const-string v1, "video_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->c:Ljava/util/ArrayList;

    const-string v1, "current_index"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->d:I

    const-string v1, "start_time_millis"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->e:I

    const-string v1, "autoplay"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->f:Z

    const-string v1, "lightbox_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Z

    const-string v1, "window_has_status_bar"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->h:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Z

    iget-boolean v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->h:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/api/ac;->a(ZZ)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->setTheme(I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->setRequestedOrientation(I)V

    :cond_1
    const-string v0, "^(\\d+\\.){2}(\\d+)(\\w?)$"

    invoke-virtual {v6, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid client version"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->a(Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_2
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object v0, p0

    move-object v2, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/youtube/api/j;->a(Lcom/google/android/apps/youtube/api/m;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->isFinishing()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/a/a;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->i:Lcom/google/android/apps/youtube/api/j;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->i:Lcom/google/android/apps/youtube/api/j;

    invoke-virtual {p0}, Lcom/google/android/youtube/api/StandalonePlayerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/api/j;->a(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->k:Lcom/google/android/apps/youtube/api/ac;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->k:Lcom/google/android/apps/youtube/api/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ac;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->k:Lcom/google/android/apps/youtube/api/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ac;->dismiss()V

    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/api/a/a;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/api/a/a;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/a/a;->g()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->k:Lcom/google/android/apps/youtube/api/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->k:Lcom/google/android/apps/youtube/api/ac;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ac;->a()V

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/a/a;->f()V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/a/a;->e()V

    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/StandalonePlayerActivity;->j:Lcom/google/android/apps/youtube/api/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/a/a;->h()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
