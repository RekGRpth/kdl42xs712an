.class public final Lcom/google/zxing/aztec/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/zxing/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .locals 7

    const/4 v3, 0x0

    const/4 v2, 0x0

    new-instance v5, Lcom/google/zxing/aztec/a/a;

    invoke-virtual {p1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/zxing/aztec/a/a;-><init>(Lcom/google/zxing/common/b;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v5, v0}, Lcom/google/zxing/aztec/a/a;->a(Z)Lcom/google/zxing/aztec/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/aztec/a;->e()[Lcom/google/zxing/g;
    :try_end_0
    .catch Lcom/google/zxing/NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/zxing/FormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    :try_start_1
    new-instance v4, Lcom/google/zxing/aztec/decoder/Decoder;

    invoke-direct {v4}, Lcom/google/zxing/aztec/decoder/Decoder;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/zxing/aztec/decoder/Decoder;->a(Lcom/google/zxing/aztec/a;)Lcom/google/zxing/common/d;
    :try_end_1
    .catch Lcom/google/zxing/NotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/google/zxing/FormatException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v0

    move-object v4, v2

    :goto_0
    if-nez v0, :cond_7

    const/4 v0, 0x1

    :try_start_2
    invoke-virtual {v5, v0}, Lcom/google/zxing/aztec/a/a;->a(Z)Lcom/google/zxing/aztec/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/aztec/a;->e()[Lcom/google/zxing/g;

    move-result-object v1

    new-instance v5, Lcom/google/zxing/aztec/decoder/Decoder;

    invoke-direct {v5}, Lcom/google/zxing/aztec/decoder/Decoder;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/zxing/aztec/decoder/Decoder;->a(Lcom/google/zxing/aztec/a;)Lcom/google/zxing/common/d;
    :try_end_2
    .catch Lcom/google/zxing/NotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/zxing/FormatException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    move-object v2, v0

    move-object v4, v1

    :goto_1
    if-eqz p2, :cond_4

    sget-object v0, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/h;

    if-eqz v0, :cond_4

    array-length v5, v4

    move v1, v3

    :goto_2
    if-ge v1, v5, :cond_4

    aget-object v3, v4, v1

    invoke-interface {v0, v3}, Lcom/google/zxing/h;->a(Lcom/google/zxing/g;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_3
    move-object v4, v0

    move-object v0, v2

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_4
    move-object v4, v2

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    goto :goto_0

    :catch_2
    move-exception v0

    if-eqz v4, :cond_0

    throw v4

    :cond_0
    if-eqz v2, :cond_1

    throw v2

    :cond_1
    throw v0

    :catch_3
    move-exception v0

    if-eqz v4, :cond_2

    throw v4

    :cond_2
    if-eqz v2, :cond_3

    throw v2

    :cond_3
    throw v0

    :cond_4
    new-instance v0, Lcom/google/zxing/f;

    invoke-virtual {v2}, Lcom/google/zxing/common/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/zxing/common/d;->a()[B

    move-result-object v3

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->AZTEC:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    invoke-virtual {v2}, Lcom/google/zxing/common/d;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    sget-object v3, Lcom/google/zxing/ResultMetadataType;->BYTE_SEGMENTS:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v0, v3, v1}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    :cond_5
    invoke-virtual {v2}, Lcom/google/zxing/common/d;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    sget-object v2, Lcom/google/zxing/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v0, v2, v1}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    :cond_6
    return-object v0

    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    goto :goto_3

    :cond_7
    move-object v2, v0

    move-object v4, v1

    goto :goto_1
.end method

.method public final a()V
    .locals 0

    return-void
.end method
