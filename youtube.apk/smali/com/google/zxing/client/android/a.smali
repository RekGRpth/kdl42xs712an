.class final Lcom/google/zxing/client/android/a;
.super Landroid/os/Handler;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/zxing/client/android/CaptureActivity;

.field private final c:Lcom/google/zxing/d;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/zxing/client/android/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/a;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/zxing/client/android/CaptureActivity;Ljava/util/Hashtable;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/zxing/client/android/a;->d:Z

    new-instance v0, Lcom/google/zxing/d;

    invoke-direct {v0}, Lcom/google/zxing/d;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/client/android/a;->c:Lcom/google/zxing/d;

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->c:Lcom/google/zxing/d;

    invoke-virtual {v0, p2}, Lcom/google/zxing/d;->a(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/google/zxing/client/android/a;->b:Lcom/google/zxing/client/android/CaptureActivity;

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 10

    iget-boolean v0, p0, Lcom/google/zxing/client/android/a;->d:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/google/zxing/client/android/j;->b:I

    if-ne v0, v1, :cond_3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/zxing/client/android/a;->b:Lcom/google/zxing/client/android/CaptureActivity;

    invoke-virtual {v6}, Lcom/google/zxing/client/android/CaptureActivity;->c()Lcom/google/zxing/client/android/a/c;

    move-result-object v6

    invoke-virtual {v6, v0, v2, v3}, Lcom/google/zxing/client/android/a/c;->a([BII)Lcom/google/zxing/client/android/g;

    move-result-object v2

    if-eqz v2, :cond_4

    new-instance v0, Lcom/google/zxing/b;

    new-instance v3, Lcom/google/zxing/common/i;

    invoke-direct {v3, v2}, Lcom/google/zxing/common/i;-><init>(Lcom/google/zxing/c;)V

    invoke-direct {v0, v3}, Lcom/google/zxing/b;-><init>(Lcom/google/zxing/a;)V

    :try_start_0
    iget-object v3, p0, Lcom/google/zxing/client/android/a;->c:Lcom/google/zxing/d;

    invoke-virtual {v3, v0}, Lcom/google/zxing/d;->a(Lcom/google/zxing/b;)Lcom/google/zxing/f;
    :try_end_0
    .catch Lcom/google/zxing/ReaderException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/google/zxing/client/android/a;->c:Lcom/google/zxing/d;

    invoke-virtual {v1}, Lcom/google/zxing/d;->a()V

    :goto_1
    iget-object v1, p0, Lcom/google/zxing/client/android/a;->b:Lcom/google/zxing/client/android/CaptureActivity;

    invoke-virtual {v1}, Lcom/google/zxing/client/android/CaptureActivity;->b()Landroid/os/Handler;

    move-result-object v1

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sget-object v3, Lcom/google/zxing/client/android/a;->a:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Found barcode in "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v4, v6, v4

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    sget v3, Lcom/google/zxing/client/android/j;->d:I

    invoke-static {v1, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "barcode_bitmap"

    invoke-virtual {v2}, Lcom/google/zxing/client/android/g;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/zxing/client/android/a;->c:Lcom/google/zxing/d;

    invoke-virtual {v0}, Lcom/google/zxing/d;->a()V

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/zxing/client/android/a;->c:Lcom/google/zxing/d;

    invoke-virtual {v1}, Lcom/google/zxing/d;->a()V

    throw v0

    :cond_2
    if-eqz v1, :cond_0

    sget v0, Lcom/google/zxing/client/android/j;->c:I

    invoke-static {v1, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/google/zxing/client/android/j;->g:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/client/android/a;->d:Z

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
