.class final Lcom/google/zxing/oned/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Lcom/google/zxing/oned/m;

.field private final c:Lcom/google/zxing/oned/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/zxing/oned/o;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/zxing/oned/m;

    invoke-direct {v0}, Lcom/google/zxing/oned/m;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/o;->b:Lcom/google/zxing/oned/m;

    new-instance v0, Lcom/google/zxing/oned/n;

    invoke-direct {v0}, Lcom/google/zxing/oned/n;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/o;->c:Lcom/google/zxing/oned/n;

    return-void
.end method


# virtual methods
.method final a(ILcom/google/zxing/common/a;I)Lcom/google/zxing/f;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/google/zxing/oned/o;->a:[I

    invoke-static {p2, p3, v0, v1}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;IZ[I)[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/zxing/oned/o;->c:Lcom/google/zxing/oned/n;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/zxing/oned/n;->a(ILcom/google/zxing/common/a;[I)Lcom/google/zxing/f;
    :try_end_0
    .catch Lcom/google/zxing/ReaderException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/zxing/oned/o;->b:Lcom/google/zxing/oned/m;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/zxing/oned/m;->a(ILcom/google/zxing/common/a;[I)Lcom/google/zxing/f;

    move-result-object v0

    goto :goto_0
.end method
