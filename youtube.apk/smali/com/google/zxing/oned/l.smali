.class public final Lcom/google/zxing/oned/l;
.super Lcom/google/zxing/oned/p;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/oned/p;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/zxing/oned/p;-><init>()V

    new-instance v0, Lcom/google/zxing/oned/e;

    invoke-direct {v0}, Lcom/google/zxing/oned/e;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/l;->a:Lcom/google/zxing/oned/p;

    return-void
.end method

.method private static a(Lcom/google/zxing/f;)Lcom/google/zxing/f;
    .locals 5

    invoke-virtual {p0}, Lcom/google/zxing/f;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_0

    new-instance v1, Lcom/google/zxing/f;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/zxing/f;->c()[Lcom/google/zxing/g;

    move-result-object v3

    sget-object v4, Lcom/google/zxing/BarcodeFormat;->UPC_A:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method protected final a(Lcom/google/zxing/common/a;[ILjava/lang/StringBuilder;)I
    .locals 1

    iget-object v0, p0, Lcom/google/zxing/oned/l;->a:Lcom/google/zxing/oned/p;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;[ILjava/lang/StringBuilder;)I

    move-result v0

    return v0
.end method

.method public final a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
    .locals 1

    iget-object v0, p0, Lcom/google/zxing/oned/l;->a:Lcom/google/zxing/oned/p;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/zxing/oned/p;->a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/zxing/oned/l;->a(Lcom/google/zxing/f;)Lcom/google/zxing/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILcom/google/zxing/common/a;[ILjava/util/Map;)Lcom/google/zxing/f;
    .locals 1

    iget-object v0, p0, Lcom/google/zxing/oned/l;->a:Lcom/google/zxing/oned/p;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/zxing/oned/p;->a(ILcom/google/zxing/common/a;[ILjava/util/Map;)Lcom/google/zxing/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/zxing/oned/l;->a(Lcom/google/zxing/f;)Lcom/google/zxing/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .locals 1

    iget-object v0, p0, Lcom/google/zxing/oned/l;->a:Lcom/google/zxing/oned/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/zxing/oned/l;->a(Lcom/google/zxing/f;)Lcom/google/zxing/f;

    move-result-object v0

    return-object v0
.end method

.method final b()Lcom/google/zxing/BarcodeFormat;
    .locals 1

    sget-object v0, Lcom/google/zxing/BarcodeFormat;->UPC_A:Lcom/google/zxing/BarcodeFormat;

    return-object v0
.end method
