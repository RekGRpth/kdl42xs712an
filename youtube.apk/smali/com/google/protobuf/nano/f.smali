.class public final Lcom/google/protobuf/nano/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:I

.field static final b:I

.field static final c:I

.field static final d:I

.field public static final e:[I

.field public static final f:[J

.field public static final g:[F

.field public static final h:[D

.field public static final i:[Z

.field public static final j:[Ljava/lang/String;

.field public static final k:[[B

.field public static final l:[B

.field public static final m:[Ljava/lang/Integer;

.field public static final n:[Ljava/lang/Long;

.field public static final o:[Ljava/lang/Float;

.field public static final p:[Ljava/lang/Double;

.field public static final q:[Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xb

    sput v0, Lcom/google/protobuf/nano/f;->a:I

    const/16 v0, 0xc

    sput v0, Lcom/google/protobuf/nano/f;->b:I

    const/16 v0, 0x10

    sput v0, Lcom/google/protobuf/nano/f;->c:I

    const/16 v0, 0x1a

    sput v0, Lcom/google/protobuf/nano/f;->d:I

    new-array v0, v1, [I

    sput-object v0, Lcom/google/protobuf/nano/f;->e:[I

    new-array v0, v1, [J

    sput-object v0, Lcom/google/protobuf/nano/f;->f:[J

    new-array v0, v1, [F

    sput-object v0, Lcom/google/protobuf/nano/f;->g:[F

    new-array v0, v1, [D

    sput-object v0, Lcom/google/protobuf/nano/f;->h:[D

    new-array v0, v1, [Z

    sput-object v0, Lcom/google/protobuf/nano/f;->i:[Z

    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/google/protobuf/nano/f;->j:[Ljava/lang/String;

    new-array v0, v1, [[B

    sput-object v0, Lcom/google/protobuf/nano/f;->k:[[B

    new-array v0, v1, [B

    sput-object v0, Lcom/google/protobuf/nano/f;->l:[B

    new-array v0, v1, [Ljava/lang/Integer;

    sput-object v0, Lcom/google/protobuf/nano/f;->m:[Ljava/lang/Integer;

    new-array v0, v1, [Ljava/lang/Long;

    sput-object v0, Lcom/google/protobuf/nano/f;->n:[Ljava/lang/Long;

    new-array v0, v1, [Ljava/lang/Float;

    sput-object v0, Lcom/google/protobuf/nano/f;->o:[Ljava/lang/Float;

    new-array v0, v1, [Ljava/lang/Double;

    sput-object v0, Lcom/google/protobuf/nano/f;->p:[Ljava/lang/Double;

    new-array v0, v1, [Ljava/lang/Boolean;

    sput-object v0, Lcom/google/protobuf/nano/f;->q:[Ljava/lang/Boolean;

    return-void
.end method

.method static a(I)I
    .locals 1

    and-int/lit8 v0, p0, 0x7

    return v0
.end method

.method static a(II)I
    .locals 1

    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method

.method public static final a(Lcom/google/protobuf/nano/a;I)I
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/protobuf/nano/a;->l()I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/google/protobuf/nano/a;->b(I)Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/protobuf/nano/a;->k()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/nano/a;->a()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/protobuf/nano/a;->b(I)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/protobuf/nano/a;->c(I)V

    return v0
.end method

.method public static a(Ljava/util/List;)I
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/e;

    iget v3, v0, Lcom/google/protobuf/nano/e;->a:I

    invoke-static {v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(I)I

    move-result v3

    add-int/2addr v1, v3

    iget-object v0, v0, Lcom/google/protobuf/nano/e;->b:[B

    array-length v0, v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    if-nez p0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/e;

    iget v2, v0, Lcom/google/protobuf/nano/e;->a:I

    ushr-int/lit8 v2, v2, 0x3

    iget v3, v0, Lcom/google/protobuf/nano/e;->a:I

    and-int/lit8 v3, v3, 0x7

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->g(II)V

    iget-object v0, v0, Lcom/google/protobuf/nano/e;->b:[B

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b([B)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z
    .locals 3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()I

    move-result v0

    invoke-virtual {p1, p2}, Lcom/google/protobuf/nano/a;->b(I)Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/a;->a(II)[B

    move-result-object v0

    new-instance v2, Lcom/google/protobuf/nano/e;

    invoke-direct {v2, p2, v0}, Lcom/google/protobuf/nano/e;-><init>(I[B)V

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return v1
.end method

.method public static b(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x3

    return v0
.end method
