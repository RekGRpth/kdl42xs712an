.class public Lcom/google/wireless/gdata2/client/ResourceUnavailableException;
.super Lcom/google/wireless/gdata2/GDataException;
.source "SourceFile"


# instance fields
.field private retryAfter:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Lcom/google/wireless/gdata2/GDataException;-><init>()V

    iput-wide p1, p0, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->retryAfter:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/GDataException;-><init>(Ljava/lang/String;)V

    iput-wide p2, p0, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->retryAfter:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/gdata2/GDataException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-wide p3, p0, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->retryAfter:J

    return-void
.end method


# virtual methods
.method public getRetryAfter()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/gdata2/client/ResourceUnavailableException;->retryAfter:J

    return-wide v0
.end method
