.class public final Lcom/google/a/a/a/a/fa;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/fa;


# instance fields
.field public b:[Lcom/google/a/a/a/a/fb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/fa;

    sput-object v0, Lcom/google/a/a/a/a/fa;->a:[Lcom/google/a/a/a/a/fa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    sget-object v0, Lcom/google/a/a/a/a/fb;->a:[Lcom/google/a/a/a/a/fb;

    iput-object v0, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v2, v3, v1

    const/4 v5, 0x1

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/fa;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/fa;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/fa;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/fa;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/fa;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/fb;

    iget-object v3, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    new-instance v3, Lcom/google/a/a/a/a/fb;

    invoke-direct {v3}, Lcom/google/a/a/a/a/fb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    new-instance v3, Lcom/google/a/a/a/a/fb;

    invoke-direct {v3}, Lcom/google/a/a/a/a/fb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget-object v0, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/a/a/a/a/fa;->b:[Lcom/google/a/a/a/a/fb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/fa;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
