.class public final Lcom/google/a/a/a/a/wo;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/wo;


# instance fields
.field public b:I

.field public c:[Lcom/google/a/a/a/a/wn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/wo;

    sput-object v0, Lcom/google/a/a/a/a/wo;->a:[Lcom/google/a/a/a/a/wo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/a/a/a/a/wo;->b:I

    sget-object v0, Lcom/google/a/a/a/a/wn;->a:[Lcom/google/a/a/a/a/wn;

    iput-object v0, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/a/a/a/a/wo;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/a/a/a/a/wo;->b:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    const/4 v5, 0x2

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/wo;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/wo;->dm:I

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/wo;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    :cond_2
    iput v0, p0, Lcom/google/a/a/a/a/wo;->b:I

    goto :goto_0

    :cond_3
    iput v1, p0, Lcom/google/a/a/a/a/wo;->b:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/wn;

    iget-object v3, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    new-instance v3, Lcom/google/a/a/a/a/wn;

    invoke-direct {v3}, Lcom/google/a/a/a/a/wn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    new-instance v3, Lcom/google/a/a/a/a/wn;

    invoke-direct {v3}, Lcom/google/a/a/a/a/wn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget v0, p0, Lcom/google/a/a/a/a/wo;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/a/a/a/a/wo;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/a/a/a/a/wo;->c:[Lcom/google/a/a/a/a/wn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/wo;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
