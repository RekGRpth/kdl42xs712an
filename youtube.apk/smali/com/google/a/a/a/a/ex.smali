.class public final Lcom/google/a/a/a/a/ex;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/ex;


# instance fields
.field public b:[Lcom/google/a/a/a/a/ac;

.field public c:Ljava/lang/String;

.field public d:[Lcom/google/a/a/a/a/ll;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/ex;

    sput-object v0, Lcom/google/a/a/a/a/ex;->a:[Lcom/google/a/a/a/a/ex;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    sget-object v0, Lcom/google/a/a/a/a/ac;->a:[Lcom/google/a/a/a/a/ac;

    iput-object v0, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/ex;->c:Ljava/lang/String;

    sget-object v0, Lcom/google/a/a/a/a/ll;->a:[Lcom/google/a/a/a/a/ll;

    iput-object v0, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    const/4 v6, 0x1

    invoke-static {v6, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/ex;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    const/4 v5, 0x3

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/ex;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/ex;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/ex;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/ac;

    iget-object v3, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    new-instance v3, Lcom/google/a/a/a/a/ac;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ac;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    new-instance v3, Lcom/google/a/a/a/a/ac;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ac;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/ex;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/ll;

    iget-object v3, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    :goto_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    new-instance v3, Lcom/google/a/a/a/a/ll;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ll;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    new-instance v3, Lcom/google/a/a/a/a/ll;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ll;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/ex;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/ex;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/ex;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
