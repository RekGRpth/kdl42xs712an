.class public final Lcom/google/a/a/a/a/wi;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/wi;


# instance fields
.field public b:Lcom/google/a/a/a/a/jw;

.field public c:Lcom/google/a/a/a/a/jn;

.field public d:Lcom/google/a/a/a/a/js;

.field public e:Lcom/google/a/a/a/a/jr;

.field public f:Lcom/google/a/a/a/a/ju;

.field public g:Lcom/google/a/a/a/a/jp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/wi;

    sput-object v0, Lcom/google/a/a/a/a/wi;->a:[Lcom/google/a/a/a/a/wi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->b:Lcom/google/a/a/a/a/jw;

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->c:Lcom/google/a/a/a/a/jn;

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->d:Lcom/google/a/a/a/a/js;

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->e:Lcom/google/a/a/a/a/jr;

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->f:Lcom/google/a/a/a/a/ju;

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->g:Lcom/google/a/a/a/a/jp;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->b:Lcom/google/a/a/a/a/jw;

    if-eqz v1, :cond_0

    const v0, 0x30905bb

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->b:Lcom/google/a/a/a/a/jw;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->c:Lcom/google/a/a/a/a/jn;

    if-eqz v1, :cond_1

    const v1, 0x309bf19

    iget-object v2, p0, Lcom/google/a/a/a/a/wi;->c:Lcom/google/a/a/a/a/jn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->d:Lcom/google/a/a/a/a/js;

    if-eqz v1, :cond_2

    const v1, 0x309c027

    iget-object v2, p0, Lcom/google/a/a/a/a/wi;->d:Lcom/google/a/a/a/a/js;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->e:Lcom/google/a/a/a/a/jr;

    if-eqz v1, :cond_3

    const v1, 0x342dd8c

    iget-object v2, p0, Lcom/google/a/a/a/a/wi;->e:Lcom/google/a/a/a/a/jr;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->f:Lcom/google/a/a/a/a/ju;

    if-eqz v1, :cond_4

    const v1, 0x36a6bde

    iget-object v2, p0, Lcom/google/a/a/a/a/wi;->f:Lcom/google/a/a/a/a/ju;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->g:Lcom/google/a/a/a/a/jp;

    if-eqz v1, :cond_5

    const v1, 0x38e6c6c

    iget-object v2, p0, Lcom/google/a/a/a/a/wi;->g:Lcom/google/a/a/a/a/jp;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/wi;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/wi;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/jw;

    invoke-direct {v0}, Lcom/google/a/a/a/a/jw;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->b:Lcom/google/a/a/a/a/jw;

    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->b:Lcom/google/a/a/a/a/jw;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/jn;

    invoke-direct {v0}, Lcom/google/a/a/a/a/jn;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->c:Lcom/google/a/a/a/a/jn;

    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->c:Lcom/google/a/a/a/a/jn;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/js;

    invoke-direct {v0}, Lcom/google/a/a/a/a/js;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->d:Lcom/google/a/a/a/a/js;

    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->d:Lcom/google/a/a/a/a/js;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/jr;

    invoke-direct {v0}, Lcom/google/a/a/a/a/jr;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->e:Lcom/google/a/a/a/a/jr;

    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->e:Lcom/google/a/a/a/a/jr;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/ju;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ju;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->f:Lcom/google/a/a/a/a/ju;

    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->f:Lcom/google/a/a/a/a/ju;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/jp;

    invoke-direct {v0}, Lcom/google/a/a/a/a/jp;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wi;->g:Lcom/google/a/a/a/a/jp;

    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->g:Lcom/google/a/a/a/a/jp;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18482dda -> :sswitch_1
        0x184df8ca -> :sswitch_2
        0x184e013a -> :sswitch_3
        0x1a16ec62 -> :sswitch_4
        0x1b535ef2 -> :sswitch_5
        0x1c736362 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->b:Lcom/google/a/a/a/a/jw;

    if-eqz v0, :cond_0

    const v0, 0x30905bb

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->b:Lcom/google/a/a/a/a/jw;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->c:Lcom/google/a/a/a/a/jn;

    if-eqz v0, :cond_1

    const v0, 0x309bf19

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->c:Lcom/google/a/a/a/a/jn;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->d:Lcom/google/a/a/a/a/js;

    if-eqz v0, :cond_2

    const v0, 0x309c027

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->d:Lcom/google/a/a/a/a/js;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->e:Lcom/google/a/a/a/a/jr;

    if-eqz v0, :cond_3

    const v0, 0x342dd8c

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->e:Lcom/google/a/a/a/a/jr;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->f:Lcom/google/a/a/a/a/ju;

    if-eqz v0, :cond_4

    const v0, 0x36a6bde

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->f:Lcom/google/a/a/a/a/ju;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->g:Lcom/google/a/a/a/a/jp;

    if-eqz v0, :cond_5

    const v0, 0x38e6c6c

    iget-object v1, p0, Lcom/google/a/a/a/a/wi;->g:Lcom/google/a/a/a/a/jp;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/wi;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
