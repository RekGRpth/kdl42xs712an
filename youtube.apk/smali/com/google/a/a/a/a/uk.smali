.class public final Lcom/google/a/a/a/a/uk;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/uk;


# instance fields
.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Lcom/google/a/a/a/a/un;

.field public g:Lcom/google/a/a/a/a/uj;

.field public h:Lcom/google/a/a/a/a/fk;

.field public i:Lcom/google/a/a/a/a/up;

.field public j:Lcom/google/a/a/a/a/uo;

.field public k:Lcom/google/a/a/a/a/ui;

.field public l:Lcom/google/a/a/a/a/ur;

.field public m:Lcom/google/a/a/a/a/um;

.field public n:Lcom/google/a/a/a/a/uq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/uk;

    sput-object v0, Lcom/google/a/a/a/a/uk;->a:[Lcom/google/a/a/a/a/uk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-boolean v1, p0, Lcom/google/a/a/a/a/uk;->b:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/uk;->c:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/uk;->d:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/uk;->e:Z

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->f:Lcom/google/a/a/a/a/un;

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->g:Lcom/google/a/a/a/a/uj;

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->h:Lcom/google/a/a/a/a/fk;

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->i:Lcom/google/a/a/a/a/up;

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->j:Lcom/google/a/a/a/a/uo;

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->k:Lcom/google/a/a/a/a/ui;

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->l:Lcom/google/a/a/a/a/ur;

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->m:Lcom/google/a/a/a/a/um;

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->n:Lcom/google/a/a/a/a/uq;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->b:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->b:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/a/a/a/a/uk;->c:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->d:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/a/a/a/a/uk;->d:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->e:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/a/a/a/a/uk;->e:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->f:Lcom/google/a/a/a/a/un;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->f:Lcom/google/a/a/a/a/un;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->g:Lcom/google/a/a/a/a/uj;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->g:Lcom/google/a/a/a/a/uj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->h:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->i:Lcom/google/a/a/a/a/up;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->i:Lcom/google/a/a/a/a/up;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->j:Lcom/google/a/a/a/a/uo;

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->j:Lcom/google/a/a/a/a/uo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->k:Lcom/google/a/a/a/a/ui;

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->k:Lcom/google/a/a/a/a/ui;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->l:Lcom/google/a/a/a/a/ur;

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->l:Lcom/google/a/a/a/a/ur;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->m:Lcom/google/a/a/a/a/um;

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->m:Lcom/google/a/a/a/a/um;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->n:Lcom/google/a/a/a/a/uq;

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/uk;->n:Lcom/google/a/a/a/a/uq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/uk;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/uk;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/uk;->b:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/uk;->c:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/uk;->d:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/uk;->e:Z

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/un;

    invoke-direct {v0}, Lcom/google/a/a/a/a/un;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->f:Lcom/google/a/a/a/a/un;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->f:Lcom/google/a/a/a/a/un;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/uj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/uj;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->g:Lcom/google/a/a/a/a/uj;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->g:Lcom/google/a/a/a/a/uj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->h:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->h:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/up;

    invoke-direct {v0}, Lcom/google/a/a/a/a/up;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->i:Lcom/google/a/a/a/a/up;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->i:Lcom/google/a/a/a/a/up;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/uo;

    invoke-direct {v0}, Lcom/google/a/a/a/a/uo;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->j:Lcom/google/a/a/a/a/uo;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->j:Lcom/google/a/a/a/a/uo;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/ui;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ui;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->k:Lcom/google/a/a/a/a/ui;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->k:Lcom/google/a/a/a/a/ui;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/a/a/a/a/ur;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ur;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->l:Lcom/google/a/a/a/a/ur;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->l:Lcom/google/a/a/a/a/ur;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lcom/google/a/a/a/a/um;

    invoke-direct {v0}, Lcom/google/a/a/a/a/um;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->m:Lcom/google/a/a/a/a/um;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->m:Lcom/google/a/a/a/a/um;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_d
    new-instance v0, Lcom/google/a/a/a/a/uq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/uq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uk;->n:Lcom/google/a/a/a/a/uq;

    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->n:Lcom/google/a/a/a/a/uq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/a/a/a/a/uk;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/a/a/a/a/uk;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/a/a/a/a/uk;->d:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/a/a/a/a/uk;->e:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/a/a/a/a/uk;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->f:Lcom/google/a/a/a/a/un;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->f:Lcom/google/a/a/a/a/un;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->g:Lcom/google/a/a/a/a/uj;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->g:Lcom/google/a/a/a/a/uj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->h:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->h:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->i:Lcom/google/a/a/a/a/up;

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->i:Lcom/google/a/a/a/a/up;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->j:Lcom/google/a/a/a/a/uo;

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->j:Lcom/google/a/a/a/a/uo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->k:Lcom/google/a/a/a/a/ui;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->k:Lcom/google/a/a/a/a/ui;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->l:Lcom/google/a/a/a/a/ur;

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->l:Lcom/google/a/a/a/a/ur;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->m:Lcom/google/a/a/a/a/um;

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->m:Lcom/google/a/a/a/a/um;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->n:Lcom/google/a/a/a/a/uq;

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/uk;->n:Lcom/google/a/a/a/a/uq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_c
    iget-object v0, p0, Lcom/google/a/a/a/a/uk;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
