.class public final Lcom/google/a/a/a/a/th;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/th;


# instance fields
.field public b:Lcom/google/a/a/a/a/tl;

.field public c:Lcom/google/a/a/a/a/tm;

.field public d:Lcom/google/a/a/a/a/tk;

.field public e:Lcom/google/a/a/a/a/ti;

.field public f:Lcom/google/a/a/a/a/tj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/th;

    sput-object v0, Lcom/google/a/a/a/a/th;->a:[Lcom/google/a/a/a/a/th;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->b:Lcom/google/a/a/a/a/tl;

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->c:Lcom/google/a/a/a/a/tm;

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->d:Lcom/google/a/a/a/a/tk;

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->e:Lcom/google/a/a/a/a/ti;

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->f:Lcom/google/a/a/a/a/tj;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/th;->b:Lcom/google/a/a/a/a/tl;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/th;->b:Lcom/google/a/a/a/a/tl;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/th;->c:Lcom/google/a/a/a/a/tm;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/th;->c:Lcom/google/a/a/a/a/tm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/th;->d:Lcom/google/a/a/a/a/tk;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/th;->d:Lcom/google/a/a/a/a/tk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/th;->e:Lcom/google/a/a/a/a/ti;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/a/a/a/th;->e:Lcom/google/a/a/a/a/ti;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/th;->f:Lcom/google/a/a/a/a/tj;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/th;->f:Lcom/google/a/a/a/a/tj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/th;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/th;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/th;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/th;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/th;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/tl;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tl;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->b:Lcom/google/a/a/a/a/tl;

    iget-object v0, p0, Lcom/google/a/a/a/a/th;->b:Lcom/google/a/a/a/a/tl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/tm;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tm;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->c:Lcom/google/a/a/a/a/tm;

    iget-object v0, p0, Lcom/google/a/a/a/a/th;->c:Lcom/google/a/a/a/a/tm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/tk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->d:Lcom/google/a/a/a/a/tk;

    iget-object v0, p0, Lcom/google/a/a/a/a/th;->d:Lcom/google/a/a/a/a/tk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/ti;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ti;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->e:Lcom/google/a/a/a/a/ti;

    iget-object v0, p0, Lcom/google/a/a/a/a/th;->e:Lcom/google/a/a/a/a/ti;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/tj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tj;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/th;->f:Lcom/google/a/a/a/a/tj;

    iget-object v0, p0, Lcom/google/a/a/a/a/th;->f:Lcom/google/a/a/a/a/tj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/th;->b:Lcom/google/a/a/a/a/tl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/th;->b:Lcom/google/a/a/a/a/tl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/th;->c:Lcom/google/a/a/a/a/tm;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/th;->c:Lcom/google/a/a/a/a/tm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/th;->d:Lcom/google/a/a/a/a/tk;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/th;->d:Lcom/google/a/a/a/a/tk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/th;->e:Lcom/google/a/a/a/a/ti;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/th;->e:Lcom/google/a/a/a/a/ti;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/th;->f:Lcom/google/a/a/a/a/tj;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/th;->f:Lcom/google/a/a/a/a/tj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/th;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
