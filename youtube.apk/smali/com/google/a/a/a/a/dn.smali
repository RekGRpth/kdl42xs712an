.class public final Lcom/google/a/a/a/a/dn;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/dn;


# instance fields
.field public b:Lcom/google/a/a/a/a/gu;

.field public c:Lcom/google/a/a/a/a/pn;

.field public d:Lcom/google/a/a/a/a/pq;

.field public e:Lcom/google/a/a/a/a/my;

.field public f:Lcom/google/a/a/a/a/ad;

.field public g:Lcom/google/a/a/a/a/ep;

.field public h:Lcom/google/a/a/a/a/qx;

.field public i:Lcom/google/a/a/a/a/na;

.field public j:Lcom/google/a/a/a/a/ae;

.field public k:Lcom/google/a/a/a/a/mz;

.field public l:Lcom/google/a/a/a/a/aj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/dn;

    sput-object v0, Lcom/google/a/a/a/a/dn;->a:[Lcom/google/a/a/a/a/dn;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->b:Lcom/google/a/a/a/a/gu;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->c:Lcom/google/a/a/a/a/pn;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->d:Lcom/google/a/a/a/a/pq;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->e:Lcom/google/a/a/a/a/my;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->f:Lcom/google/a/a/a/a/ad;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->g:Lcom/google/a/a/a/a/ep;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->h:Lcom/google/a/a/a/a/qx;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->i:Lcom/google/a/a/a/a/na;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->j:Lcom/google/a/a/a/a/ae;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->k:Lcom/google/a/a/a/a/mz;

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->l:Lcom/google/a/a/a/a/aj;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->b:Lcom/google/a/a/a/a/gu;

    if-eqz v1, :cond_0

    const v0, 0x329aa9d

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->b:Lcom/google/a/a/a/a/gu;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->c:Lcom/google/a/a/a/a/pn;

    if-eqz v1, :cond_1

    const v1, 0x329ef44

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->c:Lcom/google/a/a/a/a/pn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->d:Lcom/google/a/a/a/a/pq;

    if-eqz v1, :cond_2

    const v1, 0x329fb79

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->d:Lcom/google/a/a/a/a/pq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->e:Lcom/google/a/a/a/a/my;

    if-eqz v1, :cond_3

    const v1, 0x33449ad

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->e:Lcom/google/a/a/a/a/my;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->f:Lcom/google/a/a/a/a/ad;

    if-eqz v1, :cond_4

    const v1, 0x34d6cf6

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->f:Lcom/google/a/a/a/a/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->g:Lcom/google/a/a/a/a/ep;

    if-eqz v1, :cond_5

    const v1, 0x37256f3

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->g:Lcom/google/a/a/a/a/ep;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->h:Lcom/google/a/a/a/a/qx;

    if-eqz v1, :cond_6

    const v1, 0x3742f13

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->h:Lcom/google/a/a/a/a/qx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->i:Lcom/google/a/a/a/a/na;

    if-eqz v1, :cond_7

    const v1, 0x39515b9

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->i:Lcom/google/a/a/a/a/na;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->j:Lcom/google/a/a/a/a/ae;

    if-eqz v1, :cond_8

    const v1, 0x3a21c49

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->j:Lcom/google/a/a/a/a/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->k:Lcom/google/a/a/a/a/mz;

    if-eqz v1, :cond_9

    const v1, 0x3a23626

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->k:Lcom/google/a/a/a/a/mz;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->l:Lcom/google/a/a/a/a/aj;

    if-eqz v1, :cond_a

    const v1, 0x3b93ca7

    iget-object v2, p0, Lcom/google/a/a/a/a/dn;->l:Lcom/google/a/a/a/a/aj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/dn;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/dn;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/gu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/gu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->b:Lcom/google/a/a/a/a/gu;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->b:Lcom/google/a/a/a/a/gu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/pn;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pn;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->c:Lcom/google/a/a/a/a/pn;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->c:Lcom/google/a/a/a/a/pn;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/pq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->d:Lcom/google/a/a/a/a/pq;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->d:Lcom/google/a/a/a/a/pq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/my;

    invoke-direct {v0}, Lcom/google/a/a/a/a/my;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->e:Lcom/google/a/a/a/a/my;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->e:Lcom/google/a/a/a/a/my;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/ad;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ad;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->f:Lcom/google/a/a/a/a/ad;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->f:Lcom/google/a/a/a/a/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/ep;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ep;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->g:Lcom/google/a/a/a/a/ep;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->g:Lcom/google/a/a/a/a/ep;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/qx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/qx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->h:Lcom/google/a/a/a/a/qx;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->h:Lcom/google/a/a/a/a/qx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/na;

    invoke-direct {v0}, Lcom/google/a/a/a/a/na;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->i:Lcom/google/a/a/a/a/na;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->i:Lcom/google/a/a/a/a/na;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/ae;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ae;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->j:Lcom/google/a/a/a/a/ae;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->j:Lcom/google/a/a/a/a/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/mz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/mz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->k:Lcom/google/a/a/a/a/mz;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->k:Lcom/google/a/a/a/a/mz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/a/a/a/a/aj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/aj;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dn;->l:Lcom/google/a/a/a/a/aj;

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->l:Lcom/google/a/a/a/a/aj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x194d54ea -> :sswitch_1
        0x194f7a22 -> :sswitch_2
        0x194fdbca -> :sswitch_3
        0x19a24d6a -> :sswitch_4
        0x1a6b67b2 -> :sswitch_5
        0x1b92b79a -> :sswitch_6
        0x1ba1789a -> :sswitch_7
        0x1ca8adca -> :sswitch_8
        0x1d10e24a -> :sswitch_9
        0x1d11b132 -> :sswitch_a
        0x1dc9e53a -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->b:Lcom/google/a/a/a/a/gu;

    if-eqz v0, :cond_0

    const v0, 0x329aa9d

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->b:Lcom/google/a/a/a/a/gu;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->c:Lcom/google/a/a/a/a/pn;

    if-eqz v0, :cond_1

    const v0, 0x329ef44

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->c:Lcom/google/a/a/a/a/pn;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->d:Lcom/google/a/a/a/a/pq;

    if-eqz v0, :cond_2

    const v0, 0x329fb79

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->d:Lcom/google/a/a/a/a/pq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->e:Lcom/google/a/a/a/a/my;

    if-eqz v0, :cond_3

    const v0, 0x33449ad

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->e:Lcom/google/a/a/a/a/my;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->f:Lcom/google/a/a/a/a/ad;

    if-eqz v0, :cond_4

    const v0, 0x34d6cf6

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->f:Lcom/google/a/a/a/a/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->g:Lcom/google/a/a/a/a/ep;

    if-eqz v0, :cond_5

    const v0, 0x37256f3

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->g:Lcom/google/a/a/a/a/ep;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->h:Lcom/google/a/a/a/a/qx;

    if-eqz v0, :cond_6

    const v0, 0x3742f13

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->h:Lcom/google/a/a/a/a/qx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->i:Lcom/google/a/a/a/a/na;

    if-eqz v0, :cond_7

    const v0, 0x39515b9

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->i:Lcom/google/a/a/a/a/na;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->j:Lcom/google/a/a/a/a/ae;

    if-eqz v0, :cond_8

    const v0, 0x3a21c49

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->j:Lcom/google/a/a/a/a/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->k:Lcom/google/a/a/a/a/mz;

    if-eqz v0, :cond_9

    const v0, 0x3a23626

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->k:Lcom/google/a/a/a/a/mz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->l:Lcom/google/a/a/a/a/aj;

    if-eqz v0, :cond_a

    const v0, 0x3b93ca7

    iget-object v1, p0, Lcom/google/a/a/a/a/dn;->l:Lcom/google/a/a/a/a/aj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/dn;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
