.class public final Lcom/google/a/a/a/a/an;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/an;


# instance fields
.field public b:Lcom/google/a/a/a/a/ba;

.field public c:Lcom/google/a/a/a/a/fh;

.field public d:Lcom/google/a/a/a/a/oi;

.field public e:Lcom/google/a/a/a/a/sm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/an;

    sput-object v0, Lcom/google/a/a/a/a/an;->a:[Lcom/google/a/a/a/a/an;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    iput-object v0, p0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    iput-object v0, p0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    iput-object v0, p0, Lcom/google/a/a/a/a/an;->e:Lcom/google/a/a/a/a/sm;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    if-eqz v1, :cond_0

    const v0, 0x2c42002

    iget-object v1, p0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    if-eqz v1, :cond_1

    const v1, 0x2fe8b38

    iget-object v2, p0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    if-eqz v1, :cond_2

    const v1, 0x32ce059

    iget-object v2, p0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/an;->e:Lcom/google/a/a/a/a/sm;

    if-eqz v1, :cond_3

    const v1, 0x3ce028d

    iget-object v2, p0, Lcom/google/a/a/a/a/an;->e:Lcom/google/a/a/a/a/sm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/an;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/an;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/an;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/an;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/an;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/ba;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ba;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    iget-object v0, p0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/fh;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fh;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    iget-object v0, p0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/oi;

    invoke-direct {v0}, Lcom/google/a/a/a/a/oi;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    iget-object v0, p0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/sm;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sm;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/an;->e:Lcom/google/a/a/a/a/sm;

    iget-object v0, p0, Lcom/google/a/a/a/a/an;->e:Lcom/google/a/a/a/a/sm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x16210012 -> :sswitch_1
        0x17f459c2 -> :sswitch_2
        0x196702ca -> :sswitch_3
        0x1e70146a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    if-eqz v0, :cond_0

    const v0, 0x2c42002

    iget-object v1, p0, Lcom/google/a/a/a/a/an;->b:Lcom/google/a/a/a/a/ba;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    if-eqz v0, :cond_1

    const v0, 0x2fe8b38

    iget-object v1, p0, Lcom/google/a/a/a/a/an;->c:Lcom/google/a/a/a/a/fh;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    if-eqz v0, :cond_2

    const v0, 0x32ce059

    iget-object v1, p0, Lcom/google/a/a/a/a/an;->d:Lcom/google/a/a/a/a/oi;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/an;->e:Lcom/google/a/a/a/a/sm;

    if-eqz v0, :cond_3

    const v0, 0x3ce028d

    iget-object v1, p0, Lcom/google/a/a/a/a/an;->e:Lcom/google/a/a/a/a/sm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/an;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
