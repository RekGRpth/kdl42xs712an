.class public final Lcom/google/a/a/a/a/au;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/au;


# instance fields
.field public b:Lcom/google/a/a/a/a/uc;

.field public c:Lcom/google/a/a/a/a/bx;

.field public d:Lcom/google/a/a/a/a/oq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/au;

    sput-object v0, Lcom/google/a/a/a/a/au;->a:[Lcom/google/a/a/a/a/au;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/au;->b:Lcom/google/a/a/a/a/uc;

    iput-object v0, p0, Lcom/google/a/a/a/a/au;->c:Lcom/google/a/a/a/a/bx;

    iput-object v0, p0, Lcom/google/a/a/a/a/au;->d:Lcom/google/a/a/a/a/oq;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/au;->b:Lcom/google/a/a/a/a/uc;

    if-eqz v1, :cond_0

    const v0, 0x2da9690

    iget-object v1, p0, Lcom/google/a/a/a/a/au;->b:Lcom/google/a/a/a/a/uc;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/au;->c:Lcom/google/a/a/a/a/bx;

    if-eqz v1, :cond_1

    const v1, 0x31a40a8

    iget-object v2, p0, Lcom/google/a/a/a/a/au;->c:Lcom/google/a/a/a/a/bx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/au;->d:Lcom/google/a/a/a/a/oq;

    if-eqz v1, :cond_2

    const v1, 0x3c8cc39

    iget-object v2, p0, Lcom/google/a/a/a/a/au;->d:Lcom/google/a/a/a/a/oq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/au;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/au;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/au;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/au;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/au;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/uc;

    invoke-direct {v0}, Lcom/google/a/a/a/a/uc;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/au;->b:Lcom/google/a/a/a/a/uc;

    iget-object v0, p0, Lcom/google/a/a/a/a/au;->b:Lcom/google/a/a/a/a/uc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/bx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/bx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/au;->c:Lcom/google/a/a/a/a/bx;

    iget-object v0, p0, Lcom/google/a/a/a/a/au;->c:Lcom/google/a/a/a/a/bx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/oq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/oq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/au;->d:Lcom/google/a/a/a/a/oq;

    iget-object v0, p0, Lcom/google/a/a/a/a/au;->d:Lcom/google/a/a/a/a/oq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x16d4b482 -> :sswitch_1
        0x18d20542 -> :sswitch_2
        0x1e4661ca -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/au;->b:Lcom/google/a/a/a/a/uc;

    if-eqz v0, :cond_0

    const v0, 0x2da9690

    iget-object v1, p0, Lcom/google/a/a/a/a/au;->b:Lcom/google/a/a/a/a/uc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/au;->c:Lcom/google/a/a/a/a/bx;

    if-eqz v0, :cond_1

    const v0, 0x31a40a8

    iget-object v1, p0, Lcom/google/a/a/a/a/au;->c:Lcom/google/a/a/a/a/bx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/au;->d:Lcom/google/a/a/a/a/oq;

    if-eqz v0, :cond_2

    const v0, 0x3c8cc39

    iget-object v1, p0, Lcom/google/a/a/a/a/au;->d:Lcom/google/a/a/a/a/oq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/au;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
