.class public final Lcom/google/a/a/a/a/nl;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/nl;


# instance fields
.field public b:Lcom/google/a/a/a/a/np;

.field public c:Lcom/google/a/a/a/a/nn;

.field public d:Lcom/google/a/a/a/a/nm;

.field public e:Lcom/google/a/a/a/a/nq;

.field public f:Lcom/google/a/a/a/a/no;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Ljava/lang/String;

.field public p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/nl;

    sput-object v0, Lcom/google/a/a/a/a/nl;->a:[Lcom/google/a/a/a/a/nl;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->b:Lcom/google/a/a/a/a/np;

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->c:Lcom/google/a/a/a/a/nn;

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->d:Lcom/google/a/a/a/a/nm;

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->e:Lcom/google/a/a/a/a/nq;

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->f:Lcom/google/a/a/a/a/no;

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->g:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->h:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->i:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->j:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->k:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->l:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->m:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->n:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->o:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/a/a/a/a/nl;->p:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->b:Lcom/google/a/a/a/a/np;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->b:Lcom/google/a/a/a/a/np;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->c:Lcom/google/a/a/a/a/nn;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/nl;->c:Lcom/google/a/a/a/a/nn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->d:Lcom/google/a/a/a/a/nm;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/nl;->d:Lcom/google/a/a/a/a/nm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->e:Lcom/google/a/a/a/a/nq;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/a/a/a/nl;->e:Lcom/google/a/a/a/a/nq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->f:Lcom/google/a/a/a/a/no;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/nl;->f:Lcom/google/a/a/a/a/no;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->g:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->g:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->h:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->h:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->i:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->i:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->j:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->j:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->k:Z

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->k:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->l:Z

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->l:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->m:Z

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->m:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->n:Z

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->n:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/nl;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->p:Z

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nl;->p:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/nl;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/nl;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/np;

    invoke-direct {v0}, Lcom/google/a/a/a/a/np;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->b:Lcom/google/a/a/a/a/np;

    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->b:Lcom/google/a/a/a/a/np;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/nn;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nn;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->c:Lcom/google/a/a/a/a/nn;

    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->c:Lcom/google/a/a/a/a/nn;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/nm;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nm;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->d:Lcom/google/a/a/a/a/nm;

    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->d:Lcom/google/a/a/a/a/nm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/nq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->e:Lcom/google/a/a/a/a/nq;

    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->e:Lcom/google/a/a/a/a/nq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/no;

    invoke-direct {v0}, Lcom/google/a/a/a/a/no;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->f:Lcom/google/a/a/a/a/no;

    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->f:Lcom/google/a/a/a/a/no;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->g:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->h:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->i:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->j:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->k:Z

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->l:Z

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->m:Z

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->n:Z

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/nl;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nl;->p:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->b:Lcom/google/a/a/a/a/np;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->b:Lcom/google/a/a/a/a/np;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->c:Lcom/google/a/a/a/a/nn;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->c:Lcom/google/a/a/a/a/nn;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->d:Lcom/google/a/a/a/a/nm;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->d:Lcom/google/a/a/a/a/nm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->e:Lcom/google/a/a/a/a/nq;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->e:Lcom/google/a/a/a/a/nq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->f:Lcom/google/a/a/a/a/no;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->f:Lcom/google/a/a/a/a/no;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->g:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_5
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->h:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_6
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->i:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->j:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_8
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->k:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_9
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->l:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_a
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->m:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_b
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->n:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->n:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_c
    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/a/a/a/nl;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_d
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nl;->p:Z

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nl;->p:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_e
    iget-object v0, p0, Lcom/google/a/a/a/a/nl;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
