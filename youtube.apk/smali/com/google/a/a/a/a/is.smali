.class public final Lcom/google/a/a/a/a/is;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/is;


# instance fields
.field public b:Lcom/google/a/a/a/a/dj;

.field public c:Lcom/google/a/a/a/a/de;

.field public d:Lcom/google/a/a/a/a/db;

.field public e:Lcom/google/a/a/a/a/df;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/is;

    sput-object v0, Lcom/google/a/a/a/a/is;->a:[Lcom/google/a/a/a/a/is;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    iput-object v0, p0, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    iput-object v0, p0, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    iput-object v0, p0, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    if-eqz v1, :cond_0

    const v0, 0x3049143

    iget-object v1, p0, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    if-eqz v1, :cond_1

    const v1, 0x3064567

    iget-object v2, p0, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    if-eqz v1, :cond_2

    const v1, 0x3070f41

    iget-object v2, p0, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    if-eqz v1, :cond_3

    const v1, 0x32b52b9

    iget-object v2, p0, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/is;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/is;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/is;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/is;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/is;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/dj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dj;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    iget-object v0, p0, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/de;

    invoke-direct {v0}, Lcom/google/a/a/a/a/de;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    iget-object v0, p0, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/db;

    invoke-direct {v0}, Lcom/google/a/a/a/a/db;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    iget-object v0, p0, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/df;

    invoke-direct {v0}, Lcom/google/a/a/a/a/df;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    iget-object v0, p0, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18248a1a -> :sswitch_1
        0x18322b3a -> :sswitch_2
        0x18387a0a -> :sswitch_3
        0x195a95ca -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    if-eqz v0, :cond_0

    const v0, 0x3049143

    iget-object v1, p0, Lcom/google/a/a/a/a/is;->b:Lcom/google/a/a/a/a/dj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    if-eqz v0, :cond_1

    const v0, 0x3064567

    iget-object v1, p0, Lcom/google/a/a/a/a/is;->c:Lcom/google/a/a/a/a/de;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    if-eqz v0, :cond_2

    const v0, 0x3070f41

    iget-object v1, p0, Lcom/google/a/a/a/a/is;->d:Lcom/google/a/a/a/a/db;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    if-eqz v0, :cond_3

    const v0, 0x32b52b9

    iget-object v1, p0, Lcom/google/a/a/a/a/is;->e:Lcom/google/a/a/a/a/df;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/is;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
