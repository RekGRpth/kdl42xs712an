.class public final Lcom/google/a/a/a/a/wg;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/wg;


# instance fields
.field public b:Lcom/google/a/a/a/a/py;

.field public c:Lcom/google/a/a/a/a/pu;

.field public d:Lcom/google/a/a/a/a/pu;

.field public e:[Lcom/google/a/a/a/a/pu;

.field public f:Lcom/google/a/a/a/a/pu;

.field public g:Ljava/lang/String;

.field public h:Lcom/google/a/a/a/a/wk;

.field public i:Lcom/google/a/a/a/a/dq;

.field public j:Lcom/google/a/a/a/a/kz;

.field public k:Lcom/google/a/a/a/a/ej;

.field public l:Lcom/google/a/a/a/a/pi;

.field public m:[Lcom/google/a/a/a/a/wc;

.field public n:[B

.field public o:Lcom/google/a/a/a/a/nu;

.field public p:Lcom/google/a/a/a/a/dm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/wg;

    sput-object v0, Lcom/google/a/a/a/a/wg;->a:[Lcom/google/a/a/a/a/wg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->b:Lcom/google/a/a/a/a/py;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->c:Lcom/google/a/a/a/a/pu;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->d:Lcom/google/a/a/a/a/pu;

    sget-object v0, Lcom/google/a/a/a/a/pu;->a:[Lcom/google/a/a/a/a/pu;

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->f:Lcom/google/a/a/a/a/pu;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->i:Lcom/google/a/a/a/a/dq;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->k:Lcom/google/a/a/a/a/ej;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->l:Lcom/google/a/a/a/a/pi;

    sget-object v0, Lcom/google/a/a/a/a/wc;->a:[Lcom/google/a/a/a/a/wc;

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    sget-object v0, Lcom/google/protobuf/nano/f;->l:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->n:[B

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->o:Lcom/google/a/a/a/a/nu;

    iput-object v1, p0, Lcom/google/a/a/a/a/wg;->p:Lcom/google/a/a/a/a/dm;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->b:Lcom/google/a/a/a/a/py;

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->b:Lcom/google/a/a/a/a/py;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->c:Lcom/google/a/a/a/a/pu;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->c:Lcom/google/a/a/a/a/pu;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->d:Lcom/google/a/a/a/a/pu;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->d:Lcom/google/a/a/a/a/pu;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    if-eqz v2, :cond_2

    iget-object v4, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v3, v4, v2

    const/4 v6, 0x4

    invoke-static {v6, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->f:Lcom/google/a/a/a/a/pu;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->f:Lcom/google/a/a/a/a/pu;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->i:Lcom/google/a/a/a/a/dq;

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->i:Lcom/google/a/a/a/a/dq;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->k:Lcom/google/a/a/a/a/ej;

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->k:Lcom/google/a/a/a/a/ej;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->l:Lcom/google/a/a/a/a/pi;

    if-eqz v2, :cond_9

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->l:Lcom/google/a/a/a/a/pi;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    const/16 v5, 0xc

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->n:[B

    sget-object v2, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_b

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->n:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->o:Lcom/google/a/a/a/a/nu;

    if-eqz v1, :cond_c

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->o:Lcom/google/a/a/a/a/nu;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->p:Lcom/google/a/a/a/a/dm;

    if-eqz v1, :cond_d

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->p:Lcom/google/a/a/a/a/dm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/wg;->dm:I

    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/wg;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/py;

    invoke-direct {v0}, Lcom/google/a/a/a/a/py;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->b:Lcom/google/a/a/a/a/py;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->b:Lcom/google/a/a/a/a/py;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/pu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->c:Lcom/google/a/a/a/a/pu;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->c:Lcom/google/a/a/a/a/pu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/pu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->d:Lcom/google/a/a/a/a/pu;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->d:Lcom/google/a/a/a/a/pu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/pu;

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    new-instance v3, Lcom/google/a/a/a/a/pu;

    invoke-direct {v3}, Lcom/google/a/a/a/a/pu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    new-instance v3, Lcom/google/a/a/a/a/pu;

    invoke-direct {v3}, Lcom/google/a/a/a/a/pu;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/pu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->f:Lcom/google/a/a/a/a/pu;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->f:Lcom/google/a/a/a/a/pu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/wk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/wk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/dq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->i:Lcom/google/a/a/a/a/dq;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->i:Lcom/google/a/a/a/a/dq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/kz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/ej;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ej;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->k:Lcom/google/a/a/a/a/ej;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->k:Lcom/google/a/a/a/a/ej;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/a/a/a/a/pi;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pi;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->l:Lcom/google/a/a/a/a/pi;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->l:Lcom/google/a/a/a/a/pi;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/wc;

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    :goto_4
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    new-instance v3, Lcom/google/a/a/a/a/wc;

    invoke-direct {v3}, Lcom/google/a/a/a/a/wc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    new-instance v3, Lcom/google/a/a/a/a/wc;

    invoke-direct {v3}, Lcom/google/a/a/a/a/wc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->n:[B

    goto/16 :goto_0

    :sswitch_e
    new-instance v0, Lcom/google/a/a/a/a/nu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->o:Lcom/google/a/a/a/a/nu;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->o:Lcom/google/a/a/a/a/nu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_f
    new-instance v0, Lcom/google/a/a/a/a/dm;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dm;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wg;->p:Lcom/google/a/a/a/a/dm;

    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->p:Lcom/google/a/a/a/a/dm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->b:Lcom/google/a/a/a/a/py;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->b:Lcom/google/a/a/a/a/py;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->c:Lcom/google/a/a/a/a/pu;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->c:Lcom/google/a/a/a/a/pu;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->d:Lcom/google/a/a/a/a/pu;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->d:Lcom/google/a/a/a/a/pu;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->e:[Lcom/google/a/a/a/a/pu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->f:Lcom/google/a/a/a/a/pu;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->f:Lcom/google/a/a/a/a/pu;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->i:Lcom/google/a/a/a/a/dq;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->i:Lcom/google/a/a/a/a/dq;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->k:Lcom/google/a/a/a/a/ej;

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->k:Lcom/google/a/a/a/a/ej;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->l:Lcom/google/a/a/a/a/pi;

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/a/a/a/a/wg;->l:Lcom/google/a/a/a/a/pi;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->m:[Lcom/google/a/a/a/a/wc;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    const/16 v4, 0xc

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->n:[B

    sget-object v1, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->n:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    :cond_c
    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->o:Lcom/google/a/a/a/a/nu;

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->o:Lcom/google/a/a/a/a/nu;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_d
    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->p:Lcom/google/a/a/a/a/dm;

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/a/a/a/a/wg;->p:Lcom/google/a/a/a/a/dm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_e
    iget-object v0, p0, Lcom/google/a/a/a/a/wg;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
