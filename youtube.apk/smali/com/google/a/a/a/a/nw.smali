.class public final Lcom/google/a/a/a/a/nw;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/nw;


# instance fields
.field public b:Lcom/google/a/a/a/a/py;

.field public c:Lcom/google/a/a/a/a/mw;

.field public d:Lcom/google/a/a/a/a/sb;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/a/a/a/a/gr;

.field public g:[Lcom/google/a/a/a/a/nc;

.field public h:Lcom/google/a/a/a/a/nb;

.field public i:Lcom/google/a/a/a/a/bh;

.field public j:Lcom/google/a/a/a/a/uh;

.field public k:Lcom/google/a/a/a/a/v;

.field public l:Lcom/google/a/a/a/a/md;

.field public m:Lcom/google/a/a/a/a/nh;

.field public n:Lcom/google/a/a/a/a/sa;

.field public o:Lcom/google/a/a/a/a/sv;

.field public p:Lcom/google/a/a/a/a/kf;

.field public q:Lcom/google/a/a/a/a/jm;

.field public r:Lcom/google/a/a/a/a/ig;

.field public s:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/nw;

    sput-object v0, Lcom/google/a/a/a/a/nw;->a:[Lcom/google/a/a/a/a/nw;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->b:Lcom/google/a/a/a/a/py;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->f:Lcom/google/a/a/a/a/gr;

    sget-object v0, Lcom/google/a/a/a/a/nc;->a:[Lcom/google/a/a/a/a/nc;

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->h:Lcom/google/a/a/a/a/nb;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->n:Lcom/google/a/a/a/a/sa;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->o:Lcom/google/a/a/a/a/sv;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->p:Lcom/google/a/a/a/a/kf;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->q:Lcom/google/a/a/a/a/jm;

    iput-object v1, p0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    sget-object v0, Lcom/google/protobuf/nano/f;->l:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->s:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->b:Lcom/google/a/a/a/a/py;

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->b:Lcom/google/a/a/a/a/py;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/a/a/a/a/nw;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->f:Lcom/google/a/a/a/a/gr;

    if-eqz v2, :cond_3

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/a/a/a/a/nw;->f:Lcom/google/a/a/a/a/gr;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    const/4 v5, 0x7

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->h:Lcom/google/a/a/a/a/nb;

    if-eqz v1, :cond_5

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->h:Lcom/google/a/a/a/a/nb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    if-eqz v1, :cond_6

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    if-eqz v1, :cond_7

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    if-eqz v1, :cond_8

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    if-eqz v1, :cond_9

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    if-eqz v1, :cond_a

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->n:Lcom/google/a/a/a/a/sa;

    if-eqz v1, :cond_b

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->n:Lcom/google/a/a/a/a/sa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->o:Lcom/google/a/a/a/a/sv;

    if-eqz v1, :cond_c

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->o:Lcom/google/a/a/a/a/sv;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->p:Lcom/google/a/a/a/a/kf;

    if-eqz v1, :cond_d

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->p:Lcom/google/a/a/a/a/kf;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->q:Lcom/google/a/a/a/a/jm;

    if-eqz v1, :cond_e

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->q:Lcom/google/a/a/a/a/jm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    if-eqz v1, :cond_f

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->s:[B

    sget-object v2, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_10

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->s:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/nw;->dm:I

    return v0

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/nw;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/py;

    invoke-direct {v0}, Lcom/google/a/a/a/a/py;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->b:Lcom/google/a/a/a/a/py;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->b:Lcom/google/a/a/a/a/py;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/mw;

    invoke-direct {v0}, Lcom/google/a/a/a/a/mw;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/sb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/gr;

    invoke-direct {v0}, Lcom/google/a/a/a/a/gr;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->f:Lcom/google/a/a/a/a/gr;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->f:Lcom/google/a/a/a/a/gr;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/nc;

    iget-object v3, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    new-instance v3, Lcom/google/a/a/a/a/nc;

    invoke-direct {v3}, Lcom/google/a/a/a/a/nc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    new-instance v3, Lcom/google/a/a/a/a/nc;

    invoke-direct {v3}, Lcom/google/a/a/a/a/nc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/nb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->h:Lcom/google/a/a/a/a/nb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->h:Lcom/google/a/a/a/a/nb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/bh;

    invoke-direct {v0}, Lcom/google/a/a/a/a/bh;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/uh;

    invoke-direct {v0}, Lcom/google/a/a/a/a/uh;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/v;

    invoke-direct {v0}, Lcom/google/a/a/a/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/a/a/a/a/md;

    invoke-direct {v0}, Lcom/google/a/a/a/a/md;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lcom/google/a/a/a/a/nh;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nh;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_d
    new-instance v0, Lcom/google/a/a/a/a/sa;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sa;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->n:Lcom/google/a/a/a/a/sa;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->n:Lcom/google/a/a/a/a/sa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_e
    new-instance v0, Lcom/google/a/a/a/a/sv;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sv;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->o:Lcom/google/a/a/a/a/sv;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->o:Lcom/google/a/a/a/a/sv;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_f
    new-instance v0, Lcom/google/a/a/a/a/kf;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kf;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->p:Lcom/google/a/a/a/a/kf;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->p:Lcom/google/a/a/a/a/kf;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_10
    new-instance v0, Lcom/google/a/a/a/a/jm;

    invoke-direct {v0}, Lcom/google/a/a/a/a/jm;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->q:Lcom/google/a/a/a/a/jm;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->q:Lcom/google/a/a/a/a/jm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_11
    new-instance v0, Lcom/google/a/a/a/a/ig;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ig;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/nw;->s:[B

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
        0x82 -> :sswitch_d
        0x8a -> :sswitch_e
        0x92 -> :sswitch_f
        0x9a -> :sswitch_10
        0xa2 -> :sswitch_11
        0xaa -> :sswitch_12
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->b:Lcom/google/a/a/a/a/py;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->b:Lcom/google/a/a/a/a/py;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->f:Lcom/google/a/a/a/a/gr;

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->f:Lcom/google/a/a/a/a/gr;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->h:Lcom/google/a/a/a/a/nb;

    if-eqz v0, :cond_6

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->h:Lcom/google/a/a/a/a/nb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    if-eqz v0, :cond_7

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    if-eqz v0, :cond_8

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    if-eqz v0, :cond_9

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    if-eqz v0, :cond_a

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    if-eqz v0, :cond_b

    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->n:Lcom/google/a/a/a/a/sa;

    if-eqz v0, :cond_c

    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->n:Lcom/google/a/a/a/a/sa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_c
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->o:Lcom/google/a/a/a/a/sv;

    if-eqz v0, :cond_d

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->o:Lcom/google/a/a/a/a/sv;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_d
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->p:Lcom/google/a/a/a/a/kf;

    if-eqz v0, :cond_e

    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->p:Lcom/google/a/a/a/a/kf;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_e
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->q:Lcom/google/a/a/a/a/jm;

    if-eqz v0, :cond_f

    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->q:Lcom/google/a/a/a/a/jm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_f
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    if-eqz v0, :cond_10

    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_10
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->s:[B

    sget-object v1, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_11

    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/a/a/a/a/nw;->s:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    :cond_11
    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
