.class public final Lcom/google/a/a/a/a/ar;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/ar;


# instance fields
.field public b:Lcom/google/a/a/a/a/py;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/a/a/a/a/as;

.field public e:Lcom/google/a/a/a/a/dq;

.field public f:Lcom/google/a/a/a/a/ao;

.field public g:Lcom/google/a/a/a/a/an;

.field public h:[Lcom/google/a/a/a/a/al;

.field public i:Lcom/google/a/a/a/a/ap;

.field public j:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/ar;

    sput-object v0, Lcom/google/a/a/a/a/ar;->a:[Lcom/google/a/a/a/a/ar;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/ar;->b:Lcom/google/a/a/a/a/py;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    iput-object v1, p0, Lcom/google/a/a/a/a/ar;->e:Lcom/google/a/a/a/a/dq;

    iput-object v1, p0, Lcom/google/a/a/a/a/ar;->f:Lcom/google/a/a/a/a/ao;

    iput-object v1, p0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    sget-object v0, Lcom/google/a/a/a/a/al;->a:[Lcom/google/a/a/a/a/al;

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    iput-object v1, p0, Lcom/google/a/a/a/a/ar;->i:Lcom/google/a/a/a/a/ap;

    sget-object v0, Lcom/google/protobuf/nano/f;->l:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->j:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->b:Lcom/google/a/a/a/a/py;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->b:Lcom/google/a/a/a/a/py;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/a/a/a/a/ar;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    if-eqz v2, :cond_1

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->e:Lcom/google/a/a/a/a/dq;

    if-eqz v2, :cond_2

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/a/a/a/a/ar;->e:Lcom/google/a/a/a/a/dq;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->f:Lcom/google/a/a/a/a/ao;

    if-eqz v2, :cond_3

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/a/a/a/a/ar;->f:Lcom/google/a/a/a/a/ao;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    if-eqz v2, :cond_4

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    const/16 v5, 0xe

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->i:Lcom/google/a/a/a/a/ap;

    if-eqz v1, :cond_6

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->i:Lcom/google/a/a/a/a/ap;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->j:[B

    sget-object v2, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->j:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/ar;->dm:I

    return v0

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/ar;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/py;

    invoke-direct {v0}, Lcom/google/a/a/a/a/py;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->b:Lcom/google/a/a/a/a/py;

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->b:Lcom/google/a/a/a/a/py;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/as;

    invoke-direct {v0}, Lcom/google/a/a/a/a/as;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/dq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->e:Lcom/google/a/a/a/a/dq;

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->e:Lcom/google/a/a/a/a/dq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/ao;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ao;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->f:Lcom/google/a/a/a/a/ao;

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->f:Lcom/google/a/a/a/a/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/an;

    invoke-direct {v0}, Lcom/google/a/a/a/a/an;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x72

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/al;

    iget-object v3, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    new-instance v3, Lcom/google/a/a/a/a/al;

    invoke-direct {v3}, Lcom/google/a/a/a/a/al;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    new-instance v3, Lcom/google/a/a/a/a/al;

    invoke-direct {v3}, Lcom/google/a/a/a/a/al;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/ap;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ap;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->i:Lcom/google/a/a/a/a/ap;

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->i:Lcom/google/a/a/a/a/ap;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/ar;->j:[B

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x2a -> :sswitch_2
        0x4a -> :sswitch_3
        0x52 -> :sswitch_4
        0x62 -> :sswitch_5
        0x6a -> :sswitch_6
        0x72 -> :sswitch_7
        0x7a -> :sswitch_8
        0x82 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->b:Lcom/google/a/a/a/a/py;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->b:Lcom/google/a/a/a/a/py;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    if-eqz v0, :cond_2

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->d:Lcom/google/a/a/a/a/as;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->e:Lcom/google/a/a/a/a/dq;

    if-eqz v0, :cond_3

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->e:Lcom/google/a/a/a/a/dq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->f:Lcom/google/a/a/a/a/ao;

    if-eqz v0, :cond_4

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->f:Lcom/google/a/a/a/a/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    if-eqz v0, :cond_5

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->g:Lcom/google/a/a/a/a/an;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->h:[Lcom/google/a/a/a/a/al;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    const/16 v4, 0xe

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->i:Lcom/google/a/a/a/a/ap;

    if-eqz v0, :cond_7

    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->i:Lcom/google/a/a/a/a/ap;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->j:[B

    sget-object v1, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_8

    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/a/a/a/a/ar;->j:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/ar;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
