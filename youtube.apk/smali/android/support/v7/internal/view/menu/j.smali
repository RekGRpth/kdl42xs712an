.class public abstract Landroid/support/v7/internal/view/menu/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/ad;


# instance fields
.field private a:Landroid/support/v7/internal/view/menu/ae;

.field private b:I

.field protected c:Landroid/content/Context;

.field protected d:Landroid/content/Context;

.field protected e:Landroid/support/v7/internal/view/menu/o;

.field protected f:Landroid/view/LayoutInflater;

.field protected g:Landroid/view/LayoutInflater;

.field protected h:Landroid/support/v7/internal/view/menu/af;

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/internal/view/menu/j;->c:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/j;->f:Landroid/view/LayoutInflater;

    iput p2, p0, Landroid/support/v7/internal/view/menu/j;->b:I

    iput p3, p0, Landroid/support/v7/internal/view/menu/j;->i:I

    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/af;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->h:Landroid/support/v7/internal/view/menu/af;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->f:Landroid/view/LayoutInflater;

    iget v1, p0, Landroid/support/v7/internal/view/menu/j;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/af;

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/j;->h:Landroid/support/v7/internal/view/menu/af;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->h:Landroid/support/v7/internal/view/menu/af;

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/j;->e:Landroid/support/v7/internal/view/menu/o;

    invoke-interface {v0, v1}, Landroid/support/v7/internal/view/menu/af;->a(Landroid/support/v7/internal/view/menu/o;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/j;->c(Z)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->h:Landroid/support/v7/internal/view/menu/af;

    return-object v0
.end method

.method public a(Landroid/support/v7/internal/view/menu/s;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    instance-of v0, p2, Landroid/support/v7/internal/view/menu/ag;

    if-eqz v0, :cond_0

    check-cast p2, Landroid/support/v7/internal/view/menu/ag;

    move-object v0, p2

    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/support/v7/internal/view/menu/j;->a(Landroid/support/v7/internal/view/menu/s;Landroid/support/v7/internal/view/menu/ag;)V

    check-cast v0, Landroid/view/View;

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->f:Landroid/view/LayoutInflater;

    iget v1, p0, Landroid/support/v7/internal/view/menu/j;->i:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ag;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/o;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/internal/view/menu/j;->d:Landroid/content/Context;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/j;->g:Landroid/view/LayoutInflater;

    iput-object p2, p0, Landroid/support/v7/internal/view/menu/j;->e:Landroid/support/v7/internal/view/menu/o;

    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ae;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/internal/view/menu/j;->a:Landroid/support/v7/internal/view/menu/ae;

    return-void
.end method

.method public a(Landroid/support/v7/internal/view/menu/o;Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->a:Landroid/support/v7/internal/view/menu/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->a:Landroid/support/v7/internal/view/menu/ae;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/ae;->a(Landroid/support/v7/internal/view/menu/o;Z)V

    :cond_0
    return-void
.end method

.method public abstract a(Landroid/support/v7/internal/view/menu/s;Landroid/support/v7/internal/view/menu/ag;)V
.end method

.method public a(Landroid/support/v7/internal/view/menu/aj;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->a:Landroid/support/v7/internal/view/menu/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->a:Landroid/support/v7/internal/view/menu/ae;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/view/menu/ae;->b(Landroid/support/v7/internal/view/menu/o;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/support/v7/internal/view/menu/s;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Landroid/view/ViewGroup;I)Z
    .locals 1

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public final b(I)V
    .locals 0

    iput p1, p0, Landroid/support/v7/internal/view/menu/j;->j:I

    return-void
.end method

.method public final b(Landroid/support/v7/internal/view/menu/s;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c(Z)V
    .locals 10

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/j;->h:Landroid/support/v7/internal/view/menu/af;

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/j;->e:Landroid/support/v7/internal/view/menu/o;

    if-eqz v1, :cond_8

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/j;->e:Landroid/support/v7/internal/view/menu/o;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/o;->k()V

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/j;->e:Landroid/support/v7/internal/view/menu/o;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/o;->j()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v5

    move v4, v5

    :goto_0
    if-ge v6, v8, :cond_6

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/view/menu/s;

    invoke-virtual {p0, v1}, Landroid/support/v7/internal/view/menu/j;->a(Landroid/support/v7/internal/view/menu/s;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v2, v3, Landroid/support/v7/internal/view/menu/ag;

    if-eqz v2, :cond_5

    move-object v2, v3

    check-cast v2, Landroid/support/v7/internal/view/menu/ag;

    invoke-interface {v2}, Landroid/support/v7/internal/view/menu/ag;->a()Landroid/support/v7/internal/view/menu/s;

    move-result-object v2

    :goto_1
    invoke-virtual {p0, v1, v3, v0}, Landroid/support/v7/internal/view/menu/j;->a(Landroid/support/v7/internal/view/menu/s;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    if-eq v1, v2, :cond_2

    invoke-virtual {v9, v5}, Landroid/view/View;->setPressed(Z)V

    :cond_2
    if-eq v9, v3, :cond_4

    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/j;->h:Landroid/support/v7/internal/view/menu/af;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v9, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_4
    add-int/lit8 v1, v4, 0x1

    :goto_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    :goto_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v4, v1, :cond_0

    invoke-virtual {p0, v0, v4}, Landroid/support/v7/internal/view/menu/j;->a(Landroid/view/ViewGroup;I)Z

    move-result v1

    if-nez v1, :cond_6

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_7
    move v1, v4

    goto :goto_2

    :cond_8
    move v4, v5

    goto :goto_3
.end method

.method public final c(Landroid/support/v7/internal/view/menu/s;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
