.class public Landroid/support/v7/internal/view/menu/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/ad;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;


# static fields
.field static final b:I


# instance fields
.field private a:Landroid/content/Context;

.field c:Z

.field private d:Landroid/view/LayoutInflater;

.field private e:Landroid/support/v7/internal/widget/ListPopupWindow;

.field private f:Landroid/support/v7/internal/view/menu/o;

.field private g:I

.field private h:Landroid/view/View;

.field private i:Z

.field private j:Landroid/view/ViewTreeObserver;

.field private k:Landroid/support/v7/internal/view/menu/ac;

.field private l:Landroid/support/v7/internal/view/menu/ae;

.field private m:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Landroid/support/v7/a/h;->q:I

    sput v0, Landroid/support/v7/internal/view/menu/ab;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/o;Landroid/view/View;Z)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/internal/view/menu/ab;->a:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->d:Landroid/view/LayoutInflater;

    iput-object p2, p0, Landroid/support/v7/internal/view/menu/ab;->f:Landroid/support/v7/internal/view/menu/o;

    iput-boolean p4, p0, Landroid/support/v7/internal/view/menu/ab;->i:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Landroid/support/v7/a/e;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/internal/view/menu/ab;->g:I

    iput-object p3, p0, Landroid/support/v7/internal/view/menu/ab;->h:Landroid/view/View;

    invoke-virtual {p2, p0}, Landroid/support/v7/internal/view/menu/o;->a(Landroid/support/v7/internal/view/menu/ad;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/ab;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/ab;->i:Z

    return v0
.end method

.method static synthetic b(Landroid/support/v7/internal/view/menu/ab;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->d:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic c(Landroid/support/v7/internal/view/menu/ab;)Landroid/support/v7/internal/view/menu/o;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->f:Landroid/support/v7/internal/view/menu/o;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/o;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ae;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/internal/view/menu/ab;->l:Landroid/support/v7/internal/view/menu/ae;

    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/o;Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->f:Landroid/support/v7/internal/view/menu/o;

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ab;->b()V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->l:Landroid/support/v7/internal/view/menu/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->l:Landroid/support/v7/internal/view/menu/ae;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/ae;->a(Landroid/support/v7/internal/view/menu/o;Z)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 13

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/support/v7/internal/widget/ListPopupWindow;

    iget-object v4, p0, Landroid/support/v7/internal/view/menu/ab;->a:Landroid/content/Context;

    sget v5, Landroid/support/v7/a/c;->k:I

    invoke-direct {v0, v4, v3, v5}, Landroid/support/v7/internal/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ListPopupWindow;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/ListPopupWindow;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Landroid/support/v7/internal/view/menu/ac;

    iget-object v4, p0, Landroid/support/v7/internal/view/menu/ab;->f:Landroid/support/v7/internal/view/menu/o;

    invoke-direct {v0, p0, v4}, Landroid/support/v7/internal/view/menu/ac;-><init>(Landroid/support/v7/internal/view/menu/ab;Landroid/support/v7/internal/view/menu/o;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->k:Landroid/support/v7/internal/view/menu/ac;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    iget-object v4, p0, Landroid/support/v7/internal/view/menu/ab;->k:Landroid/support/v7/internal/view/menu/ac;

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/ListPopupWindow;->a(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ListPopupWindow;->a(Z)V

    iget-object v4, p0, Landroid/support/v7/internal/view/menu/ab;->h:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->j:Landroid/view/ViewTreeObserver;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    iput-object v5, p0, Landroid/support/v7/internal/view/menu/ab;->j:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->j:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/widget/ListPopupWindow;->a(Landroid/view/View;)V

    iget-object v7, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    iget-object v8, p0, Landroid/support/v7/internal/view/menu/ab;->k:Landroid/support/v7/internal/view/menu/ac;

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I

    move-result v11

    move v5, v2

    move-object v4, v3

    move v6, v2

    :goto_1
    if-ge v5, v11, :cond_4

    invoke-interface {v8, v5}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    if-eq v0, v2, :cond_5

    move-object v2, v3

    :goto_2
    iget-object v4, p0, Landroid/support/v7/internal/view/menu/ab;->m:Landroid/view/ViewGroup;

    if-nez v4, :cond_1

    new-instance v4, Landroid/widget/FrameLayout;

    iget-object v12, p0, Landroid/support/v7/internal/view/menu/ab;->a:Landroid/content/Context;

    invoke-direct {v4, v12}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Landroid/support/v7/internal/view/menu/ab;->m:Landroid/view/ViewGroup;

    :cond_1
    iget-object v4, p0, Landroid/support/v7/internal/view/menu/ab;->m:Landroid/view/ViewGroup;

    invoke-interface {v8, v5, v2, v4}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9, v10}, Landroid/view/View;->measure(II)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    :goto_3
    return v1

    :cond_4
    iget v0, p0, Landroid/support/v7/internal/view/menu/ab;->g:I

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/support/v7/internal/widget/ListPopupWindow;->d(I)V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ListPopupWindow;->e(I)V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ListPopupWindow;->c()V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ListPopupWindow;->h()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_3

    :cond_5
    move v0, v2

    move-object v2, v4

    goto :goto_2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/aj;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/aj;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v3, Landroid/support/v7/internal/view/menu/ab;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->a:Landroid/content/Context;

    iget-object v4, p0, Landroid/support/v7/internal/view/menu/ab;->h:Landroid/view/View;

    invoke-direct {v3, v0, p1, v4, v2}, Landroid/support/v7/internal/view/menu/ab;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/o;Landroid/view/View;Z)V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->l:Landroid/support/v7/internal/view/menu/ae;

    iput-object v0, v3, Landroid/support/v7/internal/view/menu/ab;->l:Landroid/support/v7/internal/view/menu/ae;

    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/aj;->size()I

    move-result v4

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_3

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/view/menu/aj;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Landroid/support/v7/internal/view/menu/ab;->c:Z

    invoke-virtual {v3}, Landroid/support/v7/internal/view/menu/ab;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->l:Landroid/support/v7/internal/view/menu/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->l:Landroid/support/v7/internal/view/menu/ae;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/view/menu/ae;->b(Landroid/support/v7/internal/view/menu/o;)Z

    :cond_0
    :goto_2
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ab;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ListPopupWindow;->d()V

    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v7/internal/view/menu/s;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->k:Landroid/support/v7/internal/view/menu/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->k:Landroid/support/v7/internal/view/menu/ac;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/ac;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ListPopupWindow;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/support/v7/internal/view/menu/s;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDismiss()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->f:Landroid/support/v7/internal/view/menu/o;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/o;->close()V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->j:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->j:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->j:Landroid/view/ViewTreeObserver;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->j:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iput-object v1, p0, Landroid/support/v7/internal/view/menu/ab;->j:Landroid/view/ViewTreeObserver;

    :cond_1
    return-void
.end method

.method public onGlobalLayout()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ab;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ab;->b()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ab;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->e:Landroid/support/v7/internal/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ListPopupWindow;->c()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/ab;->k:Landroid/support/v7/internal/view/menu/ac;

    invoke-static {v0}, Landroid/support/v7/internal/view/menu/ac;->a(Landroid/support/v7/internal/view/menu/ac;)Landroid/support/v7/internal/view/menu/o;

    move-result-object v1

    invoke-virtual {v0, p3}, Landroid/support/v7/internal/view/menu/ac;->a(I)Landroid/support/v7/internal/view/menu/s;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/internal/view/menu/o;->a(Landroid/view/MenuItem;I)Z

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/ab;->b()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
