.class final Landroid/support/v7/media/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Landroid/support/v7/media/as;

.field private final b:Landroid/os/Messenger;

.field private final c:Landroid/support/v7/media/ay;

.field private final d:Landroid/os/Messenger;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private final i:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>(Landroid/support/v7/media/as;Landroid/os/Messenger;)V
    .locals 2

    const/4 v0, 0x1

    iput-object p1, p0, Landroid/support/v7/media/at;->a:Landroid/support/v7/media/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    iput v0, p0, Landroid/support/v7/media/at;->f:I

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    iput-object p2, p0, Landroid/support/v7/media/at;->b:Landroid/os/Messenger;

    new-instance v0, Landroid/support/v7/media/ay;

    invoke-direct {v0, p0}, Landroid/support/v7/media/ay;-><init>(Landroid/support/v7/media/at;)V

    iput-object v0, p0, Landroid/support/v7/media/at;->c:Landroid/support/v7/media/ay;

    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Landroid/support/v7/media/at;->c:Landroid/support/v7/media/ay;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/support/v7/media/at;->d:Landroid/os/Messenger;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/at;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/x;

    invoke-virtual {v0, v2, v2}, Landroid/support/v7/media/x;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method private a(IIILjava/lang/Object;Landroid/os/Bundle;)Z
    .locals 3

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Landroid/support/v7/media/at;->d:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    :try_start_0
    iget-object v1, p0, Landroid/support/v7/media/at;->b:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const-string v1, "MediaRouteProviderProxy"

    const-string v2, "Could not send message to service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 6

    iget v3, p0, Landroid/support/v7/media/at;->f:I

    add-int/lit8 v0, v3, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->f:I

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v0, "routeId"

    invoke-virtual {v5, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x3

    iget v2, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    const/4 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    return v3
.end method

.method public final a(II)V
    .locals 6

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v1, 0x7

    iget v2, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    return-void
.end method

.method public final a(Landroid/support/v7/media/e;)V
    .locals 6

    const/4 v5, 0x0

    const/16 v1, 0xa

    iget v2, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/media/e;->d()Landroid/os/Bundle;

    move-result-object v4

    :goto_0
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    return-void

    :cond_0
    move-object v4, v5

    goto :goto_0
.end method

.method public final a()Z
    .locals 7

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    iget v0, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Landroid/support/v7/media/at;->e:I

    iput v0, p0, Landroid/support/v7/media/at;->h:I

    iget v2, p0, Landroid/support/v7/media/at;->h:I

    move-object v0, p0

    move v3, v1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v6

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/media/at;->b:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Landroid/support/v7/media/at;->binderDied()V

    move v1, v6

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Landroid/support/v7/media/at;->h:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/media/at;->h:I

    iget-object v0, p0, Landroid/support/v7/media/at;->a:Landroid/support/v7/media/as;

    const-string v1, "Registation failed"

    invoke-static {v0, p0, v1}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/as;Landroid/support/v7/media/at;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/x;

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v0, v2, v2}, Landroid/support/v7/media/x;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public final a(IILandroid/os/Bundle;)Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Landroid/support/v7/media/at;->g:I

    if-nez v1, :cond_0

    iget v1, p0, Landroid/support/v7/media/at;->h:I

    if-ne p1, v1, :cond_0

    if-lez p2, :cond_0

    iput v0, p0, Landroid/support/v7/media/at;->h:I

    iput p2, p0, Landroid/support/v7/media/at;->g:I

    iget-object v0, p0, Landroid/support/v7/media/at;->a:Landroid/support/v7/media/as;

    invoke-static {p3}, Landroid/support/v7/media/k;->a(Landroid/os/Bundle;)Landroid/support/v7/media/k;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/as;Landroid/support/v7/media/at;Landroid/support/v7/media/k;)V

    iget-object v0, p0, Landroid/support/v7/media/at;->a:Landroid/support/v7/media/as;

    invoke-static {v0, p0}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/as;Landroid/support/v7/media/at;)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final a(ILandroid/content/Intent;Landroid/support/v7/media/x;)Z
    .locals 6

    iget v2, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    const/16 v1, 0x9

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/os/Bundle;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/x;

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v0, p2}, Landroid/support/v7/media/x;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Landroid/os/Bundle;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/x;

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/at;->i:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v0, p2, p3}, Landroid/support/v7/media/x;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 2

    iget v0, p0, Landroid/support/v7/media/at;->g:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/at;->a:Landroid/support/v7/media/as;

    invoke-static {p1}, Landroid/support/v7/media/k;->a(Landroid/os/Bundle;)Landroid/support/v7/media/k;

    move-result-object v1

    invoke-static {v0, p0, v1}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/as;Landroid/support/v7/media/at;Landroid/support/v7/media/k;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x2

    move-object v0, p0

    move v3, v2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    iget-object v0, p0, Landroid/support/v7/media/at;->c:Landroid/support/v7/media/ay;

    invoke-virtual {v0}, Landroid/support/v7/media/ay;->a()V

    iget-object v0, p0, Landroid/support/v7/media/at;->b:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    iget-object v0, p0, Landroid/support/v7/media/at;->a:Landroid/support/v7/media/as;

    invoke-static {v0}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/as;)Landroid/support/v7/media/ax;

    move-result-object v0

    new-instance v1, Landroid/support/v7/media/au;

    invoke-direct {v1, p0}, Landroid/support/v7/media/au;-><init>(Landroid/support/v7/media/at;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/media/ax;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(I)V
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x4

    iget v2, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    return-void
.end method

.method public final b(II)V
    .locals 6

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x8

    iget v2, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    return-void
.end method

.method public final binderDied()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/at;->a:Landroid/support/v7/media/as;

    invoke-static {v0}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/as;)Landroid/support/v7/media/ax;

    move-result-object v0

    new-instance v1, Landroid/support/v7/media/av;

    invoke-direct {v1, p0}, Landroid/support/v7/media/av;-><init>(Landroid/support/v7/media/at;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/media/ax;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c(I)V
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x5

    iget v2, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    return-void
.end method

.method public final d(I)V
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x6

    iget v2, p0, Landroid/support/v7/media/at;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Landroid/support/v7/media/at;->e:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/media/at;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    return-void
.end method
