.class public final Landroid/support/v7/media/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Landroid/support/v7/media/s;


# instance fields
.field private final b:Landroid/os/Bundle;

.field private c:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/support/v7/media/s;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v7/media/s;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    sput-object v0, Landroid/support/v7/media/s;->a:Landroid/support/v7/media/s;

    return-void
.end method

.method private constructor <init>(Landroid/os/Bundle;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/media/s;->b:Landroid/os/Bundle;

    iput-object p2, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Bundle;Ljava/util/List;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/media/s;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    return-void
.end method

.method public static a(Landroid/os/Bundle;)Landroid/support/v7/media/s;
    .locals 2

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    new-instance v0, Landroid/support/v7/media/s;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/media/s;-><init>(Landroid/os/Bundle;Ljava/util/List;)V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic b(Landroid/support/v7/media/s;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/media/s;->e()V

    return-void
.end method

.method static synthetic c(Landroid/support/v7/media/s;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    return-object v0
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/s;->b:Landroid/os/Bundle;

    const-string v1, "controlCategories"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/media/s;->e()V

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    return-object v0
.end method

.method public final a(Landroid/support/v7/media/s;)Z
    .locals 2

    if-eqz p1, :cond_0

    invoke-direct {p0}, Landroid/support/v7/media/s;->e()V

    invoke-direct {p1}, Landroid/support/v7/media/s;->e()V

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    iget-object v1, p1, Landroid/support/v7/media/s;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)Z
    .locals 7

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    invoke-direct {p0}, Landroid/support/v7/media/s;->e()V

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    if-eqz v0, :cond_1

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_1

    iget-object v1, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public final b()Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/media/s;->e()V

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 2

    invoke-direct {p0}, Landroid/support/v7/media/s;->e()V

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/s;->b:Landroid/os/Bundle;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Landroid/support/v7/media/s;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/support/v7/media/s;

    invoke-direct {p0}, Landroid/support/v7/media/s;->e()V

    invoke-direct {p1}, Landroid/support/v7/media/s;->e()V

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    iget-object v1, p1, Landroid/support/v7/media/s;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/media/s;->e()V

    iget-object v0, p0, Landroid/support/v7/media/s;->c:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MediaRouteSelector{ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "controlCategories="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/media/s;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
