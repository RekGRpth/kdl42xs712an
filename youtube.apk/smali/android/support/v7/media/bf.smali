.class final Landroid/support/v7/media/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/media/al;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/support/v7/media/be;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v7/media/bf;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/bf;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/be;

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/support/v7/media/be;->c:Landroid/support/v7/media/bi;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/media/be;->c:Landroid/support/v7/media/bi;

    invoke-interface {v0, p2}, Landroid/support/v7/media/bi;->a(I)V

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/bf;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/be;

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/support/v7/media/be;->c:Landroid/support/v7/media/bi;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/media/be;->c:Landroid/support/v7/media/bi;

    invoke-interface {v0, p2}, Landroid/support/v7/media/bi;->b(I)V

    :cond_0
    return-void
.end method
