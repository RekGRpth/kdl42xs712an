.class final Landroid/support/v7/media/br;
.super Landroid/support/v7/media/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/media/bq;


# direct methods
.method constructor <init>(Landroid/support/v7/media/bq;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/media/br;->a:Landroid/support/v7/media/bq;

    invoke-direct {p0}, Landroid/support/v7/media/j;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/br;->a:Landroid/support/v7/media/bq;

    invoke-static {v0}, Landroid/support/v7/media/bq;->a(Landroid/support/v7/media/bq;)Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    iget-object v0, p0, Landroid/support/v7/media/br;->a:Landroid/support/v7/media/bq;

    invoke-static {v0}, Landroid/support/v7/media/bq;->b(Landroid/support/v7/media/bq;)V

    return-void
.end method

.method public final b(I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x3

    iget-object v0, p0, Landroid/support/v7/media/br;->a:Landroid/support/v7/media/bq;

    invoke-static {v0}, Landroid/support/v7/media/bq;->a(Landroid/support/v7/media/bq;)Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/media/br;->a:Landroid/support/v7/media/bq;

    invoke-static {v1}, Landroid/support/v7/media/bq;->a(Landroid/support/v7/media/bq;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    add-int v2, v0, p1

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/br;->a:Landroid/support/v7/media/bq;

    invoke-static {v1}, Landroid/support/v7/media/bq;->a(Landroid/support/v7/media/bq;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v3, v0, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/br;->a:Landroid/support/v7/media/bq;

    invoke-static {v0}, Landroid/support/v7/media/bq;->b(Landroid/support/v7/media/bq;)V

    return-void
.end method
