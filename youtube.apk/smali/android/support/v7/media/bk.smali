.class Landroid/support/v7/media/bk;
.super Landroid/support/v7/media/bj;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/media/af;
.implements Landroid/support/v7/media/al;


# static fields
.field private static final j:Ljava/util/ArrayList;

.field private static final k:Ljava/util/ArrayList;


# instance fields
.field protected final a:Ljava/lang/Object;

.field protected final b:Ljava/lang/Object;

.field protected final c:Ljava/lang/Object;

.field protected final d:Ljava/lang/Object;

.field protected e:I

.field protected f:Z

.field protected g:Z

.field protected final h:Ljava/util/ArrayList;

.field protected final i:Ljava/util/ArrayList;

.field private final l:Landroid/support/v7/media/bt;

.field private m:Landroid/support/v7/media/aj;

.field private n:Landroid/support/v7/media/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Landroid/support/v7/media/bk;->j:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Landroid/support/v7/media/bk;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/media/bt;)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/media/bj;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/bk;->i:Ljava/util/ArrayList;

    iput-object p2, p0, Landroid/support/v7/media/bk;->l:Landroid/support/v7/media/bt;

    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->h()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/bk;->b:Ljava/lang/Object;

    invoke-static {p0}, Landroid/support/v7/media/ae;->a(Landroid/support/v7/media/al;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/bk;->c:Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    sget v2, Landroid/support/v7/b/e;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/support/v7/media/ae;->a(Ljava/lang/Object;Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/bk;->d:Ljava/lang/Object;

    invoke-direct {p0}, Landroid/support/v7/media/bk;->j()V

    return-void
.end method

.method private a(Landroid/support/v7/media/bm;)V
    .locals 3

    new-instance v0, Landroid/support/v7/media/d;

    iget-object v1, p1, Landroid/support/v7/media/bm;->b:Ljava/lang/String;

    iget-object v2, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    invoke-direct {p0, v2}, Landroid/support/v7/media/bk;->j(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v7/media/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/media/bk;->a(Landroid/support/v7/media/bm;Landroid/support/v7/media/d;)V

    invoke-virtual {v0}, Landroid/support/v7/media/d;->a()Landroid/support/v7/media/c;

    move-result-object v0

    iput-object v0, p1, Landroid/support/v7/media/bm;->c:Landroid/support/v7/media/c;

    return-void
.end method

.method private b(Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    iget-object v0, v0, Landroid/support/v7/media/bm;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private e(Landroid/support/v7/media/ad;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/bk;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/bk;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bn;

    iget-object v0, v0, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    if-ne v0, p1, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private f(Ljava/lang/Object;)Z
    .locals 9

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/support/v7/media/bk;->i(Ljava/lang/Object;)Landroid/support/v7/media/bn;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-virtual {p0, p1}, Landroid/support/v7/media/bk;->g(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->i()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move v0, v4

    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "DEFAULT_ROUTE"

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v7/media/bk;->b(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_2

    :goto_2
    new-instance v1, Landroid/support/v7/media/bm;

    invoke-direct {v1, p1, v0}, Landroid/support/v7/media/bm;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Landroid/support/v7/media/bk;->a(Landroid/support/v7/media/bm;)V

    iget-object v0, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v4

    :goto_3
    return v0

    :cond_0
    move v0, v5

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ROUTE_%08x"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Landroid/support/v7/media/bk;->j(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_4
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s_%d"

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v0, v7, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v3, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Landroid/support/v7/media/bk;->b(Ljava/lang/String;)I

    move-result v6

    if-gez v6, :cond_3

    move-object v0, v3

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    move v0, v5

    goto :goto_3
.end method

.method private static i(Ljava/lang/Object;)Landroid/support/v7/media/bn;
    .locals 2

    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0}, Landroid/media/MediaRouter$RouteInfo;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/support/v7/media/bn;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/support/v7/media/bn;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->a()Landroid/content/Context;

    move-result-object v0

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1, v0}, Landroid/media/MediaRouter$RouteInfo;->getName(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private j()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    invoke-virtual {v0}, Landroid/media/MediaRouter;->getRouteCount()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/media/MediaRouter;->getRouteAt(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v0, v1

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/support/v7/media/bk;->f(Ljava/lang/Object;)Z

    move-result v1

    or-int/2addr v0, v1

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->f()V

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v7/media/j;
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/media/bk;->b(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    new-instance v1, Landroid/support/v7/media/bl;

    iget-object v0, v0, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/media/bl;-><init>(Landroid/support/v7/media/bk;Ljava/lang/Object;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/media/ad;)V
    .locals 3

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->p()Landroid/support/v7/media/f;

    move-result-object v0

    if-eq v0, p0, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v7/media/bk;->d:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v7/media/ae;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    new-instance v0, Landroid/support/v7/media/bn;

    invoke-direct {v0, p1, v1}, Landroid/support/v7/media/bn;-><init>(Landroid/support/v7/media/ad;Ljava/lang/Object;)V

    invoke-static {v1, v0}, Landroid/support/v7/media/ai;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v2, p0, Landroid/support/v7/media/bk;->c:Ljava/lang/Object;

    invoke-static {v1, v2}, Landroid/support/v7/media/ak;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/media/bk;->a(Landroid/support/v7/media/bn;)V

    iget-object v2, p0, Landroid/support/v7/media/bk;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->addUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    const v1, 0x800003

    invoke-static {v0, v1}, Landroid/support/v7/media/ae;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/media/bk;->g(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    iget-object v0, v0, Landroid/support/v7/media/bm;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->n()V

    goto :goto_0
.end method

.method protected a(Landroid/support/v7/media/bm;Landroid/support/v7/media/d;)V
    .locals 2

    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v0

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/support/v7/media/bk;->j:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Landroid/support/v7/media/d;->a(Ljava/util/Collection;)Landroid/support/v7/media/d;

    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    sget-object v0, Landroid/support/v7/media/bk;->k:Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->a(Ljava/util/Collection;)Landroid/support/v7/media/d;

    :cond_1
    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->a(I)Landroid/support/v7/media/d;

    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackStream()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->b(I)Landroid/support/v7/media/d;

    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->c(I)Landroid/support/v7/media/d;

    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeMax()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->d(I)Landroid/support/v7/media/d;

    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getVolumeHandling()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->e(I)Landroid/support/v7/media/d;

    return-void
.end method

.method protected a(Landroid/support/v7/media/bn;)V
    .locals 2

    iget-object v0, p1, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->a()Ljava/lang/String;

    move-result-object v1

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setName(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->h()I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->a(Ljava/lang/Object;I)V

    iget-object v0, p1, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->i()I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->b(Ljava/lang/Object;I)V

    iget-object v0, p1, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->k()I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->c(Ljava/lang/Object;I)V

    iget-object v0, p1, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->l()I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->d(Ljava/lang/Object;I)V

    iget-object v0, p1, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->j()I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->e(Ljava/lang/Object;I)V

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    const v1, 0x800003

    invoke-static {v0, v1}, Landroid/support/v7/media/ae;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/support/v7/media/bk;->i(Ljava/lang/Object;)Landroid/support/v7/media/bn;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v0, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->n()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Landroid/support/v7/media/bk;->g(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    iget-object v1, p0, Landroid/support/v7/media/bk;->l:Landroid/support/v7/media/bt;

    iget-object v0, v0, Landroid/support/v7/media/bm;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Landroid/support/v7/media/bt;->a(Ljava/lang/String;)Landroid/support/v7/media/ad;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->n()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 1

    invoke-static {p1}, Landroid/support/v7/media/bk;->i(Ljava/lang/Object;)Landroid/support/v7/media/bn;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v0, p2}, Landroid/support/v7/media/ad;->a(I)V

    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v7/media/ad;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->p()Landroid/support/v7/media/f;

    move-result-object v0

    if-eq v0, p0, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v7/media/bk;->e(Landroid/support/v7/media/ad;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bn;

    iget-object v1, v0, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    invoke-static {v1, v2}, Landroid/support/v7/media/ai;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, v0, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    invoke-static {v1, v2}, Landroid/support/v7/media/ak;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    iget-object v2, v0, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, Landroid/media/MediaRouter;

    move-object v1, v2

    check-cast v1, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeUserRoute(Landroid/media/MediaRouter$UserRouteInfo;)V

    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v7/media/e;)V
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/s;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    or-int/lit8 v0, v1, 0x1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    const-string v5, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    or-int/lit8 v0, v1, 0x2

    goto :goto_1

    :cond_1
    const/high16 v0, 0x800000

    or-int/2addr v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/media/e;->b()Z

    move-result v0

    :goto_2
    iget v2, p0, Landroid/support/v7/media/bk;->e:I

    if-ne v2, v1, :cond_3

    iget-boolean v2, p0, Landroid/support/v7/media/bk;->f:Z

    if-eq v2, v0, :cond_4

    :cond_3
    iput v1, p0, Landroid/support/v7/media/bk;->e:I

    iput-boolean v0, p0, Landroid/support/v7/media/bk;->f:Z

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->g()V

    invoke-direct {p0}, Landroid/support/v7/media/bk;->j()V

    :cond_4
    return-void

    :cond_5
    move v1, v0

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/media/bk;->f(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->f()V

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/Object;I)V
    .locals 1

    invoke-static {p1}, Landroid/support/v7/media/bk;->i(Ljava/lang/Object;)Landroid/support/v7/media/bn;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v0, p2}, Landroid/support/v7/media/ad;->b(I)V

    :cond_0
    return-void
.end method

.method public final c(Landroid/support/v7/media/ad;)V
    .locals 2

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->p()Landroid/support/v7/media/f;

    move-result-object v0

    if-eq v0, p0, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v7/media/bk;->e(Landroid/support/v7/media/ad;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bn;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/bk;->a(Landroid/support/v7/media/bn;)V

    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2

    invoke-static {p1}, Landroid/support/v7/media/bk;->i(Ljava/lang/Object;)Landroid/support/v7/media/bn;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v7/media/bk;->g(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->f()V

    :cond_0
    return-void
.end method

.method public final d(Landroid/support/v7/media/ad;)V
    .locals 2

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/media/ad;->p()Landroid/support/v7/media/f;

    move-result-object v0

    if-eq v0, p0, :cond_2

    invoke-direct {p0, p1}, Landroid/support/v7/media/bk;->e(Landroid/support/v7/media/ad;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bn;

    iget-object v0, v0, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/bk;->h(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/media/ad;->o()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/media/bk;->b(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    iget-object v0, v0, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/bk;->h(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 2

    invoke-static {p1}, Landroid/support/v7/media/bk;->i(Ljava/lang/Object;)Landroid/support/v7/media/bn;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v7/media/bk;->g(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    invoke-direct {p0, v0}, Landroid/support/v7/media/bk;->a(Landroid/support/v7/media/bm;)V

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->f()V

    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/Object;)V
    .locals 4

    invoke-static {p1}, Landroid/support/v7/media/bk;->i(Ljava/lang/Object;)Landroid/support/v7/media/bn;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v7/media/bk;->g(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getVolume()I

    move-result v1

    iget-object v2, v0, Landroid/support/v7/media/bm;->c:Landroid/support/v7/media/c;

    invoke-virtual {v2}, Landroid/support/v7/media/c;->i()I

    move-result v2

    if-eq v1, v2, :cond_0

    new-instance v2, Landroid/support/v7/media/d;

    iget-object v3, v0, Landroid/support/v7/media/bm;->c:Landroid/support/v7/media/c;

    invoke-direct {v2, v3}, Landroid/support/v7/media/d;-><init>(Landroid/support/v7/media/c;)V

    invoke-virtual {v2, v1}, Landroid/support/v7/media/d;->c(I)Landroid/support/v7/media/d;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/d;->a()Landroid/support/v7/media/c;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/media/bm;->c:Landroid/support/v7/media/c;

    invoke-virtual {p0}, Landroid/support/v7/media/bk;->f()V

    :cond_0
    return-void
.end method

.method protected final f()V
    .locals 4

    new-instance v2, Landroid/support/v7/media/l;

    invoke-direct {v2}, Landroid/support/v7/media/l;-><init>()V

    iget-object v0, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    iget-object v0, v0, Landroid/support/v7/media/bm;->c:Landroid/support/v7/media/c;

    invoke-virtual {v2, v0}, Landroid/support/v7/media/l;->a(Landroid/support/v7/media/c;)Landroid/support/v7/media/l;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Landroid/support/v7/media/l;->a()Landroid/support/v7/media/k;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/media/bk;->a(Landroid/support/v7/media/k;)V

    return-void
.end method

.method protected final g(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/bk;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    iget-object v0, v0, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected g()V
    .locals 3

    iget-boolean v0, p0, Landroid/support/v7/media/bk;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/media/bk;->g:Z

    iget-object v0, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v7/media/bk;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v7/media/ae;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    iget v0, p0, Landroid/support/v7/media/bk;->e:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/media/bk;->g:Z

    iget-object v0, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    iget v2, p0, Landroid/support/v7/media/bk;->e:I

    iget-object v1, p0, Landroid/support/v7/media/bk;->b:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$Callback;

    invoke-virtual {v0, v2, v1}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    :cond_1
    return-void
.end method

.method protected h()Ljava/lang/Object;
    .locals 1

    new-instance v0, Landroid/support/v7/media/ag;

    invoke-direct {v0, p0}, Landroid/support/v7/media/ag;-><init>(Landroid/support/v7/media/af;)V

    return-object v0
.end method

.method protected h(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/bk;->m:Landroid/support/v7/media/aj;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/media/aj;

    invoke-direct {v0}, Landroid/support/v7/media/aj;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/bk;->m:Landroid/support/v7/media/aj;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/bk;->m:Landroid/support/v7/media/aj;

    iget-object v1, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    const v2, 0x800003

    invoke-virtual {v0, v1, v2, p1}, Landroid/support/v7/media/aj;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method

.method protected i()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/bk;->n:Landroid/support/v7/media/ah;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/media/ah;

    invoke-direct {v0}, Landroid/support/v7/media/ah;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/bk;->n:Landroid/support/v7/media/ah;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/bk;->n:Landroid/support/v7/media/ah;

    iget-object v1, p0, Landroid/support/v7/media/bk;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
