.class Lcom/kehdev/SaveGameTouchEvent$1;
.super Ljava/lang/Object;
.source "SaveGameTouchEvent.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/SaveGameTouchEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/SaveGameTouchEvent;


# direct methods
.method constructor <init>(Lcom/kehdev/SaveGameTouchEvent;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    check-cast p2, Lcom/kehdev/GetRunningAppName$LocalBinder;

    invoke-virtual {p2}, Lcom/kehdev/GetRunningAppName$LocalBinder;->getService()Lcom/kehdev/GetRunningAppName;

    move-result-object v1

    # setter for: Lcom/kehdev/SaveGameTouchEvent;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0, v1}, Lcom/kehdev/SaveGameTouchEvent;->access$002(Lcom/kehdev/SaveGameTouchEvent;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0}, Lcom/kehdev/SaveGameTouchEvent;->access$000(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/GetRunningAppName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v1}, Lcom/kehdev/SaveGameTouchEvent;->access$000(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/GetRunningAppName;

    move-result-object v1

    iget-object v1, v1, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    # setter for: Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0, v1}, Lcom/kehdev/SaveGameTouchEvent;->access$102(Lcom/kehdev/SaveGameTouchEvent;Lcom/kehdev/SqliteOperation;)Lcom/kehdev/SqliteOperation;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/SaveGameTouchEvent;->access$100(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v0}, Lcom/kehdev/SaveGameTouchEvent;->checkAllMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v0}, Lcom/kehdev/SaveGameTouchEvent;->setMessageInCorr()Z

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$1;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    const/4 v1, 0x0

    # setter for: Lcom/kehdev/SaveGameTouchEvent;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0, v1}, Lcom/kehdev/SaveGameTouchEvent;->access$002(Lcom/kehdev/SaveGameTouchEvent;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;

    return-void
.end method
