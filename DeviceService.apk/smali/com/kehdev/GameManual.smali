.class public Lcom/kehdev/GameManual;
.super Landroid/app/Activity;
.source "GameManual.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private axisListItemAdapter:Landroid/widget/SimpleAdapter;

.field private axisSlt:I

.field private bindSucc:Z

.field private bindSuccRet:Z

.field private completePkgName:Ljava/lang/String;

.field private descripton:Ljava/lang/String;

.field private gameId:I

.field private getSltAppBitmap:Landroid/graphics/Bitmap;

.field private getSltLableName:Ljava/lang/String;

.field private intentGet:Landroid/content/Intent;

.field private mRunningAppName:Lcom/kehdev/GetRunningAppName;

.field private mRunningAppNameConn:Landroid/content/ServiceConnection;

.field private mSqliteOperation:Lcom/kehdev/SqliteOperation;

.field private remoteAxisListView:Landroid/widget/ListView;

.field private saveSltItem:I

.field private settingButton:Landroid/widget/Button;

.field private sltAppLabel:Landroid/widget/ImageView;

.field private sltAppName:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "GameManual"

    iput-object v0, p0, Lcom/kehdev/GameManual;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/kehdev/GameManual;->intentGet:Landroid/content/Intent;

    iput-object v1, p0, Lcom/kehdev/GameManual;->sltAppLabel:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/kehdev/GameManual;->sltAppName:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/kehdev/GameManual;->remoteAxisListView:Landroid/widget/ListView;

    iput-object v1, p0, Lcom/kehdev/GameManual;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    iput-object v1, p0, Lcom/kehdev/GameManual;->axisListItemAdapter:Landroid/widget/SimpleAdapter;

    iput v2, p0, Lcom/kehdev/GameManual;->gameId:I

    iput-object v1, p0, Lcom/kehdev/GameManual;->completePkgName:Ljava/lang/String;

    iput v2, p0, Lcom/kehdev/GameManual;->axisSlt:I

    iput-object v1, p0, Lcom/kehdev/GameManual;->getSltLableName:Ljava/lang/String;

    iput-object v1, p0, Lcom/kehdev/GameManual;->getSltAppBitmap:Landroid/graphics/Bitmap;

    const/4 v0, -0x1

    iput v0, p0, Lcom/kehdev/GameManual;->saveSltItem:I

    iput-object v1, p0, Lcom/kehdev/GameManual;->descripton:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/kehdev/GameManual;->bindSucc:Z

    iput-boolean v2, p0, Lcom/kehdev/GameManual;->bindSuccRet:Z

    iput-object v1, p0, Lcom/kehdev/GameManual;->settingButton:Landroid/widget/Button;

    new-instance v0, Lcom/kehdev/GameManual$1;

    invoke-direct {v0, p0}, Lcom/kehdev/GameManual$1;-><init>(Lcom/kehdev/GameManual;)V

    iput-object v0, p0, Lcom/kehdev/GameManual;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/kehdev/GameManual;)Lcom/kehdev/GetRunningAppName;
    .locals 1
    .param p0    # Lcom/kehdev/GameManual;

    iget-object v0, p0, Lcom/kehdev/GameManual;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object v0
.end method

.method static synthetic access$002(Lcom/kehdev/GameManual;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;
    .locals 0
    .param p0    # Lcom/kehdev/GameManual;
    .param p1    # Lcom/kehdev/GetRunningAppName;

    iput-object p1, p0, Lcom/kehdev/GameManual;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object p1
.end method

.method static synthetic access$100(Lcom/kehdev/GameManual;)Lcom/kehdev/SqliteOperation;
    .locals 1
    .param p0    # Lcom/kehdev/GameManual;

    iget-object v0, p0, Lcom/kehdev/GameManual;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object v0
.end method

.method static synthetic access$102(Lcom/kehdev/GameManual;Lcom/kehdev/SqliteOperation;)Lcom/kehdev/SqliteOperation;
    .locals 0
    .param p0    # Lcom/kehdev/GameManual;
    .param p1    # Lcom/kehdev/SqliteOperation;

    iput-object p1, p0, Lcom/kehdev/GameManual;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object p1
.end method

.method static synthetic access$202(Lcom/kehdev/GameManual;Z)Z
    .locals 0
    .param p0    # Lcom/kehdev/GameManual;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/kehdev/GameManual;->bindSucc:Z

    return p1
.end method

.method static synthetic access$300(Lcom/kehdev/GameManual;)I
    .locals 1
    .param p0    # Lcom/kehdev/GameManual;

    iget v0, p0, Lcom/kehdev/GameManual;->gameId:I

    return v0
.end method

.method static synthetic access$302(Lcom/kehdev/GameManual;I)I
    .locals 0
    .param p0    # Lcom/kehdev/GameManual;
    .param p1    # I

    iput p1, p0, Lcom/kehdev/GameManual;->gameId:I

    return p1
.end method

.method static synthetic access$400(Lcom/kehdev/GameManual;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/kehdev/GameManual;

    iget-object v0, p0, Lcom/kehdev/GameManual;->completePkgName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/kehdev/GameManual;I)I
    .locals 0
    .param p0    # Lcom/kehdev/GameManual;
    .param p1    # I

    iput p1, p0, Lcom/kehdev/GameManual;->saveSltItem:I

    return p1
.end method


# virtual methods
.method public checkOutAllMessage()Z
    .locals 6

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/kehdev/GameManual;->axisListItemAdapter:Landroid/widget/SimpleAdapter;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/kehdev/GameManual;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iget-object v5, p0, Lcom/kehdev/GameManual;->completePkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/kehdev/SqliteOperation;->findGameAxis(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/kehdev/GameManual;->axisSlt:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "axis"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/kehdev/GameManual;->axisSlt:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/kehdev/GameManual;->getIdFromDrawableString(Ljava/lang/String;)I

    move-result v1

    const/4 v4, -0x1

    if-eq v4, v1, :cond_1

    iget-object v4, p0, Lcom/kehdev/GameManual;->axisListItemAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v4, v3}, Landroid/widget/SimpleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v3, "ItemImage"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/4 v3, 0x1

    :cond_1
    return v3
.end method

.method public getIdFromDrawableString(Ljava/lang/String;)I
    .locals 3

    const/4 v0, -0x1

    :try_start_0
    const-class v1, Lcom/kehdev/R$drawable;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    new-instance v2, Lcom/kehdev/R$drawable;

    invoke-direct {v2}, Lcom/kehdev/R$drawable;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v6, -0x1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v3, 0x0

    if-nez p1, :cond_0

    if-ne p2, v6, :cond_0

    iget v4, p0, Lcom/kehdev/GameManual;->saveSltItem:I

    if-nez v4, :cond_1

    const-string v4, "sltAxis"

    iget v5, p0, Lcom/kehdev/GameManual;->axisSlt:I

    invoke-virtual {p3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/kehdev/GameManual;->axisSlt:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "axis"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/kehdev/GameManual;->axisSlt:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/kehdev/GameManual;->getIdFromDrawableString(Ljava/lang/String;)I

    move-result v1

    if-eq v6, v1, :cond_0

    iget-object v4, p0, Lcom/kehdev/GameManual;->axisListItemAdapter:Landroid/widget/SimpleAdapter;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/SimpleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    const-string v4, "ItemImage"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/kehdev/GameManual;->axisListItemAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v4}, Landroid/widget/SimpleAdapter;->notifyDataSetChanged()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v4, p0, Lcom/kehdev/GameManual;->saveSltItem:I

    if-lez v4, :cond_0

    const-string v4, "sltKeyMessage"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/kehdev/GameManual;->descripton:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v5}, Lcom/kehdev/GameManual;->requestWindowFeature(I)Z

    const v0, 0x7f030008    # com.kehdev.R.layout.game_manual_layout

    invoke-virtual {p0, v0}, Lcom/kehdev/GameManual;->setContentView(I)V

    const v0, 0x7f090016    # com.kehdev.R.id.remoteAxisListView

    invoke-virtual {p0, v0}, Lcom/kehdev/GameManual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kehdev/GameManual;->remoteAxisListView:Landroid/widget/ListView;

    const v0, 0x7f090018    # com.kehdev.R.id.button1

    invoke-virtual {p0, v0}, Lcom/kehdev/GameManual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/kehdev/GameManual;->settingButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/kehdev/GameManual;->settingButton:Landroid/widget/Button;

    const-string v1, "setting"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/kehdev/GameManual;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/GameManual;->intentGet:Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/GameManual;->intentGet:Landroid/content/Intent;

    const-string v1, "appIconBitmap"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/kehdev/GameManual;->getSltAppBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/kehdev/GameManual;->intentGet:Landroid/content/Intent;

    const-string v1, "appSltLable"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/GameManual;->getSltLableName:Ljava/lang/String;

    iget-object v0, p0, Lcom/kehdev/GameManual;->intentGet:Landroid/content/Intent;

    const-string v1, "completePkgName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/GameManual;->completePkgName:Ljava/lang/String;

    const v0, 0x7f090012    # com.kehdev.R.id.sltAppLabel

    invoke-virtual {p0, v0}, Lcom/kehdev/GameManual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/kehdev/GameManual;->sltAppLabel:Landroid/widget/ImageView;

    const v0, 0x7f090013    # com.kehdev.R.id.sltAppName

    invoke-virtual {p0, v0}, Lcom/kehdev/GameManual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/kehdev/GameManual;->sltAppName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/kehdev/GameManual;->sltAppLabel:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/kehdev/GameManual;->getSltAppBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/kehdev/GameManual;->sltAppName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/kehdev/GameManual;->getSltLableName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kehdev/GameManual;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kehdev/GetRunningAppName;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/kehdev/GameManual;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v5}, Lcom/kehdev/GameManual;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/kehdev/GameManual;->bindSuccRet:Z

    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-virtual {p0}, Lcom/kehdev/GameManual;->setAxisMessListViewItem()Ljava/util/ArrayList;

    move-result-object v2

    const v3, 0x7f030006    # com.kehdev.R.layout.game_manual_axis_listview_layout

    new-array v4, v5, [Ljava/lang/String;

    const-string v1, "ItemImage"

    aput-object v1, v4, v6

    new-array v5, v5, [I

    const v1, 0x7f09000e    # com.kehdev.R.id.ItemImage

    aput v1, v5, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/kehdev/GameManual;->axisListItemAdapter:Landroid/widget/SimpleAdapter;

    iget-object v0, p0, Lcom/kehdev/GameManual;->remoteAxisListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/kehdev/GameManual;->axisListItemAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/kehdev/GameManual;->remoteAxisListView:Landroid/widget/ListView;

    new-instance v1, Lcom/kehdev/GameManual$2;

    invoke-direct {v1, p0}, Lcom/kehdev/GameManual$2;-><init>(Lcom/kehdev/GameManual;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/kehdev/GameManual;->settingButton:Landroid/widget/Button;

    new-instance v1, Lcom/kehdev/GameManual$3;

    invoke-direct {v1, p0}, Lcom/kehdev/GameManual$3;-><init>(Lcom/kehdev/GameManual;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-boolean v0, p0, Lcom/kehdev/GameManual;->bindSucc:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/GameManual;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/GameManual;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/kehdev/GameManual;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public setAxisMessListViewItem()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "ItemImage"

    const v3, 0x7f020001    # com.kehdev.R.drawable.axis1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
