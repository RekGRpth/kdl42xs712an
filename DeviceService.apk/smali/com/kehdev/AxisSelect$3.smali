.class Lcom/kehdev/AxisSelect$3;
.super Ljava/lang/Object;
.source "AxisSelect.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kehdev/AxisSelect;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/AxisSelect;


# direct methods
.method constructor <init>(Lcom/kehdev/AxisSelect;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/AxisSelect$3;->this$0:Lcom/kehdev/AxisSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/kehdev/AxisSelect$3;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->status1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$700(Lcom/kehdev/AxisSelect;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$3;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->status2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$600(Lcom/kehdev/AxisSelect;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$3;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->rela2:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$900(Lcom/kehdev/AxisSelect;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const v1, 0x7f020036    # com.kehdev.R.drawable.trans_background

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$3;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->rela1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$800(Lcom/kehdev/AxisSelect;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$3;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$100(Lcom/kehdev/AxisSelect;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/AxisSelect$3;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->completePkgName:Ljava/lang/String;
    invoke-static {v1}, Lcom/kehdev/AxisSelect;->access$400(Lcom/kehdev/AxisSelect;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/kehdev/SqliteOperation;->saveAxisToDatabase(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_0
    iget-object v0, p0, Lcom/kehdev/AxisSelect$3;->this$0:Lcom/kehdev/AxisSelect;

    invoke-virtual {v0}, Lcom/kehdev/AxisSelect;->finish()V

    return-void
.end method
