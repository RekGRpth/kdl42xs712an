.class public Lcom/kehdev/DownloadVoiceControlApp;
.super Landroid/app/Activity;
.source "DownloadVoiceControlApp.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private downLoadApp:Landroid/widget/ImageButton;

.field private final googleVoiceDownloadAddress:Ljava/lang/String;

.field private final iflyYudianDownloadAddress:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "DownloadVoiceControlApp"

    iput-object v0, p0, Lcom/kehdev/DownloadVoiceControlApp;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kehdev/DownloadVoiceControlApp;->downLoadApp:Landroid/widget/ImageButton;

    const-string v0, "http://yudian.voicecloud.cn/yudian.htm"

    iput-object v0, p0, Lcom/kehdev/DownloadVoiceControlApp;->iflyYudianDownloadAddress:Ljava/lang/String;

    const-string v0, "http://down.tech.sina.com.cn/3gsoft/download.php?id=4815"

    iput-object v0, p0, Lcom/kehdev/DownloadVoiceControlApp;->googleVoiceDownloadAddress:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/kehdev/DownloadVoiceControlApp;->requestWindowFeature(I)Z

    const v0, 0x7f030005    # com.kehdev.R.layout.download_voice_control_app_layout

    invoke-virtual {p0, v0}, Lcom/kehdev/DownloadVoiceControlApp;->setContentView(I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f09000d    # com.kehdev.R.id.voice_control_download_butt

    invoke-virtual {p0, v0}, Lcom/kehdev/DownloadVoiceControlApp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/kehdev/DownloadVoiceControlApp;->downLoadApp:Landroid/widget/ImageButton;

    sget v0, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/kehdev/DownloadVoiceControlApp;->downLoadApp:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/kehdev/DownloadVoiceControlApp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000e    # com.kehdev.R.drawable.google_voice_download

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iget-object v0, p0, Lcom/kehdev/DownloadVoiceControlApp;->downLoadApp:Landroid/widget/ImageButton;

    new-instance v1, Lcom/kehdev/DownloadVoiceControlApp$1;

    invoke-direct {v1, p0}, Lcom/kehdev/DownloadVoiceControlApp$1;-><init>(Lcom/kehdev/DownloadVoiceControlApp;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/kehdev/DownloadVoiceControlApp;->downLoadApp:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/kehdev/DownloadVoiceControlApp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020015    # com.kehdev.R.drawable.iflytek_download

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public openWebSite(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/kehdev/DownloadVoiceControlApp;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
