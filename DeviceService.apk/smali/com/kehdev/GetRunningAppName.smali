.class public Lcom/kehdev/GetRunningAppName;
.super Landroid/app/Service;
.source "GetRunningAppName.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;,
        Lcom/kehdev/GetRunningAppName$LocalBinder;
    }
.end annotation


# static fields
.field public static TypeSlt:I

.field public static g_flg:I

.field public static g_preAppName:Ljava/lang/String;

.field public static g_runningAppName:Ljava/lang/String;

.field public static runningAppPkgName:Ljava/lang/String;


# instance fields
.field public appType:I

.field public iLastTypeSlt:I

.field private final mBinder:Landroid/os/IBinder;

.field public mSqliteOperation:Lcom/kehdev/SqliteOperation;

.field public stringTest:Ljava/lang/String;

.field private final tag:Ljava/lang/String;

.field public testType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    sput v1, Lcom/kehdev/GetRunningAppName;->TypeSlt:I

    const-string v0, ""

    sput-object v0, Lcom/kehdev/GetRunningAppName;->g_runningAppName:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/kehdev/GetRunningAppName;->g_preAppName:Ljava/lang/String;

    sput v1, Lcom/kehdev/GetRunningAppName;->g_flg:I

    const-string v0, ""

    sput-object v0, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-string v0, "getRunningAppName"

    iput-object v0, p0, Lcom/kehdev/GetRunningAppName;->tag:Ljava/lang/String;

    new-instance v0, Lcom/kehdev/GetRunningAppName$LocalBinder;

    invoke-direct {v0, p0}, Lcom/kehdev/GetRunningAppName$LocalBinder;-><init>(Lcom/kehdev/GetRunningAppName;)V

    iput-object v0, p0, Lcom/kehdev/GetRunningAppName;->mBinder:Landroid/os/IBinder;

    const/4 v0, 0x1

    iput v0, p0, Lcom/kehdev/GetRunningAppName;->testType:I

    iput v1, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    iput v1, p0, Lcom/kehdev/GetRunningAppName;->iLastTypeSlt:I

    const-string v0, "com.realarcade.DOJ/com.realarcade.DOJ.MrGame"

    iput-object v0, p0, Lcom/kehdev/GetRunningAppName;->stringTest:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-void
.end method

.method static synthetic access$000(Lcom/kehdev/GetRunningAppName;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/kehdev/GetRunningAppName;

    invoke-direct {p0}, Lcom/kehdev/GetRunningAppName;->getCurrentApp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/kehdev/GetRunningAppName;)Z
    .locals 1
    .param p0    # Lcom/kehdev/GetRunningAppName;

    invoke-direct {p0}, Lcom/kehdev/GetRunningAppName;->checkRunningAppChange()Z

    move-result v0

    return v0
.end method

.method private checkRunningAppChange()Z
    .locals 2

    sget-object v0, Lcom/kehdev/GetRunningAppName;->g_runningAppName:Ljava/lang/String;

    sget-object v1, Lcom/kehdev/GetRunningAppName;->g_preAppName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getCurrentApp()Ljava/lang/String;
    .locals 6

    const/4 v2, 0x1

    const-string v4, "activity"

    invoke-virtual {p0, v4}, Lcom/kehdev/GetRunningAppName;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/kehdev/GetRunningAppName;->g_runningAppName:Ljava/lang/String;

    return-object v3
.end method


# virtual methods
.method public checkRunningApp()Z
    .locals 6

    const/4 v1, 0x0

    const/4 v5, -0x2

    const/4 v2, 0x1

    const/4 v0, -0x1

    sget-object v3, Lcom/kehdev/GetRunningAppName;->g_runningAppName:Ljava/lang/String;

    const-string v4, "{com.kehdev/com.kehdev.DeviceService}"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget v3, Lcom/kehdev/GetRunningAppName;->TypeSlt:I

    iput v3, p0, Lcom/kehdev/GetRunningAppName;->iLastTypeSlt:I

    sget-object v3, Lcom/kehdev/GetRunningAppName;->g_runningAppName:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/kehdev/GetRunningAppName;->getAppPkgFromBaseActi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    sget-object v3, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_2
    sget-object v3, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v4, "com.sinyee.babybus.kartRacing"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iput v5, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    sget-object v4, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/kehdev/SqliteOperation;->queryDatabaseGetGameAxis(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    sget-object v3, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v4, "com.bengigi.noogranuts"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    sget-object v3, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v4, "com.polarbit.rthunder2play"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_4
    iget v1, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    if-eqz v1, :cond_5

    const/4 v1, -0x3

    iput v1, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    move v1, v2

    goto :goto_0

    :cond_5
    iput v5, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    move v1, v2

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getAppPkgFromBaseActi(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v0, ""

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "{"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    return-object v0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/GetRunningAppName;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f020011    # com.kehdev.R.drawable.icon

    const-string v2, "F-dolphin Service"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/kehdev/DeviceService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "F-dolphin Service"

    const-string v3, ""

    invoke-virtual {v0, p0, v2, v3, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    const/16 v1, 0x4d3

    invoke-virtual {p0, v1, v0}, Lcom/kehdev/GetRunningAppName;->startForeground(ILandroid/app/Notification;)V

    new-instance v0, Lcom/kehdev/SqliteOperation;

    invoke-direct {v0, p0}, Lcom/kehdev/SqliteOperation;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    new-instance v0, Ljava/lang/Thread;

    const/4 v1, 0x0

    new-instance v2, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;

    invoke-direct {v2, p0}, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;-><init>(Lcom/kehdev/GetRunningAppName;)V

    const-string v3, "getRunningAppName"

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iget-object v0, v0, Lcom/kehdev/SqliteOperation;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iget-object v0, v0, Lcom/kehdev/SqliteOperation;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iget-object v0, v0, Lcom/kehdev/SqliteOperation;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/kehdev/GetRunningAppName;->stopForeground(Z)V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    return-void
.end method

.method public setAppAsix()I
    .locals 3

    iget v1, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/kehdev/GetRunningAppName;->appType:I

    sget-object v2, Lcom/kehdev/GetRunningAppName;->g_runningAppName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/kehdev/DeviceService;->_runningApp(ILjava/lang/String;)I

    move-result v1

    return v1
.end method
