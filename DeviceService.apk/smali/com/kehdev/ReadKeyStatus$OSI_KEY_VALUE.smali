.class public final enum Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;
.super Ljava/lang/Enum;
.source "ReadKeyStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/ReadKeyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OSI_KEY_VALUE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_A_CSENSOR_BEHIND:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_A_SENSOR_FRONT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_A_SENSOR_LEFT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_A_SENSOR_RESERVE:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_A_SENSOR_RIGHT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_SCROLL_IN:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_SCROLL_OUT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_DEV:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_DOWN:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_ENTER:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_FUC:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_HOME:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_LASER:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_LEFT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_ML:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_MR:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_NEXT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_PP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_PREV:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_RESERVE:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_RIGHT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_SP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_SPWD:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_STOP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_TASK:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_UP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

.field public static final enum KEY_VALUE_VOL:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_UP"

    invoke-direct {v0, v1, v3}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_UP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_DOWN"

    invoke-direct {v0, v1, v4}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_DOWN:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_RIGHT"

    invoke-direct {v0, v1, v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_RIGHT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_LEFT"

    invoke-direct {v0, v1, v6}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_LEFT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_HOME"

    invoke-direct {v0, v1, v7}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_HOME:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_NEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_NEXT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_PREV"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_PREV:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_STOP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_STOP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_VOL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_VOL:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_PP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_PP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_MR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_MR:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_TASK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_TASK:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_ENTER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_ENTER:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_SP"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_SP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_ML"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_ML:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_FUC"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_FUC:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_SPWD"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_SPWD:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_DEV"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_DEV:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_LASER"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_LASER:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_VALUE_RESERVE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_RESERVE:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_A_SENSOR_FRONT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_SENSOR_FRONT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_A_CSENSOR_BEHIND"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_CSENSOR_BEHIND:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_A_SENSOR_LEFT"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_SENSOR_LEFT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_A_SENSOR_RIGHT"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_SENSOR_RIGHT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_A_SENSOR_RESERVE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_SENSOR_RESERVE:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_SCROLL_IN"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_SCROLL_IN:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const-string v1, "KEY_SCROLL_OUT"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_SCROLL_OUT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    sget-object v1, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_UP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_DOWN:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_RIGHT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_LEFT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_HOME:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_NEXT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_PREV:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_STOP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_VOL:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_PP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_MR:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_TASK:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_ENTER:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_SP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_ML:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_FUC:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_SPWD:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_DEV:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_LASER:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_RESERVE:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_SENSOR_FRONT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_CSENSOR_BEHIND:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_SENSOR_LEFT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_SENSOR_RIGHT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_A_SENSOR_RESERVE:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_SCROLL_IN:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_SCROLL_OUT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->$VALUES:[Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;
    .locals 1

    const-class v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    return-object v0
.end method

.method public static values()[Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;
    .locals 1

    sget-object v0, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->$VALUES:[Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v0}, [Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    return-object v0
.end method
