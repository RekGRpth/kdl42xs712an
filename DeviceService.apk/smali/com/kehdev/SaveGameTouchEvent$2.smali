.class Lcom/kehdev/SaveGameTouchEvent$2;
.super Ljava/lang/Object;
.source "SaveGameTouchEvent.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kehdev/SaveGameTouchEvent;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/SaveGameTouchEvent;


# direct methods
.method constructor <init>(Lcom/kehdev/SaveGameTouchEvent;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/SaveGameTouchEvent$2;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$2;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # setter for: Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I
    invoke-static {v0, p3}, Lcom/kehdev/SaveGameTouchEvent;->access$202(Lcom/kehdev/SaveGameTouchEvent;I)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    packed-switch p3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$2;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->p1_start_active:Z
    invoke-static {v1}, Lcom/kehdev/SaveGameTouchEvent;->access$300(Lcom/kehdev/SaveGameTouchEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$2;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    const-class v2, Lcom/kehdev/SetTouchCoorMess;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$2;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v1, v0, v3}, Lcom/kehdev/SaveGameTouchEvent;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$2;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    const-class v2, Lcom/kehdev/SetKeyMessage;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$2;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v1, v0, v3}, Lcom/kehdev/SaveGameTouchEvent;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
