.class Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;
.super Ljava/lang/Object;
.source "ReadKeyStatus.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/ReadKeyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "monitorKeyReleaseWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/ReadKeyStatus;


# direct methods
.method constructor <init>(Lcom/kehdev/ReadKeyStatus;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const-wide/16 v2, 0x12c

    :goto_0
    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-boolean v0, v0, Lcom/kehdev/ReadKeyStatus;->hadOneKeyDown:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    # getter for: Lcom/kehdev/ReadKeyStatus;->getKeyMessInTime:Z
    invoke-static {v0}, Lcom/kehdev/ReadKeyStatus;->access$000(Lcom/kehdev/ReadKeyStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    const/4 v1, 0x0

    # setter for: Lcom/kehdev/ReadKeyStatus;->getKeyMessInTime:Z
    invoke-static {v0, v1}, Lcom/kehdev/ReadKeyStatus;->access$002(Lcom/kehdev/ReadKeyStatus;Z)Z

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v0}, Lcom/kehdev/ReadKeyStatus;->releaseTouch()Z

    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v0}, Lcom/kehdev/ReadKeyStatus;->initTouchKey()V

    goto :goto_0

    :cond_1
    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0
.end method
