.class public Lcom/kehdev/DeviceService;
.super Landroid/app/Activity;
.source "DeviceService.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kehdev/DeviceService$GridItemAdapter;,
        Lcom/kehdev/DeviceService$ThreadGetRemoteId;,
        Lcom/kehdev/DeviceService$mServiceUIReceiver;
    }
.end annotation


# static fields
.field public static dongleVerStr:Ljava/lang/String;

.field public static driverVersion:Ljava/lang/String;

.field public static emu_key_test:I

.field public static emu_key_test1:I

.field public static heightPx:I

.field public static mPackageManager:Landroid/content/pm/PackageManager;

.field public static noSupportAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static processint:I

.field public static remoteVerStr:Ljava/lang/String;

.field public static width:I


# instance fields
.field private IDText:[Landroid/widget/TextView;

.field private final LogTAG:Ljava/lang/String;

.field private final MOUSERESOLUTION:I

.field private MatchCodeStatus:I

.field public RemoteId:[I

.field public RemoteIsOnline:[Z

.field public SDK_version:Ljava/lang/String;

.field private final SIZEVERSION:I

.field private final TAG:Ljava/lang/String;

.field private TypeText:[Landroid/widget/ImageView;

.field private addGameButt:Landroid/widget/Button;

.field private addNotInstallGameButt:Landroid/widget/Button;

.field private airmouseVersion:Landroid/widget/TextView;

.field private closeMatchCode:Landroid/widget/ImageButton;

.field private databaseVersion:Landroid/widget/TextView;

.field private dongleVersion:Landroid/widget/TextView;

.field private downfail:Ljava/lang/String;

.field private drawBitTmp:Landroid/graphics/drawable/BitmapDrawable;

.field private drawTmp:Landroid/graphics/drawable/Drawable;

.field public googlevoiceSearchButton:Landroid/widget/ImageButton;

.field private lastGameNameText:Landroid/widget/TextView;

.field private listItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private listMain:Landroid/widget/ListView;

.field private mContext:Landroid/content/Context;

.field private mGridView:Landroid/widget/GridView;

.field private mHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyStatus:Lcom/kehdev/ReadKeyStatus;

.field private mKeyStatusConn:Landroid/content/ServiceConnection;

.field private mMsgReciver:Landroid/os/Handler;

.field private mReceiver:Lcom/kehdev/DeviceService$mServiceUIReceiver;

.field private mResolveInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRunningAppName:Lcom/kehdev/GetRunningAppName;

.field private mRunningAppNameConn:Landroid/content/ServiceConnection;

.field private mSaveAppButton:Landroid/widget/Button;

.field private mSqliteOperation:Lcom/kehdev/SqliteOperation;

.field private mbitmap:Landroid/graphics/Bitmap;

.field private mfailedno:I

.field private newVerCode:I

.field private newVerName:Ljava/lang/String;

.field private openMatchCode:Landroid/widget/ImageButton;

.field public pBar:Landroid/app/ProgressDialog;

.field private remStateBeforeSystemVoice:I

.field private remotethread:Ljava/lang/Thread;

.field public resolutionSeekBar:Landroid/widget/SeekBar;

.field public resolutionValue:I

.field public resolutionValueTextV:Landroid/widget/TextView;

.field private softwareVersion:Landroid/widget/TextView;

.field public startStatus:I

.field public successStatus:I

.field supportGameStringList:Ljava/util/ArrayList;

.field private systemVoiceControl:Landroid/widget/RadioButton;

.field private final tag:Ljava/lang/String;

.field testName:Ljava/lang/String;

.field private updateButton:Landroid/widget/Button;

.field private updateType:I

.field private updateendflag:I

.field private userText:[Landroid/widget/TextView;

.field private userVoiceControl:Landroid/widget/RadioButton;

.field public versionPkg:[I

.field public versonName:Ljava/lang/String;

.field private viewFlip:Landroid/widget/ViewFlipper;

.field private voiceControlRadio:Landroid/widget/RadioGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const-string v0, "kehdevRuntime"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const/4 v0, 0x1

    sput v0, Lcom/kehdev/DeviceService;->emu_key_test:I

    sput v1, Lcom/kehdev/DeviceService;->emu_key_test1:I

    const/16 v0, 0x2d0

    sput v0, Lcom/kehdev/DeviceService;->heightPx:I

    const/16 v0, 0x438

    sput v0, Lcom/kehdev/DeviceService;->width:I

    const-string v0, "AnD000400R130330X"

    sput-object v0, Lcom/kehdev/DeviceService;->driverVersion:Ljava/lang/String;

    sput-object v2, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    sput-object v2, Lcom/kehdev/DeviceService;->noSupportAppList:Ljava/util/List;

    const-string v0, ""

    sput-object v0, Lcom/kehdev/DeviceService;->remoteVerStr:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/kehdev/DeviceService;->dongleVerStr:Ljava/lang/String;

    sput v1, Lcom/kehdev/DeviceService;->processint:I

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x2

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "DeviceService"

    iput-object v0, p0, Lcom/kehdev/DeviceService;->LogTAG:Ljava/lang/String;

    const-string v0, "DeviceService"

    iput-object v0, p0, Lcom/kehdev/DeviceService;->TAG:Ljava/lang/String;

    const-string v0, "DeviceService"

    iput-object v0, p0, Lcom/kehdev/DeviceService;->tag:Ljava/lang/String;

    const-string v0, "com.hello"

    iput-object v0, p0, Lcom/kehdev/DeviceService;->testName:Ljava/lang/String;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->lastGameNameText:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mSaveAppButton:Landroid/widget/Button;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mKeyStatus:Lcom/kehdev/ReadKeyStatus;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->listMain:Landroid/widget/ListView;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mGridView:Landroid/widget/GridView;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mContext:Landroid/content/Context;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mResolveInfo:Ljava/util/List;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->viewFlip:Landroid/widget/ViewFlipper;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mHashMap:Ljava/util/HashMap;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->addGameButt:Landroid/widget/Button;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->addNotInstallGameButt:Landroid/widget/Button;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->drawTmp:Landroid/graphics/drawable/Drawable;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->drawBitTmp:Landroid/graphics/drawable/BitmapDrawable;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mbitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->mReceiver:Lcom/kehdev/DeviceService$mServiceUIReceiver;

    const/4 v0, -0x1

    iput v0, p0, Lcom/kehdev/DeviceService;->newVerCode:I

    const-string v0, ""

    iput-object v0, p0, Lcom/kehdev/DeviceService;->newVerName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/kehdev/DeviceService;->versonName:Ljava/lang/String;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->updateButton:Landroid/widget/Button;

    iput v3, p0, Lcom/kehdev/DeviceService;->updateType:I

    iput v3, p0, Lcom/kehdev/DeviceService;->mfailedno:I

    const-string v0, ""

    iput-object v0, p0, Lcom/kehdev/DeviceService;->downfail:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/kehdev/DeviceService;->SDK_version:Ljava/lang/String;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    new-array v0, v5, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/kehdev/DeviceService;->RemoteId:[I

    new-array v0, v5, [Z

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/kehdev/DeviceService;->RemoteIsOnline:[Z

    new-array v0, v5, [Landroid/widget/TextView;

    aput-object v2, v0, v3

    aput-object v2, v0, v4

    aput-object v2, v0, v6

    const/4 v1, 0x3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/kehdev/DeviceService;->userText:[Landroid/widget/TextView;

    new-array v0, v5, [Landroid/widget/TextView;

    aput-object v2, v0, v3

    aput-object v2, v0, v4

    aput-object v2, v0, v6

    const/4 v1, 0x3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/kehdev/DeviceService;->IDText:[Landroid/widget/TextView;

    new-array v0, v5, [Landroid/widget/ImageView;

    aput-object v2, v0, v3

    aput-object v2, v0, v4

    aput-object v2, v0, v6

    const/4 v1, 0x3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/kehdev/DeviceService;->TypeText:[Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->voiceControlRadio:Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->systemVoiceControl:Landroid/widget/RadioButton;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->userVoiceControl:Landroid/widget/RadioButton;

    iput v4, p0, Lcom/kehdev/DeviceService;->remStateBeforeSystemVoice:I

    iput-object v2, p0, Lcom/kehdev/DeviceService;->softwareVersion:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->databaseVersion:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->airmouseVersion:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->dongleVersion:Landroid/widget/TextView;

    const/16 v0, 0x14

    iput v0, p0, Lcom/kehdev/DeviceService;->SIZEVERSION:I

    const/16 v0, 0x14

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/kehdev/DeviceService;->versionPkg:[I

    iput-object v2, p0, Lcom/kehdev/DeviceService;->supportGameStringList:Ljava/util/ArrayList;

    iput v3, p0, Lcom/kehdev/DeviceService;->updateendflag:I

    iput-object v2, p0, Lcom/kehdev/DeviceService;->resolutionValueTextV:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/kehdev/DeviceService;->resolutionSeekBar:Landroid/widget/SeekBar;

    iput v3, p0, Lcom/kehdev/DeviceService;->resolutionValue:I

    iput v3, p0, Lcom/kehdev/DeviceService;->startStatus:I

    iput v3, p0, Lcom/kehdev/DeviceService;->successStatus:I

    iput v4, p0, Lcom/kehdev/DeviceService;->MOUSERESOLUTION:I

    iput v3, p0, Lcom/kehdev/DeviceService;->MatchCodeStatus:I

    new-instance v0, Lcom/kehdev/DeviceService$1;

    invoke-direct {v0, p0}, Lcom/kehdev/DeviceService$1;-><init>(Lcom/kehdev/DeviceService;)V

    iput-object v0, p0, Lcom/kehdev/DeviceService;->mKeyStatusConn:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/kehdev/DeviceService$2;

    invoke-direct {v0, p0}, Lcom/kehdev/DeviceService$2;-><init>(Lcom/kehdev/DeviceService;)V

    iput-object v0, p0, Lcom/kehdev/DeviceService;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/kehdev/DeviceService$10;

    invoke-direct {v0, p0}, Lcom/kehdev/DeviceService$10;-><init>(Lcom/kehdev/DeviceService;)V

    iput-object v0, p0, Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public static native _GetMatchCodeStatus()I
.end method

.method public static native _GetRemoteID(I)I
.end method

.method public static native _getRemoteAndDongleVersion([I)I
.end method

.method public static native _isARemoteInline(I)Z
.end method

.method public static native _readRemoteKeyStatus()I
.end method

.method public static native _runningApp(ILjava/lang/String;)I
.end method

.method public static native _startService()I
.end method

.method public static native _writeMatchCodeStatus(I)I
.end method

.method static synthetic access$000(Lcom/kehdev/DeviceService;)Lcom/kehdev/ReadKeyStatus;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mKeyStatus:Lcom/kehdev/ReadKeyStatus;

    return-object v0
.end method

.method static synthetic access$002(Lcom/kehdev/DeviceService;Lcom/kehdev/ReadKeyStatus;)Lcom/kehdev/ReadKeyStatus;
    .locals 0
    .param p0    # Lcom/kehdev/DeviceService;
    .param p1    # Lcom/kehdev/ReadKeyStatus;

    iput-object p1, p0, Lcom/kehdev/DeviceService;->mKeyStatus:Lcom/kehdev/ReadKeyStatus;

    return-object p1
.end method

.method static synthetic access$100(Lcom/kehdev/DeviceService;)Lcom/kehdev/GetRunningAppName;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/kehdev/DeviceService;)[Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->TypeText:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/kehdev/DeviceService;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;
    .locals 0
    .param p0    # Lcom/kehdev/DeviceService;
    .param p1    # Lcom/kehdev/GetRunningAppName;

    iput-object p1, p0, Lcom/kehdev/DeviceService;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/kehdev/DeviceService;)Lcom/kehdev/SqliteOperation;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object v0
.end method

.method static synthetic access$202(Lcom/kehdev/DeviceService;Lcom/kehdev/SqliteOperation;)Lcom/kehdev/SqliteOperation;
    .locals 0
    .param p0    # Lcom/kehdev/DeviceService;
    .param p1    # Lcom/kehdev/SqliteOperation;

    iput-object p1, p0, Lcom/kehdev/DeviceService;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object p1
.end method

.method static synthetic access$300(Lcom/kehdev/DeviceService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/kehdev/DeviceService;)Landroid/widget/RadioButton;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->systemVoiceControl:Landroid/widget/RadioButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/kehdev/DeviceService;)I
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget v0, p0, Lcom/kehdev/DeviceService;->remStateBeforeSystemVoice:I

    return v0
.end method

.method static synthetic access$502(Lcom/kehdev/DeviceService;I)I
    .locals 0
    .param p0    # Lcom/kehdev/DeviceService;
    .param p1    # I

    iput p1, p0, Lcom/kehdev/DeviceService;->remStateBeforeSystemVoice:I

    return p1
.end method

.method static synthetic access$700(Lcom/kehdev/DeviceService;)Landroid/widget/ViewFlipper;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->viewFlip:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/kehdev/DeviceService;)[Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->userText:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/kehdev/DeviceService;)[Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/kehdev/DeviceService;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->IDText:[Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public ListViewSetConent()V
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemTitle"

    const v2, 0x7f070008    # com.kehdev.R.string.dolphin

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemImage"

    const v2, 0x7f02003b    # com.kehdev.R.drawable.voice_control

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ItemTitle"

    const v2, 0x7f070009    # com.kehdev.R.string.voice_control

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemImage"

    const v2, 0x7f02001f    # com.kehdev.R.drawable.mouse_reso

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ItemTitle"

    const v2, 0x7f07000b    # com.kehdev.R.string.mouse_speed

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemTitle"

    const v2, 0x7f07000c    # com.kehdev.R.string.game

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemImage"

    const v2, 0x7f02002b    # com.kehdev.R.drawable.sensor_game

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ItemTitle"

    const v2, 0x7f07000e    # com.kehdev.R.string.sensor_game

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemTitle"

    const v2, 0x7f070010    # com.kehdev.R.string.user_manager

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemImage"

    const v2, 0x7f020039    # com.kehdev.R.drawable.user_list

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ItemTitle"

    const v2, 0x7f070011    # com.kehdev.R.string.user_list

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemImage"

    const v2, 0x7f02001a    # com.kehdev.R.drawable.match_code

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ItemTitle"

    const v2, 0x7f070012    # com.kehdev.R.string.match_code_manager

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ItemImage"

    const v2, 0x7f02002d    # com.kehdev.R.drawable.software_update

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ItemTitle"

    const v2, 0x7f070014    # com.kehdev.R.string.software_update

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public MatchCode()V
    .locals 5

    const-wide/16 v3, 0x0

    invoke-static {}, Lcom/kehdev/DeviceService;->_GetMatchCodeStatus()I

    move-result v0

    iput v0, p0, Lcom/kehdev/DeviceService;->MatchCodeStatus:I

    iget v0, p0, Lcom/kehdev/DeviceService;->MatchCodeStatus:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;

    const/16 v2, 0x60

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/kehdev/DeviceService;->MatchCodeStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;

    const/16 v2, 0x90

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_2
    iget v0, p0, Lcom/kehdev/DeviceService;->MatchCodeStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;

    const/16 v2, 0x99

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public bindAllApps()V
    .locals 17

    new-instance v11, Landroid/content/Intent;

    const-string v14, "android.intent.action.MAIN"

    const/4 v15, 0x0

    invoke-direct {v11, v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v14, "android.intent.category.LAUNCHER"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v14, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v15, 0x0

    invoke-virtual {v14, v11, v15}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/kehdev/DeviceService;->mResolveInfo:Ljava/util/List;

    sget-object v14, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v15, 0x2001

    invoke-virtual {v14, v15}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/PackageInfo;

    :try_start_0
    iget-object v14, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v12, v14, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v14, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    sget-object v15, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v14, v15}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    iget-object v14, v10, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    const/4 v15, 0x0

    aget-object v3, v14, v15

    iget-object v2, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object v5, v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/kehdev/DeviceService;->mHashMap:Ljava/util/HashMap;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/kehdev/DeviceService;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    invoke-virtual {v14}, Lcom/kehdev/SqliteOperation;->checkAllGameToList()Ljava/util/ArrayList;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/kehdev/DeviceService;->supportGameStringList:Ljava/util/ArrayList;

    new-instance v13, Landroid/content/pm/ResolveInfo;

    invoke-direct {v13}, Landroid/content/pm/ResolveInfo;-><init>()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/kehdev/DeviceService;->mResolveInfo:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/pm/ResolveInfo;

    sget-object v14, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v13, v14}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/kehdev/DeviceService;->mHashMap:Ljava/util/HashMap;

    invoke-virtual {v14, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/kehdev/DeviceService;->checkIfTheGameIsSupport(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_1

    sget-object v14, Lcom/kehdev/DeviceService;->noSupportAppList:Ljava/util/List;

    invoke-interface {v14, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/kehdev/DeviceService;->mResolveInfo:Ljava/util/List;

    new-instance v15, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    sget-object v16, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct/range {v15 .. v16}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v14, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public changeBuff2Display([I)Ljava/lang/String;
    .locals 7
    .param p1    # [I

    const/16 v6, 0x50

    const/4 v5, 0x4

    new-array v2, v5, [C

    new-array v3, v6, [C

    new-array v4, v6, [C

    const/4 v0, 0x0

    :goto_0
    const/16 v5, 0x14

    if-ge v0, v5, :cond_0

    aget v5, p1, v0

    invoke-virtual {p0, v5}, Lcom/kehdev/DeviceService;->int2charString(I)[C

    move-result-object v2

    mul-int/lit8 v5, v0, 0x4

    const/4 v6, 0x3

    aget-char v6, v2, v6

    aput-char v6, v3, v5

    mul-int/lit8 v5, v0, 0x4

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x2

    aget-char v6, v2, v6

    aput-char v6, v3, v5

    mul-int/lit8 v5, v0, 0x4

    add-int/lit8 v5, v5, 0x2

    const/4 v6, 0x1

    aget-char v6, v2, v6

    aput-char v6, v3, v5

    mul-int/lit8 v5, v0, 0x4

    add-int/lit8 v5, v5, 0x3

    const/4 v6, 0x0

    aget-char v6, v2, v6

    aput-char v6, v3, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {p0, v1}, Lcom/kehdev/DeviceService;->divStrToNeed(Ljava/lang/String;)V

    return-object v1
.end method

.method public checkIfTheGameIsSupport(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/kehdev/DeviceService;->supportGameStringList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/kehdev/DeviceService;->supportGameStringList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/kehdev/DeviceService;->supportGameStringList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public divStrToNeed(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v2, ""

    sput-object v2, Lcom/kehdev/DeviceService;->remoteVerStr:Ljava/lang/String;

    const-string v2, ""

    sput-object v2, Lcom/kehdev/DeviceService;->dongleVerStr:Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/kehdev/DeviceService;->remoteVerStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/kehdev/DeviceService;->remoteVerStr:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/kehdev/DeviceService;->dongleVerStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/kehdev/DeviceService;->dongleVerStr:Ljava/lang/String;

    goto :goto_1

    :cond_3
    return-void
.end method

.method public int2charString(I)[C
    .locals 3
    .param p1    # I

    const/4 v1, 0x4

    new-array v0, v1, [C

    const/4 v1, 0x3

    and-int/lit16 v2, p1, 0xff

    int-to-char v2, v2

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const v2, 0xff00

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x8

    int-to-char v2, v2

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0xff0000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x10

    int-to-char v2, v2

    aput-char v2, v0, v1

    const/4 v1, 0x0

    const/high16 v2, -0x1000000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x18

    int-to-char v2, v2

    aput-char v2, v0, v1

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v4}, Lcom/kehdev/DeviceService;->requestWindowFeature(I)Z

    const v0, 0x7f03000c    # com.kehdev.R.layout.main

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->setContentView(I)V

    const v0, 0x7f090020    # com.kehdev.R.id.vf_Content

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->viewFlip:Landroid/widget/ViewFlipper;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/kehdev/DeviceService;->mHashMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/kehdev/DeviceService;->noSupportAppList:Ljava/util/List;

    const v0, 0x7f090029    # com.kehdev.R.id.addgame

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->addGameButt:Landroid/widget/Button;

    const v0, 0x7f09002a    # com.kehdev.R.id.addNotInstallGame

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->addNotInstallGameButt:Landroid/widget/Button;

    const v0, 0x7f090025    # com.kehdev.R.id.voice_search

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    const v0, 0x7f090049    # com.kehdev.R.id.software_version

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->softwareVersion:Landroid/widget/TextView;

    const v0, 0x7f09004a    # com.kehdev.R.id.database_version

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->databaseVersion:Landroid/widget/TextView;

    const v0, 0x7f090047    # com.kehdev.R.id.airmouse_version

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->airmouseVersion:Landroid/widget/TextView;

    const v0, 0x7f090048    # com.kehdev.R.id.dongle_version

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->dongleVersion:Landroid/widget/TextView;

    const v0, 0x7f090045    # com.kehdev.R.id.openMatchcode

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;

    const v0, 0x7f090046    # com.kehdev.R.id.closeMatchcode

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;

    new-instance v1, Lcom/kehdev/DeviceService$3;

    invoke-direct {v1, p0}, Lcom/kehdev/DeviceService$3;-><init>(Lcom/kehdev/DeviceService;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;

    new-instance v1, Lcom/kehdev/DeviceService$4;

    invoke-direct {v1, p0}, Lcom/kehdev/DeviceService$4;-><init>(Lcom/kehdev/DeviceService;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->softwareVersion:Landroid/widget/TextView;

    sget-object v1, Lcom/kehdev/DeviceService;->driverVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090022    # com.kehdev.R.id.voice_radiogroup

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->voiceControlRadio:Landroid/widget/RadioGroup;

    const v0, 0x7f090024    # com.kehdev.R.id.system_voice_control

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->systemVoiceControl:Landroid/widget/RadioButton;

    const v0, 0x7f090023    # com.kehdev.R.id.user_voice_control

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->userVoiceControl:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->userVoiceControl:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->voiceControlRadio:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/kehdev/DeviceService$5;

    invoke-direct {v1, p0}, Lcom/kehdev/DeviceService$5;-><init>(Lcom/kehdev/DeviceService;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/kehdev/DeviceService$6;

    invoke-direct {v1, p0}, Lcom/kehdev/DeviceService$6;-><init>(Lcom/kehdev/DeviceService;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v2, p0, Lcom/kehdev/DeviceService;->userText:[Landroid/widget/TextView;

    const v0, 0x7f09002c    # com.kehdev.R.id.UserText01

    mul-int/lit8 v3, v1, 0x3

    add-int/2addr v0, v3

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v1

    iget-object v2, p0, Lcom/kehdev/DeviceService;->IDText:[Landroid/widget/TextView;

    const v0, 0x7f09002d    # com.kehdev.R.id.IDText01

    mul-int/lit8 v3, v1, 0x3

    add-int/2addr v0, v3

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v1

    iget-object v2, p0, Lcom/kehdev/DeviceService;->TypeText:[Landroid/widget/ImageView;

    const v0, 0x7f09002e    # com.kehdev.R.id.TypeText01

    mul-int/lit8 v3, v1, 0x3

    add-int/2addr v0, v3

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/kehdev/DeviceService$ThreadGetRemoteId;

    invoke-direct {v1, p0}, Lcom/kehdev/DeviceService$ThreadGetRemoteId;-><init>(Lcom/kehdev/DeviceService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/kehdev/DeviceService;->remotethread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->remotethread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kehdev/DeviceService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kehdev/GetRunningAppName;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/kehdev/DeviceService;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v4}, Lcom/kehdev/DeviceService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/kehdev/SensorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/kehdev/ReadKeyStatus;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/kehdev/DeviceService;->mKeyStatusConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v4}, Lcom/kehdev/DeviceService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    const v0, 0x7f09001f    # com.kehdev.R.id.list

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->listMain:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/kehdev/DeviceService;->ListViewSetConent()V

    new-instance v0, Lcom/kehdev/DeviceService$mServiceUIReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/kehdev/DeviceService$mServiceUIReceiver;-><init>(Lcom/kehdev/DeviceService;Lcom/kehdev/DeviceService$1;)V

    iput-object v0, p0, Lcom/kehdev/DeviceService;->mReceiver:Lcom/kehdev/DeviceService$mServiceUIReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.kehdev.version"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/kehdev/DeviceService;->mReceiver:Lcom/kehdev/DeviceService$mServiceUIReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/kehdev/DeviceService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kehdev/DeviceService;->supportGameStringList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->listMain:Landroid/widget/ListView;

    new-instance v1, Lcom/kehdev/DeviceService$7;

    invoke-direct {v1, p0}, Lcom/kehdev/DeviceService$7;-><init>(Lcom/kehdev/DeviceService;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->addGameButt:Landroid/widget/Button;

    new-instance v1, Lcom/kehdev/DeviceService$8;

    invoke-direct {v1, p0}, Lcom/kehdev/DeviceService$8;-><init>(Lcom/kehdev/DeviceService;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->addNotInstallGameButt:Landroid/widget/Button;

    new-instance v1, Lcom/kehdev/DeviceService$9;

    invoke-direct {v1, p0}, Lcom/kehdev/DeviceService$9;-><init>(Lcom/kehdev/DeviceService;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mKeyStatusConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mReceiver:Lcom/kehdev/DeviceService$mServiceUIReceiver;

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mResolveInfo:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "appSltLable"

    sget-object v4, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/DeviceService;->drawTmp:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->drawTmp:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->drawBitTmp:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/kehdev/DeviceService;->drawBitTmp:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/DeviceService;->mbitmap:Landroid/graphics/Bitmap;

    const-string v0, "appIconBitmap"

    iget-object v3, p0, Lcom/kehdev/DeviceService;->mbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "completePkgName"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-class v0, Lcom/kehdev/GameManual;

    invoke-virtual {v2, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/kehdev/DeviceService;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onRestart()V
    .locals 0

    invoke-virtual {p0}, Lcom/kehdev/DeviceService;->setupSensorGameViews()V

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    return-void
.end method

.method public setupSensorGameViews()V
    .locals 4

    iput-object p0, p0, Lcom/kehdev/DeviceService;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/kehdev/DeviceService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sput-object v0, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    const v0, 0x7f090028    # com.kehdev.R.id.allapps

    invoke-virtual {p0, v0}, Lcom/kehdev/DeviceService;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/kehdev/DeviceService;->mGridView:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/kehdev/DeviceService;->bindAllApps()V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mGridView:Landroid/widget/GridView;

    new-instance v1, Lcom/kehdev/DeviceService$GridItemAdapter;

    iget-object v2, p0, Lcom/kehdev/DeviceService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/kehdev/DeviceService;->mResolveInfo:Ljava/util/List;

    invoke-direct {v1, p0, v2, v3}, Lcom/kehdev/DeviceService$GridItemAdapter;-><init>(Lcom/kehdev/DeviceService;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public softandupdate()V
    .locals 3

    iget-object v1, p0, Lcom/kehdev/DeviceService;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    invoke-virtual {v1}, Lcom/kehdev/SqliteOperation;->readDbData()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService;->databaseVersion:Landroid/widget/TextView;

    const-string v2, "20130909"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/kehdev/DeviceService;->versionPkg:[I

    invoke-static {v1}, Lcom/kehdev/DeviceService;->_getRemoteAndDongleVersion([I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/kehdev/DeviceService;->versionPkg:[I

    invoke-static {v1}, Lcom/kehdev/DeviceService;->_getRemoteAndDongleVersion([I)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/kehdev/DeviceService;->versionPkg:[I

    invoke-virtual {p0, v1}, Lcom/kehdev/DeviceService;->changeBuff2Display([I)Ljava/lang/String;

    iget-object v1, p0, Lcom/kehdev/DeviceService;->airmouseVersion:Landroid/widget/TextView;

    sget-object v2, Lcom/kehdev/DeviceService;->remoteVerStr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/kehdev/DeviceService;->dongleVersion:Landroid/widget/TextView;

    sget-object v2, Lcom/kehdev/DeviceService;->dongleVerStr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public voiceControl()V
    .locals 3

    const/4 v2, 0x1

    sget v0, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003c    # com.kehdev.R.drawable.voice_search

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003d    # com.kehdev.R.drawable.voice_search_disable

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
