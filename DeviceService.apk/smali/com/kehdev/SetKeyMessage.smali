.class public Lcom/kehdev/SetKeyMessage;
.super Landroid/app/Activity;
.source "SetKeyMessage.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private keyMessageEditText:Landroid/widget/EditText;

.field private keyMessageEditTextSet:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "keyMessage"

    iput-object v0, p0, Lcom/kehdev/SetKeyMessage;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/kehdev/SetKeyMessage;->keyMessageEditText:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/kehdev/SetKeyMessage;->keyMessageEditTextSet:Landroid/widget/Button;

    return-void
.end method

.method static synthetic access$000(Lcom/kehdev/SetKeyMessage;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/kehdev/SetKeyMessage;

    iget-object v0, p0, Lcom/kehdev/SetKeyMessage;->keyMessageEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/kehdev/SetKeyMessage;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/kehdev/SetKeyMessage;

    iget-object v0, p0, Lcom/kehdev/SetKeyMessage;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/kehdev/SetKeyMessage;->requestWindowFeature(I)Z

    const v0, 0x7f030014    # com.kehdev.R.layout.set_key_message_layout

    invoke-virtual {p0, v0}, Lcom/kehdev/SetKeyMessage;->setContentView(I)V

    const v0, 0x7f090054    # com.kehdev.R.id.keyMessageEditText

    invoke-virtual {p0, v0}, Lcom/kehdev/SetKeyMessage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/kehdev/SetKeyMessage;->keyMessageEditText:Landroid/widget/EditText;

    const v0, 0x7f090055    # com.kehdev.R.id.keyMessageEditTextSet

    invoke-virtual {p0, v0}, Lcom/kehdev/SetKeyMessage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/kehdev/SetKeyMessage;->keyMessageEditTextSet:Landroid/widget/Button;

    iget-object v0, p0, Lcom/kehdev/SetKeyMessage;->keyMessageEditTextSet:Landroid/widget/Button;

    new-instance v1, Lcom/kehdev/SetKeyMessage$1;

    invoke-direct {v1, p0}, Lcom/kehdev/SetKeyMessage$1;-><init>(Lcom/kehdev/SetKeyMessage;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
