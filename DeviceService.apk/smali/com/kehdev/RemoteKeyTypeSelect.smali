.class public Lcom/kehdev/RemoteKeyTypeSelect;
.super Landroid/app/Activity;
.source "RemoteKeyTypeSelect.java"


# instance fields
.field private keySltListview:Landroid/widget/ListView;

.field private keySltListviewAdapterAList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListview:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListviewAdapterAList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x2

    const/4 v6, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v6}, Lcom/kehdev/RemoteKeyTypeSelect;->requestWindowFeature(I)Z

    const v1, 0x7f030010    # com.kehdev.R.layout.remote_key_type_slt_layout

    invoke-virtual {p0, v1}, Lcom/kehdev/RemoteKeyTypeSelect;->setContentView(I)V

    const v1, 0x7f09004e    # com.kehdev.R.id.remotekeysltlistview

    invoke-virtual {p0, v1}, Lcom/kehdev/RemoteKeyTypeSelect;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListview:Landroid/widget/ListView;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListviewAdapterAList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/kehdev/RemoteKeyTypeSelect;->setKeySltListviewAdapterAList()V

    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v2, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListviewAdapterAList:Ljava/util/ArrayList;

    const v3, 0x7f030011    # com.kehdev.R.layout.remote_key_type_slt_listview_layout

    new-array v4, v7, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "keyTypeText"

    aput-object v5, v4, v1

    const-string v1, "keyTypeImage"

    aput-object v1, v4, v6

    new-array v5, v7, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iget-object v1, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListview:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListview:Landroid/widget/ListView;

    new-instance v2, Lcom/kehdev/RemoteKeyTypeSelect$1;

    invoke-direct {v2, p0}, Lcom/kehdev/RemoteKeyTypeSelect$1;-><init>(Lcom/kehdev/RemoteKeyTypeSelect;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void

    :array_0
    .array-data 4
        0x7f09004f    # com.kehdev.R.id.keyTypeText
        0x7f090050    # com.kehdev.R.id.keyTypeImage
    .end array-data
.end method

.method public setKeySltListviewAdapterAList()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "keyTypeText"

    const v2, 0x7f070027    # com.kehdev.R.string.press

    invoke-virtual {p0, v2}, Lcom/kehdev/RemoteKeyTypeSelect;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "keyTypeImage"

    const v2, 0x7f020035    # com.kehdev.R.drawable.touch

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListviewAdapterAList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "keyTypeText"

    const v2, 0x7f070028    # com.kehdev.R.string.singleSlide

    invoke-virtual {p0, v2}, Lcom/kehdev/RemoteKeyTypeSelect;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "keyTypeImage"

    const v2, 0x7f02002c    # com.kehdev.R.drawable.slide

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListviewAdapterAList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "keyTypeText"

    const v2, 0x7f070029    # com.kehdev.R.string.doubleSlide

    invoke-virtual {p0, v2}, Lcom/kehdev/RemoteKeyTypeSelect;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "keyTypeImage"

    const v2, 0x7f020037    # com.kehdev.R.drawable.two_p_slide

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/kehdev/RemoteKeyTypeSelect;->keySltListviewAdapterAList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method
