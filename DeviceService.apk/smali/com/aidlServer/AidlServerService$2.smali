.class Lcom/aidlServer/AidlServerService$2;
.super Lcom/aidlServer/IAIDLServerService$Stub;
.source "AidlServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/aidlServer/AidlServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/aidlServer/AidlServerService;


# direct methods
.method constructor <init>(Lcom/aidlServer/AidlServerService;)V
    .locals 0

    iput-object p1, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    invoke-direct {p0}, Lcom/aidlServer/IAIDLServerService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getAllVersion()Lcom/aidlServer/Version;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->mVersion:Lcom/aidlServer/Version;

    return-object v0
.end method

.method public getMouseResolutionRemote()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget-boolean v0, v0, Lcom/aidlServer/AidlServerService;->getMouseResoFlg:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget v0, v0, Lcom/aidlServer/AidlServerService;->mGetResoValue:I

    goto :goto_0
.end method

.method public getSetMouseResolutionRemote()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget-boolean v1, v1, Lcom/aidlServer/AidlServerService;->setMouseResoFlg:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iput-boolean v0, v1, Lcom/aidlServer/AidlServerService;->setMouseResoFlg:Z

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget v0, v0, Lcom/aidlServer/AidlServerService;->mSetResoValue:I

    goto :goto_0
.end method

.method public setAllVersion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->mVersion:Lcom/aidlServer/Version;

    invoke-virtual {v0, p1}, Lcom/aidlServer/Version;->setRemoteVersion(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->mVersion:Lcom/aidlServer/Version;

    invoke-virtual {v0, p2}, Lcom/aidlServer/Version;->setDongleVersion(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->mVersion:Lcom/aidlServer/Version;

    invoke-virtual {v0, p3}, Lcom/aidlServer/Version;->setDriverVersion(Ljava/lang/String;)V

    return-void
.end method

.method public setGetMouseResolutionRemote(I)I
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iput p1, v0, Lcom/aidlServer/AidlServerService;->mGetResoValue:I

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iput-boolean v1, v0, Lcom/aidlServer/AidlServerService;->getMouseResoFlg:Z

    return v1
.end method

.method public setMouseResolutionRemote(I)I
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iput p1, v0, Lcom/aidlServer/AidlServerService;->mSetResoValue:I

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iget v0, v0, Lcom/aidlServer/AidlServerService;->mSetResoValue:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iput v1, v0, Lcom/aidlServer/AidlServerService;->mSetResoValue:I

    :cond_0
    iget-object v0, p0, Lcom/aidlServer/AidlServerService$2;->this$0:Lcom/aidlServer/AidlServerService;

    iput-boolean v1, v0, Lcom/aidlServer/AidlServerService;->setMouseResoFlg:Z

    return v1
.end method
