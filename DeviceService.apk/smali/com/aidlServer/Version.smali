.class public Lcom/aidlServer/Version;
.super Ljava/lang/Object;
.source "Version.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/aidlServer/Version;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public TAG:Ljava/lang/String;

.field public dongleVersionStr:Ljava/lang/String;

.field public driverVersionStr:Ljava/lang/String;

.field public remoteVersionStr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/aidlServer/Version$1;

    invoke-direct {v0}, Lcom/aidlServer/Version$1;-><init>()V

    sput-object v0, Lcom/aidlServer/Version;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Version"

    iput-object v0, p0, Lcom/aidlServer/Version;->TAG:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/aidlServer/Version;->remoteVersionStr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/aidlServer/Version;->dongleVersionStr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/aidlServer/Version;->driverVersionStr:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Version"

    iput-object v0, p0, Lcom/aidlServer/Version;->TAG:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/aidlServer/Version;->remoteVersionStr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/aidlServer/Version;->dongleVersionStr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/aidlServer/Version;->driverVersionStr:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/aidlServer/Version;->remoteVersionStr:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/aidlServer/Version;->dongleVersionStr:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/aidlServer/Version;->driverVersionStr:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setDongleVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/aidlServer/Version;->dongleVersionStr:Ljava/lang/String;

    return-void
.end method

.method public setDriverVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/aidlServer/Version;->driverVersionStr:Ljava/lang/String;

    return-void
.end method

.method public setRemoteVersion(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/aidlServer/Version;->remoteVersionStr:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/aidlServer/Version;->remoteVersionStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/aidlServer/Version;->dongleVersionStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/aidlServer/Version;->driverVersionStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
