.class Lorg/javia/arity/DoubleStack;
.super Ljava/lang/Object;
.source "DoubleStack.java"


# instance fields
.field private im:[D

.field private re:[D

.field private size:I


# direct methods
.method constructor <init>()V
    .locals 2

    const/16 v1, 0x8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [D

    iput-object v0, p0, Lorg/javia/arity/DoubleStack;->re:[D

    new-array v0, v1, [D

    iput-object v0, p0, Lorg/javia/arity/DoubleStack;->im:[D

    const/4 v0, 0x0

    iput v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    return-void
.end method

.method getIm()[D
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x1

    move v0, v1

    :goto_0
    iget v3, p0, Lorg/javia/arity/DoubleStack;->size:I

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lorg/javia/arity/DoubleStack;->im:[D

    aget-wide v3, v3, v0

    const-wide/16 v5, 0x0

    cmpl-double v3, v3, v5

    if-eqz v3, :cond_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_2
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    new-array v0, v0, [D

    iget-object v2, p0, Lorg/javia/arity/DoubleStack;->im:[D

    iget v3, p0, Lorg/javia/arity/DoubleStack;->size:I

    invoke-static {v2, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method getRe()[D
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    new-array v0, v0, [D

    iget-object v1, p0, Lorg/javia/arity/DoubleStack;->re:[D

    iget v2, p0, Lorg/javia/arity/DoubleStack;->size:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method pop()V
    .locals 1

    iget v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    return-void
.end method

.method push(DD)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    iget-object v1, p0, Lorg/javia/arity/DoubleStack;->re:[D

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lorg/javia/arity/DoubleStack;->re:[D

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x1

    new-array v1, v0, [D

    new-array v0, v0, [D

    iget-object v2, p0, Lorg/javia/arity/DoubleStack;->re:[D

    iget-object v3, p0, Lorg/javia/arity/DoubleStack;->re:[D

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lorg/javia/arity/DoubleStack;->im:[D

    iget-object v3, p0, Lorg/javia/arity/DoubleStack;->re:[D

    array-length v3, v3

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lorg/javia/arity/DoubleStack;->re:[D

    iput-object v0, p0, Lorg/javia/arity/DoubleStack;->im:[D

    :cond_0
    iget-object v0, p0, Lorg/javia/arity/DoubleStack;->re:[D

    iget v1, p0, Lorg/javia/arity/DoubleStack;->size:I

    aput-wide p1, v0, v1

    iget-object v0, p0, Lorg/javia/arity/DoubleStack;->im:[D

    iget v1, p0, Lorg/javia/arity/DoubleStack;->size:I

    aput-wide p3, v0, v1

    iget v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/javia/arity/DoubleStack;->size:I

    return-void
.end method
