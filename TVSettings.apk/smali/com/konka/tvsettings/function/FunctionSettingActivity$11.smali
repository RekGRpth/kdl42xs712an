.class Lcom/konka/tvsettings/function/FunctionSettingActivity$11;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "FunctionSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemDynamicDenoise()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$7(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    invoke-static {v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$8(I)V

    if-nez v0, :cond_1

    const-string v1, "dynamic denoise mode low"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_LOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->setDNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_LOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetMpegNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne v1, v0, :cond_2

    const-string v1, "dynamic denoise mode middle"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->setDNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetMpegNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)Z

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne v1, v0, :cond_3

    const-string v1, "dynamic denoise mode hight"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_HIGH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->setDNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_HIGH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetMpegNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)Z

    goto :goto_0

    :cond_3
    const/4 v1, 0x3

    if-ne v1, v0, :cond_4

    const-string v1, "dynamic denoise mode default"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->setDNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetMpegNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)Z

    goto :goto_0

    :cond_4
    const/4 v1, 0x4

    if-ne v1, v0, :cond_0

    const-string v1, "dynamic denoise mode off"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->setDNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetMpegNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)Z

    goto/16 :goto_0
.end method
