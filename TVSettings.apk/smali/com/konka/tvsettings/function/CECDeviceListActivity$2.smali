.class Lcom/konka/tvsettings/function/CECDeviceListActivity$2;
.super Ljava/lang/Object;
.source "CECDeviceListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/CECDeviceListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/CECDeviceListActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/CECDeviceListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity$2;->this$0:Lcom/konka/tvsettings/function/CECDeviceListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity$2;->this$0:Lcom/konka/tvsettings/function/CECDeviceListActivity;

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity$2;->this$0:Lcom/konka/tvsettings/function/CECDeviceListActivity;

    iget-object v0, v0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DeviceListLa:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    iput-object v0, v1, Lcom/konka/tvsettings/function/CECDeviceListActivity;->SelectDevice:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v0, "candy_test"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "selectItem:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity$2;->this$0:Lcom/konka/tvsettings/function/CECDeviceListActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/function/CECDeviceListActivity;->SelectDevice:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity$2;->this$0:Lcom/konka/tvsettings/function/CECDeviceListActivity;

    iget-object v0, v0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    iget-object v1, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity$2;->this$0:Lcom/konka/tvsettings/function/CECDeviceListActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/function/CECDeviceListActivity;->SelectDevice:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/CecManager;->setStreamPath(Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;)V

    return-void
.end method
