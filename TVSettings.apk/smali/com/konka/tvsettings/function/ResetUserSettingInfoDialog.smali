.class public Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;
.super Landroid/app/Activity;
.source "ResetUserSettingInfoDialog.java"


# instance fields
.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private m_locale:Ljava/util/Locale;

.field private settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field protected textview_cha_reset_no_item:Landroid/widget/TextView;

.field protected textview_cha_reset_yes_item:Landroid/widget/TextView;

.field private tvCommonManager:Lcom/mstar/android/tv/TvCommonManager;

.field private tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private tvS3DManager:Lcom/mstar/android/tv/TvS3DManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvS3DManager:Lcom/mstar/android/tv/TvS3DManager;

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvCommonManager:Lcom/mstar/android/tv/TvCommonManager;

    return-void
.end method

.method private getLocale(Ljava/lang/String;)Ljava/util/Locale;
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/Locale;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->m_locale:Ljava/util/Locale;

    iget-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->m_locale:Ljava/util/Locale;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030043    # com.konka.tvsettings.R.layout.reset_usersetting_dialog

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->setContentView(I)V

    const v0, 0x7f0701c3    # com.konka.tvsettings.R.id.textview_cha_reset_yes

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->textview_cha_reset_yes_item:Landroid/widget/TextView;

    const v0, 0x7f0701c4    # com.konka.tvsettings.R.id.textview_cha_reset_no

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->textview_cha_reset_no_item:Landroid/widget/TextView;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvS3DManager:Lcom/mstar/android/tv/TvS3DManager;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvCommonManager:Lcom/mstar/android/tv/TvCommonManager;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 29
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getId()I

    move-result v6

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super/range {p0 .. p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v26

    :goto_1
    return v26

    :sswitch_0
    const v26, 0x7f0701c3    # com.konka.tvsettings.R.id.textview_cha_reset_yes

    move/from16 v0, v26

    if-ne v6, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getFactoryCusDefSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    move-result-object v24

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, v24

    iput v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/DataBaseDesk;->setFactoryCusDefSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Z

    :try_start_0
    const-string v26, "setEnvironment"

    const-string v27, "game_mode"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v26

    const-string v27, "game_mode"

    const-string v28, "disable"

    invoke-virtual/range {v26 .. v28}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/konka/kkinterface/tv/SettingDesk;->ExecRestoreToDefault()Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-interface/range {v26 .. v27}, Lcom/konka/kkinterface/tv/SettingDesk;->SetInstallationFlag(Z)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    move-object/from16 v26, v0

    const/16 v27, 0xff

    invoke-interface/range {v26 .. v27}, Lcom/konka/kkinterface/tv/SettingDesk;->SetEnvironmentPowerOnMusicVolume(S)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v26

    sget-object v27, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvS3DManager:Lcom/mstar/android/tv/TvS3DManager;

    move-object/from16 v26, v0

    sget-object v27, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual/range {v27 .. v27}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->ordinal()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormatForUI(I)V

    sget-object v26, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-interface/range {v25 .. v26}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvSetAntennaType(Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;)V

    :cond_1
    const-wide/16 v26, 0x1f4

    :try_start_1
    invoke-static/range {v26 .. v27}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v21

    :try_start_2
    invoke-interface/range {v21 .. v21}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    const-string v26, "en_US"

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->getLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v26

    move-object/from16 v0, v26

    iput-object v0, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    const/16 v26, 0x1

    move/from16 v0, v26

    iput-boolean v0, v4, Landroid/content/res/Configuration;->userSetLocale:Z

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    const-string v26, "com.android.providers.settings"

    invoke-static/range {v26 .. v26}, Landroid/app/backup/BackupManager;->dataChanged(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_4
    :try_start_3
    const-string v26, "user Start to delete channel data!"

    invoke-static/range {v26 .. v26}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v19, Ljava/io/FileInputStream;

    const-string v26, "/tvdatabase/DatabaseBackup/user_setting.db"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v20, Ljava/io/FileOutputStream;

    const-string v26, "/tvdatabase/Database/user_setting.db"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/io/FileInputStream;

    const-string v26, "/tvdatabase/DatabaseBackup/atv_cmdb_cable_bak.bin"

    move-object/from16 v0, v26

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/io/FileInputStream;

    const-string v26, "/tvdatabase/DatabaseBackup/atv_cmdb_bak.bin"

    move-object/from16 v0, v26

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljava/io/FileOutputStream;

    const-string v26, "/tvdatabase/Database/atv_cmdb.bin"

    move-object/from16 v0, v26

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/io/FileOutputStream;

    const-string v26, "/tvdatabase/Database/atv_cmdb_cable.bin"

    move-object/from16 v0, v26

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v11, Ljava/io/FileInputStream;

    const-string v26, "/tvdatabase/DatabaseBackup/dtv_cmdb_0_bak.bin"

    move-object/from16 v0, v26

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v13, Ljava/io/FileInputStream;

    const-string v26, "/tvdatabase/DatabaseBackup/dtv_cmdb_1_bak.bin"

    move-object/from16 v0, v26

    invoke-direct {v13, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v15, Ljava/io/FileInputStream;

    const-string v26, "/tvdatabase/DatabaseBackup/dtv_cmdb_2_bak.bin"

    move-object/from16 v0, v26

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v12, Ljava/io/FileOutputStream;

    const-string v26, "/tvdatabase/Database/dtv_cmdb_0.bin"

    move-object/from16 v0, v26

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v14, Ljava/io/FileOutputStream;

    const-string v26, "/tvdatabase/Database/dtv_cmdb_1.bin"

    move-object/from16 v0, v26

    invoke-direct {v14, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v16, Ljava/io/FileOutputStream;

    const-string v26, "/tvdatabase/Database/dtv_cmdb_2.bin"

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const/16 v26, 0x2000

    move/from16 v0, v26

    new-array v3, v0, [B

    const/4 v5, 0x0

    :goto_5
    invoke-virtual {v9, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    if-gtz v5, :cond_4

    :goto_6
    invoke-virtual {v7, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    if-gtz v5, :cond_5

    :goto_7
    invoke-virtual {v11, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    if-gtz v5, :cond_6

    :goto_8
    invoke-virtual {v13, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    if-gtz v5, :cond_7

    :goto_9
    invoke-virtual {v15, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    if-gtz v5, :cond_8

    :goto_a
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    if-gtz v5, :cond_9

    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v13}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual/range {v19 .. v19}, Ljava/io/FileInputStream;->close()V

    const-string v26, "user delete channel data end!"

    invoke-static/range {v26 .. v26}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_b
    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v26

    if-eqz v26, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v26

    const/16 v27, 0x17

    invoke-virtual/range {v26 .. v27}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_2
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->tvCommonManager:Lcom/mstar/android/tv/TvCommonManager;

    move-object/from16 v26, v0

    const-string v27, "reboot"

    invoke-virtual/range {v26 .. v27}, Lcom/mstar/android/tv/TvCommonManager;->rebootSystem(Ljava/lang/String;)V

    :cond_3
    const v26, 0x7f0701c4    # com.konka.tvsettings.R.id.textview_cha_reset_no

    move/from16 v0, v26

    if-ne v6, v0, :cond_0

    new-instance v22, Landroid/content/Intent;

    invoke-direct/range {v22 .. v22}, Landroid/content/Intent;-><init>()V

    const-class v26, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->startActivity(Landroid/content/Intent;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->finish()V

    goto/16 :goto_0

    :catch_0
    move-exception v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :catch_1
    move-exception v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_3

    :catch_2
    move-exception v17

    invoke-virtual/range {v17 .. v17}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_4

    :cond_4
    const/16 v26, 0x0

    :try_start_5
    move/from16 v0, v26

    invoke-virtual {v10, v3, v0, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_5

    :catch_3
    move-exception v17

    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v27, "reset fail!"

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    :cond_5
    const/16 v26, 0x0

    :try_start_6
    move/from16 v0, v26

    invoke-virtual {v8, v3, v0, v5}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_6

    :cond_6
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v12, v3, v0, v5}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_7

    :cond_7
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v14, v3, v0, v5}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_8

    :cond_8
    const/16 v26, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1, v5}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_9

    :cond_9
    const/16 v26, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_a

    :catch_4
    move-exception v17

    const-string v26, "send T_SoundSetting_IDX unsuccessful!"

    invoke-static/range {v26 .. v26}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_c

    :sswitch_1
    new-instance v23, Landroid/content/Intent;

    const-class v26, Lcom/konka/tvsettings/RootActivity;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->startActivity(Landroid/content/Intent;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->finish()V

    const/16 v26, 0x0

    const v27, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->overridePendingTransition(II)V

    goto/16 :goto_0

    :sswitch_2
    new-instance v22, Landroid/content/Intent;

    invoke-direct/range {v22 .. v22}, Landroid/content/Intent;-><init>()V

    const-class v26, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->startActivity(Landroid/content/Intent;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;->finish()V

    goto/16 :goto_0

    :sswitch_3
    const/16 v26, 0x1

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x42 -> :sswitch_0
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_3
    .end sparse-switch
.end method
