.class public Lcom/konka/tvsettings/function/PVROptionActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "PVROptionActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;,
        Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private diskFormat:Landroid/widget/LinearLayout;

.field private diskFormatStatus:Landroid/widget/TextView;

.field private listener:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

.field private mpFormatDialog:Landroid/app/ProgressDialog;

.field private myHandler:Landroid/os/Handler;

.field private pvr:Lcom/konka/kkinterface/tv/PvrDesk;

.field private selectDisk:Landroid/widget/TextView;

.field private selectedDiskLable:Ljava/lang/String;

.field private selectedDiskPath:Ljava/lang/String;

.field private speedCheck:Landroid/widget/LinearLayout;

.field private speedCheckResult:Landroid/widget/TextView;

.field private storageManager:Lcom/mstar/android/storage/MStorageManager;

.field private timeShiftSize:Landroid/widget/TextView;

.field private usbReceiver:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

.field private usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

.field private waitToFormat:Z

.field private waitToSpeedTest:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    const-string v0, "PVROptionActivity"

    iput-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectDisk:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->timeShiftSize:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormat:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormatStatus:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->speedCheck:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->speedCheckResult:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskLable:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->listener:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    iput-boolean v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->waitToFormat:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->waitToSpeedTest:Z

    new-instance v0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    invoke-direct {v0, p0, v1}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->usbReceiver:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    iput-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/konka/tvsettings/function/PVROptionActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$1;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/PVROptionActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->waitToFormat:Z

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/tvsettings/function/USBDiskSelecter;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/function/PVROptionActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->startSpeedTest()V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/function/PVROptionActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->waitToFormat:Z

    return v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/mstar/android/storage/MStorageManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormatStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectDisk:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/function/PVROptionActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/function/PVROptionActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskLable:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$18(Lcom/konka/tvsettings/function/PVROptionActivity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/konka/tvsettings/function/PVROptionActivity;->saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$19(Lcom/konka/tvsettings/function/PVROptionActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->waitToSpeedTest:Z

    return v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$20(Lcom/konka/tvsettings/function/PVROptionActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->startFormat()V

    return-void
.end method

.method static synthetic access$21(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->speedCheckResult:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/function/PVROptionActivity;)I
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->getLastTimeShiftSize()I

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/function/PVROptionActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/function/PVROptionActivity;->saveLastTimeShiftSize(I)V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/function/PVROptionActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->formatConfirm()V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/function/PVROptionActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->waitToSpeedTest:Z

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskLable:Ljava/lang/String;

    return-object v0
.end method

.method private formatConfirm()V
    .locals 5

    const-string v2, "USB format confirm ========>>>Create "

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a00ec    # com.konka.tvsettings.R.string.str_pvr_format_usb

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0026    # com.konka.tvsettings.R.string.str_program_edit_dialog_ok

    new-instance v4, Lcom/konka/tvsettings/function/PVROptionActivity$3;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$3;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0027    # com.konka.tvsettings.R.string.str_program_edit_dialog_cancel

    new-instance v4, Lcom/konka/tvsettings/function/PVROptionActivity$4;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$4;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    return-void
.end method

.method private getLastTimeShiftSize()I
    .locals 3

    const/4 v2, 0x0

    const-string v1, "save_setting_select"

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "LAST_SHIFT_SIZE"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private initUIListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectDisk:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->listener:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->timeShiftSize:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->listener:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormat:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->listener:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->speedCheck:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->listener:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private registerDiskDetector()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->usbReceiver:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->usbReceiver:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->usbReceiver:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v2, "save_setting_select"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/PVROptionActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "IS_ALREADY_CHOOSE_DISK"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_PATH"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_LABEL"

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_LABEL_PC_STYLE"

    invoke-interface {v0, v2, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private saveLastTimeShiftSize(I)V
    .locals 4
    .param p1    # I

    const-string v2, "save_setting_select"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/PVROptionActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "LAST_SHIFT_SIZE"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private startFormat()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "USB format confirm ========>>>start format 1 "

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00ee    # com.konka.tvsettings.R.string.str_pvr_usb_format_progressing

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iput-boolean v4, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->waitToFormat:Z

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v3}, Lcom/mstar/android/storage/MStorageManager;->unmountVolume(Ljava/lang/String;ZZ)V

    const-string v0, "USB format confirm ========>>>start format unmountVolume "

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "unmounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->formatVolume(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Success to format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->mountVolume(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Success to mount ========>>> "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " again"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormatStatus:Landroid/widget/TextView;

    const v1, 0x7f0a00db    # com.konka.tvsettings.R.string.str_pvr_file_system_format_context

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fail to mount "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " again"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fail to format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private startSpeedTest()V
    .locals 5

    const/4 v4, 0x0

    const-string v2, "USB speed ========>>>start test"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00ed    # com.konka.tvsettings.R.string.str_pvr_usb_speed_test_progressing

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/konka/tvsettings/function/PVROptionActivity$5;

    invoke-direct {v3, p0, v0, v1}, Lcom/konka/tvsettings/function/PVROptionActivity$5;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;Landroid/os/Handler;Landroid/app/ProgressDialog;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const v5, 0x7f070180    # com.konka.tvsettings.R.id.pvr_file_system_speed_layout

    const v4, 0x7f07017b    # com.konka.tvsettings.R.id.pvr_file_system_select_disk

    const/16 v3, 0x8

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    iput-object p0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const v2, 0x7f030042    # com.konka.tvsettings.R.layout.pvr_option

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->setContentView(I)V

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectDisk:Landroid/widget/TextView;

    const v2, 0x7f07017c    # com.konka.tvsettings.R.id.pvr_file_system_time_shift_size

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->timeShiftSize:Landroid/widget/TextView;

    const v2, 0x7f07017d    # com.konka.tvsettings.R.id.pvr_file_system_format_layout

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormat:Landroid/widget/LinearLayout;

    const v2, 0x7f07017f    # com.konka.tvsettings.R.id.pvr_file_system_format_context

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormatStatus:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/function/PVROptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->speedCheck:Landroid/widget/LinearLayout;

    const v2, 0x7f070182    # com.konka.tvsettings.R.id.pvr_file_system_speed_context

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->speedCheckResult:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->selectDisk:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setNextFocusUpId(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->speedCheck:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormatStatus:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->speedCheckResult:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    new-instance v2, Lcom/konka/tvsettings/function/PVROptionActivity$2;

    invoke-direct {v2, p0, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$2;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    new-instance v2, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)V

    iput-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->listener:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    invoke-direct {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->initUIListeners()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->registerDiskDetector()V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v2}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onDestroy()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity;->usbReceiver:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/function/PVROptionActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/PVROptionActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/PVROptionActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/function/PVROptionActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onUserInteraction()V

    return-void
.end method
