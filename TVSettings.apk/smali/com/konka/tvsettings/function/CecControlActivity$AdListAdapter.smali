.class Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;
.super Landroid/widget/BaseAdapter;
.source "CecControlActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/function/CecControlActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AdListAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/konka/tvsettings/function/CecControlActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/function/CecControlActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    # getter for: Lcom/konka/tvsettings/function/CecControlActivity;->chooses:[Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/tvsettings/function/CecControlActivity;->access$0(Lcom/konka/tvsettings/function/CecControlActivity;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030006    # com.konka.tvsettings.R.layout.cec_control_list_item

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f070035    # com.konka.tvsettings.R.id.cec_control_text_option

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    # getter for: Lcom/konka/tvsettings/function/CecControlActivity;->chooses:[Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/tvsettings/function/CecControlActivity;->access$0(Lcom/konka/tvsettings/function/CecControlActivity;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method
