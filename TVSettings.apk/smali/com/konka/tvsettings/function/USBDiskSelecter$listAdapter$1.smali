.class Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;
.super Ljava/lang/Thread;
.source "USBDiskSelecter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;

.field private final synthetic val$handler:Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;ILcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;->this$1:Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;

    iput p2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;->val$position:I

    iput-object p3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;->val$handler:Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;->this$1:Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->access$0(Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;)Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverPath:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$2(Lcom/konka/tvsettings/function/USBDiskSelecter;)Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;->val$position:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v2}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v3

    iput v3, v1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockCount()I

    move-result v3

    iput v3, v1, Landroid/os/Message;->arg2:I

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;->val$handler:Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;

    invoke-virtual {v3, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
