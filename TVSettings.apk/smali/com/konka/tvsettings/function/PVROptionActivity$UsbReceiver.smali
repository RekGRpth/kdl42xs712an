.class Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PVROptionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/function/PVROptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/PVROptionActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity;Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;)Lcom/konka/tvsettings/function/PVROptionActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->waitToFormat:Z
    invoke-static {v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$11(Lcom/konka/tvsettings/function/PVROptionActivity;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$6(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$0(Lcom/konka/tvsettings/function/PVROptionActivity;Z)V

    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "PVROptionActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Fail to unmount "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for format"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "PVROptionActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Success to unmount "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for format"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;

    invoke-direct {v5, p0, v2, v1}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;Ljava/lang/String;Landroid/os/Handler;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$6(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectDisk:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$15(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/widget/TextView;

    move-result-object v4

    const v5, 0x7f0a00d8    # com.konka.tvsettings.R.string.str_pvr_file_system_select_disk

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$16(Lcom/konka/tvsettings/function/PVROptionActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
