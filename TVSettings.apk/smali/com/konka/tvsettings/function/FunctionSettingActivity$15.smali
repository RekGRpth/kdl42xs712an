.class Lcom/konka/tvsettings/function/FunctionSettingActivity$15;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "FunctionSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemAlwaysTimeShift()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

.field private final synthetic val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    iput-object p6, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 6

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShift:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v4}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$12(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v4

    if-nez v4, :cond_1

    const/4 v2, 0x0

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FunctionSetting::memc enable[ "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-virtual {v4, v2}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->setAlTimeShift(Z)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v4}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$0(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v4, v5, :cond_0

    if-eqz v2, :cond_2

    new-instance v1, Landroid/content/Intent;

    iget-object v4, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    const-class v5, Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "PVR_AlTimeShift_Call"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v3, v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v3}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybacking()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "========>>>stop PVR TimeShift Playback"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlayback()V

    :cond_3
    const-string v3, "========>>>stop PVR TimeShift"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
