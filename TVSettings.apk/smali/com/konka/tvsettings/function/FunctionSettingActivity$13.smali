.class Lcom/konka/tvsettings/function/FunctionSettingActivity$13;
.super Ljava/lang/Object;
.source "FunctionSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemReset()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->isSystemLock()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "lockType"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    goto :goto_0
.end method
