.class Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;
.super Ljava/util/TimerTask;
.source "ShowAudioActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/screensaver/ShowAudioActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->textView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$0(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$7(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->textView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$0(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$8(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->textView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$0(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getLeft()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$9(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->textView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$0(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getTop()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$10(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$2(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->Y_STEP:I
    invoke-static {v2}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$11(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$10(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$1(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->X_STEP:I
    invoke-static {v2}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$12(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$9(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$1(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->TEXT_WIDTH:I
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$3(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->SCREEN_WIDTH:I
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$13(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    invoke-static {v0, v3}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$14(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$1(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$9(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$2(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->TEXT_HEIGHT:I
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$4(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->SCREEN_HEIGHT:I
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$15(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    invoke-static {v0, v3}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$16(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$2(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$10(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$17(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$1(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    invoke-static {v0, v4}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$14(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$1(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$9(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$2(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    invoke-static {v0, v4}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$16(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$2(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$10(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;I)V

    goto :goto_1
.end method
