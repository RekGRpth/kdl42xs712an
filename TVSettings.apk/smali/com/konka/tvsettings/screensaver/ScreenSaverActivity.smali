.class public Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;
.super Landroid/app/Activity;
.source "ScreenSaverActivity.java"


# static fields
.field private static final REFRESH_POS:I = 0x3e9


# instance fields
.field private COUNT_TIEM:I

.field private SCREEN_HEIGHT:I

.field private SCREEN_WIDTH:I

.field private TEXT_HEIGHT:I

.field private TEXT_WIDTH:I

.field private X_STEP:I

.field private Y_STEP:I

.field private broadcastReceiver:Landroid/content/BroadcastReceiver;

.field private handler:Landroid/os/Handler;

.field private mCountDownTask:Ljava/util/TimerTask;

.field private mCountDownTimer:Ljava/util/Timer;

.field private mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mRefreshTask:Ljava/util/TimerTask;

.field private mRefreshTimer:Ljava/util/Timer;

.field private m_bSignalLock:Z

.field private m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private posX:I

.field private posY:I

.field private textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x2

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTimer:Ljava/util/Timer;

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTimer:Ljava/util/Timer;

    iput v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->X_STEP:I

    iput v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->Y_STEP:I

    const/16 v0, 0x21c

    iput v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->COUNT_TIEM:I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-instance v0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$1;-><init>(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;-><init>(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;-><init>(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTask:Ljava/util/TimerTask;

    new-instance v0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$4;-><init>(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTask:Ljava/util/TimerTask;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posX:I

    return v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posY:I

    return-void
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->Y_STEP:I

    return v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->X_STEP:I

    return v0
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->SCREEN_WIDTH:I

    return v0
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->X_STEP:I

    return-void
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->SCREEN_HEIGHT:I

    return v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->Y_STEP:I

    return-void
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->COUNT_TIEM:I

    return v0
.end method

.method static synthetic access$19(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->COUNT_TIEM:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posY:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->TEXT_WIDTH:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->TEXT_HEIGHT:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->TEXT_WIDTH:I

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->TEXT_HEIGHT:I

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posX:I

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v2, 0x3e8

    const/16 v4, 0x21c

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030048    # com.konka.tvsettings.R.layout.screensaver_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->setContentView(I)V

    const v0, 0x7f0701d5    # com.konka.tvsettings.R.id.screensaver_textview

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->m_bSignalLock:Z

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->m_bSignalLock:Z

    if-eqz v0, :cond_0

    iput v4, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->COUNT_TIEM:I

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;

    const v1, 0x7f0a0122    # com.konka.tvsettings.R.string.str_screensaver_vgaupsupport

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->SCREEN_HEIGHT:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->SCREEN_WIDTH:I

    const-string v0, "DEBUG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "=====screen width: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->SCREEN_WIDTH:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "; =====screen height"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->SCREEN_HEIGHT:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "; Inputsourcce: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "; COUNT_TIEM: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->COUNT_TIEM:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTask:Ljava/util/TimerTask;

    const-wide/16 v4, 0x1f4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTimer:Ljava/util/Timer;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->getStandbyNoSignal()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v4, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTimer:Ljava/util/Timer;

    iget-object v5, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTask:Ljava/util/TimerTask;

    const-wide/16 v6, 0x0

    move-wide v8, v2

    invoke-virtual/range {v4 .. v9}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    :goto_1
    return-void

    :cond_0
    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableMonitor()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x3c

    iput v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->COUNT_TIEM:I

    goto/16 :goto_0

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->COUNT_TIEM:I

    goto/16 :goto_0

    :cond_2
    iput v4, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->COUNT_TIEM:I

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_1
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-string v1, "DEBUG"

    const-string v2, "No Signal Menu On Key Down."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.action.KEYDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "keyCode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "keyevent"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->finish()V

    const/4 v1, 0x1

    return v1
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->finish()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.tv.action.SIGNAL_LOCK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
