.class public Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;
.super Landroid/app/Activity;
.source "ElecPageTagActivity.java"


# instance fields
.field private view:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private getTvSize()I
    .locals 5

    invoke-virtual {p0}, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    move-result-object v0

    iget-object v2, v0, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strSerial:Ljava/lang/String;

    const/4 v3, 0x3

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "DST ====="

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "oo  "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030014    # com.konka.tvsettings.R.layout.elc_page_tag

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->setContentView(I)V

    const v2, 0x7f0700a4    # com.konka.tvsettings.R.id.ele_page_tag

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->view:Landroid/widget/ImageView;

    const-string v1, "/customercfg/StickerDemo/sticker.png"

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->view:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.action.KEYDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "keyCode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "keyevent"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->finish()V

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "Sticker"

    const-string v1, "XXX==========>>>onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->finish()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "Sticker"

    const-string v1, "XXX==========>>>onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/screensaver/ElecPageTagActivity;->finish()V

    return-void
.end method
