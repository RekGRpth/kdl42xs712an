.class public Lcom/konka/tvsettings/time/TimeSettingActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "TimeSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;
    }
.end annotation


# instance fields
.field private final HOURSECOND:I

.field private OffTimetxt:Landroid/widget/TextView;

.field private OnTimetxt:Landroid/widget/TextView;

.field private currentTime:Landroid/widget/TextView;

.field private getTimer:Ljava/lang/Thread;

.field private isDTRUnregisted:Z

.field private itemOffTime:Landroid/widget/LinearLayout;

.field private itemOnTime:Landroid/widget/LinearLayout;

.field private itemSleepTimer:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemTimeClock:Landroid/widget/LinearLayout;

.field private itemTimeZoneSetting:Landroid/widget/LinearLayout;

.field private itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private mDateTimeReceiver:Landroid/content/BroadcastReceiver;

.field private myHandler:Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;

.field private preTimeZoneString:Ljava/lang/String;

.field private tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

.field private timeZoneString:Ljava/lang/String;

.field private timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

.field private timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

.field private tvTimerManager:Lcom/mstar/android/tv/TvTimerManager;

.field private txtTimeZoneOption:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOnTime:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOffTime:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemSleepTimer:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->currentTime:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->OnTimetxt:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->OffTimetxt:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->txtTimeZoneOption:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->tvTimerManager:Lcom/mstar/android/tv/TvTimerManager;

    const v0, 0x36ee80

    iput v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->HOURSECOND:I

    const-string v0, "Asia/Shanghai"

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    const-string v0, "Asia/Shanghai"

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->preTimeZoneString:Ljava/lang/String;

    new-instance v0, Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->myHandler:Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/time/TimeSettingActivity$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/time/TimeSettingActivity$1;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->getTimer:Ljava/lang/Thread;

    new-instance v0, Lcom/konka/tvsettings/time/TimeSettingActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/time/TimeSettingActivity$2;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->isDTRUnregisted:Z

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/time/TimeSettingActivity;)Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->myHandler:Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/time/TimeSettingActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemTimeClock()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/time/TimeSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemSleepTimer:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/time/TimeSettingActivity;)Lcom/mstar/android/tv/TvTimerManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->tvTimerManager:Lcom/mstar/android/tv/TvTimerManager;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/time/TimeSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/time/TimeSettingActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->setTimeZonebyItemIndex(I)V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/time/TimeSettingActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->txtTimeZoneOption:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/time/TimeSettingActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/time/TimeSettingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private addItemOffTime()V
    .locals 4

    const v1, 0x7f070202    # com.konka.tvsettings.R.id.linearlayout_time_offtime

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOffTime:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOffTime:Landroid/widget/LinearLayout;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->OffTimetxt:Landroid/widget/TextView;

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->OffTimetxt:Landroid/widget/TextView;

    const v2, 0x7f0a0005    # com.konka.tvsettings.R.string.common_false_off

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOffTime:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/tvsettings/time/TimeSettingActivity$5;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/time/TimeSettingActivity$5;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->OffTimetxt:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->hour:I

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->minute:I

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private addItemOnTime()V
    .locals 4

    const v1, 0x7f070201    # com.konka.tvsettings.R.id.linearlayout_time_ontime

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOnTime:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOnTime:Landroid/widget/LinearLayout;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->OnTimetxt:Landroid/widget/TextView;

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->OnTimetxt:Landroid/widget/TextView;

    const v2, 0x7f0a0005    # com.konka.tvsettings.R.string.common_false_off

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOnTime:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/tvsettings/time/TimeSettingActivity$4;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/time/TimeSettingActivity$4;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOnTime:Landroid/widget/LinearLayout;

    const v2, 0x7f070204    # com.konka.tvsettings.R.id.linearlayout_time_timezone

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    :cond_0
    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->OnTimetxt:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->hour:I

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->minute:I

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private addItemSleepTimer()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->tvTimerManager:Lcom/mstar/android/tv/TvTimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvTimerManager;->getSleepMode()Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->ordinal()I

    move-result v5

    new-instance v0, Lcom/konka/tvsettings/time/TimeSettingActivity$6;

    const v3, 0x7f070203    # com.konka.tvsettings.R.id.linearlayout_time_sleeptimer

    const v4, 0x7f0b0027    # com.konka.tvsettings.R.array.str_arr_time_sleeptimer_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/time/TimeSettingActivity$6;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemSleepTimer:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemTimeClock()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const v1, 0x7f0701ff    # com.konka.tvsettings.R.id.linearlayout_time_clock

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    const v1, 0x7f070200    # com.konka.tvsettings.R.id.time_menu_time

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->currentTime:Landroid/widget/TextView;

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy/MM/dd  HH:mm"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->currentTime:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/tvsettings/time/TimeSettingActivity$3;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/time/TimeSettingActivity$3;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    const v2, 0x7f070204    # com.konka.tvsettings.R.id.linearlayout_time_timezone

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    goto :goto_0
.end method

.method private addItemTimeZone()V
    .locals 6

    const v3, 0x7f070204    # com.konka.tvsettings.R.id.linearlayout_time_timezone

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getTimeZonebyItemIndex()I

    move-result v5

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->txtTimeZoneOption:Landroid/widget/TextView;

    new-instance v0, Lcom/konka/tvsettings/time/TimeSettingActivity$7;

    const v4, 0x7f0b0028    # com.konka.tvsettings.R.array.str_arr_timezone_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/time/TimeSettingActivity$7;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private getGMTTxt(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getHourOffset(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getMinuteOffset(Ljava/lang/String;)I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GMT"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-ltz v1, :cond_0

    const/16 v3, 0x2b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x3a

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    if-nez v2, :cond_1

    const/16 v3, 0x30

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getHourOffset(Ljava/lang/String;)I
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    const v5, 0x36ee80

    div-int v2, v3, v5

    return v2
.end method

.method private getMinuteOffset(Ljava/lang/String;)I
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    const v5, 0xea60

    div-int v2, v3, v5

    rem-int/lit8 v2, v2, 0x3c

    return v2
.end method

.method private getTimeZonebyItemIndex()I
    .locals 5

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager;->getTimeZone()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_2

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS11_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-gt v3, v4, :cond_2

    const/4 v2, 0x0

    :cond_0
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "*******@@@@@@@@@@@@@ getTimeZonebyItemIndex @@@@@@@@@@@@@@@**********="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return v2

    :cond_1
    :try_start_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_3

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS10_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_4

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS9_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_4

    const/4 v2, 0x2

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_5

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS8_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_5

    const/4 v2, 0x3

    goto :goto_1

    :cond_5
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_6

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS7_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_6

    const/4 v2, 0x4

    goto :goto_1

    :cond_6
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_7

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS6_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_7

    const/4 v2, 0x5

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_8

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_8

    const/4 v2, 0x6

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_9

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS4_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_9

    const/4 v2, 0x7

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_a

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_a

    const/16 v2, 0x8

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_b

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_b

    const/16 v2, 0x9

    goto/16 :goto_1

    :cond_b
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_c

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS2_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_c

    const/16 v2, 0xa

    goto/16 :goto_1

    :cond_c
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_d

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS1_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_d

    const/16 v2, 0xb

    goto/16 :goto_1

    :cond_d
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_e

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_e

    const/16 v2, 0xc

    goto/16 :goto_1

    :cond_e
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_f

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_1_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_f

    const/16 v2, 0xd

    goto/16 :goto_1

    :cond_f
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_10

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_2_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_10

    const/16 v2, 0xe

    goto/16 :goto_1

    :cond_10
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_11

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_11

    const/16 v2, 0xf

    goto/16 :goto_1

    :cond_11
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_12

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_12

    const/16 v2, 0x10

    goto/16 :goto_1

    :cond_12
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_13

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_13

    const/16 v2, 0x11

    goto/16 :goto_1

    :cond_13
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_14

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_14

    const/16 v2, 0x12

    goto/16 :goto_1

    :cond_14
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_15

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_15

    const/16 v2, 0x13

    goto/16 :goto_1

    :cond_15
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_16

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_16

    const/16 v2, 0x14

    goto/16 :goto_1

    :cond_16
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT45_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_17

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT45_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_17

    const/16 v2, 0x15

    goto/16 :goto_1

    :cond_17
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_18

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_18

    const/16 v2, 0x16

    goto/16 :goto_1

    :cond_18
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_19

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_19

    const/16 v2, 0x17

    goto/16 :goto_1

    :cond_19
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_1a

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_7_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_1a

    const/16 v2, 0x18

    goto/16 :goto_1

    :cond_1a
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_1b

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_8_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_1b

    const/16 v2, 0x19

    goto/16 :goto_1

    :cond_1b
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_1c

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_1c

    const/16 v2, 0x1a

    goto/16 :goto_1

    :cond_1c
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_1d

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9POINT5_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_1d

    const/16 v2, 0x1b

    goto/16 :goto_1

    :cond_1d
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_1e

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_1e

    const/16 v2, 0x1c

    goto/16 :goto_1

    :cond_1e
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_1f

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_11_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_1f

    const/16 v2, 0x1d

    goto/16 :goto_1

    :cond_1f
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_12_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_20

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_12_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_20

    const/16 v2, 0x1e

    goto/16 :goto_1

    :cond_20
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_13_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_13_END:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->ordinal()I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    if-gt v3, v4, :cond_0

    const/16 v2, 0x1f

    goto/16 :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method private registerDTReceiver()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "CHANGE_TIME_FROM_DTV"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0, v2, v2}, Lcom/konka/tvsettings/time/TimeSettingActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method private setTimeZonebyItemIndex(I)V
    .locals 5
    .param p1    # I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "*******@@@@@@@@@@@@@@ setTimeZonebyItemIndex @@@@@@@@@@@@@@**********="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n===>>need to set timeZoneString "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    iget-object v2, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->preTimeZoneString:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    iget-object v3, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    return-void

    :pswitch_0
    const-string v2, "Pacific/Midway"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    const-string v2, "Pacific/Honolulu"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    const-string v2, "America/Anchorage"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    const-string v2, "America/Los_Angeles"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    const-string v2, "America/Phoenix"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    const-string v2, "America/Regina"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_6
    const-string v2, "America/Bogota"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_7
    const-string v2, "America/Barbados"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_8
    const-string v2, "America/St_Johns"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_9
    const-string v2, "America/Argentina/Buenos_Aires"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_a
    const-string v2, "Atlantic/South_Georgia"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_b
    const-string v2, "Atlantic/Cape_Verde"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_c
    const-string v2, "Europe/London"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_d
    const-string v2, "Africa/Brazzaville"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_e
    const-string v2, "Africa/Cairo"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_f
    const-string v2, "Europe/Minsk"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto :goto_0

    :pswitch_10
    const-string v2, "Asia/Tehran"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_11
    const-string v2, "Asia/Dubai"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_12
    const-string v2, "Asia/Kabul"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_13
    const-string v2, "Asia/Karachi"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_14
    const-string v2, "Asia/Calcutta"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_15
    const-string v2, "Asia/Katmandu"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_16
    const-string v2, "Asia/Almaty"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_17
    const-string v2, "Asia/Rangoon"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_18
    const-string v2, "Asia/Bangkok"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_19
    const-string v2, "Asia/Shanghai"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1a
    const-string v2, "Asia/Tokyo"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1b
    const-string v2, "Australia/Darwin"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1c
    const-string v2, "Australia/Brisbane"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1d
    const-string v2, "Asia/Vladivostok"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1e
    const-string v2, "Pacific/Majuro"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1f
    const-string v2, "Pacific/Tongatapu"

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    goto/16 :goto_0

    :cond_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n===>>!!!!! set timeZone GMT : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch
.end method

.method private timeUpdated()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "timeUpdated()===>System_Time_Change"

    const-string v2, "android.intent.action.TIME_SET"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private unregisterDTReceiver()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method


# virtual methods
.method public addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemTimeClock()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemOnTime()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemOffTime()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemSleepTimer()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemTimeZone()V

    return-void
.end method

.method public formatTimeField(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030051    # com.konka.tvsettings.R.layout.time_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->setContentView(I)V

    invoke-static {}, Lcom/mstar/android/tv/TvTimerManager;->getInstance()Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->tvTimerManager:Lcom/mstar/android/tv/TvTimerManager;

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->getTimer:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    :cond_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->addView()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->onResume()V

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->myHandler:Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v6, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v2, 0x1

    const v4, 0x7f070204    # com.konka.tvsettings.R.id.linearlayout_time_timezone

    const/4 v5, 0x0

    const-string v3, "onKeyDown"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    :sswitch_0
    return v2

    :sswitch_1
    if-ne v0, v4, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->preTimeZoneString:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->stateRight()V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n===>>!!!!! R GMT is same, move to next,now : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "~~~pre: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->preTimeZoneString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    const-string v3, "Asia/Tehran"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "GMT+4:30"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->stateRight()V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n===>>!!!!! R Tehran for Iran timezone is in DST~, move to next"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    if-ne v0, v4, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->preTimeZoneString:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->stateLeft()V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n===>>!!!!! L GMT is same, move to next,now: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "~~~pre: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->preTimeZoneString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    const-string v3, "Asia/Tehran"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "GMT+4:30"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->stateLeft()V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n===>>!!!!! L Tehran for Iran timezone is in DST~, move to next"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->finish()V

    invoke-virtual {p0, v5, v6}, Lcom/konka/tvsettings/time/TimeSettingActivity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :sswitch_4
    const-string v2, "Exit"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->finish()V

    invoke-virtual {p0, v5, v6}, Lcom/konka/tvsettings/time/TimeSettingActivity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :sswitch_5
    if-ne v0, v4, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOnTime:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemOnTime:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_3
    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimeClock:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_3
        0x14 -> :sswitch_5
        0x15 -> :sswitch_2
        0x16 -> :sswitch_1
        0x52 -> :sswitch_4
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->isDTRUnregisted:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->unregisterDTReceiver()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->isDTRUnregisted:Z

    :cond_0
    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->isDTRUnregisted:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->registerDTReceiver()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/time/TimeSettingActivity;->isDTRUnregisted:Z

    :cond_0
    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040007    # com.konka.tvsettings.R.anim.anim_right_in

    const v1, 0x7f04000a    # com.konka.tvsettings.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method
