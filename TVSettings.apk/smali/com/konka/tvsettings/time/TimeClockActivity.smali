.class public Lcom/konka/tvsettings/time/TimeClockActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "TimeClockActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/time/TimeClockActivity$MyHandler;
    }
.end annotation


# instance fields
.field private DateSettingOption:Landroid/widget/TextView;

.field private DateSettingSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private TimeSettingOption:Landroid/widget/TextView;

.field private TimeSettingSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private currentDay:I

.field private currentHour:I

.field private currentMinute:I

.field private currentMonth:I

.field private currentSecond:I

.field private currentYear:I

.field private epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

.field private isDTRUnregisted:Z

.field private itemAutoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemDateSetting:Landroid/widget/LinearLayout;

.field private itemTimeSetting:Landroid/widget/LinearLayout;

.field private mDateTimeReceiver:Landroid/content/BroadcastReceiver;

.field private m_iAutoTimeSelectIndex:Ljava/lang/Integer;

.field private myHandler:Lcom/konka/tvsettings/time/TimeClockActivity$MyHandler;

.field private nArrCurrent:[Ljava/lang/CharSequence;

.field private settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private tMgr:Lcom/mstar/android/tvapi/common/TimerManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemAutoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemDateSetting:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemTimeSetting:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->DateSettingOption:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->TimeSettingOption:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    iput-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->nArrCurrent:[Ljava/lang/CharSequence;

    iput v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentYear:I

    iput v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentMonth:I

    iput v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentDay:I

    iput v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentHour:I

    iput v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentMinute:I

    iput v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentSecond:I

    new-instance v0, Lcom/konka/tvsettings/time/TimeClockActivity$MyHandler;

    invoke-direct {v0, p0, v2}, Lcom/konka/tvsettings/time/TimeClockActivity$MyHandler;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;Lcom/konka/tvsettings/time/TimeClockActivity$MyHandler;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->myHandler:Lcom/konka/tvsettings/time/TimeClockActivity$MyHandler;

    new-instance v0, Lcom/konka/tvsettings/time/TimeClockActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/time/TimeClockActivity$1;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->isDTRUnregisted:Z

    new-instance v0, Lcom/konka/tvsettings/time/TimeClockActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/time/TimeClockActivity$2;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->DateSettingSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    new-instance v0, Lcom/konka/tvsettings/time/TimeClockActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/time/TimeClockActivity$3;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->TimeSettingSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/time/TimeClockActivity;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/konka/tvsettings/time/TimeClockActivity;->setDate(III)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/time/TimeClockActivity;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/tvsettings/time/TimeClockActivity;->setTime(II)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/time/TimeClockActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemAutoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/time/TimeClockActivity;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/time/TimeClockActivity;)Lcom/konka/kkinterface/tv/SettingDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/time/TimeClockActivity;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/time/TimeClockActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateAutoTimeUI()V

    return-void
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/time/TimeClockActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->showDateDialog()V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/time/TimeClockActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->showTimeDialog()V

    return-void
.end method

.method private addItemAutoMode()V
    .locals 6

    new-instance v0, Lcom/konka/tvsettings/time/TimeClockActivity$4;

    const v3, 0x7f0701f8    # com.konka.tvsettings.R.id.clock_menu_auto_setting

    const v4, 0x7f0b0038    # com.konka.tvsettings.R.array.str_time_setting_mode_option

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/time/TimeClockActivity$4;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemAutoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemDateSetting()V
    .locals 2

    const v0, 0x7f0701fa    # com.konka.tvsettings.R.id.clock_menu_date_setting

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemDateSetting:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemDateSetting:Landroid/widget/LinearLayout;

    const v1, 0x7f0701fc    # com.konka.tvsettings.R.id.clock_menu_time_setting

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemDateSetting:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/time/TimeClockActivity$5;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/time/TimeClockActivity$5;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemTimeSetting()V
    .locals 2

    const v0, 0x7f0701fc    # com.konka.tvsettings.R.id.clock_menu_time_setting

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemTimeSetting:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemTimeSetting:Landroid/widget/LinearLayout;

    const v1, 0x7f0701f8    # com.konka.tvsettings.R.id.clock_menu_auto_setting

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemTimeSetting:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/time/TimeClockActivity$6;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/time/TimeClockActivity$6;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private debug(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private registerDTReceiver()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "CHANGE_TIME_FROM_DTV"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0, v2, v2}, Lcom/konka/tvsettings/time/TimeClockActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method private setDate(III)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4, p1}, Ljava/util/Calendar;->set(II)V

    const/4 v4, 0x2

    invoke-virtual {v0, v4, p2}, Ljava/util/Calendar;->set(II)V

    const/4 v4, 0x5

    invoke-virtual {v0, v4, p3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-string v4, "Trace setTVTime"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "in setDate,Set time to:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput p1, v1, Landroid/text/format/Time;->year:I

    add-int/lit8 v4, p2, 0x1

    iput v4, v1, Landroid/text/format/Time;->month:I

    iput p3, v1, Landroid/text/format/Time;->monthDay:I

    iget v4, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentHour:I

    iput v4, v1, Landroid/text/format/Time;->hour:I

    iget v4, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentMinute:I

    iput v4, v1, Landroid/text/format/Time;->minute:I

    iget v4, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentSecond:I

    iput v4, v1, Landroid/text/format/Time;->second:I

    const-string v4, "TraceSetTVTime"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Try to set ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v1, Landroid/text/format/Time;->year:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->month:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->hour:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->minute:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->second:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->setTVTime(Landroid/text/format/Time;)V

    const-wide/16 v4, 0x3e8

    div-long v4, v2, v4

    return-void
.end method

.method private setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    .locals 3
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private setTVTime(Landroid/text/format/Time;)V
    .locals 4
    .param p1    # Landroid/text/format/Time;

    const-string v1, "Trace setTVTime"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Set time to"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Lcom/mstar/android/tvapi/common/TimerManager;->setClkTime(Landroid/text/format/Time;Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setTime(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v7, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v4, 0xb

    invoke-virtual {v0, v4, p1}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xc

    invoke-virtual {v0, v4, p2}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xd

    invoke-virtual {v0, v4, v7}, Ljava/util/Calendar;->set(II)V

    const/16 v4, 0xe

    invoke-virtual {v0, v4, v7}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-string v4, "Trace setTVTime"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "in setTime,Set time to:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    iput p1, v1, Landroid/text/format/Time;->hour:I

    iput p2, v1, Landroid/text/format/Time;->minute:I

    iput v7, v1, Landroid/text/format/Time;->second:I

    iget v4, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentYear:I

    iput v4, v1, Landroid/text/format/Time;->year:I

    iget v4, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentMonth:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Landroid/text/format/Time;->month:I

    iget v4, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentDay:I

    iput v4, v1, Landroid/text/format/Time;->monthDay:I

    const-string v4, "TraceSetTVTime"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Try to set ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v1, Landroid/text/format/Time;->year:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->month:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->hour:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->minute:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/text/format/Time;->second:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->setTVTime(Landroid/text/format/Time;)V

    const-wide/16 v4, 0x3e8

    div-long v4, v2, v4

    return-void
.end method

.method private showDateDialog()V
    .locals 9

    const/4 v8, 0x0

    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v6}, Landroid/text/format/Time;->setToNow()V

    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    invoke-virtual {v6, v1, v2}, Landroid/text/format/Time;->set(J)V

    const/4 v0, 0x0

    iget v1, v6, Landroid/text/format/Time;->hour:I

    iput v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentHour:I

    iget v1, v6, Landroid/text/format/Time;->minute:I

    iput v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentMinute:I

    iput v8, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentSecond:I

    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->DateSettingSetListener:Landroid/app/DatePickerDialog$OnDateSetListener;

    iget v3, v6, Landroid/text/format/Time;->year:I

    iget v4, v6, Landroid/text/format/Time;->month:I

    iget v5, v6, Landroid/text/format/Time;->monthDay:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    const v1, 0x7f0a014b    # com.konka.tvsettings.R.string.str_time_datesetting

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    move-object v1, v0

    check-cast v1, Landroid/app/DatePickerDialog;

    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/CalendarView;->setVisibility(I)V

    new-instance v1, Lcom/konka/tvsettings/time/TimeClockActivity$7;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/time/TimeClockActivity$7;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    check-cast v0, Landroid/app/DatePickerDialog;

    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    const v2, 0x102033e    # android.R.id.xlarge

    invoke-virtual {v1, v2}, Landroid/widget/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v7

    instance-of v1, v7, Landroid/widget/NumberPicker;

    if-eqz v1, :cond_1

    move-object v1, v7

    check-cast v1, Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    check-cast v7, Landroid/widget/NumberPicker;

    invoke-virtual {v7, v8}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void
.end method

.method private showTimeDialog()V
    .locals 14

    const/4 v6, 0x1

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v7, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Landroid/text/format/Time;->set(J)V

    const/4 v1, 0x0

    iget v2, v7, Landroid/text/format/Time;->year:I

    iput v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentYear:I

    iget v2, v7, Landroid/text/format/Time;->month:I

    iput v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentMonth:I

    iget v2, v7, Landroid/text/format/Time;->monthDay:I

    iput v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->currentDay:I

    new-instance v1, Landroid/app/TimePickerDialog;

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->TimeSettingSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    iget v4, v7, Landroid/text/format/Time;->hour:I

    iget v5, v7, Landroid/text/format/Time;->minute:I

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    const v2, 0x7f0a014c    # com.konka.tvsettings.R.string.str_time_timesetting

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(I)V

    new-instance v2, Lcom/konka/tvsettings/time/TimeClockActivity$8;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/time/TimeClockActivity$8;-><init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const/4 v12, 0x0

    :try_start_0
    const-string v2, "mTimePicker"

    invoke-virtual {v8, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v10, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TimePicker;

    move-object v12, v0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "=========XXXXXXXXXXXXXXXXXXXX========="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    if-eqz v12, :cond_1

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const/4 v13, 0x0

    :try_start_1
    const-string v2, "mHourSpinner"

    invoke-virtual {v11, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v10, v12}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v13

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "=========XXXXXXXXXX 2 XXXXXXXXXX========="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    instance-of v2, v13, Landroid/widget/NumberPicker;

    if-eqz v2, :cond_1

    move-object v2, v13

    check-cast v2, Landroid/widget/NumberPicker;

    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    const-string v2, "=========XXXXXXXXXX 3 XXXXXXXXXX========="

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    check-cast v13, Landroid/widget/NumberPicker;

    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private unregisterDTReceiver()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private updateAutoTimeUI()V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemDateSetting:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    move v3, v1

    :goto_1
    invoke-direct {p0, v4, v3}, Lcom/konka/tvsettings/time/TimeClockActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->itemTimeSetting:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    :goto_2
    invoke-direct {p0, v3, v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private updateClockDateUI(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const v0, 0x7f0701fb    # com.konka.tvsettings.R.id.clock_menu_date_setting_option

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->DateSettingOption:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->DateSettingOption:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateClockTimeUI(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const v0, 0x7f0701fd    # com.konka.tvsettings.R.id.clock_menu_time_setting_option

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->TimeSettingOption:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->TimeSettingOption:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->addItemAutoMode()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->addItemDateSetting()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->addItemTimeSetting()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    :cond_0
    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030050    # com.konka.tvsettings.R.layout.time_clock_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->setContentView(I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->addView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateAutoTimeUI()V

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->myHandler:Lcom/konka/tvsettings/time/TimeClockActivity$MyHandler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v4, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v3, 0x0

    const-string v2, "onKeyDown"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->finish()V

    invoke-virtual {p0, v3, v4}, Lcom/konka/tvsettings/time/TimeClockActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    const-string v2, "Exit"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->finish()V

    invoke-virtual {p0, v3, v4}, Lcom/konka/tvsettings/time/TimeClockActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->isDTRUnregisted:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->unregisterDTReceiver()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->isDTRUnregisted:Z

    :cond_0
    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->isDTRUnregisted:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/TimeClockActivity;->registerDTReceiver()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity;->isDTRUnregisted:Z

    :cond_0
    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040007    # com.konka.tvsettings.R.anim.anim_right_in

    const v1, 0x7f04000a    # com.konka.tvsettings.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method

.method public updateTimeAndDateDisplay(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v8, 0x1

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy-MM-dd"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "HH:mm"

    invoke-direct {v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateClockTimeUI(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateClockDateUI(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v6

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateClockTimeUI(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateClockDateUI(Ljava/lang/String;)V

    goto :goto_0
.end method
