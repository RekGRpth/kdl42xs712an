.class Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;
.super Landroid/os/Handler;
.source "TimeSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/time/TimeSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/time/TimeSettingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/time/TimeSettingActivity;Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;-><init>(Lcom/konka/tvsettings/time/TimeSettingActivity;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x58

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    # invokes: Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemTimeClock()V
    invoke-static {v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->access$1(Lcom/konka/tvsettings/time/TimeSettingActivity;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const/16 v1, 0x65

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$MyHandler;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->finish()V

    :cond_1
    return-void
.end method
