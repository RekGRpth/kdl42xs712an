.class public final enum Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;
.super Ljava/lang/Enum;
.source "MyDataBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/MyDataBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumDVBCModulationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

.field public static final enum E_CAB_INVALID:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

.field public static final enum E_CAB_QAM128:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

.field public static final enum E_CAB_QAM16:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

.field public static final enum E_CAB_QAM256:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

.field public static final enum E_CAB_QAM32:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

.field public static final enum E_CAB_QAM64:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    const-string v1, "E_CAB_QAM16"

    invoke-direct {v0, v1, v3}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM16:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    const-string v1, "E_CAB_QAM32"

    invoke-direct {v0, v1, v4}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM32:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    const-string v1, "E_CAB_QAM64"

    invoke-direct {v0, v1, v5}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM64:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    const-string v1, "E_CAB_QAM128"

    invoke-direct {v0, v1, v6}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM128:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    const-string v1, "E_CAB_QAM256"

    invoke-direct {v0, v1, v7}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM256:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    const-string v1, "E_CAB_INVALID"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_INVALID:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    sget-object v1, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM16:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM32:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM64:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM128:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_QAM256:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->E_CAB_INVALID:Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->ENUM$VALUES:[Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;
    .locals 1

    const-class v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    return-object v0
.end method

.method public static values()[Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->ENUM$VALUES:[Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
