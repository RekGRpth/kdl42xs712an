.class Lcom/konka/tvsettings/RootActivity$CiHandler;
.super Landroid/os/Handler;
.source "RootActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/RootActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CiHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$CiHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const/4 v7, 0x3

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$CiHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v3, p1, Landroid/os/Message;->what:I

    const-string v4, "liying"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "handle message "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_DATA_READY:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v4

    if-eq v3, v4, :cond_1

    sget-object v4, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CLOSEMMI:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_2

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "\n----------EV_CIMMI_UI_CLOSEMMI\n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    :cond_2
    sget-object v4, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_INSERTED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_3

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$CiHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    const v5, 0x7f0a01a3    # com.konka.tvsettings.R.string.str_cimmi_hint_ci_inserted

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_3
    sget-object v4, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_REMOVED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_4

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$CiHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    const v5, 0x7f0a01a4    # com.konka.tvsettings.R.string.str_cimmi_hint_ci_removed

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_4
    sget-object v4, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_AUTOTEST_MESSAGE_SHOWN:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$CiHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    const v5, 0x7f0a01a5    # com.konka.tvsettings.R.string.str_cimmi_hint_ci_auto_test

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
