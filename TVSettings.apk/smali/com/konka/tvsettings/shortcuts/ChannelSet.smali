.class public Lcom/konka/tvsettings/shortcuts/ChannelSet;
.super Landroid/app/Activity;
.source "ChannelSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03004a    # com.konka.tvsettings.R.layout.short_cut_view

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/shortcuts/ChannelSet;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/shortcuts/ChannelSet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0132    # com.konka.tvsettings.R.string.shortcut_set_channel

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/konka/tvsettings/shortcuts/MenuName$TvSetting;

    const-string v2, "menu_channel"

    invoke-static {v1, v2}, Lcom/konka/tvsettings/shortcuts/ShortCutsManager;->creatOpenIntent(Ljava/lang/Class;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const v2, 0x7f020124    # com.konka.tvsettings.R.drawable.shortcut_icon_program

    invoke-static {p0, v0, v1, v2}, Lcom/konka/tvsettings/shortcuts/ShortCutsManager;->addShortCutToLauncher(Landroid/app/Activity;Ljava/lang/String;Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/shortcuts/ChannelSet;->finish()V

    return-void
.end method
