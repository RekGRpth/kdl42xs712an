.class public Lcom/konka/tvsettings/shortcuts/PictureSet;
.super Landroid/app/Activity;
.source "PictureSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03004a    # com.konka.tvsettings.R.layout.short_cut_view

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/shortcuts/PictureSet;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/shortcuts/PictureSet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0133    # com.konka.tvsettings.R.string.shortcut_set_picture

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/konka/tvsettings/shortcuts/MenuName$TvSetting;

    const-string v2, "menu_picture"

    invoke-static {v1, v2}, Lcom/konka/tvsettings/shortcuts/ShortCutsManager;->creatOpenIntent(Ljava/lang/Class;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const v2, 0x7f020123    # com.konka.tvsettings.R.drawable.shortcut_icon_picture_setting

    invoke-static {p0, v0, v1, v2}, Lcom/konka/tvsettings/shortcuts/ShortCutsManager;->addShortCutToLauncher(Landroid/app/Activity;Ljava/lang/String;Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/shortcuts/PictureSet;->finish()V

    return-void
.end method
