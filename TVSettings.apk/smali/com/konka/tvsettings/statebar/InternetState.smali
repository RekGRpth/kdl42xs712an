.class public Lcom/konka/tvsettings/statebar/InternetState;
.super Ljava/lang/Object;
.source "InternetState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;,
        Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/konka/tvsettings/statebar/InternetState;


# instance fields
.field private mActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private mEthernetManager:Landroid/net/ethernet/EthernetManager;

.field private mFilter:Landroid/content/IntentFilter;

.field private mNetBtn:Landroid/widget/ImageButton;

.field private mNetType:I

.field private mNetworkInfo:Landroid/net/NetworkInfo$State;

.field private mReceiver:Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "StateColumn"

    sput-object v0, Lcom/konka/tvsettings/statebar/InternetState;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mEthernetManager:Landroid/net/ethernet/EthernetManager;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0701e9    # com.konka.tvsettings.R.id.imgbtn_wifi

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetBtn:Landroid/widget/ImageButton;

    new-instance v0, Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;

    invoke-direct {v0, p0, v2}, Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;-><init>(Lcom/konka/tvsettings/statebar/InternetState;Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;)V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mReceiver:Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mReceiver:Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/InternetState;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/16 v0, 0x9

    iput v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetType:I

    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetworkInfo:Landroid/net/NetworkInfo$State;

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/InternetState;->DetectNetWorkInfo()V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;-><init>(Lcom/konka/tvsettings/statebar/InternetState;Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private DetectNetWorkInfo()V
    .locals 8

    const/4 v7, 0x1

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    const-string v5, "connectivity"

    invoke-virtual {v3, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v2

    array-length v5, v2

    move v3, v4

    :goto_0
    if-lt v3, v5, :cond_0

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/statebar/InternetState;->changeImg(Z)V

    return-void

    :cond_0
    aget-object v1, v2, v3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    if-ne v7, v6, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v6

    if-eqz v6, :cond_2

    iput v7, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetType:I

    iget v6, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetType:I

    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetworkInfo:Landroid/net/NetworkInfo$State;

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/16 v6, 0x9

    iput v6, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetType:I

    iget v6, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetType:I

    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetworkInfo:Landroid/net/NetworkInfo$State;

    goto :goto_1
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/statebar/InternetState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/statebar/InternetState;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/InternetState;->DetectNetWorkInfo()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/statebar/InternetState;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/InternetState;->changeImg(Z)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/statebar/InternetState;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/InternetState;->onClickNetworkStatusIndicator()V

    return-void
.end method

.method private changeImg(Z)V
    .locals 6
    .param p1    # Z

    const v5, 0x7f070003    # com.konka.tvsettings.R.id.hint_text

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetType:I

    sparse-switch v2, :sswitch_data_0

    :goto_0
    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    return-void

    :sswitch_0
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetworkInfo:Landroid/net/NetworkInfo$State;

    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020150    # com.konka.tvsettings.R.drawable.tv_icon_wifi_s

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020151    # com.konka.tvsettings.R.drawable.tv_icon_wifi_uns

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_2
    iget-object v3, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetBtn:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0131    # com.konka.tvsettings.R.string.str_hint_text_network_wifi

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02015e    # com.konka.tvsettings.R.drawable.tv_icon_wireless_s

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02015f    # com.konka.tvsettings.R.drawable.tv_icon_wireless_uns

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_2

    :sswitch_1
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetworkInfo:Landroid/net/NetworkInfo$State;

    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v2, v3, :cond_2

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/InternetState;->testReachable()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020154    # com.konka.tvsettings.R.drawable.tv_icon_wired2_s

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020155    # com.konka.tvsettings.R.drawable.tv_icon_wired2_uns

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_3
    iget-object v3, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetBtn:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0130    # com.konka.tvsettings.R.string.str_hint_text_network

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020156    # com.konka.tvsettings.R.drawable.tv_icon_wired_s

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020157    # com.konka.tvsettings.R.drawable.tv_icon_wired_uns

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_3

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020152    # com.konka.tvsettings.R.drawable.tv_icon_wired1_s

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020153    # com.konka.tvsettings.R.drawable.tv_icon_wired1_uns

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_3

    :cond_3
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/InternetState;
    .locals 1
    .param p0    # Landroid/app/Activity;

    sget-object v0, Lcom/konka/tvsettings/statebar/InternetState;->mInstance:Lcom/konka/tvsettings/statebar/InternetState;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/tvsettings/statebar/InternetState;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/statebar/InternetState;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/konka/tvsettings/statebar/InternetState;->mInstance:Lcom/konka/tvsettings/statebar/InternetState;

    :cond_0
    sget-object v0, Lcom/konka/tvsettings/statebar/InternetState;->mInstance:Lcom/konka/tvsettings/statebar/InternetState;

    return-object v0
.end method

.method private getIntentFilter()Landroid/content/IntentFilter;
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mFilter:Landroid/content/IntentFilter;

    return-object v0
.end method

.method private onClickNetworkStatusIndicator()V
    .locals 4

    iget v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mNetType:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_0

    const-string v0, "net_lan"

    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.systemsetting.action.SHOW_MENU"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "menu_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    const-string v0, "net_wireless"

    goto :goto_0
.end method

.method private testReachable()Z
    .locals 7

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/InternetState;->mEthernetManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v6}, Landroid/net/ethernet/EthernetManager;->isConfigured()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_0
    return v5

    :cond_0
    iget-object v6, p0, Lcom/konka/tvsettings/statebar/InternetState;->mEthernetManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v6}, Landroid/net/ethernet/EthernetManager;->getSavedConfig()Landroid/net/ethernet/EthernetDevInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/ethernet/EthernetDevInfo;->getRouteAddr()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/ethernet/EthernetDevInfo;->getConnectMode()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v6, "manual"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    const/16 v6, 0x3e8

    invoke-virtual {v2, v6}, Ljava/net/InetAddress;->isReachable(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto :goto_0

    :cond_1
    const/4 v5, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState;->mActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/InternetState;->mReceiver:Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/tvsettings/statebar/InternetState;->mInstance:Lcom/konka/tvsettings/statebar/InternetState;

    return-void
.end method
