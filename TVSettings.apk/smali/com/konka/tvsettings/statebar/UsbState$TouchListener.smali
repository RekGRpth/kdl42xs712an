.class Lcom/konka/tvsettings/statebar/UsbState$TouchListener;
.super Ljava/lang/Object;
.source "UsbState.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/UsbState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/UsbState;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/UsbState;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/UsbState;Lcom/konka/tvsettings/statebar/UsbState$TouchListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;-><init>(Lcom/konka/tvsettings/statebar/UsbState;)V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    return v5

    :pswitch_0
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->bIsHasUsb:Z
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$5(Lcom/konka/tvsettings/statebar/UsbState;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$1(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/widget/ImageButton;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbInSImg:Landroid/graphics/drawable/Drawable;
    invoke-static {v3}, Lcom/konka/tvsettings/statebar/UsbState;->access$6(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$1(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/widget/ImageButton;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbOutSImg:Landroid/graphics/drawable/Drawable;
    invoke-static {v3}, Lcom/konka/tvsettings/statebar/UsbState;->access$7(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->bIsHasUsb:Z
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$5(Lcom/konka/tvsettings/statebar/UsbState;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$1(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/widget/ImageButton;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbInUnSImg:Landroid/graphics/drawable/Drawable;
    invoke-static {v3}, Lcom/konka/tvsettings/statebar/UsbState;->access$2(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$8(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.mm"

    const-string v3, "com.konka.mm.filemanager.FileDiskActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$8(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$8(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$1(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/widget/ImageButton;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbOutUnSImg:Landroid/graphics/drawable/Drawable;
    invoke-static {v3}, Lcom/konka/tvsettings/statebar/UsbState;->access$4(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$8(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;
    invoke-static {v3}, Lcom/konka/tvsettings/statebar/UsbState;->access$8(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0120    # com.konka.tvsettings.R.string.no_usb_device

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x5dc

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
