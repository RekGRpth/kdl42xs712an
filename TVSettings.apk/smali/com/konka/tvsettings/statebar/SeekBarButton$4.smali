.class Lcom/konka/tvsettings/statebar/SeekBarButton$4;
.super Ljava/lang/Object;
.source "SeekBarButton.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/statebar/SeekBarButton;-><init>(Landroid/app/Dialog;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/statebar/SeekBarButton;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$4;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-static {}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->getInstance()Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->translateIRKey(I)I

    move-result p2

    packed-switch p2, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$4;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->decreaseProgress()V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$4;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->doUpdate()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$4;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->increaseProgress()V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$4;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->doUpdate()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
