.class Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;
.super Ljava/lang/Object;
.source "VirtualPadViewHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;-><init>(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v7

    # invokes: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->checkNumOfButton(I)Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$1(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # invokes: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->deletOneNum()V
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$2(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)V

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x11

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$3(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/media/AudioManager;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Landroid/media/AudioManager;->adjustVolume(II)V

    goto :goto_0

    :pswitch_5
    const/16 v3, 0x11

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$3(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/media/AudioManager;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v5, v3}, Landroid/media/AudioManager;->adjustVolume(II)V

    goto :goto_0

    :pswitch_6
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$4(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.konka.tvsettings.action.PROGRAM_UP"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$4(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.konka.tvsettings.action.PROGRAM_DOWN"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v4, ""

    iget-object v5, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$4(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    sget-object v6, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-interface {v4, v5, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$4(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$4(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    const/16 v6, 0x1f4

    invoke-static {v4, v5, v1, v6}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f070225
        :pswitch_6    # com.konka.tvsettings.R.id.virtual_pad_channel_add_text
        :pswitch_7    # com.konka.tvsettings.R.id.virtual_pad_channel_sub_text
        :pswitch_0    # com.konka.tvsettings.R.id.virtual_pad_channel_hint_text
        :pswitch_0    # com.konka.tvsettings.R.id.virtual_pad_edittext
        :pswitch_8    # com.konka.tvsettings.R.id.virtual_pad_go_btn
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_1
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_2
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_3
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_4
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_5
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_6
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_7
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_8
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_9
        :pswitch_2    # com.konka.tvsettings.R.id.virtual_num_c
        :pswitch_1    # com.konka.tvsettings.R.id.virtual_num_0
        :pswitch_3    # com.konka.tvsettings.R.id.virtual_num_back
        :pswitch_0    # com.konka.tvsettings.R.id.virtual_pad_volume_layout
        :pswitch_4    # com.konka.tvsettings.R.id.virtual_pad_volume_add_text
        :pswitch_5    # com.konka.tvsettings.R.id.virtual_pad_volume_sub_text
    .end packed-switch
.end method
