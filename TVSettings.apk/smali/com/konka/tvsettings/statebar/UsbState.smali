.class public Lcom/konka/tvsettings/statebar/UsbState;
.super Ljava/lang/Object;
.source "UsbState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/statebar/UsbState$TouchListener;,
        Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;,
        Lcom/konka/tvsettings/statebar/UsbState$UsbNumsUpdate;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "StateColumn"

.field public static USB_NUMS:Ljava/lang/String;

.field private static instance:Lcom/konka/tvsettings/statebar/UsbState;

.field public static mListener:Lcom/konka/tvsettings/statebar/UsbState$UsbNumsUpdate;


# instance fields
.field private bIsHasUsb:Z

.field public bundle:Landroid/os/Bundle;

.field private mAct:Landroid/app/Activity;

.field private mFilter:Landroid/content/IntentFilter;

.field private mReceiver:Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;

.field private mUsbBtn:Landroid/widget/ImageButton;

.field private mUsbInSImg:Landroid/graphics/drawable/Drawable;

.field private mUsbInUnSImg:Landroid/graphics/drawable/Drawable;

.field private mUsbOutSImg:Landroid/graphics/drawable/Drawable;

.field private mUsbOutUnSImg:Landroid/graphics/drawable/Drawable;

.field private prePartitionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "usb_nums"

    sput-object v0, Lcom/konka/tvsettings/statebar/UsbState;->USB_NUMS:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;)V
    .locals 4
    .param p1    # Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "kk"

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->prePartitionName:Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->bundle:Landroid/os/Bundle;

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    new-instance v0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;

    invoke-direct {v0, p0, v3}, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;-><init>(Lcom/konka/tvsettings/statebar/UsbState;Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;)V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mReceiver:Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/UsbState;->mReceiver:Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/UsbState;->setFilter()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    const v1, 0x7f0701ea    # com.konka.tvsettings.R.id.imgbtn_usb

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02014f    # com.konka.tvsettings.R.drawable.tv_icon_usb_uns

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbInUnSImg:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02014d    # com.konka.tvsettings.R.drawable.tv_icon_usb1_uns

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbOutUnSImg:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02014e    # com.konka.tvsettings.R.drawable.tv_icon_usb_s

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbInSImg:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02014c    # com.konka.tvsettings.R.drawable.tv_icon_usb1_s

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbOutSImg:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/UsbState;->UsbDeviceCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbInUnSImg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->bIsHasUsb:Z

    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;

    invoke-direct {v1, p0, v3}, Lcom/konka/tvsettings/statebar/UsbState$TouchListener;-><init>(Lcom/konka/tvsettings/statebar/UsbState;Lcom/konka/tvsettings/statebar/UsbState$TouchListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbOutUnSImg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->bIsHasUsb:Z

    goto :goto_0
.end method

.method private UsbDeviceCount()I
    .locals 8

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    invoke-static {v6}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/storage/MStorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "stv length==========="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v7, v5

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v6, v5

    if-lt v2, v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "count============"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/UsbState;->bundle:Landroid/os/Bundle;

    sget-object v7, Lcom/konka/tvsettings/statebar/UsbState;->USB_NUMS:Ljava/lang/String;

    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    check-cast v6, Lcom/konka/tvsettings/statebar/UsbState$UsbNumsUpdate;

    iget-object v7, p0, Lcom/konka/tvsettings/statebar/UsbState;->bundle:Landroid/os/Bundle;

    invoke-interface {v6, v7}, Lcom/konka/tvsettings/statebar/UsbState$UsbNumsUpdate;->doUpdate(Landroid/os/Bundle;)V

    const-string v6, "kk"

    iput-object v6, p0, Lcom/konka/tvsettings/statebar/UsbState;->prePartitionName:Ljava/lang/String;

    return v1

    :cond_0
    aget-object v6, v5, v2

    invoke-virtual {v4, v6}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v6, "mounted"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    const-string v6, "not usb device~!"

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    aget-object v6, v5, v2

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/statebar/UsbState;->getPartitionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/UsbState;->prePartitionName:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_3

    add-int/lit8 v1, v1, 0x1

    :cond_3
    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->prePartitionName:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v7, v5, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "============"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/statebar/UsbState;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/statebar/UsbState;->bIsHasUsb:Z

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbInUnSImg:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/statebar/UsbState;)I
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/UsbState;->UsbDeviceCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbOutUnSImg:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/statebar/UsbState;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->bIsHasUsb:Z

    return v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbInSImg:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mUsbOutSImg:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    return-object v0
.end method

.method public static getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/UsbState;
    .locals 1
    .param p0    # Landroid/app/Activity;

    sget-object v0, Lcom/konka/tvsettings/statebar/UsbState;->instance:Lcom/konka/tvsettings/statebar/UsbState;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/tvsettings/statebar/UsbState;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/statebar/UsbState;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/konka/tvsettings/statebar/UsbState;->instance:Lcom/konka/tvsettings/statebar/UsbState;

    :cond_0
    sget-object v0, Lcom/konka/tvsettings/statebar/UsbState;->instance:Lcom/konka/tvsettings/statebar/UsbState;

    return-object v0
.end method

.method private getPartitionName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-lez v0, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private setFilter()Landroid/content/IntentFilter;
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mFilter:Landroid/content/IntentFilter;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState;->mAct:Landroid/app/Activity;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/UsbState;->mReceiver:Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/tvsettings/statebar/UsbState;->instance:Lcom/konka/tvsettings/statebar/UsbState;

    return-void
.end method
