.class Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "InternetState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/InternetState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternetStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/InternetState;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/InternetState;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;->this$0:Lcom/konka/tvsettings/statebar/InternetState;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/InternetState;Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;-><init>(Lcom/konka/tvsettings/statebar/InternetState;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    # getter for: Lcom/konka/tvsettings/statebar/InternetState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/konka/tvsettings/statebar/InternetState;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EthernetManager.ETHERNET_STATE_CHANGED_ACTION"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState$InternetStateReceiver;->this$0:Lcom/konka/tvsettings/statebar/InternetState;

    # invokes: Lcom/konka/tvsettings/statebar/InternetState;->DetectNetWorkInfo()V
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/InternetState;->access$1(Lcom/konka/tvsettings/statebar/InternetState;)V

    return-void
.end method
