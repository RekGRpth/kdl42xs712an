.class public Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "MstarBaseActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/teletext/MstarBaseInterface;


# instance fields
.field protected alwaysTimeout:Z

.field private delayMessage:I

.field private delayMillis:I

.field private intent:Landroid/content/Intent;

.field private isGoingToBeClosed:Z

.field private timerHander:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    const/16 v0, 0x2710

    iput v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMillis:I

    const v0, 0x54c5638

    iput v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMessage:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->alwaysTimeout:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->isGoingToBeClosed:Z

    new-instance v0, Lcom/konka/tvsettings/teletext/MstarBaseActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity$1;-><init>(Lcom/konka/tvsettings/teletext/MstarBaseActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->timerHander:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/teletext/MstarBaseActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMessage:I

    return v0
.end method


# virtual methods
.method protected isGoingToBeClosed(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->isGoingToBeClosed:Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x101

    if-ne p1, v0, :cond_0

    const-string v0, "Mstar Base Activty"

    const-string v1, "temp delte List key"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->timerHander:Landroid/os/Handler;

    iget v1, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMessage:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MstarBaseActivity========>>>onPause="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->isGoingToBeClosed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->isGoingToBeClosed:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->finish()V

    :cond_0
    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    iget-object v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->timerHander:Landroid/os/Handler;

    iget v1, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMessage:I

    iget v2, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMillis:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->isGoingToBeClosed:Z

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStop()V

    return-void
.end method

.method public onTimeOut()V
    .locals 4

    iget-boolean v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->alwaysTimeout:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->timerHander:Landroid/os/Handler;

    iget v1, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMessage:I

    iget v2, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMillis:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method public onUserInteraction()V
    .locals 4

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    iget-object v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->timerHander:Landroid/os/Handler;

    iget v1, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMessage:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->timerHander:Landroid/os/Handler;

    iget v1, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMessage:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->timerHander:Landroid/os/Handler;

    iget v1, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMessage:I

    iget v2, p0, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->delayMillis:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method
