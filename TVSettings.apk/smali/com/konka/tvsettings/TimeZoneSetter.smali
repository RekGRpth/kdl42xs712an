.class public Lcom/konka/tvsettings/TimeZoneSetter;
.super Ljava/lang/Object;
.source "TimeZoneSetter.java"


# instance fields
.field private final HOURSECOND:I

.field private final TAG:Ljava/lang/String;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "TimeZoneSetter"

    iput-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->TAG:Ljava/lang/String;

    const v0, 0x36ee80

    iput v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->HOURSECOND:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    return-void
.end method

.method private getHourOffset()I
    .locals 7

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    const v6, 0x36ee80

    div-int v2, v4, v6

    return v2
.end method

.method private getMinuteOffset()I
    .locals 7

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    const v6, 0xea60

    div-int v3, v4, v6

    rem-int/lit8 v3, v3, 0x3c

    return v3
.end method


# virtual methods
.method public updateTimeZone()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "TimeZoneSetter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "================>>>> getMinuteOffset() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/TimeZoneSetter;->getMinuteOffset()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TimeZoneSetter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "================>>>> getHourOffset() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/TimeZoneSetter;->getHourOffset()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/tvsettings/TimeZoneSetter;->getMinuteOffset()I

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/konka/tvsettings/TimeZoneSetter;->getHourOffset()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "TimeZoneSetter"

    const-string v1, "==============>>>> ooooh~~~~, set time zone fail!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_0_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_1_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_2_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_f
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto :goto_0

    :pswitch_10
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_11
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_12
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_7_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_13
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_8_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_14
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_15
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_16
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_11_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_17
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_12_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_18
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_13_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/konka/tvsettings/TimeZoneSetter;->getMinuteOffset()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/konka/tvsettings/TimeZoneSetter;->getHourOffset()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_19
    const-string v0, "TimeZoneSetter"

    const-string v1, "==============>>>> ooooh~~~~, set time zone fail!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_1a
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_MINUS3_5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_1b
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_3POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_1c
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_4POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_1d
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_1e
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_6POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_1f
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_9POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :pswitch_20
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_10POINT5_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/konka/tvsettings/TimeZoneSetter;->getMinuteOffset()I

    move-result v0

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/TimeZoneSetter;->getHourOffset()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    const-string v0, "TimeZoneSetter"

    const-string v1, "==============>>>> ooooh~~~~, set time zone fail!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_21
    iget-object v0, p0, Lcom/konka/tvsettings/TimeZoneSetter;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_GMT_5POINT45_START:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xb
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x3
        :pswitch_1a
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_19
        :pswitch_19
        :pswitch_1f
        :pswitch_20
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x5
        :pswitch_21
    .end packed-switch
.end method
