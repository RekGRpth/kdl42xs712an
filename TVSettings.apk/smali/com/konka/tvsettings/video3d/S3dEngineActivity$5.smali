.class Lcom/konka/tvsettings/video3d/S3dEngineActivity$5;
.super Ljava/lang/Object;
.source "S3dEngineActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/video3d/S3dEngineActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$5;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$5;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;
    invoke-static {v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$7(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem1;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getCurrentState()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Sequence NOTEXCHANGE"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$5;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->setLRViewSwitch(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "Sequence EXCHANGE"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$5;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->setLRViewSwitch(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)Z

    goto :goto_0
.end method
