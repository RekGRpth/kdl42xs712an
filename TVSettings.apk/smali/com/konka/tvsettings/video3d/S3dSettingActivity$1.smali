.class Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;
.super Landroid/os/Handler;
.source "S3dSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/video3d/S3dSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dSettingActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->access$0(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    # invokes: Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findFocus()V
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->access$2(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Lcom/konka/tvsettings/view/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->requestFocus()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x65

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->finish()V

    goto :goto_0
.end method
