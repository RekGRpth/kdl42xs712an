.class public Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ProgramActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ProgramActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ProgramListAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ProgramInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/konka/tvsettings/popup/ProgramActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/ProgramActivity;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ProgramInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->mData:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v0, 0x0

    new-instance v0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->mContext:Landroid/content/Context;

    const/16 v3, 0x1ce

    const/16 v4, 0x3e

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v5, v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget v6, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;-><init>(Lcom/konka/tvsettings/popup/ProgramActivity;Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->miCurrPos:I
    invoke-static {v1}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$5(Lcom/konka/tvsettings/popup/ProgramActivity;)I

    move-result v1

    if-ne p1, v1, :cond_0

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->setSelBg()V

    :cond_0
    return-object v0
.end method
