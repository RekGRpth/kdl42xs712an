.class public Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;
.super Landroid/widget/RelativeLayout;
.source "ProgramActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ProgramActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HotkeyProgramItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;
    }
.end annotation


# instance fields
.field private final NUM_RIGHT_MARGIN_NOR:I

.field private final NUM_RIGHT_MARGIN_SEL:I

.field private final PROG_LEFT_MARGIN_NOR:I

.field private final PROG_LEFT_MARGIN_SEL:I

.field private final SIZE_NOR:F

.field private final SIZE_SEL:F

.field private final WIDTH_NUM:I

.field private final WIDTH_PROG:I

.field private mContext:Landroid/content/Context;

.field private mPos:I

.field private mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

.field private mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

.field final synthetic this$0:Lcom/konka/tvsettings/popup/ProgramActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/ProgramActivity;Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # I

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/16 v3, 0x50

    iput v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->WIDTH_NUM:I

    const/16 v3, 0x136

    iput v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->WIDTH_PROG:I

    const/16 v3, 0x1e

    iput v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->PROG_LEFT_MARGIN_NOR:I

    const/16 v3, 0x28

    iput v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->PROG_LEFT_MARGIN_SEL:I

    const/16 v3, 0x1e

    iput v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->NUM_RIGHT_MARGIN_NOR:I

    const/16 v3, 0x14

    iput v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->NUM_RIGHT_MARGIN_SEL:I

    const/high16 v3, 0x41900000    # 18.0f

    iput v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->SIZE_NOR:F

    const/high16 v3, 0x41b00000    # 22.0f

    iput v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->SIZE_SEL:F

    iput-object p2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mContext:Landroid/content/Context;

    iput p7, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mPos:I

    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v0, p3, p4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v3, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-direct {v3, p0, p2}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;-><init>(Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    new-instance v3, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-direct {v3, p0, p2}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;-><init>(Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0x136

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v3, 0x1e

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v3, v2}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setGravity(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v3, p5}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    const/high16 v4, 0x41900000    # 18.0f

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setTextSize(F)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setSingleLine()V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setMarqueeRepeatLimit(I)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0x50

    const/4 v4, -0x1

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xb

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x1e

    const/4 v6, 0x0

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v3, v1}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setGravity(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v3, p6}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    const/high16 v4, 0x41900000    # 18.0f

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setTextSize(F)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getPos()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mPos:I

    return v0
.end method

.method public setSelBg()V
    .locals 4

    const/4 v3, -0x1

    const/high16 v2, 0x41b00000    # 22.0f

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v0, v3}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v0, v2}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvProg:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x28

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v0, v3}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v0, v2}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;->mViewTvNum:Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem$MyTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x14

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    return-void
.end method
