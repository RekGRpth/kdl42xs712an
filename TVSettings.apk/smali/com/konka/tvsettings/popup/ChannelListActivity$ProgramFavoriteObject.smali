.class Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;
.super Ljava/lang/Object;
.source "ChannelListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ChannelListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgramFavoriteObject"
.end annotation


# instance fields
.field private channelId:Ljava/lang/String;

.field private channelName:Ljava/lang/String;

.field final synthetic this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/popup/ChannelListActivity;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->channelId:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->channelName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/popup/ChannelListActivity;Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;-><init>(Lcom/konka/tvsettings/popup/ChannelListActivity;)V

    return-void
.end method


# virtual methods
.method public getChannelId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->channelId:Ljava/lang/String;

    return-object v0
.end method

.method public getChannelName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->channelName:Ljava/lang/String;

    return-object v0
.end method

.method public setChannelId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->channelId:Ljava/lang/String;

    return-void
.end method

.method public setChannelName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->channelName:Ljava/lang/String;

    return-void
.end method
