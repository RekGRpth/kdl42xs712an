.class Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;
.super Ljava/lang/Object;
.source "InstallationGuideActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/InstallationGuideActivity;->addItemStartAutoSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    # getter for: Lcom/konka/tvsettings/popup/InstallationGuideActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->access$2(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/SettingDesk;->SetInstallationFlag(Z)Z

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->curCustomer:Ljava/lang/String;

    sget-object v2, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->curCustomer:Ljava/lang/String;

    sget-object v2, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    # invokes: Lcom/konka/tvsettings/popup/InstallationGuideActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->access$3(Lcom/konka/tvsettings/popup/InstallationGuideActivity;Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/konka/tvsettings/channel/Channeltuning;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "installation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->finish()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    # getter for: Lcom/konka/tvsettings/popup/InstallationGuideActivity;->miCurCountry:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->access$4(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)I

    move-result v3

    aget-object v2, v2, v3

    # invokes: Lcom/konka/tvsettings/popup/InstallationGuideActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->access$3(Lcom/konka/tvsettings/popup/InstallationGuideActivity;Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    goto :goto_0
.end method
