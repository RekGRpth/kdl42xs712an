.class Lcom/konka/tvsettings/popup/CheckParentalPwd$2;
.super Ljava/lang/Object;
.source "CheckParentalPwd.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/CheckParentalPwd;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$2;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :cond_0
    :goto_0
    sget-boolean v1, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isCheckPwdWinShown:Z

    if-nez v1, :cond_1

    return-void

    :cond_1
    const-wide/16 v1, 0x1f4

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$2;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$1(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$2;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$1(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    :cond_2
    sget-boolean v1, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$2;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->mHandle:Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$2(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$2;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isTVunlock:I
    invoke-static {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$0(Lcom/konka/tvsettings/popup/CheckParentalPwd;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;->sendEmptyMessage(I)Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
