.class public Lcom/konka/tvsettings/popup/CheckParentalPwd;
.super Landroid/app/Activity;
.source "CheckParentalPwd.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "CommitPrefEdits"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;
    }
.end annotation


# static fields
.field public static final AtvManualTuneLock:I = 0x2

.field public static final AutoTuneLock:I = 0x0

.field public static final DtvManualTuneLock:I = 0x1

.field public static IsCheckPwdInSourceInfo:Z = false

.field public static final ItemLock:I = 0x5

.field public static final ProEditLock:I = 0x6

.field public static final ProLock:I = 0x4

.field private static PwdNo1:I = 0x0

.field private static PwdNo2:I = 0x0

.field public static final RestLock:I = 0x3

.field public static b_isCheckPwdWinShown:Z


# instance fields
.field private CheckIsLockThread:Ljava/lang/Thread;

.field private EditFocusNo:I

.field private b_isTVunlock:I

.field private broadcastReceiver:Landroid/content/BroadcastReceiver;

.field private c1:I

.field private c2:I

.field private c3:I

.field private c4:I

.field private check1:Landroid/widget/EditText;

.field private check2:Landroid/widget/EditText;

.field private check3:Landroid/widget/EditText;

.field private check4:Landroid/widget/EditText;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private intent:Landroid/content/Intent;

.field private lockType:I

.field private mHandle:Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;

.field private m_CancelBtn:Landroid/widget/Button;

.field private rootApp:Lcom/konka/tvsettings/TVRootApp;

.field private rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I

    sput v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I

    sput-boolean v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isCheckPwdWinShown:Z

    sput-boolean v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->EditFocusNo:I

    iput-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isTVunlock:I

    iput-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->CheckIsLockThread:Ljava/lang/Thread;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    new-instance v0, Lcom/konka/tvsettings/popup/CheckParentalPwd$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd$1;-><init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/CheckParentalPwd;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isTVunlock:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method

.method static synthetic access$10(I)V
    .locals 0

    sput p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I

    return-void
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check3:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->c2:I

    return-void
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->c3:I

    return-void
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->checkpsw()V

    return-void
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->c4:I

    return-void
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/popup/CheckParentalPwd;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->mHandle:Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;

    return-object v0
.end method

.method static synthetic access$3()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I

    return v0
.end method

.method static synthetic access$4(I)V
    .locals 0

    sput p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I

    return-void
.end method

.method static synthetic access$5()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I

    return v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check2:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->EditFocusNo:I

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check1:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->c1:I

    return-void
.end method

.method private checkpsw()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v10, 0x0

    iget v11, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->c1:I

    iget v12, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->c2:I

    iget v13, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->c3:I

    iget v14, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->c4:I

    invoke-direct {p0, v11, v12, v13, v14}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->getpassword(IIII)I

    move-result v1

    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    iput-object v11, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->GetParentalPassword()I

    move-result v11

    if-ne v1, v11, :cond_1

    move v7, v9

    :goto_0
    if-nez v7, :cond_0

    const/16 v11, 0x7bc

    if-ne v1, v11, :cond_2

    move v7, v9

    :cond_0
    :goto_1
    if-eqz v7, :cond_3

    iget v11, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    packed-switch v11, :pswitch_data_0

    :goto_2
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    return-void

    :cond_1
    move v7, v10

    goto :goto_0

    :cond_2
    move v7, v10

    goto :goto_1

    :pswitch_0
    iget-object v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    const-class v10, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-virtual {v9, p0, v10}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v9}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    goto :goto_2

    :pswitch_1
    iget-object v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    const-class v10, Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-virtual {v9, p0, v10}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v9}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    goto :goto_2

    :pswitch_2
    iget-object v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    const-class v10, Lcom/konka/tvsettings/channel/Atvmanualtuning;

    invoke-virtual {v9, p0, v10}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v9}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    goto :goto_2

    :pswitch_3
    iget-object v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    const-class v10, Lcom/konka/tvsettings/function/ResetUserSettingInfoDialog;

    invoke-virtual {v9, p0, v10}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v9}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    goto :goto_2

    :pswitch_4
    :try_start_0
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v9

    invoke-interface {v9}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->unlockChannel()Z

    new-instance v4, Landroid/content/Intent;

    const-class v9, Lcom/konka/tvsettings/popup/NoSignalPromptActivity;

    invoke-direct {v4, p0, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v9, "result"

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/16 v9, 0x64

    invoke-virtual {p0, v9, v4}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->setResult(ILandroid/content/Intent;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :pswitch_5
    const-string v11, "menu_check_pwd"

    invoke-virtual {p0, v11, v10}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v10, "pwd_ok"

    invoke-interface {v3, v10, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-class v9, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v4, p0, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    goto/16 :goto_2

    :pswitch_6
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-class v9, Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    invoke-virtual {v6, p0, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    goto/16 :goto_2

    :cond_3
    iget v9, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    packed-switch v9, :pswitch_data_1

    :goto_3
    :pswitch_7
    const-string v9, "CheckParentalPwd"

    const-string v11, "set b_isCheckPwdWinShown is false"

    invoke-static {v9, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v10, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isCheckPwdWinShown:Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->getApplication()Landroid/app/Application;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v11, 0x7f0a0179    # com.konka.tvsettings.R.string.str_wrong_password

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    :pswitch_8
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-class v9, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v4, p0, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    :pswitch_9
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-class v9, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v5, p0, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method private findView()V
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f07005f    # com.konka.tvsettings.R.id.check_pwd1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check1:Landroid/widget/EditText;

    const v0, 0x7f070060    # com.konka.tvsettings.R.id.check_pwd2

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check2:Landroid/widget/EditText;

    const v0, 0x7f070061    # com.konka.tvsettings.R.id.check_pwd3

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check3:Landroid/widget/EditText;

    const v0, 0x7f070062    # com.konka.tvsettings.R.id.check_pwd4

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;

    const v0, 0x7f070063    # com.konka.tvsettings.R.id.popup_password_cancel_btn

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    iget v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    const v1, 0x7f020119    # com.konka.tvsettings.R.drawable.set_program_button_caf

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check1:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check2:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check3:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check1:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check2:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check3:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    return-void
.end method

.method private getpassword(IIII)I
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    mul-int/lit16 v0, p1, 0x3e8

    mul-int/lit8 v1, p2, 0x64

    add-int/2addr v0, v1

    mul-int/lit8 v1, p3, 0xa

    add-int/2addr v0, v1

    add-int/2addr v0, p4

    return v0
.end method

.method private sendKey(ILandroid/view/KeyEvent;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x1000

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/RootActivity$RootMenuHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private setMyEditListener(Landroid/widget/EditText;)V
    .locals 1
    .param p1    # Landroid/widget/EditText;

    new-instance v0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;-><init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v0, Lcom/konka/tvsettings/popup/CheckParentalPwd$4;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd$4;-><init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method


# virtual methods
.method public isTvKeyPadAction(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "Konka Smart TV Keypad"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    const v0, 0x7f03000c    # com.konka.tvsettings.R.layout.check_parental_pwd

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "lockType"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    invoke-virtual {v0}, Lcom/konka/tvsettings/TVRootApp;->getRootmenuHandler()Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    new-instance v0, Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;-><init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->mHandle:Lcom/konka/tvsettings/popup/CheckParentalPwd$MyHandle;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isCheckPwdWinShown:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/popup/CheckParentalPwd$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd$2;-><init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->CheckIsLockThread:Ljava/lang/Thread;

    iget v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->CheckIsLockThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    sput v3, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I

    sput v3, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->findView()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isCheckPwdWinShown:Z

    sput-boolean v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    const-string v0, "DTV lock"

    const-string v1, "Set b_isCheckPwdWinShown as false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, p2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->isTvKeyPadAction(Landroid/view/KeyEvent;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->sendKey(ILandroid/view/KeyEvent;)V

    :cond_0
    :goto_0
    :sswitch_0
    return v3

    :cond_1
    const-string v4, "Check passWord"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "============onkey: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch p1, :sswitch_data_0

    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0

    :sswitch_1
    iget v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    if-eq v4, v8, :cond_0

    sput v7, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I

    sput v7, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I

    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v7, v3}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->overridePendingTransition(II)V

    goto :goto_1

    :sswitch_2
    iget v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    if-eq v4, v8, :cond_0

    sput v7, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I

    sput v7, Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I

    iget v3, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    packed-switch v3, :pswitch_data_0

    :goto_2
    :pswitch_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    goto :goto_1

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v0, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :pswitch_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :sswitch_3
    iget v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    if-eq v4, v8, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :sswitch_4
    iget-object v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->EditFocusNo:I

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check1:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_3
    iget v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->EditFocusNo:I

    if-ne v4, v3, :cond_4

    iget-object v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check2:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_4
    iget v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->EditFocusNo:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check3:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_5
    iget v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->EditFocusNo:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_5
    iget v4, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I

    if-ne v4, v8, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->sendKey(ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_4
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x42 -> :sswitch_0
        0x43 -> :sswitch_5
        0x52 -> :sswitch_2
        0xa6 -> :sswitch_5
        0xa7 -> :sswitch_5
        0xaa -> :sswitch_5
        0xb2 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "PWD"

    const-string v1, "==========>>>onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onResume()V
    .locals 3

    const/4 v1, 0x1

    sput-boolean v1, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isCheckPwdWinShown:Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.tv.action.SIGNAL_UNLOCK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->m_CancelBtn:Landroid/widget/Button;

    new-instance v2, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;-><init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "PWD"

    const-string v1, "==========>>>onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    return-void
.end method
