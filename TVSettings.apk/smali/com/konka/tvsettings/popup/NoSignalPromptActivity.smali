.class public Lcom/konka/tvsettings/popup/NoSignalPromptActivity;
.super Landroid/app/Activity;
.source "NoSignalPromptActivity.java"


# static fields
.field private static final TIME_OUT:I = 0x1


# instance fields
.field private mTextView:Landroid/widget/TextView;

.field private myHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/NoSignalPromptActivity;->mTextView:Landroid/widget/TextView;

    new-instance v0, Lcom/konka/tvsettings/popup/NoSignalPromptActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/NoSignalPromptActivity$1;-><init>(Lcom/konka/tvsettings/popup/NoSignalPromptActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/NoSignalPromptActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/NoSignalPromptActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/NoSignalPromptActivity;->myHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03002f    # com.konka.tvsettings.R.layout.popup_no_signal_prompt_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/NoSignalPromptActivity;->setContentView(I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/popup/NoSignalPromptActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/NoSignalPromptActivity$2;-><init>(Lcom/konka/tvsettings/popup/NoSignalPromptActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/NoSignalPromptActivity;->myHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return v1
.end method
