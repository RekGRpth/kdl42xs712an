.class Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;
.super Ljava/lang/Thread;
.source "PVRFullPageBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayBackProgress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 3

    invoke-super {p0}, Ljava/lang/Thread;->run()V

    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$2(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-wide/16 v1, 0x64

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
