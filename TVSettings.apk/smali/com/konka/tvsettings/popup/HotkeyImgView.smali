.class public Lcom/konka/tvsettings/popup/HotkeyImgView;
.super Landroid/widget/RelativeLayout;
.source "HotkeyImgView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# instance fields
.field private TEXT_NOR_SIZE:F

.field private TEXT_SEL_SIZE:F

.field private imgNorId:I

.field private imgRunId:I

.field private imgSelId:I

.field private mContext:Landroid/content/Context;

.field private mIsRunning:Z

.field private mItemLayout:Landroid/widget/RelativeLayout;

.field private mLayout:Landroid/widget/RelativeLayout;

.field private mPos:I

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIII)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v5, 0x1

    const/4 v3, -0x2

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mIsRunning:Z

    const/high16 v2, 0x41a00000    # 20.0f

    iput v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->TEXT_NOR_SIZE:F

    const/high16 v2, 0x41d00000    # 26.0f

    iput v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->TEXT_SEL_SIZE:F

    iput-object v4, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mLayout:Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mItemLayout:Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mContext:Landroid/content/Context;

    iput p4, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->imgNorId:I

    iput p3, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->imgSelId:I

    iput p5, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->imgRunId:I

    iput p6, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mPos:I

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/HotkeyImgView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x7f03002c    # com.konka.tvsettings.R.layout.popup_img_item

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f07011b    # com.konka.tvsettings.R.id.popup_img_item_container

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mItemLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mItemLayout:Landroid/widget/RelativeLayout;

    iget v3, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->imgNorId:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f07011c    # com.konka.tvsettings.R.id.popup_img_item_textview

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/HotkeyImgView;->addView(Landroid/view/View;)V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/popup/HotkeyImgView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/popup/HotkeyImgView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/popup/HotkeyImgView;->setFocusable(Z)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/popup/HotkeyImgView;->setFocusableInTouchMode(Z)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 0

    return-void
.end method

.method public getPos()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mPos:I

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mIsRunning:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mIsRunning:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/HotkeyImgView;->doUpdate()V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->TEXT_SEL_SIZE:F

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/HotkeyImgView;->setSelBg(F)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mIsRunning:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->TEXT_NOR_SIZE:F

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/HotkeyImgView;->setRunBg(F)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->TEXT_NOR_SIZE:F

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/HotkeyImgView;->setNorBg(F)V

    goto :goto_0
.end method

.method public setNorBg(F)V
    .locals 3
    .param p1    # F

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mItemLayout:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->imgNorId:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method

.method public setRunBg(F)V
    .locals 3
    .param p1    # F

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mItemLayout:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->imgRunId:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f080000    # com.konka.tvsettings.R.color.text_running_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method

.method public setRunning(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mIsRunning:Z

    return-void
.end method

.method public setSelBg(F)V
    .locals 3
    .param p1    # F

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mItemLayout:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->imgSelId:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080001    # com.konka.tvsettings.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method
