.class public Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;
.super Landroid/widget/BaseAdapter;
.source "PVRThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PVRThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbnailAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRThumbnail;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/PVRThumbnail;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;->this$0:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;->this$0:Lcom/konka/tvsettings/popup/PVRThumbnail;

    # getter for: Lcom/konka/tvsettings/popup/PVRThumbnail;->totalNumber:I
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->access$0(Lcom/konka/tvsettings/popup/PVRThumbnail;)I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;->this$0:Lcom/konka/tvsettings/popup/PVRThumbnail;

    # getter for: Lcom/konka/tvsettings/popup/PVRThumbnail;->images:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->access$1(Lcom/konka/tvsettings/popup/PVRThumbnail;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method
