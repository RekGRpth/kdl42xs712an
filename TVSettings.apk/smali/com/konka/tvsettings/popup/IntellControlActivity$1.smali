.class Lcom/konka/tvsettings/popup/IntellControlActivity$1;
.super Landroid/os/Handler;
.source "IntellControlActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/IntellControlActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/IntellControlActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "==========================REFRESH_UI"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I
    invoke-static {v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$0(Lcom/konka/tvsettings/popup/IntellControlActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->showeEonomization(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->miBriProg:I
    invoke-static {v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$1(Lcom/konka/tvsettings/popup/IntellControlActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->showBrightness(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I
    invoke-static {v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$0(Lcom/konka/tvsettings/popup/IntellControlActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->showContrast(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7b
        :pswitch_0
    .end packed-switch
.end method
