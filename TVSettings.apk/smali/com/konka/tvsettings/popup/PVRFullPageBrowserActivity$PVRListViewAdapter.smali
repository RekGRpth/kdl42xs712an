.class public Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "PVRFullPageBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PVRListViewAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->mData:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->mData:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03003e    # com.konka.tvsettings.R.layout.pvr_listview_item

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_recording:I
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$0(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)I

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Z)V

    :goto_0
    const v1, 0x7f070193    # com.konka.tvsettings.R.id.pvr_listview_item_lcn

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->getPvr_text_view_lcn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f070194    # com.konka.tvsettings.R.id.pvr_listview_item_channel

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->getPvr_text_view_channel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f070195    # com.konka.tvsettings.R.id.pvr_listview_item_program

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->getPvr_text_view_program_service()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Z)V

    goto :goto_0
.end method
