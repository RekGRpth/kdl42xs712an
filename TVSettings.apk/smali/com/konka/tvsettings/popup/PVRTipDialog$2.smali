.class Lcom/konka/tvsettings/popup/PVRTipDialog$2;
.super Ljava/lang/Object;
.source "PVRTipDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PVRTipDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PVRTipDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$2;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PvrManager;->setIsBootByRecord(Z)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PictureManager;->disableBacklight()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MUTE_ALL:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableMute(Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    sget-boolean v3, Lcom/konka/tvsettings/popup/PvrActivity;->isPVRActivityActive:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$2;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    # getter for: Lcom/konka/tvsettings/popup/PVRTipDialog;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRTipDialog;->access$1(Lcom/konka/tvsettings/popup/PVRTipDialog;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "jump to pvr activity"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$2;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    # getter for: Lcom/konka/tvsettings/popup/PVRTipDialog;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRTipDialog;->access$1(Lcom/konka/tvsettings/popup/PVRTipDialog;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v3, "isPVRStandby"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$2;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    # getter for: Lcom/konka/tvsettings/popup/PVRTipDialog;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRTipDialog;->access$1(Lcom/konka/tvsettings/popup/PVRTipDialog;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.konka.tv.hotkey.service.UNMUTE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$2;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    # getter for: Lcom/konka/tvsettings/popup/PVRTipDialog;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRTipDialog;->access$1(Lcom/konka/tvsettings/popup/PVRTipDialog;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$2;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/PVRTipDialog;->dismiss()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
