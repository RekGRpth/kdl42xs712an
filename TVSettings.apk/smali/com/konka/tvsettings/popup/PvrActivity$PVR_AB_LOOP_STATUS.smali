.class public final enum Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;
.super Ljava/lang/Enum;
.source "PvrActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PvrActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PVR_AB_LOOP_STATUS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

.field public static final enum E_PVR_AB_LOOP_STATUS_A:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

.field public static final enum E_PVR_AB_LOOP_STATUS_AB:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

.field public static final enum E_PVR_AB_LOOP_STATUS_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    const-string v1, "E_PVR_AB_LOOP_STATUS_NONE"

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    const-string v1, "E_PVR_AB_LOOP_STATUS_A"

    invoke-direct {v0, v1, v3}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_A:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    const-string v1, "E_PVR_AB_LOOP_STATUS_AB"

    invoke-direct {v0, v1, v4}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_AB:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_A:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_AB:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->ENUM$VALUES:[Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;
    .locals 1

    const-class v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    return-object v0
.end method

.method public static values()[Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->ENUM$VALUES:[Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
