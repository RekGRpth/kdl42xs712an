.class Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$2;
.super Ljava/lang/Object;
.source "ChooseOsdLanguageActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$2;->this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$2;->this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->access$2(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v0, v2

    const/16 v2, 0x42

    if-ne p2, v2, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$2;->this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;

    # invokes: Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->updateOsdLanguage(I)V
    invoke-static {v2, v0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->access$3(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$2;->this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->finish()V

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
