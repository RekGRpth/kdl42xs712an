.class Lcom/konka/tvsettings/popup/ChannelControlActivity$1;
.super Landroid/os/Handler;
.source "ChannelControlActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ChannelControlActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/ChannelControlActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->mbIsSwitch:Z
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$0(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->m_cd:Lcom/konka/kkinterface/tv/ChannelDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$1(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$2(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    if-ne v2, v3, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "atv program sel: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$3(Lcom/konka/tvsettings/popup/ChannelControlActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->m_cd:Lcom/konka/kkinterface/tv/ChannelDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$1(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$3(Lcom/konka/tvsettings/popup/ChannelControlActivity;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    sget-object v4, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    :cond_0
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$2(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->m_cd:Lcom/konka/kkinterface/tv/ChannelDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$1(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$3(Lcom/konka/tvsettings/popup/ChannelControlActivity;)I

    move-result v3

    invoke-static {}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->values()[Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    move-result-object v4

    iget-short v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v4, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dtv program sel: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$3(Lcom/konka/tvsettings/popup/ChannelControlActivity;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$4(Lcom/konka/tvsettings/popup/ChannelControlActivity;I)V

    const-string v2, "Channel Change"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelControlActivity;->myHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->access$5(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x1f4

    invoke-static {v2, v3, v0, v4}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelControlActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->finish()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
