.class public Lcom/konka/tvsettings/popup/ChooseCountryActivity;
.super Landroid/app/Activity;
.source "ChooseCountryActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ChooseCountryActivity"

.field private static final m_Countrys:[Ljava/lang/String;

.field private static final m_timeZoneList:[Ljava/lang/String;


# instance fields
.field private country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field private mView:Lcom/konka/tvsettings/view/ChooserView;

.field private m_iCountryIndex:I

.field onKey:Landroid/view/View$OnKeyListener;

.field private sp:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "America/Noronha"

    aput-object v1, v0, v2

    const-string v1, "America/Argentina/Buenos_Aires"

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->m_timeZoneList:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "Brazil"

    aput-object v1, v0, v2

    const-string v1, "Argentina"

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->m_Countrys:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->m_iCountryIndex:I

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;-><init>(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->onKey:Landroid/view/View$OnKeyListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/ChooseCountryActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->updateCountry(I)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->sp:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    return-object v0
.end method

.method private updateCountry(I)V
    .locals 5
    .param p1    # I

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n===>>menu timeZoneString "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->m_timeZoneList:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    sget-object v2, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->m_timeZoneList:[Ljava/lang/String;

    aget-object v2, v2, p1

    iget-object v3, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    return-void

    :pswitch_0
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    goto :goto_0

    :pswitch_1
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARGENTINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    goto :goto_0

    :cond_0
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    sget-object v2, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->m_timeZoneList:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/konka/tvsettings/view/ChooserView;

    const-string v2, "Choose country"

    sget-object v3, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->m_Countrys:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->m_iCountryIndex:I

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->onKey:Landroid/view/View$OnKeyListener;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/view/ChooserView;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;ILandroid/view/View$OnKeyListener;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->mView:Lcom/konka/tvsettings/view/ChooserView;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->mView:Lcom/konka/tvsettings/view/ChooserView;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    const/4 v0, -0x2

    iput v0, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const-string v0, "TVSETTING"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->sp:Landroid/content/SharedPreferences;

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->sp:Landroid/content/SharedPreferences;

    const-string v3, "FIRST_RUN_GUIDE"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/16 v2, 0x52

    if-ne p1, v2, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "currentPage"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->finish()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method
