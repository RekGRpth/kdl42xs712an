.class public Lcom/konka/tvsettings/popup/ProgramActivity;
.super Landroid/app/Activity;
.source "ProgramActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/ProgramActivity$HotkeyProgramItem;,
        Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;,
        Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;,
        Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;,
        Lcom/konka/tvsettings/popup/ProgramActivity$ViewTouchListener;
    }
.end annotation


# instance fields
.field private final IMG_HEIGHT:I

.field private final IMG_WIDTH:I

.field private PROG_COUNT:I

.field private PROG_SIZE:I

.field private RADIO_COUNT:I

.field private adapter:Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field errProgNums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private input:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mViewBtnOk:Landroid/widget/Button;

.field private mViewLvProgList:Landroid/widget/ListView;

.field private meServiceType:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

.field private miCurrPos:I

.field mlistProgList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ProgramInfo;",
            ">;"
        }
    .end annotation
.end field

.field private myHandler:Landroid/os/Handler;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x1ce

    iput v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->IMG_WIDTH:I

    const/16 v0, 0x3e

    iput v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->IMG_HEIGHT:I

    iput v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->miCurrPos:I

    iput v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_COUNT:I

    iput v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->RADIO_COUNT:I

    iput v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_SIZE:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->errProgNums:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/tvsettings/popup/ProgramActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/ProgramActivity$1;-><init>(Lcom/konka/tvsettings/popup/ProgramActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/ProgramActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->miCurrPos:I

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/ProgramActivity;)Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->adapter:Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/ProgramActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/ProgramActivity;)Lcom/konka/kkinterface/tv/ChannelDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/popup/ProgramActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->myHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/popup/ProgramActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->miCurrPos:I

    return v0
.end method


# virtual methods
.method public getFocusIndex()I
    .locals 5

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    iget v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_SIZE:I

    if-lt v1, v3, :cond_0

    return v0

    :cond_0
    iget v4, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v4, v3, :cond_1

    move v0, v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getProgList()V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    iget-short v2, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v3

    int-to-short v3, v3

    if-ne v2, v3, :cond_2

    iget v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_COUNT:I

    :goto_0
    iget v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_COUNT:I

    iget v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->RADIO_COUNT:I

    add-int/2addr v2, v3

    if-lt v0, v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_SIZE:I

    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2, v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_COUNT:I

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2, v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->input:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "input: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->input:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v0, 0x7f030030    # com.konka.tvsettings.R.layout.popup_program_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/ProgramActivity;->setContentView(I)V

    const v0, 0x7f070129    # com.konka.tvsettings.R.id.popup_program_menu_list

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/ProgramActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->input:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_COUNT:I

    sget-object v0, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->meServiceType:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "atv program num: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_COUNT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->errProgNums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ProgramActivity;->getProgList()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ProgramActivity;->getFocusIndex()I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->miCurrPos:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "current program num: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->miCurrPos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ProgramActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;-><init>(Lcom/konka/tvsettings/popup/ProgramActivity;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->adapter:Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->adapter:Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;

    new-instance v1, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;-><init>(Lcom/konka/tvsettings/popup/ProgramActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;

    new-instance v1, Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;-><init>(Lcom/konka/tvsettings/popup/ProgramActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;

    iget v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->miCurrPos:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;

    new-instance v1, Lcom/konka/tvsettings/popup/ProgramActivity$ViewTouchListener;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/ProgramActivity$ViewTouchListener;-><init>(Lcom/konka/tvsettings/popup/ProgramActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->input:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_COUNT:I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->RADIO_COUNT:I

    sget-object v0, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_DTV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->meServiceType:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dtv program num: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/ProgramActivity;->PROG_COUNT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
