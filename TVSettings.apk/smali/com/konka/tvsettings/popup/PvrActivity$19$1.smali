.class Lcom/konka/tvsettings/popup/PvrActivity$19$1;
.super Ljava/lang/Object;
.source "PvrActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PvrActivity$19;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/popup/PvrActivity$19;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity$19;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$19$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$19;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$19$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$19;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$19;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity$19;->access$0(Lcom/konka/tvsettings/popup/PvrActivity$19;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity;->access$39(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/PvrImageFlag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrImageFlag;->setRecorderFlag(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$19$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$19;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$19;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity$19;->access$0(Lcom/konka/tvsettings/popup/PvrActivity$19;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->dismissDialog(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PVR isBootedByRecord and now is timeup record ==========>>>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$19$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$19;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$19;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$19;->access$0(Lcom/konka/tvsettings/popup/PvrActivity$19;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$36(Lcom/konka/tvsettings/popup/PvrActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$19$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$19;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$19;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity$19;->access$0(Lcom/konka/tvsettings/popup/PvrActivity$19;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v0

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity;->access$36(Lcom/konka/tvsettings/popup/PvrActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$19$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$19;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$19;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity$19;->access$0(Lcom/konka/tvsettings/popup/PvrActivity$19;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v0

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity;->access$37(Lcom/konka/tvsettings/popup/PvrActivity;)V

    :cond_0
    const-string v0, "PvrActivity=====>>>finish 3 \n"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$19$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$19;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$19;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity$19;->access$0(Lcom/konka/tvsettings/popup/PvrActivity$19;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    return-void
.end method
