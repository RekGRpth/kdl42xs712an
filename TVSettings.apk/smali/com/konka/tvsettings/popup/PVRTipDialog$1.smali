.class Lcom/konka/tvsettings/popup/PVRTipDialog$1;
.super Ljava/lang/Object;
.source "PVRTipDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PVRTipDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PVRTipDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$1;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$1;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    # invokes: Lcom/konka/tvsettings/popup/PVRTipDialog;->saveAndExit()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRTipDialog;->access$0(Lcom/konka/tvsettings/popup/PVRTipDialog;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->stopRecord()V

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    const-string v2, "pvr"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvCommonManager;->standbySystem(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRTipDialog$1;->this$0:Lcom/konka/tvsettings/popup/PVRTipDialog;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PVRTipDialog;->dismiss()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
