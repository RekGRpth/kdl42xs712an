.class public Lcom/konka/tvsettings/popup/ChannelControlActivity;
.super Landroid/app/Activity;
.source "ChannelControlActivity.java"


# instance fields
.field private final DELAY_TIME:I

.field private final MSG_SWITCH_CHANNEL:I

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mTvChannelInfoPanel:Landroid/widget/RelativeLayout;

.field private mViewTvChannelInfo:Landroid/widget/TextView;

.field private m_cd:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private mbIsSwitch:Z

.field private miChannelNum:I

.field private myHandler:Landroid/os/Handler;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->MSG_SWITCH_CHANNEL:I

    const/16 v0, 0xfa0

    iput v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->DELAY_TIME:I

    iput v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    iput-boolean v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mbIsSwitch:Z

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->timer:Ljava/util/Timer;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-instance v0, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/ChannelControlActivity$1;-><init>(Lcom/konka/tvsettings/popup/ChannelControlActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method private ChannelTimerTask()Ljava/util/TimerTask;
    .locals 1

    new-instance v0, Lcom/konka/tvsettings/popup/ChannelControlActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/ChannelControlActivity$2;-><init>(Lcom/konka/tvsettings/popup/ChannelControlActivity;)V

    return-object v0
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mbIsSwitch:Z

    return v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Lcom/konka/kkinterface/tv/ChannelDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->m_cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/ChannelControlActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/popup/ChannelControlActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    return-void
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/popup/ChannelControlActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->myHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getInputNum(I)V
    .locals 6
    .param p1    # I

    const/16 v5, 0x3e8

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    add-int/lit8 v0, p1, -0x7

    iget v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    mul-int/lit8 v1, v1, 0xa

    if-ge v1, v5, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->timer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->timer:Ljava/util/Timer;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "num: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    mul-int/lit8 v1, v1, 0xa

    add-int/2addr v1, v0

    iput v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    iget v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    if-gtz v1, :cond_1

    const-string v1, "channel num is 0"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mTvChannelInfoPanel:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mTvChannelInfoPanel:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mbIsSwitch:Z

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->timer:Ljava/util/Timer;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->ChannelTimerTask()Ljava/util/TimerTask;

    move-result-object v2

    const-wide/16 v3, 0xfa0

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    iget v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    if-ge v1, v5, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mViewTvChannelInfo:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    const v2, 0x7f03002b    # com.konka.tvsettings.R.layout.popup_channel_info_menu

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->setContentView(I)V

    const v2, 0x7f07011a    # com.konka.tvsettings.R.id.popup_channel_info_menu_tv_num

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mViewTvChannelInfo:Landroid/widget/TextView;

    const v2, 0x7f070119    # com.konka.tvsettings.R.id.popup_channel_info_menu_no_dtv_info

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mTvChannelInfoPanel:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->m_cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ChannelInfo"

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->getInputNum(I)V

    iget v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->mViewTvChannelInfo:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->miChannelNum:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->getInputNum(I)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x4

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->timer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelControlActivity;->finish()V

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x42

    if-eq v2, p1, :cond_1

    const/16 v2, 0xb2

    if-ne v2, p1, :cond_2

    :cond_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelControlActivity;->myHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method
