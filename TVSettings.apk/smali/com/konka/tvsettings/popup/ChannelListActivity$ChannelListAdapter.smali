.class public Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ChannelListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ChannelListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ChannelListAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/ChannelListActivity;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;->mData:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;->mData:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030037    # com.konka.tvsettings.R.layout.program_favorite_list_item

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f070159    # com.konka.tvsettings.R.id.program_favorite_edit_number

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->getChannelId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f07015a    # com.konka.tvsettings.R.id.program_favorite_edit_data

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->getChannelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method
