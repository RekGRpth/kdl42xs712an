.class Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;
.super Landroid/os/Handler;
.source "DTVManualTuning.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/channel/DTVManualTuning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/DTVManualTuning;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    iget v9, p1, Landroid/os/Message;->what:I

    sget-object v10, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v10

    if-ne v9, v10, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v10, "dtvSrvCount"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v10, "radioSrvCount"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v10, "dataSrvCount"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v10, "quality"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v10, "quality"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v10, "scanstatus"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$0(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v10

    iget-object v10, v10, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_dtv_val:Landroid/widget/TextView;

    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$0(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v10

    iget-object v10, v10, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_radio_val:Landroid/widget/TextView;

    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$0(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v10

    iget-object v10, v10, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_data_val:Landroid/widget/TextView;

    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    div-int/lit8 v11, v3, 0xa

    # invokes: Lcom/konka/tvsettings/channel/DTVManualTuning;->setProgressValueForSignalQuality(I)V
    invoke-static {v10, v11}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$1(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    div-int/lit8 v11, v7, 0xa

    # invokes: Lcom/konka/tvsettings/channel/DTVManualTuning;->setProgressValueForSignalStrengh(I)V
    invoke-static {v10, v11}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$2(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V

    sget-object v10, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;->STATUS_SCAN_END:Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;->ordinal()I

    move-result v10

    if-ne v5, v10, :cond_0

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z
    invoke-static {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$3(Lcom/konka/tvsettings/channel/DTVManualTuning;)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "TvApp"

    const-string v11, "scan_status->STATUS_SCAN_END"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z
    invoke-static {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$3(Lcom/konka/tvsettings/channel/DTVManualTuning;)Z

    move-result v10

    if-eqz v10, :cond_0

    add-int v10, v2, v4

    add-int/2addr v10, v1

    if-lez v10, :cond_1

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-virtual {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f0a006e    # com.konka.tvsettings.R.string.str_cha_dtvmanualtuning_warning

    invoke-static {v10, v11}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    iget-object v10, v10, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v8

    sget-object v10, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v8, v10, v11}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    :goto_0
    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$5(Lcom/konka/tvsettings/channel/DTVManualTuning;Z)V

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # invokes: Lcom/konka/tvsettings/channel/DTVManualTuning;->updatedtvManualtuningComponents()V
    invoke-static {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$6(Lcom/konka/tvsettings/channel/DTVManualTuning;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :cond_1
    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->bDvbcMode:Z
    invoke-static {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$4(Lcom/konka/tvsettings/channel/DTVManualTuning;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-virtual {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    iget-object v11, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-virtual {v11}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a01b1    # com.konka.tvsettings.R.string.str_cha_dtvcmanualtuning_return_warning

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/konka/tvsettings/common/SmartToast;->SetSmartToast(Landroid/content/Context;Ljava/lang/String;)V

    :goto_1
    const/16 v10, 0xfa0

    invoke-static {v10}, Lcom/konka/tvsettings/common/SmartToast;->show(I)V

    goto :goto_0

    :cond_2
    iget-object v10, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-virtual {v10}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    iget-object v11, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-virtual {v11}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a006f    # com.konka.tvsettings.R.string.str_cha_dtvmanualtuning_return_warning

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/konka/tvsettings/common/SmartToast;->SetSmartToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method
