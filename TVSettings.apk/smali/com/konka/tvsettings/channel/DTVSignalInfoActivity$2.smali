.class Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$2;
.super Ljava/lang/Object;
.source "DTVSignalInfoActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$2;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :goto_0
    # getter for: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->enableEpgDataThread:Z
    invoke-static {}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$3()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-wide/16 v1, 0x1f4

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$2;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    # getter for: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->dtvSignalHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$4(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    const-wide/16 v3, 0x14

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
