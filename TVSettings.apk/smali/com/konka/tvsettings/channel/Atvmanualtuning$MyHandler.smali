.class Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;
.super Landroid/os/Handler;
.source "Atvmanualtuning.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/channel/Atvmanualtuning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/Atvmanualtuning;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/Atvmanualtuning;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    iget v10, p1, Landroid/os/Message;->what:I

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v11

    if-ne v10, v11, :cond_2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v11, "frequency"

    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v11, "percent"

    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    div-int/lit16 v6, v4, 0x3e8

    rem-int/lit16 v11, v4, 0x3e8

    div-int/lit8 v5, v11, 0xa

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v11, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    # getter for: Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v11}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->access$0(Lcom/konka/tvsettings/channel/Atvmanualtuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v11

    iget-object v11, v11, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_freqency_val:Landroid/widget/TextView;

    invoke-virtual {v11, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v11, 0x64

    if-lt v7, v11, :cond_2

    iget-object v11, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    iget-object v11, v11, Lcom/konka/tvsettings/channel/Atvmanualtuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v11}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v9

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningEnd()V

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v9, v11}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v0

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentChannelNumber()I

    move-result v3

    if-eqz v0, :cond_0

    const/16 v11, 0xff

    if-ne v3, v11, :cond_1

    :cond_0
    const/4 v3, 0x0

    :cond_1
    invoke-interface {v9, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->saveAtvProgram(I)Z

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentChannelNumber()I

    move-result v2

    int-to-short v11, v2

    const/4 v12, 0x1

    invoke-interface {v9, v11, v12}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetChannel(SZ)I

    iget-object v11, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    # invokes: Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningComponents()V
    invoke-static {v11}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->access$1(Lcom/konka/tvsettings/channel/Atvmanualtuning;)V

    :cond_2
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void
.end method
