.class public Lcom/konka/tvsettings/channel/ProgramEditAdapter;
.super Landroid/widget/BaseAdapter;
.source "ProgramEditAdapter.java"


# instance fields
.field private activity:Landroid/content/Context;

.field private mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/channel/ProgramListViewItemObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/channel/ProgramListViewItemObject;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->mData:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->activity:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v9, p0, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->activity:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    const v10, 0x7f030039    # com.konka.tvsettings.R.layout.program_list_view_item

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v9, 0x7f070163    # com.konka.tvsettings.R.id.program_edit_number

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iget-object v9, p0, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->mData:Ljava/util/ArrayList;

    invoke-virtual {v9, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->getTvNumber()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v9, 0x7f070164    # com.konka.tvsettings.R.id.program_edit_data

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->getTvName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v9, 0x7f070166    # com.konka.tvsettings.R.id.program_edit_favorite_img

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v9, 0x7f020077    # com.konka.tvsettings.R.drawable.list_menu_img_favorite_focus

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isFavoriteImg()Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    const v9, 0x7f07016a    # com.konka.tvsettings.R.id.program_edit_mov_img

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v9, 0x7f02007b    # com.konka.tvsettings.R.drawable.list_menu_img_hint_m

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isMoveImg()Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    const v9, 0x7f070169    # com.konka.tvsettings.R.id.program_edit_ssl_img

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    const v9, 0x7f020081    # com.konka.tvsettings.R.drawable.list_menu_img_ssl_focus

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSslImg()Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    const v9, 0x7f070167    # com.konka.tvsettings.R.id.program_edit_skip_img

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    const v9, 0x7f020080    # com.konka.tvsettings.R.drawable.list_menu_img_skip_focus

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSkipImg()Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_3
    const v9, 0x7f070168    # com.konka.tvsettings.R.id.program_edit_lock_img

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v9, 0x7f02007e    # com.konka.tvsettings.R.drawable.list_menu_img_lock_focus

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isLockImg()Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_4
    const v9, 0x7f070165    # com.konka.tvsettings.R.id.program_edit_source_img

    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSourceImg()Z

    move-result v9

    if-eqz v9, :cond_5

    const v9, 0x7f020074    # com.konka.tvsettings.R.drawable.list_menu_img_atv_foucus

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_5
    return-object p2

    :cond_0
    const/16 v9, 0x8

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_1
    const/16 v9, 0x8

    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    const/16 v9, 0x8

    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_3
    const/16 v9, 0x8

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    :cond_4
    const/16 v9, 0x8

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    :cond_5
    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->getServiceType()S

    move-result v9

    sget-object v10, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_RADIO:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v10

    if-ne v9, v10, :cond_6

    const v9, 0x7f02007f    # com.konka.tvsettings.R.drawable.list_menu_img_radio_foucus

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5

    :cond_6
    const v9, 0x7f020076    # com.konka.tvsettings.R.drawable.list_menu_img_dtv_foucus

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v9, 0x0

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_5
.end method
