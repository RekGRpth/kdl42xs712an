.class Lcom/konka/tvsettings/channel/ProgramListViewActivity$2;
.super Ljava/lang/Object;
.source "ProgramListViewActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/ProgramListViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$2;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$2;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$2;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->flag:Z
    invoke-static {v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$1(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSourceImg()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$2;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgMove:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$2(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$2;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgMove:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$2(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
