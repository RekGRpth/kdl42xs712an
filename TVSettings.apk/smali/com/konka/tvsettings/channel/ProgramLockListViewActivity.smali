.class public Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "ProgramLockListViewActivity.java"


# instance fields
.field private adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

.field cd:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private currutPage:I

.field private m_nServiceNum:I

.field private moveFlag:Z

.field private moveble:Z

.field private pageSize:I

.field private plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

.field private plvios:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/channel/ProgramListViewItemObject;",
            ">;"
        }
    .end annotation
.end field

.field private position:I

.field private proListView:Landroid/widget/ListView;

.field private progInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ProgramInfo;",
            ">;"
        }
    .end annotation
.end field

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-direct {v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveble:Z

    const/16 v0, 0xa

    iput v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->pageSize:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    iput v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->m_nServiceNum:I

    iput-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z

    return v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Lcom/konka/tvsettings/channel/ProgramEditAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveble:Z

    return-void
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveble:Z

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->position:I

    return v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Lcom/konka/tvsettings/channel/ProgramListViewItemObject;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->position:I

    return-void
.end method

.method private addOneListViewItem(Lcom/mstar/android/tvapi/common/vo/ProgramInfo;)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v1, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-direct {v1}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;-><init>()V

    iget-object v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvName(Ljava/lang/String;)V

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvNumber(Ljava/lang/String;)V

    :goto_0
    iget-boolean v0, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setLockImg(Z)V

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSourceImg(Z)V

    :goto_1
    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvNumber(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSourceImg(Z)V

    goto :goto_1
.end method

.method private getProgList()V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->m_nServiceNum:I

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->m_nServiceNum:I

    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2, v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-boolean v2, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isDelete:Z

    if-nez v2, :cond_1

    iget-boolean v2, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isVisible:Z

    if-nez v2, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->addOneListViewItem(Lcom/mstar/android/tvapi/common/vo/ProgramInfo;)V

    goto :goto_1
.end method

.method private getfocusIndex()I
    .locals 5

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    return v1

    :cond_0
    iget v4, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v4, v3, :cond_1

    iget-short v4, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-short v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v4, v3, :cond_1

    move v1, v2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V
    .locals 1
    .param p1    # Lcom/konka/tvsettings/channel/ProgramListViewItemObject;
    .param p2    # Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-virtual {p2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSourceImg()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSourceImg(Z)V

    return-void
.end method


# virtual methods
.method checkChmoveble(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v3, 0x14

    if-ne p1, v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt p2, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    add-int/lit8 v4, p2, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    :cond_2
    :goto_1
    iget-short v3, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-short v4, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/16 v3, 0x13

    if-ne p1, v3, :cond_2

    if-eqz p2, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;

    add-int/lit8 v4, p2, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03003a    # com.konka.tvsettings.R.layout.program_lock_list_view

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->setContentView(I)V

    const v1, 0x7f07015c    # com.konka.tvsettings.R.id.program_edit_list_view

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->getProgList()V

    new-instance v1, Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->getfocusIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    new-instance v2, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;-><init>(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x15

    if-ne p1, v3, :cond_1

    iget v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    iput-boolean v6, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z

    iget v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    iget v4, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->pageSize:I

    mul-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    const/16 v3, 0x16

    if-ne p1, v3, :cond_2

    iget v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    iget-object v4, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->pageSize:I

    div-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    if-ge v3, v4, :cond_0

    iput-boolean v6, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z

    iget v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    iget v4, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->currutPage:I

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->pageSize:I

    mul-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_2
    const/16 v3, 0x13

    if-ne p1, v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_3
    const/16 v3, 0x14

    if-ne p1, v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_4
    const/16 v3, 0x52

    if-ne p1, v3, :cond_5

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v0, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->finish()V

    :cond_5
    const/4 v3, 0x4

    if-ne p1, v3, :cond_6

    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->finish()V

    :cond_6
    const/16 v3, 0xb2

    if-eq p1, v3, :cond_0

    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public splitString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I

    if-nez p1, :cond_1

    const-string v3, ""

    :cond_0
    return-object v3

    :cond_1
    const/4 v2, 0x0

    const-string v3, ""

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v4, v0

    add-int/2addr v2, v4

    if-gt v2, p2, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
