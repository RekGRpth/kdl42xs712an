.class Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;
.super Ljava/lang/Object;
.source "ProgramListViewActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/ProgramListViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 19
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$3(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v18, v0

    const/16 v2, 0x13

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$4(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/16 v2, 0x14

    move/from16 v0, p2

    if-ne v0, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$4(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    move/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->checkChmoveble(II)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$5(Lcom/konka/tvsettings/channel/ProgramListViewActivity;Z)V

    :cond_2
    const/16 v2, 0x13

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$4(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_4

    :cond_3
    const/16 v2, 0x14

    move/from16 v0, p2

    if-ne v0, v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveble:Z
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$6(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_9

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$4(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$7(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_7

    :cond_5
    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$5(Lcom/konka/tvsettings/channel/ProgramListViewActivity;Z)V

    const/4 v2, 0x1

    goto :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I
    invoke-static {v3}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$7(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    # invokes: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V
    invoke-static {v4, v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$8(Lcom/konka/tvsettings/channel/ProgramListViewActivity;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;
    invoke-static {v4}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$9(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    move-result-object v4

    # invokes: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V
    invoke-static {v3, v2, v4}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$8(Lcom/konka/tvsettings/channel/ProgramListViewActivity;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    move/from16 v0, v18

    invoke-static {v2, v0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$10(Lcom/konka/tvsettings/channel/ProgramListViewActivity;I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$11(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->notifyDataSetChanged()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$3(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidate()V

    const/4 v2, 0x1

    goto :goto_0

    :cond_8
    const/4 v2, 0x1

    goto :goto_0

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$4(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_a
    const/16 v2, 0x103

    move/from16 v0, p2

    if-ne v0, v2, :cond_13

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_b

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-boolean v7, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    if-eqz v7, :cond_e

    if-eqz v7, :cond_c

    const/4 v7, 0x0

    :goto_1
    iput-boolean v7, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->E_SKIP:Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    iget v4, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v5, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    const/4 v6, 0x0

    invoke-interface/range {v2 .. v7}, Lcom/konka/kkinterface/tv/ChannelDesk;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_d

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_c
    const/4 v7, 0x1

    goto :goto_1

    :cond_d
    if-eqz v7, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSkipImg(Z)V

    :cond_e
    :goto_2
    iget-short v15, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->favorite:S

    if-nez v15, :cond_10

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_1:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    iget v4, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v5, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->addProgramToFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V

    :goto_3
    iput-short v15, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->favorite:S

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_11

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSkipImg(Z)V

    goto :goto_2

    :cond_10
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_1:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    iget v4, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v5, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->deleteProgramFromFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V

    goto :goto_3

    :cond_11
    if-eqz v15, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setFavoriteImg(Z)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$3(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v3

    long-to-int v3, v3

    invoke-static {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$10(Lcom/konka/tvsettings/channel/ProgramListViewActivity;I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$11(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->notifyDataSetChanged()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$3(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidate()V

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setFavoriteImg(Z)V

    goto :goto_4

    :cond_13
    const/16 v2, 0x28

    move/from16 v0, p2

    if-ne v0, v2, :cond_16

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_14

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-boolean v13, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    if-eqz v13, :cond_15

    const/4 v13, 0x0

    :goto_5
    iput-boolean v13, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v8, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->E_LOCK:Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    iget v10, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v11, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    const/4 v12, 0x0

    invoke-interface/range {v8 .. v13}, Lcom/konka/kkinterface/tv/ChannelDesk;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$11(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->notifyDataSetChanged()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$3(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidate()V

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_15
    const/4 v13, 0x1

    goto :goto_5

    :cond_16
    const/16 v2, 0xba

    move/from16 v0, p2

    if-ne v0, v2, :cond_1e

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_17

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-short v15, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->favorite:S

    const/4 v2, 0x1

    if-ne v15, v2, :cond_18

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_1:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    iget v4, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v5, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->deleteProgramFromFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V

    :cond_18
    iput-short v15, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->favorite:S

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_19

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_19
    if-eqz v15, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setFavoriteImg(Z)V

    :goto_6
    iget-boolean v7, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    if-eqz v7, :cond_1b

    const/4 v7, 0x0

    :goto_7
    iput-boolean v7, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->E_SKIP:Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    iget v4, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v5, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    const/4 v6, 0x0

    invoke-interface/range {v2 .. v7}, Lcom/konka/kkinterface/tv/ChannelDesk;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_1c

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setFavoriteImg(Z)V

    goto :goto_6

    :cond_1b
    const/4 v7, 0x1

    goto :goto_7

    :cond_1c
    if-eqz v7, :cond_1d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSkipImg(Z)V

    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$11(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->notifyDataSetChanged()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$3(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidate()V

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSkipImg(Z)V

    goto :goto_8

    :cond_1e
    const/16 v2, 0x42

    move/from16 v0, p2

    if-ne v0, v2, :cond_22

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v18

    if-lt v0, v2, :cond_1f

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_1f
    new-instance v17, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.service.UNFREEZE"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->sendBroadcast(Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget v3, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v2, v3, :cond_20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    iget-short v2, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-short v3, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v2, v3, :cond_20

    const-string v2, "TuningService"

    const-string v3, "ProList:Select the same channel!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_9
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_20
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_21

    iget-short v2, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_21

    const-string v2, "ProgramListViewActivity"

    const-string v3, "ATV selected, close PIP..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v2

    if-eqz v2, :cond_21

    new-instance v16, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disablePip"

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;->this$0:Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v3, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->values()[Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    move-result-object v4

    iget-short v5, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v4, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    goto :goto_9

    :cond_22
    const/4 v2, 0x0

    goto/16 :goto_0
.end method
