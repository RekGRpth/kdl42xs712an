.class Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;
.super Ljava/lang/Object;
.source "CimmiActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;->this$1:Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;->this$1:Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->access$1(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)Lcom/konka/tvsettings/channel/CimmiActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/channel/CimmiActivity;->resetCimmiTimeCounter()V

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;->this$1:Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->access$1(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)Lcom/konka/tvsettings/channel/CimmiActivity;

    move-result-object v2

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/CimmiActivity;->access$0(Lcom/konka/tvsettings/channel/CimmiActivity;)Lcom/konka/kkinterface/tv/CiDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getEnqAnsLength()S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;->this$1:Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->editText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->access$0(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    if-lt v2, v1, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;->this$1:Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->access$1(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)Lcom/konka/tvsettings/channel/CimmiActivity;

    move-result-object v2

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/CimmiActivity;->access$0(Lcom/konka/tvsettings/channel/CimmiActivity;)Lcom/konka/kkinterface/tv/CiDesk;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;->this$1:Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->editText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->access$0(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/CiDesk;->answerEnq(Ljava/lang/String;)Z

    iget-object v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;->this$1:Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->editText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->access$0(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setVisibility(I)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
