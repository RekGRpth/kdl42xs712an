.class public Lcom/konka/tvsettings/channel/ProgramListViewItemObject;
.super Ljava/lang/Object;
.source "ProgramListViewItemObject.java"


# instance fields
.field private favoriteImg:Z

.field private lockImg:Z

.field private moveImg:Z

.field private serviceType:S

.field private skipImg:Z

.field private sourceImg:Z

.field private sslImg:Z

.field private tvName:Ljava/lang/String;

.field private tvNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->tvNumber:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->tvName:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->favoriteImg:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->moveImg:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->skipImg:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->lockImg:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->sslImg:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->sourceImg:Z

    return-void
.end method


# virtual methods
.method public getServiceType()S
    .locals 1

    iget-short v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->serviceType:S

    return v0
.end method

.method public getTvName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->tvName:Ljava/lang/String;

    return-object v0
.end method

.method public getTvNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->tvNumber:Ljava/lang/String;

    return-object v0
.end method

.method public isFavoriteImg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->favoriteImg:Z

    return v0
.end method

.method public isLockImg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->lockImg:Z

    return v0
.end method

.method public isMoveImg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->moveImg:Z

    return v0
.end method

.method public isSkipImg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->skipImg:Z

    return v0
.end method

.method public isSourceImg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->sourceImg:Z

    return v0
.end method

.method public isSslImg()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->sslImg:Z

    return v0
.end method

.method public setFavoriteImg(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->favoriteImg:Z

    return-void
.end method

.method public setLockImg(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->lockImg:Z

    return-void
.end method

.method public setMoveImg(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->moveImg:Z

    return-void
.end method

.method public setServiceType(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->serviceType:S

    return-void
.end method

.method public setSkipImg(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->skipImg:Z

    return-void
.end method

.method public setSourceImg(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->sourceImg:Z

    return-void
.end method

.method public setSslImg(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->sslImg:Z

    return-void
.end method

.method public setTvName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->tvName:Ljava/lang/String;

    return-void
.end method

.method public setTvNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->tvNumber:Ljava/lang/String;

    return-void
.end method
