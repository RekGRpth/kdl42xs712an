.class public Lcom/konka/tvsettings/sound/SoundSettingActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;
    }
.end annotation


# static fields
.field private static final ANDROID_LOG_DEBUG:Ljava/lang/String; = "SoundSettingActivity"


# instance fields
.field public ITEM_HEIGHT:I

.field public ITEM_WIDTH:I

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field public iAudioLangIndex:I

.field public iItem_bottomHeight:I

.field public iItem_topHeight:I

.field private itemAD:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemBalance:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemEqualizer:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemSmartSoundCtrl:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

.field private mAudioManager:Landroid/media/AudioManager;

.field private menuContainer:Landroid/widget/LinearLayout;

.field private myHandler:Landroid/os/Handler;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->iAudioLangIndex:I

    new-instance v0, Lcom/konka/tvsettings/sound/SoundSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity$1;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemEqualizer:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemBalance:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSmartSoundCtrl:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Landroid/media/AudioManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemAD:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method


# virtual methods
.method public LoadDataToUI()V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SoundDesk;->getSoundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemBalance:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SoundDesk;->getBalance()S

    move-result v3

    invoke-virtual {v0, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSmartSoundCtrl:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SoundDesk;->getAVCMode()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SoundDesk;->getSurroundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;->ordinal()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SoundDesk;->getSpdifOutMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->ordinal()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/SoundDesk;->getHdmiAudioSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->ordinal()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemAD:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SoundDesk;->getADEnable()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SoundDesk;->getADAbsoluteVolume()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method

.method public findViews()V
    .locals 58

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getSoundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v7

    new-instance v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$2;

    sget-object v3, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    const/4 v4, 0x0

    aget v5, v3, v4

    sget-object v3, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->ValArrOf:[I

    const/4 v4, 0x0

    aget v6, v3, v4

    move-object/from16 v0, p0

    iget v8, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move-object/from16 v3, p0

    move-object/from16 v4, p0

    invoke-direct/range {v2 .. v9}, Lcom/konka/tvsettings/sound/SoundSettingActivity$2;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    new-instance v8, Lcom/konka/tvsettings/sound/SoundSettingActivity$3;

    const v11, 0x7f0a0095    # com.konka.tvsettings.R.string.str_sound_equalizer

    const v12, 0x7f0a002b    # com.konka.tvsettings.R.string.str_3dot

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move-object/from16 v9, p0

    move-object/from16 v10, p0

    invoke-direct/range {v8 .. v14}, Lcom/konka/tvsettings/sound/SoundSettingActivity$3;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIII)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemEqualizer:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->getIndex()I

    move-result v56

    const/4 v2, 0x4

    move/from16 v0, v56

    if-ne v0, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemEqualizer:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getBalance()S

    move-result v2

    add-int/lit8 v12, v2, -0x32

    new-instance v8, Lcom/konka/tvsettings/sound/SoundSettingActivity$4;

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    const/4 v3, 0x1

    aget v11, v2, v3

    const/16 v13, -0x32

    const/16 v14, 0x32

    move-object/from16 v0, p0

    iget v15, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move/from16 v16, v0

    move-object/from16 v9, p0

    move-object/from16 v10, p0

    invoke-direct/range {v8 .. v16}, Lcom/konka/tvsettings/sound/SoundSettingActivity$4;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemBalance:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getAVCMode()Z

    move-result v57

    if-eqz v57, :cond_8

    const/16 v18, 0x1

    :goto_1
    new-instance v13, Lcom/konka/tvsettings/sound/SoundSettingActivity$5;

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    const/4 v3, 0x2

    aget v16, v2, v3

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->ValArrOf:[I

    const/4 v3, 0x2

    aget v17, v2, v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move/from16 v20, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v13 .. v20}, Lcom/konka/tvsettings/sound/SoundSettingActivity$5;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSmartSoundCtrl:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v53

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getSurroundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;->ordinal()I

    move-result v24

    new-instance v19, Lcom/konka/tvsettings/sound/SoundSettingActivity$6;

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    const/4 v3, 0x3

    aget v22, v2, v3

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->ValArrOf:[I

    const/4 v3, 0x3

    aget v23, v2, v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move/from16 v26, v0

    move-object/from16 v20, p0

    move-object/from16 v21, p0

    invoke-direct/range {v19 .. v26}, Lcom/konka/tvsettings/sound/SoundSettingActivity$6;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getSpdifOutMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->ordinal()I

    move-result v30

    const/4 v2, 0x2

    move/from16 v0, v30

    if-ne v0, v2, :cond_9

    const/16 v30, 0x1

    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SPDIF XXX=====>>>spdifIn["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v30

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v25, Lcom/konka/tvsettings/sound/SoundSettingActivity$7;

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    const/4 v3, 0x4

    aget v28, v2, v3

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->ValArrOf:[I

    const/4 v3, 0x4

    aget v29, v2, v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move/from16 v32, v0

    move-object/from16 v26, p0

    move-object/from16 v27, p0

    invoke-direct/range {v25 .. v32}, Lcom/konka/tvsettings/sound/SoundSettingActivity$7;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    :cond_0
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-ne v0, v2, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/SoundDesk;->getHdmiAudioSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->ordinal()I

    move-result v36

    new-instance v31, Lcom/konka/tvsettings/sound/SoundSettingActivity$8;

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    const/4 v3, 0x5

    aget v34, v2, v3

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->ValArrOf:[I

    const/4 v3, 0x5

    aget v35, v2, v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move/from16 v38, v0

    move-object/from16 v32, p0

    move-object/from16 v33, p0

    invoke-direct/range {v31 .. v38}, Lcom/konka/tvsettings/sound/SoundSettingActivity$8;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getADEnable()Z

    move-result v52

    if-eqz v52, :cond_c

    const/16 v42, 0x1

    :goto_4
    const-string v2, "liying"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n EnableAD----"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v42

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v37, Lcom/konka/tvsettings/sound/SoundSettingActivity$9;

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    const/4 v3, 0x6

    aget v40, v2, v3

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->ValArrOf:[I

    const/4 v3, 0x6

    aget v41, v2, v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move/from16 v44, v0

    move-object/from16 v38, p0

    move-object/from16 v39, p0

    invoke-direct/range {v37 .. v44}, Lcom/konka/tvsettings/sound/SoundSettingActivity$9;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, v37

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemAD:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getADAbsoluteVolume()I

    move-result v47

    new-instance v43, Lcom/konka/tvsettings/sound/SoundSettingActivity$10;

    sget-object v2, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    const/4 v3, 0x7

    aget v46, v2, v3

    const/16 v48, 0x0

    const/16 v49, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    move/from16 v50, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    move/from16 v51, v0

    move-object/from16 v44, p0

    move-object/from16 v45, p0

    invoke-direct/range {v43 .. v51}, Lcom/konka/tvsettings/sound/SoundSettingActivity$10;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCEC()Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableMonitor()Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemAD:Lcom/konka/tvsettings/view/ItemStringOption;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->setVisibility(I)V

    :cond_5
    :goto_5
    new-instance v55, Landroid/widget/ImageView;

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, v55

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020031    # com.konka.tvsettings.R.drawable.com_bg_popmenu1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v55

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v54, Landroid/widget/ImageView;

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, v54

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020033    # com.konka.tvsettings.R.drawable.com_bg_popmenu3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object/from16 v0, v54

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemEqualizer:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemBalance:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSmartSoundCtrl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCEC()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemAD:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, v54

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemEqualizer:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    goto/16 :goto_0

    :cond_8
    const/16 v18, 0x0

    goto/16 :goto_1

    :cond_9
    const/16 v30, 0x0

    goto/16 :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    goto/16 :goto_3

    :cond_c
    const/16 v42, 0x0

    goto/16 :goto_4

    :cond_d
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v53

    if-eq v0, v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemAD:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    goto/16 :goto_5
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03004c    # com.konka.tvsettings.R.layout.sound_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009    # com.konka.tvsettings.R.dimen.SettingStringOption_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_WIDTH:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000a    # com.konka.tvsettings.R.dimen.SettingStringOption_Height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->ITEM_HEIGHT:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000b    # com.konka.tvsettings.R.dimen.SettingStringOption_topHeight

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->iItem_topHeight:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000c    # com.konka.tvsettings.R.dimen.SettingStringOption_bottomHeight

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->iItem_bottomHeight:I

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    if-nez v0, :cond_0

    const-string v0, "the channel desk is null null null!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    const v0, 0x7f0701e3    # com.konka.tvsettings.R.id.sound_menu_layout

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->menuContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViews()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->mAudioManager:Landroid/media/AudioManager;

    return-void

    :cond_0
    const-string v0, "the channel skin is good in use!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v5, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v4, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    :sswitch_0
    return v2

    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->finish()V

    invoke-virtual {p0, v4, v5}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    const-string v2, "Exit"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->finish()V

    invoke-virtual {p0, v4, v5}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_3
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_4

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_4

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_4

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_4

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "liying"

    const-string v4, "itemsoundmod request \n"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCEC()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemAD:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v3

    if-nez v3, :cond_7

    :cond_6
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto/16 :goto_1

    :cond_7
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto/16 :goto_1

    :sswitch_4
    const-string v3, "liying"

    const-string v4, "KEYCODE_DPAD_UP request \n"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_a

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_a

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_a

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_a

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v3, :cond_8

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto/16 :goto_1

    :cond_8
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCEC()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemADVolume:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto/16 :goto_1

    :cond_9
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto/16 :goto_1

    :cond_a
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v3

    if-nez v3, :cond_b

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSurround:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto/16 :goto_1

    :cond_b
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v3

    if-nez v3, :cond_d

    :cond_c
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto/16 :goto_1

    :cond_d
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->requestFocus()Z

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_4
        0x14 -> :sswitch_3
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStop()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method

.method public unmute()V
    .locals 4

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity;->myHandler:Landroid/os/Handler;

    new-instance v1, Lcom/konka/tvsettings/sound/SoundSettingActivity$11;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/sound/SoundSettingActivity$11;-><init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
