.class Lcom/konka/tvsettings/sound/EqualizerDialogActivity$5;
.super Lcom/konka/tvsettings/statebar/SeekBarButton;
.source "EqualizerDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/EqualizerDialogActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;Landroid/app/Activity;IIZ)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    iput-object p1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$5;->this$0:Lcom/konka/tvsettings/sound/EqualizerDialogActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/statebar/SeekBarButton;-><init>(Landroid/app/Activity;IIZ)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$5;->this$0:Lcom/konka/tvsettings/sound/EqualizerDialogActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->getSoundEffect()Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$5;->this$0:Lcom/konka/tvsettings/sound/EqualizerDialogActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    invoke-interface {v1, v0, v3}, Lcom/konka/kkinterface/tv/SoundDesk;->adjustSoundMode(Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;Z)Z

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$5;->this$0:Lcom/konka/tvsettings/sound/EqualizerDialogActivity;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->bIsValueChanged:Z

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$5;->this$0:Lcom/konka/tvsettings/sound/EqualizerDialogActivity;

    # getter for: Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->access$0(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/media/AudioManager;->setMasterMute(ZI)V

    return-void
.end method
