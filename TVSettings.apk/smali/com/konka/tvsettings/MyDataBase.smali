.class public Lcom/konka/tvsettings/MyDataBase;
.super Ljava/lang/Object;
.source "MyDataBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/MyDataBase$EnumDTVCity;,
        Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;,
        Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;
    }
.end annotation


# static fields
.field public static final CODE_VERSION:I

.field private static cr:Landroid/content/ContentResolver;

.field public static myDatabase:Lcom/konka/tvsettings/MyDataBase;

.field private static strFileDir:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/MyDataBase;->context:Landroid/content/Context;

    return-void
.end method

.method public static checkDBVersion()Z
    .locals 9

    const/4 v2, 0x0

    const/4 v8, 0x0

    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/checkVersion"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "version"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-nez v7, :cond_0

    const/4 v8, 0x1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v8
.end method

.method public static closeDB()V
    .locals 0

    return-void
.end method

.method private getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/MyDataBase;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    :cond_0
    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    return-object v0
.end method

.method public static getDatabaseInstance(Landroid/content/Context;)Lcom/konka/tvsettings/MyDataBase;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->myDatabase:Lcom/konka/tvsettings/MyDataBase;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/tvsettings/MyDataBase;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/MyDataBase;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase;->myDatabase:Lcom/konka/tvsettings/MyDataBase;

    :cond_0
    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->myDatabase:Lcom/konka/tvsettings/MyDataBase;

    return-object v0
.end method

.method public static openDB()V
    .locals 0

    return-void
.end method

.method public static setDBFileDir(Ljava/lang/String;)V
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "set Dir"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sput-object p0, Lcom/konka/tvsettings/MyDataBase;->strFileDir:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDTVCity()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/programsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    move-result-object v0

    const-string v1, "enDTVCity"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getDVBCModulationType()Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/programsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->values()[Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    move-result-object v0

    const-string v1, "enDVBCModulationType"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getDVBCNetTableFrequency()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/programsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iNetTableFrequency"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public getDVBCScanType()Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/programsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->values()[Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    move-result-object v0

    const-string v1, "enDVBCScanType"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public getDVBCSymbolRate()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    sget-object v0, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/programsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iSymbolRate"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public setDTVCity(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)V
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "enDTVCity"

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    sget-object v2, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v3, "content://mstar.tv.factory/programsetting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "update tbl_programsetting error"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDVBCModulationType(Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;)V
    .locals 6
    .param p1    # Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "enDVBCModulationType"

    invoke-virtual {p1}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCModulationType;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    sget-object v2, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v3, "content://mstar.tv.factory/programsetting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "update tbl_programsetting error"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDVBCNetTableFrequency(I)V
    .locals 6
    .param p1    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "iNetTableFrequency"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    sget-object v2, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v3, "content://mstar.tv.factory/programsetting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "update tbl_programsetting error"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDVBCScanType(Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;)V
    .locals 6
    .param p1    # Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "enDVBCScanType"

    invoke-virtual {p1}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    sget-object v2, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v3, "content://mstar.tv.factory/programsetting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "update tbl_programsetting error"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDVBCSymbolRate(I)V
    .locals 6
    .param p1    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "iSymbolRate"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    sget-object v2, Lcom/konka/tvsettings/MyDataBase;->cr:Landroid/content/ContentResolver;

    const-string v3, "content://mstar.tv.factory/programsetting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "update tbl_programsetting error"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V

    goto :goto_0
.end method
