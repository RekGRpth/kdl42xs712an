.class public Lcom/konka/tvsettings/common/PreferancesTools;
.super Ljava/lang/Object;
.source "PreferancesTools.java"


# static fields
.field public static final SRS_DCCONTROL:Ljava/lang/String; = "srs_dccontrol"

.field public static final SRS_DEFINITIONCONTROL:Ljava/lang/String; = "srs_definitioncontrol"

.field public static final SRS_INPUTGAIN:Ljava/lang/String; = "srs_inputgain"

.field public static final SRS_ISSHOW_MENUINFO:Ljava/lang/String; = "isShowInfoMenu"

.field public static final SRS_SPEAKERANALYSIS:Ljava/lang/String; = "srs_speakeranalysis"

.field public static final SRS_SPEAKERAUDIO:Ljava/lang/String; = "srs_speakeraudio"

.field public static final SRS_SURRLEVELCONTROL:Ljava/lang/String; = "srs_surrlevelcontrol"

.field public static final SRS_TRUBASSCONTROL:Ljava/lang/String; = "srs_trubasscontrol"


# instance fields
.field context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getBooleanData(Ljava/lang/String;Z)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const/4 v3, 0x3

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    return v2
.end method

.method public getInfoMenuStatus(Z)Z
    .locals 4
    .param p1    # Z

    iget-object v1, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const-string v2, "TvSetting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "isShowInfoMenu"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public getIntPref(Ljava/lang/String;I)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const/4 v3, 0x3

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    return v2
.end method

.method public getLongPref(Ljava/lang/String;Ljava/lang/Long;)J
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Long;

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const/4 v3, 0x3

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, p1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method public getStringPref(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const/4 v3, 0x3

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public setBooleanPref(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v3, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const/4 v4, 0x3

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setIntPref(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v3, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const/4 v4, 0x3

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setLongPref(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Long;

    iget-object v3, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const/4 v4, 0x3

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v0, p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setShowInfoMenuStatus(Z)V
    .locals 5
    .param p1    # Z

    iget-object v2, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const-string v3, "TvSetting"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "isShowInfoMenu"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public setStringPref(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/tvsettings/common/PreferancesTools;->context:Landroid/content/Context;

    const/4 v4, 0x3

    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
