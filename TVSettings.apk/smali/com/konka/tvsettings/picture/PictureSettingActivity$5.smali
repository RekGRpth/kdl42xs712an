.class Lcom/konka/tvsettings/picture/PictureSettingActivity$5;
.super Lcom/konka/tvsettings/picture/PictureSettingItem2;
.source "PictureSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemPicModeUser()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem2;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;
    invoke-static {v2}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$4(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->getCurrentValue()I

    move-result v2

    int-to-short v2, v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->ExecVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;S)Z

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->bIsPicValueChanged:Z

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->onFocusChange(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    iget-boolean v0, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->bIsPicValueChanged:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->UpdateVideoItem()V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->bIsPicValueChanged:Z

    const-string v0, "itemSharpness change===>> need UpdateVideoItem database"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
