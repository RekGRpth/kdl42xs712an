.class Lcom/konka/tvsettings/picture/PictureSettingActivity$10;
.super Lcom/konka/tvsettings/picture/PictureBacklight;
.source "PictureSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemBacklight()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$10;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureBacklight;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$10;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$10;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBacklight:Lcom/konka/tvsettings/picture/PictureSettingItem2;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$12(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->getCurrentValue()I

    move-result v1

    int-to-short v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PictureDesk;->SetBacklight(S)Z

    return-void
.end method
