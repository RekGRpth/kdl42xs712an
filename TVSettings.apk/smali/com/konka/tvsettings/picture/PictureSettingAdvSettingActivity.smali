.class public Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;
.super Landroid/app/Activity;
.source "PictureSettingAdvSettingActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

.field private static mDynamicDenoiseStatus:I


# instance fields
.field private itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private myHandler:Landroid/os/Handler;

.field pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

.field serverProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_HIGH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_LOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    sput v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->mDynamicDenoiseStatus:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity$1;-><init>(Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->myHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$2(I)V
    .locals 0

    sput p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->mDynamicDenoiseStatus:I

    return-void
.end method

.method private addItemDynamicDenoise()V
    .locals 6

    invoke-static {}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->serverProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PictureDesk;->getDNR()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x3

    sput v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->mDynamicDenoiseStatus:I

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity$2;

    const v3, 0x7f0700fd    # com.konka.tvsettings.R.id.picture_setting_adv_dynamic_denoise

    const v4, 0x7f0b0017    # com.konka.tvsettings.R.array.str_pic_setting_arr_dynamic_denoise

    sget v5, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->mDynamicDenoiseStatus:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity$2;-><init>(Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :pswitch_0
    const/4 v0, 0x4

    sput v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->mDynamicDenoiseStatus:I

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    sput v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->mDynamicDenoiseStatus:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    sput v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->mDynamicDenoiseStatus:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    sput v0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->mDynamicDenoiseStatus:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->addItemDynamicDenoise()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030026    # com.konka.tvsettings.R.layout.picture_setting_adv_setting

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->setContentView(I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->serverProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->serverProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->addView()V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/picture/PictureSettingAdvSettingActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
