.class Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;
.super Ljava/lang/Thread;
.source "RootActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/RootActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AlTimeShiftThread"
.end annotation


# instance fields
.field b_needtocheckTimeShift:Z

.field pvr:Lcom/mstar/android/tvapi/common/PvrManager;

.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->b_needtocheckTimeShift:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const/4 v12, 0x1

    const/4 v11, 0x0

    :goto_0
    sget-boolean v8, Lcom/konka/tvsettings/RootActivity;->bExitThread:Z

    if-eqz v8, :cond_0

    return-void

    :cond_0
    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v8}, Lcom/konka/tvsettings/RootActivity;->access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v8

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v8, :cond_3

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    const-string v9, "activity"

    invoke-virtual {v8, v9}, Lcom/konka/tvsettings/RootActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v12}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v2, v8, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z
    invoke-static {v8}, Lcom/konka/tvsettings/RootActivity;->access$26(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "TimeShift"

    const-string v9, "===========Signal not lock \n"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v12, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->b_needtocheckTimeShift:Z

    :cond_1
    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->bNeedRestartAlwaysTimeShift:Z
    invoke-static {v8}, Lcom/konka/tvsettings/RootActivity;->access$35(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v8

    if-eqz v8, :cond_2

    iput-boolean v12, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->b_needtocheckTimeShift:Z

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v8, v11}, Lcom/konka/tvsettings/RootActivity;->access$34(Lcom/konka/tvsettings/RootActivity;Z)V

    :cond_2
    iget-boolean v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->b_needtocheckTimeShift:Z

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z
    invoke-static {v8}, Lcom/konka/tvsettings/RootActivity;->access$26(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v8

    if-eqz v8, :cond_3

    :try_start_0
    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v8}, Lcom/konka/tvsettings/RootActivity;->access$4(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v8

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybacking()Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {v8}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/konka/tvsettings/SwitchMenuHelper;->getBestDiskPath(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v4

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "RootActivity=======DiskPath["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] \n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v8, "qhc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "getBestDiskPath:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "NO_DISK"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    const/4 v9, 0x2

    invoke-virtual {v8, v4, v9}, Lcom/mstar/android/tvapi/common/PvrManager;->setPvrParams(Ljava/lang/String;S)Z

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/PvrManager;->startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "RootActivity=====>>>Start 444...startTimeShiftRecord...>>>["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->b_needtocheckTimeShift:Z

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v7, v8, :cond_3

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v7, v8, :cond_3

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v6

    iget-object v8, p0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShift()V

    const-string v8, "PvrActivity=====>>>finish 22 \n"

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    :goto_1
    const-wide/16 v8, 0x7d0

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
