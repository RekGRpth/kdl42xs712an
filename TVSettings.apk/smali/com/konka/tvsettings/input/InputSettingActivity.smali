.class public Lcom/konka/tvsettings/input/InputSettingActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "InputSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;
    }
.end annotation


# instance fields
.field private HEIGHT:I

.field private WIDTH:I

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private iCurFocusNum:I

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mInputSourceName:[Ljava/lang/String;

.field private mViewItemContainer:Landroid/widget/LinearLayout;

.field private mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

.field private myHandler:Landroid/os/Handler;

.field private rootActivity:Lcom/konka/tvsettings/RootActivity;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    new-instance v0, Lcom/konka/tvsettings/input/InputSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/input/InputSettingActivity$1;-><init>(Lcom/konka/tvsettings/input/InputSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/input/InputSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->WIDTH:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/input/InputSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->HEIGHT:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/input/InputSettingActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/input/InputSettingActivity;Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->rootActivity:Lcom/konka/tvsettings/RootActivity;

    return-void
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/input/InputSettingActivity;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/input/InputSettingActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/input/InputSettingActivity;)Lcom/konka/tvsettings/RootActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->rootActivity:Lcom/konka/tvsettings/RootActivity;

    return-object v0
.end method

.method private dataConfigurate()V
    .locals 14

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v2, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountDTV:I

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v0, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountATV:I

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v1, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountAV:I

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v7, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountYPbPr:I

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v6, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountVGA:I

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v3, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountHDMI:I

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v5, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountSTORAGE:I

    add-int v10, v2, v0

    add-int/2addr v10, v1

    add-int/2addr v10, v7

    add-int/2addr v10, v6

    add-int v4, v10, v3

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v10

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v10

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableUSBInputSource()Z

    move-result v10

    if-eqz v10, :cond_0

    add-int/2addr v4, v5

    :cond_0
    new-array v10, v4, [Ljava/lang/String;

    iput-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    new-array v10, v4, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_0
    if-lt v9, v2, :cond_2

    const/4 v9, 0x0

    :goto_1
    if-lt v9, v0, :cond_4

    const/4 v9, 0x0

    :goto_2
    if-lt v9, v1, :cond_6

    const/4 v9, 0x0

    :goto_3
    if-lt v9, v7, :cond_8

    const/4 v9, 0x0

    :goto_4
    if-lt v9, v6, :cond_a

    const/4 v9, 0x0

    :goto_5
    if-lt v9, v3, :cond_c

    if-ge v8, v4, :cond_1

    const/4 v9, 0x0

    :goto_6
    if-lt v9, v5, :cond_e

    :cond_1
    return-void

    :cond_2
    const/4 v10, 0x1

    if-ne v2, v10, :cond_3

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00c9    # com.konka.tvsettings.R.string.str_popup_input_menu_dtv

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    :goto_7
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v11, Lcom/konka/tvsettings/ConfigurationData;->DTVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v11, v11, v9

    aput-object v11, v10, v8

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_3
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00c9    # com.konka.tvsettings.R.string.str_popup_input_menu_dtv

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    goto :goto_7

    :cond_4
    const/4 v10, 0x1

    if-ne v0, v10, :cond_5

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00ca    # com.konka.tvsettings.R.string.str_popup_input_menu_atv

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    :goto_8
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v11, Lcom/konka/tvsettings/ConfigurationData;->ATVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v11, v11, v9

    aput-object v11, v10, v8

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_5
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00ca    # com.konka.tvsettings.R.string.str_popup_input_menu_atv

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    goto :goto_8

    :cond_6
    const/4 v10, 0x1

    if-ne v1, v10, :cond_7

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00cb    # com.konka.tvsettings.R.string.str_popup_input_menu_av

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    :goto_9
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v11, Lcom/konka/tvsettings/ConfigurationData;->AVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v11, v11, v9

    aput-object v11, v10, v8

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    :cond_7
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00cb    # com.konka.tvsettings.R.string.str_popup_input_menu_av

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    goto :goto_9

    :cond_8
    const/4 v10, 0x1

    if-ne v7, v10, :cond_9

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00cd    # com.konka.tvsettings.R.string.str_popup_input_menu_ypbpr

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    :goto_a
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v11, Lcom/konka/tvsettings/ConfigurationData;->YPbPrInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v11, v11, v9

    aput-object v11, v10, v8

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_3

    :cond_9
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00cd    # com.konka.tvsettings.R.string.str_popup_input_menu_ypbpr

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    goto :goto_a

    :cond_a
    const/4 v10, 0x1

    if-ne v6, v10, :cond_b

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00ce    # com.konka.tvsettings.R.string.str_popup_input_menu_vga

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    :goto_b
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v11, Lcom/konka/tvsettings/ConfigurationData;->VGAInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v11, v11, v9

    aput-object v11, v10, v8

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_4

    :cond_b
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00ce    # com.konka.tvsettings.R.string.str_popup_input_menu_vga

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    goto :goto_b

    :cond_c
    const/4 v10, 0x1

    if-ne v3, v10, :cond_d

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00cc    # com.konka.tvsettings.R.string.str_popup_input_menu_hdmi

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    :goto_c
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v11, Lcom/konka/tvsettings/ConfigurationData;->HDMIInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v11, v11, v9

    aput-object v11, v10, v8

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_5

    :cond_d
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00cc    # com.konka.tvsettings.R.string.str_popup_input_menu_hdmi

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    goto :goto_c

    :cond_e
    const/4 v10, 0x1

    if-ne v5, v10, :cond_f

    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00cf    # com.konka.tvsettings.R.string.str_popup_input_menu_usb

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    :goto_d
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v11, Lcom/konka/tvsettings/ConfigurationData;->STORAGEInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v11, v11, v9

    aput-object v11, v10, v8

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_6

    :cond_f
    iget-object v10, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00cf    # com.konka.tvsettings.R.string.str_popup_input_menu_usb

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v8

    goto :goto_d
.end method

.method private findFocus(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->requestFocus()Z

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->requestFocus()Z

    iput v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private stopTimeShift()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private switchToUsbSource()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.mm"

    const-string v2, "com.konka.mm.filemanager.FileDiskActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/input/InputSettingActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public addItem()V
    .locals 10

    new-instance v7, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v6, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->WIDTH:I

    const/4 v1, -0x2

    invoke-direct {v8, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f02006c    # com.konka.tvsettings.R.drawable.input_item_head

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    const v0, 0x7f02006b    # com.konka.tvsettings.R.drawable.input_item_foot

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    iput-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    const/4 v5, 0x0

    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    array-length v0, v0

    if-lt v5, v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v9, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    new-instance v0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    aget-object v3, v1, v5

    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v4, v1, v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;-><init>(Lcom/konka/tvsettings/input/InputSettingActivity;Landroid/content/Context;Ljava/lang/String;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;I)V

    aput-object v0, v9, v5

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected changeInputSourceTimeout()V
    .locals 5

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    :goto_1
    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-instance v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v2}, Lcom/konka/tvsettings/RootActivity;-><init>()V

    iput-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->rootActivity:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->stopTimeShift()V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->switchToUsbSource()V

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->rootActivity:Lcom/konka/tvsettings/RootActivity;

    iget-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/tvsettings/RootActivity;->setTVInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Landroid/content/Context;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/input/InputSettingActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected isFocusOnCurrentSource()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mInputSourceName:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    :goto_1
    return v1

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001b    # com.konka.tvsettings.R.layout.input_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/input/InputSettingActivity;->setContentView(I)V

    const v0, 0x7f0700c7    # com.konka.tvsettings.R.id.input_item_container

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/input/InputSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090007    # com.konka.tvsettings.R.dimen.TVSettingInputItem_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->WIDTH:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090008    # com.konka.tvsettings.R.dimen.TVSettingInputItem_Height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->HEIGHT:I

    invoke-direct {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->dataConfigurate()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->addItem()V

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/input/InputSettingActivity;->findFocus(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->destroy()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v2, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v1, 0x1

    const/4 v4, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_1
    :sswitch_0
    return v1

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/input/InputSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->finish()V

    invoke-virtual {p0, v4, v2}, Lcom/konka/tvsettings/input/InputSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    const-string v1, "Exit"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity;->finish()V

    invoke-virtual {p0, v4, v2}, Lcom/konka/tvsettings/input/InputSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_3
    iget v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    iget v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->requestFocus()Z

    goto :goto_1

    :cond_0
    iget v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    iget v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->requestFocus()Z

    goto :goto_1

    :sswitch_4
    iget v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    iget-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    iput v4, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    iget v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->requestFocus()Z

    goto :goto_1

    :cond_1
    iget v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->mViewItems:[Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;

    iget v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity;->iCurFocusNum:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->requestFocus()Z

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method
