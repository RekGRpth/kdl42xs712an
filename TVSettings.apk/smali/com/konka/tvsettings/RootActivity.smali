.class public Lcom/konka/tvsettings/RootActivity;
.super Landroid/app/Activity;
.source "RootActivity.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;,
        Lcom/konka/tvsettings/RootActivity$CiHandler;,
        Lcom/konka/tvsettings/RootActivity$MyHandler;,
        Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;,
        Lcom/konka/tvsettings/RootActivity$RootMenuHandler;,
        Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I = null

.field public static CurProBlock:Z = false

.field private static final NO_DISK:Ljava/lang/String; = "NO_DISK"

.field private static final TAG:Ljava/lang/String; = "RootActivity"

.field public static bExitThread:Z

.field private static m_bIsPVRStart:Z

.field private static m_bIsShowSourceInfo:Z

.field private static m_bIsStartMsrv:Z

.field private static systemAutoTime:I


# instance fields
.field private STICKER_COUNT_DOWN:I

.field private STICKER_COUNT_TIME:I

.field public TVInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field alTimeShiftThread:Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;

.field private bNeedRestartAlwaysTimeShift:Z

.field private bSwitchChannelControl:Z

.field private benableSwitchChannelControl:Z

.field cecManager:Lcom/mstar/android/tvapi/common/CecManager;

.field private ciHandler:Lcom/konka/tvsettings/RootActivity$CiHandler;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private disableBacklightRunnable:Ljava/lang/Runnable;

.field private enableDoubleChannel:Z

.field private iTimeChannelInterval:I

.field private isTvKeyPadAction:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mStickerCountDownTask:Ljava/util/TimerTask;

.field private mStickerCountDownTimer:Ljava/util/Timer;

.field private m_RadioBgView:Landroid/widget/ImageView;

.field private m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;

.field private m_bColorWheelDemon:Z

.field private m_bDTVScreenWarning:Z

.field private m_bEnergySavingDemon:Z

.field private m_bIsFirstDoingAfterStart:Z

.field private m_bIsScreenSaverOn:Z

.field private m_bRootActivityActive:Z

.field private m_bRootActivityOnTop:Z

.field private m_bSignalLock:Z

.field private m_iDTVScreenStatus:I

.field private m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

.field private m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private m_surfaceHolder:Landroid/view/SurfaceHolder;

.field private m_surfaceView:Landroid/view/SurfaceView;

.field mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

.field private pvr:Lcom/konka/kkinterface/tv/PvrDesk;

.field private rOpenFramepacking:Ljava/lang/Runnable;

.field private rShowAudioOnlyMenu:Ljava/lang/Runnable;

.field private rShowSettingMenu:Ljava/lang/Runnable;

.field private rootApp:Lcom/konka/tvsettings/TVRootApp;

.field private rootmenuEventReceiver:Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;

.field private rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

.field private setBackLightRunnable:Ljava/lang/Runnable;

.field private settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

.field private final showMenu:Landroid/content/Intent;

.field private sp:Landroid/content/SharedPreferences;

.field standbyHandler:Landroid/os/Handler;

.field private subtitlePosLive:I

.field private subtitleRunnable:Ljava/lang/Runnable;

.field private tmpHandler:Landroid/os/Handler;

.field usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

.field private vgaSupport:Z


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/RootActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/tvsettings/RootActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/tvsettings/RootActivity;->m_bIsStartMsrv:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/RootActivity;->m_bIsShowSourceInfo:Z

    sput v1, Lcom/konka/tvsettings/RootActivity;->systemAutoTime:I

    sput-boolean v1, Lcom/konka/tvsettings/RootActivity;->m_bIsPVRStart:Z

    sput-boolean v1, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    sput-boolean v1, Lcom/konka/tvsettings/RootActivity;->bExitThread:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x3

    const/4 v0, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsScreenSaverOn:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->m_bColorWheelDemon:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->m_bEnergySavingDemon:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityActive:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->m_bDTVScreenWarning:Z

    iput v0, p0, Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I

    iput v0, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->TVInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->benableSwitchChannelControl:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->bSwitchChannelControl:Z

    iput v2, p0, Lcom/konka/tvsettings/RootActivity;->iTimeChannelInterval:I

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsFirstDoingAfterStart:Z

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tvsettings.intent.action.MainMenuActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->showMenu:Landroid/content/Intent;

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->ciHandler:Lcom/konka/tvsettings/RootActivity$CiHandler;

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->vgaSupport:Z

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTimer:Ljava/util/Timer;

    iput-object v3, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTask:Ljava/util/TimerTask;

    iput v4, p0, Lcom/konka/tvsettings/RootActivity;->STICKER_COUNT_TIME:I

    iput v4, p0, Lcom/konka/tvsettings/RootActivity;->STICKER_COUNT_DOWN:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->bNeedRestartAlwaysTimeShift:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->isTvKeyPadAction:Z

    new-instance v0, Lcom/konka/tvsettings/RootActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$1;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->setBackLightRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$2;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->subtitleRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$3;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rOpenFramepacking:Ljava/lang/Runnable;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$4;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rShowAudioOnlyMenu:Ljava/lang/Runnable;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$5;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->disableBacklightRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$6;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$6;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->tmpHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$7;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->standbyHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$8;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$8;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rShowSettingMenu:Ljava/lang/Runnable;

    return-void
.end method

.method private DTVScreenWarningHandle(Landroid/os/Message;)V
    .locals 1
    .param p1    # Landroid/os/Message;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$16;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$16;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    invoke-virtual {v0}, Lcom/konka/tvsettings/RootActivity$16;->start()V

    return-void
.end method

.method private ToastWarning(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v5, 0x7f030055    # com.konka.tvsettings.R.layout.toast_warning

    const v4, 0x7f070219    # com.konka.tvsettings.R.id.ll_toastwarning

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/RootActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v0, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v4, 0x7f07021a    # com.konka.tvsettings.R.id.tv_warning

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(I)V

    new-instance v3, Landroid/widget/Toast;

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    const/16 v4, 0x11

    invoke-virtual {v3, v4, v6, v6}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v3, v6}, Landroid/widget/Toast;->setDuration(I)V

    invoke-virtual {v3, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private UnregisterUSBMountlister()V
    .locals 6

    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/RootActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RootActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UnregisterUSBMountlister current activity pid = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "com.konka.tvsettings"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->dismiss()V

    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->resumeSubtitle()V

    return-void
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/RootActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/RootActivity;->ToastWarning(I)V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->stopPVRByLock()V

    return-void
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->startCheckPwdActivity()V

    return-void
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/RootActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/common/CountDownTimer;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->isShowRadio()Z

    move-result v0

    return v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rShowAudioOnlyMenu:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->gotoPVRRecordingForStandBy()V

    return-void
.end method

.method static synthetic access$18(Lcom/konka/tvsettings/RootActivity;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->showMenu:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$19(Lcom/konka/tvsettings/RootActivity;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/RootActivity;->checkmsg(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->open3DForFramePacking()V

    return-void
.end method

.method static synthetic access$20(Lcom/konka/tvsettings/RootActivity;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/RootActivity;->signalLockHandle(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$21(Lcom/konka/tvsettings/RootActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I

    return-void
.end method

.method static synthetic access$22(Lcom/konka/tvsettings/RootActivity;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/RootActivity;->DTVScreenWarningHandle(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$23(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsFirstDoingAfterStart:Z

    return v0
.end method

.method static synthetic access$24(Lcom/konka/tvsettings/RootActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsFirstDoingAfterStart:Z

    return-void
.end method

.method static synthetic access$25(Lcom/konka/tvsettings/RootActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    return-void
.end method

.method static synthetic access$26(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    return v0
.end method

.method static synthetic access$27(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bDTVScreenWarning:Z

    return v0
.end method

.method static synthetic access$28(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bColorWheelDemon:Z

    return v0
.end method

.method static synthetic access$29(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bEnergySavingDemon:Z

    return v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    return v0
.end method

.method static synthetic access$30(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->vgaSupport:Z

    return v0
.end method

.method static synthetic access$31(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->isReadyShowScreenSaver()Z

    move-result v0

    return v0
.end method

.method static synthetic access$32(Z)V
    .locals 0

    sput-boolean p0, Lcom/konka/tvsettings/RootActivity;->m_bIsShowSourceInfo:Z

    return-void
.end method

.method static synthetic access$33(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$RootMenuHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    return-object v0
.end method

.method static synthetic access$34(Lcom/konka/tvsettings/RootActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/RootActivity;->bNeedRestartAlwaysTimeShift:Z

    return-void
.end method

.method static synthetic access$35(Lcom/konka/tvsettings/RootActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->bNeedRestartAlwaysTimeShift:Z

    return v0
.end method

.method static synthetic access$36(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method

.method static synthetic access$37(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/PvrDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    return-object v0
.end method

.method static synthetic access$38(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->subtitleRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$39(Lcom/konka/tvsettings/RootActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/RootActivity;->STICKER_COUNT_DOWN:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/SettingDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    return-object v0
.end method

.method static synthetic access$40(Lcom/konka/tvsettings/RootActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/RootActivity;->STICKER_COUNT_DOWN:I

    return-void
.end method

.method static synthetic access$41(Lcom/konka/tvsettings/RootActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/RootActivity;->STICKER_COUNT_TIME:I

    return v0
.end method

.method static synthetic access$42(Lcom/konka/tvsettings/RootActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/RootActivity;->bSwitchChannelControl:Z

    return-void
.end method

.method static synthetic access$43(Lcom/konka/tvsettings/RootActivity;)Landroid/view/SurfaceHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$45(Lcom/konka/tvsettings/RootActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->tmpHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$MyHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->disableBacklightRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/RootActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I

    return v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/RootActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/RootActivity;->vgaSupport:Z

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/RootActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/RootActivity;->m_bDTVScreenWarning:Z

    return-void
.end method

.method private broadSignalLock()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/RootActivity$15;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/RootActivity$15;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private checkSystemAutoTime()V
    .locals 6

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_time"

    invoke-static {v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    sput v3, Lcom/konka/tvsettings/RootActivity;->systemAutoTime:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget v3, Lcom/konka/tvsettings/RootActivity;->systemAutoTime:I

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v3, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_time"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    return-void

    :catch_0
    move-exception v2

    sput v5, Lcom/konka/tvsettings/RootActivity;->systemAutoTime:I

    goto :goto_0
.end method

.method private checkmsg(I)Z
    .locals 4
    .param p1    # I

    const/4 v0, 0x1

    const/16 v1, 0x40

    if-le p1, v1, :cond_1

    const/16 v1, 0x60

    if-ge p1, v1, :cond_1

    const-string v1, "TvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "get xxx msg:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x1001

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_HBBTV_UI_EVENT:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v1

    if-gt p1, v1, :cond_2

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_CHANNELNAME_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v1

    if-ge p1, v1, :cond_0

    :cond_2
    const-string v0, "TvApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "!!!error  msg:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createSurfaceView()V
    .locals 3

    const-string v0, "createsurfaceview"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v0, 0x7f0701c5    # com.konka.tvsettings.R.id.tranplentview

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_surfaceView:Landroid/view/SurfaceView;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_surfaceView:Landroid/view/SurfaceView;

    new-instance v1, Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;-><init>(Lcom/konka/tvsettings/RootActivity;Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;)V

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    return-void
.end method

.method private doProgramDown()V
    .locals 3

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->programDown()Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    const/16 v2, 0x1f4

    invoke-static {p0, v1, v0, v2}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    invoke-static {p0}, Lcom/konka/tvsettings/SwitchMenuHelper;->unFfeeze(Landroid/app/Activity;)V

    return-void
.end method

.method private doProgramReturn()V
    .locals 3

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->programReturn()Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    const/16 v2, 0x1f4

    invoke-static {p0, v1, v0, v2}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    invoke-static {p0}, Lcom/konka/tvsettings/SwitchMenuHelper;->unFfeeze(Landroid/app/Activity;)V

    return-void
.end method

.method private doProgramUp()V
    .locals 3

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->programUp()Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    const/16 v2, 0x1f4

    invoke-static {p0, v1, v0, v2}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    invoke-static {p0}, Lcom/konka/tvsettings/SwitchMenuHelper;->unFfeeze(Landroid/app/Activity;)V

    return-void
.end method

.method private getStickerDemoStatus()Z
    .locals 2

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SettingDesk;->getStickerDemoStatus()Z

    move-result v0

    return v0
.end method

.method private gotoPVRRecordingForStandBy()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private initChannelSwitch()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/konka/kkinterface/tv/ChannelDesk;->readTurnChannelInterval(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/RootActivity;->iTimeChannelInterval:I

    iget v0, p0, Lcom/konka/tvsettings/RootActivity;->iTimeChannelInterval:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->benableSwitchChannelControl:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->benableSwitchChannelControl:Z

    iput-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->bSwitchChannelControl:Z

    goto :goto_0
.end method

.method private intentSetDTVProgram()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, -0x1

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "ProgramNum"

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v5, "ServiceType"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RootActivity ========>>>intentSetDTVProgram="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    if-lez v0, :cond_0

    const/4 v5, 0x4

    if-ge v0, v5, :cond_0

    const-string v5, "ServiceType"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {v8}, Lcom/konka/tvsettings/RootActivity;->setIsShowSourceInfo(Z)V

    const/4 v5, 0x1

    sput-boolean v5, Lcom/konka/tvsettings/RootActivity;->m_bIsPVRStart:Z

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v2

    :try_start_0
    invoke-interface {v2}, Lcom/konka/kkinterface/tv/EpgDesk;->execEpgTimerAction()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private isReadyShowScreenSaver()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getBurnInMode()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsScreenSaverOn:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isShowRadio()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v3, v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    iget-short v3, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v4, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_RADIO:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isSubInputSourceExistAndDTVorATV()Z
    .locals 2

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->getIsPipOn()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTVInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v0, 0x0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private onResumeInit()V
    .locals 2

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->getColorWheelMode()Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;->MODE_DEMO:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bColorWheelDemon:Z

    :goto_0
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->getScreenSaveModeStatus()Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsScreenSaverOn:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "m_bEnergySavingDemon=="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->m_bEnergySavingDemon:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " m_bIsScreenSaverOn=="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsScreenSaverOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "m_bColorWheelDeomon===="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->m_bColorWheelDemon:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bColorWheelDemon:Z

    goto :goto_0
.end method

.method private onStartInit()V
    .locals 1

    const-string v0, "initializing in RootActiviyt"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bEnergySavingDemon:Z

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->SyncUserSettingDB()V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->TVInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-void
.end method

.method private open3DForFramePacking()V
    .locals 6

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "open3DForFramePacking:: eS3dMode="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v1, v4, :cond_0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v1, v4, :cond_1

    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/RootActivity;->sendBroadcast(Landroid/content/Intent;)V

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private openAlTimeShift()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "start the activity=====PvrActivity"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private resumeSubtitle()V
    .locals 9

    const/4 v8, -0x1

    const-string v6, "TvSetting"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/konka/tvsettings/RootActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v6, "subtitlePos"

    invoke-interface {v4, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iget-object v6, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    sget-object v7, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v6, v7, :cond_1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "RootActivity ========>>>subtitlePosLive="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "->"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-short v7, v5, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "->"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-short v7, v5, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    if-eqz v5, :cond_0

    iget v6, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    if-eq v6, v8, :cond_0

    iget v6, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    iget-short v7, v5, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    if-gt v6, v7, :cond_0

    iget v6, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    iget-short v7, v5, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    add-int/lit8 v7, v7, 0x1

    if-eq v6, v7, :cond_0

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeSubtitle()Z

    iget v6, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v1, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->openSubtitle(I)Z

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v6, "subtitlePos"

    iget v7, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iput v8, p0, Lcom/konka/tvsettings/RootActivity;->subtitlePosLive:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v6, "!!!!!!!!@@@ resumeSubtitle ===>>>now is non DTV "

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private saveAndExit()V
    .locals 3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybacking()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlayback()V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlaybackLoop()V

    :cond_0
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "RootActivity=====>>>stopTimeShiftRecord666\n"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public static setIsShowSourceInfo(Z)V
    .locals 2
    .param p0    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IsShowSourceInfo ========>>>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sput-boolean p0, Lcom/konka/tvsettings/RootActivity;->m_bIsShowSourceInfo:Z

    return-void
.end method

.method private setUsbEjectListener()V
    .locals 2

    new-instance v0, Lcom/konka/tvsettings/RootActivity$9;

    invoke-direct {v0, p0, p0}, Lcom/konka/tvsettings/RootActivity$9;-><init>(Lcom/konka/tvsettings/RootActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    new-instance v1, Lcom/konka/tvsettings/RootActivity$10;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/RootActivity$10;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter;->setUSBListener(Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;)V

    return-void
.end method

.method private showSettingMenu(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    if-eqz p1, :cond_0

    const-string v0, "menu_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->showMenu:Landroid/content/Intent;

    const-string v1, "menu_name"

    const-string v2, "menu_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->rShowSettingMenu:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/RootActivity$MyHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->rShowSettingMenu:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/konka/tvsettings/RootActivity$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->setIntent(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private signalLockHandle(Landroid/os/Message;)V
    .locals 14
    .param p1    # Landroid/os/Message;

    const/4 v10, 0x1

    const/4 v1, 0x0

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v9, "MsgSource"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v9, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v9

    if-ne v8, v9, :cond_1

    iget-boolean v9, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    if-nez v9, :cond_0

    new-instance v3, Landroid/content/Intent;

    const-string v9, "com.konka.tv.action.SIGNAL_LOCK"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/RootActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->broadSignalLock()V

    :cond_0
    iput-boolean v10, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    iget-object v9, p0, Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    invoke-virtual {v9}, Lcom/konka/tvsettings/common/CountDownTimer;->start()V

    const-string v9, "==========EV_SIGNAL_LOCK==========="

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_1
    sget-object v9, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v9

    if-ne v8, v9, :cond_3

    iget-boolean v9, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    if-eqz v9, :cond_2

    const-string v9, "=!!!!!!!!!EV_SIGNAL_UNLOCK!!!!!!!!!!=="

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v9, "com.konka.tv.action.SIGNAL_UNLOCK"

    invoke-direct {v2, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/RootActivity;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z

    iget-boolean v9, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsScreenSaverOn:Z

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v9

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getBurnInMode()Z

    move-result v9

    if-nez v9, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    if-eqz v9, :cond_2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    const/4 v10, 0x1

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/16 v12, 0x378

    sget-object v13, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9, v10, v11, v12, v13}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    iget-boolean v9, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    invoke-virtual {v9}, Lcom/konka/tvsettings/common/CountDownTimer;->start()V

    :cond_3
    iget-boolean v9, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    if-eqz v9, :cond_4

    sget-object v9, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v9

    if-ne v8, v9, :cond_4

    if-eqz v0, :cond_4

    const-string v9, "DeskTvEventListener"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "Signal lock and show source info"

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    const-class v9, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v7, p0, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-static {}, Lcom/konka/tvsettings/RootActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v9

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    iget-object v9, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    const/16 v10, 0x1f4

    invoke-static {p0, v9, v7, v10}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    :cond_4
    :goto_1
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    iget-object v9, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    const/16 v10, 0x7d0

    invoke-static {p0, v9, v7, v10}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private startCheckPwdActivity()V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isCheckPwdWinShown:Z

    if-nez v1, :cond_0

    sput-boolean v2, Lcom/konka/tvsettings/popup/CheckParentalPwd;->b_isCheckPwdWinShown:Z

    sput-boolean v2, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "lockType"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "DTV lock"

    const-string v2, "Try to open check parental pwd window"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/RootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method private stopPVRByLock()V
    .locals 3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "\n ========>>>>>stopTVRecordByLock \n"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PvrManager;->isRecording()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.tv.action.PVR_STOP_RECORD"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/RootActivity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public channelDown()V
    .locals 3

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->isSubInputSourceExistAndDTVorATV()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->isTvKeyPadAction:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.CHANNEL_DOWN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    :cond_2
    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->benableSwitchChannelControl:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->bSwitchChannelControl:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->bSwitchChannelControl:Z

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->doProgramDown()V

    iget v1, p0, Lcom/konka/tvsettings/RootActivity;->iTimeChannelInterval:I

    mul-int/lit16 v1, v1, 0x3e8

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/RootActivity;->switchChannelTimer(I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->doProgramDown()V

    goto :goto_0
.end method

.method public channelUp()V
    .locals 3

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->isSubInputSourceExistAndDTVorATV()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->isTvKeyPadAction:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.CHANNEL_UP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    :cond_2
    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->benableSwitchChannelControl:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->bSwitchChannelControl:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->bSwitchChannelControl:Z

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->doProgramUp()V

    iget v1, p0, Lcom/konka/tvsettings/RootActivity;->iTimeChannelInterval:I

    mul-int/lit16 v1, v1, 0x3e8

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/RootActivity;->switchChannelTimer(I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->doProgramUp()V

    goto :goto_0
.end method

.method public enterFullSrc()V
    .locals 6

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.konka.SwitchSource"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "SwitchSourceByTVHotKey"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v2, v4, v3

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/konka/tvsettings/RootActivity;->setTVInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Landroid/content/Context;)V

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/RootActivity;->setIntent(Landroid/content/Intent;)V

    const-string v4, "========>>>start source info activity"

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v4, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v1, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    const/16 v5, 0x1f4

    invoke-static {p0, v4, v1, v5}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public isTvKeyPadAction(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "Konka Smart TV Keypad"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-boolean v1, p0, Lcom/konka/tvsettings/RootActivity;->isTvKeyPadAction:Z

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v1, v0

    :cond_1
    :goto_0
    return v1

    :pswitch_0
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v3, 0x16

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0, v1}, Landroid/media/AudioManager;->adjustMasterVolume(II)V

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    :pswitch_1
    sput-boolean v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->channelUp()V

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->isTvKeyPadAction:Z

    goto :goto_0

    :pswitch_2
    sput-boolean v0, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->channelDown()V

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->isTvKeyPadAction:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "keycode"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "keyevent"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/RootActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const/16 v4, 0x1001

    const/4 v2, 0x0

    const/4 v11, 0x1

    const-string v0, "onCreate"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030044    # com.konka.tvsettings.R.layout.root

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->setContentView(I)V

    const v0, 0x7f0701c6    # com.konka.tvsettings.R.id.radiobgview

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->createSurfaceView()V

    new-instance v0, Lcom/konka/tvsettings/RootActivity$MyHandler;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$MyHandler;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sput-boolean v11, Lcom/konka/tvsettings/RootActivity;->m_bIsStartMsrv:Z

    new-instance v0, Lcom/konka/tvsettings/common/CountDownTimer;

    const/4 v1, 0x3

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/common/CountDownTimer;-><init>(IILandroid/os/Handler;II)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$RootMenuHandler;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/TVRootApp;->setRootmenuHandler(Lcom/konka/tvsettings/RootActivity$RootMenuHandler;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getMhlManager()Lcom/mstar/android/tvapi/common/MhlManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    new-instance v0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;-><init>(Lcom/konka/tvsettings/RootActivity;Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuEventReceiver:Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;

    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "com.konka.tv.action.KEYDOWN"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.konka.tv.action.SYNCDB"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.konka.tv.action.SHOWINFO"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.konka.tv.action.DTV_SEARCH_START"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.konka.tv.action.DTV_SEARCH_END"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.konka.stop.alwaytimeshiftthread"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.konka.start.alwaytimeshiftthread"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuEventReceiver:Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;

    invoke-virtual {p0, v0, v8}, Lcom/konka/tvsettings/RootActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->intentSetDTVProgram()V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCEC()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCecManager()Lcom/mstar/android/tvapi/common/CecManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    :cond_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isPVRStandby"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    const-string v0, "qhc"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "oncreate isPVRStandby="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v9, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->standbyHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v7, v0, :cond_2

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v7, v0, :cond_2

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v7, v0, :cond_2

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v7, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->rOpenFramepacking:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/konka/tvsettings/RootActivity$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    new-instance v10, Lcom/konka/tvsettings/common/PreferancesTools;

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v10, v0}, Lcom/konka/tvsettings/common/PreferancesTools;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, Lcom/konka/tvsettings/common/PreferancesTools;->getInfoMenuStatus(Z)Z

    move-result v0

    sput-boolean v0, Lcom/konka/tvsettings/RootActivity;->m_bIsShowSourceInfo:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RootActivity isShowInfoMenu="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v1, Lcom/konka/tvsettings/RootActivity;->m_bIsShowSourceInfo:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-boolean v0, Lcom/konka/tvsettings/RootActivity;->m_bIsShowSourceInfo:Z

    if-nez v0, :cond_4

    invoke-virtual {v10, v11}, Lcom/konka/tvsettings/common/PreferancesTools;->setShowInfoMenuStatus(Z)V

    :cond_4
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mAudioManager:Landroid/media/AudioManager;

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->setUsbEjectListener()V

    :cond_5
    new-instance v0, Lcom/konka/tvsettings/RootActivity$CiHandler;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$CiHandler;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->ciHandler:Lcom/konka/tvsettings/RootActivity$CiHandler;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->ciHandler:Lcom/konka/tvsettings/RootActivity$CiHandler;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->attachHandler(Landroid/os/Handler;)V

    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "onDestroy"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuEventReceiver:Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/RootActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDemoManagerInstance()Lcom/konka/kkinterface/tv/DemoDesk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/DemoDesk;->forceThreadSleep(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->releaseHandler(I)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iput-boolean v2, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/4 v2, 0x1

    const-string v3, "Tvapp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " onKeyDown  keyCode = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCEC()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, p1}, Lcom/konka/tvsettings/RootActivity;->sendCecKey(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    :sswitch_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/MhlManager;->CbusStatus()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/MhlManager;->IsMhlPortInUse()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "aaron"

    const-string v4, "mhl key in"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    invoke-virtual {v3, p1, v6}, Lcom/mstar/android/tvapi/common/MhlManager;->IRKeyProcess(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "aaron"

    const-string v4, "mhl key out"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v3, 0x8c

    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v3

    if-eqz v3, :cond_4

    const/16 v3, 0xa6

    if-eq p1, v3, :cond_3

    const/16 v3, 0xa7

    if-eq p1, v3, :cond_3

    const/16 v3, 0x43

    if-ne p1, v3, :cond_4

    :cond_3
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v4, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    if-eqz v3, :cond_4

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "========>>>stop PVR TimeShift"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->stopAlTimeShiftThread()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_1
    sparse-switch p1, :sswitch_data_0

    invoke-virtual {p0, p2}, Lcom/konka/tvsettings/RootActivity;->isTvKeyPadAction(Landroid/view/KeyEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToSetBurnMode(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToAutoSearch(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToMenuPage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToSourcePage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToEpgPage(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToSourceInfo(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToChannelInputPage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToAudioLangPage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToSubtitleLangPage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToProgrameListInfo(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToFavorateListInfo(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToTestMenuPage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToSleepMode(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToPvrPage(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToPvrBrowserPage(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToHotKey(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToPictureModePage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToScreenModePage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToSoundModePage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goTo3DSettingPage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToTeletextPage(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnbaleScreenShot()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {p0, p1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToScreenCapture(Landroid/app/Activity;I)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_5
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :sswitch_1
    sput-boolean v6, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->channelUp()V

    goto/16 :goto_0

    :sswitch_2
    sput-boolean v6, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->channelDown()V

    goto/16 :goto_0

    :sswitch_3
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    if-eq v3, v4, :cond_6

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    if-eq v3, v4, :cond_6

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    if-ne v3, v4, :cond_0

    :cond_6
    sput-boolean v6, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->doProgramReturn()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x43 -> :sswitch_3
        0xa6 -> :sswitch_1
        0xa7 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/konka/tvsettings/RootActivity;->setIntent(Landroid/content/Intent;)V

    const-string v1, "isPVRStandby"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "qhc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onNewIntent isPVRStandby="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->standbyHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    invoke-virtual {v0}, Lcom/konka/tvsettings/common/CountDownTimer;->stop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    :cond_0
    const-string v0, "OnPause=================="

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->getStickerDemoStatus()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v1, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTimer:Ljava/util/Timer;

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    iput-object v1, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTask:Ljava/util/TimerTask;

    :cond_2
    iget v0, p0, Lcom/konka/tvsettings/RootActivity;->STICKER_COUNT_TIME:I

    iput v0, p0, Lcom/konka/tvsettings/RootActivity;->STICKER_COUNT_DOWN:I

    :cond_3
    return-void
.end method

.method protected onRestart()V
    .locals 6

    const-string v2, "onRestart!!!!!!!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/RootActivity;->showSettingMenu(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk;->loadEssentialDataFromDB()V

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->intentSetDTVProgram()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->rOpenFramepacking:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v3, v4, v5}, Lcom/konka/tvsettings/RootActivity$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/USBDiskSelecter;->registerUSBDetector()V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 8

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "onResume"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/konka/tvsettings/SwitchMenuHelper;->setActivity(Landroid/app/Activity;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->onResumeInit()V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->SyncUserSettingDB()V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->refreshVideoPara()Z

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    invoke-virtual {v0}, Lcom/konka/tvsettings/common/CountDownTimer;->reStart()V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    const-string v0, "RootActivity========>>>onResume"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->intentSetDTVProgram()V

    sget-boolean v0, Lcom/konka/tvsettings/RootActivity;->m_bIsStartMsrv:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->startMsrv()Z

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDemoManagerInstance()Lcom/konka/kkinterface/tv/DemoDesk;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/konka/kkinterface/tv/DemoDesk;->forceThreadSleep(Z)V

    sput-boolean v3, Lcom/konka/tvsettings/RootActivity;->m_bIsStartMsrv:Z

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    if-eqz v0, :cond_1

    const-string v0, "Tvapp"

    const-string v1, " reset  myHandler "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    invoke-interface {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->ciHandler:Lcom/konka/tvsettings/RootActivity$CiHandler;

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    int-to-short v1, v1

    if-eq v0, v1, :cond_2

    const-string v0, "radio bg set finish"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iput-boolean v4, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/RootActivity;->showSettingMenu(Landroid/content/Intent;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/RootActivity$11;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/RootActivity$11;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->GetInstallationFlag()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v0

    if-nez v0, :cond_8

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/RootActivity;->startActivity(Landroid/content/Intent;)V

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->startAlTimeShiftThread()V

    :cond_4
    const-string v0, "onResume is end"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->setBackLightRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/RootActivity$MyHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->setBackLightRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/konka/tvsettings/RootActivity$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->getStickerDemoStatus()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTimer:Ljava/util/Timer;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTimer:Ljava/util/Timer;

    :cond_5
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTask:Ljava/util/TimerTask;

    if-nez v0, :cond_6

    new-instance v0, Lcom/konka/tvsettings/RootActivity$12;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$12;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTask:Ljava/util/TimerTask;

    :cond_6
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->mStickerCountDownTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    :cond_7
    return-void

    :cond_8
    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getFactoryCusDefSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    move-result-object v7

    iget v0, v7, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    if-eq v0, v4, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v0, v3}, Lcom/konka/kkinterface/tv/SettingDesk;->SetInstallationFlag(Z)Z

    goto :goto_0
.end method

.method protected onStart()V
    .locals 4

    const/4 v3, 0x1

    const-string v1, "On Start!!!!!!!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/konka/tvsettings/RootActivity;->m_bIsFirstDoingAfterStart:Z

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->onStartInit()V

    sget-boolean v1, Lcom/konka/tvsettings/RootActivity;->m_bIsPVRStart:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/tvsettings/RootActivity;->m_bIsPVRStart:Z

    :goto_0
    const-string v1, "setInputsource over,start show info now!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-boolean v1, Lcom/konka/tvsettings/RootActivity;->m_bIsShowSourceInfo:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SettingDesk;->GetInstallationFlag()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "========>>>start source info activity"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    const/16 v2, 0x1f4

    invoke-static {p0, v1, v0, v2}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    :goto_1
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iput-boolean v3, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityActive:Z

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity;->TVInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/RootActivity;->setTVInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    sput-boolean v3, Lcom/konka/tvsettings/RootActivity;->m_bIsShowSourceInfo:Z

    goto :goto_1
.end method

.method protected onStop()V
    .locals 5

    const-string v3, "On Stop"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeSubtitle()Z

    :cond_0
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v4, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    if-eqz v3, :cond_2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybacking()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "========>>>stop PVR TimeShift Playback"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlayback()V

    :cond_1
    const-string v3, "========>>>stop PVR TimeShift"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->stopAlTimeShiftThread()V

    :cond_3
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/tvsettings/RootActivity;->m_bRootActivityActive:Z

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.konka.action.STATUSBAR_CONTROL"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "show"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/RootActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    invoke-static {p0}, Lcom/konka/tvsettings/SwitchMenuHelper;->stopSwitchInput(Landroid/app/Activity;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/konka/tvsettings/RootActivity;->UnregisterUSBMountlister()V

    :cond_4
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void

    :cond_5
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->isRecording()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "========>>>stop PVR Recording"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->stopRecord()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_6
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybacking()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "========>>>stop PVR Playback"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlayback()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method public sendCecKey(I)Z
    .locals 7
    .param p1    # I

    const/4 v3, 0x1

    const-string v4, "wangjian"

    const-string v5, "----------in sendCecKey----------------"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getCECFunctionModeStatus()Z

    move-result v0

    const-string v4, "liying"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " CEC Setting: cecstatus "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    const-string v4, "wangjian"

    const-string v5, "---------setting.cecStatus == 1----------------"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/tvsettings/RootActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    aget v4, v4, v5

    sparse-switch v4, :sswitch_data_0

    const-string v3, "wangjian"

    const-string v4, "------------switch default------------------"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v3, 0x0

    :goto_1
    return v3

    :sswitch_0
    const-string v4, "wangjian"

    const-string v5, "-----------HDMI1234---------------"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "wangjian"

    const-string v5, "--------------m_serviceProvider.getCommonManagerInstance().isHdmiSignalMode() == true---------------"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity;->cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    invoke-virtual {v4, p1}, Lcom/mstar/android/tvapi/common/CecManager;->sendCecKey(I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "wangjian"

    const-string v5, "-------------cecManager.sendCecKey(keyCode)------------"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "RootActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "send Cec key,keyCode is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", tv don\'t handl the key"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :sswitch_1
    :try_start_1
    const-string v4, "wangjian"

    const-string v5, "----------DTVATV-----------------"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity;->cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    invoke-virtual {v4, p1}, Lcom/mstar/android/tvapi/common/CecManager;->sendCecKey(I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "RootActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "send Cec key,keyCode is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", tv don\'t handl the key"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1b -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method public setTVInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Landroid/content/Context;)V
    .locals 22
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Landroid/content/Context;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "the TV inputsource is ==="

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/RootActivity;->TVInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "==="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v11

    iget v0, v11, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    move/from16 v19, v0

    if-eqz v19, :cond_5

    const/16 v19, 0x1

    :goto_0
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/RootActivity;->enableDoubleChannel:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/RootActivity;->enableDoubleChannel:Z

    move/from16 v19, v0

    if-eqz v19, :cond_b

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v19

    if-eqz v19, :cond_b

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v13

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v19

    if-eqz v19, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/mstar/android/tv/TvPipPopManager;->checkPipSupport(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    move-result v19

    if-nez v19, :cond_0

    new-instance v8, Landroid/content/Intent;

    const-string v19, "com.konka.hotkey.disablePip"

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v19, "MenuHolderPIP==>displaySubChannelFullScreen, disablePip"

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v19

    if-eqz v19, :cond_1

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/mstar/android/tv/TvPipPopManager;->checkPopSupport(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    move-result v19

    if-nez v19, :cond_1

    new-instance v8, Landroid/content/Intent;

    const-string v19, "com.konka.hotkey.disablePop"

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v19, "MenuHolderPIP==>displaySubChannelFullScreen, disablePop"

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    :cond_1
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v19

    if-eqz v19, :cond_2

    new-instance v8, Landroid/content/Intent;

    const-string v19, "com.konka.hotkey.disable3dDualView"

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v19, "MenuHolderPIP==>displaySubChannelFullScreen, disable3dDualView"

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->disable3dDualView()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/mstar/android/tv/TvPipPopManager;->setDualViewOnFlag(Z)Z

    :cond_2
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v19

    if-eqz v19, :cond_9

    const/4 v10, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v19

    if-eqz v19, :cond_6

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    :goto_1
    const/16 v16, 0x780

    const/16 v6, 0x438

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/4 v15, 0x0

    const/4 v5, 0x0

    if-eqz v10, :cond_3

    iget v0, v10, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    move/from16 v16, v0

    iget v6, v10, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    :cond_3
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v19

    if-eqz v19, :cond_7

    div-int/lit8 v15, v16, 0x3

    div-int/lit8 v6, v6, 0x3

    move v5, v6

    sub-int v17, v16, v15

    const/16 v18, 0x0

    new-instance v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v3}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    move/from16 v0, v17

    iput v0, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    move/from16 v0, v18

    iput v0, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iput v15, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v5, v3, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "==========>>>>>333..."

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "..."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "..."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v3}, Lcom/mstar/android/tv/TvPipPopManager;->enablePipTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v12

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "==========>>>>>444..."

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "..."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_4
    :goto_2
    sget-object v19, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_d

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget v7, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v19

    sget-object v20, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v7, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    :goto_3
    return-void

    :cond_5
    const/16 v19, 0x0

    goto/16 :goto_0

    :cond_6
    :try_start_1
    const-string v19, "TvManger.getInstance() == null"

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_7
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v19

    if-eqz v19, :cond_8

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lcom/mstar/android/tv/TvPipPopManager;->enablePopTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    goto :goto_2

    :cond_8
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v19

    if-eqz v19, :cond_4

    move/from16 v15, v16

    move v5, v6

    new-instance v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v9}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    new-instance v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v14}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    move/from16 v0, v17

    iput v0, v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    move/from16 v0, v17

    iput v0, v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    move/from16 v0, v18

    iput v0, v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    move/from16 v0, v18

    iput v0, v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iput v15, v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v15, v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v5, v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iput v5, v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v9, v14}, Lcom/mstar/android/tv/TvPipPopManager;->enable3dDualView(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z

    goto/16 :goto_2

    :cond_9
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    if-ne v0, v1, :cond_a

    const-string v19, "RootActivity"

    const-string v20, "No need to switch source for current source is the same!!"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_a
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v19

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)V

    goto/16 :goto_2

    :cond_b
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    if-ne v0, v1, :cond_c

    const-string v19, "RootActivity"

    const-string v20, "No need to switch source for current source is the same!!"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_c
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v19

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)V

    goto/16 :goto_2

    :cond_d
    sget-object v19, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_f

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v19

    if-nez v19, :cond_e

    const-string v19, "liying"

    const-string v20, "========Current source DTV,get Channel manager error\n"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvplayCurrentProgram()Z

    goto/16 :goto_3

    :cond_f
    invoke-direct/range {p0 .. p1}, Lcom/konka/tvsettings/RootActivity;->isTVInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    move-result v19

    if-eqz v19, :cond_10

    const-string v19, "liying"

    const-string v20, "========Current source DTV,222222 \n"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_10
    const-string v19, "the currren input source is not any part of TV,so swith it to ATV for default"

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v19

    sget-object v20, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/16 v21, 0x1

    invoke-interface/range {v19 .. v21}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget v7, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v19

    sget-object v20, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v7, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    goto/16 :goto_3
.end method

.method public startAlTimeShiftThread()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->alTimeShiftThread:Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->alTimeShiftThread:Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/RootActivity;->bExitThread:Z

    const-string v0, "RootActivity=========StartAlTimeshift \n"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->alTimeShiftThread:Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;

    invoke-virtual {v0}, Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;->start()V

    :cond_0
    return-void
.end method

.method public stopAlTimeShiftThread()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity;->alTimeShiftThread:Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;

    if-eqz v0, :cond_0

    const-string v0, "RootActivity=========StopAlTimeshift \n"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/RootActivity;->bExitThread:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/RootActivity;->alTimeShiftThread:Lcom/konka/tvsettings/RootActivity$AlTimeShiftThread;

    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "surfaceChanged called"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "surfaceCreated called"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/RootActivity$14;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/RootActivity$14;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "surfaceDestroyed called"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public switchChannelTimer(I)V
    .locals 4
    .param p1    # I

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    new-instance v1, Lcom/konka/tvsettings/RootActivity$13;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/RootActivity$13;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method
