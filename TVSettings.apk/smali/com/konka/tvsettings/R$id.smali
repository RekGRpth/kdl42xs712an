.class public final Lcom/konka/tvsettings/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final FunctionMenuLayout:I = 0x7f0700b5

.field public static final ItemImage:I = 0x7f0700c5

.field public static final ItemText:I = 0x7f0700c6

.field public static final Pic_adjust_layout:I = 0x7f0700f0

.field public static final ProgramMenuLayout:I = 0x7f07016c

.field public static final StandyBy:I = 0x7f070217

.field public static final TimeMenuLayout:I = 0x7f0701fe

.field public static final action_settings:I = 0x7f07023b

.field public static final ad_container:I = 0x7f070004

.field public static final ad_list_item:I = 0x7f070006

.field public static final ad_text_option:I = 0x7f070007

.field public static final adv_set_title:I = 0x7f0701e5

.field public static final audio_language_img_back:I = 0x7f070020

.field public static final audio_language_list_view:I = 0x7f07001f

.field public static final audio_language_title:I = 0x7f07001e

.field public static final but1:I = 0x7f070154

.field public static final button_cha_autotuning_choosecountry_australia:I = 0x7f070028

.field public static final button_cha_autotuning_choosecountry_austria:I = 0x7f070029

.field public static final button_cha_autotuning_choosecountry_beligum:I = 0x7f07002a

.field public static final button_cha_autotuning_choosecountry_bulgaral:I = 0x7f07002b

.field public static final button_cha_autotuning_choosecountry_croatia:I = 0x7f07002c

.field public static final button_cha_autotuning_choosecountry_czech:I = 0x7f07002d

.field public static final button_cha_autotuning_choosecountry_denmark:I = 0x7f07002e

.field public static final button_cha_autotuning_choosecountry_finland:I = 0x7f07002f

.field public static final button_cha_autotuning_choosecountry_france:I = 0x7f070030

.field public static final cancel:I = 0x7f070218

.field public static final cec_control_list_item:I = 0x7f070034

.field public static final cec_control_list_view:I = 0x7f070037

.field public static final cec_control_text_option:I = 0x7f070035

.field public static final cec_control_title:I = 0x7f070036

.field public static final cec_devicelist:I = 0x7f07003a

.field public static final cec_devicelist_container:I = 0x7f070038

.field public static final cec_devicelist_title:I = 0x7f070039

.field public static final cec_menu_container:I = 0x7f07003c

.field public static final cec_setting_title:I = 0x7f07003d

.field public static final check_pwd1:I = 0x7f07005f

.field public static final check_pwd2:I = 0x7f070060

.field public static final check_pwd3:I = 0x7f070061

.field public static final check_pwd4:I = 0x7f070062

.field public static final ci_bottom:I = 0x7f070069

.field public static final ci_password:I = 0x7f070067

.field public static final ci_subtitle:I = 0x7f070066

.field public static final ci_title:I = 0x7f070065

.field public static final cimmi_list_view:I = 0x7f07006e

.field public static final cimmi_subtitle_one:I = 0x7f07006c

.field public static final cimmi_subtitle_two:I = 0x7f07006d

.field public static final cimmi_text_end:I = 0x7f07006f

.field public static final cimmi_title:I = 0x7f07006b

.field public static final clock_menu_auto_mode_option:I = 0x7f0701f9

.field public static final clock_menu_auto_setting:I = 0x7f0701f8

.field public static final clock_menu_date_setting:I = 0x7f0701fa

.field public static final clock_menu_date_setting_option:I = 0x7f0701fb

.field public static final clock_menu_time_setting:I = 0x7f0701fc

.field public static final clock_menu_time_setting_option:I = 0x7f0701fd

.field public static final clock_menu_title:I = 0x7f0701f7

.field public static final color_wheel_imageView:I = 0x7f070072

.field public static final color_wheel_item_blue:I = 0x7f070079

.field public static final color_wheel_item_green:I = 0x7f070076

.field public static final color_wheel_item_red:I = 0x7f070073

.field public static final color_wheel_layout_container:I = 0x7f070071

.field public static final color_wheel_pb_blue:I = 0x7f07007a

.field public static final color_wheel_pb_green:I = 0x7f070077

.field public static final color_wheel_pb_red:I = 0x7f070074

.field public static final color_wheel_textView:I = 0x7f07007c

.field public static final color_wheel_textView_inverted:I = 0x7f07007d

.field public static final color_wheel_tv_blue:I = 0x7f07007b

.field public static final color_wheel_tv_green:I = 0x7f070078

.field public static final color_wheel_tv_red:I = 0x7f070075

.field public static final confirm_pwd1:I = 0x7f0701de

.field public static final confirm_pwd2:I = 0x7f0701df

.field public static final confirm_pwd3:I = 0x7f0701e0

.field public static final confirm_pwd4:I = 0x7f0701e1

.field public static final device_item:I = 0x7f07003b

.field public static final dtv_source_info_more_info:I = 0x7f07013b

.field public static final editText:I = 0x7f070070

.field public static final editText1:I = 0x7f0701ba

.field public static final ele_page_tag:I = 0x7f0700a4

.field public static final engine_frame_layout:I = 0x7f07023a

.field public static final epg_popwin_cancel_btn:I = 0x7f070151

.field public static final epg_popwin_eventinfo_content:I = 0x7f070150

.field public static final epg_popwin_eventinfo_title:I = 0x7f07014d

.field public static final epg_popwin_evetinfo_eventtitle:I = 0x7f07014e

.field public static final equalizer_menu:I = 0x7f0700a5

.field public static final function_adv_pri_audio_lang:I = 0x7f0700b1

.field public static final function_adv_pri_subtitle_lang:I = 0x7f0700b3

.field public static final function_adv_sec_subtitle_lang:I = 0x7f0700b4

.field public static final function_adv_secondary_audio_lang:I = 0x7f0700b2

.field public static final function_adv_title:I = 0x7f0700b0

.field public static final function_menu_container:I = 0x7f0700af

.field public static final gridview_mainpanel:I = 0x7f0700df

.field public static final hint_text:I = 0x7f070003

.field public static final hour:I = 0x7f070000

.field public static final image_left_arrow_hint:I = 0x7f070026

.field public static final image_right_arrow_hint:I = 0x7f070031

.field public static final imgbtn_history:I = 0x7f0701ee

.field public static final imgbtn_homepage:I = 0x7f0701f2

.field public static final imgbtn_keypad:I = 0x7f0701f0

.field public static final imgbtn_search:I = 0x7f0701ed

.field public static final imgbtn_setting:I = 0x7f0701ef

.field public static final imgbtn_usb:I = 0x7f0701ea

.field public static final imgbtn_wifi:I = 0x7f0701e9

.field public static final imgview_line1:I = 0x7f0701ec

.field public static final imgview_line2:I = 0x7f0701f1

.field public static final input_item_container:I = 0x7f0700c7

.field public static final layout:I = 0x7f07021d

.field public static final layout_focus_bg:I = 0x7f0700dc

.field public static final layout_mainpanel:I = 0x7f0700dd

.field public static final layout_movingpanel:I = 0x7f0700de

.field public static final layout_statecolumn:I = 0x7f0701e6

.field public static final lcninfo:I = 0x7f070186

.field public static final linear_layout_root:I = 0x7f070214

.field public static final linear_picture_setting_pcadj_auto_adjuct:I = 0x7f070113

.field public static final linearlayout_cec_devicelist:I = 0x7f070042

.field public static final linearlayout_cha_atvmanualtuning_channelnum:I = 0x7f07000a

.field public static final linearlayout_cha_atvmanualtuning_colorsystem:I = 0x7f07000d

.field public static final linearlayout_cha_atvmanualtuning_finetune:I = 0x7f070018

.field public static final linearlayout_cha_atvmanualtuning_frequency:I = 0x7f07001a

.field public static final linearlayout_cha_atvmanualtuning_skip:I = 0x7f070013

.field public static final linearlayout_cha_atvmanualtuning_soundsystem:I = 0x7f070010

.field public static final linearlayout_cha_atvmanualtuning_starttuning:I = 0x7f070016

.field public static final linearlayout_cha_autotuning_tuningtype:I = 0x7f070022

.field public static final linearlayout_cha_dataprogram:I = 0x7f070051

.field public static final linearlayout_cha_dtvmanualtuning_channelnum:I = 0x7f07008a

.field public static final linearlayout_cha_dtvmanualtuning_frequency:I = 0x7f07008c

.field public static final linearlayout_cha_dtvmanualtuning_modulation:I = 0x7f07008f

.field public static final linearlayout_cha_dtvmanualtuning_signalquality:I = 0x7f07009a

.field public static final linearlayout_cha_dtvmanualtuning_signalquality_val:I = 0x7f07009c

.field public static final linearlayout_cha_dtvmanualtuning_signalstrength:I = 0x7f070097

.field public static final linearlayout_cha_dtvmanualtuning_signalstrength_val:I = 0x7f070099

.field public static final linearlayout_cha_dtvmanualtuning_starttuning:I = 0x7f070095

.field public static final linearlayout_cha_dtvmanualtuning_symbol:I = 0x7f070092

.field public static final linearlayout_cha_dtvmanualtuning_tuningresult:I = 0x7f07009d

.field public static final linearlayout_cha_dtvprogram:I = 0x7f070049

.field public static final linearlayout_cha_exittune:I = 0x7f0700ac

.field public static final linearlayout_cha_mainlinear:I = 0x7f070043

.field public static final linearlayout_cha_radioprogram:I = 0x7f07004d

.field public static final linearlayout_cha_reset_hint:I = 0x7f0701c2

.field public static final linearlayout_cha_tuningprogress:I = 0x7f070055

.field public static final linearlayout_cha_tuningprogress_text:I = 0x7f070056

.field public static final linearlayout_cha_tvprogram:I = 0x7f070045

.field public static final linearlayout_ci_menu:I = 0x7f070064

.field public static final linearlayout_content:I = 0x7f070008

.field public static final linearlayout_function_ARC:I = 0x7f070041

.field public static final linearlayout_function_Always_time_shift:I = 0x7f0700c2

.field public static final linearlayout_function_DemoMode:I = 0x7f0700bb

.field public static final linearlayout_function_StickerDemo:I = 0x7f0700ba

.field public static final linearlayout_function_audio_only:I = 0x7f0700c3

.field public static final linearlayout_function_cec:I = 0x7f0700bc

.field public static final linearlayout_function_cec_autopoweroff:I = 0x7f070040

.field public static final linearlayout_function_cec_autostandby:I = 0x7f07003f

.field public static final linearlayout_function_cec_control:I = 0x7f0700bd

.field public static final linearlayout_function_cec_function:I = 0x7f07003e

.field public static final linearlayout_function_dynamic_denoise:I = 0x7f0700be

.field public static final linearlayout_function_game_mode:I = 0x7f0700c1

.field public static final linearlayout_function_lock:I = 0x7f0700b7

.field public static final linearlayout_function_osdsetting:I = 0x7f0700b6

.field public static final linearlayout_function_pvr:I = 0x7f0700b8

.field public static final linearlayout_function_reset:I = 0x7f0700c4

.field public static final linearlayout_function_rgb_range:I = 0x7f0700bf

.field public static final linearlayout_function_screensaver:I = 0x7f0700b9

.field public static final linearlayout_function_ssu:I = 0x7f0700c0

.field public static final linearlayout_install_guide_antennatype:I = 0x7f0700ce

.field public static final linearlayout_install_guide_country:I = 0x7f0700cb

.field public static final linearlayout_install_guide_language:I = 0x7f0700c8

.field public static final linearlayout_install_guide_tuning:I = 0x7f0700d1

.field public static final linearlayout_program_antennatype:I = 0x7f07016e

.field public static final linearlayout_program_antennatype_option:I = 0x7f07016f

.field public static final linearlayout_program_antennatype_parent:I = 0x7f07016d

.field public static final linearlayout_program_atvmanualtuning:I = 0x7f070173

.field public static final linearlayout_program_autotuning:I = 0x7f070170

.field public static final linearlayout_program_ci_information:I = 0x7f070179

.field public static final linearlayout_program_dtvmanualtuning:I = 0x7f070174

.field public static final linearlayout_program_edit:I = 0x7f070177

.field public static final linearlayout_program_epg:I = 0x7f070172

.field public static final linearlayout_program_lcn_arrange:I = 0x7f070176

.field public static final linearlayout_program_lcnarrange_parent:I = 0x7f070175

.field public static final linearlayout_program_signal_info:I = 0x7f070178

.field public static final linearlayout_sound_equalizer10khz:I = 0x7f0700aa

.field public static final linearlayout_sound_equalizer120hz:I = 0x7f0700a6

.field public static final linearlayout_sound_equalizer1_5khz:I = 0x7f0700a8

.field public static final linearlayout_sound_equalizer500hz:I = 0x7f0700a7

.field public static final linearlayout_sound_equalizer5khz:I = 0x7f0700a9

.field public static final linearlayout_sound_srsmenu:I = 0x7f0701e4

.field public static final linearlayout_time_clock:I = 0x7f0701ff

.field public static final linearlayout_time_offtime:I = 0x7f070202

.field public static final linearlayout_time_ontime:I = 0x7f070201

.field public static final linearlayout_time_sleeptimer:I = 0x7f070203

.field public static final linearlayout_time_timezone:I = 0x7f070204

.field public static final linearlayout_tuning_hint:I = 0x7f07005e

.field public static final linearmenu:I = 0x7f0701a0

.field public static final linearprogress:I = 0x7f07019b

.field public static final lineartitle:I = 0x7f070198

.field public static final listview_ci_card:I = 0x7f070068

.field public static final ll_toastwarning:I = 0x7f070219

.field public static final lock_adv_blockprogram:I = 0x7f0700d8

.field public static final lock_adv_childlock:I = 0x7f0700da

.field public static final lock_adv_locksystem:I = 0x7f0700d6

.field public static final lock_adv_parentalguidance:I = 0x7f0700d9

.field public static final lock_adv_setpassword:I = 0x7f0700d7

.field public static final lock_menu_container:I = 0x7f0700d4

.field public static final lock_menu_title:I = 0x7f0700d5

.field public static final minute:I = 0x7f070001

.field public static final mtsType:I = 0x7f0700e6

.field public static final mts_audio_info:I = 0x7f0700e0

.field public static final mts_audio_info_left:I = 0x7f0700e4

.field public static final mts_audio_info_right:I = 0x7f0700e5

.field public static final mts_audio_info_text:I = 0x7f0700e1

.field public static final mts_audio_info_type:I = 0x7f0700e2

.field public static final mts_audio_info_vi:I = 0x7f0700e3

.field public static final new_pwd1:I = 0x7f0701da

.field public static final new_pwd2:I = 0x7f0701db

.field public static final new_pwd3:I = 0x7f0701dc

.field public static final new_pwd4:I = 0x7f0701dd

.field public static final offtime_menu_offtimeswitch:I = 0x7f070208

.field public static final offtime_menu_settime:I = 0x7f070209

.field public static final offtime_menu_settime_option:I = 0x7f07020a

.field public static final offtime_menu_title:I = 0x7f070207

.field public static final old_pwd1:I = 0x7f0701d6

.field public static final old_pwd2:I = 0x7f0701d7

.field public static final old_pwd3:I = 0x7f0701d8

.field public static final old_pwd4:I = 0x7f0701d9

.field public static final ontime_menu_ontimeswitch:I = 0x7f07020d

.field public static final ontime_menu_settime:I = 0x7f07020e

.field public static final ontime_menu_settime_option:I = 0x7f07020f

.field public static final ontime_menu_source:I = 0x7f070210

.field public static final ontime_menu_source_leftarrow:I = 0x7f070211

.field public static final ontime_menu_source_option:I = 0x7f070212

.field public static final ontime_menu_source_rightarrow:I = 0x7f070213

.field public static final ontime_menu_title:I = 0x7f07020c

.field public static final osd_language_list_title:I = 0x7f0700e7

.field public static final osd_language_list_view:I = 0x7f0700e8

.field public static final osd_language_list_view_item:I = 0x7f0700e9

.field public static final osd_language_select_icon:I = 0x7f0700eb

.field public static final osd_language_text_option:I = 0x7f0700ea

.field public static final osd_setting_language:I = 0x7f0700ee

.field public static final osd_setting_menu_title:I = 0x7f0700ed

.field public static final osd_setting_time:I = 0x7f0700ef

.field public static final osdsetting_menu_container:I = 0x7f0700ec

.field public static final pic_adj_seekbar:I = 0x7f0700f5

.field public static final pic_adj_textlayout:I = 0x7f0700f1

.field public static final pic_adjoption_name:I = 0x7f0700f2

.field public static final pic_adjoption_value:I = 0x7f0700f3

.field public static final pic_menu_container:I = 0x7f0700fb

.field public static final pic_mod_item_container_layout:I = 0x7f0700f6

.field public static final pic_mod_item_left_arrow:I = 0x7f0700f8

.field public static final pic_mod_item_right_arrow:I = 0x7f0700fa

.field public static final pic_mod_item_textname:I = 0x7f0700f7

.field public static final pic_mod_item_textvalue:I = 0x7f0700f9

.field public static final picture_setting_adv_dynamic_denoise:I = 0x7f0700fd

.field public static final picture_setting_adv_title:I = 0x7f0700fc

.field public static final picture_setting_backlight:I = 0x7f070110

.field public static final picture_setting_brightness:I = 0x7f070109

.field public static final picture_setting_colortemp:I = 0x7f07010f

.field public static final picture_setting_contrast:I = 0x7f07010a

.field public static final picture_setting_hue:I = 0x7f07010d

.field public static final picture_setting_item1:I = 0x7f0700fe

.field public static final picture_setting_item1_left_arrow:I = 0x7f070100

.field public static final picture_setting_item1_name:I = 0x7f0700ff

.field public static final picture_setting_item1_right_arrow:I = 0x7f070102

.field public static final picture_setting_item1_value:I = 0x7f070101

.field public static final picture_setting_item2_name:I = 0x7f070103

.field public static final picture_setting_item2_sb_container:I = 0x7f070105

.field public static final picture_setting_item2_seekbar:I = 0x7f070106

.field public static final picture_setting_item2_value:I = 0x7f070104

.field public static final picture_setting_pc_adjust:I = 0x7f070111

.field public static final picture_setting_pcadj_auto_adjuct:I = 0x7f070114

.field public static final picture_setting_pcadj_auto_adjuct_title:I = 0x7f070112

.field public static final picture_setting_pcadj_hpos:I = 0x7f070115

.field public static final picture_setting_pcadj_phase:I = 0x7f070118

.field public static final picture_setting_pcadj_rate:I = 0x7f070117

.field public static final picture_setting_pcadj_vpos:I = 0x7f070116

.field public static final picture_setting_pic_mode:I = 0x7f070108

.field public static final picture_setting_pic_mode_container:I = 0x7f070107

.field public static final picture_setting_saturation:I = 0x7f07010b

.field public static final picture_setting_sharpness:I = 0x7f07010c

.field public static final picture_setting_zoom_mode:I = 0x7f07010e

.field public static final play_record_progress:I = 0x7f07019d

.field public static final player_ff:I = 0x7f0701a9

.field public static final player_pause:I = 0x7f0701a6

.field public static final player_play:I = 0x7f0701a3

.field public static final player_recorder:I = 0x7f0701a2

.field public static final player_recorder_icon:I = 0x7f0701a1

.field public static final player_rev:I = 0x7f0701a7

.field public static final player_slow:I = 0x7f0701ab

.field public static final player_stop:I = 0x7f0701a5

.field public static final player_time:I = 0x7f0701ad

.field public static final popup_channel_info_menu_no_dtv_info:I = 0x7f070119

.field public static final popup_channel_info_menu_tv_num:I = 0x7f07011a

.field public static final popup_dtv_source_info_tv_current_name:I = 0x7f070133

.field public static final popup_dtv_source_info_tv_current_time:I = 0x7f070132

.field public static final popup_dtv_source_info_tv_date:I = 0x7f07012f

.field public static final popup_dtv_source_info_tv_name:I = 0x7f07012e

.field public static final popup_dtv_source_info_tv_next_name:I = 0x7f070131

.field public static final popup_dtv_source_info_tv_next_time:I = 0x7f070130

.field public static final popup_dtv_source_info_tv_num:I = 0x7f07012d

.field public static final popup_img_item_container:I = 0x7f07011b

.field public static final popup_img_item_textview:I = 0x7f07011c

.field public static final popup_input_menu:I = 0x7f07011d

.field public static final popup_input_menu_container:I = 0x7f07011e

.field public static final popup_intell_control_menu_ll_bright:I = 0x7f070123

.field public static final popup_intell_control_menu_ll_contrast:I = 0x7f070125

.field public static final popup_intell_control_menu_ll_economization:I = 0x7f070121

.field public static final popup_intell_control_menu_tv_bright:I = 0x7f070122

.field public static final popup_intell_control_menu_tv_contrast:I = 0x7f070124

.field public static final popup_intell_control_menu_tv_economization:I = 0x7f070120

.field public static final popup_intell_control_menu_tv_economization_content:I = 0x7f07011f

.field public static final popup_no_signal_text:I = 0x7f070126

.field public static final popup_password_cancel_btn:I = 0x7f070063

.field public static final popup_program_menu_body:I = 0x7f070128

.field public static final popup_program_menu_list:I = 0x7f070129

.field public static final popup_program_menu_title:I = 0x7f070127

.field public static final popup_source_info_menu_dtv_info:I = 0x7f07012c

.field public static final popup_source_info_menu_no_dtv_info:I = 0x7f07012a

.field public static final popup_source_info_menu_tv_num:I = 0x7f07012b

.field public static final pre:I = 0x7f070152

.field public static final preview_image:I = 0x7f070153

.field public static final program_ad_title:I = 0x7f070005

.field public static final program_edit_data:I = 0x7f070164

.field public static final program_edit_favorite_img:I = 0x7f070166

.field public static final program_edit_img_delete:I = 0x7f07015f

.field public static final program_edit_img_edit:I = 0x7f07015e

.field public static final program_edit_img_favorite:I = 0x7f070161

.field public static final program_edit_img_lock:I = 0x7f07016b

.field public static final program_edit_img_move:I = 0x7f070160

.field public static final program_edit_img_skip:I = 0x7f07015d

.field public static final program_edit_list_view:I = 0x7f07015c

.field public static final program_edit_list_view_item_bg:I = 0x7f070162

.field public static final program_edit_lock_img:I = 0x7f070168

.field public static final program_edit_mov_img:I = 0x7f07016a

.field public static final program_edit_number:I = 0x7f070163

.field public static final program_edit_skip_img:I = 0x7f070167

.field public static final program_edit_source_img:I = 0x7f070165

.field public static final program_edit_ssl_img:I = 0x7f070169

.field public static final program_edit_text:I = 0x7f070155

.field public static final program_edit_title:I = 0x7f07015b

.field public static final program_epg_item_container:I = 0x7f070171

.field public static final program_favorite_edit_data:I = 0x7f07015a

.field public static final program_favorite_edit_number:I = 0x7f070159

.field public static final program_favorite_list_view:I = 0x7f070157

.field public static final program_favorite_list_view_item:I = 0x7f070158

.field public static final program_favorite_title:I = 0x7f070156

.field public static final progressbar_cha_tuningprogress:I = 0x7f07005c

.field public static final progressbar_cha_tuningprogress_below:I = 0x7f07005d

.field public static final progressbar_loopab:I = 0x7f07019e

.field public static final pvrPlaybacktext:I = 0x7f0701b6

.field public static final pvr_channel_textview:I = 0x7f070188

.field public static final pvr_dialog:I = 0x7f0701b8

.field public static final pvr_file_number:I = 0x7f07018b

.field public static final pvr_file_system_always_context:I = 0x7f070185

.field public static final pvr_file_system_always_layout:I = 0x7f070183

.field public static final pvr_file_system_always_timeshift:I = 0x7f070184

.field public static final pvr_file_system_format_context:I = 0x7f07017f

.field public static final pvr_file_system_format_layout:I = 0x7f07017d

.field public static final pvr_file_system_format_start:I = 0x7f07017e

.field public static final pvr_file_system_select_disk:I = 0x7f07017b

.field public static final pvr_file_system_speed_check:I = 0x7f070181

.field public static final pvr_file_system_speed_context:I = 0x7f070182

.field public static final pvr_file_system_speed_layout:I = 0x7f070180

.field public static final pvr_file_system_time_shift_size:I = 0x7f07017c

.field public static final pvr_file_system_title:I = 0x7f07017a

.field public static final pvr_gallery:I = 0x7f070192

.field public static final pvr_lcn_data:I = 0x7f07018e

.field public static final pvr_lcn_img:I = 0x7f07018f

.field public static final pvr_lcn_record_data:I = 0x7f07018d

.field public static final pvr_lcn_textview:I = 0x7f070187

.field public static final pvr_lcn_title:I = 0x7f07018c

.field public static final pvr_listview:I = 0x7f07018a

.field public static final pvr_listview_item_channel:I = 0x7f070194

.field public static final pvr_listview_item_lcn:I = 0x7f070193

.field public static final pvr_listview_item_program:I = 0x7f070195

.field public static final pvr_menu_dialog_hours:I = 0x7f0701b9

.field public static final pvr_menu_dialog_minutes:I = 0x7f0701bb

.field public static final pvr_menu_dialog_seconds:I = 0x7f0701bc

.field public static final pvr_menu_info_layout:I = 0x7f0701bd

.field public static final pvr_menu_info_list_view:I = 0x7f0701c0

.field public static final pvr_menu_info_subtitle:I = 0x7f0701bf

.field public static final pvr_menu_info_title:I = 0x7f0701be

.field public static final pvr_program_service_textview:I = 0x7f070189

.field public static final pvr_progressBar:I = 0x7f070191

.field public static final pvrisrecording:I = 0x7f0701b3

.field public static final pvrmenu:I = 0x7f070196

.field public static final pvrprocess_layout:I = 0x7f07019c

.field public static final pvrrecordimage:I = 0x7f0701b4

.field public static final pvrrecordtext:I = 0x7f0701b5

.field public static final pvrrootmenu:I = 0x7f070197

.field public static final radiobgview:I = 0x7f0701c6

.field public static final record_time:I = 0x7f07019f

.field public static final relativelayout_country_choose:I = 0x7f070027

.field public static final relativelayout_option_body:I = 0x7f070025

.field public static final s3d_engine_3d_effect:I = 0x7f0701c7

.field public static final s3d_engine_depth:I = 0x7f0701c8

.field public static final s3d_engine_offset:I = 0x7f0701c9

.field public static final s3d_engine_sequence:I = 0x7f0701ca

.field public static final s3d_item1_name:I = 0x7f0701cb

.field public static final s3d_item1_tag:I = 0x7f0701cc

.field public static final s3d_settings_2dto3d:I = 0x7f0701d1

.field public static final s3d_settings_3dauto:I = 0x7f0701d3

.field public static final s3d_settings_3dclar:I = 0x7f0701d2

.field public static final s3d_settings_3dengine:I = 0x7f0701d4

.field public static final s3d_settings_3dlr:I = 0x7f0701cd

.field public static final s3d_settings_3doff:I = 0x7f0701cf

.field public static final s3d_settings_3dto2d:I = 0x7f0701d0

.field public static final s3d_settings_3dud:I = 0x7f0701ce

.field public static final screensaver_textview:I = 0x7f0701d5

.field public static final scrollView1:I = 0x7f07014f

.field public static final second:I = 0x7f070002

.field public static final seekbar_layout:I = 0x7f0700f4

.field public static final showradio_textview:I = 0x7f070021

.field public static final signal_info_current_channel:I = 0x7f070080

.field public static final signal_info_menu_container:I = 0x7f07007e

.field public static final signal_info_menu_title:I = 0x7f07007f

.field public static final signal_info_modulation:I = 0x7f070082

.field public static final signal_info_network:I = 0x7f070081

.field public static final signal_info_quality:I = 0x7f070086

.field public static final signal_info_quality_percent:I = 0x7f070088

.field public static final signal_info_quality_val:I = 0x7f070087

.field public static final signal_info_strength:I = 0x7f070083

.field public static final signal_info_strength_percent:I = 0x7f070085

.field public static final signal_info_strength_val:I = 0x7f070084

.field public static final smart_toast_txt:I = 0x7f0701e2

.field public static final sound_menu_layout:I = 0x7f0701e3

.field public static final source_info_DTVname:I = 0x7f07013d

.field public static final source_info_DTVnumber:I = 0x7f07013c

.field public static final source_info_DTVtime:I = 0x7f07013f

.field public static final source_info_Subtitle:I = 0x7f070146

.field public static final source_info_age:I = 0x7f070148

.field public static final source_info_audio_format:I = 0x7f070145

.field public static final source_info_audioformat:I = 0x7f070139

.field public static final source_info_description:I = 0x7f07014c

.field public static final source_info_description_title:I = 0x7f07014b

.field public static final source_info_digital_TV:I = 0x7f070142

.field public static final source_info_genre:I = 0x7f070149

.field public static final source_info_imageformat:I = 0x7f070138

.field public static final source_info_language:I = 0x7f070147

.field public static final source_info_mheg5:I = 0x7f070143

.field public static final source_info_more_info:I = 0x7f070134

.field public static final source_info_program_name:I = 0x7f070141

.field public static final source_info_program_period:I = 0x7f070140

.field public static final source_info_program_type:I = 0x7f07013e

.field public static final source_info_soundformat:I = 0x7f07013a

.field public static final source_info_teletext:I = 0x7f07014a

.field public static final source_info_tvname:I = 0x7f070136

.field public static final source_info_tvnumber:I = 0x7f070135

.field public static final source_info_tvtime:I = 0x7f070137

.field public static final source_info_video_format:I = 0x7f070144

.field public static final standbyRecordingTip:I = 0x7f070215

.field public static final stopRecording:I = 0x7f070216

.field public static final subtitle_language_img_back:I = 0x7f0701f5

.field public static final subtitle_language_list_view:I = 0x7f0701f4

.field public static final subtitle_language_title:I = 0x7f0701f3

.field public static final textView1:I = 0x7f070199

.field public static final textView2:I = 0x7f07019a

.field public static final text_view_player_ff:I = 0x7f0701aa

.field public static final text_view_player_play:I = 0x7f0701a4

.field public static final text_view_player_rev:I = 0x7f0701a8

.field public static final text_view_player_slow:I = 0x7f0701ac

.field public static final textview_cha_atvmanualtuning:I = 0x7f070009

.field public static final textview_cha_atvmanualtuning_channelnum:I = 0x7f07000b

.field public static final textview_cha_atvmanualtuning_channelnum_val:I = 0x7f07000c

.field public static final textview_cha_atvmanualtuning_colorsystem:I = 0x7f07000e

.field public static final textview_cha_atvmanualtuning_colorsystem_val:I = 0x7f07000f

.field public static final textview_cha_atvmanualtuning_finetune:I = 0x7f070019

.field public static final textview_cha_atvmanualtuning_frequency:I = 0x7f07001b

.field public static final textview_cha_atvmanualtuning_frequency_mhz:I = 0x7f07001d

.field public static final textview_cha_atvmanualtuning_frequency_val:I = 0x7f07001c

.field public static final textview_cha_atvmanualtuning_skip:I = 0x7f070014

.field public static final textview_cha_atvmanualtuning_skip_val:I = 0x7f070015

.field public static final textview_cha_atvmanualtuning_soundsystem:I = 0x7f070011

.field public static final textview_cha_atvmanualtuning_soundsystem_val:I = 0x7f070012

.field public static final textview_cha_atvmanualtuning_starttuning:I = 0x7f070017

.field public static final textview_cha_autotuning_tuningtype:I = 0x7f070023

.field public static final textview_cha_autotuning_tuningtype_val:I = 0x7f070024

.field public static final textview_cha_channeltuning:I = 0x7f070044

.field public static final textview_cha_data:I = 0x7f070052

.field public static final textview_cha_dataprogram_pro:I = 0x7f070054

.field public static final textview_cha_dataprogram_val:I = 0x7f070053

.field public static final textview_cha_dtv:I = 0x7f07004a

.field public static final textview_cha_dtvmanualtuning:I = 0x7f070089

.field public static final textview_cha_dtvmanualtuning_channelnum_val:I = 0x7f07008b

.field public static final textview_cha_dtvmanualtuning_frequency:I = 0x7f07008d

.field public static final textview_cha_dtvmanualtuning_frequency_val:I = 0x7f07008e

.field public static final textview_cha_dtvmanualtuning_modulation:I = 0x7f070090

.field public static final textview_cha_dtvmanualtuning_modulation_val:I = 0x7f070091

.field public static final textview_cha_dtvmanualtuning_signalquality:I = 0x7f07009b

.field public static final textview_cha_dtvmanualtuning_signalstrength:I = 0x7f070098

.field public static final textview_cha_dtvmanualtuning_starttuning:I = 0x7f070096

.field public static final textview_cha_dtvmanualtuning_symbol:I = 0x7f070093

.field public static final textview_cha_dtvmanualtuning_symbol_val:I = 0x7f070094

.field public static final textview_cha_dtvmanualtuning_tuningresult_data:I = 0x7f0700a0

.field public static final textview_cha_dtvmanualtuning_tuningresult_data_val:I = 0x7f0700a1

.field public static final textview_cha_dtvmanualtuning_tuningresult_dtv:I = 0x7f07009e

.field public static final textview_cha_dtvmanualtuning_tuningresult_dtv_val:I = 0x7f07009f

.field public static final textview_cha_dtvmanualtuning_tuningresult_radio:I = 0x7f0700a2

.field public static final textview_cha_dtvmanualtuning_tuningresult_radio_val:I = 0x7f0700a3

.field public static final textview_cha_dtvprogram_pro:I = 0x7f07004c

.field public static final textview_cha_dtvprogram_val:I = 0x7f07004b

.field public static final textview_cha_exittune_no:I = 0x7f0700ae

.field public static final textview_cha_exittune_yes:I = 0x7f0700ad

.field public static final textview_cha_exittuning_info:I = 0x7f0700ab

.field public static final textview_cha_hint_currentpage:I = 0x7f070032

.field public static final textview_cha_hint_totalpage:I = 0x7f070033

.field public static final textview_cha_radio:I = 0x7f07004e

.field public static final textview_cha_radioprogram_pro:I = 0x7f070050

.field public static final textview_cha_radioprogram_val:I = 0x7f07004f

.field public static final textview_cha_reset_info:I = 0x7f0701c1

.field public static final textview_cha_reset_no:I = 0x7f0701c4

.field public static final textview_cha_reset_yes:I = 0x7f0701c3

.field public static final textview_cha_tuningprogress_ch:I = 0x7f070059

.field public static final textview_cha_tuningprogress_num:I = 0x7f07005a

.field public static final textview_cha_tuningprogress_percent:I = 0x7f070057

.field public static final textview_cha_tuningprogress_type:I = 0x7f07005b

.field public static final textview_cha_tuningprogress_vhf:I = 0x7f070058

.field public static final textview_cha_tv:I = 0x7f070046

.field public static final textview_cha_tvprogram_pro:I = 0x7f070048

.field public static final textview_cha_tvprogram_val:I = 0x7f070047

.field public static final textview_ci_item:I = 0x7f07006a

.field public static final textview_install_guide_antennatype:I = 0x7f0700cf

.field public static final textview_install_guide_antennatype_val:I = 0x7f0700d0

.field public static final textview_install_guide_country:I = 0x7f0700cc

.field public static final textview_install_guide_country_val:I = 0x7f0700cd

.field public static final textview_install_guide_language:I = 0x7f0700c9

.field public static final textview_install_guide_language_val:I = 0x7f0700ca

.field public static final textview_install_guide_tuning:I = 0x7f0700d2

.field public static final textview_install_guide_tuning_val:I = 0x7f0700d3

.field public static final thumbnailRoot:I = 0x7f0701b7

.field public static final time_menu_offtime:I = 0x7f0701f6

.field public static final time_menu_ontime:I = 0x7f07020b

.field public static final time_menu_time:I = 0x7f070200

.field public static final timezone_leftarrow:I = 0x7f070205

.field public static final timezone_rightarrow:I = 0x7f070206

.field public static final tip:I = 0x7f07021f

.field public static final total_record_time:I = 0x7f070190

.field public static final tranplentview:I = 0x7f0701c5

.field public static final tv_channel:I = 0x7f0701e7

.field public static final tv_inputsource:I = 0x7f0701e8

.field public static final tv_main:I = 0x7f0700db

.field public static final tv_usb_nums:I = 0x7f0701eb

.field public static final tv_warning:I = 0x7f07021a

.field public static final usbFreeSpace:I = 0x7f0701b0

.field public static final usbFreeSpacePercent:I = 0x7f0701b1

.field public static final usbImage:I = 0x7f0701af

.field public static final usbInfoLayout:I = 0x7f0701ae

.field public static final usbItemImage:I = 0x7f07021b

.field public static final usbItemName:I = 0x7f07021c

.field public static final usbItemSpace:I = 0x7f07021e

.field public static final usbLabelName:I = 0x7f0701b2

.field public static final usb_driver_scroller:I = 0x7f070220

.field public static final usb_driver_selecter:I = 0x7f070221

.field public static final usb_driver_selecter_cancel:I = 0x7f070222

.field public static final video_3d_menu_container:I = 0x7f070223

.field public static final virtual_num_0:I = 0x7f070234

.field public static final virtual_num_1:I = 0x7f07022a

.field public static final virtual_num_2:I = 0x7f07022b

.field public static final virtual_num_3:I = 0x7f07022c

.field public static final virtual_num_4:I = 0x7f07022d

.field public static final virtual_num_5:I = 0x7f07022e

.field public static final virtual_num_6:I = 0x7f07022f

.field public static final virtual_num_7:I = 0x7f070230

.field public static final virtual_num_8:I = 0x7f070231

.field public static final virtual_num_9:I = 0x7f070232

.field public static final virtual_num_back:I = 0x7f070235

.field public static final virtual_num_c:I = 0x7f070233

.field public static final virtual_pad_channel_add_text:I = 0x7f070225

.field public static final virtual_pad_channel_hint_text:I = 0x7f070227

.field public static final virtual_pad_channel_layout:I = 0x7f070224

.field public static final virtual_pad_channel_sub_text:I = 0x7f070226

.field public static final virtual_pad_edittext:I = 0x7f070228

.field public static final virtual_pad_go_btn:I = 0x7f070229

.field public static final virtual_pad_volume_add_text:I = 0x7f070237

.field public static final virtual_pad_volume_hint_text:I = 0x7f070239

.field public static final virtual_pad_volume_layout:I = 0x7f070236

.field public static final virtual_pad_volume_sub_text:I = 0x7f070238


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
