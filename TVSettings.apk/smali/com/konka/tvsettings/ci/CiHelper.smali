.class public Lcom/konka/tvsettings/ci/CiHelper;
.super Ljava/lang/Object;
.source "CiHelper.java"


# static fields
.field private static instance:Lcom/konka/tvsettings/ci/CiHelper;


# instance fields
.field private ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

.field private iMenuNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/tvsettings/ci/CiHelper;->instance:Lcom/konka/tvsettings/ci/CiHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCiManagerInstance()Lcom/konka/kkinterface/tv/CiDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    return-void
.end method

.method public static getInstance()Lcom/konka/tvsettings/ci/CiHelper;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/ci/CiHelper;->instance:Lcom/konka/tvsettings/ci/CiHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/tvsettings/ci/CiHelper;

    invoke-direct {v0}, Lcom/konka/tvsettings/ci/CiHelper;-><init>()V

    sput-object v0, Lcom/konka/tvsettings/ci/CiHelper;->instance:Lcom/konka/tvsettings/ci/CiHelper;

    :cond_0
    sget-object v0, Lcom/konka/tvsettings/ci/CiHelper;->instance:Lcom/konka/tvsettings/ci/CiHelper;

    return-object v0
.end method


# virtual methods
.method public ansEnq(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2, p1}, Lcom/konka/kkinterface/tv/CiDesk;->answerEnq(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public ansMenu(I)V
    .locals 3
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    int-to-short v2, p1

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/CiDesk;->answerMenu(S)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "now menu is ===== "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "iMenuNum ==== "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public backEnq()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->backEnq()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public backMenu()V
    .locals 2

    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiHelper;->isGetData()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_ENQ:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiHelper;->backEnq()Z

    :cond_0
    :goto_0
    iget v0, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "iMenuNum ==== "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_MENU:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-eq v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_LIST:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v0, v1, :cond_4

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/ci/CiHelper;->ansMenu(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    goto :goto_0
.end method

.method public close()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CiDesk;->close()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public enterMenu()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CiDesk;->enterMenu()V

    iget v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "iMenuNum ==== "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCardState()Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;
    .locals 3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_NO:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getCardState()Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEnqAnsLength()S
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getEnqAnsLength()S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEnqBlindAns()S
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getEnqBlindAnswer()S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEnqLength()S
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getEnqLength()S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEnqString()Ljava/lang/String;
    .locals 3

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getEnqString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getListBottom()Ljava/lang/String;
    .locals 3

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getListBottomString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getListChoiceNum()I
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getListChoiceNumber()S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getListSelection(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2, p1}, Lcom/konka/kkinterface/tv/CiDesk;->getListSelectionString(I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getListSubTitle()Ljava/lang/String;
    .locals 3

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getListSubtitleString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getListTitle()Ljava/lang/String;
    .locals 3

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getListTitleString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMenuBottom()Ljava/lang/String;
    .locals 3

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuBottomString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMenuChoiceNum()I
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuChoiceNumber()S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMenuNum()I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "iMenuNum ===== "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    return v0
.end method

.method public getMenuSelection(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2, p1}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuSelectionString(I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMenuSubTitle()Ljava/lang/String;
    .locals 3

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuSubtitleString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMenuTitle()Ljava/lang/String;
    .locals 3

    const-string v1, ""

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuTitleString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;
    .locals 4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ci MmiType ==== "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isCiMenuOn()Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->isCiMenuOn()Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "is CiMenu On ==== "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isGetData()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiHelper;->ciDesk:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->isDataExisted()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setMenuNum(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/ci/CiHelper;->iMenuNum:I

    return-void
.end method
