.class public Lcom/konka/tvsettings/ci/CiInfoActivity;
.super Landroid/app/Activity;
.source "CiInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;,
        Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;,
        Lcom/konka/tvsettings/ci/CiInfoActivity$MSG;
    }
.end annotation


# instance fields
.field private bIsCiAct:Z

.field private m_BtmTitle:Landroid/widget/TextView;

.field private m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

.field private m_EditText:Landroid/widget/EditText;

.field private m_Handler:Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

.field private m_ListData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_ListView:Landroid/widget/ListView;

.field private m_SubTitle:Landroid/widget/TextView;

.field private m_Thread:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

.field private m_Title:Landroid/widget/TextView;

.field private m_arrayAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_iCurFocusItemId:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    invoke-direct {v0, p0, v1}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity;Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)V

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Thread:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    new-instance v0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

    invoke-direct {v0, p0, v1}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity;Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;)V

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Handler:Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

    iput-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    iput-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;

    iput-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListData:Ljava/util/List;

    iput-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Title:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_BtmTitle:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    iput v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_iCurFocusItemId:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->bIsCiAct:Z

    return-void
.end method

.method private GetData()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v2, "GetData"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v2}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MmiType ======= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_MENU:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v0, v2, :cond_2

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v2}, Lcom/konka/tvsettings/ci/CiHelper;->getMenuChoiceNum()I

    move-result v2

    if-lt v1, v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListData:Ljava/util/List;

    return-object v2

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListData:Ljava/util/List;

    iget-object v3, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v3, v1}, Lcom/konka/tvsettings/ci/CiHelper;->getMenuSelection(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_LIST:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v0, v2, :cond_0

    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v2}, Lcom/konka/tvsettings/ci/CiHelper;->getListChoiceNum()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListData:Ljava/util/List;

    iget-object v3, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v3, v1}, Lcom/konka/tvsettings/ci/CiHelper;->getListSelection(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private SetOnItemClickListener()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;

    new-instance v1, Lcom/konka/tvsettings/ci/CiInfoActivity$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/ci/CiInfoActivity$1;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;

    new-instance v1, Lcom/konka/tvsettings/ci/CiInfoActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/ci/CiInfoActivity$2;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    new-instance v1, Lcom/konka/tvsettings/ci/CiInfoActivity$3;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/ci/CiInfoActivity$3;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/ci/CiInfoActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->bIsCiAct:Z

    return v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Handler:Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Title:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_BtmTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/ci/CiInfoActivity;Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->refreshData(Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;)V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/ci/CiInfoActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_iCurFocusItemId:I

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/ci/CiInfoActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_iCurFocusItemId:I

    return v0
.end method

.method private findView()V
    .locals 1

    const v0, 0x7f070065    # com.konka.tvsettings.R.id.ci_title

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Title:Landroid/widget/TextView;

    const v0, 0x7f070066    # com.konka.tvsettings.R.id.ci_subtitle

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;

    const v0, 0x7f070069    # com.konka.tvsettings.R.id.ci_bottom

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_BtmTitle:Landroid/widget/TextView;

    const v0, 0x7f070068    # com.konka.tvsettings.R.id.listview_ci_card

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;

    const v0, 0x7f070067    # com.konka.tvsettings.R.id.ci_password

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    return-void
.end method

.method private initView()V
    .locals 3

    invoke-static {}, Lcom/konka/tvsettings/ci/CiHelper;->getInstance()Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListData:Ljava/util/List;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f03000e    # com.konka.tvsettings.R.layout.ci_singlestring

    invoke-direct {p0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->GetData()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_arrayAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_arrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->enterMenu()V

    :cond_0
    return-void
.end method

.method private refreshData(Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    const/16 v3, 0x8

    const/4 v2, 0x0

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_MENU:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getMenuTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getMenuSubTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_BtmTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getMenuBottom()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->GetData()Ljava/util/List;

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_arrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    const-string v0, "===refreshData====="

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_LIST:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getListTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getListSubTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_BtmTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getListBottom()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_ENQ:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getEnqString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setListener()V
    .locals 3

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Handler:Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    invoke-direct {p0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->SetOnItemClickListener()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03000d    # com.konka.tvsettings.R.layout.ci_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->setContentView(I)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    return v3

    :sswitch_1
    const-string v0, "Exit"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->getMenuSubTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0104    # com.konka.tvsettings.R.string.str_ci_info_mainmenu

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->getMenuNum()I

    move-result v0

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->close()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->finish()V

    const/4 v0, 0x0

    const v1, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->overridePendingTransition(II)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->getMenuNum()I

    move-result v0

    if-le v0, v3, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->backMenu()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x52 -> :sswitch_1
        0x6f -> :sswitch_1
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->bIsCiAct:Z

    invoke-direct {p0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->findView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->initView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->setListener()V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Thread:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->start()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->m_iCurFocusItemId:I

    iput-boolean v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity;->bIsCiAct:Z

    const-string v0, "===on stop====="

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
