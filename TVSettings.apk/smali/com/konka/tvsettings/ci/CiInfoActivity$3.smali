.class Lcom/konka/tvsettings/ci/CiInfoActivity$3;
.super Ljava/lang/Object;
.source "CiInfoActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/ci/CiInfoActivity;->SetOnItemClickListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/ci/CiInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$3;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/16 v0, 0x42

    if-eq p2, v0, :cond_0

    const/16 v0, 0x17

    if-eq p2, v0, :cond_0

    const/16 v0, 0xb2

    if-ne p2, v0, :cond_2

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$3;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$10(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$3;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$10(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$3;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$3;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_EditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$10(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/ci/CiHelper;->ansEnq(Ljava/lang/String;)Z

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
