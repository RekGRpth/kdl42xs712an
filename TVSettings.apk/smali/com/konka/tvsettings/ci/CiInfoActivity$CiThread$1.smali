.class Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$1;
.super Ljava/lang/Object;
.source "CiInfoActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$1;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$1;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Title:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$3(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0a0103    # com.konka.tvsettings.R.string.str_ci_info_nocard

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$1;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$4(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$1;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_BtmTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$5(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$1;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$6(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    return-void
.end method
