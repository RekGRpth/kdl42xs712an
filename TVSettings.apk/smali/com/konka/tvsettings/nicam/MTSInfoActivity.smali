.class public Lcom/konka/tvsettings/nicam/MTSInfoActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "MTSInfoActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType:[I


# instance fields
.field private final TAG:Ljava/lang/String;

.field private audioInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mtsInfo:Landroid/widget/TextView;

.field private soundDesk:Lcom/konka/kkinterface/tv/SoundDesk;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_13

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_12

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_11

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_FORCED_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_10

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_G_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_f

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_HIDEV_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_e

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_d

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_K_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_c

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_LEFT_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_b

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_LEFT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_a

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_9

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_8

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_7

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_6

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_5

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_4

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_3

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_2

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_RIGHT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_1

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_STEREO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_0

    :goto_14
    sput-object v0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_14

    :catch_1
    move-exception v1

    goto :goto_13

    :catch_2
    move-exception v1

    goto :goto_12

    :catch_3
    move-exception v1

    goto :goto_11

    :catch_4
    move-exception v1

    goto :goto_10

    :catch_5
    move-exception v1

    goto :goto_f

    :catch_6
    move-exception v1

    goto :goto_e

    :catch_7
    move-exception v1

    goto :goto_d

    :catch_8
    move-exception v1

    goto :goto_c

    :catch_9
    move-exception v1

    goto :goto_b

    :catch_a
    move-exception v1

    goto :goto_a

    :catch_b
    move-exception v1

    goto/16 :goto_9

    :catch_c
    move-exception v1

    goto/16 :goto_8

    :catch_d
    move-exception v1

    goto/16 :goto_7

    :catch_e
    move-exception v1

    goto/16 :goto_6

    :catch_f
    move-exception v1

    goto/16 :goto_5

    :catch_10
    move-exception v1

    goto/16 :goto_4

    :catch_11
    move-exception v1

    goto/16 :goto_3

    :catch_12
    move-exception v1

    goto/16 :goto_2

    :catch_13
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    const-string v0, "MTSInfoActivity"

    iput-object v0, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->soundDesk:Lcom/konka/kkinterface/tv/SoundDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->mtsInfo:Landroid/widget/TextView;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->audioInfo:Ljava/util/ArrayList;

    return-void
.end method

.method private getSoundFormat()Ljava/lang/String;
    .locals 3

    const-string v0, ""

    invoke-static {}, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->soundDesk:Lcom/konka/kkinterface/tv/SoundDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "MONO"

    goto :goto_0

    :pswitch_1
    const-string v0, "DUAL A"

    goto :goto_0

    :pswitch_2
    const-string v0, "DUAL AB"

    goto :goto_0

    :pswitch_3
    const-string v0, "DUAL B"

    goto :goto_0

    :pswitch_4
    const-string v0, "MONO"

    goto :goto_0

    :pswitch_5
    const-string v0, "STEREO"

    goto :goto_0

    :pswitch_6
    const-string v0, "HIDEV MONO"

    goto :goto_0

    :pswitch_7
    const-string v0, "UNKNOWN"

    goto :goto_0

    :pswitch_8
    const-string v0, "STEREO"

    goto :goto_0

    :pswitch_9
    const-string v0, "LEFT LEFT"

    goto :goto_0

    :pswitch_a
    const-string v0, "LEFT RIGHT"

    goto :goto_0

    :pswitch_b
    const-string v0, "MONO SAP"

    goto :goto_0

    :pswitch_c
    const-string v0, "DUAL A"

    goto :goto_0

    :pswitch_d
    const-string v0, "DUAL AB"

    goto :goto_0

    :pswitch_e
    const-string v0, "DUAL B"

    goto :goto_0

    :pswitch_f
    const-string v0, "NICAM MONO"

    goto :goto_0

    :pswitch_10
    const-string v0, "NICAM STEREO"

    goto :goto_0

    :pswitch_11
    const-string v0, "RIGHT RIGHT"

    goto :goto_0

    :pswitch_12
    const-string v0, "STEREO SAP"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_b
        :pswitch_12
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_f
        :pswitch_10
        :pswitch_c
        :pswitch_e
        :pswitch_d
        :pswitch_6
        :pswitch_9
        :pswitch_11
        :pswitch_a
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030020    # com.konka.tvsettings.R.layout.mtsinfo

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->setContentView(I)V

    const v2, 0x7f0700e6    # com.konka.tvsettings.R.id.mtsType

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->mtsInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->soundDesk:Lcom/konka/kkinterface/tv/SoundDesk;

    iget-object v2, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->mtsInfo:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->getSoundFormat()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_1
    return v0

    :sswitch_0
    iget-object v0, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->soundDesk:Lcom/konka/kkinterface/tv/SoundDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SoundDesk;->setToNextAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    iget-object v0, p0, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->mtsInfo:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->getSoundFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0xb2 -> :sswitch_1
        0x104 -> :sswitch_0
        0x214 -> :sswitch_0
    .end sparse-switch
.end method

.method public onTimeOut()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onTimeOut()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/nicam/MTSInfoActivity;->finish()V

    return-void
.end method
