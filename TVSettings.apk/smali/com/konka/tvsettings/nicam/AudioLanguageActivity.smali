.class public Lcom/konka/tvsettings/nicam/AudioLanguageActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "AudioLanguageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private audioLangCount:S

.field private audioLanguageListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    const-string v0, "AudioLanguageActivity"

    iput-object v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLanguageListView:Landroid/widget/ListView;

    const/4 v0, 0x0

    iput-short v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/nicam/AudioLanguageActivity;)S
    .locals 1

    iget-short v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/4 v12, 0x0

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v9, 0x7f030003    # com.konka.tvsettings.R.layout.audio_language_list_view

    invoke-virtual {p0, v9}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->setContentView(I)V

    const v9, 0x7f07001f    # com.konka.tvsettings.R.id.audio_language_list_view

    invoke-virtual {p0, v9}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ListView;

    iput-object v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLanguageListView:Landroid/widget/ListView;

    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;-><init>()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v8

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getAudioInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;

    move-result-object v1

    const-string v9, "AudioLanguageActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "audioInfo.audioLangNum=========="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v11, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-short v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    if-lez v9, :cond_3

    iget-short v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    if-nez v9, :cond_4

    const/4 v9, 0x1

    iput-short v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S

    :goto_0
    iget-short v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S

    new-array v2, v9, [Ljava/lang/String;

    iget-short v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S

    new-array v4, v9, [I

    iget-short v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S

    new-array v3, v9, [I

    const/4 v6, 0x0

    :goto_1
    iget-short v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S

    if-lt v6, v9, :cond_5

    iget-short v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    if-nez v9, :cond_0

    const-string v9, "UNKNOWN"

    aput-object v9, v2, v12

    :cond_0
    const/4 v6, 0x0

    :goto_2
    iget-short v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    if-lt v6, v9, :cond_6

    iget-object v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLanguageListView:Landroid/widget/ListView;

    new-instance v10, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;

    invoke-direct {v10, p0, v2, v4, v3}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;-><init>(Lcom/konka/tvsettings/nicam/AudioLanguageActivity;[Ljava/lang/String;[I[I)V

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLanguageListView:Landroid/widget/ListView;

    invoke-virtual {v9, v12}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-short v7, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->currentAudioIndex:S

    if-ltz v7, :cond_1

    iget-short v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    if-lt v7, v9, :cond_2

    :cond_1
    const/4 v7, 0x0

    :cond_2
    iget-object v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLanguageListView:Landroid/widget/ListView;

    invoke-virtual {v9, v7}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLanguageListView:Landroid/widget/ListView;

    new-instance v10, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$1;

    invoke-direct {v10, p0}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$1;-><init>(Lcom/konka/tvsettings/nicam/AudioLanguageActivity;)V

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_3
    return-void

    :cond_4
    iget-short v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    iput-short v9, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S

    goto :goto_0

    :cond_5
    aput v12, v4, v6

    aput v12, v3, v6

    add-int/lit8 v9, v6, 0x1

    int-to-short v6, v9

    goto :goto_1

    :cond_6
    iget-object v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    aget-object v9, v9, v6

    iget-object v9, v9, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;->isoLangInfo:Lcom/mstar/android/tvapi/dtv/vo/LangIso639;

    iget-object v9, v9, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->isoLangInfo:[C

    invoke-static {v9}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->name()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v2, v6

    iget-object v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    aget-object v9, v9, v6

    iget-short v9, v9, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;->audioType:S

    aput v9, v4, v6

    iget-object v9, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    aget-object v9, v9, v6

    iget-object v9, v9, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;->isoLangInfo:Lcom/mstar/android/tvapi/dtv/vo/LangIso639;

    iget-short v9, v9, Lcom/mstar/android/tvapi/dtv/vo/LangIso639;->audioMode:S

    aput v9, v3, v6

    add-int/lit8 v9, v6, 0x1

    int-to-short v6, v9

    goto :goto_2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-static {}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->getInstance()Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->translateIRKey(I)I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0xb2 -> :sswitch_0
        0x104 -> :sswitch_1
    .end sparse-switch
.end method
