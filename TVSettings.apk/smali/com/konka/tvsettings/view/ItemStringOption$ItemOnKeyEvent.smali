.class Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;
.super Ljava/lang/Object;
.source "ItemStringOption.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/view/ItemStringOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemOnKeyEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/view/ItemStringOption;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/view/ItemStringOption;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/view/ItemStringOption;Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;-><init>(Lcom/konka/tvsettings/view/ItemStringOption;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-boolean v2, v2, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # getter for: Lcom/konka/tvsettings/view/ItemStringOption;->LEFT_KEY_CODE:I
    invoke-static {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->access$3(Lcom/konka/tvsettings/view/ItemStringOption;)I

    move-result v2

    if-ne p2, v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->onKeyLeft()V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->doUpdate()V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # invokes: Lcom/konka/tvsettings/view/ItemStringOption;->updateItemState()V
    invoke-static {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->access$4(Lcom/konka/tvsettings/view/ItemStringOption;)V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # getter for: Lcom/konka/tvsettings/view/ItemStringOption;->RIGHT_KEY_CODE:I
    invoke-static {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->access$5(Lcom/konka/tvsettings/view/ItemStringOption;)I

    move-result v2

    if-ne p2, v2, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->onKeyRight()V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->doUpdate()V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # invokes: Lcom/konka/tvsettings/view/ItemStringOption;->updateItemState()V
    invoke-static {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->access$4(Lcom/konka/tvsettings/view/ItemStringOption;)V

    move v0, v1

    goto :goto_0
.end method
