.class public Lcom/konka/tvsettings/view/PictureSettingItem2;
.super Landroid/widget/LinearLayout;
.source "PictureSettingItem2.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# static fields
.field private static final NAMESPACE:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"


# instance fields
.field private START_VALUE:I

.field private STEP:I

.field private callback:Lcom/konka/tvsettings/view/IUpdateSysData;

.field private mCurrentValue:I

.field private mDisplayValue:I

.field public mItemName:Landroid/widget/TextView;

.field private mItemSbContainer:Landroid/widget/LinearLayout;

.field public mItemValue:Landroid/widget/TextView;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private viewContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemSbContainer:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mCurrentValue:I

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->STEP:I

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-direct {p0, p2}, Lcom/konka/tvsettings/view/PictureSettingItem2;->init(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemSbContainer:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mCurrentValue:I

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->STEP:I

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-direct {p0, p2}, Lcom/konka/tvsettings/view/PictureSettingItem2;->init(Landroid/util/AttributeSet;)V

    return-void
.end method

.method private init(Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/util/AttributeSet;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setClickable(Z)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setFocusable(Z)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setFocusableInTouchMode(Z)V

    const-string v1, "http://schemas.android.com/apk/res/android"

    const-string v2, "text"

    const v3, 0x7f0a00a0    # com.konka.tvsettings.R.string.str_pic_setting_pic_mod

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030028    # com.konka.tvsettings.R.layout.picture_setting_item2

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v1, 0x7f070103    # com.konka.tvsettings.R.id.picture_setting_item2_name

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    const v1, 0x7f070104    # com.konka.tvsettings.R.id.picture_setting_item2_value

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    const v1, 0x7f070105    # com.konka.tvsettings.R.id.picture_setting_item2_sb_container

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemSbContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f070106    # com.konka.tvsettings.R.id.picture_setting_item2_seekbar

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method


# virtual methods
.method public getCurrentValue()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mCurrentValue:I

    return v0
.end method

.method public initData(IILcom/konka/tvsettings/view/IUpdateSysData;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/konka/tvsettings/view/IUpdateSysData;

    iput-object p3, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    iput p2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->START_VALUE:I

    iput p1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mCurrentValue:I

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->START_VALUE:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method public initData(IILcom/konka/tvsettings/view/IUpdateSysData;I)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/konka/tvsettings/view/IUpdateSysData;
    .param p4    # I

    iput p4, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->STEP:I

    iput-object p3, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    iput p2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->START_VALUE:I

    iput p1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mCurrentValue:I

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->START_VALUE:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemSbContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemSbContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x15

    if-ne v0, p2, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem2;->valueDec()V

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x16

    if-ne v0, p2, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem2;->valueInc()V

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    iput p2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mCurrentValue:I

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mCurrentValue:I

    iget v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->START_VALUE:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->START_VALUE:I

    if-gez v0, :cond_1

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onProgressChanged:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-interface {v0}, Lcom/konka/tvsettings/view/IUpdateSysData;->doUpdate()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public setCurrentValue(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mCurrentValue:I

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->START_VALUE:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMax(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setMax(I)V

    return-void
.end method

.method public setStatusFbd()V
    .locals 3

    const v2, -0xcdcdce

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setStatusNor()V
    .locals 3

    const v2, -0x666667

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->viewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public valueDec()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->STEP:I

    neg-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    return-void
.end method

.method public valueInc()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem2;->STEP:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    return-void
.end method
