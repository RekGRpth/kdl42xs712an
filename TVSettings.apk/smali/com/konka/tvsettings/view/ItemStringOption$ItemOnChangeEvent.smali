.class Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;
.super Ljava/lang/Object;
.source "ItemStringOption.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/view/ItemStringOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemOnChangeEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/view/ItemStringOption;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/view/ItemStringOption;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/view/ItemStringOption;Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;-><init>(Lcom/konka/tvsettings/view/ItemStringOption;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v4, 0x0

    const/4 v1, -0x1

    const v3, -0x7c7c78

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v0, v0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # invokes: Lcom/konka/tvsettings/view/ItemStringOption;->imageLeftRightVisibility(I)V
    invoke-static {v0, v4}, Lcom/konka/tvsettings/view/ItemStringOption;->access$0(Lcom/konka/tvsettings/view/ItemStringOption;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # getter for: Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->access$1(Lcom/konka/tvsettings/view/ItemStringOption;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # getter for: Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->access$2(Lcom/konka/tvsettings/view/ItemStringOption;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const-string v0, "ItemStringOption"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "================="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setSelected(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ItemStringOption"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "================="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->isSelected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    const/4 v1, 0x4

    # invokes: Lcom/konka/tvsettings/view/ItemStringOption;->imageLeftRightVisibility(I)V
    invoke-static {v0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->access$0(Lcom/konka/tvsettings/view/ItemStringOption;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # getter for: Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->access$1(Lcom/konka/tvsettings/view/ItemStringOption;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # getter for: Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->access$2(Lcom/konka/tvsettings/view/ItemStringOption;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0, v4}, Lcom/konka/tvsettings/view/ItemStringOption;->setSelected(Z)V

    goto :goto_0
.end method
