.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_AUDYSSEY_EQ_MODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AUDYSSEY_EQ_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

.field public static final enum AUDYSSEY_EQ_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

.field public static final enum AUDYSSEY_EQ_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    const-string v1, "AUDYSSEY_EQ_OFF"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->AUDYSSEY_EQ_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    const-string v1, "AUDYSSEY_EQ_ON"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->AUDYSSEY_EQ_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    const-string v1, "AUDYSSEY_EQ_NUM"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->AUDYSSEY_EQ_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->AUDYSSEY_EQ_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->AUDYSSEY_EQ_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->AUDYSSEY_EQ_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
