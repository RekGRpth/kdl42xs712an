.class public Lcom/konka/printscreen/PreviewActivity;
.super Landroid/app/Activity;
.source "PreviewActivity.java"


# static fields
.field public static usbState:Lcom/konka/printscreen/Storage;


# instance fields
.field private fileName:Ljava/lang/String;

.field private image:Landroid/widget/ImageView;

.field private mPsi1:Landroid/widget/LinearLayout;

.field private pathname:Ljava/lang/String;

.field private print_list:Landroid/widget/LinearLayout;

.field private str:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/printscreen/PreviewActivity;->usbState:Lcom/konka/printscreen/Storage;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/printscreen/PreviewActivity;->pathname:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/printscreen/PreviewActivity;->fileName:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/printscreen/PreviewActivity;->str:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/printscreen/PreviewActivity;->image:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/printscreen/PreviewActivity;->mPsi1:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/printscreen/PreviewActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/printscreen/PreviewActivity;->pathname:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030034    # com.konka.tvsettings.R.layout.print_screen_preview

    invoke-virtual {p0, v3}, Lcom/konka/printscreen/PreviewActivity;->setContentView(I)V

    const v3, 0x7f070152    # com.konka.tvsettings.R.id.pre

    invoke-virtual {p0, v3}, Lcom/konka/printscreen/PreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/konka/printscreen/PreviewActivity;->print_list:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/konka/printscreen/PreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "IMAGE_PATH"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/printscreen/PreviewActivity;->pathname:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "FILE_NAME"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/printscreen/PreviewActivity;->str:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/printscreen/PreviewActivity;->pathname:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    const v3, 0x7f070153    # com.konka.tvsettings.R.id.preview_image

    invoke-virtual {p0, v3}, Lcom/konka/printscreen/PreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/konka/printscreen/PreviewActivity;->image:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/konka/printscreen/PreviewActivity;->image:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v3, Lcom/konka/printscreen/Storage;

    invoke-virtual {p0}, Lcom/konka/printscreen/PreviewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/konka/printscreen/Storage;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/konka/printscreen/PreviewActivity;->usbState:Lcom/konka/printscreen/Storage;

    const v3, 0x7f070154    # com.konka.tvsettings.R.id.but1

    invoke-virtual {p0, v3}, Lcom/konka/printscreen/PreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    new-instance v3, Lcom/konka/printscreen/PreviewActivity$1;

    invoke-direct {v3, p0}, Lcom/konka/printscreen/PreviewActivity$1;-><init>(Lcom/konka/printscreen/PreviewActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    sget-object v0, Lcom/konka/printscreen/PreviewActivity;->usbState:Lcom/konka/printscreen/Storage;

    invoke-virtual {p0}, Lcom/konka/printscreen/PreviewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/printscreen/Storage;->destoryStorage(Landroid/content/Context;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 2

    iget-object v0, p0, Lcom/konka/printscreen/PreviewActivity;->print_list:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method
