.class public Lcom/konka/printscreen/doScreenCapture;
.super Landroid/app/Activity;
.source "doScreenCapture.java"


# instance fields
.field private final FINISH_SCREENCAPTURE:I

.field private flag:Z

.field private final mHandler:Landroid/os/Handler;

.field private mScreenCapture:Lcom/konka/printscreen/ScreenCapture;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/printscreen/doScreenCapture;->flag:Z

    const/16 v0, 0x65

    iput v0, p0, Lcom/konka/printscreen/doScreenCapture;->FINISH_SCREENCAPTURE:I

    new-instance v0, Lcom/konka/printscreen/doScreenCapture$1;

    invoke-direct {v0, p0}, Lcom/konka/printscreen/doScreenCapture$1;-><init>(Lcom/konka/printscreen/doScreenCapture;)V

    iput-object v0, p0, Lcom/konka/printscreen/doScreenCapture;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/printscreen/doScreenCapture;)Lcom/konka/printscreen/ScreenCapture;
    .locals 1

    iget-object v0, p0, Lcom/konka/printscreen/doScreenCapture;->mScreenCapture:Lcom/konka/printscreen/ScreenCapture;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/printscreen/doScreenCapture;Lcom/konka/printscreen/ScreenCapture;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/printscreen/doScreenCapture;->mScreenCapture:Lcom/konka/printscreen/ScreenCapture;

    return-void
.end method

.method static synthetic access$2(Lcom/konka/printscreen/doScreenCapture;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/printscreen/doScreenCapture;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/printscreen/doScreenCapture;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/printscreen/doScreenCapture;->flag:Z

    return-void
.end method


# virtual methods
.method public doCaptureWork()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/printscreen/doScreenCapture;->flag:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/printscreen/doScreenCapture;->flag:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/printscreen/doScreenCapture$2;

    invoke-direct {v1, p0}, Lcom/konka/printscreen/doScreenCapture$2;-><init>(Lcom/konka/printscreen/doScreenCapture;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030033    # com.konka.tvsettings.R.layout.print_screen

    invoke-virtual {p0, v1}, Lcom/konka/printscreen/doScreenCapture;->setContentView(I)V

    const-wide/16 v1, 0x46

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/konka/printscreen/doScreenCapture;->doCaptureWork()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
