.class public Lcom/android/exchange/adapter/AttachmentLoader;
.super Ljava/lang/Object;
.source "AttachmentLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/AttachmentLoader$1;,
        Lcom/android/exchange/adapter/AttachmentLoader$AttachmentNameEncoder;
    }
.end annotation


# instance fields
.field private final mAccountId:J

.field private final mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

.field private final mAttachmentId:J

.field private final mAttachmentSize:I

.field private final mAttachmentUri:Landroid/net/Uri;

.field private final mContext:Landroid/content/Context;

.field private final mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

.field private final mMessageId:J

.field private final mResolver:Landroid/content/ContentResolver;

.field private final mService:Lcom/android/exchange/EasSyncService;


# direct methods
.method public constructor <init>(Lcom/android/exchange/EasSyncService;Lcom/android/exchange/PartRequest;)V
    .locals 4
    .param p1    # Lcom/android/exchange/EasSyncService;
    .param p2    # Lcom/android/exchange/PartRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, p1, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mContext:Landroid/content/Context;

    iget-object v0, p1, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    iput-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mResolver:Landroid/content/ContentResolver;

    iget-object v0, p2, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iput-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    iput-wide v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentId:J

    iget-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    long-to-int v0, v0

    iput v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentSize:I

    iget-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    iput-wide v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAccountId:J

    iget-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    iput-wide v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mMessageId:J

    iget-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mContext:Landroid/content/Context;

    iget-wide v1, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mMessageId:J

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAccountId:J

    iget-wide v2, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentId:J

    invoke-static {v0, v1, v2, v3}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentUri(JJ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentUri:Landroid/net/Uri;

    return-void
.end method

.method private doProgressCallback(I)V
    .locals 7
    .param p1    # I

    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mMessageId:J

    iget-wide v3, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentId:J

    const/4 v5, 0x1

    move v6, p1

    invoke-interface/range {v0 .. v6}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadAttachmentStatus(JJII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private doStatusCallback(I)V
    .locals 7
    .param p1    # I

    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mMessageId:J

    iget-wide v3, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentId:J

    const/4 v6, 0x0

    move v5, p1

    invoke-interface/range {v0 .. v6}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadAttachmentStatus(JJII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static encodeForExchange2003(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    new-instance v0, Lcom/android/exchange/adapter/AttachmentLoader$AttachmentNameEncoder;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/android/exchange/adapter/AttachmentLoader$AttachmentNameEncoder;-><init>(Lcom/android/exchange/adapter/AttachmentLoader$1;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, v1, p0}, Lcom/android/exchange/adapter/AttachmentLoader$AttachmentNameEncoder;->appendPartiallyEncoded(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private finishLoadAttachment()V
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "contentUri"

    iget-object v2, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "uiState"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v2, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/AttachmentLoader;->doStatusCallback(I)V

    return-void
.end method


# virtual methods
.method public loadAttachment()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mMessage:Lcom/android/emailcommon/provider/EmailContent$Message;

    if-nez v11, :cond_0

    const/16 v11, 0x10

    invoke-direct {p0, v11}, Lcom/android/exchange/adapter/AttachmentLoader;->doStatusCallback(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v11, 0x0

    invoke-direct {p0, v11}, Lcom/android/exchange/adapter/AttachmentLoader;->doProgressCallback(I)V

    iget-object v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v11, v11, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v11

    const-wide/high16 v13, 0x402c000000000000L    # 14.0

    cmpl-double v11, v11, v13

    if-ltz v11, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    new-instance v9, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v9}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    const/16 v11, 0x505

    invoke-virtual {v9, v11}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v11

    const/16 v12, 0x506

    invoke-virtual {v11, v12}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    const/16 v11, 0x507

    const-string v12, "Mailbox"

    invoke-virtual {v9, v11, v12}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    const/16 v11, 0x451

    iget-object v12, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v12, v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    invoke-virtual {v9, v11, v12}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    invoke-virtual {v9}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/exchange/adapter/Serializer;->done()V

    iget-object v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    const-string v12, "ItemOperations"

    invoke-virtual {v9}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v8

    :goto_2
    :try_start_0
    invoke-virtual {v8}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v10

    const/16 v11, 0xc8

    if-ne v10, v11, :cond_7

    invoke-virtual {v8}, Lcom/android/exchange/EasResponse;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_7

    invoke-virtual {v8}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    const/4 v6, 0x0

    :try_start_1
    iget-object v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mResolver:Landroid/content/ContentResolver;

    iget-object v12, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentUri:Landroid/net/Uri;

    invoke-virtual {v11, v12}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v6

    if-eqz v2, :cond_5

    new-instance v7, Lcom/android/exchange/adapter/ItemOperationsParser;

    iget v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentSize:I

    invoke-direct {v7, p0, v3, v6, v11}, Lcom/android/exchange/adapter/ItemOperationsParser;-><init>(Lcom/android/exchange/adapter/AttachmentLoader;Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    invoke-virtual {v7}, Lcom/android/exchange/adapter/ItemOperationsParser;->parse()Z

    invoke-virtual {v7}, Lcom/android/exchange/adapter/ItemOperationsParser;->getStatusCode()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_9

    invoke-direct {p0}, Lcom/android/exchange/adapter/AttachmentLoader;->finishLoadAttachment()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v6, :cond_1

    :try_start_2
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_3
    invoke-virtual {v8}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    iget-object v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v5, v11, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    iget-object v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v11, v11, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v11

    const-wide/high16 v13, 0x4028000000000000L    # 12.0

    cmpg-double v11, v11, v13

    if-gez v11, :cond_4

    invoke-static {v5}, Lcom/android/exchange/adapter/AttachmentLoader;->encodeForExchange2003(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_4
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "GetAttachment&AttachmentName="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    const/4 v12, 0x0

    const/16 v13, 0x7530

    invoke-virtual {v11, v0, v12, v13}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v8

    goto :goto_2

    :cond_5
    :try_start_3
    invoke-virtual {v8}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v4

    if-eqz v4, :cond_9

    if-gez v4, :cond_6

    iget v4, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mAttachmentSize:I

    :cond_6
    invoke-virtual {p0, v3, v6, v4}, Lcom/android/exchange/adapter/AttachmentLoader;->readChunked(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    invoke-direct {p0}, Lcom/android/exchange/adapter/AttachmentLoader;->finishLoadAttachment()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v6, :cond_1

    :try_start_4
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v11

    invoke-virtual {v8}, Lcom/android/exchange/EasResponse;->close()V

    throw v11

    :catch_0
    move-exception v1

    :try_start_5
    iget-object v11, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    const-string v12, "Can\'t get attachment; write file not found?"

    invoke-virtual {v11, v12}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v6, :cond_7

    :try_start_6
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_7
    :goto_4
    invoke-virtual {v8}, Lcom/android/exchange/EasResponse;->close()V

    const/16 v11, 0x11

    invoke-direct {p0, v11}, Lcom/android/exchange/adapter/AttachmentLoader;->doStatusCallback(I)V

    goto/16 :goto_0

    :catchall_1
    move-exception v11

    if-eqz v6, :cond_8

    :try_start_7
    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    :cond_8
    throw v11

    :cond_9
    if-eqz v6, :cond_7

    invoke-virtual {v6}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4
.end method

.method public readChunked(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .locals 11
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v10, 0x4000

    const/4 v9, 0x0

    new-array v0, v10, [B

    move v3, p3

    const/4 v6, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    iget-object v7, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    const-string v8, "Expected attachment length: "

    invoke-virtual {v7, v8, p3}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    invoke-virtual {p1, v0, v9, v10}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    if-gez v5, :cond_2

    iget-object v7, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    const-string v8, "Attachment load reached EOF, totalRead: "

    invoke-virtual {v7, v8, v6}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    if-le v6, v3, :cond_1

    iget-object v7, p0, Lcom/android/exchange/adapter/AttachmentLoader;->mService:Lcom/android/exchange/EasSyncService;

    const-string v8, "Read more than expected: "

    invoke-virtual {v7, v8, v6}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    :cond_1
    return-void

    :cond_2
    add-int/2addr v6, v5

    invoke-virtual {p2, v0, v9, v5}, Ljava/io/OutputStream;->write([BII)V

    if-lez v3, :cond_0

    mul-int/lit8 v7, v6, 0x64

    div-int v4, v7, v3

    if-le v4, v1, :cond_0

    add-int/lit16 v7, v2, 0x4000

    if-le v6, v7, :cond_0

    invoke-direct {p0, v4}, Lcom/android/exchange/adapter/AttachmentLoader;->doProgressCallback(I)V

    move v2, v6

    move v1, v4

    goto :goto_0
.end method
