.class public Lcom/android/quicksearchbox/AppsCorpus;
.super Lcom/android/quicksearchbox/SingleSourceCorpus;
.source "AppsCorpus.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/quicksearchbox/Config;Lcom/android/quicksearchbox/Source;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/quicksearchbox/Config;
    .param p3    # Lcom/android/quicksearchbox/Source;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/quicksearchbox/SingleSourceCorpus;-><init>(Landroid/content/Context;Lcom/android/quicksearchbox/Config;Lcom/android/quicksearchbox/Source;)V

    return-void
.end method

.method private createAppSearchIntent(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/AppsCorpus;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090002    # com.android.quicksearchbox.R.string.apps_search_activity

    invoke-static {v4, v5}, Lcom/android/quicksearchbox/AppsCorpus;->getComponentName(Landroid/content/Context;I)Landroid/content/ComponentName;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v1, v3

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-static {v2, p1, p2}, Lcom/android/quicksearchbox/AbstractSource;->createSourceSearchIntent(Landroid/content/ComponentName;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v1, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/quicksearchbox/AppsCorpus;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v4, "QSB.AppsCorpus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Can\'t find app search activity "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    goto :goto_0
.end method

.method private static getComponentName(Landroid/content/Context;I)Landroid/content/ComponentName;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public createSearchIntent(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;

    invoke-direct {p0, p1, p2}, Lcom/android/quicksearchbox/AppsCorpus;->createAppSearchIntent(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/quicksearchbox/SingleSourceCorpus;->createSearchIntent(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public getCorpusIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/AppsCorpus;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020015    # com.android.quicksearchbox.R.drawable.corpus_icon_apps

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/AppsCorpus;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090011    # com.android.quicksearchbox.R.string.corpus_hint_apps

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getLabel()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/AppsCorpus;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f09000f    # com.android.quicksearchbox.R.string.corpus_label_apps

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "apps"

    return-object v0
.end method

.method public getSettingsDescription()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/AppsCorpus;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090010    # com.android.quicksearchbox.R.string.corpus_description_apps

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
