.class Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;
.super Ljava/lang/Object;
.source "SourceShortcutRefresher.java"

# interfaces
.implements Lcom/android/quicksearchbox/util/NamedTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/quicksearchbox/SourceShortcutRefresher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShortcutRefreshTask"
.end annotation


# instance fields
.field private final mExtraData:Ljava/lang/String;

.field private final mListener:Lcom/android/quicksearchbox/ShortcutRefresher$Listener;

.field private final mShortcutId:Ljava/lang/String;

.field private final mSource:Lcom/android/quicksearchbox/Source;

.field final synthetic this$0:Lcom/android/quicksearchbox/SourceShortcutRefresher;


# direct methods
.method constructor <init>(Lcom/android/quicksearchbox/SourceShortcutRefresher;Lcom/android/quicksearchbox/Source;Ljava/lang/String;Ljava/lang/String;Lcom/android/quicksearchbox/ShortcutRefresher$Listener;)V
    .locals 0
    .param p2    # Lcom/android/quicksearchbox/Source;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/android/quicksearchbox/ShortcutRefresher$Listener;

    iput-object p1, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->this$0:Lcom/android/quicksearchbox/SourceShortcutRefresher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mSource:Lcom/android/quicksearchbox/Source;

    iput-object p3, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mShortcutId:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mExtraData:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mListener:Lcom/android/quicksearchbox/ShortcutRefresher$Listener;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mSource:Lcom/android/quicksearchbox/Source;

    iget-object v2, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mShortcutId:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mExtraData:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/android/quicksearchbox/Source;->refreshShortcut(Ljava/lang/String;Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Lcom/android/quicksearchbox/SuggestionCursor;->close()V

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->this$0:Lcom/android/quicksearchbox/SourceShortcutRefresher;

    iget-object v2, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mSource:Lcom/android/quicksearchbox/Source;

    iget-object v3, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mShortcutId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/quicksearchbox/SourceShortcutRefresher;->markShortcutRefreshed(Lcom/android/quicksearchbox/Source;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mListener:Lcom/android/quicksearchbox/ShortcutRefresher$Listener;

    iget-object v2, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mSource:Lcom/android/quicksearchbox/Source;

    iget-object v3, p0, Lcom/android/quicksearchbox/SourceShortcutRefresher$ShortcutRefreshTask;->mShortcutId:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/android/quicksearchbox/ShortcutRefresher$Listener;->onShortcutRefreshed(Lcom/android/quicksearchbox/Source;Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;)V

    return-void
.end method
