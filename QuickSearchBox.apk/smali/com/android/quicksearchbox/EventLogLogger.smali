.class public Lcom/android/quicksearchbox/EventLogLogger;
.super Ljava/lang/Object;
.source "EventLogLogger.java"

# interfaces
.implements Lcom/android/quicksearchbox/Logger;


# instance fields
.field private final mConfig:Lcom/android/quicksearchbox/Config;

.field private final mContext:Landroid/content/Context;

.field private final mPackageName:Ljava/lang/String;

.field private final mRandom:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/quicksearchbox/Config;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/quicksearchbox/Config;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/EventLogLogger;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/quicksearchbox/EventLogLogger;->mConfig:Lcom/android/quicksearchbox/Config;

    iget-object v0, p0, Lcom/android/quicksearchbox/EventLogLogger;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/EventLogLogger;->mPackageName:Ljava/lang/String;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/EventLogLogger;->mRandom:Ljava/util/Random;

    return-void
.end method

.method private getCorpusLogName(Lcom/android/quicksearchbox/Corpus;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Corpus;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lcom/android/quicksearchbox/Corpus;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getCorpusLogNames(Ljava/util/Collection;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-nez p1, :cond_0

    const-string v3, ""

    :goto_0
    return-object v3

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/Corpus;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_1

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/quicksearchbox/EventLogLogger;->getCorpusLogName(Lcom/android/quicksearchbox/Corpus;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private getSuggestions(Lcom/android/quicksearchbox/SuggestionCursor;)Ljava/lang/String;
    .locals 8
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;

    const/16 v7, 0x3a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_4

    if-lez v1, :cond_0

    const/16 v6, 0x7c

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-interface {p1, v1}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v6

    invoke-interface {v6}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    const-string v5, ""

    :cond_1
    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->isSuggestionShortcut()Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v3, "shortcut"

    :goto_2
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v0

    goto :goto_0

    :cond_3
    const-string v3, ""

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private shouldLogLatency()Z
    .locals 3

    iget-object v1, p0, Lcom/android/quicksearchbox/EventLogLogger;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-virtual {v1}, Lcom/android/quicksearchbox/Config;->getLatencyLogFrequency()I

    move-result v0

    iget-object v1, p0, Lcom/android/quicksearchbox/EventLogLogger;->mRandom:Ljava/util/Random;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/EventLogLogger;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected getVersionCode()I
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/EventLogLogger;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/quicksearchbox/QsbApplication;->get(Landroid/content/Context;)Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getVersionCode()I

    move-result v0

    return v0
.end method

.method public logExit(Lcom/android/quicksearchbox/SuggestionCursor;I)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/EventLogLogger;->getSuggestions(Lcom/android/quicksearchbox/SuggestionCursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/quicksearchbox/EventLogTags;->writeQsbExit(Ljava/lang/String;I)V

    return-void
.end method

.method public logLatency(Lcom/android/quicksearchbox/CorpusResult;)V
    .locals 4
    .param p1    # Lcom/android/quicksearchbox/CorpusResult;

    invoke-direct {p0}, Lcom/android/quicksearchbox/EventLogLogger;->shouldLogLatency()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/android/quicksearchbox/CorpusResult;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/quicksearchbox/EventLogLogger;->getCorpusLogName(Lcom/android/quicksearchbox/Corpus;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/android/quicksearchbox/CorpusResult;->getLatency()I

    move-result v1

    invoke-interface {p1}, Lcom/android/quicksearchbox/CorpusResult;->getUserQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/android/quicksearchbox/EventLogTags;->writeQsbLatency(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public logSearch(Lcom/android/quicksearchbox/Corpus;II)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Corpus;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/EventLogLogger;->getCorpusLogName(Lcom/android/quicksearchbox/Corpus;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/android/quicksearchbox/EventLogTags;->writeQsbSearch(Ljava/lang/String;II)V

    return-void
.end method

.method public logStart(IILjava/lang/String;Lcom/android/quicksearchbox/Corpus;Ljava/util/List;)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/android/quicksearchbox/Corpus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Lcom/android/quicksearchbox/Corpus;",
            "Ljava/util/List",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;)V"
        }
    .end annotation

    move-object v2, p3

    invoke-direct {p0, p4}, Lcom/android/quicksearchbox/EventLogLogger;->getCorpusLogName(Lcom/android/quicksearchbox/Corpus;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p5}, Lcom/android/quicksearchbox/EventLogLogger;->getCorpusLogNames(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/android/quicksearchbox/EventLogLogger;->mPackageName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/EventLogLogger;->getVersionCode()I

    move-result v1

    move v3, p2

    move v6, p1

    invoke-static/range {v0 .. v6}, Lcom/android/quicksearchbox/EventLogTags;->writeQsbStart(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public logSuggestionClick(JLcom/android/quicksearchbox/SuggestionCursor;Ljava/util/Collection;I)V
    .locals 6
    .param p1    # J
    .param p3    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/android/quicksearchbox/SuggestionCursor;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0, p3}, Lcom/android/quicksearchbox/EventLogLogger;->getSuggestions(Lcom/android/quicksearchbox/SuggestionCursor;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p4}, Lcom/android/quicksearchbox/EventLogLogger;->getCorpusLogNames(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p3}, Lcom/android/quicksearchbox/SuggestionCursor;->getUserQuery()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    move-wide v0, p1

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/android/quicksearchbox/EventLogTags;->writeQsbClick(JLjava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method public logVoiceSearch(Lcom/android/quicksearchbox/Corpus;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Corpus;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/EventLogLogger;->getCorpusLogName(Lcom/android/quicksearchbox/Corpus;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/quicksearchbox/EventLogTags;->writeQsbVoiceSearch(Ljava/lang/String;)V

    return-void
.end method
