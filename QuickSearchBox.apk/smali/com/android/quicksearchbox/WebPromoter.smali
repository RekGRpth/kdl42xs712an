.class public Lcom/android/quicksearchbox/WebPromoter;
.super Ljava/lang/Object;
.source "WebPromoter.java"

# interfaces
.implements Lcom/android/quicksearchbox/Promoter;


# instance fields
.field private final mMaxShortcuts:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/quicksearchbox/WebPromoter;->mMaxShortcuts:I

    return-void
.end method


# virtual methods
.method public pickPromoted(Lcom/android/quicksearchbox/Suggestions;ILcom/android/quicksearchbox/ListSuggestionCursor;)V
    .locals 8
    .param p1    # Lcom/android/quicksearchbox/Suggestions;
    .param p2    # I
    .param p3    # Lcom/android/quicksearchbox/ListSuggestionCursor;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/android/quicksearchbox/Suggestions;->getShortcuts()Lcom/android/quicksearchbox/ShortcutCursor;

    move-result-object v3

    if-nez v3, :cond_1

    move v2, v6

    :goto_0
    iget v7, p0, Lcom/android/quicksearchbox/WebPromoter;->mMaxShortcuts:I

    invoke-static {v7, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    invoke-virtual {p3}, Lcom/android/quicksearchbox/ListSuggestionCursor;->getCount()I

    move-result v7

    if-ge v7, v1, :cond_2

    invoke-interface {v3, v0}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    invoke-interface {v3}, Lcom/android/quicksearchbox/SuggestionCursor;->isWebSearchSuggestion()Z

    move-result v7

    if-eqz v7, :cond_0

    new-instance v7, Lcom/android/quicksearchbox/SuggestionPosition;

    invoke-direct {v7, v3, v0}, Lcom/android/quicksearchbox/SuggestionPosition;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    invoke-virtual {p3, v7}, Lcom/android/quicksearchbox/ListSuggestionCursor;->add(Lcom/android/quicksearchbox/Suggestion;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v3}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/android/quicksearchbox/Suggestions;->getWebResult()Lcom/android/quicksearchbox/CorpusResult;

    move-result-object v5

    if-nez v5, :cond_4

    move v4, v6

    :goto_2
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v4, :cond_5

    invoke-virtual {p3}, Lcom/android/quicksearchbox/ListSuggestionCursor;->getCount()I

    move-result v6

    if-ge v6, p2, :cond_5

    invoke-interface {v5, v0}, Lcom/android/quicksearchbox/CorpusResult;->moveTo(I)V

    invoke-interface {v5}, Lcom/android/quicksearchbox/CorpusResult;->isWebSearchSuggestion()Z

    move-result v6

    if-eqz v6, :cond_3

    new-instance v6, Lcom/android/quicksearchbox/SuggestionPosition;

    invoke-direct {v6, v5, v0}, Lcom/android/quicksearchbox/SuggestionPosition;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    invoke-virtual {p3, v6}, Lcom/android/quicksearchbox/ListSuggestionCursor;->add(Lcom/android/quicksearchbox/Suggestion;)Z

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-interface {v5}, Lcom/android/quicksearchbox/CorpusResult;->getCount()I

    move-result v4

    goto :goto_2

    :cond_5
    return-void
.end method
