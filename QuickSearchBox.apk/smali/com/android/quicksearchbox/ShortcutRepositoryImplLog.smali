.class public Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;
.super Ljava/lang/Object;
.source "ShortcutRepositoryImplLog.java"

# interfaces
.implements Lcom/android/quicksearchbox/ShortcutRepository;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;,
        Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;,
        Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;,
        Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;,
        Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SuggestionCursorImpl;
    }
.end annotation


# static fields
.field private static final GROUP_BY:Ljava/lang/String;

.field private static final HAS_HISTORY_QUERY:Ljava/lang/String;

.field private static final LAST_HIT_TIME_EXPR:Ljava/lang/String;

.field private static final PREFER_LATEST_PREFIX:Ljava/lang/String;

.field private static final PREFIX_RESTRICTION:Ljava/lang/String;

.field private static final SHORTCUT_BY_ID_WHERE:Ljava/lang/String;

.field private static final SHORTCUT_QUERY_COLUMNS:[Ljava/lang/String;

.field private static final SOURCE_RANKING_SQL:Ljava/lang/String;

.field private static final TABLES:Ljava/lang/String;


# instance fields
.field private final mConfig:Lcom/android/quicksearchbox/Config;

.field private final mContext:Landroid/content/Context;

.field private final mCorpora:Lcom/android/quicksearchbox/Corpora;

.field private mEmptyQueryShortcutQuery:Ljava/lang/String;

.field private final mLogExecutor:Ljava/util/concurrent/Executor;

.field private final mOpenHelper:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

.field private final mRefresher:Lcom/android/quicksearchbox/ShortcutRefresher;

.field private final mSearchSpinner:Ljava/lang/String;

.field private mShortcutQuery:Ljava/lang/String;

.field private final mUiThread:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_key:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    iget-object v1, v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "shortcuts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->HAS_HISTORY_QUERY:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->shortcut_id:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->source:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->SHORTCUT_BY_ID_WHERE:Ljava/lang/String;

    invoke-static {}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->buildSourceRankingSql()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->SOURCE_RANKING_SQL:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "clicklog INNER JOIN shortcuts ON "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->intent_key:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    iget-object v1, v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_key:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    iget-object v1, v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->TABLES:Ljava/lang/String;

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_key:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    iget-object v2, v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->fullName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->source:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    iget-object v2, v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->fullName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->source_version_code:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    iget-object v2, v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->fullName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->format:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    iget-object v3, v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->fullName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_format"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->title:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_text_1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->description:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_text_2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->description_url:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_text_2_url"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->icon1:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_icon_1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->icon2:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_icon_2"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_action:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_intent_action"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_component:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    iget-object v2, v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->fullName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_data:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_intent_data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_query:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_intent_query"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_extradata:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_intent_extra_data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->shortcut_id:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_shortcut_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->spinner_while_refreshing:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_spinner_while_refreshing"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->log_type:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AS "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "suggest_log_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->custom_columns:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    iget-object v2, v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->fullName:Ljava/lang/String;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->SHORTCUT_QUERY_COLUMNS:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->query:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    iget-object v1, v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " >= ?1 AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->query:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    iget-object v1, v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " < ?2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->PREFIX_RESTRICTION:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MAX("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->hit_time:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    iget-object v1, v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->LAST_HIT_TIME_EXPR:Ljava/lang/String;

    sget-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->intent_key:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    iget-object v0, v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->fullName:Ljava/lang/String;

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->GROUP_BY:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->LAST_HIT_TIME_EXPR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = (SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->LAST_HIT_TIME_EXPR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "clicklog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->PREFER_LATEST_PREFIX:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/quicksearchbox/Config;Lcom/android/quicksearchbox/Corpora;Lcom/android/quicksearchbox/ShortcutRefresher;Landroid/os/Handler;Ljava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/quicksearchbox/Config;
    .param p3    # Lcom/android/quicksearchbox/Corpora;
    .param p4    # Lcom/android/quicksearchbox/ShortcutRefresher;
    .param p5    # Landroid/os/Handler;
    .param p6    # Ljava/util/concurrent/Executor;
    .param p7    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mConfig:Lcom/android/quicksearchbox/Config;

    iput-object p3, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mCorpora:Lcom/android/quicksearchbox/Corpora;

    iput-object p4, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mRefresher:Lcom/android/quicksearchbox/ShortcutRefresher;

    iput-object p5, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mUiThread:Landroid/os/Handler;

    iput-object p6, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mLogExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

    const/16 v1, 0x20

    invoke-direct {v0, p1, p7, v1, p2}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/android/quicksearchbox/Config;)V

    iput-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mOpenHelper:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->buildShortcutQueries()V

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mContext:Landroid/content/Context;

    const v1, 0x7f020046    # com.android.quicksearchbox.R.drawable.search_spinner

    invoke-static {v0, v1}, Lcom/android/quicksearchbox/util/Util;->getResourceUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mSearchSpinner:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;)Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mOpenHelper:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->hasHistory(Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;

    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->getCorpusScores()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->SHORTCUT_BY_ID_WHERE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->stringToComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Lcom/android/quicksearchbox/Suggestion;)Z
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->shouldRefresh(Lcom/android/quicksearchbox/Suggestion;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mSearchSpinner:Ljava/lang/String;

    return-object v0
.end method

.method private buildShortcutQueries()V
    .locals 14

    const/4 v7, 0x0

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(?3 - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-virtual {v2}, Lcom/android/quicksearchbox/Config;->getMaxStatAgeMillis()J

    move-result-wide v12

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->hit_time:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    iget-object v2, v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->fullName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v5, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SUM(("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->hit_time:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    iget-object v2, v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->fullName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") / 1000)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v3, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->PREFER_LATEST_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "))"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DESC, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DESC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->TABLES:Ljava/lang/String;

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->SHORTCUT_QUERY_COLUMNS:[Ljava/lang/String;

    sget-object v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->GROUP_BY:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mEmptyQueryShortcutQuery:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->PREFIX_RESTRICTION:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->PREFER_LATEST_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "))"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DESC, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DESC"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->TABLES:Ljava/lang/String;

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->SHORTCUT_QUERY_COLUMNS:[Ljava/lang/String;

    sget-object v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->GROUP_BY:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mShortcutQuery:Ljava/lang/String;

    return-void
.end method

.method private static buildShortcutQueryParams(Ljava/lang/String;J)[Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # J

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->nextString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method private static buildSourceRankingSql()Ljava/lang/String;
    .locals 10

    sget-object v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;->total_clicks:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;->name()Ljava/lang/String;

    move-result-object v8

    const-string v9, "sourcetotals"

    sget-object v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;->COLUMNS:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;->total_clicks:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " >= $1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v0, 0x0

    const-string v1, "sourcetotals"

    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private componentNameToString(Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/ComponentName;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static create(Landroid/content/Context;Lcom/android/quicksearchbox/Config;Lcom/android/quicksearchbox/Corpora;Lcom/android/quicksearchbox/ShortcutRefresher;Landroid/os/Handler;Ljava/util/concurrent/Executor;)Lcom/android/quicksearchbox/ShortcutRepository;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/quicksearchbox/Config;
    .param p2    # Lcom/android/quicksearchbox/Corpora;
    .param p3    # Lcom/android/quicksearchbox/ShortcutRefresher;
    .param p4    # Landroid/os/Handler;
    .param p5    # Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;

    const-string v7, "qsb-log.db"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;-><init>(Landroid/content/Context;Lcom/android/quicksearchbox/Config;Lcom/android/quicksearchbox/Corpora;Lcom/android/quicksearchbox/ShortcutRefresher;Landroid/os/Handler;Ljava/util/concurrent/Executor;Ljava/lang/String;)V

    return-object v0
.end method

.method private getCorpusScores()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/Config;->getMinClicksForSourceRanking()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->getCorpusScores(I)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private getIconUriString(Lcom/android/quicksearchbox/Source;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/Source;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "0"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const-string v2, "android.resource"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "content"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "file"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move-object v1, p2

    goto :goto_0

    :cond_3
    invoke-interface {p1, p2}, Lcom/android/quicksearchbox/Source;->getIconUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private hasHistory(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 3
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    sget-object v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->HAS_HISTORY_QUERY:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method

.method private makeIntentKey(Lcom/android/quicksearchbox/Suggestion;)Ljava/lang/String;
    .locals 8
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIntentComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->componentNameToString(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v5

    invoke-interface {v5}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "#"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v2, :cond_0

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v7, "#"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v7, "#"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_2

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v7, "#"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v3, :cond_3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method private makeShortcutRow(Lcom/android/quicksearchbox/Suggestion;)Landroid/content/ContentValues;
    .locals 19
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIntentComponent()Landroid/content/ComponentName;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->componentNameToString(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v9

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIntentDataString()Ljava/lang/String;

    move-result-object v10

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v13

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIntentExtraData()Ljava/lang/String;

    move-result-object v11

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v14

    invoke-interface {v14}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-direct/range {p0 .. p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->makeIntentKey(Lcom/android/quicksearchbox/Suggestion;)Ljava/lang/String;

    move-result-object v12

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIcon1()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->getIconUriString(Lcom/android/quicksearchbox/Source;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionIcon2()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v14, v1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->getIconUriString(Lcom/android/quicksearchbox/Source;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x0

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getExtras()Lcom/android/quicksearchbox/SuggestionExtras;

    move-result-object v4

    if-eqz v4, :cond_0

    :try_start_0
    invoke-interface {v4}, Lcom/android/quicksearchbox/SuggestionExtras;->toJsonString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :cond_0
    :goto_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_key:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->source:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->source_version_code:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    invoke-interface {v14}, Lcom/android/quicksearchbox/Source;->getVersionCode()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->format:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionFormat()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->title:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionText1()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->description:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionText2()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->description_url:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionText2Url()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->icon1:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->icon2:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_action:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_component:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_data:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_query:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_extradata:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->shortcut_id:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getShortcutId()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->isSpinnerWhileRefreshing()Z

    move-result v16

    if-eqz v16, :cond_1

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->spinner_while_refreshing:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    const-string v17, "true"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->log_type:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {p1 .. p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionLogType()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v16, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->custom_columns:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual/range {v16 .. v16}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    :catch_0
    move-exception v3

    const-string v16, "QSB.ShortcutRepositoryImplLog"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Could not flatten extras to JSON from "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private static nextString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->codePointBefore(I)I

    move-result v0

    add-int/lit8 v3, v0, 0x1

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    sub-int v1, v2, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v5, 0x0

    invoke-virtual {v4, p0, v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private runQueryAsync(Lcom/android/quicksearchbox/util/SQLiteAsyncQuery;Lcom/android/quicksearchbox/util/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/android/quicksearchbox/util/SQLiteAsyncQuery",
            "<TA;>;",
            "Lcom/android/quicksearchbox/util/Consumer",
            "<TA;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mLogExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$2;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Lcom/android/quicksearchbox/util/SQLiteAsyncQuery;Lcom/android/quicksearchbox/util/Consumer;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private runTransactionAsync(Lcom/android/quicksearchbox/util/SQLiteTransaction;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/util/SQLiteTransaction;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mLogExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$1;

    invoke-direct {v1, p0, p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$1;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Lcom/android/quicksearchbox/util/SQLiteTransaction;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private shouldRefresh(Lcom/android/quicksearchbox/Suggestion;)Z
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mRefresher:Lcom/android/quicksearchbox/ShortcutRefresher;

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v1

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getShortcutId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/android/quicksearchbox/ShortcutRefresher;->shouldRefresh(Lcom/android/quicksearchbox/Source;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private stringToComponentName(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public clearHistory()V
    .locals 1

    new-instance v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$5;

    invoke-direct {v0, p0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$5;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;)V

    invoke-direct {p0, v0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->runTransactionAsync(Lcom/android/quicksearchbox/util/SQLiteTransaction;)V

    return-void
.end method

.method public close()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->getOpenHelper()Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;->close()V

    return-void
.end method

.method public deleteRepository()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->getOpenHelper()Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;->deleteDatabase()V

    return-void
.end method

.method getCorpusScores(I)Ljava/util/Map;
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v5, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mOpenHelper:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

    invoke-virtual {v5}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v5, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->SOURCE_RANKING_SQL:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/HashMap;-><init>(I)V

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;->corpus:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;

    invoke-virtual {v5}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;->ordinal()I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;->total_clicks:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;

    invoke-virtual {v5}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SourceStats;->ordinal()I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-object v1
.end method

.method public getCorpusScores(Lcom/android/quicksearchbox/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/util/Consumer",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    new-instance v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$7;

    invoke-direct {v0, p0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$7;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;)V

    invoke-direct {p0, v0, p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->runQueryAsync(Lcom/android/quicksearchbox/util/SQLiteAsyncQuery;Lcom/android/quicksearchbox/util/Consumer;)V

    return-void
.end method

.method protected getOpenHelper()Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mOpenHelper:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

    return-object v0
.end method

.method getShortcutsForQuery(Ljava/lang/String;Ljava/util/Collection;ZJ)Lcom/android/quicksearchbox/ShortcutCursor;
    .locals 18
    .param p1    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;ZJ)",
            "Lcom/android/quicksearchbox/ShortcutCursor;"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mEmptyQueryShortcutQuery:Ljava/lang/String;

    move-object/from16 v17, v0

    :goto_0
    move-object/from16 v0, p1

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->buildShortcutQueryParams(Ljava/lang/String;J)[Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mOpenHelper:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;

    invoke-virtual {v3}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$DbOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    move-object/from16 v0, v17

    invoke-virtual {v12, v0, v15}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    const/4 v3, 0x0

    :goto_1
    return-object v3

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mShortcutQuery:Ljava/lang/String;

    move-object/from16 v17, v0

    goto :goto_0

    :cond_1
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/quicksearchbox/Corpus;

    invoke-interface {v10}, Lcom/android/quicksearchbox/Corpus;->getSources()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/quicksearchbox/Source;

    invoke-interface/range {v16 .. v16}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v9, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    new-instance v3, Lcom/android/quicksearchbox/ShortcutCursor;

    new-instance v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SuggestionCursorImpl;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v9, v1, v11}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$SuggestionCursorImpl;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Ljava/util/HashMap;Ljava/lang/String;Landroid/database/Cursor;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mUiThread:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mRefresher:Lcom/android/quicksearchbox/ShortcutRefresher;

    move/from16 v5, p3

    move-object/from16 v8, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/quicksearchbox/ShortcutCursor;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;ZLandroid/os/Handler;Lcom/android/quicksearchbox/ShortcutRefresher;Lcom/android/quicksearchbox/ShortcutRepository;)V

    goto :goto_1
.end method

.method public getShortcutsForQuery(Ljava/lang/String;Ljava/util/Collection;ZLcom/android/quicksearchbox/util/Consumer;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;Z",
            "Lcom/android/quicksearchbox/util/Consumer",
            "<",
            "Lcom/android/quicksearchbox/ShortcutCursor;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-object v8, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mLogExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$6;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$6;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Ljava/lang/String;Ljava/util/Collection;ZJLcom/android/quicksearchbox/util/Consumer;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public hasHistory(Lcom/android/quicksearchbox/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/util/Consumer",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$3;

    invoke-direct {v0, p0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$3;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;)V

    invoke-direct {p0, v0, p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->runQueryAsync(Lcom/android/quicksearchbox/util/SQLiteAsyncQuery;Lcom/android/quicksearchbox/util/Consumer;)V

    return-void
.end method

.method refreshShortcut(Lcom/android/quicksearchbox/Source;Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;)V
    .locals 5
    .param p1    # Lcom/android/quicksearchbox/Source;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/quicksearchbox/SuggestionCursor;

    const/4 v4, 0x0

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "source"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-nez p2, :cond_1

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "shortcutId"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    aput-object p2, v1, v4

    const/4 v2, 0x1

    invoke-interface {p1}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    if-eqz p3, :cond_2

    invoke-interface {p3}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    const/4 v0, 0x0

    :goto_0
    new-instance v2, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$8;

    invoke-direct {v2, p0, v0, p2, v1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$8;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->runTransactionAsync(Lcom/android/quicksearchbox/util/SQLiteTransaction;)V

    return-void

    :cond_3
    invoke-interface {p3, v4}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    invoke-direct {p0, p3}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->makeShortcutRow(Lcom/android/quicksearchbox/Suggestion;)Landroid/content/ContentValues;

    move-result-object v0

    goto :goto_0
.end method

.method public removeFromHistory(Lcom/android/quicksearchbox/SuggestionCursor;I)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I

    invoke-interface {p1, p2}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->makeIntentKey(Lcom/android/quicksearchbox/Suggestion;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$4;

    invoke-direct {v1, p0, v0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$4;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->runTransactionAsync(Lcom/android/quicksearchbox/util/SQLiteTransaction;)V

    return-void
.end method

.method public reportClick(Lcom/android/quicksearchbox/SuggestionCursor;I)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->reportClickAtTime(Lcom/android/quicksearchbox/SuggestionCursor;IJ)V

    return-void
.end method

.method reportClickAtTime(Lcom/android/quicksearchbox/SuggestionCursor;IJ)V
    .locals 7
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I
    .param p3    # J

    invoke-interface {p1, p2}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    const-string v4, "_-1"

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getShortcutId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mCorpora:Lcom/android/quicksearchbox/Corpora;

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/android/quicksearchbox/Corpora;->getCorpusForSource(Lcom/android/quicksearchbox/Source;)Lcom/android/quicksearchbox/Corpus;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v4, "QSB.ShortcutRepositoryImplLog"

    const-string v5, "no corpus for clicked suggestion"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->mRefresher:Lcom/android/quicksearchbox/ShortcutRefresher;

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v5

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getShortcutId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/android/quicksearchbox/ShortcutRefresher;->markShortcutRefreshed(Lcom/android/quicksearchbox/Source;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->makeShortcutRow(Lcom/android/quicksearchbox/Suggestion;)Landroid/content/ContentValues;

    move-result-object v3

    sget-object v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->intent_key:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;

    invoke-virtual {v4}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$Shortcuts;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sget-object v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->intent_key:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    invoke-virtual {v4}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->query:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    invoke-virtual {v4}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getUserQuery()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->hit_time:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    invoke-virtual {v4}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->corpus:Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;

    invoke-virtual {v4}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$ClickLog;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, Lcom/android/quicksearchbox/Corpus;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$9;

    invoke-direct {v4, p0, v3, v0}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog$9;-><init>(Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    invoke-direct {p0, v4}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->runTransactionAsync(Lcom/android/quicksearchbox/util/SQLiteTransaction;)V

    goto :goto_0
.end method

.method public updateShortcut(Lcom/android/quicksearchbox/Source;Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/Source;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/quicksearchbox/SuggestionCursor;

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/quicksearchbox/ShortcutRepositoryImplLog;->refreshShortcut(Lcom/android/quicksearchbox/Source;Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;)V

    return-void
.end method
