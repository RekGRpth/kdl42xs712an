.class public Lcom/android/quicksearchbox/SuggestionData;
.super Ljava/lang/Object;
.source "SuggestionData.java"

# interfaces
.implements Lcom/android/quicksearchbox/Suggestion;


# instance fields
.field private mExtras:Lcom/android/quicksearchbox/SuggestionExtras;

.field private mFormat:Ljava/lang/String;

.field private mIcon1:Ljava/lang/String;

.field private mIcon2:Ljava/lang/String;

.field private mIntentAction:Ljava/lang/String;

.field private mIntentData:Ljava/lang/String;

.field private mIntentExtraData:Ljava/lang/String;

.field private mIsHistory:Z

.field private mIsShortcut:Z

.field private mLogType:Ljava/lang/String;

.field private mShortcutId:Ljava/lang/String;

.field private final mSource:Lcom/android/quicksearchbox/Source;

.field private mSpinnerWhileRefreshing:Z

.field private mSuggestionQuery:Ljava/lang/String;

.field private mText1:Ljava/lang/String;

.field private mText2:Ljava/lang/String;

.field private mText2Url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/quicksearchbox/Source;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/Source;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    return-void
.end method

.method private appendField(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    if-eqz p3, :cond_0

    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/android/quicksearchbox/SuggestionData;

    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mFormat:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mFormat:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mFormat:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mFormat:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon1:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mIcon1:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    :cond_6
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon1:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mIcon1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    goto :goto_0

    :cond_7
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon2:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mIcon2:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    goto :goto_0

    :cond_8
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon2:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mIcon2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    goto :goto_0

    :cond_9
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    if-nez v3, :cond_a

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v1, v2

    goto :goto_0

    :cond_a
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    goto :goto_0

    :cond_b
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    if-nez v3, :cond_c

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v1, v2

    goto :goto_0

    :cond_c
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    goto/16 :goto_0

    :cond_d
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentExtraData:Ljava/lang/String;

    if-nez v3, :cond_e

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mIntentExtraData:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v1, v2

    goto/16 :goto_0

    :cond_e
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentExtraData:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mIntentExtraData:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    goto/16 :goto_0

    :cond_f
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    if-nez v3, :cond_10

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    if-eqz v3, :cond_11

    move v1, v2

    goto/16 :goto_0

    :cond_10
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    move v1, v2

    goto/16 :goto_0

    :cond_11
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    if-nez v3, :cond_12

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    if-eqz v3, :cond_13

    move v1, v2

    goto/16 :goto_0

    :cond_12
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    move v1, v2

    goto/16 :goto_0

    :cond_13
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    if-nez v3, :cond_14

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    if-eqz v3, :cond_15

    move v1, v2

    goto/16 :goto_0

    :cond_14
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    move v1, v2

    goto/16 :goto_0

    :cond_15
    iget-boolean v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mSpinnerWhileRefreshing:Z

    iget-boolean v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mSpinnerWhileRefreshing:Z

    if-eq v3, v4, :cond_16

    move v1, v2

    goto/16 :goto_0

    :cond_16
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    if-nez v3, :cond_17

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    if-eqz v3, :cond_18

    move v1, v2

    goto/16 :goto_0

    :cond_17
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    move v1, v2

    goto/16 :goto_0

    :cond_18
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    if-nez v3, :cond_19

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    if-eqz v3, :cond_1a

    move v1, v2

    goto/16 :goto_0

    :cond_19
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    move v1, v2

    goto/16 :goto_0

    :cond_1a
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mText2:Ljava/lang/String;

    if-nez v3, :cond_1b

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionData;->mText2:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    goto/16 :goto_0

    :cond_1b
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mText2:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionData;->mText2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto/16 :goto_0
.end method

.method public getExtras()Lcom/android/quicksearchbox/SuggestionExtras;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mExtras:Lcom/android/quicksearchbox/SuggestionExtras;

    return-object v0
.end method

.method public getShortcutId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionFormat()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIcon1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon1:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIcon2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon2:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIntentAction()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getDefaultIntentAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSuggestionIntentComponent()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getIntentComponent()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestionIntentDataString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionIntentExtraData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentExtraData:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionLogType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionSource()Lcom/android/quicksearchbox/Source;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    return-object v0
.end method

.method public getSuggestionText1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionText2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mText2:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestionText2Url()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mText2Url:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    const/4 v3, 0x0

    const/16 v0, 0x1f

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mFormat:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon1:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon2:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v3

    :goto_3
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    if-nez v2, :cond_4

    move v2, v3

    :goto_4
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentExtraData:Ljava/lang/String;

    if-nez v2, :cond_5

    move v2, v3

    :goto_5
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    if-nez v2, :cond_6

    move v2, v3

    :goto_6
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    if-nez v2, :cond_7

    move v2, v3

    :goto_7
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    if-nez v2, :cond_8

    move v2, v3

    :goto_8
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-boolean v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mSpinnerWhileRefreshing:Z

    if-eqz v2, :cond_9

    const/16 v2, 0x4cf

    :goto_9
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    if-nez v2, :cond_a

    move v2, v3

    :goto_a
    add-int v1, v4, v2

    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    if-nez v2, :cond_b

    move v2, v3

    :goto_b
    add-int v1, v4, v2

    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/android/quicksearchbox/SuggestionData;->mText2:Ljava/lang/String;

    if-nez v4, :cond_c

    :goto_c
    add-int v1, v2, v3

    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mFormat:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon1:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon2:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentExtraData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_7
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_8
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_9
    const/16 v2, 0x4d5

    goto :goto_9

    :cond_a
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_b
    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_c
    iget-object v3, p0, Lcom/android/quicksearchbox/SuggestionData;->mText2:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_c
.end method

.method public isHistorySuggestion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mIsHistory:Z

    return v0
.end method

.method public isSpinnerWhileRefreshing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mSpinnerWhileRefreshing:Z

    return v0
.end method

.method public isSuggestionShortcut()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/quicksearchbox/SuggestionData;->mIsShortcut:Z

    return v0
.end method

.method public isWebSearchSuggestion()Z
    .locals 2

    const-string v0, "android.intent.action.WEB_SEARCH"

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SuggestionData;->getSuggestionIntentAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setExtras(Lcom/android/quicksearchbox/SuggestionExtras;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/SuggestionExtras;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mExtras:Lcom/android/quicksearchbox/SuggestionExtras;

    return-void
.end method

.method public setFormat(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mFormat:Ljava/lang/String;

    return-object p0
.end method

.method public setIcon1(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon1:Ljava/lang/String;

    return-object p0
.end method

.method public setIcon2(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mIcon2:Ljava/lang/String;

    return-object p0
.end method

.method public setIntentAction(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    return-object p0
.end method

.method public setIntentData(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    return-object p0
.end method

.method public setIntentExtraData(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentExtraData:Ljava/lang/String;

    return-object p0
.end method

.method public setIsHistory(Z)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mIsHistory:Z

    return-object p0
.end method

.method public setIsShortcut(Z)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mIsShortcut:Z

    return-object p0
.end method

.method public setShortcutId(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    return-object p0
.end method

.method public setSpinnerWhileRefreshing(Z)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mSpinnerWhileRefreshing:Z

    return-object p0
.end method

.method public setSuggestionLogType(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    return-object p0
.end method

.method public setSuggestionQuery(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    return-object p0
.end method

.method public setText1(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    return-object p0
.end method

.method public setText2(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mText2:Ljava/lang/String;

    return-object p0
.end method

.method public setText2Url(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionData;->mText2Url:Ljava/lang/String;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SuggestionData("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "source"

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v2}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/quicksearchbox/SuggestionData;->appendField(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "text1"

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mText1:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/quicksearchbox/SuggestionData;->appendField(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "intentAction"

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentAction:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/quicksearchbox/SuggestionData;->appendField(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "intentData"

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mIntentData:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/quicksearchbox/SuggestionData;->appendField(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "query"

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mSuggestionQuery:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/quicksearchbox/SuggestionData;->appendField(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "shortcutid"

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mShortcutId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/quicksearchbox/SuggestionData;->appendField(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "logtype"

    iget-object v2, p0, Lcom/android/quicksearchbox/SuggestionData;->mLogType:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/quicksearchbox/SuggestionData;->appendField(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
