.class public abstract Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;
.super Ljava/lang/Object;
.source "AbstractGoogleSourceResult.java"

# interfaces
.implements Lcom/android/quicksearchbox/SourceResult;


# instance fields
.field private mPos:I

.field private final mSource:Lcom/android/quicksearchbox/Source;

.field private final mUserQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/quicksearchbox/Source;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Source;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mPos:I

    iput-object p1, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mSource:Lcom/android/quicksearchbox/Source;

    iput-object p2, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mUserQuery:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    return-void
.end method

.method public abstract getCount()I
.end method

.method public getExtraColumns()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtras()Lcom/android/quicksearchbox/SuggestionExtras;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mPos:I

    return v0
.end method

.method public getShortcutId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSource()Lcom/android/quicksearchbox/Source;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mSource:Lcom/android/quicksearchbox/Source;

    return-object v0
.end method

.method public getSuggestionFormat()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSuggestionIcon1()Ljava/lang/String;
    .locals 1

    const v0, 0x7f02003e    # com.android.quicksearchbox.R.drawable.magnifying_glass

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestionIcon2()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSuggestionIntentAction()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getDefaultIntentAction()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestionIntentComponent()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getIntentComponent()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestionIntentDataString()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSuggestionIntentExtraData()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSuggestionLogType()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getSuggestionQuery()Ljava/lang/String;
.end method

.method public getSuggestionSource()Lcom/android/quicksearchbox/Source;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mSource:Lcom/android/quicksearchbox/Source;

    return-object v0
.end method

.method public getSuggestionText1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestionText2()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSuggestionText2Url()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getUserQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mUserQuery:Ljava/lang/String;

    return-object v0
.end method

.method public isHistorySuggestion()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSpinnerWhileRefreshing()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSuggestionShortcut()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isWebSearchSuggestion()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public moveTo(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mPos:I

    return-void
.end method

.method public moveToNext()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->getCount()I

    move-result v0

    iget v2, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mPos:I

    if-lt v2, v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v2, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mPos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mPos:I

    iget v2, p0, Lcom/android/quicksearchbox/google/AbstractGoogleSourceResult;->mPos:I

    if-ge v2, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
