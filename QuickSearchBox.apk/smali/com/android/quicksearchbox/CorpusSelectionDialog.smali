.class public Lcom/android/quicksearchbox/CorpusSelectionDialog;
.super Landroid/app/Dialog;
.source "CorpusSelectionDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/CorpusSelectionDialog$1;,
        Lcom/android/quicksearchbox/CorpusSelectionDialog$OnCorpusSelectedListener;,
        Lcom/android/quicksearchbox/CorpusSelectionDialog$CorpusEditListener;,
        Lcom/android/quicksearchbox/CorpusSelectionDialog$CorpusClickListener;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/android/quicksearchbox/ui/CorporaAdapter;

.field private mCorpus:Lcom/android/quicksearchbox/Corpus;

.field private mCorpusGrid:Landroid/widget/GridView;

.field private mEditItems:Landroid/widget/ImageView;

.field private mListener:Lcom/android/quicksearchbox/CorpusSelectionDialog$OnCorpusSelectedListener;

.field private final mSettings:Lcom/android/quicksearchbox/SearchSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/quicksearchbox/SearchSettings;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/quicksearchbox/SearchSettings;

    const v0, 0x7f0d000d    # com.android.quicksearchbox.R.style.Theme_SelectSearchSource

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object p2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mSettings:Lcom/android/quicksearchbox/SearchSettings;

    return-void
.end method

.method private getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/quicksearchbox/QsbApplication;->get(Landroid/content/Context;)Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    return-object v0
.end method

.method private getSearchActivity()Lcom/android/quicksearchbox/SearchActivity;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/SearchActivity;

    return-object v0
.end method

.method private setAdapter(Lcom/android/quicksearchbox/ui/CorporaAdapter;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/ui/CorporaAdapter;

    iget-object v0, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mAdapter:Lcom/android/quicksearchbox/ui/CorporaAdapter;

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mAdapter:Lcom/android/quicksearchbox/ui/CorporaAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mAdapter:Lcom/android/quicksearchbox/ui/CorporaAdapter;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/CorporaAdapter;->close()V

    :cond_1
    iput-object p1, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mAdapter:Lcom/android/quicksearchbox/ui/CorporaAdapter;

    iget-object v0, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mCorpusGrid:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mAdapter:Lcom/android/quicksearchbox/ui/CorporaAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method


# virtual methods
.method protected getSettings()Lcom/android/quicksearchbox/SearchSettings;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mSettings:Lcom/android/quicksearchbox/SearchSettings;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    invoke-direct {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->getSearchActivity()Lcom/android/quicksearchbox/SearchActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/SearchActivity;->startedIntoCorpusSelectionDialog()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/SearchActivity;->onBackPressed()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->cancel()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const/4 v4, -0x1

    const v2, 0x7f040003    # com.android.quicksearchbox.R.layout.corpus_selection_dialog

    invoke-virtual {p0, v2}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->setContentView(I)V

    const v2, 0x7f0f000c    # com.android.quicksearchbox.R.id.corpus_grid

    invoke-virtual {p0, v2}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    iput-object v2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mCorpusGrid:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mCorpusGrid:Landroid/widget/GridView;

    new-instance v3, Lcom/android/quicksearchbox/CorpusSelectionDialog$CorpusClickListener;

    invoke-direct {v3, p0, v5}, Lcom/android/quicksearchbox/CorpusSelectionDialog$CorpusClickListener;-><init>(Lcom/android/quicksearchbox/CorpusSelectionDialog;Lcom/android/quicksearchbox/CorpusSelectionDialog$1;)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mCorpusGrid:Landroid/widget/GridView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setFocusable(Z)V

    const v2, 0x7f0f000b    # com.android.quicksearchbox.R.id.corpus_edit_items

    invoke-virtual {p0, v2}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mEditItems:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mEditItems:Landroid/widget/ImageView;

    new-instance v3, Lcom/android/quicksearchbox/CorpusSelectionDialog$CorpusEditListener;

    invoke-direct {v3, p0, v5}, Lcom/android/quicksearchbox/CorpusSelectionDialog$CorpusEditListener;-><init>(Lcom/android/quicksearchbox/CorpusSelectionDialog;Lcom/android/quicksearchbox/CorpusSelectionDialog$1;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x20000

    or-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/16 v2, 0x13

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mEditItems:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->cancel()V

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isPrintingKey()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->cancel()V

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->getSettings()Lcom/android/quicksearchbox/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p1, v1}, Lcom/android/quicksearchbox/SearchSettings;->addMenuItems(Landroid/view/Menu;Z)V

    return v1
.end method

.method protected onStart()V
    .locals 4

    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    invoke-direct {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/quicksearchbox/QsbApplication;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v1

    new-instance v0, Lcom/android/quicksearchbox/ui/CorporaAdapter;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f040001    # com.android.quicksearchbox.R.layout.corpus_grid_item

    invoke-direct {v0, v2, v1, v3}, Lcom/android/quicksearchbox/ui/CorporaAdapter;-><init>(Landroid/content/Context;Lcom/android/quicksearchbox/Corpora;I)V

    iget-object v2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mCorpus:Lcom/android/quicksearchbox/Corpus;

    invoke-virtual {v0, v2}, Lcom/android/quicksearchbox/ui/CorporaAdapter;->setCurrentCorpus(Lcom/android/quicksearchbox/Corpus;)V

    invoke-direct {p0, v0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->setAdapter(Lcom/android/quicksearchbox/ui/CorporaAdapter;)V

    iget-object v2, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mCorpusGrid:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mCorpus:Lcom/android/quicksearchbox/Corpus;

    invoke-virtual {v0, v3}, Lcom/android/quicksearchbox/ui/CorporaAdapter;->getCorpusPosition(Lcom/android/quicksearchbox/Corpus;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setSelection(I)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->setAdapter(Lcom/android/quicksearchbox/ui/CorporaAdapter;)V

    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->cancel()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected selectCorpus(Lcom/android/quicksearchbox/Corpus;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Corpus;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->dismiss()V

    iget-object v1, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mListener:Lcom/android/quicksearchbox/CorpusSelectionDialog$OnCorpusSelectedListener;

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mListener:Lcom/android/quicksearchbox/CorpusSelectionDialog$OnCorpusSelectedListener;

    invoke-interface {v1, v0}, Lcom/android/quicksearchbox/CorpusSelectionDialog$OnCorpusSelectedListener;->onCorpusSelected(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Lcom/android/quicksearchbox/Corpus;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setOnCorpusSelectedListener(Lcom/android/quicksearchbox/CorpusSelectionDialog$OnCorpusSelectedListener;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/CorpusSelectionDialog$OnCorpusSelectedListener;

    iput-object p1, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mListener:Lcom/android/quicksearchbox/CorpusSelectionDialog$OnCorpusSelectedListener;

    return-void
.end method

.method public show(Lcom/android/quicksearchbox/Corpus;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/Corpus;

    iput-object p1, p0, Lcom/android/quicksearchbox/CorpusSelectionDialog;->mCorpus:Lcom/android/quicksearchbox/Corpus;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->show()V

    return-void
.end method
