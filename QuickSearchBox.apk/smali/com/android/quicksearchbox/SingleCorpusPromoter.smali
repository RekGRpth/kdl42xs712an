.class public Lcom/android/quicksearchbox/SingleCorpusPromoter;
.super Ljava/lang/Object;
.source "SingleCorpusPromoter.java"

# interfaces
.implements Lcom/android/quicksearchbox/Promoter;


# instance fields
.field private final mAllowedSources:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mCorpus:Lcom/android/quicksearchbox/Corpus;

.field private final mMaxShortcuts:I


# direct methods
.method public constructor <init>(Lcom/android/quicksearchbox/Corpus;I)V
    .locals 4
    .param p1    # Lcom/android/quicksearchbox/Corpus;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/SingleCorpusPromoter;->mCorpus:Lcom/android/quicksearchbox/Corpus;

    iput p2, p0, Lcom/android/quicksearchbox/SingleCorpusPromoter;->mMaxShortcuts:I

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/android/quicksearchbox/SingleCorpusPromoter;->mAllowedSources:Ljava/util/Set;

    invoke-interface {p1}, Lcom/android/quicksearchbox/Corpus;->getSources()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/quicksearchbox/Source;

    iget-object v2, p0, Lcom/android/quicksearchbox/SingleCorpusPromoter;->mAllowedSources:Ljava/util/Set;

    invoke-interface {v1}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method private promoteUntilFull(Lcom/android/quicksearchbox/SuggestionCursor;ILcom/android/quicksearchbox/ListSuggestionCursor;)V
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I
    .param p3    # Lcom/android/quicksearchbox/ListSuggestionCursor;

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p3}, Lcom/android/quicksearchbox/ListSuggestionCursor;->getCount()I

    move-result v2

    if-ge v2, p2, :cond_0

    invoke-interface {p1, v1}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/SingleCorpusPromoter;->accept(Lcom/android/quicksearchbox/Suggestion;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Lcom/android/quicksearchbox/SuggestionPosition;

    invoke-direct {v2, p1, v1}, Lcom/android/quicksearchbox/SuggestionPosition;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    invoke-virtual {p3, v2}, Lcom/android/quicksearchbox/ListSuggestionCursor;->add(Lcom/android/quicksearchbox/Suggestion;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected accept(Lcom/android/quicksearchbox/Suggestion;)Z
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleCorpusPromoter;->mAllowedSources:Ljava/util/Set;

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public pickPromoted(Lcom/android/quicksearchbox/Suggestions;ILcom/android/quicksearchbox/ListSuggestionCursor;)V
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/Suggestions;
    .param p2    # I
    .param p3    # Lcom/android/quicksearchbox/ListSuggestionCursor;

    invoke-virtual {p1}, Lcom/android/quicksearchbox/Suggestions;->getShortcuts()Lcom/android/quicksearchbox/ShortcutCursor;

    move-result-object v1

    iget v2, p0, Lcom/android/quicksearchbox/SingleCorpusPromoter;->mMaxShortcuts:I

    invoke-direct {p0, v1, v2, p3}, Lcom/android/quicksearchbox/SingleCorpusPromoter;->promoteUntilFull(Lcom/android/quicksearchbox/SuggestionCursor;ILcom/android/quicksearchbox/ListSuggestionCursor;)V

    iget-object v2, p0, Lcom/android/quicksearchbox/SingleCorpusPromoter;->mCorpus:Lcom/android/quicksearchbox/Corpus;

    invoke-virtual {p1, v2}, Lcom/android/quicksearchbox/Suggestions;->getCorpusResult(Lcom/android/quicksearchbox/Corpus;)Lcom/android/quicksearchbox/CorpusResult;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/android/quicksearchbox/SingleCorpusPromoter;->promoteUntilFull(Lcom/android/quicksearchbox/SuggestionCursor;ILcom/android/quicksearchbox/ListSuggestionCursor;)V

    return-void
.end method
