.class public abstract Lcom/android/quicksearchbox/AbstractSuggestionExtras;
.super Ljava/lang/Object;
.source "AbstractSuggestionExtras.java"

# interfaces
.implements Lcom/android/quicksearchbox/SuggestionExtras;


# instance fields
.field private final mMore:Lcom/android/quicksearchbox/SuggestionExtras;


# direct methods
.method protected constructor <init>(Lcom/android/quicksearchbox/SuggestionExtras;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/SuggestionExtras;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/AbstractSuggestionExtras;->mMore:Lcom/android/quicksearchbox/SuggestionExtras;

    return-void
.end method


# virtual methods
.method protected abstract doGetExtra(Ljava/lang/String;)Ljava/lang/String;
.end method

.method protected abstract doGetExtraColumnNames()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public getExtra(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/AbstractSuggestionExtras;->doGetExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/android/quicksearchbox/AbstractSuggestionExtras;->mMore:Lcom/android/quicksearchbox/SuggestionExtras;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/quicksearchbox/AbstractSuggestionExtras;->mMore:Lcom/android/quicksearchbox/SuggestionExtras;

    invoke-interface {v1, p1}, Lcom/android/quicksearchbox/SuggestionExtras;->getExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getExtraColumnNames()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/AbstractSuggestionExtras;->doGetExtraColumnNames()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/android/quicksearchbox/AbstractSuggestionExtras;->mMore:Lcom/android/quicksearchbox/SuggestionExtras;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/quicksearchbox/AbstractSuggestionExtras;->mMore:Lcom/android/quicksearchbox/SuggestionExtras;

    invoke-interface {v1}, Lcom/android/quicksearchbox/SuggestionExtras;->getExtraColumnNames()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-object v0
.end method

.method public toJsonString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v0, Lcom/android/quicksearchbox/JsonBackedSuggestionExtras;

    invoke-direct {v0, p0}, Lcom/android/quicksearchbox/JsonBackedSuggestionExtras;-><init>(Lcom/android/quicksearchbox/SuggestionExtras;)V

    invoke-virtual {v0}, Lcom/android/quicksearchbox/JsonBackedSuggestionExtras;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
