.class public Lcom/android/quicksearchbox/MultiSourceCorpus$Result;
.super Lcom/android/quicksearchbox/ListSuggestionCursor;
.source "MultiSourceCorpus.java"

# interfaces
.implements Lcom/android/quicksearchbox/CorpusResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/quicksearchbox/MultiSourceCorpus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Result"
.end annotation


# instance fields
.field private final mLatency:I

.field private final mResults:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/quicksearchbox/SourceResult;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/quicksearchbox/MultiSourceCorpus;


# direct methods
.method public constructor <init>(Lcom/android/quicksearchbox/MultiSourceCorpus;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/quicksearchbox/SourceResult;",
            ">;I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->this$0:Lcom/android/quicksearchbox/MultiSourceCorpus;

    invoke-direct {p0, p2}, Lcom/android/quicksearchbox/ListSuggestionCursor;-><init>(Ljava/lang/String;)V

    iput-object p3, p0, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->mResults:Ljava/util/ArrayList;

    iput p4, p0, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->mLatency:I

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    invoke-super {p0}, Lcom/android/quicksearchbox/ListSuggestionCursor;->close()V

    iget-object v2, p0, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->mResults:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/quicksearchbox/SourceResult;

    invoke-interface {v1}, Lcom/android/quicksearchbox/SourceResult;->close()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public fill()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->getResults()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/quicksearchbox/SourceResult;

    invoke-interface {v3}, Lcom/android/quicksearchbox/SourceResult;->getCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-interface {v3, v1}, Lcom/android/quicksearchbox/SourceResult;->moveTo(I)V

    new-instance v4, Lcom/android/quicksearchbox/SuggestionPosition;

    invoke-direct {v4, v3}, Lcom/android/quicksearchbox/SuggestionPosition;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;)V

    invoke-virtual {p0, v4}, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->add(Lcom/android/quicksearchbox/Suggestion;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getCorpus()Lcom/android/quicksearchbox/Corpus;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->this$0:Lcom/android/quicksearchbox/MultiSourceCorpus;

    return-object v0
.end method

.method public getLatency()I
    .locals 1

    iget v0, p0, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->mLatency:I

    return v0
.end method

.method protected getResults()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/quicksearchbox/SourceResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->mResults:Ljava/util/ArrayList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->getUserQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";n="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/MultiSourceCorpus$Result;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
