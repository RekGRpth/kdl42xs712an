.class public Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;
.super Landroid/widget/ExpandableListView;
.source "ClusteredSuggestionsView.java"

# interfaces
.implements Lcom/android/quicksearchbox/ui/SuggestionsListView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ExpandableListView;",
        "Lcom/android/quicksearchbox/ui/SuggestionsListView",
        "<",
        "Landroid/widget/ExpandableListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ExpandableListAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public expandAll()V
    .locals 3

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v2}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getListAdapter()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListAdapter;

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->expandGroup(I)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getSuggestionsAdapter()Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ExpandableListAdapter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    return-object v0
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ExpandableListView;->onFinishInflate()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->setItemsCanFocus(Z)V

    new-instance v0, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView$1;

    invoke-direct {v0, p0}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView$1;-><init>(Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;)V

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    return-void
.end method

.method public setLimitSuggestionsToViewHeight(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setSuggestionsAdapter(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ExpandableListAdapter;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, v0}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    return-void

    :cond_0
    invoke-interface {p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getListAdapter()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListAdapter;

    goto :goto_0
.end method
