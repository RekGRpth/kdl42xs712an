.class public Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;
.super Lorg/jivesoftware/smack/packet/IQ;
.source "SharedStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus$1;
    }
.end annotation


# instance fields
.field private mDefaultStatusList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDndStatusList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHasStatusMinVersion:Z

.field private mInvisible:Z

.field private mShow:Ljava/lang/String;

.field private mStatus:Ljava/lang/String;

.field private mStatusListContentsMax:I

.field private mStatusListMax:I

.field private mStatusMax:I

.field private mStatusMinVersion:I

.field private mUseServerAttributes:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gtalkservice/Presence;)V
    .locals 2
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    invoke-direct {p0}, Lorg/jivesoftware/smack/packet/IQ;-><init>()V

    sget-object v0, Lorg/jivesoftware/smack/packet/IQ$Type;->SET:Lorg/jivesoftware/smack/packet/IQ$Type;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setType(Lorg/jivesoftware/smack/packet/IQ$Type;)V

    sget-object v0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus$1;->$SwitchMap$com$google$android$gtalkservice$Presence$Show:[I

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->getShow()Lcom/google/android/gtalkservice/Presence$Show;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gtalkservice/Presence$Show;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->getStatus()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setStatus(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->isInvisible()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setInvisible(Z)V

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->getDefaultStatusList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setDefaultStatusList(Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/google/android/gtalkservice/Presence;->getDndStatusList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setDndStatusList(Ljava/util/List;)V

    return-void

    :pswitch_0
    const-string v0, "default"

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setShow(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "dnd"

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setShow(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(Lorg/jivesoftware/smack/packet/IQ$Type;)V
    .locals 0
    .param p1    # Lorg/jivesoftware/smack/packet/IQ$Type;

    invoke-direct {p0}, Lorg/jivesoftware/smack/packet/IQ;-><init>()V

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->setType(Lorg/jivesoftware/smack/packet/IQ$Type;)V

    return-void
.end method

.method private addServerAttribute(Ljava/lang/StringBuilder;Ljava/lang/String;II)V
    .locals 2
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    if-ne p3, p4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private appendStatusList(Lcom/google/common/io/protocol/ProtoBuf;Ljava/util/List;)V
    .locals 3
    .param p1    # Lcom/google/common/io/protocol/ProtoBuf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/io/protocol/ProtoBuf;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v1}, Lcom/google/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private appendStatusList(Ljava/lang/StringBuilder;Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/16 v4, 0x3e

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/16 v2, 0x3c

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "status"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lorg/jivesoftware/smack/util/StringUtils;->escapeForXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "</"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "status"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static final getVersion()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method


# virtual methods
.method public getChildElementXML()Ljava/lang/String;
    .locals 8

    const/4 v5, 0x0

    const/16 v7, 0x3c

    const/16 v6, 0x3e

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getType()Lorg/jivesoftware/smack/packet/IQ$Type;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->isInvisible()Z

    move-result v1

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "query"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " xmlns=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "google:shared-status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mUseServerAttributes:Z

    if-eqz v3, :cond_6

    const-string v3, "status-max"

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatusMax()I

    move-result v4

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->addServerAttribute(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    const-string v3, "status-list-max"

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatusListMax()I

    move-result v4

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->addServerAttribute(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    const-string v3, "status-list-contents-max"

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatusListContentsMax()I

    move-result v4

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->addServerAttribute(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->hasStatusMinVersion()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "status-min-ver"

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getStatusMinVersion()I

    move-result v4

    const/4 v5, -0x1

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->addServerAttribute(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    :cond_0
    :goto_0
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v3, Lorg/jivesoftware/smack/packet/IQ$Type;->GET:Lorg/jivesoftware/smack/packet/IQ$Type;

    if-eq v2, v3, :cond_5

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatus:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatus:Ljava/lang/String;

    invoke-static {v3}, Lorg/jivesoftware/smack/util/StringUtils;->escapeForXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "</"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "status"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mShow:Ljava/lang/String;

    if-eqz v3, :cond_2

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "show"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mShow:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "</"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "show"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDefaultStatusList:Ljava/util/List;

    if-eqz v3, :cond_3

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "status-list"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " show=\'default\'>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDefaultStatusList:Ljava/util/List;

    invoke-direct {p0, v0, v3}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->appendStatusList(Ljava/lang/StringBuilder;Ljava/util/List;)V

    const-string v3, "</"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "status-list"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDndStatusList:Ljava/util/List;

    if-eqz v3, :cond_4

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "status-list"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " show=\'dnd\'>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDndStatusList:Ljava/util/List;

    invoke-direct {p0, v0, v3}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->appendStatusList(Ljava/lang/StringBuilder;Ljava/util/List;)V

    const-string v3, "</"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "status-list"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "invisible"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " value=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v1, :cond_7

    const-string v3, "true"

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' />"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v3, "</"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "query"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_6
    const-string v3, "version"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_7
    const-string v3, "false"

    goto :goto_1
.end method

.method public getDefaultStatusList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDefaultStatusList:Ljava/util/List;

    return-object v0
.end method

.method public getDndStatusList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDndStatusList:Ljava/util/List;

    return-object v0
.end method

.method protected getExtensionProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;
    .locals 13

    const/4 v12, 0x7

    const/4 v11, 0x5

    const/4 v10, 0x1

    const/4 v9, 0x6

    sget-byte v7, Lorg/jivesoftware/smack/XMPPConnection;->CURRENT_VERSION:B

    if-ne v7, v10, :cond_0

    invoke-super {p0}, Lorg/jivesoftware/smack/packet/IQ;->getExtensionProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    new-instance v2, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->EXTENSION:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v7}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v2, v10, v11}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    new-instance v4, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkExtensionsMessageTypes;->SHARED_STATUS:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v4, v7}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->getType()Lorg/jivesoftware/smack/packet/IQ$Type;

    move-result-object v6

    iget-boolean v3, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mInvisible:Z

    sget-object v7, Lorg/jivesoftware/smack/packet/IQ$Type;->GET:Lorg/jivesoftware/smack/packet/IQ$Type;

    if-eq v6, v7, :cond_6

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatus:Ljava/lang/String;

    if-eqz v7, :cond_1

    const/4 v7, 0x4

    iget-object v8, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatus:Ljava/lang/String;

    invoke-virtual {v4, v7, v8}, Lcom/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    :cond_1
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mShow:Ljava/lang/String;

    if-eqz v7, :cond_3

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mShow:Ljava/lang/String;

    const-string v8, "dnd"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v5, 0x1

    :cond_2
    invoke-virtual {v4, v11, v5}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    :cond_3
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDefaultStatusList:Ljava/util/List;

    if-eqz v7, :cond_4

    new-instance v0, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkExtensionsMessageTypes;->SHARED_STATUS:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v7, v9}, Lcom/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v7}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    const/4 v7, 0x0

    invoke-virtual {v0, v12, v7}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDefaultStatusList:Ljava/util/List;

    invoke-direct {p0, v0, v7}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->appendStatusList(Lcom/google/common/io/protocol/ProtoBuf;Ljava/util/List;)V

    invoke-virtual {v4, v9, v0}, Lcom/google/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    :cond_4
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDndStatusList:Ljava/util/List;

    if-eqz v7, :cond_5

    new-instance v1, Lcom/google/common/io/protocol/ProtoBuf;

    sget-object v7, Lcom/google/android/gsf/gtalkservice/proto/GtalkExtensionsMessageTypes;->SHARED_STATUS:Lcom/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v7, v9}, Lcom/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v7}, Lcom/google/common/io/protocol/ProtoBuf;-><init>(Lcom/google/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v1, v12, v10}, Lcom/google/common/io/protocol/ProtoBuf;->setInt(II)V

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDndStatusList:Ljava/util/List;

    invoke-direct {p0, v1, v7}, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->appendStatusList(Lcom/google/common/io/protocol/ProtoBuf;Ljava/util/List;)V

    invoke-virtual {v4, v9, v1}, Lcom/google/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    :cond_5
    const/16 v7, 0x9

    invoke-virtual {v4, v7, v3}, Lcom/google/common/io/protocol/ProtoBuf;->setBool(IZ)V

    :cond_6
    const/4 v7, 0x2

    invoke-virtual {v2, v7, v4}, Lcom/google/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/common/io/protocol/ProtoBuf;)V

    goto :goto_0
.end method

.method public getShow()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mShow:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusListContentsMax()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatusListContentsMax:I

    return v0
.end method

.method public getStatusListMax()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatusListMax:I

    return v0
.end method

.method public getStatusMax()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatusMax:I

    return v0
.end method

.method public getStatusMinVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatusMinVersion:I

    return v0
.end method

.method public hasStatusMinVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mHasStatusMinVersion:Z

    return v0
.end method

.method public isInvisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mInvisible:Z

    return v0
.end method

.method public setDefaultStatusList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDefaultStatusList:Ljava/util/List;

    return-void
.end method

.method public setDndStatusList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mDndStatusList:Ljava/util/List;

    return-void
.end method

.method setHasStatusMinVersion(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mHasStatusMinVersion:Z

    return-void
.end method

.method public setInvisible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mInvisible:Z

    return-void
.end method

.method public setShow(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mShow:Ljava/lang/String;

    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatus:Ljava/lang/String;

    return-void
.end method

.method public setStatusListContentsMax(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatusListContentsMax:I

    return-void
.end method

.method public setStatusListMax(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatusListMax:I

    return-void
.end method

.method public setStatusMax(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatusMax:I

    return-void
.end method

.method public setStatusMinVersion(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mStatusMinVersion:I

    return-void
.end method

.method setUseServerAttributes(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/SharedStatus;->mUseServerAttributes:Z

    return-void
.end method
