.class public Lcom/google/android/gsf/gtalkservice/LogTag;
.super Ljava/lang/Object;
.source "LogTag.java"


# static fields
.field public static sDebug:Z

.field public static sDebugConnection:Z

.field public static sDebugPresence:Z

.field public static sDebugRmq:Z

.field public static sShowDebugLogs:Z

.field public static sVerbose:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebug:Z

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sVerbose:Z

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugPresence:Z

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugConnection:Z

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sShowDebugLogs:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static enableDebugLogs(Z)V
    .locals 3
    .param p0    # Z

    const-string v0, "GTalkService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "### enable debug logs : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean p0, Lcom/google/android/gsf/gtalkservice/LogTag;->sShowDebugLogs:Z

    return-void
.end method

.method private static getMessageType(Lcom/google/common/io/protocol/ProtoBufType;Z)I
    .locals 2
    .param p0    # Lcom/google/common/io/protocol/ProtoBufType;
    .param p1    # Z

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->HEARTBEAT_PING:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_2

    const/4 v0, 0x7

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    mul-int/lit8 v0, v0, -0x1

    :cond_1
    return v0

    :cond_2
    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->HEARTBEAT_ACK:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_3

    const/16 v0, 0x8

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->LOGIN_RESPONSE:Lcom/google/common/io/protocol/ProtoBufType;

    if-eq p0, v1, :cond_4

    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->LOGIN_REQUEST:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_5

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->MESSAGE_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_6

    const/4 v0, 0x2

    goto :goto_0

    :cond_6
    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->PRESENCE_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-eq p0, v1, :cond_7

    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->BATCH_PRESENCE_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_8

    :cond_7
    const/4 v0, 0x5

    goto :goto_0

    :cond_8
    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->IQ_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_9

    const/4 v0, 0x4

    goto :goto_0

    :cond_9
    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->DATA_MESSAGE_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_a

    const/4 v0, 0x3

    goto :goto_0

    :cond_a
    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->CLOSE:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_b

    const/16 v0, 0x9

    goto :goto_0

    :cond_b
    sget-object v1, Lcom/google/android/gsf/gtalkservice/proto/GtalkCoreMessageTypes;->STREAM_ERROR_STANZA:Lcom/google/common/io/protocol/ProtoBufType;

    if-ne p0, v1, :cond_0

    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static getShowDebugLogs()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sShowDebugLogs:Z

    return v0
.end method

.method public static logConnectionClosed(III)V
    .locals 5
    .param p0    # I
    .param p1    # I
    .param p2    # I

    shl-int/lit8 v1, p1, 0x8

    add-int v0, v1, p0

    const v1, 0x31ce3

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    return-void
.end method

.method public static logConnectionEvent(IIII)V
    .locals 3
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    shl-int/lit8 v1, p0, 0x18

    shl-int/lit8 v2, p1, 0x10

    add-int/2addr v1, v2

    shl-int/lit8 v2, p2, 0x8

    add-int/2addr v1, v2

    add-int v0, v1, p3

    const v1, 0x31ce2

    invoke-static {v1, v0}, Landroid/util/EventLog;->writeEvent(II)I

    return-void
.end method

.method public static logEvent(I)V
    .locals 1
    .param p0    # I

    const v0, 0x31ce1

    invoke-static {v0, p0}, Landroid/util/EventLog;->writeEvent(II)I

    return-void
.end method

.method public static logHeartbeatReset(IILjava/lang/String;)V
    .locals 5
    .param p0    # I
    .param p1    # I
    .param p2    # Ljava/lang/String;

    shl-int/lit8 v1, p1, 0x10

    add-int v0, v1, p0

    const v1, 0x31ce4

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    return-void
.end method

.method public static logRmq2(Lcom/google/common/io/protocol/ProtoBufType;Ljava/lang/String;IIZ)V
    .locals 5
    .param p0    # Lcom/google/common/io/protocol/ProtoBufType;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    invoke-static {p0, p4}, Lcom/google/android/gsf/gtalkservice/LogTag;->getMessageType(Lcom/google/common/io/protocol/ProtoBufType;Z)I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const v1, 0x31ce5

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public static queryLoggingLevel()V
    .locals 3

    const/4 v2, 0x3

    const-string v0, "GTalkService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebug:Z

    const-string v0, "GTalkService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sVerbose:Z

    const-string v0, "GTalkService/c"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugConnection:Z

    const-string v0, "GTalkService/p"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugPresence:Z

    const-string v0, "Rmq"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gsf/gtalkservice/LogTag;->sDebugRmq:Z

    return-void
.end method
