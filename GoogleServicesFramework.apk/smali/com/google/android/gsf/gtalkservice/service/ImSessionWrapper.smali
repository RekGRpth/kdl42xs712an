.class public Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;
.super Lcom/google/android/gtalkservice/IImSession$Stub;
.source "ImSessionWrapper.java"


# instance fields
.field mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-direct {p0}, Lcom/google/android/gtalkservice/IImSession$Stub;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    return-void
.end method


# virtual methods
.method public addConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IConnectionStateListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->addConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V

    return-void
.end method

.method public addContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->addContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public addGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IGroupChatInvitationListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->addGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V

    return-void
.end method

.method public addRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IChatListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->addRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V

    return-void
.end method

.method public addRemoteJingleInfoStanzaListener(Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->addRemoteJingleInfoStanzaListener(Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;)V

    return-void
.end method

.method public addRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IRosterListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->addRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V

    return-void
.end method

.method public addRemoteSessionStanzaListener(Lcom/google/android/gtalkservice/ISessionStanzaListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/ISessionStanzaListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->addRemoteSessionStanzaListener(Lcom/google/android/gtalkservice/ISessionStanzaListener;)V

    return-void
.end method

.method public approveSubscriptionRequest(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->approveSubscriptionRequest(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public blockContact(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->blockContact(Ljava/lang/String;)V

    return-void
.end method

.method public clearContactFlags(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->clearContactFlags(Ljava/lang/String;)V

    return-void
.end method

.method public closeAllChatSessions()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->closeAllChatSessions()V

    return-void
.end method

.method public createChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->createChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v0

    return-object v0
.end method

.method public createGroupChatSession(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->createGroupChatSession(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public declineGroupChatInvitation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->declineGroupChatInvitation(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public declineSubscriptionRequest(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->declineSubscriptionRequest(Ljava/lang/String;)V

    return-void
.end method

.method public editContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->editContact(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public getAccountId()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getAccountId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getChatSession(Ljava/lang/String;)Lcom/google/android/gtalkservice/IChatSession;

    move-result-object v0

    return-object v0
.end method

.method public getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getConnectionState()Lcom/google/android/gtalkservice/ConnectionState;

    move-result-object v0

    return-object v0
.end method

.method public getJid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getHostConnection()Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/GTalkConnection;->getJid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPresence()Lcom/google/android/gtalkservice/Presence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getPresence()Lcom/google/android/gtalkservice/Presence;

    move-result-object v0

    return-object v0
.end method

.method public getSessionImpl()Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->getUsername()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public goOffRecordInRoom(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->goOffRecordInRoom(Ljava/lang/String;Z)V

    return-void
.end method

.method public goOffRecordWithContacts(Ljava/util/List;Z)V
    .locals 1
    .param p1    # Ljava/util/List;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->goOffRecordWithContact(Ljava/util/ArrayList;Z)V

    return-void
.end method

.method public hideContact(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->hideContact(Ljava/lang/String;)V

    return-void
.end method

.method public inviteContactsToGroupchat(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->inviteContactsToGroupchat(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public isOffRecordWithContact(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->isOffRecordWithContact(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public joinGroupChatSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->joinGroupChatSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public login(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->login(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-void

    :catch_0
    move-exception v0

    const-string v3, "GTalkService"

    const-string v4, "[ImSessionWrapper] login: caught "

    invoke-static {v3, v4, v0}, Lcom/google/android/gsf/gtalkservice/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public logout()V
    .locals 3

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v2}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->logout()V

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-void
.end method

.method public pinContact(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->pinContact(Ljava/lang/String;)V

    return-void
.end method

.method public pruneOldChatSessions(JJJZ)V
    .locals 8
    .param p1    # J
    .param p3    # J
    .param p5    # J
    .param p7    # Z

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->pruneOldChatSessions(JJJZ)V

    return-void
.end method

.method public queryJingleInfo()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->queryJingleInfo()V

    return-void
.end method

.method public removeConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IConnectionStateListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->removeConnectionStateListener(Lcom/google/android/gtalkservice/IConnectionStateListener;)V

    return-void
.end method

.method public removeContact(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->removeContact(Ljava/lang/String;)V

    return-void
.end method

.method public removeGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IGroupChatInvitationListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->removeGroupChatInvitationListener(Lcom/google/android/gtalkservice/IGroupChatInvitationListener;)V

    return-void
.end method

.method public removeRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IChatListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->removeRemoteChatListener(Lcom/google/android/gtalkservice/IChatListener;)V

    return-void
.end method

.method public removeRemoteJingleInfoStanzaListener(Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->removeRemoteJingleInfoStanzaListener(Lcom/google/android/gtalkservice/IJingleInfoStanzaListener;)V

    return-void
.end method

.method public removeRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/IRosterListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->removeRemoteRosterListener(Lcom/google/android/gtalkservice/IRosterListener;)V

    return-void
.end method

.method public removeRemoteSessionStanzaListener(Lcom/google/android/gtalkservice/ISessionStanzaListener;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/ISessionStanzaListener;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->removeRemoteSessionStanzaListener(Lcom/google/android/gtalkservice/ISessionStanzaListener;)V

    return-void
.end method

.method public requestBatchedBuddyPresence()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->requestBatchedBuddyPresence()V

    return-void
.end method

.method public sendCallPerfStatsStanza(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->sendCallPerfStatsStanza(Ljava/lang/String;)V

    return-void
.end method

.method public sendSessionStanza(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->sendSessionStanza(Ljava/lang/String;)V

    return-void
.end method

.method public setPresence(Lcom/google/android/gtalkservice/Presence;)V
    .locals 1
    .param p1    # Lcom/google/android/gtalkservice/Presence;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->setAndSendPresence(Lcom/google/android/gtalkservice/Presence;)V

    return-void
.end method

.method public uploadAvatar(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->uploadAvatar(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public uploadAvatarFromDb()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/service/ImSessionWrapper;->mSession:Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/gtalk/ImSession;->uploadAvatarFromDb()V

    return-void
.end method
