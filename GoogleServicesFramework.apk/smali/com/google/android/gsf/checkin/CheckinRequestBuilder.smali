.class public Lcom/google/android/gsf/checkin/CheckinRequestBuilder;
.super Ljava/lang/Object;
.source "CheckinRequestBuilder.java"


# static fields
.field private static final DEFAULT_KEYSTORE:Ljava/io/File;

.field private static DEVICE_ID_PATTERN:Ljava/util/regex/Pattern;

.field private static ESN_PATTERN:Ljava/util/regex/Pattern;

.field private static MAC_ADDR_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "^([0-9a-fA-F][0-9a-fA-F][:-]?){5}[0-9a-fA-F][0-9a-fA-F]$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->MAC_ADDR_PATTERN:Ljava/util/regex/Pattern;

    new-instance v0, Ljava/io/File;

    const-string v1, "/system/etc/security/otacerts.zip"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->DEFAULT_KEYSTORE:Ljava/io/File;

    const-string v0, "^(([0-9]{15})|([0-9a-fA-F]{14}))$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->DEVICE_ID_PATTERN:Ljava/util/regex/Pattern;

    const-string v0, "^([0-9a-fA-F]{8})$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->ESN_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addAccountInfo(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v7, "com.google"

    invoke-virtual {v2, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    move-object v3, v1

    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v3, v4

    const/4 v6, 0x0

    :try_start_0
    const-string v7, "ac2dm"

    const/4 v8, 0x1

    invoke-virtual {v2, v0, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v6

    :goto_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    :try_start_1
    const-string v7, "SID"

    const/4 v8, 0x1

    invoke-virtual {v2, v0, v7, v8}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    :cond_0
    :goto_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addAccountCookie(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    if-eqz v6, :cond_1

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {p1, v6}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addAccountCookie(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    array-length v7, v1

    if-nez v7, :cond_3

    const-string v7, ""

    invoke-virtual {p1, v7}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addAccountCookie(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_3
    return-void

    :catch_0
    move-exception v7

    goto :goto_2

    :catch_1
    move-exception v7

    goto :goto_2

    :catch_2
    move-exception v7

    goto :goto_2

    :catch_3
    move-exception v7

    goto :goto_1

    :catch_4
    move-exception v7

    goto :goto_1

    :catch_5
    move-exception v7

    goto :goto_1
.end method

.method public static addBuildProperties(Landroid/content/SharedPreferences;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 7
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    invoke-virtual {p1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->getCheckin()Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->getBuild()Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    move-result-object v0

    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setId(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_0
    invoke-static {}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->getRadioVersion()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x0

    :cond_1
    const-string v3, "CheckinTask_lastRadio"

    const/4 v4, 0x0

    invoke-interface {p0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v1, :cond_b

    move-object v1, v2

    :cond_2
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setRadio(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_3
    sget-object v3, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    sget-object v3, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setBootloader(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_4
    sget-object v3, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    sget-object v3, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setProduct(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_5
    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setCarrier(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_6
    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setDevice(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_7
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setModel(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_8
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setManufacturer(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_9
    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setBuildProduct(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_a
    sget-wide v3, Landroid/os/Build;->TIME:J

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setTimestamp(J)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setSdkVersion(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    new-instance v3, Ljava/io/File;

    const-string v4, "/system/recovery-from-boot.p"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setOtaInstalled(Z)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    invoke-static {p1}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addOtaCerts(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V

    return-void

    :cond_b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "CheckinTask_lastRadio"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0
.end method

.method public static addDeviceConfiguration(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->getDeviceConfiguration()Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    invoke-direct {v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setDeviceConfiguration(Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->getDeviceConfiguration()Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    move-result-object v4

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->touchScreenFromConfig(Landroid/content/pm/ConfigurationInfo;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setTouchScreen(I)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    invoke-static {v3}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->keyboardFromConfig(Landroid/content/pm/ConfigurationInfo;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setKeyboard(I)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    invoke-static {v3}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->navigationFromConfig(Landroid/content/pm/ConfigurationInfo;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setNavigation(I)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->screenLayoutFromConfig(Landroid/content/res/Configuration;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setScreenLayout(I)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    iget v0, v3, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setHasHardKeyboard(Z)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    iget v0, v3, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setHasFiveWayNavigation(Z)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setScreenDensity(I)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setScreenWidth(I)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setScreenHeight(I)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    iget v0, v3, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->setGlEsVersion(I)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getSystemSharedLibraryNames()[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    array-length v5, v3

    move v0, v2

    :goto_2
    if-ge v0, v5, :cond_3

    aget-object v6, v3, v0

    invoke-virtual {v4, v6}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->addSystemSharedLibrary(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v5

    if-eqz v5, :cond_5

    array-length v0, v5

    new-array v6, v0, [Ljava/lang/String;

    array-length v7, v5

    move v3, v2

    move v1, v2

    :goto_3
    if-ge v3, v7, :cond_4

    aget-object v8, v5, v3

    iget-object v0, v8, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    add-int/lit8 v0, v1, 0x1

    iget-object v8, v8, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    aput-object v8, v6, v1

    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_3

    :cond_4
    invoke-static {v6, v2, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;II)V

    move v0, v2

    :goto_5
    if-ge v0, v1, :cond_5

    aget-object v3, v6, v0

    invoke-virtual {v4, v3}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->addSystemAvailableFeature(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->addNativePlatform(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    sget-object v0, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->addNativePlatform(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    array-length v1, v0

    :goto_6
    if-ge v2, v1, :cond_8

    aget-object v3, v0, v2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v4, v3}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->addSystemSupportedLocale(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_8
    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->getGlExtensions(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;->addGlExtension(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Config$DeviceConfigurationProto;

    goto :goto_7

    :cond_9
    return-void

    :cond_a
    move v0, v1

    goto :goto_4
.end method

.method public static addEvents(Landroid/os/DropBoxManager;IILjava/util/Map;JLcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)J
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/DropBoxManager;",
            "II",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;",
            ")J"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->getCheckin()Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    move-result-object v13

    move-wide/from16 v0, p4

    invoke-virtual {v13, v0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->setLastCheckinMsec(J)Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    const-string v3, "checkin_dropbox_upload"

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_6

    sget-object v7, Lcom/google/android/gsf/Gservices;->TRUE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x1

    move v12, v3

    :goto_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    :try_start_1
    const-string v3, "event_log"

    move-object/from16 v0, p0

    move-wide/from16 v1, p4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v5

    :try_start_2
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    const-wide/16 v9, -0x1

    const/16 v3, 0x1000

    new-array v15, v3, [B

    new-instance v16, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    const/4 v3, 0x0

    move v11, v3

    move-object v7, v6

    move-object v3, v8

    move-object/from16 v19, v4

    move-object v4, v5

    move-object/from16 v5, v19

    move-wide/from16 v20, v9

    move-wide/from16 v8, v20

    :goto_1
    move/from16 v0, p1

    if-ge v11, v0, :cond_1c

    if-nez v4, :cond_0

    if-eqz v7, :cond_1c

    :cond_0
    move-object v6, v4

    move-wide/from16 v19, v8

    move-wide/from16 v9, v19

    move-object v8, v3

    :cond_1
    :goto_2
    if-eqz v6, :cond_a

    :try_start_3
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    cmp-long v3, v9, p4

    if-gtz v3, :cond_a

    :cond_2
    if-nez v8, :cond_3

    invoke-virtual {v6}, Landroid/os/DropBoxManager$Entry;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    if-eqz v4, :cond_3

    new-instance v3, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-direct {v8, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v4, 0x1000

    invoke-direct {v3, v8, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v8, v3

    :cond_3
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    if-eqz v8, :cond_4

    invoke-static {v8, v14}, Lcom/google/android/common/Csv;->parseLine(Ljava/io/BufferedReader;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_7

    :cond_4
    invoke-virtual {v6}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v3

    if-eqz v8, :cond_5

    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V

    :cond_5
    const/4 v8, 0x0

    invoke-virtual {v6}, Landroid/os/DropBoxManager$Entry;->close()V

    const-string v17, "event_log"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;

    move-result-object v6

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    move v12, v3

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x0

    :try_start_4
    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/16 v4, 0x2e

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/4 v9, -0x1

    if-ne v4, v9, :cond_8

    :goto_3
    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    move-wide v9, v3

    goto :goto_2

    :cond_8
    const/4 v9, 0x0

    invoke-virtual {v3, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v3

    goto :goto_3

    :catch_0
    move-exception v3

    move-object v4, v3

    :try_start_5
    const-string v9, "CheckinRequestBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Can\'t parse event_log timestamp: "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-wide/16 v3, -0x1

    move-wide v9, v3

    goto/16 :goto_2

    :cond_9
    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v3

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->close()V

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;

    move-result-object v7

    :cond_a
    if-eqz v7, :cond_c

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "event_log"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getFlags()I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_9

    :cond_b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "checkin_dropbox_upload:"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_f

    sget-object v4, Lcom/google/android/gsf/Gservices;->TRUE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_f

    :cond_c
    :goto_4
    if-eqz v6, :cond_14

    if-eqz v7, :cond_d

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v3

    cmp-long v3, v9, v3

    if-gez v3, :cond_14

    :cond_d
    new-instance v4, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;

    invoke-direct {v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;-><init>()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v13, v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->addEvent(Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    const/4 v3, 0x3

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->setTag(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;

    invoke-virtual {v4, v9, v10}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->setTimeMsec(J)Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x5

    if-ne v3, v5, :cond_10

    const/4 v3, 0x4

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->setValue(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;

    :cond_e
    :goto_5
    invoke-virtual {v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->getSerializedSize()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v11

    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v11, v3

    move-object v5, v4

    move-object v3, v8

    move-object v4, v6

    move-wide/from16 v19, v9

    move-wide/from16 v8, v19

    goto/16 :goto_1

    :cond_f
    if-eqz v12, :cond_9

    if-eqz v3, :cond_c

    :try_start_7
    sget-object v4, Lcom/google/android/gsf/Gservices;->FALSE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v3

    if-nez v3, :cond_9

    goto :goto_4

    :cond_10
    :try_start_8
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x5

    if-le v3, v5, :cond_e

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x4

    invoke-virtual {v14, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/google/android/common/Csv;->writeValue(Ljava/lang/String;Ljava/lang/Appendable;)V

    const/4 v3, 0x5

    move v5, v3

    :goto_6
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v5, v3, :cond_11

    const-string v3, ","

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/google/android/common/Csv;->writeValue(Ljava/lang/String;Ljava/lang/Appendable;)V

    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_6

    :cond_11
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->setValue(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_5

    :catch_1
    move-exception v3

    move-object v5, v6

    move-object v6, v7

    :goto_7
    :try_start_9
    const-string v7, "CheckinRequestBuilder"

    const-string v8, "Can\'t copy dropbox data"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    if-eqz v6, :cond_12

    invoke-virtual {v6}, Landroid/os/DropBoxManager$Entry;->close()V

    :cond_12
    if-eqz v5, :cond_20

    invoke-virtual {v5}, Landroid/os/DropBoxManager$Entry;->close()V

    move-object v5, v4

    :cond_13
    :goto_8
    if-nez v5, :cond_1f

    :goto_9
    return-wide p4

    :cond_14
    if-eqz v7, :cond_21

    if-eqz v6, :cond_15

    :try_start_a
    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v3

    cmp-long v3, v3, v9

    if-gtz v3, :cond_21

    :cond_15
    new-instance v4, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;

    invoke-direct {v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;-><init>()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    invoke-virtual {v13, v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->addEvent(Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->setTag(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v4, v0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->setTimeMsec(J)Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;

    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    :cond_16
    if-eqz v3, :cond_17

    invoke-virtual {v3, v15}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-lez v5, :cond_17

    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v17

    sub-int v17, p2, v17

    move/from16 v0, v17

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v17

    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v15, v1, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    move/from16 v0, v17

    if-ge v0, v5, :cond_16

    const-string v5, "CheckinRequestBuilder"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Truncating "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getTag()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " entry to "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " bytes for upload"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getFlags()I

    move-result v5

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_17

    const-string v5, "\n=== TRUNCATED FOR UPLOAD ===\n"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :cond_17
    if-eqz v3, :cond_18

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_18
    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result-object v3

    :try_start_c
    array-length v5, v3

    if-lez v5, :cond_19

    new-instance v5, Ljava/lang/String;

    const-string v17, "ISO-8859-1"

    move-object/from16 v0, v17

    invoke-direct {v5, v3, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->setValue(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;
    :try_end_c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_19
    :try_start_d
    invoke-virtual {v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->getSerializedSize()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v11

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v17

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->close()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v17

    invoke-virtual {v0, v5, v1, v2}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;

    move-result-object v7

    move v11, v3

    move-object v5, v4

    move-object v3, v8

    move-object v4, v6

    move-wide/from16 v19, v9

    move-wide/from16 v8, v19

    goto/16 :goto_1

    :catch_2
    move-exception v3

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v5, "ISO-8859-1 not supported?"

    invoke-direct {v3, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catchall_0
    move-exception v3

    :goto_a
    if-eqz v7, :cond_1a

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->close()V

    :cond_1a
    if-eqz v6, :cond_1b

    invoke-virtual {v6}, Landroid/os/DropBoxManager$Entry;->close()V

    :cond_1b
    throw v3

    :cond_1c
    if-eqz v3, :cond_1d

    :try_start_e
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :cond_1d
    if-eqz v7, :cond_1e

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->close()V

    :cond_1e
    if-eqz v4, :cond_13

    invoke-virtual {v4}, Landroid/os/DropBoxManager$Entry;->close()V

    goto/16 :goto_8

    :cond_1f
    invoke-virtual {v5}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidEventProto;->getTimeMsec()J

    move-result-wide p4

    goto/16 :goto_9

    :catchall_1
    move-exception v3

    move-object v7, v6

    move-object v6, v5

    goto :goto_a

    :catchall_2
    move-exception v3

    move-object v7, v6

    move-object v6, v5

    goto :goto_a

    :catchall_3
    move-exception v3

    move-object v7, v6

    move-object v6, v5

    goto :goto_a

    :catchall_4
    move-exception v3

    move-object v6, v4

    goto :goto_a

    :catch_3
    move-exception v3

    goto/16 :goto_7

    :catch_4
    move-exception v3

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    goto/16 :goto_7

    :catch_5
    move-exception v3

    move-object v6, v7

    move-object/from16 v19, v4

    move-object v4, v5

    move-object/from16 v5, v19

    goto/16 :goto_7

    :cond_20
    move-object v5, v4

    goto/16 :goto_8

    :cond_21
    move-object v3, v8

    move-object v4, v6

    move-wide/from16 v19, v9

    move-wide/from16 v8, v19

    goto/16 :goto_1
.end method

.method private static addExtensionsForConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I[ILjava/util/Set;)V
    .locals 10
    .param p0    # Ljavax/microedition/khronos/egl/EGL10;
    .param p1    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p2    # Ljavax/microedition/khronos/egl/EGLConfig;
    .param p3    # [I
    .param p4    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/microedition/khronos/egl/EGL10;",
            "Ljavax/microedition/khronos/egl/EGLDisplay;",
            "Ljavax/microedition/khronos/egl/EGLConfig;",
            "[I[I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    sget-object v7, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {p0, p1, p2, v7, p4}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    sget-object v7, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v1, v7, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p0, p1, p2, p3}, Ljavax/microedition/khronos/egl/EGL10;->eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v6

    sget-object v7, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v6, v7, :cond_1

    invoke-interface {p0, p1, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    goto :goto_0

    :cond_1
    invoke-interface {p0, p1, v6, v6, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    const/16 v7, 0x1f03

    invoke-static {v7}, Landroid/opengl/GLES10;->glGetString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, " "

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v2, v0, v4

    invoke-interface {p5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    sget-object v7, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v8, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v9, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {p0, p1, v7, v8, v9}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    invoke-interface {p0, p1, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    invoke-interface {p0, p1, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    goto :goto_0
.end method

.method public static addIdProperties(Landroid/content/Context;JLcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {p0}, Lcom/google/android/gsf/settings/IdUtils;->getAndroidId(Landroid/content/Context;)J

    move-result-wide v0

    invoke-virtual {p3, v0, v1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setId(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    const-wide/16 v10, 0x0

    cmp-long v10, p1, v10

    if-eqz v10, :cond_0

    invoke-virtual {p3, p1, p2}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setSecurityToken(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_0
    const-wide/16 v10, 0x0

    cmp-long v10, p1, v10

    if-nez v10, :cond_1

    const-wide/16 v10, 0x0

    cmp-long v10, v0, v10

    if-nez v10, :cond_2

    :cond_1
    const/4 v10, 0x3

    invoke-virtual {p3, v10}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setVersion(I)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_2
    invoke-static {p0}, Lcom/google/android/gsf/settings/IdUtils;->getLoggingId(Landroid/content/Context;)J

    move-result-wide v3

    invoke-virtual {p3, v3, v4}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setLoggingId(J)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    const-string v10, "digest"

    invoke-static {v6, v10}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    const-string v5, ""

    :cond_3
    invoke-virtual {p3, v5}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setDigest(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    sget-object v10, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    sget-object v10, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    const-string v11, "unknown"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    sget-object v10, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {p3, v10}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setSerialNumber(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_4
    const-string v10, "user"

    invoke-virtual {p0, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/UserManager;

    invoke-virtual {p3}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->getCheckin()Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v9

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v10

    invoke-virtual {v2, v10}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->setUserNumber(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    invoke-virtual {v8, v9}, Landroid/os/UserManager;->getUserSerialNumber(I)I

    move-result v7

    if-ltz v7, :cond_5

    invoke-virtual {p3, v7}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setUserSerialNumber(I)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_5
    return-void
.end method

.method public static addLocaleProperty(Ljava/util/Locale;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 1
    .param p0    # Ljava/util/Locale;
    .param p1    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setLocale(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static addNetworkProperties(Landroid/telephony/TelephonyManager;Landroid/net/wifi/WifiManager;Landroid/net/ConnectivityManager;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 15
    .param p0    # Landroid/telephony/TelephonyManager;
    .param p1    # Landroid/net/wifi/WifiManager;
    .param p2    # Landroid/net/ConnectivityManager;
    .param p3    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->getCheckin()Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    move-result-object v2

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->getCurrentPhoneType(Landroid/telephony/TelephonyManager;)I

    move-result v9

    packed-switch v9, :pswitch_data_0

    const-string v12, "CheckinRequestBuilder"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unknown phone type: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " id="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_1

    invoke-virtual {v2, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->setCellOperator(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    :cond_1
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    invoke-virtual {v2, v10}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->setSimOperator(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    :cond_2
    const/16 v12, 0x9

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    sget-object v12, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->MAC_ADDR_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v12, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/regex/Matcher;->matches()Z

    move-result v12

    if-eqz v12, :cond_3

    const-string v12, ":"

    const-string v13, ""

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "-"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addMacAddr(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    const-string v12, "ethernet"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addMacAddrType(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_3
    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v8

    if-nez v8, :cond_8

    const-string v5, "unknown"

    :goto_1
    if-nez p0, :cond_a

    const-string v6, "unknown"

    :goto_2
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "-"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->setRoaming(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    if-eqz p1, :cond_4

    invoke-virtual/range {p1 .. p1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v11

    if-eqz v11, :cond_4

    invoke-virtual {v11}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    const-string v12, ":"

    const-string v13, ""

    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-virtual {v0, v7}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addMacAddr(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    const-string v12, "wifi"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addMacAddrType(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_4
    return-void

    :pswitch_1
    if-eqz v4, :cond_6

    sget-object v12, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->ESN_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v12, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/regex/Matcher;->matches()Z

    move-result v12

    if-eqz v12, :cond_6

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const-string v13, "80"

    if-ne v12, v13, :cond_5

    const-string v12, "CheckinRequestBuilder"

    const-string v13, "TelephonyManager.getDeviceId() is returning a pseudo-ESN instead of an MEID"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setEsn(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    goto/16 :goto_0

    :cond_6
    :pswitch_2
    if-eqz v4, :cond_0

    sget-object v12, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->DEVICE_ID_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v12, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/regex/Matcher;->matches()Z

    move-result v12

    if-eqz v12, :cond_7

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setMeid(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    goto/16 :goto_0

    :cond_7
    const-string v12, "CheckinRequestBuilder"

    const-string v13, "TelephonyManager.getDeviceId() must return a 15-decimal-digit IMEI, a 14-hex-digit MEID or an 8-hex-digit ESN "

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v8}, Landroid/net/NetworkInfo;->getType()I

    move-result v12

    if-nez v12, :cond_9

    const-string v5, "mobile"

    goto/16 :goto_1

    :cond_9
    const-string v5, "notmobile"

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v12

    if-eqz v12, :cond_b

    const-string v6, "roaming"

    goto/16 :goto_2

    :cond_b
    const-string v6, "notroaming"

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static addOtaCerts(Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 13
    .param p0    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    const/4 v9, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v10, Ljava/util/zip/ZipFile;

    sget-object v11, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->DEFAULT_KEYSTORE:Ljava/io/File;

    invoke-direct {v10, v11}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v10}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v3

    const/16 v11, 0x800

    new-array v8, v11, [B

    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/zip/ZipEntry;

    invoke-virtual {v10, v4}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v5

    const-string v11, "SHA-1"

    invoke-static {v11}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    :goto_1
    invoke-virtual {v5, v8}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-lez v7, :cond_2

    const/4 v11, 0x0

    invoke-virtual {v6, v8, v11, v7}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v11

    move-object v9, v10

    :goto_2
    if-eqz v9, :cond_0

    :try_start_2
    invoke-virtual {v9}, Ljava/util/zip/ZipFile;->close()V

    :cond_0
    throw v11
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_0
    move-exception v2

    :goto_3
    const-string v11, "CheckinRequestBuilder"

    const-string v12, "error reading OTA certs"

    invoke-static {v11, v12, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v11, "--IOException--"

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addOtaCert(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    add-int/lit8 v1, v1, 0x1

    :goto_4
    if-nez v1, :cond_1

    const-string v11, "--no-output--"

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addOtaCert(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_1
    return-void

    :cond_2
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v11

    const/4 v12, 0x2

    invoke-static {v11, v12}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addOtaCert(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    if-eqz v10, :cond_4

    :try_start_4
    invoke-virtual {v10}, Ljava/util/zip/ZipFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    move-object v9, v10

    goto :goto_4

    :catch_1
    move-exception v2

    move-object v9, v10

    :goto_5
    const-string v11, "CheckinRequestBuilder"

    const-string v12, "no support for SHA-1?"

    invoke-static {v11, v12, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v11, "--NoSuchAlgorithmException--"

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->addOtaCert(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :catch_2
    move-exception v2

    goto :goto_5

    :catch_3
    move-exception v2

    move-object v9, v10

    goto :goto_3

    :catchall_1
    move-exception v11

    goto :goto_2
.end method

.method public static addPackageProperties(Landroid/content/Context;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    invoke-virtual {p1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->getCheckin()Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->getBuild()Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "client_id"

    invoke-static {v4, v5}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setClient(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v0, v4}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setGoogleServices(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v4, "CheckinRequestBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Our own package not found: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static addTimeZone(Ljava/util/TimeZone;Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;)V
    .locals 1
    .param p0    # Ljava/util/TimeZone;
    .param p1    # Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setTimeZone(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    :cond_0
    return-void
.end method

.method private static getCurrentPhoneType(Landroid/telephony/TelephonyManager;)I
    .locals 5
    .param p0    # Landroid/telephony/TelephonyManager;

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getCurrentPhoneType"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    goto :goto_0
.end method

.method public static getGlExtensions(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinService;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v5, "CheckinService_cachedGlExt"

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gsf/checkin/CheckinService;->wasSystemUpgraded(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_0
    return-object v2

    :cond_0
    invoke-static {}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->getGlExtensionsFromDriver()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const-string v5, " "

    invoke-static {v5, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "CheckinService_cachedGlExt"

    invoke-interface {v5, v6, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private static getGlExtensionsFromDriver()Ljava/util/Set;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    if-nez v0, :cond_0

    const-string v2, "CheckinRequestBuilder"

    const-string v4, "Couldn\'t get EGL"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v5

    :cond_0
    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    const/4 v2, 0x2

    new-array v14, v2, [I

    invoke-interface {v0, v1, v14}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    const/4 v2, 0x1

    new-array v13, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v4, v13}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigs(Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "CheckinRequestBuilder"

    const-string v4, "Couldn\'t get EGL config count"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    aget v2, v13, v2

    new-array v10, v2, [Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v2, 0x0

    aget v2, v13, v2

    invoke-interface {v0, v1, v10, v2, v13}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigs(Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "CheckinRequestBuilder"

    const-string v4, "Couldn\'t get EGL configs"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v2, 0x5

    new-array v3, v2, [I

    fill-array-data v3, :array_0

    const/16 v6, 0x3098

    const/4 v2, 0x3

    new-array v11, v2, [I

    fill-array-data v11, :array_1

    const/4 v2, 0x1

    new-array v9, v2, [I

    const/4 v12, 0x0

    :goto_1
    const/4 v2, 0x0

    aget v2, v13, v2

    if-ge v12, v2, :cond_6

    aget-object v2, v10, v12

    const/16 v4, 0x3027

    invoke-interface {v0, v1, v2, v4, v9}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    const/4 v2, 0x0

    aget v2, v9, v2

    const/16 v4, 0x3050

    if-ne v2, v4, :cond_4

    :cond_3
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_4
    aget-object v2, v10, v12

    const/16 v4, 0x3033

    invoke-interface {v0, v1, v2, v4, v9}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    const/4 v2, 0x0

    aget v2, v9, v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    const/4 v8, 0x1

    const/4 v7, 0x4

    aget-object v2, v10, v12

    const/16 v4, 0x3040

    invoke-interface {v0, v1, v2, v4, v9}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    const/4 v2, 0x0

    aget v2, v9, v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    aget-object v2, v10, v12

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addExtensionsForConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I[ILjava/util/Set;)V

    :cond_5
    const/4 v2, 0x0

    aget v2, v9, v2

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_3

    aget-object v2, v10, v12

    move-object v4, v11

    invoke-static/range {v0 .. v5}, Lcom/google/android/gsf/checkin/CheckinRequestBuilder;->addExtensionsForConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I[ILjava/util/Set;)V

    goto :goto_2

    :cond_6
    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        0x3057
        0x1
        0x3056
        0x1
        0x3038
    .end array-data

    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method private static getRadioVersion()Ljava/lang/String;
    .locals 5

    :try_start_0
    const-string v3, "android.os.Build"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v3, "getRadioVersion"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v3

    :catch_0
    move-exception v2

    sget-object v3, Landroid/os/Build;->RADIO:Ljava/lang/String;

    goto :goto_0
.end method

.method public static invalidateAuthTokens(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v7, "com.google"

    invoke-virtual {v2, v7}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    move-object v3, v1

    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v3, v4

    const-string v7, "ac2dm"

    invoke-virtual {v2, v0, v7}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "com.google"

    invoke-virtual {v2, v7, v6}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v7, "SID"

    invoke-virtual {v2, v0, v7}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "com.google"

    invoke-virtual {v2, v7, v6}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static keyboardFromConfig(Landroid/content/pm/ConfigurationInfo;)I
    .locals 1
    .param p0    # Landroid/content/pm/ConfigurationInfo;

    iget v0, p0, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static navigationFromConfig(Landroid/content/pm/ConfigurationInfo;)I
    .locals 1
    .param p0    # Landroid/content/pm/ConfigurationInfo;

    iget v0, p0, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static newRequest(I)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;
    .locals 3
    .param p0    # I

    new-instance v1, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    invoke-direct {v1}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;-><init>()V

    new-instance v0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    invoke-direct {v0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;-><init>()V

    new-instance v2, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    invoke-direct {v2}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;->setBuild(Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setCheckin(Lcom/google/android/gsf/checkin/proto/Logs$AndroidCheckinProto;)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    invoke-virtual {v1, p0}, Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;->setFragment(I)Lcom/google/android/gsf/checkin/proto/Checkin$AndroidCheckinRequest;

    return-object v1
.end method

.method private static screenLayoutFromConfig(Landroid/content/res/Configuration;)I
    .locals 2
    .param p0    # Landroid/content/res/Configuration;

    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v1, 0xf

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static touchScreenFromConfig(Landroid/content/pm/ConfigurationInfo;)I
    .locals 1
    .param p0    # Landroid/content/pm/ConfigurationInfo;

    iget v0, p0, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
