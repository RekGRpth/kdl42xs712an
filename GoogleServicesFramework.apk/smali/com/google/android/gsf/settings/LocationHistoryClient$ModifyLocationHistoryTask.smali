.class Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;
.super Landroid/os/AsyncTask;
.source "LocationHistoryClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/settings/LocationHistoryClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ModifyLocationHistoryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<[",
        "Landroid/accounts/Account;",
        "Ljava/lang/Integer;",
        "[I>;"
    }
.end annotation


# instance fields
.field mAccounts:[Landroid/accounts/Account;

.field mCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

.field mStates:[I

.field final synthetic this$0:Lcom/google/android/gsf/settings/LocationHistoryClient;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/settings/LocationHistoryClient;[ILcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;)V
    .locals 0
    .param p2    # [I
    .param p3    # Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    iput-object p1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->this$0:Lcom/google/android/gsf/settings/LocationHistoryClient;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p3, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    iput-object p2, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mStates:[I

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [[Landroid/accounts/Account;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->doInBackground([[Landroid/accounts/Account;)[I

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([[Landroid/accounts/Account;)[I
    .locals 7
    .param p1    # [[Landroid/accounts/Account;

    const/4 v2, 0x1

    const/4 v3, 0x0

    aget-object v1, p1, v3

    iput-object v1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mAccounts:[Landroid/accounts/Account;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mAccounts:[Landroid/accounts/Account;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v4, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mStates:[I

    iget-object v5, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->this$0:Lcom/google/android/gsf/settings/LocationHistoryClient;

    iget-object v1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mAccounts:[Landroid/accounts/Account;

    aget-object v6, v1, v0

    iget-object v1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mStates:[I

    aget v1, v1, v0

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_1
    # invokes: Lcom/google/android/gsf/settings/LocationHistoryClient;->putLocationHistoryState(Landroid/accounts/Account;Z)I
    invoke-static {v5, v6, v1}, Lcom/google/android/gsf/settings/LocationHistoryClient;->access$100(Lcom/google/android/gsf/settings/LocationHistoryClient;Landroid/accounts/Account;Z)I

    move-result v1

    aput v1, v4, v0

    iget-object v1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mStates:[I

    aget v1, v1, v0

    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mStates:[I

    return-object v1

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [I

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->onPostExecute([I)V

    return-void
.end method

.method public onPostExecute([I)V
    .locals 1
    .param p1    # [I

    iget-object v0, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->mCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    invoke-interface {v0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;->onResult([I)V

    return-void
.end method
