.class public Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;
.super Ljava/lang/Object;
.source "KonkaEventInfo.java"


# instance fields
.field public _mContentDescriptor:I

.field public _mEndTime:I

.field public _mEventId:I

.field public _mEventName:Ljava/lang/String;

.field public _mIndex:I

.field public _mIsScheduled:Z

.field public _mParentalRating:I

.field public _mProgNo:I

.field public _mServiceType:I

.field public _mStartTime:I

.field public _mWhichDay:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x5

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEndTime:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventId:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mProgNo:I

    iput-boolean v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIndex:I

    iput v3, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mServiceType:I

    iput v2, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mContentDescriptor:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mParentalRating:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEndTime:I

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventId:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mProgNo:I

    iput-boolean v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIndex:I

    iput v3, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mServiceType:I

    iput v2, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mContentDescriptor:I

    iput v1, p0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mParentalRating:I

    return-void
.end method
