.class public Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "CaDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/CaDesk;


# static fields
.field private static caMgrImpl:Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;


# instance fields
.field private caMgr:Lcom/mstar/android/tvapi/dtv/common/CaManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;->caMgrImpl:Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;->caMgr:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/CaManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;->caMgr:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    return-void
.end method

.method public static getCaMgrInstance()Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;->caMgrImpl:Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;->caMgrImpl:Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;->caMgrImpl:Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;

    return-object v0
.end method


# virtual methods
.method public CaChangePin(Ljava/lang/String;Ljava/lang/String;)S
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaChangePin(Ljava/lang/String;Ljava/lang/String;)S

    move-result v0

    return v0
.end method

.method public CaDelDetitleChkNum(SI)Z
    .locals 1
    .param p1    # S
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaDelDetitleChkNum(SI)Z

    move-result v0

    return v0
.end method

.method public CaDelEmail(I)V
    .locals 0
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaDelEmail(I)V

    return-void
.end method

.method public CaGetACList(S)Lcom/mstar/android/tvapi/dtv/vo/CaACListInfo;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetACList(S)Lcom/mstar/android/tvapi/dtv/vo/CaACListInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetCardSN()Lcom/mstar/android/tvapi/dtv/vo/CACardSNInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetCardSN()Lcom/mstar/android/tvapi/dtv/vo/CACardSNInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetDetitleChkNums(S)Lcom/mstar/android/tvapi/dtv/vo/CaDetitleChkNums;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetDetitleChkNums(S)Lcom/mstar/android/tvapi/dtv/vo/CaDetitleChkNums;

    move-result-object v0

    return-object v0
.end method

.method public CaGetDetitleReaded(S)Z
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetDetitleReaded(S)Z

    move-result v0

    return v0
.end method

.method public CaGetEmailContent(I)Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetEmailContent(I)Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetEmailHead(I)Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetEmailHead(I)Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetEmailHeads(SS)Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;
    .locals 1
    .param p1    # S
    .param p2    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetEmailHeads(SS)Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetEmailSpaceInfo()Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetEmailSpaceInfo()Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetEntitleIDs(S)Lcom/mstar/android/tvapi/dtv/vo/CaEntitleIDs;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetEntitleIDs(S)Lcom/mstar/android/tvapi/dtv/vo/CaEntitleIDs;

    move-result-object v0

    return-object v0
.end method

.method public CaGetIPPVProgram(S)Lcom/mstar/android/tvapi/dtv/vo/CaIPPVProgramInfos;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetIPPVProgram(S)Lcom/mstar/android/tvapi/dtv/vo/CaIPPVProgramInfos;

    move-result-object v0

    return-object v0
.end method

.method public CaGetOperatorChildStatus(S)Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetOperatorChildStatus(S)Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;

    move-result-object v0

    return-object v0
.end method

.method public CaGetOperatorIds()Lcom/mstar/android/tvapi/dtv/vo/CaOperatorIds;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetOperatorIds()Lcom/mstar/android/tvapi/dtv/vo/CaOperatorIds;

    move-result-object v0

    return-object v0
.end method

.method public CaGetOperatorInfo(S)Lcom/mstar/android/tvapi/dtv/vo/CaOperatorInfo;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetOperatorInfo(S)Lcom/mstar/android/tvapi/dtv/vo/CaOperatorInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetPlatformID()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetPlatformID()S

    move-result v0

    return v0
.end method

.method public CaGetRating()Lcom/mstar/android/tvapi/dtv/vo/CARatingInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetRating()Lcom/mstar/android/tvapi/dtv/vo/CARatingInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetServiceEntitles(S)Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetServiceEntitles(S)Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;

    move-result-object v0

    return-object v0
.end method

.method public CaGetSlotIDs(S)Lcom/mstar/android/tvapi/dtv/vo/CaSlotIDs;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetSlotIDs(S)Lcom/mstar/android/tvapi/dtv/vo/CaSlotIDs;

    move-result-object v0

    return-object v0
.end method

.method public CaGetSlotInfo(SS)Lcom/mstar/android/tvapi/dtv/vo/CaSlotInfo;
    .locals 1
    .param p1    # S
    .param p2    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetSlotInfo(SS)Lcom/mstar/android/tvapi/dtv/vo/CaSlotInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaGetVer()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetVer()I

    move-result v0

    return v0
.end method

.method public CaGetWorkTime()Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaGetWorkTime()Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaIsPaired(SLjava/lang/String;)S
    .locals 1
    .param p1    # S
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaIsPaired(SLjava/lang/String;)S

    move-result v0

    return v0
.end method

.method public CaOTAStateConfirm(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaOTAStateConfirm(II)Z

    move-result v0

    return v0
.end method

.method public CaReadFeedDataFromParent(S)Lcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaReadFeedDataFromParent(S)Lcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;

    move-result-object v0

    return-object v0
.end method

.method public CaRefreshInterface()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaRefreshInterface()V

    return-void
.end method

.method public CaSetRating(Ljava/lang/String;S)S
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaSetRating(Ljava/lang/String;S)S

    move-result v0

    return v0
.end method

.method public CaSetWorkTime(Ljava/lang/String;Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;)S
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaSetWorkTime(Ljava/lang/String;Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;)S

    move-result v0

    return v0
.end method

.method public CaStopIPPVBuyDlg(Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;)S
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaStopIPPVBuyDlg(Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;)S

    move-result v0

    return v0
.end method

.method public CaWriteFeedDataToChild(SLcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;)S
    .locals 1
    .param p1    # S
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->CaWriteFeedDataToChild(SLcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;)S

    move-result v0

    return v0
.end method

.method public getCurrentEvent()I
    .locals 1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getCurrentEvent()I

    move-result v0

    return v0
.end method

.method public getCurrentMsgType()I
    .locals 1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getCurrentMsgType()I

    move-result v0

    return v0
.end method

.method public setCurrentEvent(I)V
    .locals 0
    .param p1    # I

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->setCurrentEvent(I)V

    return-void
.end method

.method public setCurrentMsgType(I)V
    .locals 0
    .param p1    # I

    invoke-static {p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->setCurrentMsgType(I)V

    return-void
.end method

.method public setOnCaEventListener(Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;->caMgr:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->setOnCaEventListener(Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;)V

    return-void
.end method
