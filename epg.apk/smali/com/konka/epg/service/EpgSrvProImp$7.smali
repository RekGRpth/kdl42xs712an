.class Lcom/konka/epg/service/EpgSrvProImp$7;
.super Ljava/lang/Object;
.source "EpgSrvProImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/service/EpgSrvProImp;->refreshDayEvent(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/service/EpgSrvProImp;

.field private final synthetic val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

.field private final synthetic val$iChannel:I

.field private final synthetic val$iDayId:I


# direct methods
.method constructor <init>(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;II)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    iput-object p2, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iput p3, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$iChannel:I

    iput p4, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$iDayId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/epg/service/EpgSrvProImp$7;)Lcom/konka/epg/service/EpgSrvProImp;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v6, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    iget-object v7, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget v8, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$iChannel:I

    # invokes: Lcom/konka/epg/service/EpgSrvProImp;->getCurProgNum(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)Landroid/content/ContentValues;
    invoke-static {v6, v7, v8}, Lcom/konka/epg/service/EpgSrvProImp;->access$5(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)Landroid/content/ContentValues;

    move-result-object v5

    const-string v6, "sServiceType"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->getAsShort(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Short;->shortValue()S

    move-result v4

    const/4 v3, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz v2, :cond_0

    iget-object v6, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v6}, Lcom/konka/epg/service/EpgSrvProImp;->access$0(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v6

    iget v7, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$iDayId:I

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventListByDay(I)Ljava/util/ArrayList;

    move-result-object v2

    const-string v6, "filte epgInfoList By day"

    invoke-static {v6}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "filte epgInfoList finish,count is ----->"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    iget-object v6, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/konka/epg/service/EpgSrvProImp;->access$7(Lcom/konka/epg/service/EpgSrvProImp;)Ljava/util/ArrayList;

    move-result-object v7

    monitor-enter v7

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v6, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/konka/epg/service/EpgSrvProImp;->access$7(Lcom/konka/epg/service/EpgSrvProImp;)Ljava/util/ArrayList;

    move-result-object v6

    iget v8, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$iDayId:I

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v6}, Lcom/konka/epg/adapter/EventListAdapter;->delAllItem()V

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v3, v6, :cond_3

    :cond_1
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v6, "Type"

    const/16 v7, 0x1002

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v6, "ServiceType"

    invoke-virtual {v0, v6, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v6, "dayId"

    iget v7, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$iDayId:I

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v6, "channelId"

    iget v7, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$iChannel:I

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v6, Lcom/konka/epg/TVRootApp;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/konka/epg/service/EpgSrvProImp$7$1;

    invoke-direct {v7, p0, v0}, Lcom/konka/epg/service/EpgSrvProImp$7$1;-><init>(Lcom/konka/epg/service/EpgSrvProImp$7;Landroid/os/Bundle;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_2
    const-string v6, "filte epgInfoList fail"

    invoke-static {v6}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget-object v6, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/konka/epg/service/EpgSrvProImp;->access$7(Lcom/konka/epg/service/EpgSrvProImp;)Ljava/util/ArrayList;

    move-result-object v6

    iget v8, p0, Lcom/konka/epg/service/EpgSrvProImp$7;->val$iDayId:I

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v6, v1}, Lcom/konka/epg/adapter/EventListAdapter;->addItem(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method
