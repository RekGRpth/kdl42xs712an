.class Lcom/konka/epg/service/EpgSrvProImp$4;
.super Ljava/lang/Object;
.source "EpgSrvProImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/service/EpgSrvProImp;->getEventOfDay(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I


# instance fields
.field final synthetic this$0:Lcom/konka/epg/service/EpgSrvProImp;

.field private final synthetic val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

.field private final synthetic val$iDayId:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I
    .locals 3

    sget-object v0, Lcom/konka/epg/service/EpgSrvProImp$4;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DATA:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_UNITED_TV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/epg/service/EpgSrvProImp$4;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp$4;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    iput-object p2, p0, Lcom/konka/epg/service/EpgSrvProImp$4;->val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iput p3, p0, Lcom/konka/epg/service/EpgSrvProImp$4;->val$iDayId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/epg/service/EpgSrvProImp$4;)Lcom/konka/epg/service/EpgSrvProImp;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp$4;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v1, 0x0

    invoke-static {}, Lcom/konka/epg/service/EpgSrvProImp$4;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I

    move-result-object v2

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp$4;->val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Type"

    const/16 v3, 0x1002

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "ServiceType"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "dayId"

    iget v3, p0, Lcom/konka/epg/service/EpgSrvProImp$4;->val$iDayId:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v2, Lcom/konka/epg/TVRootApp;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/konka/epg/service/EpgSrvProImp$4$1;

    invoke-direct {v3, p0, v0}, Lcom/konka/epg/service/EpgSrvProImp$4$1;-><init>(Lcom/konka/epg/service/EpgSrvProImp$4;Landroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :pswitch_0
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v2

    int-to-short v1, v2

    goto :goto_0

    :pswitch_1
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v2

    int-to-short v1, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
