.class Lcom/konka/epg/service/EpgSrvProImp$5;
.super Ljava/lang/Object;
.source "EpgSrvProImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/service/EpgSrvProImp;->delayProgSel(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/service/EpgSrvProImp;

.field private final synthetic val$iIndex:I


# direct methods
.method constructor <init>(Lcom/konka/epg/service/EpgSrvProImp;I)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp$5;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    iput p2, p0, Lcom/konka/epg/service/EpgSrvProImp$5;->val$iIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    :try_start_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->sleep(J)V

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp$5;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;
    invoke-static {v3}, Lcom/konka/epg/service/EpgSrvProImp;->access$3(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    const/16 v3, 0x1007

    iput v3, v2, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "channelId"

    iget v4, p0, Lcom/konka/epg/service/EpgSrvProImp$5;->val$iIndex:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp$5;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;
    invoke-static {v3}, Lcom/konka/epg/service/EpgSrvProImp;->access$3(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
