.class public Lcom/konka/epg/ui/EpgMainMenuActivity;
.super Landroid/app/Activity;
.source "EpgMainMenuActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;,
        Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;,
        Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;
    }
.end annotation


# static fields
.field public static currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

.field public static iSwitchServiceTimes:I

.field private static isExit:Z


# instance fields
.field private enableDoubleChannel:Z

.field private epgEventInfoPopWin:Lcom/konka/epg/ui/EpgEventInfoPopWin;

.field private epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

.field private epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

.field private m_ExitTask:Ljava/util/TimerTask;

.field private tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->isExit:Z

    sput v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->iSwitchServiceTimes:I

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgEventInfoPopWin:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/epg/ui/EpgMainMenuActivity$1;-><init>(Lcom/konka/epg/ui/EpgMainMenuActivity;)V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->m_ExitTask:Ljava/util/TimerTask;

    return-void
.end method

.method static synthetic access$0(Z)V
    .locals 0

    sput-boolean p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->isExit:Z

    return-void
.end method

.method static synthetic access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/epg/ui/EpgMainMenuActivity;Landroid/os/Message;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgMainMenuActivity;->switchMsgToScreenMode(Landroid/os/Message;)I

    move-result v0

    return v0
.end method

.method static synthetic access$3(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/service/EpgSrvProImp;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/epg/ui/EpgMainMenuActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->close3DandDoubleChannel()V

    return-void
.end method

.method private backKey()V
    .locals 2

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    invoke-virtual {v0}, Lcom/konka/epg/service/EpgSrvProImp;->releaseHandle()V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->onStop()V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->finish()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V

    goto :goto_0
.end method

.method private close3DandDoubleChannel()V
    .locals 5

    const/4 v3, 0x0

    invoke-static {}, Lcom/konka/epg/TVRootApp;->getS3dSkin()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v2

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v2, v4}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v1

    iget v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->enableDoubleChannel:Z

    iget-boolean v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->enableDoubleChannel:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disablePip"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_0
    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disablePop"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    :cond_1
    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disable3dDualView"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvPipPopManager;->disable3dDualView()Z

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tv/TvPipPopManager;->setDualViewOnFlag(Z)Z

    :cond_2
    return-void

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method private getMicStatusFromDB()I
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EpgMainMenuActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "******In db mic status is :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "bMicOn"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "bMicOn"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const-string v1, "EpgMainMenuActivity"

    const-string v2, "Fetch db fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setInputSource2Storage()V
    .locals 6

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->getMicStatusFromDB()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    if-eq v2, v1, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;ZZZ)Z

    const-string v2, "EpgMainMenuActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Force (mic) switch to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    goto :goto_0

    :cond_2
    const-string v2, "EpgMainMenuActivity"

    const-string v3, "Do not need to switch to MM, because it is already here!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method private stopSwitchInput()V
    .locals 6

    const-string v3, "activity"

    invoke-virtual {p0, v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "EpgMainMenuActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onstop status pid = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "com.konka.avenger"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "com.konka.metrolauncher"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "com.konka.tvsettings"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "com.konka.epg"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const-string v3, "EpgMainMenuActivity"

    const-string v4, "onstop status not switchinput"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->setInputSource2Storage()V

    goto :goto_0
.end method

.method private switchMsgToScreenMode(Landroid/os/Message;)I
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "Status"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget v3, p1, Landroid/os/Message;->what:I

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SCREEN_SAVER_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_b

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_INVALID_SERVICE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_1

    const/16 v2, 0x44f

    :cond_0
    :goto_0
    return v2

    :cond_1
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_NO_CI_MODULE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_2

    const/16 v2, 0x450

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CI_PLUS_AUTHENTICATION:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_3

    const/16 v2, 0x451

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_SCRAMBLED_PROGRAM:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_4

    const/16 v2, 0x452

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CH_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_5

    const/16 v2, 0x453

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_PARENTAL_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_6

    const/16 v2, 0x454

    goto :goto_0

    :cond_6
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_AUDIO_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_7

    const/16 v2, 0x455

    goto :goto_0

    :cond_7
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_DATA_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_8

    const/16 v2, 0x456

    goto :goto_0

    :cond_8
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_COMMON_VIDEO:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_9

    const/16 v2, 0x457

    goto :goto_0

    :cond_9
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_UNSUPPORTED_FORMAT:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_a

    const/16 v2, 0x458

    goto :goto_0

    :cond_a
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_INVALID_PMT:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_0

    const/16 v2, 0x459

    goto :goto_0

    :cond_b
    iget v3, p1, Landroid/os/Message;->what:I

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_c

    const/16 v2, 0x44d

    goto :goto_0

    :cond_c
    iget v3, p1, Landroid/os/Message;->what:I

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/16 v2, 0x44c

    goto/16 :goto_0
.end method


# virtual methods
.method public isTvKeyPadAction(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "Konka Smart TV Keypad"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x52
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030002    # com.konka.epg.R.layout.epg_main_menu

    invoke-virtual {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/konka/epg/service/EpgSrvProImp;->getEpgMgrInstance(Landroid/app/Activity;)Lcom/konka/epg/service/EpgSrvProImp;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    invoke-static {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getInstance(Landroid/app/Activity;Lcom/konka/epg/service/EpgServiceProvider;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->initView()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setIsActive(Z)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    const/16 v1, 0x83

    const/16 v2, 0x27d

    const/16 v3, 0x259

    const/16 v4, 0x14b

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/epg/service/EpgSrvProImp;->showTVWidgetSurface(IIII)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    new-instance v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;-><init>(Lcom/konka/epg/ui/EpgMainMenuActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/epg/service/EpgSrvProImp;->setListener(Lcom/konka/epg/service/EpgServiceListener;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v7, 0x0

    const/4 v2, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "keycod=============="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-virtual {p0, p2}, Lcom/konka/epg/ui/EpgMainMenuActivity;->isTvKeyPadAction(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->backKey()V

    :cond_0
    move v1, v2

    :goto_1
    return v1

    :sswitch_0
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-eq v1, v3, :cond_1

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    :cond_1
    move v1, v2

    goto :goto_1

    :sswitch_1
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v3, :cond_2

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    const v3, 0x7f0a0013    # com.konka.epg.R.id.epg_mainmenu_channel_list

    if-ne v1, v3, :cond_2

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v3, :cond_4

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getPosInEventList()I

    move-result v2

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v2, v1, :cond_3

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ListView;->setSelection(I)V

    :cond_3
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1

    :sswitch_2
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v3, :cond_5

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1

    :cond_5
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v3, :cond_6

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIRST_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v3, :cond_7

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V

    :cond_6
    :goto_2
    move v1, v2

    goto :goto_1

    :cond_7
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    const/4 v3, 0x2

    if-le v1, v3, :cond_8

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuDayText()Lcom/konka/epg/ui/EpgMainMenuDayText;

    move-result-object v1

    invoke-static {}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->values()[Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v3

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v4}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v1

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    :goto_3
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1, v7}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setPosInEventList(I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1, v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshEventList(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_8
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIRST_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_3

    :sswitch_3
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v3, :cond_b

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V

    :cond_9
    :goto_4
    move v1, v2

    goto/16 :goto_1

    :cond_a
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuDayText()Lcom/konka/epg/ui/EpgMainMenuDayText;

    move-result-object v1

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1, v3}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v1

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->evListRequestFocus()V

    goto :goto_4

    :cond_b
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v3, :cond_9

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuDayText()Lcom/konka/epg/ui/EpgMainMenuDayText;

    move-result-object v1

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1, v3}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v1

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1, v7}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setPosInEventList(I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1, v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshEventList(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)V

    :try_start_1
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    :sswitch_4
    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->backKey()V

    goto/16 :goto_0

    :sswitch_5
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->startBookMenu()V

    const-string v1, "StartBookMenu"

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    move v1, v2

    goto/16 :goto_1

    :sswitch_6
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->startRecordMenu()V

    const-string v1, "StartRecordMenu"

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    move v1, v2

    goto/16 :goto_1

    :sswitch_7
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v3, :cond_d

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgEventInfoPopWin:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    if-nez v1, :cond_c

    new-instance v1, Lcom/konka/epg/ui/EpgEventInfoPopWin;

    const v3, 0x7f0a000f    # com.konka.epg.R.id.epg_main_menu

    invoke-virtual {p0, v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/konka/epg/ui/EpgEventInfoPopWin;-><init>(Landroid/view/View;Landroid/content/Context;Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/service/EpgServiceProvider;)V

    iput-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgEventInfoPopWin:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    :cond_c
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgEventInfoPopWin:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    const v3, 0x7f030006    # com.konka.epg.R.layout.epg_popwin_eventinfo

    invoke-virtual {v1, v3}, Lcom/konka/epg/ui/EpgEventInfoPopWin;->initEpgPopWin(I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgEventInfoPopWin:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    invoke-virtual {v1, v7, v7}, Lcom/konka/epg/ui/EpgEventInfoPopWin;->showEpgPopWin(II)V

    :cond_d
    move v1, v2

    goto/16 :goto_1

    :sswitch_8
    :try_start_2
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    add-int/lit8 v3, v3, -0x5

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setSelection(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_5
    move v1, v2

    goto/16 :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    :sswitch_9
    :try_start_3
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setSelection(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :goto_6
    move v1, v2

    goto/16 :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    :sswitch_a
    const-string v1, "Volume control"

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_4
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x18 -> :sswitch_a
        0x19 -> :sswitch_a
        0x5c -> :sswitch_8
        0x5d -> :sswitch_9
        0x6f -> :sswitch_4
        0xa4 -> :sswitch_a
        0xa5 -> :sswitch_7
        0xac -> :sswitch_4
        0xb7 -> :sswitch_6
        0xba -> :sswitch_5
        0x100 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const/16 v2, 0x8f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setGpioDeviceStatus(IZ)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v1, "=============>on resume"

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    sget-object v1, Lcom/konka/epg/TVRootApp;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/konka/epg/ui/EpgMainMenuActivity$3;

    invoke-direct {v2, p0}, Lcom/konka/epg/ui/EpgMainMenuActivity$3;-><init>(Lcom/konka/epg/ui/EpgMainMenuActivity;)V

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getIsActive()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "========>>> requestFocus 3"

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V

    :cond_1
    const-string v1, "=============>on resume 222"

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getPosInChannelList()I

    move-result v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v4}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/konka/epg/service/EpgSrvProImp;->refreshDayEvent(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;II)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setIsActive(Z)V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EpgTypeView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f050002    # com.konka.epg.R.color.black

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setIsActiveCreate(Z)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    invoke-virtual {v0}, Lcom/konka/epg/service/EpgSrvProImp;->getHandler()Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    move-result-object v0

    new-instance v1, Lcom/konka/epg/ui/EpgMainMenuActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgMainMenuActivity$2;-><init>(Lcom/konka/epg/ui/EpgMainMenuActivity;)V

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected onStop()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const/16 v2, 0x8f

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setGpioDeviceStatus(IZ)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;

    invoke-virtual {v1}, Lcom/konka/epg/service/EpgSrvProImp;->closeTVWidgetSurface()V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1, v4}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setIsActiveCreate(Z)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1, v4}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setIsActive(Z)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->finish()V

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuActivity;->stopSwitchInput()V

    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
