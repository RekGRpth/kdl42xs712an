.class public final enum Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;
.super Ljava/lang/Enum;
.source "EpgMainMenuActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumFocusPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_FIFTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_FIRST_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_FOURTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_POPUP_WIN:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_SECOND_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_SEVENTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_SIXTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field public static final enum E_FCS_POS_THIRD_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_CHANNEL_LIST"

    invoke-direct {v0, v1, v3}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_EVENT_LIST"

    invoke-direct {v0, v1, v4}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_POPUP_WIN"

    invoke-direct {v0, v1, v5}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_POPUP_WIN:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_NONE_DAY"

    invoke-direct {v0, v1, v6}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_FIRST_DAY"

    invoke-direct {v0, v1, v7}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIRST_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_SECOND_DAY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SECOND_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_THIRD_DAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_THIRD_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_FOURTH_DAY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FOURTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_FIFTH_DAY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIFTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_SIXTH_DAY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SIXTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const-string v1, "E_FCS_POS_SEVENTH_DAY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SEVENTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_POPUP_WIN:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIRST_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SECOND_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_THIRD_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FOURTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIFTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SIXTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SEVENTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ENUM$VALUES:[Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;
    .locals 1

    const-class v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    return-object v0
.end method

.method public static values()[Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ENUM$VALUES:[Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
