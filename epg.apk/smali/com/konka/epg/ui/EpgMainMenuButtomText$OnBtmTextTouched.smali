.class Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;
.super Ljava/lang/Object;
.source "EpgMainMenuButtomText.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuButtomText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnBtmTextTouched"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;


# direct methods
.method private constructor <init>(Lcom/konka/epg/ui/EpgMainMenuButtomText;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/epg/ui/EpgMainMenuButtomText;Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;-><init>(Lcom/konka/epg/ui/EpgMainMenuButtomText;)V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v5

    :pswitch_1
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v1

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1, v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshByService(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuButtomText()Lcom/konka/epg/ui/EpgMainMenuButtomText;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->setVisibility(I)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->startBookMenu()V

    goto :goto_0

    :pswitch_3
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->evListRequestFocus()V

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuDayText()Lcom/konka/epg/ui/EpgMainMenuDayText;

    move-result-object v1

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1, v2}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v1

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v1

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1, v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshEventList(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/konka/epg/ui/EpgEventInfoPopWin;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$1(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$1(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;->this$0:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;
    invoke-static {v4}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->access$2(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/service/EpgServiceProvider;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/konka/epg/ui/EpgEventInfoPopWin;-><init>(Landroid/view/View;Landroid/content/Context;Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/service/EpgServiceProvider;)V

    const v1, 0x7f030006    # com.konka.epg.R.layout.epg_popwin_eventinfo

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgEventInfoPopWin;->initEpgPopWin(I)V

    invoke-virtual {v0, v5, v5}, Lcom/konka/epg/ui/EpgEventInfoPopWin;->showEpgPopWin(II)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a002e
        :pswitch_2    # com.konka.epg.R.id.epg_mainmenu_bookmanager_text
        :pswitch_1    # com.konka.epg.R.id.epg_mainmenu_record_text
        :pswitch_0    # com.konka.epg.R.id.epg_mainmenu_switchlist_left_arrow
        :pswitch_0    # com.konka.epg.R.id.epg_mainmenu_switchlist_right_arrow
        :pswitch_3    # com.konka.epg.R.id.epg_mainmenu_switchlist_text
        :pswitch_0    # com.konka.epg.R.id.epg_mainmenu_bookevent_text
        :pswitch_4    # com.konka.epg.R.id.epg_mainmenu_eventinfo_text
    .end packed-switch
.end method
