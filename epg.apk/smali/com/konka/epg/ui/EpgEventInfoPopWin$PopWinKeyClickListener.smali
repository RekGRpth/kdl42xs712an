.class Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;
.super Ljava/lang/Object;
.source "EpgEventInfoPopWin.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgEventInfoPopWin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopWinKeyClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;


# direct methods
.method private constructor <init>(Lcom/konka/epg/ui/EpgEventInfoPopWin;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;->this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/epg/ui/EpgEventInfoPopWin;Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;-><init>(Lcom/konka/epg/ui/EpgEventInfoPopWin;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;->this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_PopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;->this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_1
    const/16 v0, 0x42

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;->this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;->this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_PopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;->this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :cond_2
    const/16 v0, 0x13

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;->this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EpgEventInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "========>>>keyCode=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;->this$0:Lcom/konka/epg/ui/EpgEventInfoPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    goto :goto_0
.end method
