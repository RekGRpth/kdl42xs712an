.class public Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;
.super Ljava/lang/Object;
.source "EpgMainMenuActivity.java"

# interfaces
.implements Lcom/konka/epg/service/EpgServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EpgListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;


# direct methods
.method public constructor <init>(Lcom/konka/epg/ui/EpgMainMenuActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Landroid/os/Bundle;Ljava/util/ArrayList;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/epg/adapter/EventListAdapter;",
            ">;)V"
        }
    .end annotation

    const-string v3, "Type"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v3

    const-string v4, "ServiceType"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    aget-object v2, v3, v4

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    const-string v4, "dayId"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ne v3, v4, :cond_0

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3, v2}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v4

    const-string v3, "dayId"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ListAdapter;

    invoke-virtual {v4, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    const-string v3, "========>>> requestFocus 2"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_1
    const-string v3, "========>>> requestFocus 1"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v3

    const-string v4, "ServiceType"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    aget-object v2, v3, v4

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getPosInChannelList()I

    move-result v3

    const-string v4, "channelId"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ne v3, v4, :cond_0

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3, v2}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgListener;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    const-string v4, "dayId"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ne v3, v4, :cond_0

    const-string v3, "dayId"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v3}, Lcom/konka/epg/adapter/EventListAdapter;->setPlay()V

    const-string v3, "dayId"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v3}, Lcom/konka/epg/adapter/EventListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1001
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
