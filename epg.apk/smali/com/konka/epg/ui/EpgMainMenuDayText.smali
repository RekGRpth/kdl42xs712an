.class public Lcom/konka/epg/ui/EpgMainMenuDayText;
.super Ljava/lang/Object;
.source "EpgMainMenuDayText.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$epg$ui$EpgMainMenuActivity$EnumFocusPosition:[I


# instance fields
.field private activity:Landroid/app/Activity;

.field private epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

.field private iTodayDayId:I

.field private m_FifthDayImg:Landroid/widget/ImageView;

.field private m_FifthDayText:Landroid/widget/TextView;

.field private m_FirstDayImg:Landroid/widget/ImageView;

.field private m_FirstDayText:Landroid/widget/TextView;

.field private m_FourthDayImg:Landroid/widget/ImageView;

.field private m_FourthDayText:Landroid/widget/TextView;

.field private m_SecondDayImg:Landroid/widget/ImageView;

.field private m_SecondDayText:Landroid/widget/TextView;

.field private m_SeventhDayImg:Landroid/widget/ImageView;

.field private m_SeventhDayText:Landroid/widget/TextView;

.field private m_SixthDayImg:Landroid/widget/ImageView;

.field private m_SixthDayText:Landroid/widget/TextView;

.field private m_ThirdDayImg:Landroid/widget/ImageView;

.field private m_ThirdDayText:Landroid/widget/TextView;

.field private strWeekDayArray:[Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$epg$ui$EpgMainMenuActivity$EnumFocusPosition()[I
    .locals 3

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuDayText;->$SWITCH_TABLE$com$konka$epg$ui$EpgMainMenuActivity$EnumFocusPosition:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->values()[Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIFTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIRST_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FOURTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_POPUP_WIN:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_7
    :try_start_7
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SECOND_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_8
    :try_start_8
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SEVENTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_9
    :try_start_9
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SIXTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_a
    :try_start_a
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_THIRD_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_b
    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuDayText;->$SWITCH_TABLE$com$konka$epg$ui$EpgMainMenuActivity$EnumFocusPosition:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_b

    :catch_1
    move-exception v1

    goto :goto_a

    :catch_2
    move-exception v1

    goto :goto_9

    :catch_3
    move-exception v1

    goto :goto_8

    :catch_4
    move-exception v1

    goto :goto_7

    :catch_5
    move-exception v1

    goto :goto_6

    :catch_6
    move-exception v1

    goto :goto_5

    :catch_7
    move-exception v1

    goto :goto_4

    :catch_8
    move-exception v1

    goto :goto_3

    :catch_9
    move-exception v1

    goto :goto_2

    :catch_a
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/konka/epg/service/EpgServiceProvider;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/konka/epg/service/EpgServiceProvider;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayImg:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayImg:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayImg:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayImg:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayImg:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayImg:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayImg:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->findView()V

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->intiView()V

    return-void
.end method

.method private findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0019    # com.konka.epg.R.id.FIRST_DAY_TEXT

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a001c    # com.konka.epg.R.id.SECOND_DAY_TEXT

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a001f    # com.konka.epg.R.id.THIRD_DAY_TEXT

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0022    # com.konka.epg.R.id.FOURTH_DAY_TEXT

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0025    # com.konka.epg.R.id.FIFTH_DAY_TEXT

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0028    # com.konka.epg.R.id.SIXTH_DAY_TEXT

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a002b    # com.konka.epg.R.id.SEVENTH_DAY_TEXT

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a001a    # com.konka.epg.R.id.FIRST_DAY_IMG

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayImg:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a001d    # com.konka.epg.R.id.SECOND_DAY_IMG

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayImg:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0020    # com.konka.epg.R.id.THIRD_DAY_IMG

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayImg:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0023    # com.konka.epg.R.id.FOURTH_DAY_IMG

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayImg:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0026    # com.konka.epg.R.id.FIFTH_DAY_IMG

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayImg:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0029    # com.konka.epg.R.id.SIXTH_DAY_IMG

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayImg:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a002c    # com.konka.epg.R.id.SEVENTH_DAY_IMG

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayImg:Landroid/widget/ImageView;

    return-void
.end method

.method private initBgOfSevenText()V
    .locals 3

    const/4 v2, 0x0

    const/high16 v1, -0x1000000

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private intiView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090002    # com.konka.epg.R.array.epg_Mainmenu_WeekDay_Str

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->refreshDayText()V

    return-void
.end method

.method private setBgOfSevenText(Landroid/widget/TextView;)V
    .locals 3
    .param p1    # Landroid/widget/TextView;

    const v2, 0x7f020013    # com.konka.epg.R.drawable.epg_day_img

    const v1, -0xff8601

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->initBgOfSevenText()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;
    .locals 2
    .param p1    # Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-static {}, Lcom/konka/epg/ui/EpgMainMenuDayText;->$SWITCH_TABLE$com$konka$epg$ui$EpgMainMenuActivity$EnumFocusPosition()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-object p1

    :pswitch_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->setBgOfSevenText(Landroid/widget/TextView;)V

    sget-object p1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIRST_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->setBgOfSevenText(Landroid/widget/TextView;)V

    sget-object p1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SECOND_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->setBgOfSevenText(Landroid/widget/TextView;)V

    sget-object p1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_THIRD_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->setBgOfSevenText(Landroid/widget/TextView;)V

    sget-object p1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FOURTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->setBgOfSevenText(Landroid/widget/TextView;)V

    sget-object p1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIFTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->setBgOfSevenText(Landroid/widget/TextView;)V

    sget-object p1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SIXTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->setBgOfSevenText(Landroid/widget/TextView;)V

    sget-object p1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_SEVENTH_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuDayText;->setBgOfSevenText(Landroid/widget/TextView;)V

    sget-object p1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_FIRST_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getCurrentDayId(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)I
    .locals 3
    .param p1    # Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const/4 v0, 0x0

    invoke-static {}, Lcom/konka/epg/ui/EpgMainMenuDayText;->$SWITCH_TABLE$com$konka$epg$ui$EpgMainMenuActivity$EnumFocusPosition()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getFirstDayIdOfWeek()I
    .locals 7

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    goto/16 :goto_0

    :cond_6
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method public getM_FifthText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_FirstDayText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_FourthText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_SecondDayText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_SeventhText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_SixthText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_ThirdText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    return-object v0
.end method

.method public refreshDayText()V
    .locals 4

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v1}, Lcom/konka/epg/service/EpgServiceProvider;->getTimeInfo()Landroid/text/format/Time;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    iget v1, v0, Landroid/text/format/Time;->weekDay:I

    iput v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    const-string v1, "get timeinfo fail, use moke one."

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayIdOfWeek(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayIdOfWeek(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    add-int/lit8 v3, v3, 0x2

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayIdOfWeek(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    add-int/lit8 v3, v3, 0x3

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayIdOfWeek(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    add-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayIdOfWeek(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    add-int/lit8 v3, v3, 0x5

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayIdOfWeek(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->strWeekDayArray:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    add-int/lit8 v3, v3, 0x6

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayIdOfWeek(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v1}, Lcom/konka/epg/service/EpgServiceProvider;->getTimeInfo()Landroid/text/format/Time;

    move-result-object v1

    iget v1, v1, Landroid/text/format/Time;->weekDay:I

    iput v1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    const-string v1, "get timeinfo finish"

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EPGMainMenuDayText cur todayId is----->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->iTodayDayId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public setM_FifthText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FifthDayText:Landroid/widget/TextView;

    return-void
.end method

.method public setM_FirstDayText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FirstDayText:Landroid/widget/TextView;

    return-void
.end method

.method public setM_FourthText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_FourthDayText:Landroid/widget/TextView;

    return-void
.end method

.method public setM_SecondDayText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SecondDayText:Landroid/widget/TextView;

    return-void
.end method

.method public setM_SeventhText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SeventhDayText:Landroid/widget/TextView;

    return-void
.end method

.method public setM_SixthText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_SixthDayText:Landroid/widget/TextView;

    return-void
.end method

.method public setM_ThirdText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuDayText;->m_ThirdDayText:Landroid/widget/TextView;

    return-void
.end method
