.class public Lcom/konka/epg/ui/EpgBookMenuViewHolder;
.super Ljava/lang/Object;
.source "EpgBookMenuViewHolder.java"


# instance fields
.field private final EPG_EVENT_NONE:I

.field private final EPG_EVENT_RECORDER:I

.field private final EPG_EVENT_REMIDER:I

.field private final EPG_REPEAT_DAILY:I

.field private final EPG_REPEAT_ONCE:I

.field private final EPG_REPEAT_WEEKLY:I

.field private final MILLISECOND_PER_DAY:J

.field private final MILLISECOND_PER_WEEK:J

.field private epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

.field private isAct:Z

.field private m_Activity:Landroid/app/Activity;

.field private m_BookEventList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;",
            ">;"
        }
    .end annotation
.end field

.field private m_BookList:Landroid/widget/ListView;

.field private m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

.field private m_BtmText:Landroid/widget/TextView;

.field private m_Context:Landroid/content/Context;

.field private m_EpgDelPopWin:Lcom/konka/epg/ui/EpgDelPopWin;

.field private m_TimeInfo:Landroid/text/format/Time;

.field private m_TimeText:Landroid/widget/TextView;

.field private m_TitleText:Landroid/widget/TextView;

.field private m_ViewReturn:Landroid/view/View;

.field private tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Context:Landroid/content/Context;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TitleText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookList:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TimeText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BtmText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookEventList:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_EpgDelPopWin:Lcom/konka/epg/ui/EpgDelPopWin;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TimeInfo:Landroid/text/format/Time;

    iput-boolean v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->isAct:Z

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_ViewReturn:Landroid/view/View;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->EPG_EVENT_NONE:I

    iput v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->EPG_EVENT_REMIDER:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->EPG_EVENT_RECORDER:I

    const-wide/32 v0, 0x5265c00

    iput-wide v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->MILLISECOND_PER_DAY:J

    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->MILLISECOND_PER_WEEK:J

    const/16 v0, 0x81

    iput v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->EPG_REPEAT_ONCE:I

    const/16 v0, 0x7f

    iput v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->EPG_REPEAT_DAILY:I

    const/16 v0, 0x82

    iput v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->EPG_REPEAT_WEEKLY:I

    iput-object p1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->findView()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->isAct:Z

    return v0
.end method

.method static synthetic access$1(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Lcom/konka/kkinterface/tv/EpgDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/epg/ui/EpgBookMenuViewHolder;Landroid/text/format/Time;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TimeInfo:Landroid/text/format/Time;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->refreshSysTime()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Lcom/konka/epg/ui/EpgDelPopWin;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_EpgDelPopWin:Lcom/konka/epg/ui/EpgDelPopWin;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Lcom/konka/epg/adapter/BookListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    return-object v0
.end method

.method private refreshSysTime()V
    .locals 6

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TimeText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TimeInfo:Landroid/text/format/Time;

    invoke-static {v2}, Lcom/konka/epg/util/EpgFormat;->formatDayText(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TimeInfo:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-static {v2}, Lcom/konka/epg/util/EpgFormat;->formatTimeText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setListener()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookList:Landroid/widget/ListView;

    new-instance v1, Lcom/konka/epg/ui/EpgBookMenuViewHolder$2;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder$2;-><init>(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_ViewReturn:Landroid/view/View;

    new-instance v1, Lcom/konka/epg/ui/EpgBookMenuViewHolder$3;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder$3;-><init>(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public findView()V
    .locals 4

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    const/high16 v2, 0x7f0a0000    # com.konka.epg.R.id.epg_bookmenu_title_text

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TitleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    const v2, 0x7f0a0001    # com.konka.epg.R.id.epg_bookmenu_time_text

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TimeText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    const v2, 0x7f0a0005    # com.konka.epg.R.id.epg_bookmenu_booked_list

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    const v2, 0x7f0a0006    # com.konka.epg.R.id.epg_bookmenu_btm_text

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BtmText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    const v2, 0x7f0a0007    # com.konka.epg.R.id.epg_bookmenu_btn_return

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_ViewReturn:Landroid/view/View;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/TVRootApp;

    invoke-virtual {v0}, Lcom/konka/epg/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    new-instance v1, Lcom/konka/epg/adapter/BookListAdapter;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/konka/epg/adapter/BookListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    new-instance v1, Lcom/konka/epg/ui/EpgDelPopWin;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Context:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/konka/epg/ui/EpgDelPopWin;-><init>(Landroid/view/View;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_EpgDelPopWin:Lcom/konka/epg/ui/EpgDelPopWin;

    return-void
.end method

.method public getM_BookList()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookList:Landroid/widget/ListView;

    return-object v0
.end method

.method public getM_BookListAdapter()Lcom/konka/epg/adapter/BookListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    return-object v0
.end method

.method public getM_TitleText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TitleText:Landroid/widget/TextView;

    return-object v0
.end method

.method public initView()V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->setAct(Z)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BtmText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_Activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001    # com.konka.epg.R.array.epg_Mainmenu_BookEvent_Str

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->setListener()V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->updateBookMenu()V

    return-void
.end method

.method public isAct()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->isAct:Z

    return v0
.end method

.method public setAct(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->isAct:Z

    return-void
.end method

.method public setM_BookList(Landroid/widget/ListView;)V
    .locals 0
    .param p1    # Landroid/widget/ListView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookList:Landroid/widget/ListView;

    return-void
.end method

.method public setM_BookListAdapter(Lcom/konka/epg/adapter/BookListAdapter;)V
    .locals 0
    .param p1    # Lcom/konka/epg/adapter/BookListAdapter;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    return-void
.end method

.method public setM_TitleText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_TitleText:Landroid/widget/TextView;

    return-void
.end method

.method public startTimeText()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;-><init>(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public updateBookMenu()V
    .locals 28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v5

    new-instance v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-direct {v12}, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/konka/epg/adapter/BookListAdapter;->delAllItem()V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/konka/kkinterface/tv/EpgDesk;->getEpgTimerEventCount()I

    move-result v13

    new-instance v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v15}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    const/16 v17, 0x0

    const/16 v19, 0x0

    new-instance v21, Landroid/text/format/Time;

    invoke-direct/range {v21 .. v21}, Landroid/text/format/Time;-><init>()V

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n===epgTimerEventCount "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v16, 0x0

    :goto_0
    move/from16 v0, v16

    if-lt v0, v13, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/konka/epg/adapter/BookListAdapter;->notifyDataSetChanged()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookList:Landroid/widget/ListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/ListView;->invalidate()V

    :goto_1
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/EpgDesk;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v12

    new-instance v18, Landroid/text/format/Time;

    invoke-direct/range {v18 .. v18}, Landroid/text/format/Time;-><init>()V

    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    new-instance v6, Landroid/text/format/Time;

    invoke-direct {v6}, Landroid/text/format/Time;-><init>()V

    invoke-virtual/range {v18 .. v18}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v10}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v6}, Landroid/text/format/Time;->setToNow()V

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n--->cur Time       "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v26, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    move-object/from16 v24, v0

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-short v0, v0

    move/from16 v25, v0

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    move/from16 v26, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-interface {v0, v1, v2, v6}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventCount(SILandroid/text/format/Time;)I

    move-result v14

    if-lez v14, :cond_5

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    move-object/from16 v24, v0

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-short v0, v0

    move/from16 v25, v0

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    move/from16 v26, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-interface {v0, v1, v2, v6, v14}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v24

    add-int/lit8 v24, v24, -0x1

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    move-object/from16 v0, v24

    iget v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x3e8

    mul-long v24, v24, v26

    const-wide/16 v26, 0x1

    add-long v24, v24, v26

    move-wide/from16 v0, v24

    invoke-virtual {v10, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x3e8

    mul-long v24, v24, v26

    const-wide/16 v26, 0x1

    add-long v24, v24, v26

    move-object/from16 v0, v18

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n--->eventCount     "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n--->startTime now  "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v26, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n--->endTime now    "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v10, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n--->enRepeatMode   "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v24, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v24

    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v10, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v26

    cmp-long v24, v24, v26

    if-lez v24, :cond_1

    iget-short v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    move/from16 v24, v0

    const/16 v25, 0x7f

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_2

    const/16 v24, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v24

    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v26

    sub-long v7, v24, v26

    const-wide/32 v24, 0x5265c00

    div-long v22, v7, v24

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x3e8

    mul-long v24, v24, v26

    const-wide/16 v26, 0x1

    add-long v24, v24, v26

    const-wide/32 v26, 0x5265c00

    mul-long v26, v26, v22

    sub-long v24, v24, v26

    move-object/from16 v0, v18

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    :cond_1
    :goto_2
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n--->startTime now C "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v26, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v26

    invoke-virtual/range {v25 .. v27}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    move-object/from16 v24, v0

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-short v0, v0

    move/from16 v25, v0

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    move/from16 v26, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v18

    invoke-interface {v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventCount(SILandroid/text/format/Time;)I

    move-result v14

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n--->eventCount2299     "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-lez v14, :cond_4

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n--->eventCount22     "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    move-object/from16 v24, v0

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-short v0, v0

    move/from16 v25, v0

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v18

    move/from16 v4, v27

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;

    move-result-object v20

    const/16 v24, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    :goto_3
    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    move/from16 v24, v0

    sget-object v25, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_DTV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    const/16 v26, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-interface {v5, v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramName(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;S)Ljava/lang/String;

    move-result-object v17

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "\n###########addEpgEvent schedule list-- "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x3e8

    mul-long v24, v24, v26

    move-object/from16 v0, v21

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    new-instance v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;-><init>()V

    iget-short v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_6

    const/16 v24, 0x2

    move/from16 v0, v24

    iput v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->timerType:I

    :goto_4
    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "CH"

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "  "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    iput-object v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    iget-object v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iput-object v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progNumber:I

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventId:I

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->serviceType:I

    iget-short v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->repeatMode:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Lcom/konka/epg/adapter/BookListAdapter;->addItem(Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;)V

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    :cond_2
    iget-short v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    move/from16 v24, v0

    const/16 v25, 0x82

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3

    const/16 v24, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v24

    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v26

    sub-long v7, v24, v26

    const-wide/32 v24, 0x240c8400

    div-long v22, v7, v24

    iget v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x3e8

    mul-long v24, v24, v26

    const-wide/16 v26, 0x1

    add-long v24, v24, v26

    const-wide/32 v26, 0x240c8400

    mul-long v26, v26, v22

    sub-long v24, v24, v26

    move-object/from16 v0, v18

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_3
    :try_start_1
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v24

    add-int/lit8 v24, v24, -0x1

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    move-object/from16 v0, v24

    iget v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    const-wide/16 v26, 0x3e8

    mul-long v24, v24, v26

    const-wide/16 v26, 0x1

    add-long v24, v24, v26

    move-object/from16 v0, v18

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_2

    :cond_4
    const-string v24, "NONE"

    move-object/from16 v0, v24

    iput-object v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    goto/16 :goto_3

    :cond_5
    const-string v24, "NONE"

    move-object/from16 v0, v24

    iput-object v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    goto/16 :goto_3

    :cond_6
    iget-short v0, v12, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_7

    const/16 v24, 0x1

    move/from16 v0, v24

    iput v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->timerType:I

    goto/16 :goto_4

    :cond_7
    const/16 v24, 0x0

    move/from16 v0, v24

    iput v0, v11, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->timerType:I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4
.end method
