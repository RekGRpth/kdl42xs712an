.class Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;
.super Ljava/lang/Object;
.source "EpgBookMenuViewHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/ui/EpgBookMenuViewHolder;->startTimeText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;)Lcom/konka/epg/ui/EpgBookMenuViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 3

    :goto_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgBookMenuViewHolder;->isAct:Z
    invoke-static {v1}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->access$0(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v2}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->access$1(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/EpgDesk;->getCurClkTime()Landroid/text/format/Time;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->access$2(Lcom/konka/epg/ui/EpgBookMenuViewHolder;Landroid/text/format/Time;)V

    sget-object v1, Lcom/konka/epg/TVRootApp;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1$1;

    invoke-direct {v2, p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder$1$1;-><init>(Lcom/konka/epg/ui/EpgBookMenuViewHolder$1;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-wide/32 v1, 0xea60

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
