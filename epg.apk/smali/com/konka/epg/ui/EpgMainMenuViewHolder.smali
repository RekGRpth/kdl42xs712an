.class public Lcom/konka/epg/ui/EpgMainMenuViewHolder;
.super Ljava/lang/Object;
.source "EpgMainMenuViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;,
        Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemClick;,
        Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;,
        Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;,
        Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemClick;,
        Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;,
        Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

.field private static epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;


# instance fields
.field private activity:Landroid/app/Activity;

.field private context:Landroid/content/Context;

.field private eThread:Ljava/util/concurrent/ExecutorService;

.field private epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

.field private epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

.field private epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

.field private eventHandler:Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;

.field private iCurrentDayId:I

.field private iLastPosInDtv:I

.field private iLastPosInRadio:I

.field private iPosInChannelList:I

.field private iPosInEventList:I

.field private isActiveByStart:Z

.field private isMainMenuActive:Z

.field private m_ChannelList:Landroid/widget/ListView;

.field private m_DateText:Landroid/widget/TextView;

.field private m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

.field private m_EpgStatus:I

.field private m_EpgTypeTxt:Landroid/widget/TextView;

.field private m_EpgTypeView:Landroid/view/View;

.field private m_EventList:Landroid/widget/ListView;

.field private m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

.field private m_SourceTypeText:Landroid/widget/TextView;

.field private m_WeekEventAdapterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/epg/adapter/EventListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private res:Landroid/content/res/Resources;

.field private strServiceTypeArray:[Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I
    .locals 3

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DATA:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_UNITED_TV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/konka/epg/service/EpgServiceProvider;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/konka/epg/service/EpgServiceProvider;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->res:Landroid/content/res/Resources;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->context:Landroid/content/Context;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eventHandler:Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eThread:Ljava/util/concurrent/ExecutorService;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DateText:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_SourceTypeText:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->strServiceTypeArray:[Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    iput v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    iput v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    iput v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    iput-boolean v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->isMainMenuActive:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    iput v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iLastPosInRadio:I

    iput v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iLastPosInDtv:I

    iput-boolean v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->isActiveByStart:Z

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->res:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->context:Landroid/content/Context;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->res:Landroid/content/res/Resources;

    const v1, 0x7f090003    # com.konka.epg.R.array.epg_Mainmenu_Source_Str

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->strServiceTypeArray:[Ljava/lang/String;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;

    invoke-direct {v0, p0, v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;)V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eventHandler:Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/epg/util/EpgToast;->setContext(Landroid/content/Context;)V

    return-void
.end method

.method private GetEpgChannelNameFromChannelNum(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v1, p1}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v1

    iget-object v0, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$0(Lcom/konka/epg/ui/EpgMainMenuViewHolder;I)V
    .locals 0

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    return-void
.end method

.method static synthetic access$1(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    return v0
.end method

.method static synthetic access$10(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/adapter/ChannelListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    return v0
.end method

.method static synthetic access$12(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eventHandler:Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/epg/ui/EpgMainMenuViewHolder;I)V
    .locals 0

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/ui/EpgMainMenuDayText;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/epg/ui/EpgMainMenuViewHolder;I)V
    .locals 0

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    return v0
.end method

.method static synthetic access$9(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/adapter/ChannelListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    return-object v0
.end method

.method private bookThisEventOrCancel()V
    .locals 8

    const/4 v7, 0x1

    const/16 v6, 0x3e8

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/epg/adapter/EventListAdapter;

    iget v4, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    invoke-virtual {v3, v4}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v2

    iget-boolean v3, v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v3, v2}, Lcom/konka/epg/service/EpgServiceProvider;->bookEvent(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    move-result-object v0

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_SUCCESSS:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    if-ne v0, v3, :cond_1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->getM_BookEventText()Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    invoke-virtual {v4}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->getM_BookEventStr()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v7

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v3, "IsScheduled ========>>> invisible 3"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iput-boolean v7, v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    const-string v3, "\u9884\u8ba2\u4e86\u8be5\u8282\u76ee"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f08001c    # com.konka.epg.R.string.epg_Toast_BookSuc_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_OF_EXIST:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    if-ne v0, v3, :cond_2

    const-string v3, "\u9884\u8ba2\u5df2\u7ecf\u5b58\u5728"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f080015    # com.konka.epg.R.string.epg_Toast_BookFail_Exist_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_FULL:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    if-ne v0, v3, :cond_3

    const-string v3, "\u9884\u8ba2\u5df2\u6ee1"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f080016    # com.konka.epg.R.string.epg_Toast_BookFail_Full_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_OF_ATHAND:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    if-ne v0, v3, :cond_4

    const-string v3, "\u5373\u5c07\u64ad\u51fa"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f080019    # com.konka.epg.R.string.epg_Toast_BookFail_AtHand_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_OVERLAY:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    if-ne v0, v3, :cond_5

    const-string v3, "schedule fails for other exception"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f080021    # com.konka.epg.R.string.epg_Toast_BookFail_OverLay_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_OTHER_EXCEPTION:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    if-ne v0, v3, :cond_6

    const-string v3, "schedule fails for other exception"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f08001a    # com.konka.epg.R.string.epg_Toast_BookFail_Exception_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto :goto_0

    :cond_6
    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_TIMEOUT:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    if-ne v0, v3, :cond_0

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f080022    # com.konka.epg.R.string.epg_Toast_BookFail_TimeOut_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto :goto_0

    :cond_7
    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    iget v4, v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIndex:I

    invoke-interface {v3, v4, v7}, Lcom/konka/epg/service/EpgServiceProvider;->delBookedEvent(IZ)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v3, "IsScheduled ========>>> invisible 1"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iput-boolean v7, v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    const-string v3, "\u53d6\u6d88\u9884\u8ba2\u5931\u8d25"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f080017    # com.konka.epg.R.string.epg_Toast_BookFail_Cancel_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto/16 :goto_0

    :cond_8
    const-string v3, "IsScheduled ========>>> invisible 2"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->getM_BookEventText()Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    invoke-virtual {v4}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->getM_BookEventStr()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v5, v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    const-string v3, "\u53d6\u6d88\u9884\u8ba2\u4e86\u8be5\u8282\u76ee"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v3

    const v4, 0x7f08001d    # com.konka.epg.R.string.epg_Toast_UnBookSuc_Str

    invoke-virtual {v3, v4, v5, v5, v6}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto/16 :goto_0
.end method

.method public static getInstance(Landroid/app/Activity;Lcom/konka/epg/service/EpgServiceProvider;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    .locals 1
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/konka/epg/service/EpgServiceProvider;

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-direct {v0, p0, p1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;-><init>(Landroid/app/Activity;Lcom/konka/epg/service/EpgServiceProvider;)V

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    :cond_0
    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    return-object v0
.end method

.method private startRefreshFirstDay()V
    .locals 6

    const-wide/16 v2, 0x1f40

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    new-instance v1, Lcom/konka/epg/ui/EpgMainMenuViewHolder$3;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$3;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method


# virtual methods
.method public GetEpgEventNameFromEventNum(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v2, p1}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v2, "\n Get event name failed\n"

    invoke-static {v2}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public chListRequestFocus()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iput v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v0

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->setVisibility(I)V

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-virtual {v0, v1}, Lcom/konka/epg/adapter/ChannelListAdapter;->setSelect(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->notifyDataSetChanged()V

    :goto_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v0, v2}, Lcom/konka/epg/adapter/EventListAdapter;->setFocus(Z)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/EventListAdapter;->notifyDataSetChanged()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-virtual {v0, v1}, Lcom/konka/epg/adapter/ChannelListAdapter;->setSelect(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public delayProgSel(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-interface {v0, v1}, Lcom/konka/epg/service/EpgServiceProvider;->delayProgSel(I)V

    return-void
.end method

.method public evListRequestFocus()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "the event pos is ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    if-le v0, v1, :cond_1

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    iget v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    :goto_0
    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v1, v2}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v1

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    invoke-virtual {v1, v3}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->setVisibility(I)V

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iget v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-virtual {v1, v2}, Lcom/konka/epg/adapter/ChannelListAdapter;->setSelect(I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/ChannelListAdapter;->notifyDataSetChanged()V

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "=========================="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/epg/adapter/EventListAdapter;->setFocus(Z)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->notifyDataSetChanged()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iget v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-virtual {v1, v2}, Lcom/konka/epg/adapter/ChannelListAdapter;->setSelect(I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/ChannelListAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method public findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0011    # com.konka.epg.R.id.epg_mainmenu_time_text

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DateText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0012    # com.konka.epg.R.id.epg_mainmenu_source_text

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_SourceTypeText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0013    # com.konka.epg.R.id.epg_mainmenu_channel_list

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0015    # com.konka.epg.R.id.epg_mainmenu_epgtype_view

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0016    # com.konka.epg.R.id.epg_mainmenu_epgtype_txt

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    const v1, 0x7f0a002d    # com.konka.epg.R.id.epg_mainmenu_event_list

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    return-void
.end method

.method public getCurrentDayId()I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    return v0
.end method

.method public getEpgMainMenuButtomText()Lcom/konka/epg/ui/EpgMainMenuButtomText;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    return-object v0
.end method

.method public getEpgMainMenuDayText()Lcom/konka/epg/ui/EpgMainMenuDayText;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    return-object v0
.end method

.method public getIsActive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->isMainMenuActive:Z

    return v0
.end method

.method public getIsActiveCreate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->isActiveByStart:Z

    return v0
.end method

.method public getIsSignalTabled()Z
    .locals 2

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    const/16 v1, 0x44c

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getM_ChannelAdapter()Lcom/konka/epg/adapter/ChannelListAdapter;
    .locals 2

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getM_ChannelList()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    return-object v0
.end method

.method public getM_DateText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DateText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_EpgTypeTxt()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_EpgTypeView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    return-object v0
.end method

.method public getM_EventList()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    return-object v0
.end method

.method public getM_ServiceTypeStr()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->strServiceTypeArray:[Ljava/lang/String;

    return-object v0
.end method

.method public getM_SourceTypeText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_SourceTypeText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_WeekEventAdapterList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/epg/adapter/EventListAdapter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPosInChannelList()I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    return v0
.end method

.method public getPosInEventList()I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    return v0
.end method

.method public initEpgStatus()V
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v0}, Lcom/konka/epg/service/EpgServiceProvider;->isSignalStabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setIsSignalTabled(Z)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getIsSignalTabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x44d

    invoke-virtual {p0, v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setCurEpgStatus(I)V

    :cond_0
    return-void
.end method

.method public initView()V
    .locals 3

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setListeners()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v0}, Lcom/konka/epg/service/EpgServiceProvider;->initTimeInfoFromDtv()V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->initEpgStatus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v0}, Lcom/konka/epg/service/EpgServiceProvider;->getDtvChannlAdapter()Lcom/konka/epg/adapter/ChannelListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v0}, Lcom/konka/epg/service/EpgServiceProvider;->getRadioChannelAdapter()Lcom/konka/epg/adapter/ChannelListAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v0}, Lcom/konka/epg/service/EpgServiceProvider;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_SourceTypeText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->strServiceTypeArray:[Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v2}, Lcom/konka/epg/service/EpgServiceProvider;->getCurServiceType()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v0}, Lcom/konka/epg/service/EpgServiceProvider;->getCurServiceType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    :goto_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v0}, Lcom/konka/epg/service/EpgServiceProvider;->initPosInChannelList()I

    move-result v0

    iput v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->startRefreshFirstDay()V

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuDayText;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-direct {v0, v1, v2}, Lcom/konka/epg/ui/EpgMainMenuDayText;-><init>(Landroid/app/Activity;Lcom/konka/epg/service/EpgServiceProvider;)V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuButtomText;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-direct {v0, v1, p0, v2}, Lcom/konka/epg/ui/EpgMainMenuButtomText;-><init>(Landroid/app/Activity;Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/service/EpgServiceProvider;)V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuButtomText:Lcom/konka/epg/ui/EpgMainMenuButtomText;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->chListRequestFocus()V

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eThread:Ljava/util/concurrent/ExecutorService;

    const-string v0, "init epg success here =========="

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    goto :goto_0
.end method

.method public refreshByService(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    const v5, 0x7f080018    # com.konka.epg.R.string.epg_Toast_ListEmpty_Str

    const/16 v4, 0x44c

    const/16 v3, 0x3e8

    const/4 v2, 0x0

    invoke-static {}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    iput v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iLastPosInDtv:I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_ChannelList()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_SourceTypeText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_ServiceTypeStr()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iLastPosInRadio:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    if-eq v0, v4, :cond_0

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EpgTypeView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020021    # com.konka.epg.R.drawable.epg_type

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pos in channel list of radio ========= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iLastPosInRadio:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v0

    invoke-virtual {v0, v5, v2, v2, v3}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    iput v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iLastPosInRadio:I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_CHANNEL_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_ChannelList()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_SourceTypeText()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_ServiceTypeStr()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iLastPosInDtv:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    const v1, 0x7f050003    # com.konka.epg.R.color.transparent

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "pos in channel list of dtv ========= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iLastPosInDtv:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    invoke-static {}, Lcom/konka/epg/util/EpgToast;->getInstance()Lcom/konka/epg/util/EpgToast;

    move-result-object v0

    invoke-virtual {v0, v5, v2, v2, v3}, Lcom/konka/epg/util/EpgToast;->showToast(IIII)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public refreshEventList(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)V
    .locals 3
    .param p1    # Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgMainMenuDayText:Lcom/konka/epg/ui/EpgMainMenuDayText;

    invoke-virtual {v0, p1}, Lcom/konka/epg/ui/EpgMainMenuDayText;->getCurrentDayId(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)I

    move-result v0

    iput v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    invoke-interface {v0, v1, v2}, Lcom/konka/epg/service/EpgServiceProvider;->getEventOfDay(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V

    return-void
.end method

.method public refreshEventListByChannel(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-interface {v0, p1, v1}, Lcom/konka/epg/service/EpgServiceProvider;->getEventOfWeek(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V

    return-void
.end method

.method public refreshFirstDayEvent(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    iget v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    invoke-interface {v0, p1, v1}, Lcom/konka/epg/service/EpgServiceProvider;->getEventOfFirstDay(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V

    return-void
.end method

.method public refreshSysTime()V
    .locals 5

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v1}, Lcom/konka/epg/service/EpgServiceProvider;->getTimeInfo()Landroid/text/format/Time;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    long-to-int v0, v1

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DateText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-interface {v3}, Lcom/konka/epg/service/EpgServiceProvider;->getTimeInfo()Landroid/text/format/Time;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayText(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/konka/epg/util/EpgFormat;->formatTimeText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setCurEpgStatus(I)V
    .locals 5
    .param p1    # I

    const v4, 0x7f020021    # com.konka.epg.R.drawable.epg_type

    const/16 v3, 0x44d

    const/16 v1, 0x44c

    const v2, 0x7f050003    # com.konka.epg.R.color.transparent

    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    if-ne v0, v1, :cond_3

    if-ne p1, v3, :cond_0

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    if-ne p1, v1, :cond_5

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_4
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    const v1, 0x7f080008    # com.konka.epg.R.string.epg_Mainmenu_Signal_Unlock

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_5
    if-ne p1, v3, :cond_7

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_6
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_7
    const/16 v0, 0x457

    if-ne p1, v0, :cond_9

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_8
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_9
    const/16 v0, 0x450

    if-ne p1, v0, :cond_a

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/ui/EpgMainMenuViewHolder$1;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$1;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_a
    const/16 v0, 0x452

    if-ne p1, v0, :cond_b

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/ui/EpgMainMenuViewHolder$2;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$2;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    :cond_b
    const/16 v0, 0x455

    if-ne p1, v0, :cond_0

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method public setCurrentDayId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I

    return-void
.end method

.method public setIsActive(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->isMainMenuActive:Z

    return-void
.end method

.method public setIsActiveCreate(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->isActiveByStart:Z

    return-void
.end method

.method public setIsSignalTabled(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/16 v0, 0x44d

    iput v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x44c

    iput v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I

    goto :goto_0
.end method

.method public setListeners()V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemClick;

    invoke-direct {v0, p0, v6}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemClick;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemClick;)V

    new-instance v1, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;

    invoke-direct {v1, p0, v6}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;)V

    new-instance v3, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemClick;

    invoke-direct {v3, p0, v6}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemClick;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemClick;)V

    new-instance v4, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;

    invoke-direct {v4, p0, v6}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;)V

    new-instance v2, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;

    invoke-direct {v2, p0, v6}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;)V

    new-instance v5, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;

    invoke-direct {v5, p0, v6}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;)V

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v6, v4}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v6, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_SourceTypeText:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v6, v5}, Landroid/widget/ListView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v6, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    invoke-virtual {v6, v5}, Landroid/widget/ListView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public setM_ChannelList(Landroid/widget/ListView;)V
    .locals 0
    .param p1    # Landroid/widget/ListView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_ChannelList:Landroid/widget/ListView;

    return-void
.end method

.method public setM_DateText(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DateText:Landroid/widget/TextView;

    return-void
.end method

.method public setM_DtvChannelAdapter(Lcom/konka/epg/adapter/ChannelListAdapter;)V
    .locals 0
    .param p1    # Lcom/konka/epg/adapter/ChannelListAdapter;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    return-void
.end method

.method public setM_EpgTypeTxt(Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;

    return-void
.end method

.method public setM_EpgTypeView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;

    return-void
.end method

.method public setM_EventList(Landroid/widget/ListView;)V
    .locals 0
    .param p1    # Landroid/widget/ListView;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    return-void
.end method

.method public setM_RadioChannelAdapter(Lcom/konka/epg/adapter/ChannelListAdapter;)V
    .locals 0
    .param p1    # Lcom/konka/epg/adapter/ChannelListAdapter;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    return-void
.end method

.method public setM_ServiceTypeStr([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->strServiceTypeArray:[Ljava/lang/String;

    return-void
.end method

.method public setM_SourceTypeText(Landroid/widget/Button;)V
    .locals 0
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_SourceTypeText:Landroid/widget/TextView;

    return-void
.end method

.method public setM_WeekEventAdapterList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/epg/adapter/EventListAdapter;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    return-void
.end method

.method public setPosInChannelList(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I

    return-void
.end method

.method public setPosInEventList(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I

    return-void
.end method

.method public startBookMenu()V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setIsActive(Z)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->context:Landroid/content/Context;

    const-class v2, Lcom/konka/epg/ui/EpgBookMenuActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public startRecordMenu()V
    .locals 5

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->context:Landroid/content/Context;

    const-class v3, Lcom/konka/epg/ui/EpgRecordMenuActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v2}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v2

    if-ge v3, v2, :cond_0

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/epg/adapter/EventListAdapter;

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v1

    const-string v2, "EventBaseTime"

    iget v3, v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "FocusIndex"

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "CurrentDayId"

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public startReminderMenu()V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setIsActive(Z)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->context:Landroid/content/Context;

    const-class v3, Lcom/konka/epg/ui/EpgReminderMenuActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EPG startReminderMenu ========>>> CurrentDayId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v2}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v2}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v2

    if-ge v3, v2, :cond_0

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/epg/adapter/EventListAdapter;

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v1

    const-string v2, "EventBaseTime"

    iget v3, v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "FocusIndex"

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "CurrentDayId"

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->activity:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
