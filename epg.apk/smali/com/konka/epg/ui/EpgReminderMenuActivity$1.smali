.class Lcom/konka/epg/ui/EpgReminderMenuActivity$1;
.super Ljava/lang/Object;
.source "EpgReminderMenuActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/ui/EpgReminderMenuActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;


# direct methods
.method constructor <init>(Lcom/konka/epg/ui/EpgReminderMenuActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "\n###########addEpgEvent reminder onClick\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    invoke-static {v5}, Lcom/konka/epg/service/EpgSrvProImp;->getEpgMgrInstance(Landroid/app/Activity;)Lcom/konka/epg/service/EpgSrvProImp;

    move-result-object v1

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v5

    iget v6, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget-object v7, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->offsetTimeInS:J
    invoke-static {v7}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$1(Lcom/konka/epg/ui/EpgReminderMenuActivity;)J

    move-result-wide v7

    long-to-int v7, v7

    sub-int/2addr v6, v7

    iput v6, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$2()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v5

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/EpgDesk;->addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    move-result-object v3

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\n########### status.ordinal()  \n"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\n########### TvOsType.EnumEpgTimerCheck.E_SUCCESS.ordinal()  \n"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_SUCCESS:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v5

    sget-object v6, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_SUCCESS:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v6

    if-ne v5, v6, :cond_0

    invoke-virtual {v1}, Lcom/konka/epg/service/EpgSrvProImp;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventDayIdx:I
    invoke-static {v6}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$3(Lcom/konka/epg/ui/EpgReminderMenuActivity;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/konka/epg/adapter/EventListAdapter;

    iget-object v6, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventIdx:I
    invoke-static {v6}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$4(Lcom/konka/epg/ui/EpgReminderMenuActivity;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v5

    iget-object v2, v5, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\n########### start time: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v7

    iget v7, v7, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " #eventname: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " #servicename: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    invoke-static {v7}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$5(Lcom/konka/epg/ui/EpgReminderMenuActivity;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v7

    iget-object v7, v7, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$2()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    invoke-static {v6}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$5(Lcom/konka/epg/ui/EpgReminderMenuActivity;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v6

    iget-object v6, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v7

    iget v7, v7, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-interface {v5, v6, v2, v7}, Lcom/konka/kkinterface/tv/EpgDesk;->UpdateEpgTimer(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    iget-object v5, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    iget-object v6, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgReminderMenuActivity;->bookToast:[Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->access$6(Lcom/konka/epg/ui/EpgReminderMenuActivity;)[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    const/16 v7, 0x1e

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v5, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgReminderMenuActivity;

    invoke-virtual {v5}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->finish()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
