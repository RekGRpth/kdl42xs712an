.class public Lcom/konka/epg/ui/EpgReminderMenuActivity;
.super Landroid/app/Activity;
.source "EpgReminderMenuActivity.java"


# static fields
.field private static cd:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private static ed:Lcom/konka/kkinterface/tv/EpgDesk;

.field private static eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

.field private static userProgramIndex:I


# instance fields
.field private final EPG_EVENT_NONE:I

.field private final EPG_EVENT_RECORDER:I

.field private final EPG_EVENT_REMIDER:I

.field private final EPG_REPEAT_DAILY:I

.field private final EPG_REPEAT_ONCE:I

.field private final EPG_REPEAT_WEEKLY:I

.field private bookToast:[Ljava/lang/String;

.field private channelLayout:Landroid/widget/LinearLayout;

.field private channelValue:Landroid/widget/TextView;

.field private curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

.field private dateLayout:Landroid/widget/LinearLayout;

.field private dateValue:Landroid/widget/TextView;

.field private eventBaseTime:I

.field private eventDayIdx:I

.field private eventIdx:I

.field private eventInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;",
            ">;"
        }
    .end annotation
.end field

.field private hourLayout:Landroid/widget/LinearLayout;

.field private hourValue:Landroid/widget/TextView;

.field private minuteLayout:Landroid/widget/LinearLayout;

.field private minuteValue:Landroid/widget/TextView;

.field private modeLayout:Landroid/widget/LinearLayout;

.field private modeValue:Landroid/widget/TextView;

.field private monthLayout:Landroid/widget/LinearLayout;

.field private monthValue:Landroid/widget/TextView;

.field private offsetTimeInS:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;-><init>()V

    sput-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    sput-object v1, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sput-object v1, Lcom/konka/epg/ui/EpgReminderMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    const/4 v0, 0x0

    sput v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventBaseTime:I

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventIdx:I

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventDayIdx:I

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->EPG_EVENT_NONE:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->EPG_EVENT_REMIDER:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->EPG_EVENT_RECORDER:I

    const/16 v0, 0x81

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->EPG_REPEAT_ONCE:I

    const/16 v0, 0x7f

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->EPG_REPEAT_DAILY:I

    const/16 v0, 0x82

    iput v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->EPG_REPEAT_WEEKLY:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventInfoList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->monthValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->dateValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelLayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteLayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourLayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->monthLayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->dateLayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeLayout:Landroid/widget/LinearLayout;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->offsetTimeInS:J

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    return-void
.end method

.method static synthetic access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .locals 1

    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/epg/ui/EpgReminderMenuActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->offsetTimeInS:J

    return-wide v0
.end method

.method static synthetic access$2()Lcom/konka/kkinterface/tv/EpgDesk;
    .locals 1

    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/epg/ui/EpgReminderMenuActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventDayIdx:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/epg/ui/EpgReminderMenuActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventIdx:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/epg/ui/EpgReminderMenuActivity;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/epg/ui/EpgReminderMenuActivity;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->bookToast:[Ljava/lang/String;

    return-object v0
.end method

.method private clearFocus()V
    .locals 2

    const/high16 v1, 0x7f020000    # com.konka.epg.R.drawable.alpha_img

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->monthValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->dateValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->monthLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->dateLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    return-void
.end method


# virtual methods
.method public changeFocusDown()V
    .locals 2

    const v1, 0x7f020028    # com.konka.epg.R.drawable.programme_epg_img_focus

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public changeFocusUp()V
    .locals 2

    const v1, 0x7f020028    # com.konka.epg.R.drawable.programme_epg_img_focus

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public changeReminderHour(Z)V
    .locals 8
    .param p1    # Z

    const-wide/16 v6, 0x3e8

    sget-object v3, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v2, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    int-to-long v3, v2

    mul-long/2addr v3, v6

    invoke-virtual {v0, v3, v4}, Landroid/text/format/Time;->set(J)V

    iget v2, v0, Landroid/text/format/Time;->hour:I

    if-eqz p1, :cond_1

    if-lez v2, :cond_0

    add-int/lit8 v2, v2, -0x1

    :goto_0
    iput v2, v0, Landroid/text/format/Time;->hour:I

    sget-object v3, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    div-long/2addr v4, v6

    long-to-int v4, v4

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\n Start time: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->invalidate()V

    return-void

    :cond_0
    const/16 v2, 0x17

    goto :goto_0

    :cond_1
    const/16 v3, 0x17

    if-ge v2, v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public changeReminderMin(Z)V
    .locals 8
    .param p1    # Z

    const-wide/16 v6, 0x3e8

    sget-object v3, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v2, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    int-to-long v3, v2

    mul-long/2addr v3, v6

    invoke-virtual {v0, v3, v4}, Landroid/text/format/Time;->set(J)V

    iget v2, v0, Landroid/text/format/Time;->minute:I

    if-eqz p1, :cond_1

    if-lez v2, :cond_0

    add-int/lit8 v2, v2, -0x1

    :goto_0
    iput v2, v0, Landroid/text/format/Time;->minute:I

    sget-object v3, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    div-long/2addr v4, v6

    long-to-int v4, v4

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->invalidate()V

    return-void

    :cond_0
    const/16 v2, 0x3b

    goto :goto_0

    :cond_1
    const/16 v3, 0x3b

    if-ge v2, v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public changeReminderMode(Z)V
    .locals 5
    .param p1    # Z

    const v4, 0x7f08002a    # com.konka.epg.R.string.epg_reminder_once_str

    const/16 v3, 0x82

    const/16 v2, 0x7f

    const/16 v1, 0x81

    if-eqz p1, :cond_0

    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    :goto_0
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    return-void

    :pswitch_2
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v2, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v3, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    packed-switch v0, :pswitch_data_2

    :pswitch_5
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v3, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v2, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_a
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    const v1, 0x7f08002b    # com.konka.epg.R.string.epg_reminder_daily_str

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_b
    iget-object v0, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    const v1, 0x7f08002c    # com.konka.epg.R.string.epg_reminder_weekly_str

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f
        :pswitch_a
        :pswitch_1
        :pswitch_9
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7f
        :pswitch_8
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public changeReminderProgramNumber(Z)V
    .locals 7
    .param p1    # Z

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/TVRootApp;

    invoke-virtual {v0}, Lcom/konka/epg/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v4

    sput-object v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v4

    sput-object v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    if-eqz p1, :cond_1

    sget v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    if-lez v4, :cond_0

    sget v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    add-int/lit8 v4, v4, -0x1

    sput v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    :goto_0
    sget-object v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget v5, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    sget-object v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    new-instance v4, Ljava/lang/StringBuilder;

    iget v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->invalidate()V

    return-void

    :cond_0
    sget-object v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    sput v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    goto :goto_0

    :cond_1
    sget v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    sget-object v5, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_2

    sget v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    sput v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v10, 0x7f03000b    # com.konka.epg.R.layout.epg_reminder_menu

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/TVRootApp;

    invoke-virtual {v0}, Lcom/konka/epg/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v7

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v10

    sput-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v10

    sput-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    new-instance v10, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    invoke-direct {v10}, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;-><init>()V

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    new-instance v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-direct {v10}, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;-><init>()V

    sput-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "EventBaseTime"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    iput v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventBaseTime:I

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "FocusIndex"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    iput v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventIdx:I

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "CurrentDayId"

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    iput v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventDayIdx:I

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "\n======>>>REC eventBaseTime "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventBaseTime:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "\n======>>>REC eventIdx "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventIdx:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ",eventDayIdx "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventDayIdx:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v10

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v10, v11}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v5

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_1

    :goto_1
    const v10, 0x7f0a0067    # com.konka.epg.R.id.reminder_channel_value

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    const v10, 0x7f0a006a    # com.konka.epg.R.id.reminder_minute_value

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    const v10, 0x7f0a006d    # com.konka.epg.R.id.reminder_hour_value

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    const v10, 0x7f0a0070    # com.konka.epg.R.id.reminder_month_value

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->monthValue:Landroid/widget/TextView;

    const v10, 0x7f0a0073    # com.konka.epg.R.id.reminder_date_value

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->dateValue:Landroid/widget/TextView;

    const v10, 0x7f0a0075    # com.konka.epg.R.id.reminder_mode_value

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    const v10, 0x7f0a0065    # com.konka.epg.R.id.reminder_channel_layout

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelLayout:Landroid/widget/LinearLayout;

    const v10, 0x7f0a0068    # com.konka.epg.R.id.reminder_minute_layout

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteLayout:Landroid/widget/LinearLayout;

    const v10, 0x7f0a006b    # com.konka.epg.R.id.reminder_hour_layout

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourLayout:Landroid/widget/LinearLayout;

    const v10, 0x7f0a006e    # com.konka.epg.R.id.reminder_month_layout

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->monthLayout:Landroid/widget/LinearLayout;

    const v10, 0x7f0a0071    # com.konka.epg.R.id.reminder_date_layout

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->dateLayout:Landroid/widget/LinearLayout;

    const v10, 0x7f0a0074    # com.konka.epg.R.id.reminder_mode_layout

    invoke-virtual {p0, v10}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeLayout:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteLayout:Landroid/widget/LinearLayout;

    const v11, 0x7f020028    # com.konka.epg.R.drawable.programme_epg_img_focus

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->monthValue:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->dateValue:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090004    # com.konka.epg.R.array.epg_book_toast

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->bookToast:[Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget v11, v11, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v11, v11, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/4 v11, 0x1

    iput-short v11, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/16 v11, 0x81

    iput-short v11, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v11, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventBaseTime:I

    iput v11, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v11, v2, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iput v11, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-object v11, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-short v11, v11, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iput v11, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-object v11, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget v11, v11, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iput v11, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v11, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventIdx:I

    iput v11, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v10, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    int-to-long v10, v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    invoke-virtual {v8, v10, v11}, Landroid/text/format/Time;->set(J)V

    new-instance v10, Ljava/lang/StringBuilder;

    iget v11, v8, Landroid/text/format/Time;->minute:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v10, Ljava/lang/StringBuilder;

    iget v11, v8, Landroid/text/format/Time;->hour:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v10, Ljava/lang/StringBuilder;

    iget v11, v8, Landroid/text/format/Time;->month:I

    add-int/lit8 v11, v11, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->monthValue:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v10, Ljava/lang/StringBuilder;

    iget v11, v8, Landroid/text/format/Time;->monthDay:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->dateValue:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-short v10, v10, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    packed-switch v10, :pswitch_data_0

    :pswitch_0
    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    const v11, 0x7f08002a    # com.konka.epg.R.string.epg_reminder_once_str

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    new-instance v1, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;

    invoke-direct {v1, p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity$1;-><init>(Lcom/konka/epg/ui/EpgReminderMenuActivity;)V

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    sget-object v10, Lcom/konka/epg/ui/EpgReminderMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v10, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v4

    iget v10, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-object v11, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget v11, v11, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v10, v11, :cond_2

    iget-short v10, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-object v11, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-short v11, v11, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v10, v11, :cond_2

    sput v3, Lcom/konka/epg/ui/EpgReminderMenuActivity;->userProgramIndex:I

    goto/16 :goto_1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :pswitch_1
    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    const v11, 0x7f08002a    # com.konka.epg.R.string.epg_reminder_once_str

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :pswitch_2
    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    const v11, 0x7f08002b    # com.konka.epg.R.string.epg_reminder_daily_str

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :pswitch_3
    iget-object v10, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    const v11, 0x7f08002c    # com.konka.epg.R.string.epg_reminder_weekly_str

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x7f
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    :cond_0
    :goto_0
    return v3

    :sswitch_0
    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeFocusDown()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeFocusUp()V

    goto :goto_0

    :sswitch_2
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "\n###########addEpgEvent reminder KEYCODE_ENTER\n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    sget-object v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget-wide v6, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->offsetTimeInS:J

    long-to-int v6, v6

    sub-int/2addr v5, v6

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v4, Lcom/konka/epg/ui/EpgReminderMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    sget-object v5, Lcom/konka/epg/ui/EpgReminderMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/EpgDesk;->addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    move-result-object v1

    const-string v4, "addEpgEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "===========>>>> status is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->bookToast:[Ljava/lang/String;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v5

    aget-object v4, v4, v5

    const/16 v5, 0x1e

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->finish()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->finish()V

    goto :goto_0

    :sswitch_4
    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v3}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeReminderProgramNumber(Z)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v3}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeReminderMin(Z)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0, v3}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeReminderHour(Z)V

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v3}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeReminderMode(Z)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0, v5}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeReminderProgramNumber(Z)V

    goto/16 :goto_0

    :cond_4
    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->minuteValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0, v5}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeReminderMin(Z)V

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->hourValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {p0, v5}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeReminderHour(Z)V

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/konka/epg/ui/EpgReminderMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v5}, Lcom/konka/epg/ui/EpgReminderMenuActivity;->changeReminderMode(Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_3
        0x13 -> :sswitch_1
        0x14 -> :sswitch_0
        0x15 -> :sswitch_4
        0x16 -> :sswitch_5
        0x42 -> :sswitch_2
        0xba -> :sswitch_3
    .end sparse-switch
.end method
