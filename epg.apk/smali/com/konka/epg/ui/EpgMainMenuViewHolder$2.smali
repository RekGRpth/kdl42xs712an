.class Lcom/konka/epg/ui/EpgMainMenuViewHolder$2;
.super Ljava/lang/Object;
.source "EpgMainMenuViewHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setCurEpgStatus(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0xce4

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->sleep(J)V

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I
    invoke-static {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$11(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I

    move-result v2

    const/16 v3, 0x452

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eventHandler:Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;
    invoke-static {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$14(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x4b1

    iput v2, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->eventHandler:Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;
    invoke-static {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$14(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "here a thread interrupted ======"

    invoke-static {v2}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method
