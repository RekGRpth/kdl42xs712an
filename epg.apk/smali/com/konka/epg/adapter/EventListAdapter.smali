.class public Lcom/konka/epg/adapter/EventListAdapter;
.super Landroid/widget/BaseAdapter;
.source "EventListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private isFocus:Z

.field private isInList:Z

.field private isPlaying:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private m_EventData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/adapter/EventListAdapter;->m_EventData:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->isPlaying:Z

    iput-boolean v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->isFocus:Z

    iput-boolean v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->isInList:Z

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/adapter/EventListAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public addItem(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)V
    .locals 1
    .param p1    # Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget-object v0, p0, Lcom/konka/epg/adapter/EventListAdapter;->m_EventData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public delAllItem()V
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/adapter/EventListAdapter;->m_EventData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/adapter/EventListAdapter;->m_EventData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/epg/adapter/EventListAdapter;->m_EventData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v6, 0x7f020016    # com.konka.epg.R.drawable.epg_event_play

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v2

    if-lt p1, v2, :cond_0

    :goto_0
    return-object v1

    :cond_0
    const/4 v0, 0x0

    if-nez p2, :cond_1

    new-instance v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;

    invoke-direct {v0, p0, v1}, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;-><init>(Lcom/konka/epg/adapter/EventListAdapter;Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;)V

    iget-object v2, p0, Lcom/konka/epg/adapter/EventListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030004    # com.konka.epg.R.layout.epg_mainmenu_event_listitem

    invoke-virtual {v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0a0039    # com.konka.epg.R.id.epg_eventlist_name_text

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->eventName:Landroid/widget/TextView;

    const v1, 0x7f0a003a    # com.konka.epg.R.id.epg_eventlist_bookhint_img

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->bookHint:Landroid/widget/ImageView;

    const v1, 0x7f0a003b    # com.konka.epg.R.id.epg_eventlist_playhint_img

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    iget-object v2, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->eventName:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->m_EventData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget v1, v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    invoke-static {v1}, Lcom/konka/epg/util/EpgFormat;->formatTimeText(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\t"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->m_EventData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget-object v1, v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->bookHint:Landroid/widget/ImageView;

    const v2, 0x7f020012    # com.konka.epg.R.drawable.epg_clock

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->m_EventData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget-boolean v1, v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->bookHint:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    if-nez p1, :cond_4

    iget-boolean v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->isPlaying:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->isFocus:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/konka/epg/adapter/EventListAdapter;->isInList:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    const v2, 0x7f020017    # com.konka.epg.R.drawable.epg_event_play1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->eventName:Landroid/widget/TextView;

    const v2, -0xff8601

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_3
    move-object v1, p2

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->bookHint:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_3
    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->eventName:Landroid/widget/TextView;

    const v2, -0x87e2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    :cond_4
    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/konka/epg/adapter/EventListAdapter$ViewHolder;->eventName:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3
.end method

.method public setFocus(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/epg/adapter/EventListAdapter;->isFocus:Z

    return-void
.end method

.method public setIsInList(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/epg/adapter/EventListAdapter;->isInList:Z

    return-void
.end method

.method public setPlay()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/epg/adapter/EventListAdapter;->isPlaying:Z

    return-void
.end method
