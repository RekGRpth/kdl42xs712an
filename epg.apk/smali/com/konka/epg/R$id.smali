.class public final Lcom/konka/epg/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final FIFTH_DAY_IMG:I = 0x7f0a0026

.field public static final FIFTH_DAY_LAYOUT:I = 0x7f0a0024

.field public static final FIFTH_DAY_TEXT:I = 0x7f0a0025

.field public static final FIRST_DAY_IMG:I = 0x7f0a001a

.field public static final FIRST_DAY_LAYOUT:I = 0x7f0a0018

.field public static final FIRST_DAY_TEXT:I = 0x7f0a0019

.field public static final FOURTH_DAY_IMG:I = 0x7f0a0023

.field public static final FOURTH_DAY_LAYOUT:I = 0x7f0a0021

.field public static final FOURTH_DAY_TEXT:I = 0x7f0a0022

.field public static final SECOND_DAY_IMG:I = 0x7f0a001d

.field public static final SECOND_DAY_LAYOUT:I = 0x7f0a001b

.field public static final SECOND_DAY_TEXT:I = 0x7f0a001c

.field public static final SEVENTH_DAY_IMG:I = 0x7f0a002c

.field public static final SEVENTH_DAY_LAYOUT:I = 0x7f0a002a

.field public static final SEVENTH_DAY_TEXT:I = 0x7f0a002b

.field public static final SEVEN_DAY_VIEW:I = 0x7f0a0017

.field public static final SIXTH_DAY_IMG:I = 0x7f0a0029

.field public static final SIXTH_DAY_LAYOUT:I = 0x7f0a0027

.field public static final SIXTH_DAY_TEXT:I = 0x7f0a0028

.field public static final THIRD_DAY_IMG:I = 0x7f0a0020

.field public static final THIRD_DAY_LAYOUT:I = 0x7f0a001e

.field public static final THIRD_DAY_TEXT:I = 0x7f0a001f

.field public static final epg_booklist_channelid_text:I = 0x7f0a0008

.field public static final epg_booklist_channelname_text:I = 0x7f0a0009

.field public static final epg_booklist_eventmode_text:I = 0x7f0a000a

.field public static final epg_booklist_eventname_text:I = 0x7f0a000e

.field public static final epg_booklist_view_item_mode:I = 0x7f0a000b

.field public static final epg_booklist_view_item_reminder:I = 0x7f0a000c

.field public static final epg_booklist_view_item_video:I = 0x7f0a000d

.field public static final epg_bookmenu_booked_list:I = 0x7f0a0005

.field public static final epg_bookmenu_btm_text:I = 0x7f0a0006

.field public static final epg_bookmenu_btn_return:I = 0x7f0a0007

.field public static final epg_bookmenu_channelid_text:I = 0x7f0a0002

.field public static final epg_bookmenu_channelname_text:I = 0x7f0a0003

.field public static final epg_bookmenu_eventname_text:I = 0x7f0a0004

.field public static final epg_bookmenu_time_text:I = 0x7f0a0001

.field public static final epg_bookmenu_title_text:I = 0x7f0a0000

.field public static final epg_channellist_layout:I = 0x7f0a0036

.field public static final epg_channellist_name_text:I = 0x7f0a0037

.field public static final epg_channellist_playhint_img:I = 0x7f0a0038

.field public static final epg_eventlist_bookhint_img:I = 0x7f0a003a

.field public static final epg_eventlist_name_text:I = 0x7f0a0039

.field public static final epg_eventlist_playhint_img:I = 0x7f0a003b

.field public static final epg_main_menu:I = 0x7f0a000f

.field public static final epg_mainmenu_appname_text:I = 0x7f0a0010

.field public static final epg_mainmenu_bookevent_text:I = 0x7f0a0033

.field public static final epg_mainmenu_bookmanager_text:I = 0x7f0a002e

.field public static final epg_mainmenu_channel_list:I = 0x7f0a0013

.field public static final epg_mainmenu_epgtype_txt:I = 0x7f0a0016

.field public static final epg_mainmenu_epgtype_view:I = 0x7f0a0015

.field public static final epg_mainmenu_event_list:I = 0x7f0a002d

.field public static final epg_mainmenu_eventinfo_text:I = 0x7f0a0034

.field public static final epg_mainmenu_record_text:I = 0x7f0a002f

.field public static final epg_mainmenu_source_text:I = 0x7f0a0012

.field public static final epg_mainmenu_surface_view:I = 0x7f0a0014

.field public static final epg_mainmenu_switchlist_left_arrow:I = 0x7f0a0030

.field public static final epg_mainmenu_switchlist_right_arrow:I = 0x7f0a0031

.field public static final epg_mainmenu_switchlist_text:I = 0x7f0a0032

.field public static final epg_mainmenu_time_text:I = 0x7f0a0011

.field public static final epg_mainmenu_versioninfo_text:I = 0x7f0a0035

.field public static final epg_popwin_cancel_btn:I = 0x7f0a003e

.field public static final epg_popwin_confrim_btn:I = 0x7f0a003d

.field public static final epg_popwin_eventinfo_content:I = 0x7f0a0042

.field public static final epg_popwin_eventinfo_title:I = 0x7f0a003f

.field public static final epg_popwin_evetinfo_eventtitle:I = 0x7f0a0040

.field public static final epg_popwin_search_edit:I = 0x7f0a0043

.field public static final epg_popwin_search_list:I = 0x7f0a0044

.field public static final popwin_del_title:I = 0x7f0a003c

.field public static final record_channel:I = 0x7f0a0047

.field public static final record_channel_value:I = 0x7f0a0048

.field public static final record_end_date:I = 0x7f0a005f

.field public static final record_end_date_value:I = 0x7f0a0060

.field public static final record_end_hour:I = 0x7f0a0059

.field public static final record_end_hour_value:I = 0x7f0a005a

.field public static final record_end_minute:I = 0x7f0a0056

.field public static final record_end_minute_value:I = 0x7f0a0057

.field public static final record_end_month:I = 0x7f0a005c

.field public static final record_end_month_value:I = 0x7f0a005d

.field public static final record_end_time_date:I = 0x7f0a005e

.field public static final record_end_time_hour:I = 0x7f0a0058

.field public static final record_end_time_minute:I = 0x7f0a0055

.field public static final record_end_time_month:I = 0x7f0a005b

.field public static final record_mode_value:I = 0x7f0a0062

.field public static final record_mode_value_layout:I = 0x7f0a0061

.field public static final record_service_name:I = 0x7f0a0046

.field public static final record_start_date:I = 0x7f0a0053

.field public static final record_start_date_value:I = 0x7f0a0054

.field public static final record_start_hour:I = 0x7f0a004d

.field public static final record_start_hour_value:I = 0x7f0a004e

.field public static final record_start_minute:I = 0x7f0a004a

.field public static final record_start_minute_value:I = 0x7f0a004b

.field public static final record_start_month:I = 0x7f0a0050

.field public static final record_start_month_value:I = 0x7f0a0051

.field public static final record_start_time_date:I = 0x7f0a0052

.field public static final record_start_time_hour:I = 0x7f0a004c

.field public static final record_start_time_minute:I = 0x7f0a0049

.field public static final record_start_time_month:I = 0x7f0a004f

.field public static final reminder_channel:I = 0x7f0a0066

.field public static final reminder_channel_layout:I = 0x7f0a0065

.field public static final reminder_channel_value:I = 0x7f0a0067

.field public static final reminder_date:I = 0x7f0a0072

.field public static final reminder_date_layout:I = 0x7f0a0071

.field public static final reminder_date_value:I = 0x7f0a0073

.field public static final reminder_hint_back:I = 0x7f0a0064

.field public static final reminder_hint_ok:I = 0x7f0a0063

.field public static final reminder_hour:I = 0x7f0a006c

.field public static final reminder_hour_layout:I = 0x7f0a006b

.field public static final reminder_hour_value:I = 0x7f0a006d

.field public static final reminder_minute:I = 0x7f0a0069

.field public static final reminder_minute_layout:I = 0x7f0a0068

.field public static final reminder_minute_value:I = 0x7f0a006a

.field public static final reminder_mode_layout:I = 0x7f0a0074

.field public static final reminder_mode_value:I = 0x7f0a0075

.field public static final reminder_month:I = 0x7f0a006f

.field public static final reminder_month_layout:I = 0x7f0a006e

.field public static final reminder_month_value:I = 0x7f0a0070

.field public static final scrollView1:I = 0x7f0a0041

.field public static final text1:I = 0x7f0a0045


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
