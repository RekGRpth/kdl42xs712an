.class public abstract Lcom/jme3/animation/CompactArray;
.super Ljava/lang/Object;
.source "CompactArray.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected array:[F

.field protected index:[I

.field private indexPool:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private invalid:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/animation/CompactArray;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/animation/CompactArray;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/animation/CompactArray;->indexPool:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public varargs add([Ljava/lang/Object;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    array-length v5, p1

    if-nez v5, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/jme3/animation/CompactArray;->invalid:Z

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->index:[I

    if-nez v5, :cond_2

    array-length v5, p1

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/jme3/animation/CompactArray;->index:[I

    :goto_0
    const/4 v2, 0x0

    :goto_1
    array-length v5, p1

    if-ge v2, v5, :cond_0

    aget-object v3, p1, v2

    if-nez v3, :cond_4

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->index:[I

    add-int v6, v0, v2

    const/4 v7, -0x1

    aput v7, v5, v6

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->indexPool:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Internal is already fixed"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_3
    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->index:[I

    array-length v0, v5

    array-length v5, p1

    add-int/2addr v5, v0

    new-array v4, v5, [I

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->index:[I

    iget-object v6, p0, Lcom/jme3/animation/CompactArray;->index:[I

    array-length v6, v6

    invoke-static {v5, v7, v4, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v4, p0, Lcom/jme3/animation/CompactArray;->index:[I

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->indexPool:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_5

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->indexPool:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->indexPool:Ljava/util/Map;

    invoke-interface {v5, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->index:[I

    add-int v6, v0, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v5, v6

    goto :goto_2
.end method

.method protected abstract deserialize(ILjava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)TT;"
        }
    .end annotation
.end method

.method protected ensureCapacity([FI)[F
    .locals 3
    .param p1    # [F
    .param p2    # I

    const/4 v2, 0x0

    if-nez p1, :cond_1

    new-array p1, p2, [F

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    array-length v1, p1

    if-ge v1, p2, :cond_0

    new-array v0, p2, [F

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p1, v0

    goto :goto_0
.end method

.method public freeze()V
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->serialize()V

    iget-object v0, p0, Lcom/jme3/animation/CompactArray;->indexPool:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public final get(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->serialize()V

    invoke-virtual {p0, p1}, Lcom/jme3/animation/CompactArray;->getCompactIndex(I)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/jme3/animation/CompactArray;->deserialize(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public getCompactIndex(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jme3/animation/CompactArray;->index:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/animation/CompactArray;->index:[I

    aget p1, v0, p1

    :cond_0
    return p1
.end method

.method protected abstract getElementClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end method

.method public final getSerializedData()[F
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->serialize()V

    iget-object v0, p0, Lcom/jme3/animation/CompactArray;->array:[F

    return-object v0
.end method

.method protected final getSerializedSize()I
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getSerializedData()[F

    move-result-object v0

    invoke-static {v0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getTotalObjectSize()I
    .locals 2

    sget-boolean v0, Lcom/jme3/animation/CompactArray;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getSerializedSize()I

    move-result v0

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getTupleSize()I

    move-result v1

    rem-int/2addr v0, v1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/animation/CompactArray;->index:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jme3/animation/CompactArray;->index:[I

    array-length v0, v0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getSerializedSize()I

    move-result v0

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getTupleSize()I

    move-result v1

    div-int/2addr v0, v1

    goto :goto_0
.end method

.method protected abstract getTupleSize()I
.end method

.method public final serialize()V
    .locals 7

    iget-boolean v5, p0, Lcom/jme3/animation/CompactArray;->invalid:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->indexPool:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getTupleSize()I

    move-result v6

    mul-int v3, v5, v6

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->array:[F

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->array:[F

    invoke-static {v5}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v5

    if-ge v5, v3, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->array:[F

    invoke-virtual {p0, v5, v3}, Lcom/jme3/animation/CompactArray;->ensureCapacity([FI)[F

    move-result-object v5

    iput-object v5, p0, Lcom/jme3/animation/CompactArray;->array:[F

    iget-object v5, p0, Lcom/jme3/animation/CompactArray;->indexPool:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v1, v4}, Lcom/jme3/animation/CompactArray;->serialize(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/jme3/animation/CompactArray;->invalid:Z

    :cond_2
    return-void
.end method

.method protected abstract serialize(ILjava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;)V"
        }
    .end annotation
.end method

.method public final toObjectArray()[Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[TT;"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getElementClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getSerializedSize()I

    move-result v7

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getTupleSize()I

    move-result v8

    div-int/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    move-object v0, v6

    check-cast v0, [Ljava/lang/Object;

    move-object v1, v0

    const/4 v4, 0x0

    :goto_0
    array-length v6, v1

    if-ge v4, v6, :cond_0

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getElementClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v1, v4

    aget-object v6, v1, v4

    invoke-virtual {p0, v4, v6}, Lcom/jme3/animation/CompactArray;->deserialize(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getElementClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {p0}, Lcom/jme3/animation/CompactArray;->getTotalObjectSize()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    move-object v0, v6

    check-cast v0, [Ljava/lang/Object;

    move-object v5, v0

    const/4 v4, 0x0

    :goto_1
    array-length v6, v5

    if-ge v4, v6, :cond_1

    invoke-virtual {p0, v4}, Lcom/jme3/animation/CompactArray;->getCompactIndex(I)I

    move-result v2

    aget-object v6, v1, v2

    aput-object v6, v5, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v3

    const/4 v5, 0x0

    :cond_1
    return-object v5
.end method
