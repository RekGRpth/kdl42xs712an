.class public Lcom/jme3/asset/ImplHandler;
.super Ljava/lang/Object;
.source "ImplHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/asset/ImplHandler$ImplThreadLocal;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private final genericLocators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/asset/ImplHandler$ImplThreadLocal;",
            ">;"
        }
    .end annotation
.end field

.field private final loaders:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/jme3/asset/ImplHandler$ImplThreadLocal;",
            ">;"
        }
    .end annotation
.end field

.field private final owner:Lcom/jme3/asset/AssetManager;

.field private final parentAssetKey:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lcom/jme3/asset/AssetKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/asset/ImplHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/asset/ImplHandler;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/jme3/asset/AssetManager;)V
    .locals 1
    .param p1    # Lcom/jme3/asset/AssetManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/jme3/asset/ImplHandler;->parentAssetKey:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/asset/ImplHandler;->genericLocators:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/asset/ImplHandler;->loaders:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/jme3/asset/ImplHandler;->owner:Lcom/jme3/asset/AssetManager;

    return-void
.end method

.method static synthetic access$000()Ljava/util/logging/Logger;
    .locals 1

    sget-object v0, Lcom/jme3/asset/ImplHandler;->logger:Ljava/util/logging/Logger;

    return-object v0
.end method


# virtual methods
.method public varargs addLoader(Ljava/lang/Class;[Ljava/lang/String;)V
    .locals 7
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    new-instance v4, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;

    invoke-direct {v4, p0, p1}, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;-><init>(Lcom/jme3/asset/ImplHandler;Ljava/lang/Class;)V

    move-object v0, p2

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/jme3/asset/ImplHandler;->loaders:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/jme3/asset/ImplHandler;->loaders:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_0
    return-void
.end method

.method public addLocator(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;

    invoke-direct {v0, p0, p1, p2}, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;-><init>(Lcom/jme3/asset/ImplHandler;Ljava/lang/Class;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jme3/asset/ImplHandler;->genericLocators:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/jme3/asset/ImplHandler;->genericLocators:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public aquireLoader(Lcom/jme3/asset/AssetKey;)Lcom/jme3/asset/AssetLoader;
    .locals 5
    .param p1    # Lcom/jme3/asset/AssetKey;

    iget-object v3, p0, Lcom/jme3/asset/ImplHandler;->loaders:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/jme3/asset/ImplHandler;->loaders:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/jme3/asset/AssetKey;->getExtension()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/asset/AssetLoader;

    monitor-exit v3

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public establishParentKey(Lcom/jme3/asset/AssetKey;)V
    .locals 1
    .param p1    # Lcom/jme3/asset/AssetKey;

    iget-object v0, p0, Lcom/jme3/asset/ImplHandler;->parentAssetKey:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jme3/asset/ImplHandler;->parentAssetKey:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public getLocatorCount()I
    .locals 2

    iget-object v1, p0, Lcom/jme3/asset/ImplHandler;->genericLocators:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/jme3/asset/ImplHandler;->genericLocators:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getParentKey()Lcom/jme3/asset/AssetKey;
    .locals 1

    iget-object v0, p0, Lcom/jme3/asset/ImplHandler;->parentAssetKey:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/asset/AssetKey;

    return-object v0
.end method

.method public releaseParentKey(Lcom/jme3/asset/AssetKey;)V
    .locals 2
    .param p1    # Lcom/jme3/asset/AssetKey;

    iget-object v0, p0, Lcom/jme3/asset/ImplHandler;->parentAssetKey:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/jme3/asset/ImplHandler;->parentAssetKey:Ljava/lang/ThreadLocal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public tryLocate(Lcom/jme3/asset/AssetKey;)Lcom/jme3/asset/AssetInfo;
    .locals 7
    .param p1    # Lcom/jme3/asset/AssetKey;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jme3/asset/ImplHandler;->genericLocators:Ljava/util/ArrayList;

    monitor-enter v5

    :try_start_0
    iget-object v6, p0, Lcom/jme3/asset/ImplHandler;->genericLocators:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    monitor-exit v5

    move-object v1, v4

    :goto_0
    return-object v1

    :cond_0
    iget-object v6, p0, Lcom/jme3/asset/ImplHandler;->genericLocators:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;

    invoke-virtual {v2}, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jme3/asset/AssetLocator;

    invoke-virtual {v2}, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;->getPath()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/jme3/asset/ImplHandler$ImplThreadLocal;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Lcom/jme3/asset/AssetLocator;->setRootPath(Ljava/lang/String;)V

    :cond_2
    iget-object v6, p0, Lcom/jme3/asset/ImplHandler;->owner:Lcom/jme3/asset/AssetManager;

    invoke-interface {v3, v6, p1}, Lcom/jme3/asset/AssetLocator;->locate(Lcom/jme3/asset/AssetManager;Lcom/jme3/asset/AssetKey;)Lcom/jme3/asset/AssetInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_3
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v4

    goto :goto_0
.end method
