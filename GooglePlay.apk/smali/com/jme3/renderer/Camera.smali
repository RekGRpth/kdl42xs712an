.class public Lcom/jme3/renderer/Camera;
.super Ljava/lang/Object;
.source "Camera.java"

# interfaces
.implements Lcom/jme3/export/Savable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/renderer/Camera$FrustumIntersect;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected coeffBottom:[F

.field protected coeffLeft:[F

.field protected coeffRight:[F

.field protected coeffTop:[F

.field protected frustumBottom:F

.field protected frustumFar:F

.field protected frustumLeft:F

.field protected frustumNear:F

.field protected frustumRight:F

.field protected frustumTop:F

.field private guiBounding:Lcom/jme3/bounding/BoundingBox;

.field protected height:I

.field protected location:Lcom/jme3/math/Vector3f;

.field protected name:Ljava/lang/String;

.field private parallelProjection:Z

.field private planeState:I

.field protected projectionMatrix:Lcom/jme3/math/Matrix4f;

.field protected projectionMatrixOverride:Lcom/jme3/math/Matrix4f;

.field protected rotation:Lcom/jme3/math/Quaternion;

.field protected viewMatrix:Lcom/jme3/math/Matrix4f;

.field protected viewPortBottom:F

.field protected viewPortLeft:F

.field protected viewPortRight:F

.field protected viewPortTop:F

.field protected viewProjectionMatrix:Lcom/jme3/math/Matrix4f;

.field protected viewportChanged:Z

.field protected width:I

.field protected worldPlane:[Lcom/jme3/math/Plane;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/renderer/Camera;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/renderer/Camera;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jme3/renderer/Camera;->viewportChanged:Z

    new-instance v1, Lcom/jme3/math/Matrix4f;

    invoke-direct {v1}, Lcom/jme3/math/Matrix4f;-><init>()V

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->viewMatrix:Lcom/jme3/math/Matrix4f;

    new-instance v1, Lcom/jme3/math/Matrix4f;

    invoke-direct {v1}, Lcom/jme3/math/Matrix4f;-><init>()V

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->projectionMatrix:Lcom/jme3/math/Matrix4f;

    new-instance v1, Lcom/jme3/math/Matrix4f;

    invoke-direct {v1}, Lcom/jme3/math/Matrix4f;-><init>()V

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->viewProjectionMatrix:Lcom/jme3/math/Matrix4f;

    new-instance v1, Lcom/jme3/bounding/BoundingBox;

    invoke-direct {v1}, Lcom/jme3/bounding/BoundingBox;-><init>()V

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->guiBounding:Lcom/jme3/bounding/BoundingBox;

    new-array v1, v3, [Lcom/jme3/math/Plane;

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    new-instance v2, Lcom/jme3/math/Plane;

    invoke-direct {v2}, Lcom/jme3/math/Plane;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    const/high16 v2, -0x41000000    # -0.5f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, 0x2

    invoke-direct {p0}, Lcom/jme3/renderer/Camera;-><init>()V

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Quaternion;

    invoke-direct {v0}, Lcom/jme3/math/Quaternion;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    iput v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/jme3/renderer/Camera;->frustumFar:F

    iput v2, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    iput v5, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    iput v5, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    iput v2, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    new-array v0, v3, [F

    iput-object v0, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    iput v4, p0, Lcom/jme3/renderer/Camera;->viewPortLeft:F

    iput v1, p0, Lcom/jme3/renderer/Camera;->viewPortRight:F

    iput v1, p0, Lcom/jme3/renderer/Camera;->viewPortTop:F

    iput v4, p0, Lcom/jme3/renderer/Camera;->viewPortBottom:F

    iput p1, p0, Lcom/jme3/renderer/Camera;->width:I

    iput p2, p0, Lcom/jme3/renderer/Camera;->height:I

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrustumChange()V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onViewPortChange()V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrameChange()V

    sget-object v0, Lcom/jme3/renderer/Camera;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Camera created (W: {0}, H: {1})"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private setGuiBounding()V
    .locals 11

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v10, 0x0

    iget v6, p0, Lcom/jme3/renderer/Camera;->width:I

    int-to-float v6, v6

    iget v7, p0, Lcom/jme3/renderer/Camera;->viewPortLeft:F

    mul-float v2, v6, v7

    iget v6, p0, Lcom/jme3/renderer/Camera;->width:I

    int-to-float v6, v6

    iget v7, p0, Lcom/jme3/renderer/Camera;->viewPortRight:F

    mul-float v0, v6, v7

    iget v6, p0, Lcom/jme3/renderer/Camera;->height:I

    int-to-float v6, v6

    iget v7, p0, Lcom/jme3/renderer/Camera;->viewPortBottom:F

    mul-float v3, v6, v7

    iget v6, p0, Lcom/jme3/renderer/Camera;->height:I

    int-to-float v6, v6

    iget v7, p0, Lcom/jme3/renderer/Camera;->viewPortTop:F

    mul-float v1, v6, v7

    sub-float v6, v0, v2

    div-float/2addr v6, v8

    invoke-static {v10, v6}, Ljava/lang/Math;->max(FF)F

    move-result v4

    sub-float v6, v1, v3

    div-float/2addr v6, v8

    invoke-static {v10, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iget-object v6, p0, Lcom/jme3/renderer/Camera;->guiBounding:Lcom/jme3/bounding/BoundingBox;

    new-instance v7, Lcom/jme3/math/Vector3f;

    add-float v8, v2, v4

    add-float v9, v3, v5

    invoke-direct {v7, v8, v9, v10}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    invoke-virtual {v6, v7}, Lcom/jme3/bounding/BoundingBox;->setCenter(Lcom/jme3/math/Vector3f;)V

    iget-object v6, p0, Lcom/jme3/renderer/Camera;->guiBounding:Lcom/jme3/bounding/BoundingBox;

    invoke-virtual {v6, v4}, Lcom/jme3/bounding/BoundingBox;->setXExtent(F)V

    iget-object v6, p0, Lcom/jme3/renderer/Camera;->guiBounding:Lcom/jme3/bounding/BoundingBox;

    invoke-virtual {v6, v5}, Lcom/jme3/bounding/BoundingBox;->setYExtent(F)V

    iget-object v6, p0, Lcom/jme3/renderer/Camera;->guiBounding:Lcom/jme3/bounding/BoundingBox;

    const v7, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {v6, v7}, Lcom/jme3/bounding/BoundingBox;->setZExtent(F)V

    return-void
.end method


# virtual methods
.method public clearViewportChanged()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/renderer/Camera;->viewportChanged:Z

    return-void
.end method

.method public clone()Lcom/jme3/renderer/Camera;
    .locals 5

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/renderer/Camera;

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/jme3/renderer/Camera;->viewportChanged:Z

    const/4 v3, 0x0

    iput v3, v0, Lcom/jme3/renderer/Camera;->planeState:I

    const/4 v3, 0x6

    new-array v3, v3, [Lcom/jme3/math/Plane;

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v3, v0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    iget-object v4, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Lcom/jme3/math/Plane;->clone()Lcom/jme3/math/Plane;

    move-result-object v4

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x2

    new-array v3, v3, [F

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    const/4 v3, 0x2

    new-array v3, v3, [F

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    const/4 v3, 0x2

    new-array v3, v3, [F

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    const/4 v3, 0x2

    new-array v3, v3, [F

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    iget-object v3, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v3}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v3

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    iget-object v3, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    invoke-virtual {v3}, Lcom/jme3/math/Quaternion;->clone()Lcom/jme3/math/Quaternion;

    move-result-object v3

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    iget-object v3, p0, Lcom/jme3/renderer/Camera;->projectionMatrixOverride:Lcom/jme3/math/Matrix4f;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jme3/renderer/Camera;->projectionMatrixOverride:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v3}, Lcom/jme3/math/Matrix4f;->clone()Lcom/jme3/math/Matrix4f;

    move-result-object v3

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->projectionMatrixOverride:Lcom/jme3/math/Matrix4f;

    :cond_1
    iget-object v3, p0, Lcom/jme3/renderer/Camera;->viewMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v3}, Lcom/jme3/math/Matrix4f;->clone()Lcom/jme3/math/Matrix4f;

    move-result-object v3

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->viewMatrix:Lcom/jme3/math/Matrix4f;

    iget-object v3, p0, Lcom/jme3/renderer/Camera;->projectionMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v3}, Lcom/jme3/math/Matrix4f;->clone()Lcom/jme3/math/Matrix4f;

    move-result-object v3

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->projectionMatrix:Lcom/jme3/math/Matrix4f;

    iget-object v3, p0, Lcom/jme3/renderer/Camera;->viewProjectionMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v3}, Lcom/jme3/math/Matrix4f;->clone()Lcom/jme3/math/Matrix4f;

    move-result-object v3

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->viewProjectionMatrix:Lcom/jme3/math/Matrix4f;

    iget-object v3, p0, Lcom/jme3/renderer/Camera;->guiBounding:Lcom/jme3/bounding/BoundingBox;

    invoke-virtual {v3}, Lcom/jme3/bounding/BoundingBox;->clone()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v3

    check-cast v3, Lcom/jme3/bounding/BoundingBox;

    iput-object v3, v0, Lcom/jme3/renderer/Camera;->guiBounding:Lcom/jme3/bounding/BoundingBox;

    invoke-virtual {v0}, Lcom/jme3/renderer/Camera;->update()V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->clone()Lcom/jme3/renderer/Camera;

    move-result-object v0

    return-object v0
.end method

.method public contains(Lcom/jme3/bounding/BoundingVolume;)Lcom/jme3/renderer/Camera$FrustumIntersect;
    .locals 6
    .param p1    # Lcom/jme3/bounding/BoundingVolume;

    if-nez p1, :cond_1

    sget-object v3, Lcom/jme3/renderer/Camera$FrustumIntersect;->Inside:Lcom/jme3/renderer/Camera$FrustumIntersect;

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    sget-object v3, Lcom/jme3/renderer/Camera$FrustumIntersect;->Inside:Lcom/jme3/renderer/Camera$FrustumIntersect;

    const/4 v1, 0x6

    :goto_1
    if-ltz v1, :cond_0

    invoke-virtual {p1}, Lcom/jme3/bounding/BoundingVolume;->getCheckPlane()I

    move-result v5

    if-ne v1, v5, :cond_3

    :cond_2
    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    const/4 v5, 0x6

    if-ne v1, v5, :cond_4

    invoke-virtual {p1}, Lcom/jme3/bounding/BoundingVolume;->getCheckPlane()I

    move-result v2

    :goto_3
    const/4 v5, 0x1

    shl-int v0, v5, v2

    iget v5, p0, Lcom/jme3/renderer/Camera;->planeState:I

    and-int/2addr v5, v0

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    aget-object v5, v5, v2

    invoke-virtual {p1, v5}, Lcom/jme3/bounding/BoundingVolume;->whichSide(Lcom/jme3/math/Plane;)Lcom/jme3/math/Plane$Side;

    move-result-object v4

    sget-object v5, Lcom/jme3/math/Plane$Side;->Negative:Lcom/jme3/math/Plane$Side;

    if-ne v4, v5, :cond_5

    invoke-virtual {p1, v2}, Lcom/jme3/bounding/BoundingVolume;->setCheckPlane(I)V

    sget-object v3, Lcom/jme3/renderer/Camera$FrustumIntersect;->Outside:Lcom/jme3/renderer/Camera$FrustumIntersect;

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_3

    :cond_5
    sget-object v5, Lcom/jme3/math/Plane$Side;->Positive:Lcom/jme3/math/Plane$Side;

    if-ne v4, v5, :cond_6

    iget v5, p0, Lcom/jme3/renderer/Camera;->planeState:I

    or-int/2addr v5, v0

    iput v5, p0, Lcom/jme3/renderer/Camera;->planeState:I

    goto :goto_2

    :cond_6
    sget-object v3, Lcom/jme3/renderer/Camera$FrustumIntersect;->Intersects:Lcom/jme3/renderer/Camera$FrustumIntersect;

    goto :goto_2
.end method

.method public containsGui(Lcom/jme3/bounding/BoundingVolume;)Z
    .locals 1
    .param p1    # Lcom/jme3/bounding/BoundingVolume;

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->guiBounding:Lcom/jme3/bounding/BoundingBox;

    invoke-virtual {v0, p1}, Lcom/jme3/bounding/BoundingBox;->intersects(Lcom/jme3/bounding/BoundingVolume;)Z

    move-result v0

    return v0
.end method

.method public getDirection()Lcom/jme3/math/Vector3f;
    .locals 2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/jme3/math/Quaternion;->getRotationColumn(I)Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public getDirection(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/jme3/math/Quaternion;->getRotationColumn(ILcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public getFrustumFar()F
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumFar:F

    return v0
.end method

.method public getFrustumNear()F
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->height:I

    return v0
.end method

.method public getLeft(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/jme3/math/Quaternion;->getRotationColumn(ILcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()Lcom/jme3/math/Vector3f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    return-object v0
.end method

.method public getPlaneState()I
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->planeState:I

    return v0
.end method

.method public getProjectionMatrix()Lcom/jme3/math/Matrix4f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->projectionMatrixOverride:Lcom/jme3/math/Matrix4f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->projectionMatrixOverride:Lcom/jme3/math/Matrix4f;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/renderer/Camera;->projectionMatrix:Lcom/jme3/math/Matrix4f;

    goto :goto_0
.end method

.method public getUp(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;
    .locals 2
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/jme3/math/Quaternion;->getRotationColumn(ILcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v0

    return-object v0
.end method

.method public getViewMatrix()Lcom/jme3/math/Matrix4f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->viewMatrix:Lcom/jme3/math/Matrix4f;

    return-object v0
.end method

.method public getViewPortBottom()F
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->viewPortBottom:F

    return v0
.end method

.method public getViewPortLeft()F
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->viewPortLeft:F

    return v0
.end method

.method public getViewPortRight()F
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->viewPortRight:F

    return v0
.end method

.method public getViewPortTop()F
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->viewPortTop:F

    return v0
.end method

.method public getViewProjectionMatrix()Lcom/jme3/math/Matrix4f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->viewProjectionMatrix:Lcom/jme3/math/Matrix4f;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/jme3/renderer/Camera;->width:I

    return v0
.end method

.method public isParallelProjection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/renderer/Camera;->parallelProjection:Z

    return v0
.end method

.method public isViewportChanged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/renderer/Camera;->viewportChanged:Z

    return v0
.end method

.method public lookAt(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 7
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    const/4 v6, 0x0

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v3

    iget-object v0, v3, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    iget-object v2, v3, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    iget-object v1, v3, Lcom/jme3/util/TempVars;->vect3:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v4

    iget-object v5, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4, v5}, Lcom/jme3/math/Vector3f;->subtractLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->normalizeLocal()Lcom/jme3/math/Vector3f;

    invoke-virtual {v2, p2}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->normalizeLocal()Lcom/jme3/math/Vector3f;

    sget-object v4, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2, v4}, Lcom/jme3/math/Vector3f;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/jme3/math/Vector3f;->UNIT_Y:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2, v4}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    :cond_0
    invoke-virtual {v1, v2}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/jme3/math/Vector3f;->crossLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->normalizeLocal()Lcom/jme3/math/Vector3f;

    sget-object v4, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v4}, Lcom/jme3/math/Vector3f;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, v0, Lcom/jme3/math/Vector3f;->x:F

    cmpl-float v4, v4, v6

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/jme3/math/Vector3f;->y:F

    iget v5, v0, Lcom/jme3/math/Vector3f;->x:F

    neg-float v5, v5

    invoke-virtual {v1, v4, v5, v6}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    :cond_1
    :goto_0
    invoke-virtual {v2, v0}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/jme3/math/Vector3f;->crossLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jme3/math/Vector3f;->normalizeLocal()Lcom/jme3/math/Vector3f;

    iget-object v4, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    invoke-virtual {v4, v1, v2, v0}, Lcom/jme3/math/Quaternion;->fromAxes(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Quaternion;

    iget-object v4, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    invoke-virtual {v4}, Lcom/jme3/math/Quaternion;->normalizeLocal()V

    invoke-virtual {v3}, Lcom/jme3/util/TempVars;->release()V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrameChange()V

    return-void

    :cond_2
    iget v4, v0, Lcom/jme3/math/Vector3f;->z:F

    iget v5, v0, Lcom/jme3/math/Vector3f;->y:F

    neg-float v5, v5

    invoke-virtual {v1, v6, v4, v5}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    goto :goto_0
.end method

.method public onFrameChange()V
    .locals 14

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v8

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    invoke-virtual {p0, v9}, Lcom/jme3/renderer/Camera;->getLeft(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v3

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    invoke-virtual {p0, v9}, Lcom/jme3/renderer/Camera;->getDirection(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v2

    iget-object v9, v8, Lcom/jme3/util/TempVars;->vect3:Lcom/jme3/math/Vector3f;

    invoke-virtual {p0, v9}, Lcom/jme3/renderer/Camera;->getUp(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v7

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2, v9}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v1

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    invoke-virtual {v9}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v4

    iget v9, v3, Lcom/jme3/math/Vector3f;->x:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v4, Lcom/jme3/math/Vector3f;->x:F

    iget v9, v3, Lcom/jme3/math/Vector3f;->y:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v4, Lcom/jme3/math/Vector3f;->y:F

    iget v9, v3, Lcom/jme3/math/Vector3f;->z:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v4, Lcom/jme3/math/Vector3f;->z:F

    iget v9, v2, Lcom/jme3/math/Vector3f;->x:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iget v10, v2, Lcom/jme3/math/Vector3f;->y:F

    iget-object v11, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    mul-float/2addr v10, v11

    iget v11, v2, Lcom/jme3/math/Vector3f;->z:F

    iget-object v12, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    const/4 v13, 0x1

    aget v12, v12, v13

    mul-float/2addr v11, v12

    invoke-virtual {v4, v9, v10, v11}, Lcom/jme3/math/Vector3f;->addLocal(FFF)Lcom/jme3/math/Vector3f;

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v10, v4}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v10

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    invoke-virtual {v9}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v5

    iget v9, v3, Lcom/jme3/math/Vector3f;->x:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v5, Lcom/jme3/math/Vector3f;->x:F

    iget v9, v3, Lcom/jme3/math/Vector3f;->y:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v5, Lcom/jme3/math/Vector3f;->y:F

    iget v9, v3, Lcom/jme3/math/Vector3f;->z:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v5, Lcom/jme3/math/Vector3f;->z:F

    iget v9, v2, Lcom/jme3/math/Vector3f;->x:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iget v10, v2, Lcom/jme3/math/Vector3f;->y:F

    iget-object v11, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    mul-float/2addr v10, v11

    iget v11, v2, Lcom/jme3/math/Vector3f;->z:F

    iget-object v12, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    const/4 v13, 0x1

    aget v12, v12, v13

    mul-float/2addr v11, v12

    invoke-virtual {v5, v9, v10, v11}, Lcom/jme3/math/Vector3f;->addLocal(FFF)Lcom/jme3/math/Vector3f;

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v10, v5}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v10

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x2

    aget-object v9, v9, v10

    invoke-virtual {v9}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v0

    iget v9, v7, Lcom/jme3/math/Vector3f;->x:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v0, Lcom/jme3/math/Vector3f;->x:F

    iget v9, v7, Lcom/jme3/math/Vector3f;->y:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v0, Lcom/jme3/math/Vector3f;->y:F

    iget v9, v7, Lcom/jme3/math/Vector3f;->z:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v0, Lcom/jme3/math/Vector3f;->z:F

    iget v9, v2, Lcom/jme3/math/Vector3f;->x:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iget v10, v2, Lcom/jme3/math/Vector3f;->y:F

    iget-object v11, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    mul-float/2addr v10, v11

    iget v11, v2, Lcom/jme3/math/Vector3f;->z:F

    iget-object v12, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    const/4 v13, 0x1

    aget v12, v12, v13

    mul-float/2addr v11, v12

    invoke-virtual {v0, v9, v10, v11}, Lcom/jme3/math/Vector3f;->addLocal(FFF)Lcom/jme3/math/Vector3f;

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x2

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v10, v0}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v10

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x3

    aget-object v9, v9, v10

    invoke-virtual {v9}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v6

    iget v9, v7, Lcom/jme3/math/Vector3f;->x:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v6, Lcom/jme3/math/Vector3f;->x:F

    iget v9, v7, Lcom/jme3/math/Vector3f;->y:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v6, Lcom/jme3/math/Vector3f;->y:F

    iget v9, v7, Lcom/jme3/math/Vector3f;->z:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iput v9, v6, Lcom/jme3/math/Vector3f;->z:F

    iget v9, v2, Lcom/jme3/math/Vector3f;->x:F

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iget v10, v2, Lcom/jme3/math/Vector3f;->y:F

    iget-object v11, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    const/4 v12, 0x1

    aget v11, v11, v12

    mul-float/2addr v10, v11

    iget v11, v2, Lcom/jme3/math/Vector3f;->z:F

    iget-object v12, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    const/4 v13, 0x1

    aget v12, v12, v13

    mul-float/2addr v11, v12

    invoke-virtual {v6, v9, v10, v11}, Lcom/jme3/math/Vector3f;->addLocal(FFF)Lcom/jme3/math/Vector3f;

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x3

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v10, v6}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v10

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->isParallelProjection()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v11, 0x0

    aget-object v10, v10, v11

    invoke-virtual {v10}, Lcom/jme3/math/Plane;->getConstant()F

    move-result v10

    iget v11, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    add-float/2addr v10, v11

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x1

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v11, 0x1

    aget-object v10, v10, v11

    invoke-virtual {v10}, Lcom/jme3/math/Plane;->getConstant()F

    move-result v10

    iget v11, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    sub-float/2addr v10, v11

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x3

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v11, 0x3

    aget-object v10, v10, v11

    invoke-virtual {v10}, Lcom/jme3/math/Plane;->getConstant()F

    move-result v10

    iget v11, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    sub-float/2addr v10, v11

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x2

    aget-object v9, v9, v10

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v11, 0x2

    aget-object v10, v10, v11

    invoke-virtual {v10}, Lcom/jme3/math/Plane;->getConstant()F

    move-result v10

    iget v11, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    add-float/2addr v10, v11

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    :cond_0
    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x4

    aget-object v9, v9, v10

    invoke-virtual {v9, v3}, Lcom/jme3/math/Plane;->setNormal(Lcom/jme3/math/Vector3f;)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x4

    aget-object v9, v9, v10

    iget v10, v2, Lcom/jme3/math/Vector3f;->x:F

    neg-float v10, v10

    iget v11, v2, Lcom/jme3/math/Vector3f;->y:F

    neg-float v11, v11

    iget v12, v2, Lcom/jme3/math/Vector3f;->z:F

    neg-float v12, v12

    invoke-virtual {v9, v10, v11, v12}, Lcom/jme3/math/Plane;->setNormal(FFF)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x4

    aget-object v9, v9, v10

    iget v10, p0, Lcom/jme3/renderer/Camera;->frustumFar:F

    add-float/2addr v10, v1

    neg-float v10, v10

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x5

    aget-object v9, v9, v10

    iget v10, v2, Lcom/jme3/math/Vector3f;->x:F

    iget v11, v2, Lcom/jme3/math/Vector3f;->y:F

    iget v12, v2, Lcom/jme3/math/Vector3f;->z:F

    invoke-virtual {v9, v10, v11, v12}, Lcom/jme3/math/Plane;->setNormal(FFF)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->worldPlane:[Lcom/jme3/math/Plane;

    const/4 v10, 0x5

    aget-object v9, v9, v10

    iget v10, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    add-float/2addr v10, v1

    invoke-virtual {v9, v10}, Lcom/jme3/math/Plane;->setConstant(F)V

    iget-object v9, p0, Lcom/jme3/renderer/Camera;->viewMatrix:Lcom/jme3/math/Matrix4f;

    iget-object v10, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v9, v10, v2, v7, v3}, Lcom/jme3/math/Matrix4f;->fromFrame(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V

    invoke-virtual {v8}, Lcom/jme3/util/TempVars;->release()V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->updateViewProjection()V

    return-void
.end method

.method public onFrustumChange()V
    .locals 14

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->isParallelProjection()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    mul-float v11, v0, v1

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    mul-float v10, v0, v1

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    mul-float v12, v0, v1

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    mul-float v8, v0, v1

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    mul-float v13, v0, v1

    add-float v0, v11, v10

    invoke-static {v0}, Lcom/jme3/math/FastMath;->invSqrt(F)F

    move-result v9

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    mul-float/2addr v1, v9

    aput v1, v0, v2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    neg-float v1, v1

    mul-float/2addr v1, v9

    aput v1, v0, v3

    add-float v0, v11, v12

    invoke-static {v0}, Lcom/jme3/math/FastMath;->invSqrt(F)F

    move-result v9

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    neg-float v1, v1

    mul-float/2addr v1, v9

    aput v1, v0, v2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    mul-float/2addr v1, v9

    aput v1, v0, v3

    add-float v0, v11, v8

    invoke-static {v0}, Lcom/jme3/math/FastMath;->invSqrt(F)F

    move-result v9

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    mul-float/2addr v1, v9

    aput v1, v0, v2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    neg-float v1, v1

    mul-float/2addr v1, v9

    aput v1, v0, v3

    add-float v0, v11, v13

    invoke-static {v0}, Lcom/jme3/math/FastMath;->invSqrt(F)F

    move-result v9

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    neg-float v1, v1

    mul-float/2addr v1, v9

    aput v1, v0, v2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    mul-float/2addr v1, v9

    aput v1, v0, v3

    :goto_0
    iget-object v0, p0, Lcom/jme3/renderer/Camera;->projectionMatrix:Lcom/jme3/math/Matrix4f;

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    iget v2, p0, Lcom/jme3/renderer/Camera;->frustumFar:F

    iget v3, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    iget v4, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    iget v5, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    iget v6, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    iget-boolean v7, p0, Lcom/jme3/renderer/Camera;->parallelProjection:Z

    invoke-virtual/range {v0 .. v7}, Lcom/jme3/math/Matrix4f;->fromFrustum(FFFFFFZ)V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrameChange()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    aput v5, v0, v2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    aput v1, v0, v3

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    aput v4, v0, v2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    aput v1, v0, v3

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    aput v5, v0, v2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    aput v1, v0, v3

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    aput v4, v0, v2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    aput v1, v0, v3

    goto :goto_0
.end method

.method public onViewPortChange()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/renderer/Camera;->viewportChanged:Z

    invoke-direct {p0}, Lcom/jme3/renderer/Camera;->setGuiBounding()V

    return-void
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 8
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v6, 0x0

    const/high16 v5, -0x41000000    # -0.5f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x2

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "location"

    sget-object v2, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Vector3f;

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    const-string v1, "rotation"

    sget-object v2, Lcom/jme3/math/Quaternion;->DIRECTION_Z:Lcom/jme3/math/Quaternion;

    invoke-virtual {v2}, Lcom/jme3/math/Quaternion;->clone()Lcom/jme3/math/Quaternion;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Quaternion;

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->rotation:Lcom/jme3/math/Quaternion;

    const-string v1, "frustumNear"

    invoke-interface {v0, v1, v4}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    const-string v1, "frustumFar"

    const/high16 v2, 0x40000000    # 2.0f

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->frustumFar:F

    const-string v1, "frustumLeft"

    invoke-interface {v0, v1, v5}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    const-string v1, "frustumRight"

    invoke-interface {v0, v1, v7}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    const-string v1, "frustumTop"

    invoke-interface {v0, v1, v7}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    const-string v1, "frustumBottom"

    invoke-interface {v0, v1, v5}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    const-string v1, "coeffLeft"

    new-array v2, v3, [F

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloatArray(Ljava/lang/String;[F)[F

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->coeffLeft:[F

    const-string v1, "coeffRight"

    new-array v2, v3, [F

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloatArray(Ljava/lang/String;[F)[F

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->coeffRight:[F

    const-string v1, "coeffBottom"

    new-array v2, v3, [F

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloatArray(Ljava/lang/String;[F)[F

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->coeffBottom:[F

    const-string v1, "coeffTop"

    new-array v2, v3, [F

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readFloatArray(Ljava/lang/String;[F)[F

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->coeffTop:[F

    const-string v1, "viewPortLeft"

    invoke-interface {v0, v1, v6}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->viewPortLeft:F

    const-string v1, "viewPortRight"

    invoke-interface {v0, v1, v4}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->viewPortRight:F

    const-string v1, "viewPortTop"

    invoke-interface {v0, v1, v4}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->viewPortTop:F

    const-string v1, "viewPortBottom"

    invoke-interface {v0, v1, v6}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->viewPortBottom:F

    const-string v1, "width"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->width:I

    const-string v1, "height"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/jme3/renderer/Camera;->height:I

    const-string v1, "name"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/renderer/Camera;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrustumChange()V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onViewPortChange()V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrameChange()V

    return-void
.end method

.method public resize(IIZ)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    iput p1, p0, Lcom/jme3/renderer/Camera;->width:I

    iput p2, p0, Lcom/jme3/renderer/Camera;->height:I

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onViewPortChange()V

    if-eqz p3, :cond_0

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    int-to-float v1, p1

    int-to-float v2, p2

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    iget v0, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    neg-float v0, v0

    iput v0, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrustumChange()V

    :cond_0
    return-void
.end method

.method public setFrustum(FFFFFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F
    .param p6    # F

    iput p1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    iput p2, p0, Lcom/jme3/renderer/Camera;->frustumFar:F

    iput p3, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    iput p4, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    iput p5, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    iput p6, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrustumChange()V

    return-void
.end method

.method public setFrustumPerspective(FFFF)V
    .locals 6
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    sget-object v2, Lcom/jme3/renderer/Camera;->logger:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v4, "Invalid aspect given to setFrustumPerspective: {0}"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    const v2, 0x3c8efa35

    mul-float/2addr v2, p1

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    invoke-static {v2}, Lcom/jme3/math/FastMath;->tan(F)F

    move-result v2

    mul-float v0, v2, p3

    mul-float v1, v0, p2

    neg-float v2, v1

    iput v2, p0, Lcom/jme3/renderer/Camera;->frustumLeft:F

    iput v1, p0, Lcom/jme3/renderer/Camera;->frustumRight:F

    neg-float v2, v0

    iput v2, p0, Lcom/jme3/renderer/Camera;->frustumBottom:F

    iput v0, p0, Lcom/jme3/renderer/Camera;->frustumTop:F

    iput p3, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    iput p4, p0, Lcom/jme3/renderer/Camera;->frustumFar:F

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrustumChange()V

    goto :goto_0
.end method

.method public setLocation(Lcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrameChange()V

    return-void
.end method

.method public setParallelProjection(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/renderer/Camera;->parallelProjection:Z

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrustumChange()V

    return-void
.end method

.method public setPlaneState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jme3/renderer/Camera;->planeState:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Camera[location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/renderer/Camera;->location:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n, direction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->getDirection()Lcom/jme3/math/Vector3f;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/renderer/Camera;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/renderer/Camera;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parallel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jme3/renderer/Camera;->parallelProjection:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "near="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumNear:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", far="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jme3/renderer/Camera;->frustumFar:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update()V
    .locals 0

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrustumChange()V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onViewPortChange()V

    invoke-virtual {p0}, Lcom/jme3/renderer/Camera;->onFrameChange()V

    return-void
.end method

.method public updateViewProjection()V
    .locals 2

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->projectionMatrixOverride:Lcom/jme3/math/Matrix4f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/renderer/Camera;->viewProjectionMatrix:Lcom/jme3/math/Matrix4f;

    iget-object v1, p0, Lcom/jme3/renderer/Camera;->projectionMatrixOverride:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Matrix4f;->set(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/renderer/Camera;->viewMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Matrix4f;->multLocal(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jme3/renderer/Camera;->viewProjectionMatrix:Lcom/jme3/math/Matrix4f;

    iget-object v1, p0, Lcom/jme3/renderer/Camera;->projectionMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Matrix4f;->set(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/renderer/Camera;->viewMatrix:Lcom/jme3/math/Matrix4f;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Matrix4f;->multLocal(Lcom/jme3/math/Matrix4f;)Lcom/jme3/math/Matrix4f;

    goto :goto_0
.end method
