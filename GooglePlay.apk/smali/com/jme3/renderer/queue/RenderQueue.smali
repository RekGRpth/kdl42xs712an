.class public Lcom/jme3/renderer/queue/RenderQueue;
.super Ljava/lang/Object;
.source "RenderQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/renderer/queue/RenderQueue$1;,
        Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;,
        Lcom/jme3/renderer/queue/RenderQueue$Bucket;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private guiList:Lcom/jme3/renderer/queue/GeometryList;

.field private opaqueList:Lcom/jme3/renderer/queue/GeometryList;

.field private shadowCast:Lcom/jme3/renderer/queue/GeometryList;

.field private shadowRecv:Lcom/jme3/renderer/queue/GeometryList;

.field private skyList:Lcom/jme3/renderer/queue/GeometryList;

.field private translucentList:Lcom/jme3/renderer/queue/GeometryList;

.field private transparentList:Lcom/jme3/renderer/queue/GeometryList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/renderer/queue/RenderQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/renderer/queue/RenderQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/renderer/queue/GeometryList;

    new-instance v1, Lcom/jme3/renderer/queue/OpaqueComparator;

    invoke-direct {v1}, Lcom/jme3/renderer/queue/OpaqueComparator;-><init>()V

    invoke-direct {v0, v1}, Lcom/jme3/renderer/queue/GeometryList;-><init>(Lcom/jme3/renderer/queue/GeometryComparator;)V

    iput-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->opaqueList:Lcom/jme3/renderer/queue/GeometryList;

    new-instance v0, Lcom/jme3/renderer/queue/GeometryList;

    new-instance v1, Lcom/jme3/renderer/queue/GuiComparator;

    invoke-direct {v1}, Lcom/jme3/renderer/queue/GuiComparator;-><init>()V

    invoke-direct {v0, v1}, Lcom/jme3/renderer/queue/GeometryList;-><init>(Lcom/jme3/renderer/queue/GeometryComparator;)V

    iput-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->guiList:Lcom/jme3/renderer/queue/GeometryList;

    new-instance v0, Lcom/jme3/renderer/queue/GeometryList;

    new-instance v1, Lcom/jme3/renderer/queue/TransparentComparator;

    invoke-direct {v1}, Lcom/jme3/renderer/queue/TransparentComparator;-><init>()V

    invoke-direct {v0, v1}, Lcom/jme3/renderer/queue/GeometryList;-><init>(Lcom/jme3/renderer/queue/GeometryComparator;)V

    iput-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->transparentList:Lcom/jme3/renderer/queue/GeometryList;

    new-instance v0, Lcom/jme3/renderer/queue/GeometryList;

    new-instance v1, Lcom/jme3/renderer/queue/TransparentComparator;

    invoke-direct {v1}, Lcom/jme3/renderer/queue/TransparentComparator;-><init>()V

    invoke-direct {v0, v1}, Lcom/jme3/renderer/queue/GeometryList;-><init>(Lcom/jme3/renderer/queue/GeometryComparator;)V

    iput-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->translucentList:Lcom/jme3/renderer/queue/GeometryList;

    new-instance v0, Lcom/jme3/renderer/queue/GeometryList;

    new-instance v1, Lcom/jme3/renderer/queue/NullComparator;

    invoke-direct {v1}, Lcom/jme3/renderer/queue/NullComparator;-><init>()V

    invoke-direct {v0, v1}, Lcom/jme3/renderer/queue/GeometryList;-><init>(Lcom/jme3/renderer/queue/GeometryComparator;)V

    iput-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->skyList:Lcom/jme3/renderer/queue/GeometryList;

    new-instance v0, Lcom/jme3/renderer/queue/GeometryList;

    new-instance v1, Lcom/jme3/renderer/queue/OpaqueComparator;

    invoke-direct {v1}, Lcom/jme3/renderer/queue/OpaqueComparator;-><init>()V

    invoke-direct {v0, v1}, Lcom/jme3/renderer/queue/GeometryList;-><init>(Lcom/jme3/renderer/queue/GeometryComparator;)V

    iput-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->shadowRecv:Lcom/jme3/renderer/queue/GeometryList;

    new-instance v0, Lcom/jme3/renderer/queue/GeometryList;

    new-instance v1, Lcom/jme3/renderer/queue/OpaqueComparator;

    invoke-direct {v1}, Lcom/jme3/renderer/queue/OpaqueComparator;-><init>()V

    invoke-direct {v0, v1}, Lcom/jme3/renderer/queue/GeometryList;-><init>(Lcom/jme3/renderer/queue/GeometryComparator;)V

    iput-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->shadowCast:Lcom/jme3/renderer/queue/GeometryList;

    return-void
.end method

.method private renderGeometryList(Lcom/jme3/renderer/queue/GeometryList;Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/Camera;Z)V
    .locals 3
    .param p1    # Lcom/jme3/renderer/queue/GeometryList;
    .param p2    # Lcom/jme3/renderer/RenderManager;
    .param p3    # Lcom/jme3/renderer/Camera;
    .param p4    # Z

    invoke-virtual {p1, p3}, Lcom/jme3/renderer/queue/GeometryList;->setCamera(Lcom/jme3/renderer/Camera;)V

    invoke-virtual {p1}, Lcom/jme3/renderer/queue/GeometryList;->sort()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/jme3/renderer/queue/GeometryList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p1, v0}, Lcom/jme3/renderer/queue/GeometryList;->get(I)Lcom/jme3/scene/Geometry;

    move-result-object v1

    sget-boolean v2, Lcom/jme3/renderer/queue/RenderQueue;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-nez v1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_0
    invoke-virtual {p2, v1}, Lcom/jme3/renderer/RenderManager;->renderGeometry(Lcom/jme3/scene/Geometry;)V

    const/high16 v2, -0x800000    # Float.NEGATIVE_INFINITY

    iput v2, v1, Lcom/jme3/scene/Geometry;->queueDistance:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p1}, Lcom/jme3/renderer/queue/GeometryList;->clear()V

    :cond_2
    return-void
.end method


# virtual methods
.method public addToQueue(Lcom/jme3/scene/Geometry;Lcom/jme3/renderer/queue/RenderQueue$Bucket;)V
    .locals 3
    .param p1    # Lcom/jme3/scene/Geometry;
    .param p2    # Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    sget-object v0, Lcom/jme3/renderer/queue/RenderQueue$1;->$SwitchMap$com$jme3$renderer$queue$RenderQueue$Bucket:[I

    invoke-virtual {p2}, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown bucket type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->guiList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->opaqueList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->skyList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->transparentList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->translucentList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public addToShadowQueue(Lcom/jme3/scene/Geometry;Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;)V
    .locals 3
    .param p1    # Lcom/jme3/scene/Geometry;
    .param p2    # Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;

    sget-object v0, Lcom/jme3/renderer/queue/RenderQueue$1;->$SwitchMap$com$jme3$renderer$queue$RenderQueue$ShadowMode:[I

    invoke-virtual {p2}, Lcom/jme3/renderer/queue/RenderQueue$ShadowMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized shadow bucket type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->shadowCast:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    :goto_0
    :pswitch_1
    return-void

    :pswitch_2
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->shadowRecv:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->shadowCast:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->shadowRecv:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0, p1}, Lcom/jme3/renderer/queue/GeometryList;->add(Lcom/jme3/scene/Geometry;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->opaqueList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0}, Lcom/jme3/renderer/queue/GeometryList;->clear()V

    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->guiList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0}, Lcom/jme3/renderer/queue/GeometryList;->clear()V

    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->transparentList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0}, Lcom/jme3/renderer/queue/GeometryList;->clear()V

    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->translucentList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0}, Lcom/jme3/renderer/queue/GeometryList;->clear()V

    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->skyList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0}, Lcom/jme3/renderer/queue/GeometryList;->clear()V

    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->shadowCast:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0}, Lcom/jme3/renderer/queue/GeometryList;->clear()V

    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->shadowRecv:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v0}, Lcom/jme3/renderer/queue/GeometryList;->clear()V

    return-void
.end method

.method public isQueueEmpty(Lcom/jme3/renderer/queue/RenderQueue$Bucket;)Z
    .locals 4
    .param p1    # Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/jme3/renderer/queue/RenderQueue$1;->$SwitchMap$com$jme3$renderer$queue$RenderQueue$Bucket:[I

    invoke-virtual {p1}, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported bucket type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v2, p0, Lcom/jme3/renderer/queue/RenderQueue;->guiList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v2}, Lcom/jme3/renderer/queue/GeometryList;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/jme3/renderer/queue/RenderQueue;->opaqueList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v2}, Lcom/jme3/renderer/queue/GeometryList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/jme3/renderer/queue/RenderQueue;->skyList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v2}, Lcom/jme3/renderer/queue/GeometryList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/jme3/renderer/queue/RenderQueue;->transparentList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v2}, Lcom/jme3/renderer/queue/GeometryList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/jme3/renderer/queue/RenderQueue;->translucentList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-virtual {v2}, Lcom/jme3/renderer/queue/GeometryList;->size()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public renderQueue(Lcom/jme3/renderer/queue/RenderQueue$Bucket;Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/Camera;Z)V
    .locals 3
    .param p1    # Lcom/jme3/renderer/queue/RenderQueue$Bucket;
    .param p2    # Lcom/jme3/renderer/RenderManager;
    .param p3    # Lcom/jme3/renderer/Camera;
    .param p4    # Z

    sget-object v0, Lcom/jme3/renderer/queue/RenderQueue$1;->$SwitchMap$com$jme3$renderer$queue$RenderQueue$Bucket:[I

    invoke-virtual {p1}, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported bucket type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->guiList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/jme3/renderer/queue/RenderQueue;->renderGeometryList(Lcom/jme3/renderer/queue/GeometryList;Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/Camera;Z)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->opaqueList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/jme3/renderer/queue/RenderQueue;->renderGeometryList(Lcom/jme3/renderer/queue/GeometryList;Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/Camera;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->skyList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/jme3/renderer/queue/RenderQueue;->renderGeometryList(Lcom/jme3/renderer/queue/GeometryList;Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/Camera;Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->transparentList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/jme3/renderer/queue/RenderQueue;->renderGeometryList(Lcom/jme3/renderer/queue/GeometryList;Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/Camera;Z)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jme3/renderer/queue/RenderQueue;->translucentList:Lcom/jme3/renderer/queue/GeometryList;

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/jme3/renderer/queue/RenderQueue;->renderGeometryList(Lcom/jme3/renderer/queue/GeometryList;Lcom/jme3/renderer/RenderManager;Lcom/jme3/renderer/Camera;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
