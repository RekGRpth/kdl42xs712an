.class public Lcom/jme3/renderer/queue/OpaqueComparator;
.super Ljava/lang/Object;
.source "OpaqueComparator.java"

# interfaces
.implements Lcom/jme3/renderer/queue/GeometryComparator;


# instance fields
.field private cam:Lcom/jme3/renderer/Camera;

.field private final tempVec:Lcom/jme3/math/Vector3f;

.field private final tempVec2:Lcom/jme3/math/Vector3f;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/queue/OpaqueComparator;->tempVec:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/renderer/queue/OpaqueComparator;->tempVec2:Lcom/jme3/math/Vector3f;

    return-void
.end method


# virtual methods
.method public compare(Lcom/jme3/scene/Geometry;Lcom/jme3/scene/Geometry;)I
    .locals 6
    .param p1    # Lcom/jme3/scene/Geometry;
    .param p2    # Lcom/jme3/scene/Geometry;

    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getMaterial()Lcom/jme3/material/Material;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jme3/scene/Geometry;->getMaterial()Lcom/jme3/material/Material;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jme3/material/Material;->compareTo(Lcom/jme3/material/Material;)I

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/renderer/queue/OpaqueComparator;->distanceToCam(Lcom/jme3/scene/Geometry;)F

    move-result v0

    invoke-virtual {p0, p2}, Lcom/jme3/renderer/queue/OpaqueComparator;->distanceToCam(Lcom/jme3/scene/Geometry;)F

    move-result v1

    cmpl-float v5, v0, v1

    if-nez v5, :cond_1

    const/4 v4, 0x0

    :cond_0
    :goto_0
    return v4

    :cond_1
    cmpg-float v5, v0, v1

    if-gez v5, :cond_2

    const/4 v4, -0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/scene/Geometry;

    check-cast p2, Lcom/jme3/scene/Geometry;

    invoke-virtual {p0, p1, p2}, Lcom/jme3/renderer/queue/OpaqueComparator;->compare(Lcom/jme3/scene/Geometry;Lcom/jme3/scene/Geometry;)I

    move-result v0

    return v0
.end method

.method public distanceToCam(Lcom/jme3/scene/Geometry;)F
    .locals 5
    .param p1    # Lcom/jme3/scene/Geometry;

    const/high16 v3, -0x800000    # Float.NEGATIVE_INFINITY

    if-nez p1, :cond_0

    :goto_0
    return v3

    :cond_0
    iget v4, p1, Lcom/jme3/scene/Geometry;->queueDistance:F

    cmpl-float v3, v4, v3

    if-eqz v3, :cond_1

    iget v3, p1, Lcom/jme3/scene/Geometry;->queueDistance:F

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/jme3/renderer/queue/OpaqueComparator;->cam:Lcom/jme3/renderer/Camera;

    invoke-virtual {v3}, Lcom/jme3/renderer/Camera;->getLocation()Lcom/jme3/math/Vector3f;

    move-result-object v0

    iget-object v3, p0, Lcom/jme3/renderer/queue/OpaqueComparator;->cam:Lcom/jme3/renderer/Camera;

    iget-object v4, p0, Lcom/jme3/renderer/queue/OpaqueComparator;->tempVec2:Lcom/jme3/math/Vector3f;

    invoke-virtual {v3, v4}, Lcom/jme3/renderer/Camera;->getDirection(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v2

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getWorldBound()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getWorldBound()Lcom/jme3/bounding/BoundingVolume;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jme3/bounding/BoundingVolume;->getCenter()Lcom/jme3/math/Vector3f;

    move-result-object v1

    :goto_1
    iget-object v3, p0, Lcom/jme3/renderer/queue/OpaqueComparator;->tempVec:Lcom/jme3/math/Vector3f;

    invoke-virtual {v1, v0, v3}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget-object v3, p0, Lcom/jme3/renderer/queue/OpaqueComparator;->tempVec:Lcom/jme3/math/Vector3f;

    invoke-virtual {v3, v2}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v3

    iput v3, p1, Lcom/jme3/scene/Geometry;->queueDistance:F

    iget v3, p1, Lcom/jme3/scene/Geometry;->queueDistance:F

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/jme3/scene/Geometry;->getWorldTranslation()Lcom/jme3/math/Vector3f;

    move-result-object v1

    goto :goto_1
.end method

.method public setCamera(Lcom/jme3/renderer/Camera;)V
    .locals 0
    .param p1    # Lcom/jme3/renderer/Camera;

    iput-object p1, p0, Lcom/jme3/renderer/queue/OpaqueComparator;->cam:Lcom/jme3/renderer/Camera;

    return-void
.end method
