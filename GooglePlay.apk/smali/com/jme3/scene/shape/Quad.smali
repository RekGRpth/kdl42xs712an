.class public Lcom/jme3/scene/shape/Quad;
.super Lcom/jme3/scene/Mesh;
.source "Quad.java"


# instance fields
.field private height:F

.field private width:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jme3/scene/Mesh;-><init>()V

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1    # F
    .param p2    # F

    invoke-direct {p0}, Lcom/jme3/scene/Mesh;-><init>()V

    invoke-virtual {p0, p1, p2}, Lcom/jme3/scene/shape/Quad;->updateGeometry(FF)V

    return-void
.end method


# virtual methods
.method public updateGeometry(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jme3/scene/shape/Quad;->updateGeometry(FFZ)V

    return-void
.end method

.method public updateGeometry(FFZ)V
    .locals 8
    .param p1    # F
    .param p2    # F
    .param p3    # Z

    const/16 v7, 0x8

    const/4 v6, 0x6

    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v3, 0x0

    iput p1, p0, Lcom/jme3/scene/shape/Quad;->width:F

    iput p2, p0, Lcom/jme3/scene/shape/Quad;->height:F

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Position:Lcom/jme3/scene/VertexBuffer$Type;

    const/16 v1, 0xc

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v3, v1, v2

    const/4 v2, 0x1

    aput v3, v1, v2

    aput v3, v1, v5

    aput p1, v1, v4

    const/4 v2, 0x4

    aput v3, v1, v2

    const/4 v2, 0x5

    aput v3, v1, v2

    aput p1, v1, v6

    const/4 v2, 0x7

    aput p2, v1, v2

    aput v3, v1, v7

    const/16 v2, 0x9

    aput v3, v1, v2

    const/16 v2, 0xa

    aput p2, v1, v2

    const/16 v2, 0xb

    aput v3, v1, v2

    invoke-virtual {p0, v0, v4, v1}, Lcom/jme3/scene/shape/Quad;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;I[F)V

    if-eqz p3, :cond_0

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord:Lcom/jme3/scene/VertexBuffer$Type;

    new-array v1, v7, [F

    fill-array-data v1, :array_0

    invoke-virtual {p0, v0, v5, v1}, Lcom/jme3/scene/shape/Quad;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;I[F)V

    :goto_0
    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Normal:Lcom/jme3/scene/VertexBuffer$Type;

    const/16 v1, 0xc

    new-array v1, v1, [F

    fill-array-data v1, :array_1

    invoke-virtual {p0, v0, v4, v1}, Lcom/jme3/scene/shape/Quad;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;I[F)V

    cmpg-float v0, p2, v3

    if-gez v0, :cond_1

    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    new-array v1, v6, [S

    fill-array-data v1, :array_2

    invoke-virtual {p0, v0, v4, v1}, Lcom/jme3/scene/shape/Quad;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;I[S)V

    :goto_1
    invoke-virtual {p0}, Lcom/jme3/scene/shape/Quad;->updateBound()V

    return-void

    :cond_0
    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->TexCoord:Lcom/jme3/scene/VertexBuffer$Type;

    new-array v1, v7, [F

    fill-array-data v1, :array_3

    invoke-virtual {p0, v0, v5, v1}, Lcom/jme3/scene/shape/Quad;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;I[F)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/jme3/scene/VertexBuffer$Type;->Index:Lcom/jme3/scene/VertexBuffer$Type;

    new-array v1, v6, [S

    fill-array-data v1, :array_4

    invoke-virtual {p0, v0, v4, v1}, Lcom/jme3/scene/shape/Quad;->setBuffer(Lcom/jme3/scene/VertexBuffer$Type;I[S)V

    goto :goto_1

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 2
        0x0s
        0x2s
        0x1s
        0x0s
        0x3s
        0x2s
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_4
    .array-data 2
        0x0s
        0x1s
        0x2s
        0x0s
        0x2s
        0x3s
    .end array-data
.end method
