.class public Lcom/jme3/scene/plugins/MTLLoader;
.super Ljava/lang/Object;
.source "MTLLoader.java"

# interfaces
.implements Lcom/jme3/asset/AssetLoader;


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected alpha:F

.field protected alphaMap:Lcom/jme3/texture/Texture;

.field protected ambient:Lcom/jme3/math/ColorRGBA;

.field protected assetManager:Lcom/jme3/asset/AssetManager;

.field protected diffuse:Lcom/jme3/math/ColorRGBA;

.field protected diffuseMap:Lcom/jme3/texture/Texture;

.field protected disallowAmbient:Z

.field protected disallowSpecular:Z

.field protected disallowTransparency:Z

.field protected folderName:Ljava/lang/String;

.field protected key:Lcom/jme3/asset/AssetKey;

.field protected matList:Lcom/jme3/material/MaterialList;

.field protected matName:Ljava/lang/String;

.field protected normalMap:Lcom/jme3/texture/Texture;

.field protected scan:Ljava/util/Scanner;

.field protected shadeless:Z

.field protected shininess:F

.field protected specular:Lcom/jme3/math/ColorRGBA;

.field protected specularMap:Lcom/jme3/texture/Texture;

.field protected transparent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/scene/plugins/MTLLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/scene/plugins/MTLLoader;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/math/ColorRGBA;

    invoke-direct {v0}, Lcom/jme3/math/ColorRGBA;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->ambient:Lcom/jme3/math/ColorRGBA;

    new-instance v0, Lcom/jme3/math/ColorRGBA;

    invoke-direct {v0}, Lcom/jme3/math/ColorRGBA;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuse:Lcom/jme3/math/ColorRGBA;

    new-instance v0, Lcom/jme3/math/ColorRGBA;

    invoke-direct {v0}, Lcom/jme3/math/ColorRGBA;-><init>()V

    iput-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->specular:Lcom/jme3/math/ColorRGBA;

    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->shininess:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->alpha:F

    iput-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->transparent:Z

    iput-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowTransparency:Z

    iput-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowAmbient:Z

    iput-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowSpecular:Z

    return-void
.end method


# virtual methods
.method protected createMaterial()V
    .locals 4

    const/4 v3, 0x1

    iget v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->alpha:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->transparent:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowTransparency:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuse:Lcom/jme3/math/ColorRGBA;

    iget v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->alpha:F

    iput v2, v1, Lcom/jme3/math/ColorRGBA;->a:F

    :cond_0
    iget-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->shadeless:Z

    if-eqz v1, :cond_3

    new-instance v0, Lcom/jme3/material/Material;

    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    const-string v2, "Common/MatDefs/Misc/Unshaded.j3md"

    invoke-direct {v0, v1, v2}, Lcom/jme3/material/Material;-><init>(Lcom/jme3/asset/AssetManager;Ljava/lang/String;)V

    const-string v1, "Color"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuse:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v2}, Lcom/jme3/math/ColorRGBA;->clone()Lcom/jme3/math/ColorRGBA;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setColor(Ljava/lang/String;Lcom/jme3/math/ColorRGBA;)V

    const-string v1, "ColorMap"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuseMap:Lcom/jme3/texture/Texture;

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setTexture(Ljava/lang/String;Lcom/jme3/texture/Texture;)V

    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->transparent:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowTransparency:Z

    if-nez v1, :cond_2

    invoke-virtual {v0, v3}, Lcom/jme3/material/Material;->setTransparent(Z)V

    invoke-virtual {v0}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v1

    sget-object v2, Lcom/jme3/material/RenderState$BlendMode;->Alpha:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual {v1, v2}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    invoke-virtual {v0}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/jme3/material/RenderState;->setAlphaTest(Z)V

    invoke-virtual {v0}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v1

    const v2, 0x3c23d70a    # 0.01f

    invoke-virtual {v1, v2}, Lcom/jme3/material/RenderState;->setAlphaFallOff(F)V

    :cond_2
    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->matList:Lcom/jme3/material/MaterialList;

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->matName:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/jme3/material/MaterialList;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_3
    new-instance v0, Lcom/jme3/material/Material;

    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    const-string v2, "Common/MatDefs/Light/Lighting.j3md"

    invoke-direct {v0, v1, v2}, Lcom/jme3/material/Material;-><init>(Lcom/jme3/asset/AssetManager;Ljava/lang/String;)V

    const-string v1, "UseMaterialColors"

    invoke-virtual {v0, v1, v3}, Lcom/jme3/material/Material;->setBoolean(Ljava/lang/String;Z)V

    const-string v1, "Ambient"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->ambient:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v2}, Lcom/jme3/math/ColorRGBA;->clone()Lcom/jme3/math/ColorRGBA;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setColor(Ljava/lang/String;Lcom/jme3/math/ColorRGBA;)V

    const-string v1, "Diffuse"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuse:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v2}, Lcom/jme3/math/ColorRGBA;->clone()Lcom/jme3/math/ColorRGBA;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setColor(Ljava/lang/String;Lcom/jme3/math/ColorRGBA;)V

    const-string v1, "Specular"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->specular:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v2}, Lcom/jme3/math/ColorRGBA;->clone()Lcom/jme3/math/ColorRGBA;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setColor(Ljava/lang/String;Lcom/jme3/math/ColorRGBA;)V

    const-string v1, "Shininess"

    iget v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->shininess:F

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setFloat(Ljava/lang/String;F)V

    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuseMap:Lcom/jme3/texture/Texture;

    if-eqz v1, :cond_4

    const-string v1, "DiffuseMap"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuseMap:Lcom/jme3/texture/Texture;

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setTexture(Ljava/lang/String;Lcom/jme3/texture/Texture;)V

    :cond_4
    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->specularMap:Lcom/jme3/texture/Texture;

    if-eqz v1, :cond_5

    const-string v1, "SpecularMap"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->specularMap:Lcom/jme3/texture/Texture;

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setTexture(Ljava/lang/String;Lcom/jme3/texture/Texture;)V

    :cond_5
    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->normalMap:Lcom/jme3/texture/Texture;

    if-eqz v1, :cond_6

    const-string v1, "NormalMap"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->normalMap:Lcom/jme3/texture/Texture;

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setTexture(Ljava/lang/String;Lcom/jme3/texture/Texture;)V

    :cond_6
    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->alphaMap:Lcom/jme3/texture/Texture;

    if-eqz v1, :cond_1

    const-string v1, "AlphaMap"

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->alphaMap:Lcom/jme3/texture/Texture;

    invoke-virtual {v0, v1, v2}, Lcom/jme3/material/Material;->setTexture(Ljava/lang/String;Lcom/jme3/texture/Texture;)V

    goto/16 :goto_0
.end method

.method public load(Lcom/jme3/asset/AssetInfo;)Ljava/lang/Object;
    .locals 4
    .param p1    # Lcom/jme3/asset/AssetInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->reset()V

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->key:Lcom/jme3/asset/AssetKey;

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getManager()Lcom/jme3/asset/AssetManager;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->getKey()Lcom/jme3/asset/AssetKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jme3/asset/AssetKey;->getFolder()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->folderName:Ljava/lang/String;

    new-instance v2, Lcom/jme3/material/MaterialList;

    invoke-direct {v2}, Lcom/jme3/material/MaterialList;-><init>()V

    iput-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->matList:Lcom/jme3/material/MaterialList;

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/jme3/asset/AssetInfo;->openStream()Ljava/io/InputStream;

    move-result-object v0

    new-instance v2, Ljava/util/Scanner;

    invoke-direct {v2, v0}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Scanner;->useLocale(Ljava/util/Locale;)Ljava/util/Scanner;

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->readLine()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_1
    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->matName:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->createMaterial()V

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->resetMaterial()V

    :cond_2
    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->matList:Lcom/jme3/material/MaterialList;

    return-object v1

    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_3
    throw v2
.end method

.method protected loadTexture(Ljava/lang/String;)Lcom/jme3/texture/Texture;
    .locals 11
    .param p1    # Ljava/lang/String;

    const/4 v10, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\\p{javaWhitespace}+"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    aget-object p1, v2, v5

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/jme3/asset/TextureKey;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->folderName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/jme3/asset/TextureKey;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Lcom/jme3/asset/TextureKey;->setGenerateMips(Z)V

    :try_start_0
    iget-object v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->assetManager:Lcom/jme3/asset/AssetManager;

    invoke-interface {v5, v3}, Lcom/jme3/asset/AssetManager;->loadTexture(Lcom/jme3/asset/TextureKey;)Lcom/jme3/texture/Texture;

    move-result-object v4

    sget-object v5, Lcom/jme3/texture/Texture$WrapMode;->Repeat:Lcom/jme3/texture/Texture$WrapMode;

    invoke-virtual {v4, v5}, Lcom/jme3/texture/Texture;->setWrap(Lcom/jme3/texture/Texture$WrapMode;)V
    :try_end_0
    .catch Lcom/jme3/asset/AssetNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v4

    :catch_0
    move-exception v0

    sget-object v5, Lcom/jme3/scene/plugins/MTLLoader;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v7, "Cannot locate {0} for material {1}"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    iget-object v9, p0, Lcom/jme3/scene/plugins/MTLLoader;->key:Lcom/jme3/asset/AssetKey;

    aput-object v9, v8, v10

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v4, Lcom/jme3/texture/Texture2D;

    invoke-static {}, Lcom/jme3/util/PlaceholderAssets;->getPlaceholderImage()Lcom/jme3/texture/Image;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/jme3/texture/Texture2D;-><init>(Lcom/jme3/texture/Image;)V

    goto :goto_0
.end method

.method protected nextStatement()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    const-string v2, "\\p{javaWhitespace}+"

    invoke-virtual {v1, v2}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    return-object v0
.end method

.method protected readColor()Lcom/jme3/math/ColorRGBA;
    .locals 5

    new-instance v0, Lcom/jme3/math/ColorRGBA;

    invoke-direct {v0}, Lcom/jme3/math/ColorRGBA;-><init>()V

    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v1}, Ljava/util/Scanner;->nextFloat()F

    move-result v1

    iget-object v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v2}, Ljava/util/Scanner;->nextFloat()F

    move-result v2

    iget-object v3, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v3}, Ljava/util/Scanner;->nextFloat()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/jme3/math/ColorRGBA;->set(FFFF)Lcom/jme3/math/ColorRGBA;

    return-object v0
.end method

.method protected readLine()Z
    .locals 8

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v6}, Ljava/util/Scanner;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v5, 0x0

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v6}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v6, "#"

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->skipLine()Z

    move-result v5

    goto :goto_0

    :cond_2
    const-string v6, "newmtl"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v6}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/jme3/scene/plugins/MTLLoader;->startMaterial(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v6, "ka"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->ambient:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->readColor()Lcom/jme3/math/ColorRGBA;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/jme3/math/ColorRGBA;->set(Lcom/jme3/math/ColorRGBA;)Lcom/jme3/math/ColorRGBA;

    goto :goto_0

    :cond_4
    const-string v6, "kd"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuse:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->readColor()Lcom/jme3/math/ColorRGBA;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/jme3/math/ColorRGBA;->set(Lcom/jme3/math/ColorRGBA;)Lcom/jme3/math/ColorRGBA;

    goto :goto_0

    :cond_5
    const-string v6, "ks"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->specular:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->readColor()Lcom/jme3/math/ColorRGBA;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/jme3/math/ColorRGBA;->set(Lcom/jme3/math/ColorRGBA;)Lcom/jme3/math/ColorRGBA;

    goto :goto_0

    :cond_6
    const-string v6, "ns"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v6}, Ljava/util/Scanner;->nextFloat()F

    move-result v4

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v6, v4, v6

    if-ltz v6, :cond_0

    iput v4, p0, Lcom/jme3/scene/plugins/MTLLoader;->shininess:F

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->specular:Lcom/jme3/math/ColorRGBA;

    sget-object v7, Lcom/jme3/math/ColorRGBA;->Black:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v6, v7}, Lcom/jme3/math/ColorRGBA;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->specular:Lcom/jme3/math/ColorRGBA;

    sget-object v7, Lcom/jme3/math/ColorRGBA;->White:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v6, v7}, Lcom/jme3/math/ColorRGBA;->set(Lcom/jme3/math/ColorRGBA;)Lcom/jme3/math/ColorRGBA;

    goto/16 :goto_0

    :cond_7
    const-string v6, "d"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    const-string v6, "tr"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    :cond_8
    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v6}, Ljava/util/Scanner;->nextFloat()F

    move-result v6

    iput v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->alpha:F

    iput-boolean v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->transparent:Z

    goto/16 :goto_0

    :cond_9
    const-string v6, "map_ka"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->skipLine()Z

    move-result v5

    goto/16 :goto_0

    :cond_a
    const-string v6, "map_kd"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->nextStatement()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jme3/scene/plugins/MTLLoader;->loadTexture(Ljava/lang/String;)Lcom/jme3/texture/Texture;

    move-result-object v6

    iput-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuseMap:Lcom/jme3/texture/Texture;

    goto/16 :goto_0

    :cond_b
    const-string v6, "map_bump"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    const-string v6, "bump"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    :cond_c
    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->normalMap:Lcom/jme3/texture/Texture;

    if-nez v6, :cond_0

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->nextStatement()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jme3/scene/plugins/MTLLoader;->loadTexture(Ljava/lang/String;)Lcom/jme3/texture/Texture;

    move-result-object v6

    iput-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->normalMap:Lcom/jme3/texture/Texture;

    goto/16 :goto_0

    :cond_d
    const-string v6, "map_ks"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->nextStatement()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jme3/scene/plugins/MTLLoader;->loadTexture(Ljava/lang/String;)Lcom/jme3/texture/Texture;

    move-result-object v6

    iput-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->specularMap:Lcom/jme3/texture/Texture;

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->specularMap:Lcom/jme3/texture/Texture;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->specular:Lcom/jme3/math/ColorRGBA;

    sget-object v7, Lcom/jme3/math/ColorRGBA;->Black:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v6, v7}, Lcom/jme3/math/ColorRGBA;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->specular:Lcom/jme3/math/ColorRGBA;

    sget-object v7, Lcom/jme3/math/ColorRGBA;->White:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v6, v7}, Lcom/jme3/math/ColorRGBA;->set(Lcom/jme3/math/ColorRGBA;)Lcom/jme3/math/ColorRGBA;

    goto/16 :goto_0

    :cond_e
    const-string v6, "map_d"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v6}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jme3/scene/plugins/MTLLoader;->loadTexture(Ljava/lang/String;)Lcom/jme3/texture/Texture;

    move-result-object v6

    iput-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->alphaMap:Lcom/jme3/texture/Texture;

    iput-boolean v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->transparent:Z

    goto/16 :goto_0

    :cond_f
    const-string v6, "illum"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    iget-object v6, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    invoke-virtual {v6}, Ljava/util/Scanner;->nextInt()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput-boolean v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->shadeless:Z

    iput-boolean v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowTransparency:Z

    goto/16 :goto_0

    :pswitch_1
    iput-boolean v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowSpecular:Z

    iput-boolean v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowTransparency:Z

    goto/16 :goto_0

    :pswitch_2
    iput-boolean v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowTransparency:Z

    goto/16 :goto_0

    :pswitch_3
    iput-boolean v5, p0, Lcom/jme3/scene/plugins/MTLLoader;->transparent:Z

    goto/16 :goto_0

    :cond_10
    const-string v5, "ke"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_11

    const-string v5, "ni"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    :cond_11
    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->skipLine()Z

    move-result v5

    goto/16 :goto_0

    :cond_12
    sget-object v5, Lcom/jme3/scene/plugins/MTLLoader;->logger:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v7, "Unknown statement in MTL! {0}"

    invoke-virtual {v5, v6, v7, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->skipLine()Z

    move-result v5

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    iput-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->matList:Lcom/jme3/material/MaterialList;

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->resetMaterial()V

    return-void
.end method

.method protected resetMaterial()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->ambient:Lcom/jme3/math/ColorRGBA;

    sget-object v1, Lcom/jme3/math/ColorRGBA;->DarkGray:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v0, v1}, Lcom/jme3/math/ColorRGBA;->set(Lcom/jme3/math/ColorRGBA;)Lcom/jme3/math/ColorRGBA;

    iget-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuse:Lcom/jme3/math/ColorRGBA;

    sget-object v1, Lcom/jme3/math/ColorRGBA;->LightGray:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v0, v1}, Lcom/jme3/math/ColorRGBA;->set(Lcom/jme3/math/ColorRGBA;)Lcom/jme3/math/ColorRGBA;

    iget-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->specular:Lcom/jme3/math/ColorRGBA;

    sget-object v1, Lcom/jme3/math/ColorRGBA;->Black:Lcom/jme3/math/ColorRGBA;

    invoke-virtual {v0, v1}, Lcom/jme3/math/ColorRGBA;->set(Lcom/jme3/math/ColorRGBA;)Lcom/jme3/math/ColorRGBA;

    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->shininess:F

    iput-boolean v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowTransparency:Z

    iput-boolean v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowAmbient:Z

    iput-boolean v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->disallowSpecular:Z

    iput-boolean v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->shadeless:Z

    iput-boolean v2, p0, Lcom/jme3/scene/plugins/MTLLoader;->transparent:Z

    iput-object v3, p0, Lcom/jme3/scene/plugins/MTLLoader;->matName:Ljava/lang/String;

    iput-object v3, p0, Lcom/jme3/scene/plugins/MTLLoader;->diffuseMap:Lcom/jme3/texture/Texture;

    iput-object v3, p0, Lcom/jme3/scene/plugins/MTLLoader;->specularMap:Lcom/jme3/texture/Texture;

    iput-object v3, p0, Lcom/jme3/scene/plugins/MTLLoader;->normalMap:Lcom/jme3/texture/Texture;

    iput-object v3, p0, Lcom/jme3/scene/plugins/MTLLoader;->alphaMap:Lcom/jme3/texture/Texture;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->alpha:F

    return-void
.end method

.method protected skipLine()Z
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/jme3/scene/plugins/MTLLoader;->scan:Ljava/util/Scanner;

    const-string v2, ".*\r{0,1}\n"

    invoke-virtual {v1, v2}, Ljava/util/Scanner;->skip(Ljava/lang/String;)Ljava/util/Scanner;
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected startMaterial(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jme3/scene/plugins/MTLLoader;->matName:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->createMaterial()V

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/scene/plugins/MTLLoader;->resetMaterial()V

    iput-object p1, p0, Lcom/jme3/scene/plugins/MTLLoader;->matName:Ljava/lang/String;

    return-void
.end method
