.class public final Lcom/jme3/shader/Shader;
.super Lcom/jme3/util/NativeObject;
.source "Shader.java"

# interfaces
.implements Lcom/jme3/export/Savable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/shader/Shader$ShaderSource;,
        Lcom/jme3/shader/Shader$ShaderType;
    }
.end annotation


# instance fields
.field private attribs:Lcom/jme3/util/IntMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/IntMap",
            "<",
            "Lcom/jme3/shader/Attribute;",
            ">;"
        }
    .end annotation
.end field

.field private language:Ljava/lang/String;

.field private shaderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/shader/Shader$ShaderSource;",
            ">;"
        }
    .end annotation
.end field

.field private uniforms:Lcom/jme3/util/ListMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/ListMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/jme3/shader/Uniform;",
            ">;"
        }
    .end annotation
.end field

.field private usable:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const-class v0, Lcom/jme3/shader/Shader;

    invoke-direct {p0, v0}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Shader;->usable:Z

    return-void
.end method

.method protected constructor <init>(Lcom/jme3/shader/Shader;)V
    .locals 3

    const-class v0, Lcom/jme3/shader/Shader;

    iget v1, p1, Lcom/jme3/shader/Shader;->id:I

    invoke-direct {p0, v0, v1}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Shader;->usable:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/Shader$ShaderSource;

    iget-object v2, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/jme3/shader/Shader$ShaderSource;->createDestructableClone()Lcom/jme3/util/NativeObject;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/Shader$ShaderSource;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-class v0, Lcom/jme3/shader/Shader;

    invoke-direct {p0, v0}, Lcom/jme3/util/NativeObject;-><init>(Ljava/lang/Class;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Shader;->usable:Z

    iput-object p1, p0, Lcom/jme3/shader/Shader;->language:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    new-instance v0, Lcom/jme3/util/ListMap;

    invoke-direct {v0}, Lcom/jme3/util/ListMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/shader/Shader;->uniforms:Lcom/jme3/util/ListMap;

    new-instance v0, Lcom/jme3/util/IntMap;

    invoke-direct {v0}, Lcom/jme3/util/IntMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/shader/Shader;->attribs:Lcom/jme3/util/IntMap;

    return-void
.end method


# virtual methods
.method public addSource(Lcom/jme3/shader/Shader$ShaderType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/jme3/shader/Shader$ShaderType;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    new-instance v0, Lcom/jme3/shader/Shader$ShaderSource;

    invoke-direct {v0, p1}, Lcom/jme3/shader/Shader$ShaderSource;-><init>(Lcom/jme3/shader/Shader$ShaderType;)V

    invoke-virtual {v0, p3}, Lcom/jme3/shader/Shader$ShaderSource;->setSource(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lcom/jme3/shader/Shader$ShaderSource;->setName(Ljava/lang/String;)V

    if-eqz p4, :cond_0

    invoke-virtual {v0, p4}, Lcom/jme3/shader/Shader$ShaderSource;->setDefines(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/jme3/shader/Shader;->setUpdateNeeded()V

    return-void
.end method

.method public createDestructableClone()Lcom/jme3/util/NativeObject;
    .locals 1

    new-instance v0, Lcom/jme3/shader/Shader;

    invoke-direct {v0, p0}, Lcom/jme3/shader/Shader;-><init>(Lcom/jme3/shader/Shader;)V

    return-object v0
.end method

.method public deleteObject(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/jme3/renderer/Renderer;

    invoke-interface {p1, p0}, Lcom/jme3/renderer/Renderer;->deleteShader(Lcom/jme3/shader/Shader;)V

    return-void
.end method

.method public getAttribute(Lcom/jme3/scene/VertexBuffer$Type;)Lcom/jme3/shader/Attribute;
    .locals 3
    .param p1    # Lcom/jme3/scene/VertexBuffer$Type;

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer$Type;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/jme3/shader/Shader;->attribs:Lcom/jme3/util/IntMap;

    invoke-virtual {v2, v1}, Lcom/jme3/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/Attribute;

    if-nez v0, :cond_0

    new-instance v0, Lcom/jme3/shader/Attribute;

    invoke-direct {v0}, Lcom/jme3/shader/Attribute;-><init>()V

    invoke-virtual {p1}, Lcom/jme3/scene/VertexBuffer$Type;->name()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/jme3/shader/Attribute;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/jme3/shader/Shader;->attribs:Lcom/jme3/util/IntMap;

    invoke-virtual {v2, v1, v0}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Shader;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getSources()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/jme3/shader/Shader$ShaderSource;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getUniform(Ljava/lang/String;)Lcom/jme3/shader/Uniform;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/jme3/shader/Shader;->uniforms:Lcom/jme3/util/ListMap;

    invoke-virtual {v1, p1}, Lcom/jme3/util/ListMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/Uniform;

    if-nez v0, :cond_0

    new-instance v0, Lcom/jme3/shader/Uniform;

    invoke-direct {v0}, Lcom/jme3/shader/Uniform;-><init>()V

    iput-object p1, v0, Lcom/jme3/shader/Uniform;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/jme3/shader/Shader;->uniforms:Lcom/jme3/util/ListMap;

    invoke-virtual {v1, p1, v0}, Lcom/jme3/util/ListMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public getUniformMap()Lcom/jme3/util/ListMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jme3/util/ListMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/jme3/shader/Uniform;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/shader/Shader;->uniforms:Lcom/jme3/util/ListMap;

    return-object v0
.end method

.method public isUsable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/shader/Shader;->usable:Z

    return v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 4
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v2, "language"

    invoke-interface {v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/shader/Shader;->language:Ljava/lang/String;

    const-string v2, "shaderList"

    invoke-interface {v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readSavableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    const-string v2, "attribs"

    invoke-interface {v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readIntSavableMap(Ljava/lang/String;Lcom/jme3/util/IntMap;)Lcom/jme3/util/IntMap;

    move-result-object v2

    iput-object v2, p0, Lcom/jme3/shader/Shader;->attribs:Lcom/jme3/util/IntMap;

    const-string v2, "uniforms"

    invoke-interface {v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readStringSavableMap(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    new-instance v2, Lcom/jme3/util/ListMap;

    invoke-direct {v2, v1}, Lcom/jme3/util/ListMap;-><init>(Ljava/util/Map;)V

    iput-object v2, p0, Lcom/jme3/shader/Shader;->uniforms:Lcom/jme3/util/ListMap;

    return-void
.end method

.method public resetLocations()V
    .locals 5

    iget-object v3, p0, Lcom/jme3/shader/Shader;->uniforms:Lcom/jme3/util/ListMap;

    invoke-virtual {v3}, Lcom/jme3/util/ListMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jme3/shader/Uniform;

    invoke-virtual {v2}, Lcom/jme3/shader/Uniform;->reset()V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/jme3/shader/Shader;->attribs:Lcom/jme3/util/IntMap;

    invoke-virtual {v3}, Lcom/jme3/util/IntMap;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/util/IntMap$Entry;

    invoke-virtual {v0}, Lcom/jme3/util/IntMap$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jme3/shader/Attribute;

    const/4 v4, -0x2

    iput v4, v3, Lcom/jme3/shader/Attribute;->location:I

    goto :goto_1

    :cond_1
    return-void
.end method

.method public resetObject()V
    .locals 3

    const/4 v2, -0x1

    iput v2, p0, Lcom/jme3/shader/Shader;->id:I

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/jme3/shader/Shader;->usable:Z

    iget-object v2, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/shader/Shader$ShaderSource;

    invoke-virtual {v1}, Lcom/jme3/shader/Shader$ShaderSource;->resetObject()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/jme3/shader/Shader;->setUpdateNeeded()V

    return-void
.end method

.method public resetSources()V
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public setUpdateNeeded()V
    .locals 0

    invoke-super {p0}, Lcom/jme3/util/NativeObject;->setUpdateNeeded()V

    invoke-virtual {p0}, Lcom/jme3/shader/Shader;->resetLocations()V

    return-void
.end method

.method public setUsable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jme3/shader/Shader;->usable:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[language="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/shader/Shader;->language:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numSources="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/shader/Shader;->shaderList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numUniforms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/shader/Shader;->uniforms:Lcom/jme3/util/ListMap;

    invoke-virtual {v1}, Lcom/jme3/util/ListMap;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", shaderSources="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jme3/shader/Shader;->getSources()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
