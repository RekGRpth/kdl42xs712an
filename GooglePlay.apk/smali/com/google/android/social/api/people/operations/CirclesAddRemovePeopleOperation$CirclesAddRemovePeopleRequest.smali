.class Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$CirclesAddRemovePeopleRequest;
.super Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.source "CirclesAddRemovePeopleOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CirclesAddRemovePeopleRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest",
        "<",
        "Lcom/google/android/social/api/people/model/AudienceMember;",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "Lcom/google/api/services/plus/model/Circle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/api/services/plus/model/Circle;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$CirclesAddRemovePeopleRequest;->this$0:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    return-void
.end method


# virtual methods
.method protected processResponseData(Lcom/google/api/services/plus/model/Circle;)Lcom/google/android/social/api/people/model/AudienceMember;
    .locals 2
    .param p1    # Lcom/google/api/services/plus/model/Circle;

    iget-object v0, p1, Lcom/google/api/services/plus/model/Circle;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plus/model/Circle;->displayName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/social/api/people/model/AudienceMember;->forCircle(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plus/model/Circle;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$CirclesAddRemovePeopleRequest;->processResponseData(Lcom/google/api/services/plus/model/Circle;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v0

    return-object v0
.end method
