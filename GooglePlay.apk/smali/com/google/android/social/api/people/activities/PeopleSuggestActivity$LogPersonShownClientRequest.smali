.class Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;
.super Ljava/lang/Object;
.source "PeopleSuggestActivity.java"

# interfaces
.implements Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusInternalClientRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LogPersonShownClientRequest"
.end annotation


# instance fields
.field private final personIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;->personIds:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;->personIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
    .param p2    # Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    # getter for: Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->numberOfSuggestionsLoaded:I
    invoke-static {v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->access$700(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)I

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/util/List;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogPersonShownClientRequest;->personIds:Ljava/util/ArrayList;

    aput-object v3, v1, v2

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->postInsertLogPeopleSuggestionsShown(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;I[Ljava/util/List;)V

    return-void
.end method
