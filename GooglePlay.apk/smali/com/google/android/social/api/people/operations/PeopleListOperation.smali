.class public final Lcom/google/android/social/api/people/operations/PeopleListOperation;
.super Ljava/lang/Object;
.source "PeopleListOperation.java"

# interfaces
.implements Lcom/google/android/social/api/operations/PlusApiOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/operations/PeopleListOperation$PeopleListRequest;,
        Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;
    }
.end annotation


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final callback:Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/PeopleListOperation;->callback:Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;

    iput-object p2, p0, Lcom/google/android/social/api/people/operations/PeopleListOperation;->accountName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    new-instance v1, Lcom/google/android/social/api/people/operations/PeopleListOperation$PeopleListRequest;

    iget-object v2, p0, Lcom/google/android/social/api/people/operations/PeopleListOperation;->accountName:Ljava/lang/String;

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/social/api/people/operations/PeopleListOperation$PeopleListRequest;-><init>(Lcom/google/android/social/api/people/operations/PeopleListOperation;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Lcom/google/android/social/api/people/operations/PeopleListOperation$PeopleListRequest;->makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/PersonList;

    iget-object v1, p0, Lcom/google/android/social/api/people/operations/PeopleListOperation;->callback:Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;

    sget-object v2, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {v1, v2, v0}, Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;->onPeopleList(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V

    return-void
.end method

.method public onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/social/api/people/operations/PeopleListOperation;->callback:Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/social/api/people/operations/PeopleListOperation$Callback;->onPeopleList(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V

    return-void
.end method
