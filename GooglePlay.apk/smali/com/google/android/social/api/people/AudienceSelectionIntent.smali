.class public Lcom/google/android/social/api/people/AudienceSelectionIntent;
.super Ljava/lang/Object;
.source "AudienceSelectionIntent.java"


# instance fields
.field private final mIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;-><init>(Landroid/content/Intent;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-class v0, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;

    invoke-direct {p0, p1, v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;",
            ">(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;-><init>(Landroid/content/Intent;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    return-void
.end method

.method public static getAudienceFromResult(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    const-string v0, "AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public asIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAllowEmptySelection()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "ALLOW_EMPTY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getAudience()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "AUDIENCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCancelText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "CANCEL_TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "DESCRIPTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEveryoneCheckboxText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "EVERYONE_CHECKBOX_TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKnownAudienceMembers()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "KNOWN_AUDIENCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getLoadCircles()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "LOAD_CIRCLES"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getLoadGroups()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "LOAD_GROUPS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getLoadPeople()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "LOAD_PEOPLE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getOkText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "OK_TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowCancel()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "CANCEL_VISIBLE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getShowChips()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "CHIPS_VISIBLE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "TITLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isEveryoneChecked()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "EVERYONE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setAccountName(Ljava/lang/String;)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public setAllowEmptySelection(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "ALLOW_EMPTY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setAudience(Ljava/util/ArrayList;)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)",
            "Lcom/google/android/social/api/people/AudienceSelectionIntent;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "AUDIENCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public setEveryoneChecked(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "EVERYONE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setKnownAudienceMembers(Ljava/util/ArrayList;)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)",
            "Lcom/google/android/social/api/people/AudienceSelectionIntent;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "KNOWN_AUDIENCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public setLoadCircles(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "LOAD_CIRCLES"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setLoadGroups(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "LOAD_GROUPS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setLoadPeople(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "LOAD_PEOPLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setShowChips(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "CHIPS_VISIBLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/social/api/people/AudienceSelectionIntent;->mIntent:Landroid/content/Intent;

    const-string v1, "TITLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method
