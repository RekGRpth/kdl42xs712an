.class Lcom/google/android/social/api/people/operations/PeopleListOperation$PeopleListRequest;
.super Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.source "PeopleListOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/operations/PeopleListOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PeopleListRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest",
        "<",
        "Lcom/google/android/social/api/people/model/PersonList;",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "Lcom/google/api/services/plus/model/PeopleFeed;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/operations/PeopleListOperation;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/PeopleListOperation;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/PeopleListOperation$PeopleListRequest;->this$0:Lcom/google/android/social/api/people/operations/PeopleListOperation;

    invoke-static {}, Lcom/google/android/social/api/network/ApiaryConfig;->getPeopleList()Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    return-void
.end method


# virtual methods
.method protected processResponseData(Lcom/google/api/services/plus/model/PeopleFeed;)Lcom/google/android/social/api/people/model/PersonList;
    .locals 1
    .param p1    # Lcom/google/api/services/plus/model/PeopleFeed;

    invoke-static {p1}, Lcom/google/android/social/api/operations/OperationUtils;->personListFromPeopleFeed(Lcom/google/api/services/plus/model/PeopleFeed;)Lcom/google/android/social/api/people/model/PersonList;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plus/model/PeopleFeed;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/operations/PeopleListOperation$PeopleListRequest;->processResponseData(Lcom/google/api/services/plus/model/PeopleFeed;)Lcom/google/android/social/api/people/model/PersonList;

    move-result-object v0

    return-object v0
.end method
