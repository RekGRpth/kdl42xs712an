.class public Lcom/google/android/social/api/people/operations/GetPeopleOperation$GetPersonRequest;
.super Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.source "GetPeopleOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/operations/GetPeopleOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GetPersonRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest",
        "<",
        "Lcom/google/android/social/api/people/model/Person;",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "Lcom/google/api/services/plus/model/Person;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/social/api/network/ApiaryConfig;->getPerson(Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    return-void
.end method


# virtual methods
.method protected processResponseData(Lcom/google/api/services/plus/model/Person;)Lcom/google/android/social/api/people/model/Person;
    .locals 1
    .param p1    # Lcom/google/api/services/plus/model/Person;

    invoke-static {p1}, Lcom/google/android/social/api/operations/OperationUtils;->personFromPersonResource(Lcom/google/api/services/plus/model/Person;)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plus/model/Person;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/operations/GetPeopleOperation$GetPersonRequest;->processResponseData(Lcom/google/api/services/plus/model/Person;)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    return-object v0
.end method
