.class public Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;
.super Ljava/lang/Object;
.source "CirclesAddRemovePeopleOperation.java"

# interfaces
.implements Lcom/google/android/social/api/operations/PlusApiOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$CirclesAddRemovePeopleRequest;,
        Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;
    }
.end annotation


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final callback:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;

.field private final newCircles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private final person:Lcom/google/android/social/api/people/model/Person;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->callback:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;

    iput-object p2, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->accountName:Ljava/lang/String;

    const-string v1, "person"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/Person;

    new-instance v1, Lcom/google/android/social/api/people/model/Person;

    invoke-direct {v1, v0}, Lcom/google/android/social/api/people/model/Person;-><init>(Lcom/google/android/social/api/people/model/Person;)V

    iput-object v1, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    const-string v1, "circles"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->newCircles:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    new-instance v1, Ljava/util/HashSet;

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->newCircles:Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v6}, Lcom/google/android/social/api/people/model/Person;->getCirclesList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    new-instance v2, Ljava/util/HashSet;

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v6}, Lcom/google/android/social/api/people/model/Person;->getCirclesList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->newCircles:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    new-instance v0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->accountName:Ljava/lang/String;

    invoke-direct {v0, p1, v6}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v6, v3}, Lcom/google/android/social/api/people/model/Person;->removeCircle(Lcom/google/android/social/api/people/model/AudienceMember;)Z

    new-instance v6, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$CirclesAddRemovePeopleRequest;

    iget-object v7, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->accountName:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v9}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/social/api/network/ApiaryConfig;->getRemovePeople(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v8

    invoke-direct {v6, p0, p1, v7, v8}, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$CirclesAddRemovePeopleRequest;-><init>(Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    invoke-virtual {v0, v6}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->add(Lcom/google/android/social/api/network/ApiaryHttpRequest;)V

    sget-object v6, Lcom/google/android/social/api/logging/Aspen$View;->PEOPLE_SUGGEST_WIDGET:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iget-object v7, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v7}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/google/android/social/api/logging/PlusAnalytics;->toAddCircleMemberClientOzEvent(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v6, v3}, Lcom/google/android/social/api/people/model/Person;->addCircle(Lcom/google/android/social/api/people/model/AudienceMember;)V

    new-instance v6, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$CirclesAddRemovePeopleRequest;

    iget-object v7, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->accountName:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v9}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/google/android/social/api/network/ApiaryConfig;->getAddPeople(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v8

    invoke-direct {v6, p0, p1, v7, v8}, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$CirclesAddRemovePeopleRequest;-><init>(Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    invoke-virtual {v0, v6}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->add(Lcom/google/android/social/api/network/ApiaryHttpRequest;)V

    sget-object v6, Lcom/google/android/social/api/logging/Aspen$View;->PEOPLE_SUGGEST_WIDGET:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iget-object v7, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {v7}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/google/android/social/api/logging/PlusAnalytics;->toRemoveCircleMemberClientOzEvent(Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/plus/model/ClientOzEvent;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v6, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;

    iget-object v7, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->accountName:Ljava/lang/String;

    invoke-direct {v6, p1, v7, v5}, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v6}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->add(Lcom/google/android/social/api/network/ApiaryHttpRequest;)V

    invoke-virtual {v0, p2}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->callback:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;

    sget-object v7, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v8, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-interface {v6, v7, v8}, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;->onCirclesAddRemovePeople(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/Person;)V

    return-void
.end method

.method public onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->callback:Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;

    iget-object v1, p0, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation;->person:Lcom/google/android/social/api/people/model/Person;

    invoke-interface {v0, p1, v1}, Lcom/google/android/social/api/people/operations/CirclesAddRemovePeopleOperation$Callback;->onCirclesAddRemovePeople(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/Person;)V

    return-void
.end method
