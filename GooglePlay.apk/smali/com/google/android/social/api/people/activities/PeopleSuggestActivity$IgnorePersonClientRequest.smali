.class Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$IgnorePersonClientRequest;
.super Ljava/lang/Object;
.source "PeopleSuggestActivity.java"

# interfaces
.implements Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusInternalClientRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IgnorePersonClientRequest"
.end annotation


# instance fields
.field private final personId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$IgnorePersonClientRequest;->personId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
    .param p2    # Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$IgnorePersonClientRequest;->personId:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->ignoreSuggestion(Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;Ljava/lang/String;Z)V

    sget-object v0, Lcom/google/android/social/api/logging/Rhs$Action;->ITEM_REJECTED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$IgnorePersonClientRequest;->personId:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->postInsertLogPeopleSuggestionAction(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;)V

    return-void
.end method
