.class public Lcom/google/android/social/api/people/operations/PeopleSearchOperation;
.super Ljava/lang/Object;
.source "PeopleSearchOperation.java"

# interfaces
.implements Lcom/google/android/social/api/operations/PlusApiOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/operations/PeopleSearchOperation$PeopleSearchRequest;,
        Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;
    }
.end annotation


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final callback:Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;

.field private final query:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->query:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->accountName:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->callback:Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    new-instance v5, Lcom/google/android/social/api/people/operations/PeopleSearchOperation$PeopleSearchRequest;

    iget-object v6, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->accountName:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->query:Ljava/lang/String;

    invoke-direct {v5, p0, p1, v6, v7}, Lcom/google/android/social/api/people/operations/PeopleSearchOperation$PeopleSearchRequest;-><init>(Lcom/google/android/social/api/people/operations/PeopleSearchOperation;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Lcom/google/android/social/api/people/operations/PeopleSearchOperation$PeopleSearchRequest;->makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plus/model/PeopleFeed;

    new-instance v3, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    iget-object v5, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->accountName:Ljava/lang/String;

    invoke-direct {v3, p1, v5}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v5, v1, Lcom/google/api/services/plus/model/PeopleFeed;->items:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, v1, Lcom/google/api/services/plus/model/PeopleFeed;->items:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    new-instance v6, Lcom/google/android/social/api/people/operations/GetPeopleOperation$GetPersonRequest;

    iget-object v7, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->accountName:Ljava/lang/String;

    iget-object v5, v1, Lcom/google/api/services/plus/model/PeopleFeed;->items:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plus/model/Person;

    iget-object v5, v5, Lcom/google/api/services/plus/model/Person;->id:Ljava/lang/String;

    invoke-direct {v6, p1, v7, v5}, Lcom/google/android/social/api/people/operations/GetPeopleOperation$GetPersonRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->add(Lcom/google/android/social/api/network/ApiaryHttpRequest;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v3, p2}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->callback:Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;

    sget-object v6, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    new-instance v7, Lcom/google/android/social/api/people/model/PersonList;

    invoke-direct {v7, v4}, Lcom/google/android/social/api/people/model/PersonList;-><init>(Ljava/util/ArrayList;)V

    invoke-interface {v5, v6, v7}, Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;->onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V

    return-void
.end method

.method public onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation;->callback:Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/social/api/people/operations/PeopleSearchOperation$Callback;->onPeopleSearch(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/social/api/people/model/PersonList;)V

    return-void
.end method
