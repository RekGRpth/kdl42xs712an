.class public Lcom/google/android/social/api/operations/PostInsertLogOperation;
.super Ljava/lang/Object;
.source "PostInsertLogOperation.java"

# interfaces
.implements Lcom/google/android/social/api/operations/PlusApiOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
    }
.end annotation


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final callback:Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

.field private final logEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientOzEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/operations/PostInsertLogOperation;->callback:Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

    iput-object p2, p0, Lcom/google/android/social/api/operations/PostInsertLogOperation;->accountName:Ljava/lang/String;

    const-string v3, "client_logs"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {}, Lcom/google/api/services/plus/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plus/model/ClientOzEventJson;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/api/services/plus/model/ClientOzEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plus/model/ClientOzEvent;

    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v2, p0, Lcom/google/android/social/api/operations/PostInsertLogOperation;->logEvents:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;

    iget-object v1, p0, Lcom/google/android/social/api/operations/PostInsertLogOperation;->accountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/social/api/operations/PostInsertLogOperation;->logEvents:Ljava/util/List;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, p2}, Lcom/google/android/social/api/network/PostInsertLogApiaryRequest;->makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/social/api/operations/PostInsertLogOperation;->callback:Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

    sget-object v1, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {v0, v1}, Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;->onPostInsertLog(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/social/api/operations/PostInsertLogOperation;->callback:Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

    invoke-interface {v0, p1}, Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;->onPostInsertLog(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method
