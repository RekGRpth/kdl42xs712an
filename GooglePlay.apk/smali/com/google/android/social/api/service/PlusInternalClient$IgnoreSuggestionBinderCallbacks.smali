.class final Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;
.super Lcom/google/android/social/api/service/IPlusInternalCallbacks$Stub;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "IgnoreSuggestionBinderCallbacks"
.end annotation


# instance fields
.field private final listener:Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks$Stub;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;->listener:Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

    return-void
.end method


# virtual methods
.method public onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    new-instance v1, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;

    iget-object v2, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v3, p0, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionBinderCallbacks;->listener:Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

    invoke-static {p1, p2}, Lcom/google/android/social/api/service/Results;->getConnectionResult(ILandroid/os/Bundle;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v4

    const-string v5, "ignored"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/social/api/service/PlusInternalClient$IgnoreSuggestionCallback;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;Lcom/google/android/gms/common/ConnectionResult;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    return-void
.end method
