.class final Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "DownloadImageCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/social/api/service/IPlusInternalService;",
        ">.CallbackProxy<",
        "Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;",
        ">;"
    }
.end annotation


# instance fields
.field public final bitmap:Landroid/graphics/Bitmap;

.field public final height:I

.field public final status:Lcom/google/android/gms/common/ConnectionResult;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalClient;

.field public final url:Ljava/lang/String;

.field public final width:I


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;Lcom/google/android/gms/common/ConnectionResult;Landroid/graphics/Bitmap;Ljava/lang/String;II)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;
    .param p3    # Lcom/google/android/gms/common/ConnectionResult;
    .param p4    # Landroid/graphics/Bitmap;
    .param p5    # Ljava/lang/String;
    .param p6    # I
    .param p7    # I

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    iput-object p4, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->bitmap:Landroid/graphics/Bitmap;

    iput-object p5, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->url:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->width:I

    iput p7, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->height:I

    return-void
.end method


# virtual methods
.method protected deliverCallback(Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;)V
    .locals 6
    .param p1    # Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    # invokes: Lcom/google/android/social/api/service/PlusInternalClient;->getContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->access$100(Lcom/google/android/social/api/service/PlusInternalClient;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/social/api/internal/PlusApiContextUtil;->getPlusApiContext(Landroid/content/Context;)Lcom/google/android/social/api/PlusApiContext;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v2, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->url:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->width:I

    iget v4, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->height:I

    # invokes: Lcom/google/android/social/api/service/PlusInternalClient;->makeImageKey(Ljava/lang/String;II)Ljava/lang/String;
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/social/api/service/PlusInternalClient;->access$000(Lcom/google/android/social/api/service/PlusInternalClient;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->bitmap:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Lcom/google/android/social/api/PlusApiContext;->putImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v2, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->url:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->width:I

    iget v4, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->height:I

    iget-object v5, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->bitmap:Landroid/graphics/Bitmap;

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;->onDownloadImage(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/String;IILandroid/graphics/Bitmap;)V

    :cond_1
    return-void
.end method

.method protected bridge synthetic deliverCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;->deliverCallback(Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;)V

    return-void
.end method
