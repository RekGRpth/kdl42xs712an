.class public Lcom/google/android/play/analytics/EventLogger;
.super Lcom/google/android/play/utils/LoggableHandler;
.source "EventLogger.java"

# interfaces
.implements Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/analytics/EventLogger$Configuration;,
        Lcom/google/android/play/analytics/EventLogger$LogSource;
    }
.end annotation


# instance fields
.field private final mAndroidId:J

.field private final mAppVersion:Ljava/lang/String;

.field private final mConfiguration:Lcom/google/android/play/analytics/EventLogger$Configuration;

.field private final mContext:Landroid/content/Context;

.field private final mDelayBetweenUploadsMs:J

.field private final mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

.field private mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

.field private final mLogSource:I

.field private final mLoggingId:Ljava/lang/String;

.field private final mMccmnc:Ljava/lang/String;

.field private volatile mNextAllowedUploadTimeMs:J

.field private final mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

.field private volatile mProtoWriter:Lcom/google/protobuf/micro/CodedOutputStreamMicro;

.field private final mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

.field private volatile mUploadAccount:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/accounts/Account;
    .param p4    # Lcom/google/android/play/analytics/EventLogger$LogSource;
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .param p10    # Lcom/google/android/play/analytics/EventLogger$Configuration;

    const-string v2, "PlayEventLogger"

    invoke-direct {p0, v2}, Lcom/google/android/play/utils/LoggableHandler;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    iput-object p1, p0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/play/analytics/EventLogger$LogSource;->getProtoValue()I

    move-result v2

    iput v2, p0, Lcom/google/android/play/analytics/EventLogger;->mLogSource:I

    iput-object p2, p0, Lcom/google/android/play/analytics/EventLogger;->mLoggingId:Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {p0, v0}, Lcom/google/android/play/analytics/EventLogger;->setUploadAccount(Landroid/accounts/Account;)V

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    if-nez p5, :cond_0

    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/play/utils/PlayUtils;->getDefaultUserAgentString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p5

    :cond_0
    new-instance v2, Lcom/google/android/volley/GoogleHttpClient;

    iget-object v4, p0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    move-object/from16 v0, p5

    invoke-direct {v2, v4, v0, v5}, Lcom/google/android/volley/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

    move-wide/from16 v0, p6

    iput-wide v0, p0, Lcom/google/android/play/analytics/EventLogger;->mAndroidId:J

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mAppVersion:Ljava/lang/String;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mMccmnc:Ljava/lang/String;

    if-eqz p10, :cond_1

    new-instance v2, Lcom/google/android/play/analytics/EventLogger$Configuration;

    move-object/from16 v0, p10

    invoke-direct {v2, v0}, Lcom/google/android/play/analytics/EventLogger$Configuration;-><init>(Lcom/google/android/play/analytics/EventLogger$Configuration;)V

    :goto_0
    iput-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mConfiguration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/EventLogger;->getDelayBetweenUploads()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mDelayBetweenUploadsMs:J

    invoke-virtual {p0}, Lcom/google/android/play/analytics/EventLogger;->getRecommendedLogFileSize()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/EventLogger;->getMaxStoreSize()J

    move-result-wide v8

    invoke-virtual {p0}, Lcom/google/android/play/analytics/EventLogger;->getNumberOfFiles()J

    move-result-wide v4

    long-to-int v12, v4

    new-instance v11, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v4, "logs"

    invoke-direct {v11, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v11, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/EventLogger;->getStoreFilenamePrefix()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".log"

    move-object v10, p0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/play/analytics/RollingFileStream;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;)V

    iput-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/EventLogger;->sendEmptyMessage(I)Z

    return-void

    :cond_1
    new-instance v2, Lcom/google/android/play/analytics/EventLogger$Configuration;

    invoke-direct {v2}, Lcom/google/android/play/analytics/EventLogger$Configuration;-><init>()V

    goto :goto_0
.end method

.method private addEventImpl(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v2, v1}, Lcom/google/android/play/analytics/RollingFileStream;->write(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->checkIfShouldUpload()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v2, v1}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "PlayEventLogger"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not write string ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") to file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v2, v1}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)V

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v3, v1}, Lcom/google/android/play/analytics/ProtoCache;->recycle(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)V

    throw v2
.end method

.method private checkIfShouldUpload()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/RollingFileStream;->hasUnreadFiles()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/analytics/EventLogger;->queueUpload(J)V

    :cond_0
    return-void
.end method

.method private createByteArrayFrom(Ljava/io/InputStream;I)[B
    .locals 4
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-array v0, p2, [B

    :cond_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_1
    if-gez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private getMinDelayBetweenUploads()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mConfiguration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    iget-wide v0, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->minDelayBetweenUploadsMs:J

    return-wide v0
.end method

.method private handleResponse(Lorg/apache/http/HttpResponse;)V
    .locals 9
    .param p1    # Lorg/apache/http/HttpResponse;

    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    const/16 v6, 0x80

    invoke-direct {p0, v2, v6}, Lcom/google/android/play/analytics/EventLogger;->createByteArrayFrom(Ljava/io/InputStream;I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->parseFrom([B)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasNextRequestWaitMillis()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v3}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->getNextRequestWaitMillis()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/google/android/play/analytics/EventLogger;->setNextUploadTimeAfter(J)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v6, "PlayEventLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error parsing content: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v6, "PlayEventLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error getting the content of the response body: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v6, "PlayEventLogger"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error reading the content of the response body: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private queueUpload(J)V
    .locals 8
    .param p1    # J

    const/4 v6, 0x3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-lez v4, :cond_1

    add-long v2, v0, p1

    iget-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    iget-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    sub-long p1, v4, v0

    :cond_0
    invoke-virtual {p0, v6, p1, p2}, Lcom/google/android/play/analytics/EventLogger;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    iget-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->getMinDelayBetweenUploads()J

    move-result-wide v6

    add-long/2addr v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    return-void

    :cond_1
    invoke-virtual {p0, v6}, Lcom/google/android/play/analytics/EventLogger;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method private setNextUploadTimeAfter(J)V
    .locals 4
    .param p1    # J

    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->getMinDelayBetweenUploads()J

    move-result-wide v2

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/android/play/analytics/EventLogger;->mNextAllowedUploadTimeMs:J

    return-void
.end method

.method private uploadEventsImpl()Z
    .locals 13

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/google/android/play/analytics/EventLogger;->mUploadAccount:Landroid/accounts/Account;

    const/4 v1, 0x0

    if-nez v6, :cond_1

    :try_start_0
    const-string v8, "PlayEventLogger"

    const-string v9, "No account available for uploading logs.  Skipping upload"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_0

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    :goto_0
    return v7

    :cond_0
    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->hasUnreadFiles()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v8

    if-nez v8, :cond_3

    if-eqz v1, :cond_2

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto :goto_0

    :cond_3
    :try_start_2
    new-instance v5, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    invoke-direct {v5}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->setRequestTimeMs(J)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;-><init>()V

    iget-wide v8, p0, Lcom/google/android/play/analytics/EventLogger;->mAndroidId:J

    invoke-virtual {v0, v8, v9}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setAndroidId(J)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mLoggingId:Ljava/lang/String;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mLoggingId:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setLoggingId(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    :cond_4
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setSdkVersion(I)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    sget-object v8, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setModel(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    sget-object v8, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setProduct(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    sget-object v8, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setHardware(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    sget-object v8, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setDevice(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    sget-object v8, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setOsBuild(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mMccmnc:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setMccMnc(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setLocale(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setCountry(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mAppVersion:Ljava/lang/String;

    invoke-virtual {v0, v8}, Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;->setApplicationBuild(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;

    new-instance v2, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    invoke-direct {v2}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;->setAndroidClientInfo(Lcom/google/android/play/analytics/ClientAnalytics$AndroidClientInfo;)Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;

    invoke-virtual {v5, v2}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->setClientInfo(Lcom/google/android/play/analytics/ClientAnalytics$ClientInfo;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    iget v8, p0, Lcom/google/android/play/analytics/EventLogger;->mLogSource:I

    invoke-virtual {v5, v8}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->setLogSource(I)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8, v5}, Lcom/google/android/play/analytics/RollingFileStream;->read(Ljava/lang/Object;)V

    const-string v8, "https://android.clients.google.com/play/log"

    invoke-virtual {p0}, Lcom/google/android/play/analytics/EventLogger;->getMaxNumberOfRedirects()I

    move-result v9

    invoke-virtual {p0, v6, v5, v8, v9}, Lcom/google/android/play/analytics/EventLogger;->uploadLog(Landroid/accounts/Account;Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;Ljava/lang/String;I)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v1

    :try_start_4
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v7, v5}, Lcom/google/android/play/analytics/ProtoCache;->recycleLogRequest(Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_6

    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    :goto_1
    move v7, v1

    goto/16 :goto_0

    :catch_0
    move-exception v3

    :try_start_5
    const-string v8, "PlayEventLogger"

    const-string v9, "Upload failed %s (%s)"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/4 v1, 0x0

    :try_start_6
    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v8, v5}, Lcom/google/android/play/analytics/ProtoCache;->recycleLogRequest(Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v1, :cond_5

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    goto/16 :goto_0

    :cond_5
    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto/16 :goto_0

    :catchall_0
    move-exception v7

    :try_start_7
    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v8, v5}, Lcom/google/android/play/analytics/ProtoCache;->recycleLogRequest(Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;)V

    throw v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v7

    if-eqz v1, :cond_7

    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->deleteAllReadFiles()V

    :goto_2
    throw v7

    :cond_6
    iget-object v7, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v7}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto :goto_1

    :cond_7
    iget-object v8, p0, Lcom/google/android/play/analytics/EventLogger;->mRollingFileStream:Lcom/google/android/play/analytics/RollingFileStream;

    invoke-virtual {v8}, Lcom/google/android/play/analytics/RollingFileStream;->markAllFilesAsUnread()V

    goto :goto_2
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    const-string v1, "PlayEventLogger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown msg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->checkIfShouldUpload()V

    iget-wide v1, p0, Lcom/google/android/play/analytics/EventLogger;->mDelayBetweenUploadsMs:J

    invoke-direct {p0, v1, v2}, Lcom/google/android/play/analytics/EventLogger;->queueUpload(J)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/play/analytics/EventLogger;->addEventImpl(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/EventLogger;->removeMessages(I)V

    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->uploadEventsImpl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/play/analytics/EventLogger;->checkIfShouldUpload()V

    :cond_0
    iget-wide v1, p0, Lcom/google/android/play/analytics/EventLogger;->mDelayBetweenUploadsMs:J

    invoke-direct {p0, v1, v2}, Lcom/google/android/play/analytics/EventLogger;->queueUpload(J)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 5
    .param p1    # Landroid/accounts/Account;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string v3, "PlayEventLogger"

    const-string v4, "No account for auth token provided"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "androidmarket"

    const/4 v4, 0x1

    invoke-virtual {v2, p1, v3, v4}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "PlayEventLogger"

    const-string v4, "OperationCanceledException"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "PlayEventLogger"

    const-string v4, "AuthenticatorException"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v1

    const-string v3, "PlayEventLogger"

    const-string v4, "IOException"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected getDelayBetweenUploads()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mConfiguration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    iget-wide v0, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->delayBetweenUploadsMs:J

    return-wide v0
.end method

.method protected getMaxNumberOfRedirects()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mConfiguration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    iget v0, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxNumberOfRedirects:I

    return v0
.end method

.method protected getMaxStoreSize()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mConfiguration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    iget-wide v0, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->maxStorageSize:J

    return-wide v0
.end method

.method protected getNumberOfFiles()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mConfiguration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    iget v0, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->numberOfFiles:I

    int-to-long v0, v0

    return-wide v0
.end method

.method protected getRecommendedLogFileSize()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mConfiguration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    iget-wide v0, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    return-wide v0
.end method

.method protected getStoreFilenamePrefix()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/google/android/play/utils/PlayUtils;->getProcessName()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "eventlog.store"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_0

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    if-ge v0, v3, :cond_0

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public varargs logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    .param p3    # Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;
    .param p4    # [Ljava/lang/Object;

    if-eqz p4, :cond_0

    array-length v3, p4

    rem-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Extras must be in the format <key>, <value>, <key>, <value>...  incorrect: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v3}, Lcom/google/android/play/analytics/ProtoCache;->obtainEvent()Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->setEventTimeMs(J)Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-virtual {v0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->setTag(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->setExp(Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;)Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {v0, p3}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->setStore(Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;)Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    :cond_2
    if-eqz p4, :cond_4

    const/4 v1, 0x0

    :goto_0
    array-length v3, p4

    if-ge v1, v3, :cond_4

    iget-object v3, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v3}, Lcom/google/android/play/analytics/ProtoCache;->obtainKeyValue()Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    move-result-object v2

    aget-object v3, p4, v1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;->setKey(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    add-int/lit8 v3, v1, 0x1

    aget-object v3, p4, v3

    if-eqz v3, :cond_3

    add-int/lit8 v3, v1, 0x1

    aget-object v3, p4, v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v2, v3}, Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;->setValue(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;

    invoke-virtual {v0, v2}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->addValue(Lcom/google/android/play/analytics/ClientAnalytics$LogEventKeyValues;)Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_3
    const-string v3, "null"

    goto :goto_1

    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p0, v3, v0}, Lcom/google/android/play/analytics/EventLogger;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/play/analytics/EventLogger;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public varargs logEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;Lcom/google/android/play/analytics/PlayStore$PlayStoreLogEvent;[Ljava/lang/Object;)V

    return-void
.end method

.method public onNewOutputFile(Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;

    invoke-static {p1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->newInstance(Ljava/io/OutputStream;)Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoWriter:Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/EventLogger;->mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    return-void
.end method

.method public onRead(Ljava/io/InputStream;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->newInstance(Ljava/io/InputStream;)Lcom/google/protobuf/micro/CodedInputStreamMicro;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->isAtEnd()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoCache:Lcom/google/android/play/analytics/ProtoCache;

    invoke-virtual {v2}, Lcom/google/android/play/analytics/ProtoCache;->obtainEvent()Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    move-object v2, p2

    check-cast v2, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    invoke-virtual {v2, v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->addLogEvent(Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;)Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onWrite(Ljava/io/OutputStream;Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object v0, p2

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->hasExp()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->getExp()Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;->clearExp()Lcom/google/android/play/analytics/ClientAnalytics$LogEvent;

    :goto_0
    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoWriter:Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    invoke-virtual {v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessageNoTag(Lcom/google/protobuf/micro/MessageMicro;)V

    iget-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mProtoWriter:Lcom/google/protobuf/micro/CodedOutputStreamMicro;

    invoke-virtual {v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->flush()V

    return-void

    :cond_0
    const-string v2, "PlayEventLogger"

    const-string v3, "ActiveExperiments changed, sending with next LogEvent"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v1, p0, Lcom/google/android/play/analytics/EventLogger;->mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/play/analytics/EventLogger;->mLastSentExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    goto :goto_0
.end method

.method public setUploadAccount(Landroid/accounts/Account;)V
    .locals 0
    .param p1    # Landroid/accounts/Account;

    iput-object p1, p0, Lcom/google/android/play/analytics/EventLogger;->mUploadAccount:Landroid/accounts/Account;

    return-void
.end method

.method protected uploadLog(Landroid/accounts/Account;Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;Ljava/lang/String;I)Z
    .locals 28
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/play/analytics/EventLogger;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v5

    new-instance v15, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, p3

    invoke-direct {v15, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const-string v25, "Authorization"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "GoogleLogin auth="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v15, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v10, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v10, v6}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;->toByteArray()[B

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    invoke-virtual {v10}, Ljava/util/zip/GZIPOutputStream;->close()V

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    new-instance v9, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v9, v8}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    const-string v25, "gzip"

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Lorg/apache/http/entity/ByteArrayEntity;->setContentEncoding(Ljava/lang/String;)V

    const-string v25, "application/x-gzip"

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    invoke-virtual {v15, v9}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/analytics/EventLogger;->mHttpClient:Lcom/google/android/volley/GoogleHttpClient;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Lcom/google/android/volley/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v23

    const/16 v25, 0xc8

    move/from16 v0, v25

    move/from16 v1, v23

    if-gt v0, v1, :cond_0

    const/16 v25, 0x12c

    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_0

    const/4 v7, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/play/analytics/EventLogger;->handleResponse(Lorg/apache/http/HttpResponse;)V

    :goto_0
    return v7

    :cond_0
    const/16 v25, 0x12c

    move/from16 v0, v25

    move/from16 v1, v23

    if-gt v0, v1, :cond_3

    const/16 v25, 0x190

    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_3

    if-lez p4, :cond_2

    const-string v25, "Location"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v11

    if-nez v11, :cond_1

    const-string v25, "PlayEventLogger"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Status "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "... redirect: no location header"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v11}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v17

    add-int/lit8 v25, p4, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v17

    move/from16 v4, v25

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/analytics/EventLogger;->uploadLog(Landroid/accounts/Account;Lcom/google/android/play/analytics/ClientAnalytics$LogRequest;Ljava/lang/String;I)Z

    move-result v7

    goto :goto_0

    :cond_2
    const-string v25, "PlayEventLogger"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Server returned "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "... redirect, but no more redirects"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " allowed."

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_3
    const/16 v25, 0x190

    move/from16 v0, v23

    move/from16 v1, v25

    if-ne v0, v1, :cond_4

    const-string v25, "PlayEventLogger"

    const-string v26, "Server returned 400... deleting local malformed logs"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_4
    const/16 v25, 0x191

    move/from16 v0, v23

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    const-string v25, "PlayEventLogger"

    const-string v26, "Server returned 401... invalidating auth token"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/analytics/EventLogger;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v12, v0, v5}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_5
    const/16 v25, 0x1f4

    move/from16 v0, v23

    move/from16 v1, v25

    if-ne v0, v1, :cond_6

    const-string v25, "PlayEventLogger"

    const-string v26, "Server returned 500... server crashed"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_6
    const/16 v25, 0x1f5

    move/from16 v0, v23

    move/from16 v1, v25

    if-ne v0, v1, :cond_7

    const-string v25, "PlayEventLogger"

    const-string v26, "Server returned 501... service doesn\'t seem to exist"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_7
    const/16 v25, 0x1f6

    move/from16 v0, v23

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    const-string v25, "PlayEventLogger"

    const-string v26, "Server returned 502... servers are down"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_8
    const/16 v25, 0x1f7

    move/from16 v0, v23

    move/from16 v1, v25

    if-ne v0, v1, :cond_b

    const-string v25, "Retry-After"

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v21

    if-eqz v21, :cond_a

    const/4 v14, 0x0

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v22

    :try_start_0
    invoke-static/range {v22 .. v22}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    const-string v25, "PlayEventLogger"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Server said to retry after "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-wide/from16 v1, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " seconds"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v25, 0x3e8

    mul-long v25, v25, v19

    move-object/from16 v0, p0

    move-wide/from16 v1, v25

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/analytics/EventLogger;->setNextUploadTimeAfter(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v14, 0x1

    :goto_1
    if-nez v14, :cond_9

    const/4 v7, 0x1

    :goto_2
    goto/16 :goto_0

    :catch_0
    move-exception v13

    const-string v25, "PlayEventLogger"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Unknown retry value: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_9
    const/4 v7, 0x0

    goto :goto_2

    :cond_a
    const-string v25, "PlayEventLogger"

    const-string v26, "Status 503 without retry-after header"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_b
    const/16 v25, 0x1f8

    move/from16 v0, v23

    move/from16 v1, v25

    if-ne v0, v1, :cond_c

    const-string v25, "PlayEventLogger"

    const-string v26, "Server returned 504... timeout"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_c
    const-string v25, "PlayEventLogger"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Unexpected error received from server: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-interface/range {v24 .. v24}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x1

    goto/16 :goto_0
.end method
