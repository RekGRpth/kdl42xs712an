.class public final Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreUiElementInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    }
.end annotation


# instance fields
.field private cachedSize:I

.field private hasInstrumentInfo:Z

.field private hasSerialDocid:Z

.field private instrumentInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

.field private serialDocid_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->cachedSize:I

    return v0
.end method

.method public getInstrumentInfo()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    return-object v0
.end method

.method public getSerialDocid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->hasInstrumentInfo()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->getInstrumentInfo()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->hasSerialDocid()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->getSerialDocid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->cachedSize:I

    return v0
.end method

.method public hasInstrumentInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->hasInstrumentInfo:Z

    return v0
.end method

.method public hasSerialDocid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->hasSerialDocid:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->setInstrumentInfo(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->setSerialDocid(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    move-result-object v0

    return-object v0
.end method

.method public setInstrumentInfo(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->hasInstrumentInfo:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    return-object p0
.end method

.method public setSerialDocid(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->hasSerialDocid:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->serialDocid_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->hasInstrumentInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->getInstrumentInfo()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->hasSerialDocid()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;->getSerialDocid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    return-void
.end method
