.class Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;
.super Landroid/os/AsyncTask;
.source "PackageVerificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/vending/verifier/PackageVerificationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WorkerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

.field final synthetic this$0:Lcom/google/android/vending/verifier/PackageVerificationService;


# direct methods
.method public constructor <init>(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    .locals 0
    .param p2    # Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iput-object p1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 9
    .param p1    # [Ljava/lang/Void;

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iput v8, v4, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->mResult:I

    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iget v4, v4, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingUid:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iget v4, v4, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingUid:I

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iput-object v1, v4, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingPackageNames:[Ljava/lang/String;

    if-eqz v1, :cond_0

    array-length v4, v1

    if-lez v4, :cond_0

    const/4 v4, 0x0

    :try_start_0
    aget-object v4, v1, v4

    const/16 v5, 0x40

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iget-object v5, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->getRawSignatures([Landroid/content/pm/Signature;)[[B
    invoke-static {v5}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$000([Landroid/content/pm/Signature;)[[B

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingSignatures:[[B
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v5, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->getPackageInfo(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)Z
    invoke-static {v4, v5}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$100(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    :goto_1
    return-object v4

    :catch_0
    move-exception v0

    const-string v4, "Could not retrieve signatures for package %s"

    new-array v5, v8, [Ljava/lang/Object;

    aget-object v6, v1, v7

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->originatingPackageNames:[Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->resolveHosts(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    invoke-static {v4}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$200(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public execute()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedAntiMalwareConsent:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->sendVerificationRequest(Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    invoke-static {v0, v1}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$300(Lcom/google/android/vending/verifier/PackageVerificationService;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iget-boolean v0, v0, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->fromVerificationActivity:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iget v1, v1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    const/4 v2, 0x1

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->extendTimeout(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$400(Lcom/google/android/vending/verifier/PackageVerificationService;II)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    iget v1, v1, Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;->id:I

    invoke-static {v0, v1}, Lcom/google/android/vending/verifier/ConsentDialog;->show(Landroid/content/Context;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->this$0:Lcom/google/android/vending/verifier/PackageVerificationService;

    iget-object v1, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    # invokes: Lcom/google/android/vending/verifier/PackageVerificationService;->reportAndCleanup(Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/vending/verifier/PackageVerificationService;->access$500(Lcom/google/android/vending/verifier/PackageVerificationService;Landroid/content/Context;Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    const-string v0, "Verification Requested for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/vending/verifier/PackageVerificationService$WorkerTask;->mState:Lcom/google/android/vending/verifier/PackageVerificationService$VerificationState;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
