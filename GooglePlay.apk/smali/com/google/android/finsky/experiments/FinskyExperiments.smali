.class public Lcom/google/android/finsky/experiments/FinskyExperiments;
.super Ljava/lang/Object;
.source "FinskyExperiments.java"

# interfaces
.implements Lcom/google/android/finsky/experiments/Experiments;


# static fields
.field private static final sRecognizedExperiments:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

.field private final mEnabledExperiments:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEnabledHeaderValue:Ljava/lang/String;

.field private mUnsupportedHeaderValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.show_buy_verb_in_button"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.purchase_button_show_wallet_icon"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.purchase_button_show_lock_icon"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.purchase_button_show_play_icon"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.purchase_button_blue_background"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.purchase_dialog_hide_play_logo"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mAccountName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/finsky/experiments/FinskyExperiments;->loadExperimentsFromDisk()V

    return-void
.end method

.method private loadExperimentsFromDisk()V
    .locals 5

    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->experimentList:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mAccountName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v3, ","

    invoke-static {v0, v3}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    invoke-static {v2}, Lcom/google/android/finsky/utils/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/finsky/experiments/FinskyExperiments;->setExperimentsInternal(Ljava/util/Collection;)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1
.end method

.method private setExperimentsInternal(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    iput-object v5, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledHeaderValue:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mUnsupportedHeaderValue:Ljava/lang/String;

    new-instance v4, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-direct {v4}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;-><init>()V

    iput-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    sget-object v4, Lcom/google/android/finsky/config/G;->additionalExperiments:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p1}, Lcom/google/android/finsky/utils/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object p1

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_0
    sget-object v4, Lcom/google/android/finsky/api/DfeApiConfig;->showStagingData:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    const-string v5, "android_group:eng.finsky.merchandising.staging"

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    const-string v5, "android_group:eng.finsky.merchandising.staging"

    invoke-virtual {v4, v5}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->addClientAlteringExperiment(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    :cond_1
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v4, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-virtual {v4, v1}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->addClientAlteringExperiment(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    goto :goto_0

    :cond_2
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-virtual {v4, v1}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->addOtherExperiment(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    goto :goto_0

    :cond_3
    const-string v4, ","

    iget-object v5, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledHeaderValue:Ljava/lang/String;

    const-string v4, ","

    invoke-static {v4, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mUnsupportedHeaderValue:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public declared-synchronized getActiveExperiments()Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getEnabledExperimentsHeaderValue()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledHeaderValue:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUnsupportedExperimentsHeaderValue()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mUnsupportedHeaderValue:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasEnabledExperiments()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasUnsupportedExperiments()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mUnsupportedHeaderValue:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setExperiments(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ","

    invoke-static {v1, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->experimentList:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v2, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/experiments/FinskyExperiments;->setExperimentsInternal(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->experimentList:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v2, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
