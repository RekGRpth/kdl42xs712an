.class public Lcom/google/android/finsky/adapters/QuickLinkHelper;
.super Ljava/lang/Object;
.source "QuickLinkHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getQuickLinksForStream(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mQuickLink:Lcom/google/android/finsky/protos/Browse$QuickLink;
    invoke-static {v1}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$000(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getDisplayRequired()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v3

    invoke-static {v6, v3, v7}, Lcom/google/android/finsky/utils/PlayUtils;->getStreamQuickLinkColumnCount(Landroid/content/res/Resources;II)I

    move-result v2

    int-to-double v6, v3

    int-to-double v8, v2

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v4, v6

    mul-int v6, v4, v2

    sub-int v5, v6, v3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mQuickLink:Lcom/google/android/finsky/protos/Browse$QuickLink;
    invoke-static {v1}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$000(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getDisplayRequired()Z

    move-result v6

    if-nez v6, :cond_2

    if-lez v5, :cond_3

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    :cond_3
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    return-void
.end method

.method public static getQuickLinksRow(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/view/LayoutInflater;Lcom/google/android/finsky/utils/BitmapLoader;Ljava/lang/String;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Ljava/util/List;IILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View;
    .locals 27
    .param p0    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p1    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/view/ViewGroup;
    .param p6    # Landroid/view/ViewGroup;
    .param p8    # I
    .param p9    # I
    .param p10    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/DfeToc;",
            "Lcom/google/android/finsky/navigationmanager/NavigationManager;",
            "Landroid/view/LayoutInflater;",
            "Lcom/google/android/finsky/utils/BitmapLoader;",
            "Ljava/lang/String;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;II",
            "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    if-nez p8, :cond_1

    const/16 v17, 0x1

    :goto_0
    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, p9

    invoke-static {v3, v0}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v26

    add-int/lit8 v3, v26, -0x1

    move/from16 v0, p8

    if-ne v0, v3, :cond_2

    const/16 v18, 0x1

    :goto_1
    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    move-result v21

    mul-int v22, p8, p9

    const/16 v20, 0x0

    if-nez p5, :cond_3

    const v3, 0x7f0400fa    # com.android.vending.R.layout.quick_links_row

    const/4 v4, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p6

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p5

    check-cast p5, Landroid/view/ViewGroup;

    const/16 v20, 0x1

    :cond_0
    if-eqz v20, :cond_7

    invoke-virtual/range {p5 .. p5}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v14, 0x0

    :goto_2
    move/from16 v0, p9

    if-ge v14, v0, :cond_7

    add-int v15, v22, v14

    move/from16 v0, v21

    if-ge v15, v0, :cond_5

    move-object/from16 v0, p7

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mQuickLink:Lcom/google/android/finsky/protos/Browse$QuickLink;
    invoke-static {v3}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$000(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getPrismStyle()Z

    move-result v3

    if-eqz v3, :cond_5

    const/16 v19, 0x1

    :goto_3
    if-eqz v19, :cond_6

    const v13, 0x7f0400f9    # com.android.vending.R.layout.quick_link_prism

    :goto_4
    const/4 v3, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-virtual {v0, v13, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_1
    const/16 v17, 0x0

    goto :goto_0

    :cond_2
    const/16 v18, 0x0

    goto :goto_1

    :cond_3
    const/4 v14, 0x0

    :goto_5
    move/from16 v0, p9

    if-ge v14, v0, :cond_0

    add-int v15, v22, v14

    move/from16 v0, v21

    if-ge v15, v0, :cond_0

    move-object/from16 v0, p7

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mQuickLink:Lcom/google/android/finsky/protos/Browse$QuickLink;
    invoke-static {v3}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$000(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getPrismStyle()Z

    move-result v19

    move-object/from16 v0, p5

    invoke-virtual {v0, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v0, v3, Lcom/google/android/finsky/layout/play/PlayQuickLinkPrism;

    move/from16 v16, v0

    move/from16 v0, v19

    move/from16 v1, v16

    if-eq v0, v1, :cond_4

    const/16 v20, 0x1

    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    :cond_5
    const/16 v19, 0x0

    goto :goto_3

    :cond_6
    const v13, 0x7f0400f8    # com.android.vending.R.layout.quick_link

    goto :goto_4

    :cond_7
    invoke-virtual/range {p5 .. p5}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v3, 0x7f0b0069    # com.android.vending.R.dimen.quick_link_block_padding

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    const v3, 0x7f0b006a    # com.android.vending.R.dimen.quick_link_block_padding_bottom

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    const/4 v14, 0x0

    :goto_6
    invoke-virtual/range {p5 .. p5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v14, v3, :cond_9

    move-object/from16 v0, p5

    invoke-virtual {v0, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->resetUiElementNode()V

    add-int v15, v22, v14

    move/from16 v0, v21

    if-lt v15, v0, :cond_8

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setVisibility(I)V

    :goto_7
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    :cond_8
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setVisibility(I)V

    move-object/from16 v0, p7

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mQuickLink:Lcom/google/android/finsky/protos/Browse$QuickLink;
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$000(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v3

    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mBackendId:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$100(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)I

    move-result v4

    move-object/from16 v5, p4

    move-object/from16 v6, p1

    move-object/from16 v7, p0

    move-object/from16 v8, p3

    move-object/from16 v9, p10

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->bind(Lcom/google/android/finsky/protos/Browse$QuickLink;ILjava/lang/String;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_7

    :cond_9
    if-eqz v18, :cond_a

    move v12, v11

    :goto_8
    if-eqz v17, :cond_b

    move/from16 v25, v10

    :goto_9
    move-object/from16 v0, p5

    move/from16 v1, v25

    invoke-virtual {v0, v10, v1, v10, v12}, Landroid/view/ViewGroup;->setPadding(IIII)V

    return-object p5

    :cond_a
    const/4 v12, 0x0

    goto :goto_8

    :cond_b
    const/16 v25, 0x0

    goto :goto_9
.end method
