.class Lcom/google/android/finsky/adapters/CardListAdapter$1;
.super Ljava/lang/Object;
.source "CardListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/adapters/CardListAdapter;->getSingleDocCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

.field final synthetic val$clusterDoc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$serverLogsCookie:Lcom/google/protobuf/micro/ByteStringMicro;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/adapters/CardListAdapter;Lcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/api/model/Document;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/adapters/CardListAdapter$1;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/adapters/CardListAdapter$1;->val$serverLogsCookie:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object p3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$1;->val$clusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x197

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$1;->val$serverLogsCookie:Lcom/google/protobuf/micro/ByteStringMicro;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter$1;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v4, v4, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter$1;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v0, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter$1;->val$clusterDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$1;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter;->mCurrentPageUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/finsky/adapters/CardListAdapter;->access$400(Lcom/google/android/finsky/adapters/CardListAdapter;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
