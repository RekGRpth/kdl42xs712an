.class public Lcom/google/android/finsky/layout/DecoratedTextView;
.super Landroid/widget/TextView;
.source "DecoratedTextView.java"

# interfaces
.implements Lcom/google/android/finsky/utils/BitmapLoader$BitmapLoadedHandler;


# instance fields
.field private mBorderPaint:Landroid/graphics/Paint;

.field private mBorderThickness:F

.field private mDecorationPosition:I

.field private mDrawBorder:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/android/vending/R$styleable;->DecoratedTextView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mDecorationPosition:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0062    # com.android.vending.R.dimen.play_tipper_sticker_outline

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderThickness:F

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderThickness:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->setWillNotDraw(Z)V

    return-void
.end method

.method private setBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DecoratedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mDecorationPosition:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/google/android/finsky/layout/DecoratedTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v2, v2, v0, v2}, Lcom/google/android/finsky/layout/DecoratedTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setDrawable(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mDecorationPosition:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, p1, v1, v1, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v1, v1, p1, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public loadDecoration(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DecoratedTextView;->setDrawable(I)V

    return-void
.end method

.method public loadDecoration(Lcom/google/android/finsky/utils/BitmapLoader;Ljava/lang/String;I)V
    .locals 8
    .param p1    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v2, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v3, p0

    move v4, p3

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/utils/BitmapLoader;->get(Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/google/android/finsky/utils/BitmapLoader$BitmapLoadedHandler;II)Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-direct {p0, v7}, Lcom/google/android/finsky/layout/DecoratedTextView;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mDrawBorder:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderThickness:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v6, v0

    int-to-float v1, v6

    int-to-float v2, v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DecoratedTextView;->getWidth()I

    move-result v0

    sub-int/2addr v0, v6

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DecoratedTextView;->getHeight()I

    move-result v0

    sub-int/2addr v0, v6

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;

    invoke-virtual {p1}, Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/DecoratedTextView;->onResponse(Lcom/google/android/finsky/utils/BitmapLoader$BitmapContainer;)V

    return-void
.end method

.method public setContentColorId(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DecoratedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setTextColor(I)V

    iput-boolean p2, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mDrawBorder:Z

    iget-boolean v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mDrawBorder:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DecoratedTextView;->invalidate()V

    return-void
.end method

.method public setContentColorStateListId(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DecoratedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iput-boolean p2, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mDrawBorder:Z

    iget-boolean v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mDrawBorder:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/DecoratedTextView;->mBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DecoratedTextView;->invalidate()V

    return-void
.end method
