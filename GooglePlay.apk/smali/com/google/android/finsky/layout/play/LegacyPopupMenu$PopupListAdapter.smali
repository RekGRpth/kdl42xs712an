.class public Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;
.super Landroid/widget/BaseAdapter;
.source "LegacyPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/play/LegacyPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PopupListAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPopupActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0400e6    # com.android.vending.R.layout.play_popup_selector_item

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object v1, p2

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;

    # getter for: Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mTitle:Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->access$000(Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    # getter for: Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mIsEnabled:Z
    invoke-static {v0}, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->access$100(Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    return-object v1
.end method

.method public onSelect(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupListAdapter;->mPopupActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;

    # getter for: Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mActionListener:Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;
    invoke-static {v0}, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->access$200(Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;)Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;->onActionSelected()V

    return-void
.end method
