.class Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;
.super Ljava/lang/Object;
.source "PlayCardOverflowView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/play/PlayCardOverflowView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CardDismissalAction"
.end annotation


# instance fields
.field private final mClickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private final mDoc:Lcom/google/android/finsky/api/model/Document;

.field private final mOnDismissListener:Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;

.field private final mReferrerUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/api/DfeApi;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p3, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mOnDismissListener:Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;

    iput-object p4, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mReferrerUrl:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mClickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mDoc:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;)Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mOnDismissListener:Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;

    return-object v0
.end method


# virtual methods
.method public onActionSelected()V
    .locals 8

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mReferrerUrl:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "recDismiss.click?doc="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v7, v5}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, 0xd4

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mClickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v3, v4, v7, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getNeutralDismissal()Lcom/google/android/finsky/protos/DocAnnotations$Dismissal;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Dismissal;->getUrl()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$1;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;)V

    new-instance v7, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$2;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction$2;-><init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;)V

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/finsky/api/DfeApi;->rateSuggestedContent(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    return-void
.end method
