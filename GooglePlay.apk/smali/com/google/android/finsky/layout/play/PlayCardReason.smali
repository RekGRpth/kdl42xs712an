.class public Lcom/google/android/finsky/layout/play/PlayCardReason;
.super Lcom/google/android/finsky/layout/play/PlaySeparatorLayout;
.source "PlayCardReason.java"


# instance fields
.field private final mAvatarHeight:I

.field private mReason:Landroid/widget/TextView;

.field private mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

.field private mTextOnlyMarginLeft:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardReason;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlaySeparatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050    # com.android.vending.R.dimen.play_card_reason_avatar_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mAvatarHeight:I

    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;I)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Lcom/google/android/finsky/protos/Doc$Image;
    .param p3    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/FifeImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/FifeImageView;->setViewToFadeIn(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    :goto_0
    iput p4, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mTextOnlyMarginLeft:I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/FifeImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlaySeparatorLayout;->onFinishInflate()V

    const v0, 0x7f0801c7    # com.android.vending.R.id.li_reason

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardReason;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    const v0, 0x7f0801c6    # com.android.vending.R.id.li_thumbnail_reason

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardReason;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getHeight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getPaddingTop()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/FifeImageView;->getVisibility()I

    move-result v13

    const/16 v14, 0x8

    if-ne v13, v14, :cond_0

    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v9, v7, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mTextOnlyMarginLeft:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mTextOnlyMarginLeft:I

    add-int/2addr v15, v10

    add-int v16, v9, v8

    move/from16 v0, v16

    invoke-virtual {v13, v14, v9, v15, v0}, Landroid/widget/TextView;->layout(IIII)V

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v13, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int v12, v5, v13

    if-le v3, v8, :cond_1

    sub-int v13, v2, v3

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v6, v7, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    const/4 v14, 0x0

    add-int v15, v6, v3

    invoke-virtual {v13, v14, v6, v5, v15}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v11, v7, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    add-int v14, v12, v10

    add-int v15, v11, v8

    invoke-virtual {v13, v12, v11, v14, v15}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0

    :cond_1
    sub-int v13, v2, v8

    sub-int/2addr v13, v7

    div-int/lit8 v13, v13, 0x2

    add-int v1, v7, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    const/4 v14, 0x0

    add-int v15, v1, v3

    invoke-virtual {v13, v14, v1, v5, v15}, Lcom/google/android/finsky/layout/FifeImageView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    add-int v14, v12, v10

    add-int v15, v1, v8

    invoke-virtual {v13, v12, v1, v14, v15}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const/4 v7, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getPaddingLeft()I

    move-result v8

    sub-int v8, v6, v8

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getPaddingRight()I

    move-result v9

    sub-int v5, v8, v9

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/FifeImageView;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_0

    const/4 v3, 0x1

    :goto_0
    if-nez v3, :cond_1

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    iget v9, v2, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    iget v10, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/finsky/layout/FifeImageView;->measure(II)V

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredWidth()I

    move-result v8

    iget v9, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v8, v9

    sub-int/2addr v5, v8

    :goto_1
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-static {v5, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v7}, Landroid/widget/TextView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReason:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    if-eqz v3, :cond_2

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mAvatarHeight:I

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getPaddingTop()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardReason;->getPaddingBottom()I

    move-result v8

    add-int v1, v7, v8

    invoke-virtual {p0, v6, v1}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setMeasuredDimension(II)V

    return-void

    :cond_0
    move v3, v7

    goto :goto_0

    :cond_1
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mTextOnlyMarginLeft:I

    sub-int/2addr v5, v8

    goto :goto_1

    :cond_2
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardReason;->mReasonImage:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/FifeImageView;->getMeasuredHeight()I

    move-result v7

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2
.end method
