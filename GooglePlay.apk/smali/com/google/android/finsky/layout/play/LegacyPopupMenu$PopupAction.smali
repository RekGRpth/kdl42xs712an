.class Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;
.super Ljava/lang/Object;
.source "LegacyPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/play/LegacyPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PopupAction"
.end annotation


# instance fields
.field private final mActionListener:Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;

.field private final mIsEnabled:Z

.field private final mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;ZLcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z
    .param p3    # Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mTitle:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mIsEnabled:Z

    iput-object p3, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mActionListener:Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;)Z
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mIsEnabled:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;)Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/LegacyPopupMenu$PopupAction;->mActionListener:Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;

    return-object v0
.end method
