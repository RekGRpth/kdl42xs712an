.class public Lcom/google/android/finsky/layout/play/PlayTextView;
.super Landroid/widget/TextView;
.source "PlayTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayTextView$SelfishUrlSpan;
    }
.end annotation


# static fields
.field private static final RESPECT_COMPACT_MODE:Z


# instance fields
.field private final mCompactPercentage:I

.field private final mExpandable:Z

.field private mExpansionState:I

.field private mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private mLastLineFadeOutHintMargin:I

.field private mLastLineFadeOutSize:I

.field private final mLastLineOverdrawHint:Ljava/lang/String;

.field private mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

.field private mLastLineOverdrawPaint:Landroid/graphics/Paint;

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private final mSupportsHtmlLinks:Z

.field private final mToDrawOverLastLineIfNecessary:Z

.field private mUrlSpanClicked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/layout/play/PlayTextView;->RESPECT_COMPACT_MODE:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpansionState:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v3, Lcom/android/vending/R$styleable;->PlayTextView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    sget-boolean v3, Lcom/google/android/finsky/layout/play/PlayTextView;->RESPECT_COMPACT_MODE:Z

    if-eqz v3, :cond_3

    invoke-virtual {v2, v4, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    :goto_0
    iput v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    invoke-virtual {v2, v8}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    iget-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0a0001    # com.android.vending.R.color.white

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v8, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const v3, 0x7f0b0073    # com.android.vending.R.dimen.play_text_view_fadeout

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutSize:I

    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    sget-object v5, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    new-array v6, v9, [I

    const v7, 0xffffff

    and-int/2addr v7, v0

    aput v7, v6, v4

    aput v0, v6, v8

    invoke-direct {v3, v5, v6}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    const v3, 0x7f0b0074    # com.android.vending.R.dimen.play_text_view_fadeout_hint_margin

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutHintMargin:I

    :cond_0
    invoke-virtual {v2, v9}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    const v5, 0x7f0a0038    # com.android.vending.R.color.play_fg_secondary

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    const v5, 0x7f0b0099    # com.android.vending.R.dimen.play_medium_size

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_1
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mSupportsHtmlLinks:Z

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpandable:Z

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    if-lez v3, :cond_2

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getTextSize()F

    move-result v4

    mul-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/layout/play/PlayTextView;->setLineSpacing(FF)V

    :cond_2
    return-void

    :cond_3
    move v3, v4

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayTextView;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/layout/play/PlayTextView;

    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->handleClick()V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/finsky/layout/play/PlayTextView;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/finsky/layout/play/PlayTextView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mUrlSpanClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/play/PlayTextView;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0    # Lcom/google/android/finsky/layout/play/PlayTextView;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method private collapseContent()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpansionState:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->requestLayout()V

    return-void
.end method

.method private expandContent()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpansionState:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->requestLayout()V

    return-void
.end method

.method private handleClick()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v1}, Lcom/google/android/finsky/layout/play/PlayTextView;->scrollTo(II)V

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mUrlSpanClicked:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mUrlSpanClicked:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpandable:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpansionState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->collapseContent()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->expandContent()V

    goto :goto_0
.end method

.method private selfishifyUrlSpans(Ljava/lang/CharSequence;)V
    .locals 8

    const/4 v2, 0x0

    instance-of v0, p1, Landroid/text/Spannable;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    check-cast p1, Landroid/text/Spannable;

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-interface {p1, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v3, v0

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    invoke-interface {p1, v4}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-interface {p1, v4}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {p1, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    new-instance v7, Lcom/google/android/finsky/layout/play/PlayTextView$SelfishUrlSpan;

    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, p0, v4}, Lcom/google/android/finsky/layout/play/PlayTextView$SelfishUrlSpan;-><init>(Lcom/google/android/finsky/layout/play/PlayTextView;Ljava/lang/String;)V

    invoke-interface {p1, v7, v5, v6, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public isExpanded()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpandable:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpansionState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 22
    .param p1    # Landroid/graphics/Canvas;

    invoke-super/range {p0 .. p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getHeight()I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getWidth()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v14

    if-eqz v14, :cond_0

    const/16 v20, -0x1

    const/16 v18, -0x1

    const/4 v15, 0x0

    :goto_1
    invoke-virtual {v14}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    if-ge v15, v2, :cond_0

    invoke-virtual {v14, v15}, Landroid/text/Layout;->getLineTop(I)I

    move-result v9

    invoke-virtual {v14, v15}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v8

    if-ge v9, v11, :cond_4

    if-le v8, v11, :cond_4

    const/4 v3, 0x0

    int-to-float v4, v9

    move/from16 v0, v21

    int-to-float v5, v0

    int-to-float v6, v11

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    if-lez v15, :cond_0

    add-int/lit8 v2, v15, -0x1

    invoke-virtual {v14, v2}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v2

    float-to-int v0, v2

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getPaddingLeft()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getPaddingRight()I

    move-result v17

    add-int v10, v16, v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v13, v2

    sub-int v2, v21, v17

    sub-int v12, v2, v13

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutHintMargin:I

    sub-int v2, v12, v2

    move/from16 v0, v19

    if-ge v2, v0, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutHintMargin:I

    sub-int v10, v12, v2

    int-to-float v3, v10

    move/from16 v0, v20

    int-to-float v4, v0

    sub-int v2, v21, v17

    int-to-float v5, v2

    move/from16 v0, v18

    int-to-float v6, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    int-to-float v3, v12

    add-int/lit8 v4, v15, -0x1

    invoke-virtual {v14, v4}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutSize:I

    sub-int v3, v10, v3

    move/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v2, v3, v0, v10, v1}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    :cond_4
    move/from16 v20, v9

    move/from16 v18, v8

    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mCompactPercentage:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayTextView;->getLineHeight()I

    move-result v3

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/play/PlayTextView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setContent(Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mSupportsHtmlLinks:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/finsky/layout/play/PlayTextView;->selfishifyUrlSpans(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpandable:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayTextView;->mExpansionState:I

    :cond_1
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayTextView$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/play/PlayTextView$1;-><init>(Lcom/google/android/finsky/layout/play/PlayTextView;)V

    invoke-super {p0, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p2}, Lcom/google/android/finsky/layout/play/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Do not call this method directly"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
