.class public abstract Lcom/google/android/finsky/layout/play/PlayCardView;
.super Lcom/google/android/finsky/layout/AccessibleRelativeLayout;
.source "PlayCardView.java"

# interfaces
.implements Lcom/google/android/finsky/installer/InstallerListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
.implements Lcom/google/android/finsky/library/Libraries$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    }
.end annotation


# static fields
.field private static final CORE_IMAGE_TYPES:[I

.field private static final NO_OVERFLOW_FOCUS:Z


# instance fields
.field protected mAccessibilityOverlay:Landroid/view/View;

.field protected mCurrentPageUrl:Ljava/lang/String;

.field protected mDescription:Landroid/widget/TextView;

.field protected mDoc:Lcom/google/android/finsky/api/model/Document;

.field protected mIsDocOwned:Z

.field protected mItemBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

.field protected mLoadingIndicator:Landroid/view/View;

.field protected mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private final mOldOverflowArea:Landroid/graphics/Rect;

.field protected mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

.field private final mOverflowArea:Landroid/graphics/Rect;

.field private final mOverflowTouchExtend:I

.field private mOverlayMarginBottom:I

.field private mOverlayMarginLeft:I

.field private mOverlayMarginRight:I

.field private mOverlayMarginTop:I

.field private final mOwnershipRenderingType:I

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field protected mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

.field private final mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

.field protected mRatingBar:Landroid/widget/RatingBar;

.field protected mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

.field protected mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

.field private mSentImpression:Z

.field private final mShowInlineCreatorBadge:Z

.field protected mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

.field private final mSupportsSubtitleAndRating:Z

.field private final mTextOnlyReasonMarginLeft:I

.field protected mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

.field protected mThumbnailAspectRatio:F

.field protected mTitle:Landroid/widget/TextView;

.field private mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/layout/play/PlayCardView;->NO_OVERFLOW_FOCUS:Z

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardView;->CORE_IMAGE_TYPES:[I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :array_0
    .array-data 4
        0x4
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/AccessibleRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPlayStoreUiElementType()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSentImpression:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004b    # com.android.vending.R.dimen.play_card_overflow_touch_extend

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowTouchExtend:I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    new-instance v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    sget-object v1, Lcom/android/vending/R$styleable;->PlayCardView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mShowInlineCreatorBadge:Z

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSupportsSubtitleAndRating:Z

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0054    # com.android.vending.R.dimen.play_card_reason_text_extra_margin_left

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTextOnlyReasonMarginLeft:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOwnershipRenderingType:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private bindCore(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;ZZ)V
    .locals 19
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;
    .param p7    # Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    .param p8    # Z
    .param p9    # Z

    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSentImpression:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "Unexpected change in docId from %s to %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/layout/play/PlayCardView;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/layout/play/PlayCardView;->mCurrentPageUrl:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTitle:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/RatingBar;->setVisibility(I)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mItemBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mItemBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    :cond_3
    if-eqz p9, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mItemBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/finsky/utils/BadgeUtils;->configureRatingItemSection(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Landroid/widget/RatingBar;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    :cond_4
    if-eqz p8, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    const/16 v18, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_0
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mShowInlineCreatorBadge:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    const/4 v6, -0x1

    move-object/from16 v0, p2

    invoke-static {v4, v0, v5, v6}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;I)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getThumbnailImageTypes()[I

    move-result-object v5

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1, v5}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;[I)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->updatePriceLabel()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    if-eqz v4, :cond_7

    if-eqz p5, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz p5, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    const v5, 0x7f0a0044    # com.android.vending.R.color.play_dismissed_overlay

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 v4, 0x0

    move-object/from16 v0, v17

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    const/4 v4, 0x0

    move-object/from16 v0, v17

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    const/4 v4, 0x0

    move-object/from16 v0, v17

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    const/4 v4, 0x0

    move-object/from16 v0, v17

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setClickable(Z)V

    :cond_8
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v14}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getCorpusCellContentDescriptionResource(I)I

    move-result v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    const/4 v8, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v4, v15}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mLoadingIndicator:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    sget-boolean v4, Lcom/google/android/finsky/layout/play/PlayCardView;->NO_OVERFLOW_FOCUS:Z

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/view/View;->setNextFocusRightId(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->setNextFocusLeftId(I)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSentImpression:Z

    if-nez v4, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-interface {v4, v0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSentImpression:Z

    :cond_a
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/play/PlayCardView;->setVisibility(I)V

    return-void

    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v7}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v8

    move-object/from16 v6, p1

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p0

    invoke-virtual/range {v4 .. v13}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->configure(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto/16 :goto_1

    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    const v5, 0x7f020041    # com.android.vending.R.drawable.highlight_overlay

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverlayMarginTop:I

    move-object/from16 v0, v17

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverlayMarginBottom:I

    move-object/from16 v0, v17

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverlayMarginLeft:I

    move-object/from16 v0, v17

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverlayMarginRight:I

    move-object/from16 v0, v17

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    if-eqz p3, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    move-object/from16 v3, p0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private bindReason(Lcom/google/android/finsky/layout/play/PlayCardReason;Lcom/google/android/finsky/protos/DocAnnotations$Reason;Lcom/google/android/finsky/utils/BitmapLoader;)V
    .locals 22
    .param p1    # Lcom/google/android/finsky/layout/play/PlayCardReason;
    .param p2    # Lcom/google/android/finsky/protos/DocAnnotations$Reason;
    .param p3    # Lcom/google/android/finsky/utils/BitmapLoader;

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/protos/DocAnnotations$Reason;->hasReasonReview()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/protos/DocAnnotations$Reason;->getReasonReview()Lcom/google/android/finsky/protos/DocAnnotations$ReasonReview;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/protos/DocAnnotations$ReasonReview;->hasReview()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/protos/DocAnnotations$Reason;->getReasonReview()Lcom/google/android/finsky/protos/DocAnnotations$ReasonReview;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/protos/DocAnnotations$ReasonReview;->getReview()Lcom/google/android/finsky/protos/Rev$Review;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/finsky/protos/Rev$Review;->getPlusProfile()Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v16

    if-eqz v16, :cond_1

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getProfileImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v10

    :goto_1
    if-eqz v16, :cond_2

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_4

    invoke-virtual {v12}, Lcom/google/android/finsky/protos/Rev$Review;->hasComment()Z

    move-result v17

    if-eqz v17, :cond_3

    const v17, 0x7f070275    # com.android.vending.R.string.review_bold_formatting

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    invoke-virtual {v12}, Lcom/google/android/finsky/protos/Rev$Review;->getComment()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v13

    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTextOnlyReasonMarginLeft:I

    move/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move/from16 v2, v17

    invoke-virtual {v0, v13, v10, v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardReason;->bind(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;I)V

    goto :goto_0

    :cond_1
    const/4 v10, 0x0

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    :cond_3
    invoke-virtual {v12}, Lcom/google/android/finsky/protos/Rev$Review;->getStarRating()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->round(F)I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0e0004    # com.android.vending.R.plurals.star_rating_review_bold_formatting

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v15, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v13

    goto :goto_3

    :cond_4
    invoke-virtual {v12}, Lcom/google/android/finsky/protos/Rev$Review;->getComment()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTextOnlyReasonMarginLeft:I

    move/from16 v18, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, p3

    move/from16 v3, v18

    invoke-virtual {v0, v1, v10, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardReason;->bind(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/protos/DocAnnotations$Reason;->hasReasonPlusProfiles()Z

    move-result v17

    if-eqz v17, :cond_8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/protos/DocAnnotations$Reason;->getReasonPlusProfiles()Lcom/google/android/finsky/protos/DocAnnotations$ReasonPlusProfiles;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/protos/DocAnnotations$ReasonPlusProfiles;->getPlusProfileCount()I

    move-result v5

    const/16 v17, 0x1

    move/from16 v0, v17

    if-le v5, v0, :cond_6

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/google/android/finsky/protos/DocAnnotations$ReasonPlusProfiles;->getPlusProfile(I)Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v6

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/google/android/finsky/protos/DocAnnotations$ReasonPlusProfiles;->getPlusProfile(I)Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v14

    const v17, 0x7f070277    # com.android.vending.R.string.plus_one_multiple_friends_bold

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual {v6}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    invoke-virtual {v14}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v17

    invoke-virtual {v6}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getProfileImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTextOnlyReasonMarginLeft:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, p3

    move/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardReason;->bind(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;I)V

    goto/16 :goto_0

    :cond_6
    if-lez v5, :cond_7

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/google/android/finsky/protos/DocAnnotations$ReasonPlusProfiles;->getPlusProfile(I)Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v6

    const v17, 0x7f070276    # com.android.vending.R.string.plus_one_single_friend_bold

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual {v6}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v17

    invoke-virtual {v6}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getProfileImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTextOnlyReasonMarginLeft:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, p3

    move/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardReason;->bind(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;I)V

    goto/16 :goto_0

    :cond_7
    const-string v17, "Server returned plus profile reason with no profiles"

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    invoke-static/range {v17 .. v18}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/protos/DocAnnotations$Reason;->getDescriptionHtml()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v17

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTextOnlyReasonMarginLeft:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, p3

    move/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardReason;->bind(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;I)V

    goto/16 :goto_0
.end method

.method private updatePriceLabel()V
    .locals 13

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v3

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->stylePurchaseButton(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getContext()Landroid/content/Context;

    move-result-object v7

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mIsDocOwned:Z

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget-object v0, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelOffer:Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget-object v0, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelOffer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget v0, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelListingResourceId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    move-object v10, v9

    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v10, v2}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget v0, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelListingResourceId:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v9, v1, v2

    invoke-virtual {v7, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget v0, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelListingResourceId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget-boolean v0, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isOwned:Z

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mIsDocOwned:Z

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mIsDocOwned:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOwnershipRenderingType:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_6

    :cond_5
    const/4 v12, 0x1

    :goto_3
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mIsDocOwned:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOwnershipRenderingType:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    const/4 v11, 0x1

    :goto_4
    if-eqz v12, :cond_8

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelListingResourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    :goto_5
    if-eqz v11, :cond_9

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getOwnedItemIndicator(I)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_2

    :cond_6
    const/4 v12, 0x0

    goto :goto_3

    :cond_7
    const/4 v11, 0x0

    goto :goto_4

    :cond_8
    const/4 v10, 0x0

    goto :goto_5

    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_2
.end method


# virtual methods
.method public bindInList(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;FLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 13
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;
    .param p7    # Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    .param p8    # F
    .param p9    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->setVisibility(I)V

    const/4 v1, 0x0

    cmpg-float v1, p8, v1

    if-gtz v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v12

    invoke-virtual {p0, v12}, Lcom/google/android/finsky/layout/play/PlayCardView;->setThumbnailAspectRatio(F)V

    :goto_0
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardView;->shouldPreferRatingOverSubtitle(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v11

    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSupportsSubtitleAndRating:Z

    if-nez v1, :cond_0

    if-eqz v11, :cond_3

    :cond_0
    const/4 v10, 0x1

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSupportsSubtitleAndRating:Z

    if-nez v1, :cond_1

    if-nez v11, :cond_4

    :cond_1
    const/4 v9, 0x1

    :goto_2
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v10}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindCore(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;ZZ)V

    return-void

    :cond_2
    move/from16 v0, p8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->setThumbnailAspectRatio(F)V

    goto :goto_0

    :cond_3
    const/4 v10, 0x0

    goto :goto_1

    :cond_4
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public bindInList(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;
    .param p7    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v7, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object/from16 v6, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindInList(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;FLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method

.method public bindInStream(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;FLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 14
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    .param p7    # F
    .param p8    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v1, 0x0

    cmpg-float v1, p7, v1

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Aspect ratio must be set in the stream"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardView;->shouldPreferRatingOverSubtitle(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v10

    if-nez v10, :cond_2

    const/4 v9, 0x1

    :goto_0
    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v10}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindCore(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;ZZ)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-nez v1, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v9, 0x0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setSeparatorVisible(Z)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setSeparatorVisible(Z)V

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocAnnotations$SuggestionReasons;

    move-result-object v12

    if-eqz v12, :cond_1

    invoke-virtual {v12}, Lcom/google/android/finsky/protos/DocAnnotations$SuggestionReasons;->getReasonCount()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v12}, Lcom/google/android/finsky/protos/DocAnnotations$SuggestionReasons;->getReasonCount()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-nez v1, :cond_8

    :cond_6
    invoke-static {v12}, Lcom/google/android/finsky/utils/PlayUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocAnnotations$SuggestionReasons;)Lcom/google/android/finsky/protos/DocAnnotations$Reason;

    move-result-object v11

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    :goto_2
    move-object/from16 v0, p2

    invoke-direct {p0, v1, v11, v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindReason(Lcom/google/android/finsky/layout/play/PlayCardReason;Lcom/google/android/finsky/protos/DocAnnotations$Reason;Lcom/google/android/finsky/utils/BitmapLoader;)V

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setSeparatorVisible(Z)V

    invoke-static {v12}, Lcom/google/android/finsky/utils/PlayUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocAnnotations$SuggestionReasons;)Lcom/google/android/finsky/protos/DocAnnotations$Reason;

    move-result-object v11

    invoke-static {v12, v11}, Lcom/google/android/finsky/utils/PlayUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocAnnotations$SuggestionReasons;Lcom/google/android/finsky/protos/DocAnnotations$Reason;)Lcom/google/android/finsky/protos/DocAnnotations$Reason;

    move-result-object v13

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    move-object/from16 v0, p2

    invoke-direct {p0, v1, v11, v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindReason(Lcom/google/android/finsky/layout/play/PlayCardReason;Lcom/google/android/finsky/protos/DocAnnotations$Reason;Lcom/google/android/finsky/utils/BitmapLoader;)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    move-object/from16 v0, p2

    invoke-direct {p0, v1, v13, v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindReason(Lcom/google/android/finsky/layout/play/PlayCardReason;Lcom/google/android/finsky/protos/DocAnnotations$Reason;Lcom/google/android/finsky/utils/BitmapLoader;)V

    goto :goto_1
.end method

.method public bindLoading()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mLoadingIndicator:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mItemBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mItemBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setVisibility(I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardReason;->setVisibility(I)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->setVisibility(I)V

    :cond_7
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/play/PlayCardView;->setVisibility(I)V

    return-void
.end method

.method public bindNoDocument()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->setThumbnailAspectRatio(F)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->setVisibility(I)V

    return-void
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected abstract getPlayStoreUiElementType()I
.end method

.method protected getThumbnailImageTypes()[I
    .locals 1

    sget-object v0, Lcom/google/android/finsky/layout/play/PlayCardView;->CORE_IMAGE_TYPES:[I

    return-object v0
.end method

.method protected measureThumbnailSpanningHeight(I)V
    .locals 8
    .param p1    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingBottom()I

    move-result v1

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    sub-int v6, v0, v2

    sub-int/2addr v6, v1

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v3, v6, v7

    int-to-float v6, v3

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnailAspectRatio:F

    div-float/2addr v6, v7

    float-to-int v5, v6

    iput v5, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    return-void
.end method

.method protected measureThumbnailSpanningWidth(I)V
    .locals 8
    .param p1    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingRight()I

    move-result v2

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    sub-int v6, v0, v1

    sub-int/2addr v6, v2

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v5, v6, v7

    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnailAspectRatio:F

    int-to-float v7, v5

    mul-float/2addr v6, v7

    float-to-int v3, v6

    iput v3, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    return-void
.end method

.method public onAllLibrariesLoaded()V
    .locals 0

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/finsky/layout/AccessibleRelativeLayout;->onAttachedToWindow()V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->addListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->addListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/finsky/layout/AccessibleRelativeLayout;->onDetachedFromWindow()V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->removeListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->removeListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/finsky/layout/AccessibleRelativeLayout;->onFinishInflate()V

    const v1, 0x7f0800be    # com.android.vending.R.id.li_thumbnail_frame

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    const v1, 0x7f08010c    # com.android.vending.R.id.li_title

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTitle:Landroid/widget/TextView;

    const v1, 0x7f080143    # com.android.vending.R.id.li_subtitle

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    const v1, 0x7f080144    # com.android.vending.R.id.li_rating

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RatingBar;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mRatingBar:Landroid/widget/RatingBar;

    const v1, 0x7f0801c5    # com.android.vending.R.id.li_badge

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mItemBadge:Lcom/google/android/finsky/layout/DecoratedTextView;

    const v1, 0x7f0801bd    # com.android.vending.R.id.li_reason_1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardReason;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    const v1, 0x7f0801bc    # com.android.vending.R.id.li_reason_2

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardReason;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    const v1, 0x7f08010e    # com.android.vending.R.id.li_overflow

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    const v1, 0x7f08010d    # com.android.vending.R.id.li_price

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayActionButton;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const v1, 0x7f08010f    # com.android.vending.R.id.li_description

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    const v1, 0x7f0800d3    # com.android.vending.R.id.loading_progress_bar

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mLoadingIndicator:Landroid/view/View;

    const v1, 0x7f080020    # com.android.vending.R.id.accessibility_overlay

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverlayMarginTop:I

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverlayMarginBottom:I

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverlayMarginLeft:I

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverlayMarginRight:I

    return-void
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3    # I

    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->updatePriceLabel()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/AccessibleRelativeLayout;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v4, v1

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v5, v2

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v6, v1

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v7, v2

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getHitRect(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowTouchExtend:I

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowTouchExtend:I

    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->left:I

    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowTouchExtend:I

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v4, v3, Landroid/graphics/Rect;->right:I

    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowTouchExtend:I

    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    if-eq v3, v4, :cond_0

    :cond_2
    new-instance v3, Landroid/view/TouchDelegate;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-direct {v3, v4, v5}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/play/PlayCardView;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOldOverflowArea:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mOverflowArea:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public onLibraryContentsChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/library/AccountLibrary;

    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->updatePriceLabel()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/high16 v9, 0x40000000    # 2.0f

    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/AccessibleRelativeLayout;->onMeasure(II)V

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingLeft()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v2, v7, v8

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingTop()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v7, v8

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mAccessibilityOverlay:Landroid/view/View;

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/view/View;->measure(II)V

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v6, 0x0

    :goto_1
    invoke-virtual {v5}, Landroid/text/Layout;->getLineCount()I

    move-result v7

    if-ge v6, v7, :cond_0

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v3

    if-le v3, v4, :cond_3

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDescription:Landroid/widget/TextView;

    const/4 v7, 0x2

    if-lt v6, v7, :cond_2

    const/4 v7, 0x0

    :goto_2
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 v7, 0x4

    goto :goto_2

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public resetUiElementNode()V
    .locals 2

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getPlayStoreUiElementType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mSentImpression:Z

    return-void
.end method

.method public setDisplayIndex(I)V
    .locals 6
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0702ad    # com.android.vending.R.string.numbered_title

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    add-int/lit8 v5, p1, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setThumbnailAspectRatio(F)V
    .locals 1
    .param p1    # F

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnailAspectRatio:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mThumbnailAspectRatio:F

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->requestLayout()V

    :cond_0
    return-void
.end method

.method protected shouldPreferRatingOverSubtitle(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 2
    .param p1    # Lcom/google/android/finsky/api/model/Document;

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public supportsDisplayingReason()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason1:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardView;->mReason2:Lcom/google/android/finsky/layout/play/PlayCardReason;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
