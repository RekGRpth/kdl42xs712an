.class Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;
.super Ljava/lang/Object;
.source "PlayCardOverflowView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->configure(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field final synthetic val$onDismissListener:Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;

.field final synthetic val$onWishlistStatusListener:Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;

.field final synthetic val$referrerUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Landroid/accounts/Account;Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput-object p3, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p4, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p5, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$account:Landroid/accounts/Account;

    iput-object p6, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$onWishlistStatusListener:Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;

    iput-object p7, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$referrerUrl:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$onDismissListener:Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;

    iput-object p9, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p10, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 20
    .param p1    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/play/PopupMenuFactory;->getInstance(Landroid/content/Context;Landroid/view/View;)Lcom/google/android/finsky/layout/play/PlayPopupMenu;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v10

    const/16 v2, 0xee

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v10, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/WishlistHelper;->shouldHideWishlistAction(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$account:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/WishlistHelper;->isInWishlist(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Z

    move-result v2

    if-eqz v2, :cond_4

    const v18, 0x7f0702f8    # com.android.vending.R.string.wishlist_remove

    const/16 v19, 0xcd

    :goto_0
    move/from16 v11, v19

    new-instance v17, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v10, v11}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;Lcom/google/android/finsky/analytics/FinskyEventLog;I)V

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-interface {v13, v2, v3, v0}, Lcom/google/android/finsky/layout/play/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$onDismissListener:Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v2}, Lcom/google/android/finsky/utils/PlayUtils;->isDismissable(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f0702f9    # com.android.vending.R.string.not_interested

    invoke-virtual {v14, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    new-instance v2, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$onDismissListener:Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$referrerUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;-><init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-interface {v13, v8, v9, v2}, Lcom/google/android/finsky/layout/play/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V

    :cond_1
    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v12

    if-eqz v12, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getSubscriptionsList()Ljava/util/List;

    move-result-object v2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$account:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xe2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$context:Landroid/content/Context;

    const v3, 0x7f07014c    # com.android.vending.R.string.magazine_subscribe

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-instance v4, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v15}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$2;-><init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;Landroid/view/View$OnClickListener;)V

    invoke-interface {v13, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V

    const/16 v16, 0x0

    :cond_2
    :goto_1
    if-eqz v16, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$account:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$referrerUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v3, v13

    # invokes: Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->configureMenuPrice(Lcom/google/android/finsky/layout/play/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    invoke-static/range {v2 .. v9}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->access$200(Lcom/google/android/finsky/layout/play/PlayCardOverflowView;Lcom/google/android/finsky/layout/play/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_3
    invoke-interface {v13}, Lcom/google/android/finsky/layout/play/PlayPopupMenu;->show()V

    return-void

    :cond_4
    const v18, 0x7f0702f7    # com.android.vending.R.string.wishlist_add

    const/16 v19, 0xcc

    goto/16 :goto_0

    :cond_5
    move/from16 v16, v12

    goto :goto_1
.end method
