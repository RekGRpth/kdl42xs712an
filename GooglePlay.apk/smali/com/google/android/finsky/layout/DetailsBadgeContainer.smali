.class public Lcom/google/android/finsky/layout/DetailsBadgeContainer;
.super Lcom/google/android/finsky/layout/SeparatorLinearLayout;
.source "DetailsBadgeContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/DetailsBadgeContainer$BadgeContainerInfoDialog;,
        Lcom/google/android/finsky/layout/DetailsBadgeContainer$ParcelableBadgeContainerData;
    }
.end annotation


# instance fields
.field private mBadgesContainer:Landroid/widget/LinearLayout;

.field private mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsBadgeContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public configure(Landroid/support/v4/app/FragmentManager;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;)V
    .locals 26
    .param p1    # Landroid/support/v4/app/FragmentManager;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/api/model/Document;
    .param p4    # Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->getBadgeCount()I

    move-result v12

    if-nez v12, :cond_0

    const/16 v3, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->setVisibility(I)V

    const/4 v6, 0x0

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->getImageList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/protos/Doc$Image;

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/finsky/protos/Doc$Image;->getImageType()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_1

    move-object/from16 v6, v22

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->getTitle()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(Lcom/google/android/finsky/utils/BitmapLoader;ILcom/google/android/finsky/protos/Doc$Image;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setBackgroundResource(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->mBadgesContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    add-int/lit8 v3, v12, 0x1

    div-int/lit8 v20, v3, 0x2

    const/16 v19, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->getContext()Landroid/content/Context;

    move-result-object v18

    const/16 v24, 0x0

    :goto_1
    move/from16 v0, v24

    move/from16 v1, v20

    if-ge v0, v1, :cond_6

    const v3, 0x7f04000d    # com.android.vending.R.layout.badge_container_row

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->mBadgesContainer:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/view/ViewGroup;

    const/16 v17, 0x0

    :goto_2
    const/4 v3, 0x2

    move/from16 v0, v17

    if-ge v0, v3, :cond_5

    move/from16 v0, v19

    if-ge v0, v12, :cond_3

    const v3, 0x7f04000e    # com.android.vending.R.layout.badge_container_single

    const/4 v4, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup;

    move-object/from16 v0, p4

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->getBadge(I)Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v11

    const v3, 0x7f080028    # com.android.vending.R.id.badge_icon

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/layout/FifeImageView;

    const/4 v3, 0x6

    move-object/from16 v0, v18

    invoke-static {v0, v11, v3}, Lcom/google/android/finsky/utils/BadgeUtils;->getImage(Landroid/content/Context;Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v14

    if-eqz v14, :cond_4

    move-object/from16 v0, p2

    invoke-virtual {v13, v14, v0}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    :goto_3
    const v3, 0x7f080029    # com.android.vending.R.id.badge_text

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    invoke-virtual {v11}, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_3
    add-int/lit8 v19, v19, 0x1

    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    :cond_4
    const/16 v3, 0x8

    invoke-virtual {v13, v3}, Lcom/google/android/finsky/layout/FifeImageView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->mBadgesContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v24, v24, 0x1

    goto :goto_1

    :cond_6
    new-instance v3, Lcom/google/android/finsky/layout/DetailsBadgeContainer$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/finsky/layout/DetailsBadgeContainer$1;-><init>(Lcom/google/android/finsky/layout/DetailsBadgeContainer;Landroid/support/v4/app/FragmentManager;Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->onFinishInflate()V

    const v0, 0x7f0800de    # com.android.vending.R.id.cluster_header

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    const v0, 0x7f0800b6    # com.android.vending.R.id.badges_container

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBadgeContainer;->mBadgesContainer:Landroid/widget/LinearLayout;

    return-void
.end method
