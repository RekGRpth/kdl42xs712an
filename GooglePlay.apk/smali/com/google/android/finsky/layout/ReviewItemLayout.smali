.class public Lcom/google/android/finsky/layout/ReviewItemLayout;
.super Landroid/widget/RelativeLayout;
.source "ReviewItemLayout.java"


# instance fields
.field private mBody:Landroid/widget/TextView;

.field private mHeader:Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;

.field private mMetadata:Landroid/widget/TextView;

.field private mProfilePicture:Lcom/google/android/finsky/layout/FifeImageView;

.field private mRatingImage:Landroid/view/View;

.field private mRatingSeparator:Landroid/view/View;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private getReviewExtraInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Rev$Review;)Ljava/lang/String;
    .locals 12
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/protos/Rev$Review;

    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v10

    const/4 v11, 0x3

    if-eq v10, v11, :cond_1

    move-object v2, v9

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/finsky/protos/Rev$Review;->getDocumentVersion()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lcom/google/android/finsky/protos/Rev$Review;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    move v4, v7

    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    move v3, v7

    :goto_2
    if-nez v4, :cond_4

    if-nez v3, :cond_0

    move-object v2, v9

    goto :goto_0

    :cond_2
    move v4, v8

    goto :goto_1

    :cond_3
    move v3, v8

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionString()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->getVersionString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    move v5, v7

    :goto_3
    if-eqz v5, :cond_6

    if-nez v3, :cond_0

    move-object v2, v9

    goto :goto_0

    :cond_5
    move v5, v8

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v3, :cond_7

    const v9, 0x7f070194    # com.android.vending.R.string.review_older_version_with_device_name

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-virtual {v1, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    :goto_4
    move-object v2, v7

    goto :goto_0

    :cond_7
    const v7, 0x7f070195    # com.android.vending.R.string.review_older_version

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_4
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f080226    # com.android.vending.R.id.review_title

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mTitle:Landroid/widget/TextView;

    const v0, 0x7f080227    # com.android.vending.R.id.review_header

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mHeader:Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;

    const v0, 0x7f08022f    # com.android.vending.R.id.review_text

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mBody:Landroid/widget/TextView;

    const v0, 0x7f08022e    # com.android.vending.R.id.review_metadata

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mMetadata:Landroid/widget/TextView;

    const v0, 0x7f080223    # com.android.vending.R.id.review_rating_image

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingImage:Landroid/view/View;

    const v0, 0x7f080224    # com.android.vending.R.id.review_rating_separator

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingSeparator:Landroid/view/View;

    const v0, 0x7f080215    # com.android.vending.R.id.user_profile_image

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewItemLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/finsky/layout/FifeImageView;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingImage:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingImage:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingSeparator:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v0, v1, v2

    iget-object v1, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingSeparator:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingSeparator:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingSeparator:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingSeparator:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method public setRateReviewClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingImage:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setReviewInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Rev$Review;)V
    .locals 9
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/protos/Rev$Review;

    const/16 v7, 0x8

    const/4 v6, 0x0

    invoke-virtual {p2}, Lcom/google/android/finsky/protos/Rev$Review;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mHeader:Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;

    invoke-virtual {v5, p2}, Lcom/google/android/finsky/layout/ReviewItemHeaderLayout;->setReviewInfo(Lcom/google/android/finsky/protos/Rev$Review;)V

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mBody:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/finsky/protos/Rev$Review;->getComment()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/finsky/protos/Rev$Review;->getCommentId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    move v3, v6

    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingImage:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mRatingSeparator:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/ReviewItemLayout;->getReviewExtraInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Rev$Review;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mMetadata:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    sget-object v5, Lcom/google/android/finsky/config/G;->enableGooglePlusReviews:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p2}, Lcom/google/android/finsky/protos/Rev$Review;->getPlusProfile()Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v2

    :goto_4
    if-eqz v2, :cond_5

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getProfileImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v7

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/finsky/utils/BitmapLoader;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/layout/FifeImageView;->setVisibility(I)V

    :goto_5
    return-void

    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1

    :cond_2
    move v3, v7

    goto :goto_2

    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mMetadata:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mMetadata:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewItemLayout;->mProfilePicture:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/layout/FifeImageView;->setVisibility(I)V

    goto :goto_5
.end method
