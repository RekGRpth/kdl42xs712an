.class Lcom/google/android/finsky/layout/YoutubeFrameView$1;
.super Ljava/lang/Object;
.source "YoutubeFrameView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/YoutubeFrameView;->showPlayIcon(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/YoutubeFrameView;

.field final synthetic val$currentPageUrl:Ljava/lang/String;

.field final synthetic val$youtubeUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/YoutubeFrameView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->this$0:Lcom/google/android/finsky/layout/YoutubeFrameView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$currentPageUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$youtubeUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$currentPageUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$youtubeUrl:Ljava/lang/String;

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$currentPageUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$youtubeUrl:Ljava/lang/String;

    move-object v4, v1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClick(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->this$0:Lcom/google/android/finsky/layout/YoutubeFrameView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$youtubeUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/IntentUtils;->createYouTubeIntentForUrl(Landroid/content/pm/PackageManager;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->this$0:Lcom/google/android/finsky/layout/YoutubeFrameView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
