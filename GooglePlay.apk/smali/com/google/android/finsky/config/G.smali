.class public Lcom/google/android/finsky/config/G;
.super Ljava/lang/Object;
.source "G.java"


# static fields
.field public static final GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

.field public static final additionalExperiments:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static analyticsEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static antiMalwareActivityEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final appRestoreHoldoffMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static appsWidgetUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateDialogFirstBackoffDays:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateDialogSecondBackoffDays:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final autoUpdateDialogSubsequentBackoffDays:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static bitmapLoaderCacheSizeOverrideMb:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static bitmapLoaderCacheSizeRatioToScreen:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static booksWidgetUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static checkoutAuthTokenType:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final checkoutEscrowUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static checkoutPrivacyPolicyUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static consumptionAppDataFilter:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final consumptionAppImageTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final consumptionAppTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static contentFilterInfoUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final continueUrlExpirationMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneBootDelayMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneImmediateDelayMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneRegularIntervalMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dailyHygieneRetryIntervalMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final dcb2SetupDelayTosLoading:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static dcb3PrepareSendDeviceRebooted:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static dcb3SetupWhileRoamingEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static dcb3VerifyAssociationRetries:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static dcbDebugOptionsEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static debugAlwaysPromptForGaiaRecovery:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static debugImageSizes:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static debugOptionsEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final deliveryDataExpirationMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static dfeLogsAdMobEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static dfeLogsMainEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadBytesOverMobileMaximum:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadBytesOverMobileRecommended:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadPatchFreeSpaceFactor:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final downloadSendBaseVersionCode:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableCorpusWidgets:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableDcbReducedBillingAddress:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableGaiaRecovery:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableGooglePlusReviews:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enablePeopleSuggestWidget:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enablePlayLogs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableRedeemGiftCardMenu:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableRemoveAppsFromLibrary:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableReviewComments:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableSensitiveLogging:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableSessionStatsLog:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final enableThirdPartyDirectPurchases:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static enableUiTestLogStreamer:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static eventTagsWhitelist:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final explorerEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final explorerMaxDiskCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final explorerMinBufferSecs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final explorerPrecacheSecs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final explorerPrefetchThreads:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final explorerPreviewSecs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final explorerSampleRateKbit:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static extraMenuItemTitle:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static extraMenuItemUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final gaiaAuthValidityMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final gaiaOptOutLearnMoreLink:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static gamesBrowseUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final gmsCorePackageName:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static gtvNavigationalSuggestEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static iabV3MaxPurchasesInResponse:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final iabV3SubscriptionsEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static iabV3VerboseLogEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static imageCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final initializeBillingDelayMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final instrumentCheckLifetimeMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final launchUrlsEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final lightPurchaseAutoDismissMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final lightPurchaseOptimisticProvisioning:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final logBillingEventsEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static logsDispatchIntervalSeconds:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static mainCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final maxAutoUpdateDialogShownCount:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static maxLogQueueSizeBeforeFlush:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final minimumNumberOfRecs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final minimumVideosVersionCodeHoneycombPlus:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final minimumVideosVersionCodePreHoneycomb:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static moviesWidgetUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final musicDmcaReportLink:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static musicWidgetUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgeMidtermScores:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetAlwaysIncludeMostRecentBackend:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetHasContentScoreMap:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetImportantContributionFraction:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetInteractionClusterMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetMidtermDurationMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetShorttermDecay:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetShorttermDurationMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final myLibraryWidgetShorttermScores:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpPackageName:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final nlpSharedUserId:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final passwordHelpUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final passwordPurchaseRestrictOnByDefault:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final percentOfImageSize2G:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final percentOfImageSize3G:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final percentOfImageSize4G:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final percentOfImageSizeWifi:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final placesApiBiasRadiusMeters:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final placesApiKey:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static platformAntiMalwareDialogDelayMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static platformAntiMalwareEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogDelayBetweenUploadsMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogImpressionSettleTimeMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogMaxStorageSize:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogMinDelayBetweenUploadsMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogNumberOfFiles:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final playLogRecommendedFileSize:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final postPurchaseSharingEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final prePurchaseSharingEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static readBookUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final recommendationTtlMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final recommendationsFetchTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final recommendationsWidgetFlipIntervalMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static recsCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final reducedBillingAddressRequiresPhonenumber:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final selectAddinstrumentByDefault:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final skipAutoUpdateCleanupClientVersionCheck:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final subscriptionsEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final tabskyMinVersion:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final tentativeGcRunnerEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final theItBlacklist:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAddressServerUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAlarmExpirationTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAlarmMaxTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAlarmMinTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingAlwaysSendConfig:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static vendingAuthTokenType:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingBillingCountriesRefreshMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingCarrierCredentialsBufferMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingCarrierProvisioningRefreshFrequencyMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingCarrierProvisioningRetryMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingCarrierProvisioningUseTosVersion:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingContentRating:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbConnectionType:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbMmsFallback:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbPollTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbProxyHost:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingDcbProxyPort:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingHideContentRating:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingRequestTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static vendingSecureAuthTokenType:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final vendingSupportUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final volleyBufferPoolSizeKb:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final warmWelcomeFillColor:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final warmWelcomeGraphicUrl:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final whitelistedPlacesApiCountries:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final wishlistEnabled:Lcom/google/android/finsky/config/GservicesValue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android_id"

    aput-object v1, v0, v4

    const-string v1, "finsky."

    aput-object v1, v0, v3

    const-string v1, "vending_"

    aput-object v1, v0, v6

    const/4 v1, 0x3

    const-string v2, "logging_id2"

    aput-object v2, v0, v1

    const-string v1, "market_client_id"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/finsky/config/G;->GSERVICES_KEY_PREFIXES:[Ljava/lang/String;

    const-string v0, "finsky.bitmap_loader_cache_size_mb"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->bitmapLoaderCacheSizeOverrideMb:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.bitmap_loader_cache_size_ratio_to_screen"

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->bitmapLoaderCacheSizeRatioToScreen:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.main_cache_size_mb"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->mainCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.image_cache_size_mb"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->imageCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.recs_cache_size_mb"

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recsCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.debug_options_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->debugOptionsEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.debug_display_image_sizes"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->debugImageSizes:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.always_prompt_for_gaia_recovery"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->debugAlwaysPromptForGaiaRecovery:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_gaia_recovery"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableGaiaRecovery:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.gtv_navigational_suggestions_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gtvNavigationalSuggestEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.extra_menu_item_title"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->extraMenuItemTitle:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.extra_menu_item_url"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->extraMenuItemUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_redeem_gift_card_menu"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableRedeemGiftCardMenu:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.games_browse_url"

    const-string v1, "browse?c=3&cat=GAME"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gamesBrowseUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.dfe_logs_main_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dfeLogsMainEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.dfe_logs_admob_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dfeLogsAdMobEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.logs_dispatch_interval_seconds"

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->logsDispatchIntervalSeconds:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.maximum_log_queue_size"

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->maxLogQueueSizeBeforeFlush:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending.auth_token_type"

    const-string v1, "android"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAuthTokenType:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending.secure_auth_token_type"

    const-string v1, "androidsecure"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingSecureAuthTokenType:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.checkout_auth_token_type"

    const-string v1, "sierra"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->checkoutAuthTokenType:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.checkout_privacy_policy_url"

    const-string v1, "https://checkout.google.com/files/privacy.html?hl=%lang%"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->checkoutPrivacyPolicyUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_corpus_widgets"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableCorpusWidgets:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.widget_url"

    const-string v1, "promo?c=3"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appsWidgetUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.music_widget_url"

    const-string v1, "promo?c=2"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->musicWidgetUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.books_widget_url"

    const-string v1, "promo?c=1"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->booksWidgetUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.movies_widget_url"

    const-string v1, "promo?c=4"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->moviesWidgetUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.consumption_app_data_filter"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->consumptionAppDataFilter:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.read_book_url"

    const-string v1, "http://books.google.com/ebooks/reader"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->readBookUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.content_filter_info_url"

    const-string v1, "http://www.google.com/support/androidmarket/bin/answer.py?answer=1075738"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->contentFilterInfoUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.analytics_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->analyticsEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.checkout_escrow_url"

    const-string v1, "https://checkout.google.com/dehEfe"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->checkoutEscrowUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.music_dmca_report_link"

    const-string v1, "https://www.google.com/support/artists?p=DMCA"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->musicDmcaReportLink:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.gaia_opt_out_learn_more_link"

    const-string v1, "http://support.google.com/googleplay/bin/answer.py?&answer=2889951"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gaiaOptOutLearnMoreLink:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.initialize_billing_delay_ms"

    const/16 v1, 0x2710

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->initializeBillingDelayMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.log_billing_events_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->logBillingEventsEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_sensitive_logging"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableSensitiveLogging:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_playlog"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enablePlayLogs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.event_tags_whitelist"

    const-string v1, "reviews.plusDeclined,reviews.plusAccepted,reviews.plusShowSignup,googleplus.signedUp,plusOne"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->eventTagsWhitelist:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_uitest_log_streamer"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableUiTestLogStreamer:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.playlog_number_of_files"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogNumberOfFiles:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.playlog_max_storage_size_bytes"

    const-wide/32 v1, 0x200000

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogMaxStorageSize:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.playlog_recommended_file_size_bytes"

    const-wide/32 v1, 0x8000

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogRecommendedFileSize:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.playlog_delay_between_uploads_ms"

    const-wide/32 v1, 0x493e0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogDelayBetweenUploadsMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.playlog_min_delay_between_uploads_ms"

    const-wide/32 v1, 0xea60

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogMinDelayBetweenUploadsMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.playlog_impression_settle_time_ms"

    const-wide/16 v1, 0x7d0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->playLogImpressionSettleTimeMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.vending_request_timeout_ms"

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingRequestTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.tabsky_min_version"

    const/16 v1, 0x6e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->tabskyMinVersion:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.volley_buffer_pool_size_kb"

    const/16 v1, 0x100

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->volleyBufferPoolSizeKb:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.reduced_billing_address_requires_phonenumber"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->reducedBillingAddressRequiresPhonenumber:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_dcb_reduced_billing_address"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableDcbReducedBillingAddress:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_always_send_config"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAlwaysSendConfig:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_address_server_url"

    const-string v1, "http://i18napis.appspot.com/address"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAddressServerUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_carrier_ref_freq_ms"

    const-wide/32 v1, 0x240c8400

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingCarrierProvisioningRefreshFrequencyMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_carrier_prov_retry_ms"

    const-wide/32 v1, 0x1d4c0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingCarrierProvisioningRetryMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_carrier_prov_use_tos_version"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingCarrierProvisioningUseTosVersion:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_dcb_connection_type"

    const-string v1, "HIPRI"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbConnectionType:Lcom/google/android/finsky/config/GservicesValue;

    const-string v1, "vending_dcb_proxy_host"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbProxyHost:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_dcb_proxy_port"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbProxyPort:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_dcb_poll_timeout_ms"

    const/16 v1, 0x1388

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbPollTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_dcb_mms_fallback"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingDcbMmsFallback:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_billingcountries_refresh_ms"

    const-wide/32 v1, 0x19bfcc00

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingBillingCountriesRefreshMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_carrier_cred_buf_ms"

    const-wide/32 v1, 0x493e0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingCarrierCredentialsBufferMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_content_rating"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingContentRating:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_hide_content_rating"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingHideContentRating:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.dcb_debug_options_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcbDebugOptionsEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.dcb3_setup_while_roaming_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb3SetupWhileRoamingEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.dcb3_verify_association_retries"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb3VerifyAssociationRetries:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.dcb3_prepare_send_device_rebooted"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb3PrepareSendDeviceRebooted:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_support_url"

    const-string v1, "https://support.google.com/androidmarket/"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingSupportUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_alarm_max_timeout_ms"

    const-wide/32 v1, 0x5265c00

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAlarmMaxTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_alarm_expiration_timeout_ms"

    const-wide/32 v1, 0x240c8400

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAlarmExpirationTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "vending_alarm_min_timeout_ms"

    const-wide/16 v1, 0x2710

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->vendingAlarmMinTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.music_explorer_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->explorerEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.explorer_prefetch_threads"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->explorerPrefetchThreads:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.explorer_sample_rate_kbit"

    const/16 v1, 0x40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->explorerSampleRateKbit:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.explorer_precache_secs"

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->explorerPrecacheSecs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.explorer_min_buffer_secs"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->explorerMinBufferSecs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.explorer_preview_secs"

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->explorerPreviewSecs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.explorer_disk_cache_size_mb"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->explorerMaxDiskCacheSizeMb:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.instrument_check_lifetime_ms"

    const-wide/32 v1, 0x19bfcc00

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->instrumentCheckLifetimeMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.select_add_instrument_by_default"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->selectAddinstrumentByDefault:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.post_purchase_sharing_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->postPurchaseSharingEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.pre_purchase_sharing_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->prePurchaseSharingEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.launch_urls_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->launchUrlsEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.subscriptions_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->subscriptionsEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_third_party_direct_purchases"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableThirdPartyDirectPurchases:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.daily_hygiene_regular_interval_ms"

    const-wide/32 v1, 0x5265c00

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneRegularIntervalMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.daily_hygiene_retry_interval_ms"

    const-wide/32 v1, 0x927c0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneRetryIntervalMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.daily_hygiene_boot_delay_ms"

    const-wide/16 v1, 0x7530

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneBootDelayMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.daily_hygiene_immediate_delay_ms"

    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneImmediateDelayMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.continue_url_expiration_ms"

    const-wide/32 v1, 0x124f80

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->continueUrlExpirationMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.play_services_package_name"

    const-string v1, "com.google.android.gms"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gmsCorePackageName:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.nlp_shared_user_id"

    const-string v1, "com.google.uid.shared"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpSharedUserId:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.nlp_package_name"

    const-string v1, "com.google.android.location"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->nlpPackageName:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.download_bytes_mobile_recommended"

    const-wide/32 v1, 0x3200000

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadBytesOverMobileRecommended:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.download_bytes_mobile_maximum"

    const-wide v1, 0x7fffffffffffffffL

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadBytesOverMobileMaximum:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.download_send_base_version"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadSendBaseVersionCode:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.download_deliverydata_expiration"

    const-wide/32 v1, 0xdbba00

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->deliveryDataExpirationMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.download_patch_free_space_factor"

    const/16 v1, 0xd2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->downloadPatchFreeSpaceFactor:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.app_restore_holdoff_ms"

    const-wide/32 v1, 0x1d4c0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->appRestoreHoldoffMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_widget_interactions_cluster_ms"

    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetInteractionClusterMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_widget_shortterm_duration_ms"

    const-wide/32 v1, 0x5265c00

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetShorttermDurationMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_widget_midterm_duration_ms"

    const-wide/32 v1, 0x240c8400

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetMidtermDurationMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_widget_important_contribution_fraction"

    const v1, 0x3e4ccccd    # 0.2f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetImportantContributionFraction:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_widget_shortterm_decay"

    const v1, 0x3f4ccccd    # 0.8f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetShorttermDecay:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_has_content_scores"

    const-string v1, "0:15"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetHasContentScoreMap:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_widget_shortterm_scores"

    const-string v1, "0:5"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetShorttermScores:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_widget_midterm_scores"

    const-string v1, "0:1"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgeMidtermScores:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.my_library_widget_always_include_most_recent"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetAlwaysIncludeMostRecentBackend:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.recs_widget_flip_interval_ms"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recommendationsWidgetFlipIntervalMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.consumption_app_timeout_ms"

    const-wide/16 v1, 0x1b58

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->consumptionAppTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.consumption_app_image_timeout_ms"

    const-wide/16 v1, 0x1388

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->consumptionAppImageTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.recommendations_fetch_timeout_ms"

    const-wide/16 v1, 0x3a98

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recommendationsFetchTimeoutMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.wishlist_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->wishlistEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.minimum_videos_app_version_code_pre_honeycomb"

    const/16 v1, 0x7d9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->minimumVideosVersionCodePreHoneycomb:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.minimum_videos_app_version_code_post_honeycomb"

    const/16 v1, 0x4e7a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->minimumVideosVersionCodeHoneycombPlus:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.minimum_number_of_recommendations"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->minimumNumberOfRecs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.recommendation_ttl_ms"

    const-wide/32 v1, 0xa4cb800

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->recommendationTtlMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.gaia_auth_validity_ms"

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->gaiaAuthValidityMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.platform_anti_malware_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->platformAntiMalwareEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.platform_anti_malware_dialog_delay_ms"

    const-wide/32 v1, 0x36ee80

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->platformAntiMalwareDialogDelayMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.anti_malware_activity_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->antiMalwareActivityEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_google_plus_reviews"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableGooglePlusReviews:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_review_comments"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableReviewComments:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_remove_apps_from_library"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableRemoveAppsFromLibrary:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.iabv3_verbose_log_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3VerboseLogEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.iabv3_subscriptions_enabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3SubscriptionsEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.iabv3_max_purchases_in_response"

    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->iabV3MaxPurchasesInResponse:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.places_api_bias_radius_meters"

    const v1, 0xc350

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->placesApiBiasRadiusMeters:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.tentative_gc_runner_enabled"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->tentativeGcRunnerEnabled:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.whitelisted_places_api_countries"

    const-string v1, "0:CA,CH,US"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->whitelistedPlacesApiCountries:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.places_api_key"

    const-string v1, "AIzaSyCsc46SHdb57jPJa9fo7euV_HBDgwdrXsE"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->placesApiKey:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_people_suggest_widget"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enablePeopleSuggestWidget:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.light_purchase_auto_dismiss_ms"

    const-wide/16 v1, 0x9c4

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->lightPurchaseAutoDismissMs:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.light_purchase_optimistic_provisioning"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->lightPurchaseOptimisticProvisioning:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.password_purchase_restrict_on_by_default"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->passwordPurchaseRestrictOnByDefault:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.password_help_url"

    const-string v1, "https://accounts.google.com/RecoverAccount?fpOnly=1&Email=%email%"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->passwordHelpUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.percent_of_image_size_wifi"

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->percentOfImageSizeWifi:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.percent_of_image_size_4g"

    const v1, 0x3f666666    # 0.9f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->percentOfImageSize4G:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.percent_of_image_size_3g"

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->percentOfImageSize3G:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.percent_of_image_size_2g"

    const v1, 0x3ee66666    # 0.45f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->percentOfImageSize2G:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.max_times_auto_update_cleanup_shown"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->maxAutoUpdateDialogShownCount:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.auto_update_cleanup_first_backoff_days"

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateDialogFirstBackoffDays:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.auto_update_cleanup_second_backoff_days"

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateDialogSecondBackoffDays:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.auto_update_cleanup_subsequent_backoff_days"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->autoUpdateDialogSubsequentBackoffDays:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.skip_auto_update_cleanup_client_version_check"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->skipAutoUpdateCleanupClientVersionCheck:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.playWarmWelcome.graphicUrl"

    const-string v1, "https://lh5.ggpht.com/SN4RcNICTFkWgSoAfjWZW_HnO11a4RoSzwbBDyBKggBwGsnVOJydto2z2slK6t38jAQ"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->warmWelcomeGraphicUrl:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.playWarmWelcome.fillColor"

    const v1, -0xab1d34

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->warmWelcomeFillColor:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.enable_session_stats_log"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->enableSessionStatsLog:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.dcb2_setup_delay_tos_loading"

    const-wide/16 v1, 0xbb8

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->dcb2SetupDelayTosLoading:Lcom/google/android/finsky/config/GservicesValue;

    const-string v0, "finsky.nautilus_backend_docid_blacklist"

    const-string v1, "Saeaaaaaa"

    invoke-static {v0, v1}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->theItBlacklist:Lcom/google/android/finsky/config/GservicesValue;

    const-string v1, "finsky.additional_experiments"

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/finsky/config/GservicesValue;->value(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/config/GservicesValue;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/config/G;->additionalExperiments:Lcom/google/android/finsky/config/GservicesValue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
