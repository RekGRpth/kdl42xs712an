.class public Lcom/google/android/finsky/exploreactivity/ExploreApplication;
.super Lcom/jme3/app/Application;
.source "ExploreApplication.java"

# interfaces
.implements Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;
.implements Lcom/jme3/input/controls/TouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/exploreactivity/ExploreApplication$4;
    }
.end annotation


# instance fields
.field private final collisionResults:Lcom/jme3/collision/CollisionResults;

.field private final delta:Lcom/jme3/math/Vector3f;

.field private mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

.field private mBackground:Lcom/jme3/scene/Geometry;

.field private final mCameraLocation:Lcom/jme3/math/Vector3f;

.field private mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

.field private mInRotation:Z

.field private mInScroll:Z

.field private mLastMoveX:F

.field private mLastMoveY:F

.field mLastUpdate:J

.field private final mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

.field private mOriginalCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

.field private final mParentLayout:Landroid/widget/RelativeLayout;

.field private mPlaybackControls:Landroid/widget/TextView;

.field private mPreviousNodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/exploreactivity/DocumentNode;",
            ">;"
        }
    .end annotation
.end field

.field private mRootNode:Lcom/jme3/scene/Node;

.field private final mScreenBounds:[Lcom/jme3/math/Vector2f;

.field mScreenHeight:F

.field private mScreenScaleFactor:F

.field mScreenWidth:F

.field private final mScrollThreshold:F

.field private mSeedDocument:Lcom/google/android/finsky/api/model/Document;

.field private mSwipe:Lcom/jme3/scene/Geometry;

.field private mSwipeFader:Lcom/google/android/finsky/exploreactivity/FadeAdapter;

.field private mTouch:Lcom/jme3/math/Vector2f;

.field private mTouchWorld:Lcom/jme3/math/Vector2f;

.field private mWasInRotation:Z

.field private final mWorldBounds:[Lcom/jme3/math/Vector2f;

.field private final ray:Lcom/jme3/math/Ray;

.field private final screenCenter:Lcom/jme3/math/Vector2f;

.field private final screenSize:Lcom/jme3/math/Vector2f;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/exploreactivity/ExploreActivity;Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/api/model/Document;Landroid/widget/RelativeLayout;)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/jme3/app/Application;-><init>()V

    new-instance v0, Lcom/jme3/math/Vector3f;

    const/high16 v1, 0x447a0000    # 1000.0f

    invoke-direct {v0, v3, v3, v1}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mCameraLocation:Lcom/jme3/math/Vector3f;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/jme3/math/Vector2f;

    new-instance v1, Lcom/jme3/math/Vector2f;

    invoke-direct {v1}, Lcom/jme3/math/Vector2f;-><init>()V

    aput-object v1, v0, v2

    new-instance v1, Lcom/jme3/math/Vector2f;

    invoke-direct {v1}, Lcom/jme3/math/Vector2f;-><init>()V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jme3/math/Vector2f;

    invoke-direct {v1}, Lcom/jme3/math/Vector2f;-><init>()V

    aput-object v1, v0, v5

    new-instance v1, Lcom/jme3/math/Vector2f;

    invoke-direct {v1}, Lcom/jme3/math/Vector2f;-><init>()V

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenBounds:[Lcom/jme3/math/Vector2f;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/jme3/math/Vector2f;

    new-instance v1, Lcom/jme3/math/Vector2f;

    invoke-direct {v1}, Lcom/jme3/math/Vector2f;-><init>()V

    aput-object v1, v0, v2

    new-instance v1, Lcom/jme3/math/Vector2f;

    invoke-direct {v1}, Lcom/jme3/math/Vector2f;-><init>()V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jme3/math/Vector2f;

    invoke-direct {v1}, Lcom/jme3/math/Vector2f;-><init>()V

    aput-object v1, v0, v5

    new-instance v1, Lcom/jme3/math/Vector2f;

    invoke-direct {v1}, Lcom/jme3/math/Vector2f;-><init>()V

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mPreviousNodes:Ljava/util/List;

    new-instance v0, Lcom/jme3/scene/Node;

    const-string v1, "Root Node"

    invoke-direct {v0, v1}, Lcom/jme3/scene/Node;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mOriginalCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastUpdate:J

    iput-boolean v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInScroll:Z

    iput-boolean v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInRotation:Z

    iput-boolean v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWasInRotation:Z

    new-instance v0, Lcom/jme3/math/Vector2f;

    invoke-direct {v0}, Lcom/jme3/math/Vector2f;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    new-instance v0, Lcom/jme3/math/Vector2f;

    invoke-direct {v0}, Lcom/jme3/math/Vector2f;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouchWorld:Lcom/jme3/math/Vector2f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector2f;

    invoke-direct {v0}, Lcom/jme3/math/Vector2f;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenSize:Lcom/jme3/math/Vector2f;

    new-instance v0, Lcom/jme3/math/Vector2f;

    invoke-direct {v0}, Lcom/jme3/math/Vector2f;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenCenter:Lcom/jme3/math/Vector2f;

    new-instance v0, Lcom/jme3/math/Ray;

    invoke-direct {v0}, Lcom/jme3/math/Ray;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->ray:Lcom/jme3/math/Ray;

    new-instance v0, Lcom/jme3/collision/CollisionResults;

    invoke-direct {v0}, Lcom/jme3/collision/CollisionResults;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->collisionResults:Lcom/jme3/collision/CollisionResults;

    iput-object p3, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSeedDocument:Lcom/google/android/finsky/api/model/Document;

    iput-object p4, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mParentLayout:Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    iput-object p2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/exploreactivity/NodeController;->setSongListener(Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/2addr v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScrollThreshold:F

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->stateManager:Lcom/jme3/app/state/AppStateManager;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->stateManager:Lcom/jme3/app/state/AppStateManager;

    const-class v2, Lcom/jme3/app/ResetStatsState;

    invoke-virtual {v1, v2}, Lcom/jme3/app/state/AppStateManager;->getState(Ljava/lang/Class;)Lcom/jme3/app/state/AppState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jme3/app/state/AppStateManager;->detach(Lcom/jme3/app/state/AppState;)Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/exploreactivity/ExploreApplication;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/ExploreApplication;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mPlaybackControls:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/exploreactivity/ExploreApplication;)Lcom/google/android/finsky/exploreactivity/NodeController;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/ExploreApplication;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/exploreactivity/ExploreApplication;)Lcom/google/android/finsky/exploreactivity/ExploreActivity;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/ExploreApplication;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    return-object v0
.end method

.method private doNodeTap(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/jme3/collision/CollisionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .param p2    # Lcom/jme3/collision/CollisionResult;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/NodeController;->getCenterNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/NodeController;->isWishlistEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->isWishlistTap(Lcom/jme3/collision/CollisionResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->toggleWishlist(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/IntentUtils;->createViewDocumentIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->setCenterNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    goto :goto_0
.end method

.method private expandHeap(I)I
    .locals 7
    .param p1    # I

    const/4 v4, 0x1

    const/4 v6, 0x0

    new-array v1, p1, [[B

    const/4 v0, 0x0

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    const/high16 v2, 0x100000

    new-array v2, v2, [B

    aput-object v2, v1, v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "Reserved %d MB of heap"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return v0

    :catch_0
    move-exception v2

    const-string v2, "Reserved %d MB of heap"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    const-string v3, "Reserved %d MB of heap"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v2
.end method

.method private updateCamera(Lcom/jme3/math/Vector3f;)V
    .locals 12
    .param p1    # Lcom/jme3/math/Vector3f;

    const/4 v11, 0x3

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mCameraLocation:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->cam:Lcom/jme3/renderer/Camera;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mCameraLocation:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Lcom/jme3/renderer/Camera;->setLocation(Lcom/jme3/math/Vector3f;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->cam:Lcom/jme3/renderer/Camera;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jme3/renderer/Camera;->setParallelProjection(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->cam:Lcom/jme3/renderer/Camera;

    const/high16 v1, 0x44610000    # 900.0f

    const v2, 0x44898000    # 1100.0f

    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mCameraLocation:Lcom/jme3/math/Vector3f;

    iget v3, v3, Lcom/jme3/math/Vector3f;->x:F

    iget v4, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenWidth:F

    const v5, 0x3ea8f5c2    # 0.32999998f

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mCameraLocation:Lcom/jme3/math/Vector3f;

    iget v4, v4, Lcom/jme3/math/Vector3f;->x:F

    iget v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenWidth:F

    const v6, 0x3f2b851f    # 0.67f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mCameraLocation:Lcom/jme3/math/Vector3f;

    iget v5, v5, Lcom/jme3/math/Vector3f;->y:F

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenHeight:F

    const v8, 0x3f666666    # 0.9f

    mul-float/2addr v6, v8

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mCameraLocation:Lcom/jme3/math/Vector3f;

    iget v6, v6, Lcom/jme3/math/Vector3f;->y:F

    iget v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenHeight:F

    const v9, 0x3dcccccc    # 0.099999994f

    mul-float/2addr v8, v9

    sub-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Lcom/jme3/renderer/Camera;->setFrustum(FFFFFF)V

    const/4 v7, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenBounds:[Lcom/jme3/math/Vector2f;

    array-length v0, v0

    if-ge v7, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->cam:Lcom/jme3/renderer/Camera;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenBounds:[Lcom/jme3/math/Vector2f;

    aget-object v2, v2, v7

    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    aget-object v3, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getBasePlaneCoords(Lcom/jme3/renderer/Camera;Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenSize:Lcom/jme3/math/Vector2f;

    invoke-static {v0, v1}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getPolySize([Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mBackground:Lcom/jme3/scene/Geometry;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenSize:Lcom/jme3/math/Vector2f;

    iget v1, v1, Lcom/jme3/math/Vector2f;->x:F

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenSize:Lcom/jme3/math/Vector2f;

    iget v2, v2, Lcom/jme3/math/Vector2f;->y:F

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3}, Lcom/jme3/scene/Geometry;->setLocalScale(FFF)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mBackground:Lcom/jme3/scene/Geometry;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    aget-object v1, v1, v10

    iget v1, v1, Lcom/jme3/math/Vector2f;->x:F

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    aget-object v2, v2, v11

    iget v2, v2, Lcom/jme3/math/Vector2f;->y:F

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v0, v1, v2, v3}, Lcom/jme3/scene/Geometry;->setLocalTranslation(FFF)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipe:Lcom/jme3/scene/Geometry;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    aget-object v1, v1, v10

    iget v1, v1, Lcom/jme3/math/Vector2f;->x:F

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    aget-object v2, v2, v11

    iget v2, v2, Lcom/jme3/math/Vector2f;->y:F

    iget v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenHeight:F

    const v4, 0x3dcccccd    # 0.1f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1, v2, v3}, Lcom/jme3/scene/Geometry;->setLocalTranslation(FFF)V

    return-void
.end method


# virtual methods
.method public createViews()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f040070    # com.android.vending.R.layout.explorer_playback_controls

    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mParentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f08013d    # com.android.vending.R.id.explorer_song_title

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mPlaybackControls:Landroid/widget/TextView;

    return-void
.end method

.method public initialize()V
    .locals 13

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v12, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0016    # com.android.vending.R.integer.explorer_bitmap_pool_mb

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-direct {p0, v6}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->expandHeap(I)I

    move-result v6

    add-int/lit8 v3, v6, -0x2

    invoke-static {v3, v12}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/finsky/utils/BitmapLoader;

    move-result-object v2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/volley/RequestQueue;->getSequenceNumber()I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/google/android/finsky/utils/BitmapLoader;->drain(I)V

    invoke-virtual {v2}, Lcom/google/android/finsky/utils/BitmapLoader;->evictCache()V

    invoke-super {p0}, Lcom/jme3/app/Application;->initialize()V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->viewPort:Lcom/jme3/renderer/ViewPort;

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    invoke-virtual {v6, v7}, Lcom/jme3/renderer/ViewPort;->attachScene(Lcom/jme3/scene/Spatial;)V

    new-instance v6, Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->assetManager:Lcom/jme3/asset/AssetManager;

    iget-object v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-virtual {v8}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    mul-int/lit16 v9, v3, 0x400

    mul-int/lit16 v9, v9, 0x400

    invoke-direct {v6, v7, v8, v9}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;-><init>(Lcom/jme3/asset/AssetManager;Landroid/content/res/Resources;I)V

    iput-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    new-instance v7, Lcom/google/android/finsky/exploreactivity/DocWrapper;

    iget-object v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSeedDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v7, v8}, Lcom/google/android/finsky/exploreactivity/DocWrapper;-><init>(Lcom/google/android/finsky/api/model/Document;)V

    iget-object v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    invoke-virtual {v6, v7, v8}, Lcom/google/android/finsky/exploreactivity/NodeController;->createRoot(Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/DrawingUtils;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->inputManager:Lcom/jme3/input/InputManager;

    const-string v7, "Touch"

    new-array v8, v10, [Lcom/jme3/input/controls/Trigger;

    new-instance v9, Lcom/jme3/input/controls/TouchTrigger;

    invoke-direct {v9, v12}, Lcom/jme3/input/controls/TouchTrigger;-><init>(I)V

    aput-object v9, v8, v12

    invoke-virtual {v6, v7, v8}, Lcom/jme3/input/InputManager;->addMapping(Ljava/lang/String;[Lcom/jme3/input/controls/Trigger;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->inputManager:Lcom/jme3/input/InputManager;

    new-array v7, v10, [Ljava/lang/String;

    const-string v8, "Touch"

    aput-object v8, v7, v12

    invoke-virtual {v6, p0, v7}, Lcom/jme3/input/InputManager;->addListener(Lcom/jme3/input/controls/InputListener;[Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenBounds:[Lcom/jme3/math/Vector2f;

    aget-object v6, v6, v12

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v7}, Lcom/jme3/system/AppSettings;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v6, v11, v7}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenBounds:[Lcom/jme3/math/Vector2f;

    aget-object v6, v6, v10

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v7}, Lcom/jme3/system/AppSettings;->getWidth()I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v8}, Lcom/jme3/system/AppSettings;->getHeight()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v6, v7, v8}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenBounds:[Lcom/jme3/math/Vector2f;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v7}, Lcom/jme3/system/AppSettings;->getWidth()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v6, v7, v11}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenBounds:[Lcom/jme3/math/Vector2f;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    invoke-virtual {v6, v11, v11}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    const/high16 v6, 0x41200000    # 10.0f

    iput v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenWidth:F

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v6}, Lcom/jme3/system/AppSettings;->getWidth()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenWidth:F

    div-float/2addr v6, v7

    iput v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenScaleFactor:F

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v6}, Lcom/jme3/system/AppSettings;->getHeight()I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenScaleFactor:F

    div-float/2addr v6, v7

    iput v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenHeight:F

    new-instance v6, Lcom/jme3/scene/Geometry;

    const-string v7, "background"

    sget-object v8, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->UNIT_QUAD:Lcom/jme3/scene/Mesh;

    invoke-direct {v6, v7, v8}, Lcom/jme3/scene/Geometry;-><init>(Ljava/lang/String;Lcom/jme3/scene/Mesh;)V

    iput-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mBackground:Lcom/jme3/scene/Geometry;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v6, v6, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    const v7, 0x7f08013c    # com.android.vending.R.id.screen_background

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mBackground:Lcom/jme3/scene/Geometry;

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    iget-object v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    invoke-virtual {v8, v5}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->createViewBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7, v8, v10}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->convertBitmapToMaterial(Landroid/graphics/Bitmap;Z)Lcom/jme3/material/Material;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/jme3/scene/Geometry;->setMaterial(Lcom/jme3/material/Material;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mBackground:Lcom/jme3/scene/Geometry;

    invoke-virtual {v6}, Lcom/jme3/scene/Geometry;->getMaterial()Lcom/jme3/material/Material;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v6

    sget-object v7, Lcom/jme3/material/RenderState$BlendMode;->Off:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual {v6, v7}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mBackground:Lcom/jme3/scene/Geometry;

    sget-object v7, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Opaque:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    invoke-virtual {v6, v7}, Lcom/jme3/scene/Geometry;->setQueueBucket(Lcom/jme3/renderer/queue/RenderQueue$Bucket;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mBackground:Lcom/jme3/scene/Geometry;

    invoke-virtual {v6, v7}, Lcom/jme3/scene/Node;->attachChild(Lcom/jme3/scene/Spatial;)I

    new-instance v6, Lcom/jme3/scene/Geometry;

    const-string v7, "swipe"

    sget-object v8, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->UNIT_QUAD:Lcom/jme3/scene/Mesh;

    invoke-direct {v6, v7, v8}, Lcom/jme3/scene/Geometry;-><init>(Ljava/lang/String;Lcom/jme3/scene/Mesh;)V

    iput-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipe:Lcom/jme3/scene/Geometry;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v6, v6, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    const v7, 0x7f08013b    # com.android.vending.R.id.swipe

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    invoke-virtual {v6, v5}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->createViewBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipe:Lcom/jme3/scene/Geometry;

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    invoke-virtual {v7, v0, v10}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->convertBitmapToMaterial(Landroid/graphics/Bitmap;Z)Lcom/jme3/material/Material;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/jme3/scene/Geometry;->setMaterial(Lcom/jme3/material/Material;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipe:Lcom/jme3/scene/Geometry;

    invoke-virtual {v6}, Lcom/jme3/scene/Geometry;->getMaterial()Lcom/jme3/material/Material;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jme3/material/Material;->getAdditionalRenderState()Lcom/jme3/material/RenderState;

    move-result-object v6

    sget-object v7, Lcom/jme3/material/RenderState$BlendMode;->AlphaAdditive:Lcom/jme3/material/RenderState$BlendMode;

    invoke-virtual {v6, v7}, Lcom/jme3/material/RenderState;->setBlendMode(Lcom/jme3/material/RenderState$BlendMode;)V

    const/high16 v1, 0x3f900000    # 1.125f

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    mul-float v4, v6, v1

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipe:Lcom/jme3/scene/Geometry;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v4, v1, v7}, Lcom/jme3/scene/Geometry;->setLocalScale(FFF)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipe:Lcom/jme3/scene/Geometry;

    invoke-virtual {v6, v7}, Lcom/jme3/scene/Node;->attachChild(Lcom/jme3/scene/Spatial;)I

    new-instance v6, Lcom/google/android/finsky/exploreactivity/FadeAdapter;

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, 0x3e800000    # 0.25f

    const/high16 v9, 0x40a00000    # 5.0f

    new-array v10, v10, [Lcom/jme3/scene/Geometry;

    iget-object v11, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipe:Lcom/jme3/scene/Geometry;

    aput-object v11, v10, v12

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/google/android/finsky/exploreactivity/FadeAdapter;-><init>(FFF[Lcom/jme3/scene/Geometry;)V

    iput-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipeFader:Lcom/google/android/finsky/exploreactivity/FadeAdapter;

    sget-object v6, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    invoke-direct {p0, v6}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->updateCamera(Lcom/jme3/math/Vector3f;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    sget-object v7, Lcom/jme3/scene/Spatial$CullHint;->Never:Lcom/jme3/scene/Spatial$CullHint;

    invoke-virtual {v6, v7}, Lcom/jme3/scene/Node;->setCullHint(Lcom/jme3/scene/Spatial$CullHint;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    sget-object v7, Lcom/jme3/renderer/queue/RenderQueue$Bucket;->Transparent:Lcom/jme3/renderer/queue/RenderQueue$Bucket;

    invoke-virtual {v6, v7}, Lcom/jme3/scene/Node;->setQueueBucket(Lcom/jme3/renderer/queue/RenderQueue$Bucket;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->showPlaybackControls()V

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->audioRenderer:Lcom/jme3/audio/AudioRenderer;

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/exploreactivity/NodeController;->resetPlayback(I)V

    :cond_0
    return-void
.end method

.method public onPlayStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    new-instance v1, Lcom/google/android/finsky/exploreactivity/ExploreApplication$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/exploreactivity/ExploreApplication$3;-><init>(Lcom/google/android/finsky/exploreactivity/ExploreApplication;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/NodeController;->disposeObjects()V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->unloadCache()V

    :cond_0
    return-void
.end method

.method public onTouch(Ljava/lang/String;Lcom/jme3/input/event/TouchEvent;F)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jme3/input/event/TouchEvent;
    .param p3    # F

    invoke-virtual {p2}, Lcom/jme3/input/event/TouchEvent;->getPointerId()I

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    invoke-virtual {p2}, Lcom/jme3/input/event/TouchEvent;->getX()F

    move-result v6

    invoke-virtual {p2}, Lcom/jme3/input/event/TouchEvent;->getY()F

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->cam:Lcom/jme3/renderer/Camera;

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget-object v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouchWorld:Lcom/jme3/math/Vector2f;

    invoke-virtual {v5, v6, v7, v8}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getBasePlaneCoords(Lcom/jme3/renderer/Camera;Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V

    sget-object v5, Lcom/google/android/finsky/exploreactivity/ExploreApplication$4;->$SwitchMap$com$jme3$input$event$TouchEvent$Type:[I

    invoke-virtual {p2}, Lcom/jme3/input/event/TouchEvent;->getType()Lcom/jme3/input/event/TouchEvent$Type;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jme3/input/event/TouchEvent$Type;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouchWorld:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    iget v6, v6, Lcom/jme3/math/Vector2f;->x:F

    iget v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenWidth:F

    const v8, 0x3e4ccccd    # 0.2f

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouchWorld:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->y:F

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    iget v6, v6, Lcom/jme3/math/Vector2f;->y:F

    iget v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenHeight:F

    const v8, 0x3ecccccd    # 0.4f

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_2

    const/4 v5, 0x1

    :goto_1
    iput-boolean v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInRotation:Z

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    iput v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveX:F

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->y:F

    iput v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveY:F

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v5}, Lcom/google/android/finsky/exploreactivity/NodeController;->getCenterNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mOriginalCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iget-boolean v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInRotation:Z

    iput-boolean v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWasInRotation:Z

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    :pswitch_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInScroll:Z

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInRotation:Z

    goto :goto_0

    :pswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->ray:Lcom/jme3/math/Ray;

    invoke-virtual {v5}, Lcom/jme3/math/Ray;->getOrigin()Lcom/jme3/math/Vector3f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouchWorld:Lcom/jme3/math/Vector2f;

    iget v6, v6, Lcom/jme3/math/Vector2f;->x:F

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouchWorld:Lcom/jme3/math/Vector2f;

    iget v7, v7, Lcom/jme3/math/Vector2f;->y:F

    const/high16 v8, 0x41200000    # 10.0f

    invoke-virtual {v5, v6, v7, v8}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->ray:Lcom/jme3/math/Ray;

    invoke-virtual {v5}, Lcom/jme3/math/Ray;->getDirection()Lcom/jme3/math/Vector3f;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    invoke-virtual {v5, v6, v7, v8}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->collisionResults:Lcom/jme3/collision/CollisionResults;

    invoke-virtual {v5}, Lcom/jme3/collision/CollisionResults;->clear()V

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->ray:Lcom/jme3/math/Ray;

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->collisionResults:Lcom/jme3/collision/CollisionResults;

    invoke-virtual {v5, v6, v7}, Lcom/jme3/scene/Node;->collideWith(Lcom/jme3/collision/Collidable;Lcom/jme3/collision/CollisionResults;)I

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->collisionResults:Lcom/jme3/collision/CollisionResults;

    invoke-virtual {v5}, Lcom/jme3/collision/CollisionResults;->getClosestCollision()Lcom/jme3/collision/CollisionResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jme3/collision/CollisionResult;->getGeometry()Lcom/jme3/scene/Geometry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jme3/scene/Geometry;->getParent()Lcom/jme3/scene/Node;

    move-result-object v3

    :cond_3
    instance-of v5, v3, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    if-eqz v5, :cond_4

    check-cast v3, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-direct {p0, v3, v0}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->doNodeTap(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/jme3/collision/CollisionResult;)V

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipeFader:Lcom/google/android/finsky/exploreactivity/FadeAdapter;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/exploreactivity/FadeAdapter;->fade(Z)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v3}, Lcom/jme3/scene/Node;->getParent()Lcom/jme3/scene/Node;

    move-result-object v3

    if-nez v3, :cond_3

    goto/16 :goto_0

    :pswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveX:F

    sub-float/2addr v5, v6

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenScaleFactor:F

    div-float v1, v5, v6

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->y:F

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveY:F

    sub-float/2addr v5, v6

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenScaleFactor:F

    div-float v2, v5, v6

    iget-boolean v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInRotation:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    const v6, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v6, v2

    const/high16 v7, 0x40800000    # 4.0f

    div-float/2addr v6, v7

    const/high16 v7, 0x3f800000    # 1.0f

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/exploreactivity/NodeController;->rotate(F)V

    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    iput v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveX:F

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->y:F

    iput v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveY:F

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipeFader:Lcom/google/android/finsky/exploreactivity/FadeAdapter;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/exploreactivity/FadeAdapter;->fade(Z)V

    iget-boolean v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInScroll:Z

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    iget v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveX:F

    sub-float/2addr v5, v7

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v7, v7, Lcom/jme3/math/Vector2f;->x:F

    iget v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveX:F

    sub-float/2addr v7, v8

    mul-float/2addr v5, v7

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v7, v7, Lcom/jme3/math/Vector2f;->y:F

    iget v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveY:F

    sub-float/2addr v7, v8

    iget-object v8, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mTouch:Lcom/jme3/math/Vector2f;

    iget v8, v8, Lcom/jme3/math/Vector2f;->y:F

    iget v9, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastMoveY:F

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v5, v7

    iget v7, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScrollThreshold:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_6

    const/4 v5, 0x1

    :goto_3
    or-int/2addr v5, v6

    iput-boolean v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInScroll:Z

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mOriginalCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    move-result-object v5

    iget v4, v5, Lcom/jme3/math/Vector2f;->x:F

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    sub-float/2addr v5, v4

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    sub-float/2addr v5, v4

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    neg-float v6, v1

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    neg-float v7, v2

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    invoke-direct {p0, v5}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->updateCamera(Lcom/jme3/math/Vector3f;)V

    goto :goto_2

    :cond_6
    const/4 v5, 0x0

    goto :goto_3

    :pswitch_4
    invoke-virtual {p2}, Lcom/jme3/input/event/TouchEvent;->getDeltaY()F

    move-result v5

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWasInRotation:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mOriginalCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v5}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasParentNode()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mOriginalCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/exploreactivity/NodeController;->setCenterNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setSeedDocument(Lcom/google/android/finsky/api/model/Document;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/api/model/Document;

    iput-object p1, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSeedDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    new-instance v1, Lcom/google/android/finsky/exploreactivity/DocWrapper;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSeedDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v1, v2}, Lcom/google/android/finsky/exploreactivity/DocWrapper;-><init>(Lcom/google/android/finsky/api/model/Document;)V

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/exploreactivity/NodeController;->createRoot(Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/DrawingUtils;)V

    :cond_0
    return-void
.end method

.method public setSettings(Lcom/jme3/system/AppSettings;)V
    .locals 1
    .param p1    # Lcom/jme3/system/AppSettings;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/jme3/system/AppSettings;->setSamples(I)V

    const/16 v0, 0x1e

    invoke-virtual {p1, v0}, Lcom/jme3/system/AppSettings;->setFrameRate(I)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/jme3/system/AppSettings;->setAudioRenderer(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/jme3/app/Application;->setSettings(Lcom/jme3/system/AppSettings;)V

    return-void
.end method

.method public showPlaybackControls()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    new-instance v1, Lcom/google/android/finsky/exploreactivity/ExploreApplication$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/exploreactivity/ExploreApplication$1;-><init>(Lcom/google/android/finsky/exploreactivity/ExploreApplication;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public update()V
    .locals 19

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v15, 0x21

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastUpdate:J

    move-wide/from16 v17, v0

    sub-long v17, v8, v17

    sub-long v12, v15, v17

    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mLastUpdate:J

    const-wide/16 v15, 0x0

    cmp-long v15, v12, v15

    if-lez v15, :cond_0

    invoke-static {v12, v13}, Landroid/os/SystemClock;->sleep(J)V

    :cond_0
    invoke-super/range {p0 .. p0}, Lcom/jme3/app/Application;->update()V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->speed:F

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->paused:Z

    if-eqz v15, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->timer:Lcom/jme3/system/Timer;

    invoke-virtual {v15}, Lcom/jme3/system/Timer;->getTimePerFrame()F

    move-result v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->speed:F

    move/from16 v16, v0

    mul-float v15, v15, v16

    const v16, 0x3dcccccd    # 0.1f

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(FF)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->stateManager:Lcom/jme3/app/state/AppStateManager;

    invoke-virtual {v15, v14}, Lcom/jme3/app/state/AppStateManager;->update(F)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v15}, Lcom/google/android/finsky/exploreactivity/NodeController;->getCenterNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v14}, Lcom/google/android/finsky/exploreactivity/NodeController;->processOnscreenNodes([Lcom/jme3/math/Vector2f;F)Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInScroll:Z

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mOriginalCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/finsky/exploreactivity/NodeController;->chooseCenterNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    :cond_3
    const/4 v4, 0x0

    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v15

    if-ge v4, v15, :cond_8

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v7}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->isOnscreen()Z

    move-result v15

    if-eqz v15, :cond_6

    if-ne v7, v3, :cond_5

    const/4 v15, 0x1

    :goto_2
    invoke-virtual {v7, v14, v15}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->update(FZ)V

    :cond_4
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_5
    const/4 v15, 0x0

    goto :goto_2

    :cond_6
    invoke-virtual {v7}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasThumbnail()Z

    move-result v15

    if-eqz v15, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    invoke-virtual {v15, v7}, Lcom/jme3/scene/Node;->attachChild(Lcom/jme3/scene/Spatial;)I

    if-ne v7, v3, :cond_7

    const/4 v15, 0x1

    :goto_4
    invoke-virtual {v7, v15}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->attach(Z)V

    goto :goto_3

    :cond_7
    const/4 v15, 0x0

    goto :goto_4

    :cond_8
    const/4 v4, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-ge v4, v15, :cond_a

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v15, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_9

    invoke-virtual {v7}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->isOnscreen()Z

    move-result v15

    if-eqz v15, :cond_9

    invoke-virtual {v7}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->removeFromParent()Z

    invoke-virtual {v7}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->detach()V

    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->clear()V

    const/4 v4, 0x0

    :goto_6
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v15

    if-ge v4, v15, :cond_b

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_b
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInScroll:Z

    if-nez v15, :cond_10

    const/4 v5, 0x1

    :goto_7
    invoke-virtual {v3}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->isDescriptionOn()Z

    move-result v15

    if-eq v5, v15, :cond_c

    invoke-virtual {v3, v5}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->setDescriptionState(Z)V

    :cond_c
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInScroll:Z

    if-nez v15, :cond_d

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v15, v3}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mWorldBounds:[Lcom/jme3/math/Vector2f;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenCenter:Lcom/jme3/math/Vector2f;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getPolyCenter([Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenCenter:Lcom/jme3/math/Vector2f;

    iget v0, v15, Lcom/jme3/math/Vector2f;->x:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenWidth:F

    move/from16 v17, v0

    const v18, -0x41d1eb85    # -0.17f

    mul-float v17, v17, v18

    add-float v16, v16, v17

    move/from16 v0, v16

    iput v0, v15, Lcom/jme3/math/Vector2f;->x:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenCenter:Lcom/jme3/math/Vector2f;

    iget v0, v15, Lcom/jme3/math/Vector2f;->y:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mScreenHeight:F

    move/from16 v17, v0

    const v18, -0x41333333    # -0.4f

    mul-float v17, v17, v18

    add-float v16, v16, v17

    move/from16 v0, v16

    iput v0, v15, Lcom/jme3/math/Vector2f;->y:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    iget v0, v2, Lcom/jme3/math/Vector2f;->x:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenCenter:Lcom/jme3/math/Vector2f;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/jme3/math/Vector2f;->x:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    move/from16 v0, v16

    iput v0, v15, Lcom/jme3/math/Vector3f;->x:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    iget v0, v2, Lcom/jme3/math/Vector2f;->y:F

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->screenCenter:Lcom/jme3/math/Vector2f;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/jme3/math/Vector2f;->y:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    move/from16 v0, v16

    iput v0, v15, Lcom/jme3/math/Vector3f;->y:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    const/16 v16, 0x0

    move/from16 v0, v16

    iput v0, v15, Lcom/jme3/math/Vector3f;->z:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    invoke-virtual {v15}, Lcom/jme3/math/Vector3f;->length()F

    move-result v15

    const/high16 v16, 0x3f800000    # 1.0f

    cmpl-float v15, v15, v16

    if-lez v15, :cond_11

    const/4 v10, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    invoke-virtual {v15, v14}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->delta:Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->updateCamera(Lcom/jme3/math/Vector3f;)V

    :cond_d
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInRotation:Z

    if-nez v15, :cond_f

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v15}, Lcom/google/android/finsky/exploreactivity/NodeController;->getRotation()F

    move-result v11

    const v15, 0x40490fdb    # (float)Math.PI

    cmpl-float v15, v11, v15

    if-lez v15, :cond_e

    const v15, 0x40c90fdb

    sub-float/2addr v11, v15

    :cond_e
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    neg-float v0, v11

    move/from16 v16, v0

    mul-float v16, v16, v14

    const/high16 v17, 0x3f800000    # 1.0f

    div-float v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/finsky/exploreactivity/NodeController;->rotate(F)V

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipeFader:Lcom/google/android/finsky/exploreactivity/FadeAdapter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mInScroll:Z

    if-nez v15, :cond_12

    if-nez v10, :cond_12

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mNodeController:Lcom/google/android/finsky/exploreactivity/NodeController;

    invoke-virtual {v15}, Lcom/google/android/finsky/exploreactivity/NodeController;->isSwipable()Z

    move-result v15

    if-eqz v15, :cond_12

    const/4 v15, 0x1

    :goto_9
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/google/android/finsky/exploreactivity/FadeAdapter;->fade(Z)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mSwipeFader:Lcom/google/android/finsky/exploreactivity/FadeAdapter;

    invoke-virtual {v15, v14}, Lcom/google/android/finsky/exploreactivity/FadeAdapter;->update(F)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    invoke-virtual {v15, v14}, Lcom/jme3/scene/Node;->updateLogicalState(F)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->mRootNode:Lcom/jme3/scene/Node;

    invoke-virtual {v15}, Lcom/jme3/scene/Node;->updateGeometricState()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->stateManager:Lcom/jme3/app/state/AppStateManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->renderManager:Lcom/jme3/renderer/RenderManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/jme3/app/state/AppStateManager;->render(Lcom/jme3/renderer/RenderManager;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->renderManager:Lcom/jme3/renderer/RenderManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->context:Lcom/jme3/system/JmeContext;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Lcom/jme3/system/JmeContext;->isRenderable()Z

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v15, v14, v0}, Lcom/jme3/renderer/RenderManager;->render(FZ)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/exploreactivity/ExploreApplication;->stateManager:Lcom/jme3/app/state/AppStateManager;

    invoke-virtual {v15}, Lcom/jme3/app/state/AppStateManager;->postRender()V

    goto/16 :goto_0

    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_11
    const/4 v10, 0x0

    goto/16 :goto_8

    :cond_12
    const/4 v15, 0x0

    goto :goto_9
.end method
