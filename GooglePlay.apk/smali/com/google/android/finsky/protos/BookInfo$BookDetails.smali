.class public final Lcom/google/android/finsky/protos/BookInfo$BookDetails;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "BookInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BookInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BookDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;
    }
.end annotation


# instance fields
.field private aboutTheAuthor_:Ljava/lang/String;

.field private acsEpubTokenUrl_:Ljava/lang/String;

.field private acsPdfTokenUrl_:Ljava/lang/String;

.field private audioVideoContent_:Z

.field private author_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/BookInfo$BookAuthor;",
            ">;"
        }
    .end annotation
.end field

.field private cachedSize:I

.field private downloadEpubUrl_:Ljava/lang/String;

.field private downloadPdfUrl_:Ljava/lang/String;

.field private epubAvailable_:Z

.field private fixedLayoutContent_:Z

.field private hasAboutTheAuthor:Z

.field private hasAcsEpubTokenUrl:Z

.field private hasAcsPdfTokenUrl:Z

.field private hasAudioVideoContent:Z

.field private hasDownloadEpubUrl:Z

.field private hasDownloadPdfUrl:Z

.field private hasEpubAvailable:Z

.field private hasFixedLayoutContent:Z

.field private hasIsAgencyBook:Z

.field private hasIsbn:Z

.field private hasNumberOfPages:Z

.field private hasPdfAvailable:Z

.field private hasPublicationDate:Z

.field private hasPublisher:Z

.field private hasReaderUrl:Z

.field private hasSubtitle:Z

.field private identifier_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;",
            ">;"
        }
    .end annotation
.end field

.field private isAgencyBook_:Z

.field private isbn_:Ljava/lang/String;

.field private numberOfPages_:I

.field private pdfAvailable_:Z

.field private publicationDate_:Ljava/lang/String;

.field private publisher_:Ljava/lang/String;

.field private readerUrl_:Ljava/lang/String;

.field private subject_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/BookInfo$BookSubject;",
            ">;"
        }
    .end annotation
.end field

.field private subtitle_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->identifier_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->author_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->subject_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->publisher_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->publicationDate_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->isbn_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->numberOfPages_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->subtitle_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->readerUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->downloadEpubUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->downloadPdfUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->acsEpubTokenUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->acsPdfTokenUrl_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->epubAvailable_:Z

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->pdfAvailable_:Z

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->fixedLayoutContent_:Z

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->audioVideoContent_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->aboutTheAuthor_:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->isAgencyBook_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addAuthor(Lcom/google/android/finsky/protos/BookInfo$BookAuthor;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/BookInfo$BookAuthor;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->author_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->author_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->author_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addIdentifier(Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->identifier_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->identifier_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->identifier_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSubject(Lcom/google/android/finsky/protos/BookInfo$BookSubject;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/BookInfo$BookSubject;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->subject_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->subject_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->subject_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAboutTheAuthor()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->aboutTheAuthor_:Ljava/lang/String;

    return-object v0
.end method

.method public getAcsEpubTokenUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->acsEpubTokenUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getAcsPdfTokenUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->acsPdfTokenUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioVideoContent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->audioVideoContent_:Z

    return v0
.end method

.method public getAuthorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/BookInfo$BookAuthor;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->author_:Ljava/util/List;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->cachedSize:I

    return v0
.end method

.method public getDownloadEpubUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->downloadEpubUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadPdfUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->downloadPdfUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getEpubAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->epubAvailable_:Z

    return v0
.end method

.method public getFixedLayoutContent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->fixedLayoutContent_:Z

    return v0
.end method

.method public getIdentifierList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->identifier_:Ljava/util/List;

    return-object v0
.end method

.method public getIsAgencyBook()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->isAgencyBook_:Z

    return v0
.end method

.method public getIsbn()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->isbn_:Ljava/lang/String;

    return-object v0
.end method

.method public getNumberOfPages()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->numberOfPages_:I

    return v0
.end method

.method public getPdfAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->pdfAvailable_:Z

    return v0
.end method

.method public getPublicationDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->publicationDate_:Ljava/lang/String;

    return-object v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->publisher_:Ljava/lang/String;

    return-object v0
.end method

.method public getReaderUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->readerUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getSubjectList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BookInfo$BookSubject;

    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPublisher()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getPublisher()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPublicationDate()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getPublicationDate()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasIsbn()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getIsbn()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasNumberOfPages()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getNumberOfPages()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasSubtitle()Z

    move-result v3

    if-eqz v3, :cond_5

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getSubtitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAuthorList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BookInfo$BookAuthor;

    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasReaderUrl()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getReaderUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasDownloadEpubUrl()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getDownloadEpubUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasDownloadPdfUrl()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getDownloadPdfUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAcsEpubTokenUrl()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAcsEpubTokenUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAcsPdfTokenUrl()Z

    move-result v3

    if-eqz v3, :cond_b

    const/16 v3, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAcsPdfTokenUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasEpubAvailable()Z

    move-result v3

    if-eqz v3, :cond_c

    const/16 v3, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getEpubAvailable()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPdfAvailable()Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0x10

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getPdfAvailable()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAboutTheAuthor()Z

    move-result v3

    if-eqz v3, :cond_e

    const/16 v3, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAboutTheAuthor()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getIdentifierList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;

    const/16 v3, 0x12

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeGroupSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_2

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasFixedLayoutContent()Z

    move-result v3

    if-eqz v3, :cond_10

    const/16 v3, 0x15

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getFixedLayoutContent()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAudioVideoContent()Z

    move-result v3

    if-eqz v3, :cond_11

    const/16 v3, 0x16

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAudioVideoContent()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasIsAgencyBook()Z

    move-result v3

    if-eqz v3, :cond_12

    const/16 v3, 0x17

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getIsAgencyBook()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_12
    iput v2, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->cachedSize:I

    return v2
.end method

.method public getSubjectList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/BookInfo$BookSubject;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->subject_:Ljava/util/List;

    return-object v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->subtitle_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAboutTheAuthor()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAboutTheAuthor:Z

    return v0
.end method

.method public hasAcsEpubTokenUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAcsEpubTokenUrl:Z

    return v0
.end method

.method public hasAcsPdfTokenUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAcsPdfTokenUrl:Z

    return v0
.end method

.method public hasAudioVideoContent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAudioVideoContent:Z

    return v0
.end method

.method public hasDownloadEpubUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasDownloadEpubUrl:Z

    return v0
.end method

.method public hasDownloadPdfUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasDownloadPdfUrl:Z

    return v0
.end method

.method public hasEpubAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasEpubAvailable:Z

    return v0
.end method

.method public hasFixedLayoutContent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasFixedLayoutContent:Z

    return v0
.end method

.method public hasIsAgencyBook()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasIsAgencyBook:Z

    return v0
.end method

.method public hasIsbn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasIsbn:Z

    return v0
.end method

.method public hasNumberOfPages()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasNumberOfPages:Z

    return v0
.end method

.method public hasPdfAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPdfAvailable:Z

    return v0
.end method

.method public hasPublicationDate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPublicationDate:Z

    return v0
.end method

.method public hasPublisher()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPublisher:Z

    return v0
.end method

.method public hasReaderUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasReaderUrl:Z

    return v0
.end method

.method public hasSubtitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasSubtitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/protos/BookInfo$BookSubject;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BookInfo$BookSubject;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->addSubject(Lcom/google/android/finsky/protos/BookInfo$BookSubject;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setPublisher(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setPublicationDate(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setIsbn(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setNumberOfPages(I)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setSubtitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/protos/BookInfo$BookAuthor;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BookInfo$BookAuthor;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->addAuthor(Lcom/google/android/finsky/protos/BookInfo$BookAuthor;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setReaderUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setDownloadEpubUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setDownloadPdfUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setAcsEpubTokenUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setAcsPdfTokenUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setEpubAvailable(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setPdfAvailable(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setAboutTheAuthor(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;-><init>()V

    const/16 v2, 0x12

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readGroup(Lcom/google/protobuf/micro/MessageMicro;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->addIdentifier(Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setFixedLayoutContent(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setAudioVideoContent(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->setIsAgencyBook(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x38 -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
        0x80 -> :sswitch_e
        0x8a -> :sswitch_f
        0x93 -> :sswitch_10
        0xa8 -> :sswitch_11
        0xb0 -> :sswitch_12
        0xb8 -> :sswitch_13
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;

    move-result-object v0

    return-object v0
.end method

.method public setAboutTheAuthor(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAboutTheAuthor:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->aboutTheAuthor_:Ljava/lang/String;

    return-object p0
.end method

.method public setAcsEpubTokenUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAcsEpubTokenUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->acsEpubTokenUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setAcsPdfTokenUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAcsPdfTokenUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->acsPdfTokenUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setAudioVideoContent(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAudioVideoContent:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->audioVideoContent_:Z

    return-object p0
.end method

.method public setDownloadEpubUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasDownloadEpubUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->downloadEpubUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setDownloadPdfUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasDownloadPdfUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->downloadPdfUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setEpubAvailable(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasEpubAvailable:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->epubAvailable_:Z

    return-object p0
.end method

.method public setFixedLayoutContent(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasFixedLayoutContent:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->fixedLayoutContent_:Z

    return-object p0
.end method

.method public setIsAgencyBook(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasIsAgencyBook:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->isAgencyBook_:Z

    return-object p0
.end method

.method public setIsbn(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasIsbn:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->isbn_:Ljava/lang/String;

    return-object p0
.end method

.method public setNumberOfPages(I)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasNumberOfPages:Z

    iput p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->numberOfPages_:I

    return-object p0
.end method

.method public setPdfAvailable(Z)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPdfAvailable:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->pdfAvailable_:Z

    return-object p0
.end method

.method public setPublicationDate(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPublicationDate:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->publicationDate_:Ljava/lang/String;

    return-object p0
.end method

.method public setPublisher(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPublisher:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->publisher_:Ljava/lang/String;

    return-object p0
.end method

.method public setReaderUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasReaderUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->readerUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setSubtitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/BookInfo$BookDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasSubtitle:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->subtitle_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getSubjectList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BookInfo$BookSubject;

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPublisher()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getPublisher()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPublicationDate()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getPublicationDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasIsbn()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getIsbn()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasNumberOfPages()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getNumberOfPages()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasSubtitle()Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getSubtitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAuthorList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BookInfo$BookAuthor;

    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasReaderUrl()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getReaderUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasDownloadEpubUrl()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getDownloadEpubUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasDownloadPdfUrl()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getDownloadPdfUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAcsEpubTokenUrl()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAcsEpubTokenUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAcsPdfTokenUrl()Z

    move-result v2

    if-eqz v2, :cond_b

    const/16 v2, 0xe

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAcsPdfTokenUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasEpubAvailable()Z

    move-result v2

    if-eqz v2, :cond_c

    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getEpubAvailable()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasPdfAvailable()Z

    move-result v2

    if-eqz v2, :cond_d

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getPdfAvailable()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAboutTheAuthor()Z

    move-result v2

    if-eqz v2, :cond_e

    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAboutTheAuthor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getIdentifierList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BookInfo$BookDetails$Identifier;

    const/16 v2, 0x12

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeGroup(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_2

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasFixedLayoutContent()Z

    move-result v2

    if-eqz v2, :cond_10

    const/16 v2, 0x15

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getFixedLayoutContent()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasAudioVideoContent()Z

    move-result v2

    if-eqz v2, :cond_11

    const/16 v2, 0x16

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getAudioVideoContent()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->hasIsAgencyBook()Z

    move-result v2

    if-eqz v2, :cond_12

    const/16 v2, 0x17

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BookInfo$BookDetails;->getIsAgencyBook()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_12
    return-void
.end method
