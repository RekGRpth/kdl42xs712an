.class public final Lcom/google/android/finsky/protos/DocAnnotations;
.super Ljava/lang/Object;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/DocAnnotations$VideoSnippet;,
        Lcom/google/android/finsky/protos/DocAnnotations$OBSOLETE_Reason;,
        Lcom/google/android/finsky/protos/DocAnnotations$ReasonReview;,
        Lcom/google/android/finsky/protos/DocAnnotations$ReasonPlusProfiles;,
        Lcom/google/android/finsky/protos/DocAnnotations$Reason;,
        Lcom/google/android/finsky/protos/DocAnnotations$Dismissal;,
        Lcom/google/android/finsky/protos/DocAnnotations$SuggestionReasons;,
        Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;,
        Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;,
        Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;,
        Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;,
        Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;,
        Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;,
        Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;,
        Lcom/google/android/finsky/protos/DocAnnotations$Template;,
        Lcom/google/android/finsky/protos/DocAnnotations$Warning;,
        Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;,
        Lcom/google/android/finsky/protos/DocAnnotations$PromotedDoc;,
        Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;,
        Lcom/google/android/finsky/protos/DocAnnotations$Badge;,
        Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;,
        Lcom/google/android/finsky/protos/DocAnnotations$Link;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
