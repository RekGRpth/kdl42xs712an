.class public final Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ChallengeProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AuthenticationChallenge"
.end annotation


# instance fields
.field private authenticationType_:I

.field private cachedSize:I

.field private gaiaDescriptionTextHtml_:Ljava/lang/String;

.field private gaiaFooterTextHtml_:Ljava/lang/String;

.field private gaiaHeaderText_:Ljava/lang/String;

.field private gaiaOptOutCheckbox_:Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

.field private gaiaOptOutDescriptionTextHtml_:Ljava/lang/String;

.field private hasAuthenticationType:Z

.field private hasGaiaDescriptionTextHtml:Z

.field private hasGaiaFooterTextHtml:Z

.field private hasGaiaHeaderText:Z

.field private hasGaiaOptOutCheckbox:Z

.field private hasGaiaOptOutDescriptionTextHtml:Z

.field private hasResponseAuthenticationTypeParam:Z

.field private hasResponseRetryCountParam:Z

.field private responseAuthenticationTypeParam_:Ljava/lang/String;

.field private responseRetryCountParam_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->authenticationType_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->responseAuthenticationTypeParam_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->responseRetryCountParam_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaHeaderText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaDescriptionTextHtml_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaFooterTextHtml_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaOptOutCheckbox_:Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAuthenticationType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->authenticationType_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->cachedSize:I

    return v0
.end method

.method public getGaiaDescriptionTextHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaDescriptionTextHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getGaiaFooterTextHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaFooterTextHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getGaiaHeaderText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaHeaderText_:Ljava/lang/String;

    return-object v0
.end method

.method public getGaiaOptOutCheckbox()Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaOptOutCheckbox_:Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    return-object v0
.end method

.method public getGaiaOptOutDescriptionTextHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseAuthenticationTypeParam()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->responseAuthenticationTypeParam_:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseRetryCountParam()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->responseRetryCountParam_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasAuthenticationType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getAuthenticationType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasResponseAuthenticationTypeParam()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getResponseAuthenticationTypeParam()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasResponseRetryCountParam()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getResponseRetryCountParam()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaHeaderText()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaHeaderText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaDescriptionTextHtml()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaDescriptionTextHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaFooterTextHtml()Z

    move-result v1

    if-eqz v1, :cond_5

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaFooterTextHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaOptOutCheckbox()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaOptOutCheckbox()Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaOptOutDescriptionTextHtml()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaOptOutDescriptionTextHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->cachedSize:I

    return v0
.end method

.method public hasAuthenticationType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasAuthenticationType:Z

    return v0
.end method

.method public hasGaiaDescriptionTextHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaDescriptionTextHtml:Z

    return v0
.end method

.method public hasGaiaFooterTextHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaFooterTextHtml:Z

    return v0
.end method

.method public hasGaiaHeaderText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaHeaderText:Z

    return v0
.end method

.method public hasGaiaOptOutCheckbox()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaOptOutCheckbox:Z

    return v0
.end method

.method public hasGaiaOptOutDescriptionTextHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaOptOutDescriptionTextHtml:Z

    return v0
.end method

.method public hasResponseAuthenticationTypeParam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasResponseAuthenticationTypeParam:Z

    return v0
.end method

.method public hasResponseRetryCountParam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasResponseRetryCountParam:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->setAuthenticationType(I)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->setResponseAuthenticationTypeParam(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->setResponseRetryCountParam(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->setGaiaHeaderText(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->setGaiaDescriptionTextHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->setGaiaFooterTextHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->setGaiaOptOutCheckbox(Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->setGaiaOptOutDescriptionTextHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;

    move-result-object v0

    return-object v0
.end method

.method public setAuthenticationType(I)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasAuthenticationType:Z

    iput p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->authenticationType_:I

    return-object p0
.end method

.method public setGaiaDescriptionTextHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaDescriptionTextHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaDescriptionTextHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setGaiaFooterTextHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaFooterTextHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaFooterTextHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setGaiaHeaderText(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaHeaderText:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaHeaderText_:Ljava/lang/String;

    return-object p0
.end method

.method public setGaiaOptOutCheckbox(Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaOptOutCheckbox:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaOptOutCheckbox_:Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    return-object p0
.end method

.method public setGaiaOptOutDescriptionTextHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaOptOutDescriptionTextHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setResponseAuthenticationTypeParam(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasResponseAuthenticationTypeParam:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->responseAuthenticationTypeParam_:Ljava/lang/String;

    return-object p0
.end method

.method public setResponseRetryCountParam(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasResponseRetryCountParam:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->responseRetryCountParam_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasAuthenticationType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getAuthenticationType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasResponseAuthenticationTypeParam()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getResponseAuthenticationTypeParam()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasResponseRetryCountParam()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getResponseRetryCountParam()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaHeaderText()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaHeaderText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaDescriptionTextHtml()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaDescriptionTextHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaFooterTextHtml()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaFooterTextHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaOptOutCheckbox()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaOptOutCheckbox()Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->hasGaiaOptOutDescriptionTextHtml()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AuthenticationChallenge;->getGaiaOptOutDescriptionTextHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    return-void
.end method
