.class public final Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "EncryptedSubscriberInfo.java"


# instance fields
.field private cachedSize:I

.field private carrierKeyVersion_:I

.field private data_:Ljava/lang/String;

.field private encryptedKey_:Ljava/lang/String;

.field private googleKeyVersion_:I

.field private hasCarrierKeyVersion:Z

.field private hasData:Z

.field private hasEncryptedKey:Z

.field private hasGoogleKeyVersion:Z

.field private hasInitVector:Z

.field private hasSignature:Z

.field private initVector_:Ljava/lang/String;

.field private signature_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion_:I

    iput v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->cachedSize:I

    return v0
.end method

.method public getCarrierKeyVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion_:I

    return v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data_:Ljava/lang/String;

    return-object v0
.end method

.method public getEncryptedKey()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey_:Ljava/lang/String;

    return-object v0
.end method

.method public getGoogleKeyVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion_:I

    return v0
.end method

.method public getInitVector()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasData()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasEncryptedKey()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getEncryptedKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasSignature()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getSignature()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasInitVector()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getInitVector()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasGoogleKeyVersion()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getGoogleKeyVersion()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasCarrierKeyVersion()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getCarrierKeyVersion()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->cachedSize:I

    return v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature_:Ljava/lang/String;

    return-object v0
.end method

.method public hasCarrierKeyVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasCarrierKeyVersion:Z

    return v0
.end method

.method public hasData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasData:Z

    return v0
.end method

.method public hasEncryptedKey()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasEncryptedKey:Z

    return v0
.end method

.method public hasGoogleKeyVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasGoogleKeyVersion:Z

    return v0
.end method

.method public hasInitVector()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasInitVector:Z

    return v0
.end method

.method public hasSignature()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasSignature:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->setData(Ljava/lang/String;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->setEncryptedKey(Ljava/lang/String;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->setSignature(Ljava/lang/String;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->setInitVector(Ljava/lang/String;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->setGoogleKeyVersion(I)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->setCarrierKeyVersion(I)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    move-result-object v0

    return-object v0
.end method

.method public setCarrierKeyVersion(I)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasCarrierKeyVersion:Z

    iput p1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion_:I

    return-object p0
.end method

.method public setData(Ljava/lang/String;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasData:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data_:Ljava/lang/String;

    return-object p0
.end method

.method public setEncryptedKey(Ljava/lang/String;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasEncryptedKey:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey_:Ljava/lang/String;

    return-object p0
.end method

.method public setGoogleKeyVersion(I)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasGoogleKeyVersion:Z

    iput p1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion_:I

    return-object p0
.end method

.method public setInitVector(Ljava/lang/String;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasInitVector:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector_:Ljava/lang/String;

    return-object p0
.end method

.method public setSignature(Ljava/lang/String;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasSignature:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasData()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasEncryptedKey()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getEncryptedKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasSignature()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getSignature()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasInitVector()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getInitVector()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasGoogleKeyVersion()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getGoogleKeyVersion()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasCarrierKeyVersion()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->getCarrierKeyVersion()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    return-void
.end method
