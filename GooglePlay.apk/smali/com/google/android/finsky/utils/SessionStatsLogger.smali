.class public Lcom/google/android/finsky/utils/SessionStatsLogger;
.super Ljava/lang/Object;
.source "SessionStatsLogger.java"


# static fields
.field private static sHaveLoggedSessionStats:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static logSessionStatsIfNecessary(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-boolean v1, Lcom/google/android/finsky/utils/SessionStatsLogger;->sHaveLoggedSessionStats:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/finsky/config/G;->enableSessionStatsLog:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/finsky/utils/SessionStatsLogger;->sHaveLoggedSessionStats:Z

    :try_start_0
    invoke-static {p0}, Lcom/google/android/finsky/utils/SessionStatsLogger;->logSessionStatsImpl(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Fatal exception while logging session stats"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static logSessionStatsImpl(Landroid/content/Context;)V
    .locals 11
    .param p0    # Landroid/content/Context;

    const/4 v8, 0x0

    const/4 v10, -0x1

    new-instance v5, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    invoke-direct {v5}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;-><init>()V

    sget-object v7, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v5, v4}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGlobalAutoUpdateEnabled(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    sget-object v7, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGlobalAutoUpdateOverWifiOnly(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    sget-object v7, Lcom/google/android/finsky/utils/FinskyPreferences;->autoUpdateMigrationDialogShownCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setAutoUpdateCleanupDialogNumTimesShown(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    invoke-static {p0}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v7, v0

    invoke-virtual {v5, v7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNumAccountsOnDevice(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    :cond_0
    const-string v7, "connectivity"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNetworkType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setNetworkSubType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    sget-object v7, Lcom/google/android/finsky/utils/FinskyPreferences;->isGaiaAuthOptedOut:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v7, v2}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setGaiaPasswordAuthOptedOut(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    :cond_2
    sget-object v7, Lcom/google/android/finsky/utils/FinskyPreferences;->contentFilterLevel:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setContentFilterLevel(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x11

    if-lt v7, v9, :cond_3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v9, "install_non_market_apps"

    invoke-static {v7, v9, v10}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    :goto_0
    if-ne v6, v10, :cond_4

    const-string v7, "Couldn\'t obtain INSTALL_NON_MARKET_APPS value"

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSessionData(Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;)V

    return-void

    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v9, "install_non_market_apps"

    invoke-static {v7, v9, v10}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    goto :goto_0

    :cond_4
    if-eqz v6, :cond_5

    const/4 v7, 0x1

    :goto_2
    invoke-virtual {v5, v7}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;->setAllowUnknownSources(Z)Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    goto :goto_1

    :cond_5
    move v7, v8

    goto :goto_2
.end method
