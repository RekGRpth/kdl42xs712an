.class public Lcom/google/android/finsky/utils/CorpusResourceUtils;
.super Ljava/lang/Object;
.source "CorpusResourceUtils.java"


# static fields
.field private static sDetailRatingTilings:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/CorpusResourceUtils;->sDetailRatingTilings:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCorpusCellContentDescriptionResource(I)I
    .locals 3
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const v0, 0x7f070222    # com.android.vending.R.string.content_description_item_cell_movie

    :goto_0
    return v0

    :pswitch_2
    const v0, 0x7f070223    # com.android.vending.R.string.content_description_item_cell_magazine

    goto :goto_0

    :pswitch_3
    const v0, 0x7f070221    # com.android.vending.R.string.content_description_item_cell_generic

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getCorpusCellContentLongDescriptionResource(I)I
    .locals 3
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const v0, 0x7f070228    # com.android.vending.R.string.content_description_item_cell_movie_long

    :goto_0
    return v0

    :pswitch_2
    const v0, 0x7f070229    # com.android.vending.R.string.content_description_item_cell_magazine_long

    goto :goto_0

    :pswitch_3
    const v0, 0x7f070227    # com.android.vending.R.string.content_description_item_cell_generic_long

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getCorpusCellContentReasonDescriptionResource(I)I
    .locals 3
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const v0, 0x7f070225    # com.android.vending.R.string.content_description_item_rec_cell_movie

    :goto_0
    return v0

    :pswitch_2
    const v0, 0x7f070226    # com.android.vending.R.string.content_description_item_rec_cell_magazine

    goto :goto_0

    :pswitch_3
    const v0, 0x7f070224    # com.android.vending.R.string.content_description_item_rec_cell_generic

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getCorpusMyCollectionDescription(I)Ljava/lang/String;
    .locals 6
    .param p0    # I

    const/4 v4, 0x0

    if-nez p0, :cond_0

    const/4 p0, 0x3

    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-object v4

    :cond_2
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpusList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasBackend()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->getBackend()I

    move-result v5

    if-ne v5, p0, :cond_3

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLibraryName()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->getLibraryName()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static getCorpusMyCollectionIcon(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f02006e    # com.android.vending.R.drawable.ic_menu_market_myapps

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f02006f    # com.android.vending.R.drawable.ic_menu_market_mybooks

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020070    # com.android.vending.R.drawable.ic_menu_market_mymagazines

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020071    # com.android.vending.R.drawable.ic_menu_market_mymovies

    goto :goto_0

    :pswitch_4
    const v0, 0x7f020072    # com.android.vending.R.drawable.ic_menu_market_mymusic

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getCorpusSpinnerDrawable(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f02010a    # com.android.vending.R.drawable.spinner_background_apps

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f02010b    # com.android.vending.R.drawable.spinner_background_books

    goto :goto_0

    :pswitch_2
    const v0, 0x7f02010d    # com.android.vending.R.drawable.spinner_background_magazines

    goto :goto_0

    :pswitch_3
    const v0, 0x7f02010e    # com.android.vending.R.drawable.spinner_background_movies

    goto :goto_0

    :pswitch_4
    const v0, 0x7f02010f    # com.android.vending.R.drawable.spinner_background_music

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getDescriptionHeaderStringId(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const v0, 0x7f07015b    # com.android.vending.R.string.details_description

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f070159    # com.android.vending.R.string.details_synopsis

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public static getDetailsPageRatingDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # I

    sget-object v4, Lcom/google/android/finsky/utils/CorpusResourceUtils;->sDetailRatingTilings:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    const v0, 0x7f0200c5    # com.android.vending.R.drawable.play_ratingbar_rate_indicator_default

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->tileify(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, v3}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    sget-object v4, Lcom/google/android/finsky/utils/CorpusResourceUtils;->sDetailRatingTilings:Landroid/util/SparseArray;

    invoke-virtual {v4, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/Drawable;

    return-object v4

    :pswitch_1
    const v0, 0x7f0200c3    # com.android.vending.R.drawable.play_ratingbar_rate_indicator_apps

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0200c4    # com.android.vending.R.drawable.play_ratingbar_rate_indicator_books

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0200c6    # com.android.vending.R.drawable.play_ratingbar_rate_indicator_magazines

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0200c8    # com.android.vending.R.drawable.play_ratingbar_rate_indicator_music

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0200c7    # com.android.vending.R.drawable.play_ratingbar_rate_indicator_movies

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getLargeDetailsIconHeight(Landroid/content/Context;I)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const v0, 0x7f0b003c    # com.android.vending.R.dimen.details_summary_large_size

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    return v1

    :sswitch_0
    const v0, 0x7f0b003d    # com.android.vending.R.dimen.details_summary_large_square_size

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x4 -> :sswitch_0
        0x12 -> :sswitch_0
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public static getLargeDetailsIconWidth(Landroid/content/Context;I)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const v0, 0x7f0b003b    # com.android.vending.R.dimen.details_summary_large_width

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    return v1

    :sswitch_0
    const v0, 0x7f0b003d    # com.android.vending.R.dimen.details_summary_large_square_size

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x4 -> :sswitch_0
        0x12 -> :sswitch_0
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public static getMenuExpanderMaximized(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f020063    # com.android.vending.R.drawable.ic_menu_expander_maximized_holo_light

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f02005d    # com.android.vending.R.drawable.ic_menu_expander_apps_maximized_light

    goto :goto_0

    :pswitch_2
    const v0, 0x7f02005f    # com.android.vending.R.drawable.ic_menu_expander_books_maximized_light

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020061    # com.android.vending.R.drawable.ic_menu_expander_mags_maximized_light

    goto :goto_0

    :pswitch_4
    const v0, 0x7f020065    # com.android.vending.R.drawable.ic_menu_expander_movies_maximized_light

    goto :goto_0

    :pswitch_5
    const v0, 0x7f020067    # com.android.vending.R.drawable.ic_menu_expander_music_maximized_light

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getMenuExpanderMinimized(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f020064    # com.android.vending.R.drawable.ic_menu_expander_minimized_holo_light

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f02005e    # com.android.vending.R.drawable.ic_menu_expander_apps_minimized_light

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020060    # com.android.vending.R.drawable.ic_menu_expander_books_minimized_light

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020062    # com.android.vending.R.drawable.ic_menu_expander_mags_minimized_light

    goto :goto_0

    :pswitch_4
    const v0, 0x7f020066    # com.android.vending.R.drawable.ic_menu_expander_movies_minimized_light

    goto :goto_0

    :pswitch_5
    const v0, 0x7f020068    # com.android.vending.R.drawable.ic_menu_expander_music_minimized_light

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getOpenButtonStringId(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f070154    # com.android.vending.R.string.open

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f070126    # com.android.vending.R.string.play

    goto :goto_0

    :pswitch_2
    const v0, 0x7f070156    # com.android.vending.R.string.listen

    goto :goto_0

    :pswitch_3
    const v0, 0x7f070127    # com.android.vending.R.string.read

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getOwnedItemIndicator(I)I
    .locals 3
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const v0, 0x7f02004a    # com.android.vending.R.drawable.ic_checkmark_apps

    :goto_0
    return v0

    :pswitch_2
    const v0, 0x7f02004b    # com.android.vending.R.drawable.ic_checkmark_books

    goto :goto_0

    :pswitch_3
    const v0, 0x7f02004c    # com.android.vending.R.drawable.ic_checkmark_mags

    goto :goto_0

    :pswitch_4
    const v0, 0x7f02004d    # com.android.vending.R.drawable.ic_checkmark_movies

    goto :goto_0

    :pswitch_5
    const v0, 0x7f02004e    # com.android.vending.R.drawable.ic_checkmark_music

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPlayActionBarBackgroundDrawable(Landroid/content/Context;I)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f020003    # com.android.vending.R.drawable.action_bar_bg_home

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f020001    # com.android.vending.R.drawable.action_bar_bg_apps

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020002    # com.android.vending.R.drawable.action_bar_bg_books

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020004    # com.android.vending.R.drawable.action_bar_bg_mag

    goto :goto_0

    :pswitch_4
    const v0, 0x7f020005    # com.android.vending.R.drawable.action_bar_bg_movies

    goto :goto_0

    :pswitch_5
    const v0, 0x7f020006    # com.android.vending.R.drawable.action_bar_bg_music

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPlayActionButtonBackgroundDrawable(Landroid/content/Context;I)I
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0200b9    # com.android.vending.R.drawable.play_action_button_multi

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f0200b1    # com.android.vending.R.drawable.play_action_button_apps

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0200b3    # com.android.vending.R.drawable.play_action_button_books

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0200b5    # com.android.vending.R.drawable.play_action_button_magazines

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0200b7    # com.android.vending.R.drawable.play_action_button_movies

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0200bb    # com.android.vending.R.drawable.play_action_button_music

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPrimaryColor(Landroid/content/Context;I)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0a0011    # com.android.vending.R.color.multi_primary

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    :pswitch_1
    const v0, 0x7f0a000c    # com.android.vending.R.color.apps_primary

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a000d    # com.android.vending.R.color.books_primary

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a0010    # com.android.vending.R.color.magazines_primary

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a000e    # com.android.vending.R.color.movies_primary

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0a000f    # com.android.vending.R.color.music_primary

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPrimaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0a005f    # com.android.vending.R.color.multi_primary_text

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    return-object v1

    :pswitch_1
    const v0, 0x7f0a0056    # com.android.vending.R.color.apps_primary_text

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a0058    # com.android.vending.R.color.books_primary_text

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a005b    # com.android.vending.R.color.magazines_primary_text

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a005d    # com.android.vending.R.color.movies_primary_text

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0a0061    # com.android.vending.R.color.music_primary_text

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPurchaseButtonLockIcon(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0200ea    # com.android.vending.R.drawable.purchase_lock_neutral

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f0200e5    # com.android.vending.R.drawable.purchase_lock_apps

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0200e6    # com.android.vending.R.drawable.purchase_lock_books

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0200e7    # com.android.vending.R.drawable.purchase_lock_magazines

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0200e8    # com.android.vending.R.drawable.purchase_lock_movies

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0200e9    # com.android.vending.R.drawable.purchase_lock_music

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getPurchaseButtonPlayIcon(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0200f1    # com.android.vending.R.drawable.purchase_play_neutral

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f0200ec    # com.android.vending.R.drawable.purchase_play_apps

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0200ed    # com.android.vending.R.drawable.purchase_play_books

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0200ee    # com.android.vending.R.drawable.purchase_play_magazines

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0200ef    # com.android.vending.R.drawable.purchase_play_movies

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0200f0    # com.android.vending.R.drawable.purchase_play_music

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getQuickLinkPrismBackground(I)I
    .locals 3
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const v0, 0x7f020019    # com.android.vending.R.drawable.btn_corpus_apps_games

    :goto_0
    return v0

    :pswitch_2
    const v0, 0x7f02001a    # com.android.vending.R.drawable.btn_corpus_books

    goto :goto_0

    :pswitch_3
    const v0, 0x7f02001b    # com.android.vending.R.drawable.btn_corpus_mags

    goto :goto_0

    :pswitch_4
    const v0, 0x7f02001c    # com.android.vending.R.drawable.btn_corpus_movies

    goto :goto_0

    :pswitch_5
    const v0, 0x7f02001d    # com.android.vending.R.drawable.btn_corpus_music

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getRegularDetailsIconHeight(Landroid/content/Context;I)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported backend ID ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    const v0, 0x7f0b0008    # com.android.vending.R.dimen.summary_thumbnail_large_size

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    return v1

    :pswitch_2
    const v0, 0x7f0b0007    # com.android.vending.R.dimen.summary_thumbnail_small_size

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0a0060    # com.android.vending.R.color.multi_secondary_text

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    return-object v1

    :pswitch_1
    const v0, 0x7f0a0057    # com.android.vending.R.color.apps_secondary_text

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a0059    # com.android.vending.R.color.books_secondary_text

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a005c    # com.android.vending.R.color.magazines_secondary_text

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a005e    # com.android.vending.R.color.movies_secondary_text

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0a0062    # com.android.vending.R.color.music_secondary_text

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getShareHeaderId(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const v0, 0x7f0701ae    # com.android.vending.R.string.share_google_plus

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0701af    # com.android.vending.R.string.share_google_plus_music

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private static tileify(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
    .locals 13
    .param p0    # Landroid/graphics/drawable/Drawable;
    .param p1    # Z

    const/4 v12, 0x0

    const/4 v11, 0x1

    instance-of v10, p0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v10, :cond_3

    move-object v1, p0

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v0

    new-array v6, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_2

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/LayerDrawable;->getId(I)I

    move-result v4

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    const v10, 0x102000d    # android.R.id.progress

    if-eq v4, v10, :cond_0

    const v10, 0x102000f    # android.R.id.secondaryProgress

    if-ne v4, v10, :cond_1

    :cond_0
    move v10, v11

    :goto_1
    invoke-static {v12, v10}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->tileify(Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    aput-object v10, v6, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v10, 0x0

    goto :goto_1

    :cond_2
    new-instance v5, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v5, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v0, :cond_5

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/LayerDrawable;->getId(I)I

    move-result v10

    invoke-virtual {v5, v3, v10}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    instance-of v10, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v10, :cond_6

    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    const/16 v10, 0x8

    new-array v7, v10, [F

    fill-array-data v7, :array_0

    new-instance v8, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v10, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v10, v7, v12, v12}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-direct {v8, v10}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    new-instance v2, Landroid/graphics/BitmapShader;

    sget-object v10, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v12, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v2, v9, v10, v12}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v8}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v10

    invoke-virtual {v10, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    if-eqz p1, :cond_4

    new-instance v10, Landroid/graphics/drawable/ClipDrawable;

    const/4 v12, 0x3

    invoke-direct {v10, v8, v12, v11}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    move-object v8, v10

    :cond_4
    move-object v5, v8

    :cond_5
    :goto_3
    return-object v5

    :cond_6
    move-object v5, p0

    goto :goto_3

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method
