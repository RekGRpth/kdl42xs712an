.class public Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;
.super Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
.source "DetailsSummaryMoviesViewBinder.java"


# instance fields
.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;Lcom/google/android/finsky/library/Libraries;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Lcom/google/android/finsky/library/Libraries;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;-><init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V

    iput-object p3, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    return-void
.end method

.method private setupListOfferButton(Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/layout/play/PlayActionButton;II)V
    .locals 6
    .param p1    # Landroid/accounts/Account;
    .param p3    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    .param p4    # Lcom/google/android/finsky/layout/play/PlayActionButton;
    .param p5    # I
    .param p6    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Common$Offer;",
            ">;",
            "Lcom/google/android/finsky/utils/DocUtils$OfferFilter;",
            "Lcom/google/android/finsky/layout/play/PlayActionButton;",
            "II)V"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_0

    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {p0, p1, v1, p4}, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->setupSingleOfferButton(Landroid/accounts/Account;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/layout/play/PlayActionButton;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p2, v3}, Lcom/google/android/finsky/utils/DocUtils;->getLowestPricedOffer(Ljava/util/List;Z)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    invoke-virtual {p4, v5}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mContext:Landroid/content/Context;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, p5, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;

    invoke-direct {v3, p0, p6, p3}, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$2;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;)V

    invoke-virtual {p4, v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private setupSingleOfferButton(Landroid/accounts/Account;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/layout/play/PlayActionButton;)V
    .locals 11
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/finsky/protos/Common$Offer;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayActionButton;

    const/4 v4, 0x0

    const/4 v8, -0x1

    const/4 v6, -0x1

    invoke-virtual {p2}, Lcom/google/android/finsky/protos/Common$Offer;->getOfferType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-gez v8, :cond_0

    :goto_1
    return-void

    :pswitch_1
    const v8, 0x7f070143    # com.android.vending.R.string.rent_hd

    const/16 v6, 0xe5

    goto :goto_0

    :pswitch_2
    const v8, 0x7f070144    # com.android.vending.R.string.rent_sd

    const/16 v6, 0xe4

    goto :goto_0

    :pswitch_3
    const v8, 0x7f070147    # com.android.vending.R.string.buy_hd

    const/16 v6, 0xe8

    goto :goto_0

    :pswitch_4
    const v8, 0x7f070146    # com.android.vending.R.string.buy_sd

    const/16 v6, 0xe7

    goto :goto_0

    :cond_0
    invoke-virtual {p3, v4}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v9

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/finsky/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v8, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mExternalReferrer:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mContinueUrl:Ljava/lang/String;

    const/4 v7, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p3, v9, v10, v0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected setupActionButtons(Z)V
    .locals 5
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupActionButtons(Z)V

    const v2, 0x7f0800b4    # com.android.vending.R.id.download_button

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayActionButton;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.google.android.tv"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    const v3, 0x7f070153    # com.android.vending.R.string.download

    new-instance v4, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$1;

    invoke-direct {v4, p0, v1}, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder$1;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;Landroid/accounts/Account;)V

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected setupBuyButtons(Landroid/accounts/Account;Lcom/google/android/finsky/layout/play/PlayActionButton;Lcom/google/android/finsky/layout/play/PlayActionButton;Z)V
    .locals 18
    .param p1    # Landroid/accounts/Account;
    .param p2    # Lcom/google/android/finsky/layout/play/PlayActionButton;
    .param p3    # Lcom/google/android/finsky/layout/play/PlayActionButton;
    .param p4    # Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->setupSingleOfferButton(Landroid/accounts/Account;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/layout/play/PlayActionButton;)V

    :cond_0
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->setupSingleOfferButton(Landroid/accounts/Account;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/layout/play/PlayActionButton;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v17

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {v14}, Lcom/google/android/finsky/protos/Common$Offer;->getOfferType()I

    move-result v15

    packed-switch v15, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    :pswitch_1
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-interface {v5, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    sget-object v6, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->RENTAL:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    const v8, 0x7f070145    # com.android.vending.R.string.rent_resolution

    const/16 v9, 0xe6

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v9}, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->setupListOfferButton(Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/layout/play/PlayActionButton;II)V

    :cond_4
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v9, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->PURCHASE:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    const v11, 0x7f070148    # com.android.vending.R.string.purchase_resolution

    const/16 v12, 0xe9

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, v17

    move-object/from16 v10, p3

    invoke-direct/range {v6 .. v12}, Lcom/google/android/finsky/activities/DetailsSummaryMoviesViewBinder;->setupListOfferButton(Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;Lcom/google/android/finsky/layout/play/PlayActionButton;II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
