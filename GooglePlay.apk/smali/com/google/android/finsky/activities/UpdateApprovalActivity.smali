.class public Lcom/google/android/finsky/activities/UpdateApprovalActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "UpdateApprovalActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;
    }
.end annotation


# instance fields
.field private mBaseLayout:Landroid/widget/FrameLayout;

.field private mCurrentApprovalType:I

.field private mCurrentUpdate:I

.field private mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mUpdateApprovalLayout:Lcom/google/android/finsky/layout/UpdateApprovalLayout;

.field private mUpdatesForApproval:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method public static getIntent(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/installer/InstallPolicies$UpdateWarnings;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v3, Landroid/content/Intent;

    const-class v0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;

    invoke-direct {v3, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    new-instance v5, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/installer/InstallPolicies$UpdateWarnings;

    invoke-direct {v5, v0, v1, p3}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;-><init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/installer/InstallPolicies$UpdateWarnings;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    const-string v0, "UpdateApprovalActivity.updates-list"

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v3
.end method

.method private installUpdate(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)V
    .locals 5
    .param p1    # Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;

    iget-object v0, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    # getter for: Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->packageName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->access$000(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->versionCode:I
    invoke-static {p1}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->access$300(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)I

    move-result v2

    # getter for: Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->packageTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->access$100(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->reason:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->access$400(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/receivers/Installer;->updateSingleInstalledApp(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showCurrentWarning()V
    .locals 9

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdatesForApproval:Ljava/util/List;

    iget v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;

    iget-object v0, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdateApprovalLayout:Lcom/google/android/finsky/layout/UpdateApprovalLayout;

    # getter for: Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->packageName:Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->access$000(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->packageTitle:Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->access$100(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdatesForApproval:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget v6, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    if-ne v6, v5, :cond_0

    :goto_0
    # getter for: Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->permissions:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->access$200(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)Ljava/util/ArrayList;

    move-result-object v6

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->set(Ljava/lang/String;Ljava/lang/String;IIZLjava/util/List;Lcom/google/android/finsky/activities/UpdateApprovalActivity;)V

    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private showNextOrFinish()V
    .locals 3

    iget v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    iget-object v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdatesForApproval:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdatesForApproval:Ljava/util/List;

    iget v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->needsLargeUpdateWarning()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    invoke-direct {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->showCurrentWarning()V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->needsPermissionsWarning()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    invoke-direct {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->showCurrentWarning()V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    invoke-direct {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->showNextOrFinish()V

    goto :goto_0
.end method


# virtual methods
.method public getPermissionInfo(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PermissionInfo;",
            ">;"
        }
    .end annotation

    const/4 v8, 0x0

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    :try_start_0
    iget-object v5, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v6, 0x0

    invoke-virtual {v5, v2, v6}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, "Name not found while getting permission %s for %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v8

    const/4 v7, 0x1

    aput-object p2, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-object v4
.end method

.method public onApprove()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdatesForApproval:Ljava/util/List;

    iget v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;

    iget v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->setLargeUpdateWarningAccepted()V

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->done()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->installUpdate(Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->showNextOrFinish()V

    return-void

    :cond_2
    iget v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity$UpdateDetails;->setPermissionsWarningAccepted()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    iput-object p1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mSavedInstanceState:Landroid/os/Bundle;

    const v2, 0x7f040122    # com.android.vending.R.layout.update_approval_activity

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->setContentView(I)V

    const v2, 0x7f08024a    # com.android.vending.R.id.main_layout

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mBaseLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040123    # com.android.vending.R.layout.update_approval_layout

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/UpdateApprovalLayout;

    iput-object v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdateApprovalLayout:Lcom/google/android/finsky/layout/UpdateApprovalLayout;

    iget-object v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mBaseLayout:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdateApprovalLayout:Lcom/google/android/finsky/layout/UpdateApprovalLayout;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iput v5, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    iput v5, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    if-eqz p1, :cond_0

    const-string v2, "UpdateApprovalActivity.current-update"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    const-string v2, "UpdateApprovalActivity.current-type"

    invoke-virtual {p1, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "UpdateApprovalActivity.updates-list"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdatesForApproval:Ljava/util/List;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->showNextOrFinish()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mUpdateApprovalLayout:Lcom/google/android/finsky/layout/UpdateApprovalLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->saveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "UpdateApprovalActivity.current-update"

    iget v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "UpdateApprovalActivity.current-type"

    iget v1, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onSkip()V
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentUpdate:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->mCurrentApprovalType:I

    invoke-direct {p0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->showNextOrFinish()V

    return-void
.end method
