.class public Lcom/google/android/finsky/activities/SimpleAlertDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "SimpleAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;,
        Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    }
.end annotation


# instance fields
.field private mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mHasBeenDismissed:Z

.field private mShouldLogImpression:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mShouldLogImpression:Z

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/SimpleAlertDialog;)Z
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/SimpleAlertDialog;

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/SimpleAlertDialog;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/SimpleAlertDialog;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    .locals 1
    .param p0    # Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-result-object v0

    return-object v0
.end method

.method private getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance(III)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-static {p0, v0, v1, p1, p2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(ILjava/lang/String;III)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private static newInstance(ILjava/lang/String;III)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 3
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v1, Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "message"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "layoutId"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "positive_id"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "negative_id"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # I

    const/4 v0, -0x1

    invoke-static {v0, p0, v0, p1, p2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(ILjava/lang/String;III)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public static newInstanceWithLayout(III)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-static {v0, v1, p0, p1, p2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(ILjava/lang/String;III)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 8
    .param p1    # Landroid/content/DialogInterface;

    const/4 v6, -0x1

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    iget-boolean v5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "click_event_type_negative"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v6, :cond_2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v5, v1, v6, v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_2
    const-string v5, "extra_arguments"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "target_request_code"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;->onNegativeClick(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 27
    .param p1    # Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v15

    const-string v2, "message_id"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v21

    const-string v2, "message"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v2, "layoutId"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v19

    const-string v2, "positive_id"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v23

    const-string v2, "negative_id"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    const-string v2, "extra_arguments"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    const-string v2, "config_arguments"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v17

    const-string v2, "target_request_code"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const-string v2, "impression_type"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "impression_type"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v25

    const-string v2, "impression_cookie"

    invoke-static {v15, v2}, Lcom/google/android/finsky/utils/ParcelableProto;->getByteStringMicroFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v24

    new-instance v2, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/4 v3, 0x0

    const/4 v8, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v24

    invoke-direct {v2, v0, v1, v3, v8}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mShouldLogImpression:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v2, v8, v9, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mShouldLogImpression:Z

    :cond_1
    const-string v2, "click_event_type_positive"

    const/4 v3, -0x1

    invoke-virtual {v15, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    const-string v2, "click_event_type_negative"

    const/4 v3, -0x1

    invoke-virtual {v15, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    new-instance v16, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v2, -0x1

    move/from16 v0, v21

    if-eq v0, v2, :cond_7

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    :cond_2
    :goto_0
    const/4 v2, -0x1

    move/from16 v0, v23

    if-eq v0, v2, :cond_3

    new-instance v2, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;-><init>(Lcom/google/android/finsky/activities/SimpleAlertDialog;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILandroid/os/Bundle;)V

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_3
    const/4 v2, -0x1

    move/from16 v0, v22

    if-eq v0, v2, :cond_4

    new-instance v8, Lcom/google/android/finsky/activities/SimpleAlertDialog$2;

    move-object/from16 v9, p0

    move-object v11, v5

    move v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/finsky/activities/SimpleAlertDialog$2;-><init>(Lcom/google/android/finsky/activities/SimpleAlertDialog;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILandroid/os/Bundle;)V

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_4
    invoke-virtual/range {v16 .. v16}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v14

    const/4 v2, -0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_5

    invoke-virtual {v14}, Landroid/app/AlertDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    move/from16 v0, v19

    invoke-virtual {v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v14, v0}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    move-object/from16 v0, v26

    instance-of v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    if-eqz v2, :cond_5

    if-eqz v17, :cond_5

    check-cast v26, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;->configureView(Landroid/os/Bundle;)V

    :cond_5
    const-string v2, "cancel_on_touch_outside"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "cancel_on_touch_outside"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v14, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    :cond_6
    return-object v14

    :cond_7
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p1    # Landroid/support/v4/app/Fragment;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    if-nez p3, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_arguments"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "target_request_code"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setArguments(Landroid/os/Bundle;)V

    :cond_1
    return-object p0
.end method

.method public setCanceledOnTouchOutside(Z)V
    .locals 2
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "cancel_on_touch_outside"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setEventLog(ILcom/google/protobuf/micro/ByteStringMicro;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/protobuf/micro/ByteStringMicro;
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "impression_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "impression_cookie"

    invoke-static {v0, v1, p2}, Lcom/google/android/finsky/utils/ParcelableProto;->putByteStringMicroIntoBundle(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/micro/ByteStringMicro;)V

    const-string v1, "click_event_type_positive"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "click_event_type_negative"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setArguments(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mShouldLogImpression:Z

    return-object p0
.end method

.method public setViewConfiguration(Landroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_arguments"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setArguments(Landroid/os/Bundle;)V

    return-object p0
.end method
