.class Lcom/google/android/finsky/activities/ReviewDialog$5;
.super Ljava/lang/Object;
.source "ReviewDialog.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/ReviewDialog;->getPlusProfile()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/ReviewDialog;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/ReviewDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V
    .locals 6
    .param p1    # Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->isResumed()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Fragment is not resumed, giving up on G+ review flow"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->getPlusProfile()Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # setter for: Lcom/google/android/finsky/activities/ReviewDialog;->mUserProfile:Lcom/google/android/finsky/protos/PlusData$PlusProfile;
    invoke-static {v2, v0}, Lcom/google/android/finsky/activities/ReviewDialog;->access$702(Lcom/google/android/finsky/activities/ReviewDialog;Lcom/google/android/finsky/protos/PlusData$PlusProfile;)Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mPreviousReviewId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$300(Lcom/google/android/finsky/activities/ReviewDialog;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mAuthorName:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1000(Lcom/google/android/finsky/activities/ReviewDialog;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # setter for: Lcom/google/android/finsky/activities/ReviewDialog;->mReviewIsPublic:Z
    invoke-static {v2, v5}, Lcom/google/android/finsky/activities/ReviewDialog;->access$802(Lcom/google/android/finsky/activities/ReviewDialog;Z)Z

    :goto_1
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mAcceptedPlus:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1300(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialog;->confirmPlus(Lcom/google/android/finsky/protos/PlusData$PlusProfile;)V
    invoke-static {v2, v0}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1400(Lcom/google/android/finsky/activities/ReviewDialog;Lcom/google/android/finsky/protos/PlusData$PlusProfile;)V

    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialog;->syncOkButtonState()V
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$000(Lcom/google/android/finsky/activities/ReviewDialog;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    const/4 v3, 0x1

    # setter for: Lcom/google/android/finsky/activities/ReviewDialog;->mReviewIsPublic:Z
    invoke-static {v2, v3}, Lcom/google/android/finsky/activities/ReviewDialog;->access$802(Lcom/google/android/finsky/activities/ReviewDialog;Z)Z

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mAuthorName:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1000(Lcom/google/android/finsky/activities/ReviewDialog;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mProfilePic:Lcom/google/android/finsky/layout/FifeImageView;
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1100(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/finsky/layout/FifeImageView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getProfileImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/finsky/utils/BitmapLoader;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mReviewBy:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1200(Lcom/google/android/finsky/activities/ReviewDialog;)Landroid/widget/TextView;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mAuthorName:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1000(Lcom/google/android/finsky/activities/ReviewDialog;)Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f0701b5    # com.android.vending.R.string.review_dialog_title

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialog$5;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # setter for: Lcom/google/android/finsky/activities/ReviewDialog;->mRequiresPlusCheck:Z
    invoke-static {v2, v5}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1502(Lcom/google/android/finsky/activities/ReviewDialog;Z)Z

    goto :goto_2
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/ReviewDialog$5;->onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V

    return-void
.end method
