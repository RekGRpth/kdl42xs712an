.class Lcom/google/android/finsky/activities/AvailablePromoOfferActivity$2;
.super Ljava/lang/Object;
.source "AvailablePromoOfferActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;

.field final synthetic val$noActionMessage:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity$2;->this$0:Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;

    iput-object p2, p0, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity$2;->val$noActionMessage:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity$2;->this$0:Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;

    const-string v3, "promoOfferSkip"

    # invokes: Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;->logAnalytics(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;->access$000(Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity$2;->val$noActionMessage:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity$2;->this$0:Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "no_action_message"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity$2;->val$noActionMessage:Ljava/lang/String;

    const v3, 0x7f0701df    # com.android.vending.R.string.ok

    const/4 v4, -0x1

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v5, v2, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    const-string v2, "no_action_message"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity$2;->this$0:Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;

    # invokes: Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;->setResultAndFinish()V
    invoke-static {v2}, Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;->access$400(Lcom/google/android/finsky/activities/AvailablePromoOfferActivity;)V

    goto :goto_0
.end method
