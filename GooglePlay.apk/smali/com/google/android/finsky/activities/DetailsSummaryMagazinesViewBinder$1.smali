.class Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;
.super Ljava/lang/Object;
.source "DetailsSummaryMagazinesViewBinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->setupSubscriptionListOfferButton(Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/finsky/layout/play/PlayActionButton;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;

.field final synthetic val$account:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;

    iput-object p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0xe2

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;

    iget-object v2, v2, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;

    iget-object v2, v2, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;

    iget-object v5, v5, Lcom/google/android/finsky/activities/DetailsSummaryMagazinesViewBinder;->mExternalReferrer:Ljava/lang/String;

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->buy(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
