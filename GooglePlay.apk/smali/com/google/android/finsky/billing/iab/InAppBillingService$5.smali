.class Lcom/google/android/finsky/billing/iab/InAppBillingService$5;
.super Ljava/lang/Object;
.source "InAppBillingService.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/iab/InAppBillingService;->performIsSetupRequiredCheck(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

.field final synthetic val$authenticator:Lcom/android/volley/toolbox/AndroidAuthenticator;

.field final synthetic val$checkoutToken:Ljava/lang/String;

.field final synthetic val$invalidCheckoutToken:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

.field final synthetic val$semaphore:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/android/volley/toolbox/AndroidAuthenticator;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;Ljava/util/concurrent/Semaphore;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$authenticator:Lcom/android/volley/toolbox/AndroidAuthenticator;

    iput-object p3, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$checkoutToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$invalidCheckoutToken:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p5, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    iput-object p6, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;->getCheckoutTokenRequired()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$authenticator:Lcom/android/volley/toolbox/AndroidAuthenticator;

    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$checkoutToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/volley/toolbox/AndroidAuthenticator;->invalidateAuthToken(Ljava/lang/String;)V

    const-string v0, "Checkout token expired."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$invalidCheckoutToken:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->val$responseCode:[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {p1}, Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;->getUserHasValidInstrument()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_NO_SETUP_REQUIRED:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    :goto_1
    aput-object v0, v1, v2

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_SETUP_REQUIRED:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingService$5;->onResponse(Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;)V

    return-void
.end method
