.class public Lcom/google/android/finsky/billing/giftcard/GiftCardFormOfPayment;
.super Lcom/google/android/finsky/billing/InstrumentFactory$FormOfPayment;
.source "GiftCardFormOfPayment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/billing/InstrumentFactory$FormOfPayment;-><init>()V

    return-void
.end method

.method public static registerWithInstrumentFactory(Lcom/google/android/finsky/billing/InstrumentFactory;)V
    .locals 2
    .param p0    # Lcom/google/android/finsky/billing/InstrumentFactory;

    const/16 v0, 0x64

    new-instance v1, Lcom/google/android/finsky/billing/giftcard/GiftCardFormOfPayment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/giftcard/GiftCardFormOfPayment;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/InstrumentFactory;->registerFormOfPayment(ILcom/google/android/finsky/billing/InstrumentFactory$FormOfPayment;)V

    return-void
.end method


# virtual methods
.method public create(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/BillingFlow;
    .locals 1
    .param p1    # Lcom/google/android/finsky/billing/BillingFlowContext;
    .param p2    # Lcom/google/android/finsky/billing/BillingFlowListener;
    .param p3    # Landroid/os/Bundle;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
