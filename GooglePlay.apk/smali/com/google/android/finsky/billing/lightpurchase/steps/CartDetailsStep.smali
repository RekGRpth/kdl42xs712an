.class public Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;
.super Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;
.source "CartDetailsStep.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# static fields
.field private static final PATTERN_EM_TAG:Ljava/util/regex/Pattern;

.field private static final PATTERN_END_EM_TAG:Ljava/util/regex/Pattern;


# instance fields
.field private mBackend:I

.field private mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

.field private mContinueButtonConfirmsPurchase:Z

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mExpanded:Z

.field private mHeader:Landroid/view/View;

.field private mIsExpandable:Z

.field private mPaymentSettingsView:Landroid/widget/TextView;

.field private mPriceView:Landroid/widget/TextView;

.field private mPurchaseDetailsView:Landroid/view/View;

.field private final mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

.field private mUseBlueHighlight:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "<em>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->PATTERN_EM_TAG:Ljava/util/regex/Pattern;

    const-string v0, "</em>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->PATTERN_END_EM_TAG:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;-><init>()V

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    const/16 v1, 0x2c6

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method public static newInstance(IZLcom/google/android/finsky/protos/Purchase$ClientCart;)Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;
    .locals 4
    .param p0    # I
    .param p1    # Z
    .param p2    # Lcom/google/android/finsky/protos/Purchase$ClientCart;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;-><init>()V

    const-string v2, "CartDetailsStep.backend"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "CartDetailsStep.useBlueHighlight"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "CartDetailsStep.cart"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->setArguments(Landroid/os/Bundle;)V

    iput-object p2, v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    return-object v1
.end method

.method private static parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # I

    const-string v1, "#%06X"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0xffffff

    and-int/2addr v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->PATTERN_EM_TAG:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<b><font color=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    sget-object v1, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->PATTERN_END_EM_TAG:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string v2, "</font></b>"

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    return-object v1
.end method

.method private populateContainerWithTextViews(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;ILjava/util/List;I)V
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Landroid/view/ViewGroup;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, p3, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2, p5}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    :goto_1
    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_1
    const/16 v3, 0x8

    goto :goto_1
.end method

.method private toggleExpansion()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v1, 0x0

    const/16 v3, 0x2cb

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->updateExpansion()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateExpansion()V
    .locals 5

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v2

    iget-boolean v4, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->expand(Z)V

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPurchaseDetailsView:Landroid/view/View;

    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mIsExpandable:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUseBlueHighlight:Z

    if-eqz v2, :cond_2

    move v0, v3

    :goto_1
    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    if-eqz v2, :cond_3

    invoke-static {v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getMenuExpanderMaximized(I)I

    move-result v1

    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v2, v3, v3, v1, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :cond_0
    return-void

    :cond_1
    const/16 v2, 0x8

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mBackend:I

    goto :goto_1

    :cond_3
    invoke-static {v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getMenuExpanderMinimized(I)I

    move-result v1

    goto :goto_2
.end method


# virtual methods
.method public allowContinueButtonIcon()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mContinueButtonConfirmsPurchase:Z

    return v0
.end method

.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getButtonText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mHeader:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x2c9

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->toggleExpansion()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x2ca

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->launchBillingProfile()V

    goto :goto_0
.end method

.method public onContinueButtonClicked(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mContinueButtonConfirmsPurchase:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x2c7

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->confirmPurchase()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x2c8

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->launchBillingProfile()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CartDetailsStep.backend"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mBackend:I

    const-string v1, "CartDetailsStep.useBlueHighlight"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUseBlueHighlight:Z

    const-string v1, "CartDetailsStep.cart"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    if-eqz p1, :cond_0

    const-string v1, "CartDetailsStep.expanded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 26
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mBackend:I

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUseBlueHighlight:Z

    if-eqz v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004a    # com.android.vending.R.color.purchase_blue_highlight

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    :goto_0
    const v2, 0x7f04008c    # com.android.vending.R.layout.light_purchase_cart

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v22

    const v2, 0x7f080163    # com.android.vending.R.id.item_title

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/TextView;

    const v2, 0x7f080165    # com.android.vending.R.id.item_price

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    const v2, 0x7f080164    # com.android.vending.R.id.instrument

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    const v2, 0x7f080169    # com.android.vending.R.id.instrument_setup_message

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    const v2, 0x7f080158    # com.android.vending.R.id.account

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f08016a    # com.android.vending.R.id.purchase_details

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPurchaseDetailsView:Landroid/view/View;

    const v2, 0x7f08016d    # com.android.vending.R.id.footer

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    const v2, 0x7f08016b    # com.android.vending.R.id.payment_settings

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f0800ce    # com.android.vending.R.id.header

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mHeader:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getTitle()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v3}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getFormattedPrice()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0063    # com.android.vending.R.color.play_primary_text

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUseBlueHighlight:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    move/from16 v0, v24

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f080166    # com.android.vending.R.id.price_byline

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getPriceByline()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mUseBlueHighlight:Z

    if-eqz v2, :cond_4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_2
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasInstrument()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getInstrument()Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getDisplayTitle()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getDisabledInfoCount()I

    move-result v2

    if-lez v2, :cond_5

    const v2, 0x7f080168    # com.android.vending.R.id.instrument_error_message

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getDisabledInfo(I)Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->getDisabledMessageHtml()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    const v2, 0x7f080167    # com.android.vending.R.id.detail_messages

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    const v11, 0x7f04008d    # com.android.vending.R.layout.light_purchase_cart_detail_message

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getDetailHtmlList()Ljava/util/List;

    move-result-object v12

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->populateContainerWithTextViews(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;ILjava/util/List;I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml()Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getFooterHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v7}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mIsExpandable:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mHeader:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->updateExpansion()V

    return-object v22

    :cond_2
    invoke-virtual {v15}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v7

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_2

    :cond_5
    const v2, 0x7f08016c    # com.android.vending.R.id.extended_detail_messages

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    const v5, 0x7f04008e    # com.android.vending.R.layout.light_purchase_cart_extended_detail_message

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getExtendedDetailHtmlList()Ljava/util/List;

    move-result-object v6

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->populateContainerWithTextViews(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;ILjava/util/List;I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->getPurchaseFragment()Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mIsExpandable:Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mContinueButtonConfirmsPurchase:Z

    goto/16 :goto_3

    :cond_6
    const/16 v2, 0x8

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getAddInstrumentPromptHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v7}, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_7
    const/16 v2, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "CartDetailsStep.expanded"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/CartDetailsStep;->mExpanded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
