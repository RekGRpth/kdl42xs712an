.class Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;
.super Ljava/lang/Object;
.source "BillingProfileFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderInstruments(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

.field final synthetic val$clientCookie:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

.field final synthetic val$instrumentId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;->val$clientCookie:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    iput-object p3, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;->val$instrumentId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$600(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x322

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;->val$clientCookie:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEventWithClientCookie(ILcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$7;->val$instrumentId:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->notifyListenerOnInstrumentSelected(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$1200(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Ljava/lang/String;)V

    return-void
.end method
