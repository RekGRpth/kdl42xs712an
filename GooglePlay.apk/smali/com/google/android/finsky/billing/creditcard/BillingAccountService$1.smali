.class Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;
.super Lcom/android/vending/billing/IBillingAccountService$Stub;
.source "BillingAccountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    invoke-direct {p0}, Lcom/android/vending/billing/IBillingAccountService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getOffers(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 8
    .param p1    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    invoke-static {p1, v4}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v4, "Received invalid account name: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "result_code"

    const/4 v5, -0x5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_0
    return-object v3

    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    # invokes: Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->checkPromoOffers(Landroid/accounts/Account;)Landroid/os/Bundle;
    invoke-static {v4, v0}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->access$200(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Landroid/accounts/Account;)Landroid/os/Bundle;
    :try_end_0
    .catch Lcom/android/volley/AuthFailureError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_1
    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    # invokes: Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->checkPromoOffers(Landroid/accounts/Account;)Landroid/os/Bundle;
    invoke-static {v4, v0}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->access$200(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Landroid/accounts/Account;)Landroid/os/Bundle;
    :try_end_1
    .catch Lcom/android/volley/AuthFailureError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_0

    :catch_1
    move-exception v2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "result_code"

    const/4 v5, -0x3

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public hasValidCreditCard(Ljava/lang/String;)I
    .locals 13
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v9, -0x3

    const/4 v8, -0x4

    const/4 v12, 0x0

    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    const/4 v0, 0x1

    new-array v4, v0, [I

    new-instance v5, Ljava/util/concurrent/Semaphore;

    invoke-direct {v5, v12}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    invoke-static {p1, v0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v6

    if-nez v6, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Received invalid account name: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v0, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, -0x5

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/android/volley/toolbox/AndroidAuthenticator;

    iget-object v10, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    sget-object v0, Lcom/google/android/finsky/config/G;->checkoutAuthTokenType:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v10, v6, v0}, Lcom/android/volley/toolbox/AndroidAuthenticator;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v10, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v10}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v2

    :try_start_0
    invoke-virtual {v1}, Lcom/android/volley/toolbox/AndroidAuthenticator;->getAuthToken()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    # invokes: Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->checkValidInstrument(Lcom/android/volley/toolbox/Authenticator;Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;[ILjava/util/concurrent/Semaphore;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->access$000(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Lcom/android/volley/toolbox/Authenticator;Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;[ILjava/util/concurrent/Semaphore;)V

    const-wide/16 v10, 0x2d

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v10, v11, v0}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    const/4 v10, -0x4

    aput v10, v4, v0

    :cond_1
    const/4 v0, 0x0

    aget v0, v4, v0

    # invokes: Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->logResult(Lcom/google/android/finsky/api/DfeApi;I)V
    invoke-static {v2, v0}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->access$100(Lcom/google/android/finsky/api/DfeApi;I)V

    const/4 v0, 0x0

    aget v0, v4, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/AuthFailureError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v0, "Timed out while waiting for response."

    new-array v9, v12, [Ljava/lang/Object;

    invoke-static {v0, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    # invokes: Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->logResult(Lcom/google/android/finsky/api/DfeApi;I)V
    invoke-static {v2, v8}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->access$100(Lcom/google/android/finsky/api/DfeApi;I)V

    move v0, v8

    goto :goto_0

    :catch_1
    move-exception v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AuthFailureError: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/android/volley/AuthFailureError;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v0, v8}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    # invokes: Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->logResult(Lcom/google/android/finsky/api/DfeApi;I)V
    invoke-static {v2, v9}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->access$100(Lcom/google/android/finsky/api/DfeApi;I)V

    move v0, v9

    goto :goto_0
.end method
