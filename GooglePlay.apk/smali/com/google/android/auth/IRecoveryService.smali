.class public interface abstract Lcom/google/android/auth/IRecoveryService;
.super Ljava/lang/Object;
.source "IRecoveryService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/auth/IRecoveryService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getAccountRecoveryDecision(Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)Lcom/google/android/gms/auth/RecoveryDecision;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
