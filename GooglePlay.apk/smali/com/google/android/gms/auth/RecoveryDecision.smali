.class public Lcom/google/android/gms/auth/RecoveryDecision;
.super Ljava/lang/Object;
.source "RecoveryDecision.java"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/RecoveryDecisionCreator;


# instance fields
.field public isRecoveryInfoNeeded:Z

.field public isRecoveryInterstitialAllowed:Z

.field mVersionCode:I

.field public recoveryIntent:Landroid/app/PendingIntent;

.field public recoveryIntentWithoutIntro:Landroid/app/PendingIntent;

.field public showRecoveryInterstitial:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/auth/RecoveryDecisionCreator;

    invoke-direct {v0}, Lcom/google/android/gms/auth/RecoveryDecisionCreator;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/RecoveryDecision;->CREATOR:Lcom/google/android/gms/auth/RecoveryDecisionCreator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/auth/RecoveryDecision;->CREATOR:Lcom/google/android/gms/auth/RecoveryDecisionCreator;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    sget-object v0, Lcom/google/android/gms/auth/RecoveryDecision;->CREATOR:Lcom/google/android/gms/auth/RecoveryDecisionCreator;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/auth/RecoveryDecisionCreator;->writeToParcel(Lcom/google/android/gms/auth/RecoveryDecision;Landroid/os/Parcel;I)V

    return-void
.end method
