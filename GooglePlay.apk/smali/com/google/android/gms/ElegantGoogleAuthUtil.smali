.class public Lcom/google/android/gms/ElegantGoogleAuthUtil;
.super Ljava/lang/Object;
.source "ElegantGoogleAuthUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/ElegantGoogleAuthUtil$BlockingServiceConnection;
    }
.end annotation


# static fields
.field private static final RECOVERY_MGMT_SERVICE_COMPONENT_NAME:Landroid/content/ComponentName;

.field private static final RECOVERY_MGMT_SERVICE_INTENT:Landroid/content/Intent;

.field private static sKeyAndroidPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "androidPackageName"

    sput-object v0, Lcom/google/android/gms/ElegantGoogleAuthUtil;->sKeyAndroidPackageName:Ljava/lang/String;

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.gms"

    const-string v2, "com.google.android.gms.recovery.RecoveryService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/ElegantGoogleAuthUtil;->RECOVERY_MGMT_SERVICE_COMPONENT_NAME:Landroid/content/ComponentName;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-object v1, Lcom/google/android/gms/ElegantGoogleAuthUtil;->RECOVERY_MGMT_SERVICE_COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ElegantGoogleAuthUtil;->RECOVERY_MGMT_SERVICE_INTENT:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static ensurePlayServicesAvailable(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "GooglePlayServices is not available."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    throw v1

    :cond_0
    return-void
.end method

.method public static getRecoveryDetails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/PendingIntent;
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    invoke-static {p0}, Lcom/google/android/gms/ElegantGoogleAuthUtil;->ensurePlayServicesAvailable(Landroid/content/Context;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v7, Lcom/google/android/gms/ElegantGoogleAuthUtil;->sKeyAndroidPackageName:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v7, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/ElegantGoogleAuthUtil$BlockingServiceConnection;

    invoke-direct {v0}, Lcom/google/android/gms/ElegantGoogleAuthUtil$BlockingServiceConnection;-><init>()V

    sget-object v7, Lcom/google/android/gms/ElegantGoogleAuthUtil;->RECOVERY_MGMT_SERVICE_INTENT:Landroid/content/Intent;

    invoke-virtual {p0, v7, v0, v9}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v7

    if-eqz v7, :cond_4

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/ElegantGoogleAuthUtil$BlockingServiceConnection;->getService()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/auth/IRecoveryService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/auth/IRecoveryService;

    move-result-object v5

    invoke-interface {v5, p1, p2, p3, v2}, Lcom/google/android/auth/IRecoveryService;->getAccountRecoveryDecision(Ljava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;)Lcom/google/android/gms/auth/RecoveryDecision;

    move-result-object v4

    if-nez v4, :cond_0

    const-string v7, "RecoveryDecision was null"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    move-object v7, v8

    :goto_0
    return-object v7

    :cond_0
    :try_start_1
    sget-object v7, Lcom/google/android/finsky/config/G;->debugAlwaysPromptForGaiaRecovery:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v7}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget-boolean v7, v4, Lcom/google/android/gms/auth/RecoveryDecision;->showRecoveryInterstitial:Z

    if-nez v7, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    move v6, v9

    :cond_2
    if-eqz v6, :cond_3

    iget-boolean v7, v4, Lcom/google/android/gms/auth/RecoveryDecision;->isRecoveryInterstitialAllowed:Z

    if-eqz v7, :cond_3

    iget-object v7, v4, Lcom/google/android/gms/auth/RecoveryDecision;->recoveryIntentWithoutIntro:Landroid/app/PendingIntent;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    move-object v7, v8

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v7, "Failed to get GAIA recovery details"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v1, v7, v9}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Exception;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    move-object v7, v8

    goto :goto_0

    :catchall_0
    move-exception v7

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v7

    :cond_4
    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not bind to service: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/google/android/gms/ElegantGoogleAuthUtil;->RECOVERY_MGMT_SERVICE_INTENT:Landroid/content/Intent;

    invoke-virtual {v9}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7
.end method
