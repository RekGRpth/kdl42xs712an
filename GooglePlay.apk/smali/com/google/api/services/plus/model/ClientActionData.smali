.class public final Lcom/google/api/services/plus/model/ClientActionData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ClientActionData.java"


# instance fields
.field public autoComplete:Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;

.field public autoCompleteQuery:Ljava/lang/String;

.field public billboardImpression:Lcom/google/api/services/plus/model/ClientLoggedBillboardImpression;

.field public billboardPromoAction:Lcom/google/api/services/plus/model/ClientLoggedBillboardPromoAction;

.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientLoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public circleMember:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientLoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public gadgetId:Ljava/lang/String;

.field public intrCelebsClick:Lcom/google/api/services/plus/model/ClientLoggedIntrCelebsClick;

.field public labelId:Ljava/lang/String;

.field public obfuscatedGaiaId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public photoAlbumId:Ljava/lang/String;

.field public photoId:Ljava/lang/String;

.field public plusEventId:Ljava/lang/String;

.field public rhsComponent:Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;

.field public ribbonClick:Lcom/google/api/services/plus/model/ClientLoggedRibbonClick;

.field public ribbonOrder:Lcom/google/api/services/plus/model/ClientLoggedRibbonOrder;

.field public shareboxInfo:Lcom/google/api/services/plus/model/ClientLoggedShareboxInfo;

.field public suggestionInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionSummaryInfo:Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedCircle;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
