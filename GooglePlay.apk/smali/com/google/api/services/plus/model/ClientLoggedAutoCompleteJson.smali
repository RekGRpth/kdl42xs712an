.class public final Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientLoggedAutoCompleteJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "acceptedQuery"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "personalizationType"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "suggestedText"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "query"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "obfuscatedGaiaId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "sourceNamespace"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;->acceptedQuery:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;->personalizationType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;->suggestedText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;->query:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;->sourceNamespace:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;->getValues(Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ClientLoggedAutoCompleteJson;->newInstance()Lcom/google/api/services/plus/model/ClientLoggedAutoComplete;

    move-result-object v0

    return-object v0
.end method
