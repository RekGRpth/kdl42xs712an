.class public final Lcom/google/api/services/plus/model/CircleJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "CircleJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/Circle;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/CircleJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/CircleJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/CircleJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/CircleJson;->INSTANCE:Lcom/google/api/services/plus/model/CircleJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/Circle;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "kind"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "displayName"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plus/model/Circle_PeopleJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "people"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "etag"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "selfLink"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/CircleJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/CircleJson;->INSTANCE:Lcom/google/api/services/plus/model/CircleJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/Circle;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/Circle;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/Circle;->kind:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/Circle;->displayName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/Circle;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/Circle;->people:Lcom/google/api/services/plus/model/Circle$People;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/Circle;->etag:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/Circle;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/Circle;->selfLink:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/Circle;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/CircleJson;->getValues(Lcom/google/api/services/plus/model/Circle;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/Circle;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Circle;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Circle;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/CircleJson;->newInstance()Lcom/google/api/services/plus/model/Circle;

    move-result-object v0

    return-object v0
.end method
