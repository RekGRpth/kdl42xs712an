.class public final Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientLoggedRhsComponentItemJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plus/model/ClientLoggedHangoutJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "hangout"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "suggestionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "connectSiteId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "gamesLabelId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "person"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plus/model/ClientLoggedCircleJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "circle"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "col"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "row"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->hangout:Lcom/google/api/services/plus/model/ClientLoggedHangout;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->suggestionInfo:Lcom/google/api/services/plus/model/ClientLoggedSuggestionInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->connectSiteId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->gamesLabelId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->person:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->circle:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->col:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;->row:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;->getValues(Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItemJson;->newInstance()Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;

    move-result-object v0

    return-object v0
.end method
