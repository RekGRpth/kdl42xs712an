.class final Ljava/nio/ReadWriteLongArrayBuffer;
.super Ljava/nio/LongArrayBuffer;
.source "ReadWriteLongArrayBuffer.java"


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Ljava/nio/LongArrayBuffer;-><init>(I)V

    return-void
.end method

.method constructor <init>(I[JI)V
    .locals 0
    .param p1    # I
    .param p2    # [J
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Ljava/nio/LongArrayBuffer;-><init>(I[JI)V

    return-void
.end method

.method constructor <init>([J)V
    .locals 0
    .param p1    # [J

    invoke-direct {p0, p1}, Ljava/nio/LongArrayBuffer;-><init>([J)V

    return-void
.end method

.method static copy(Ljava/nio/LongArrayBuffer;I)Ljava/nio/ReadWriteLongArrayBuffer;
    .locals 4
    .param p0    # Ljava/nio/LongArrayBuffer;
    .param p1    # I

    new-instance v0, Ljava/nio/ReadWriteLongArrayBuffer;

    invoke-virtual {p0}, Ljava/nio/LongArrayBuffer;->capacity()I

    move-result v1

    iget-object v2, p0, Ljava/nio/LongArrayBuffer;->backingArray:[J

    iget v3, p0, Ljava/nio/LongArrayBuffer;->offset:I

    invoke-direct {v0, v1, v2, v3}, Ljava/nio/ReadWriteLongArrayBuffer;-><init>(I[JI)V

    iget v1, p0, Ljava/nio/LongArrayBuffer;->limit:I

    iput v1, v0, Ljava/nio/ReadWriteLongArrayBuffer;->limit:I

    invoke-virtual {p0}, Ljava/nio/LongArrayBuffer;->position()I

    move-result v1

    iput v1, v0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    iput p1, v0, Ljava/nio/ReadWriteLongArrayBuffer;->mark:I

    return-object v0
.end method


# virtual methods
.method public asReadOnlyBuffer()Ljava/nio/LongBuffer;
    .locals 1

    iget v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->mark:I

    invoke-static {p0, v0}, Ljava/nio/ReadOnlyLongArrayBuffer;->copy(Ljava/nio/LongArrayBuffer;I)Ljava/nio/ReadOnlyLongArrayBuffer;

    move-result-object v0

    return-object v0
.end method

.method public compact()Ljava/nio/LongBuffer;
    .locals 5

    iget-object v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->backingArray:[J

    iget v1, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    iget v2, p0, Ljava/nio/ReadWriteLongArrayBuffer;->offset:I

    add-int/2addr v1, v2

    iget-object v2, p0, Ljava/nio/ReadWriteLongArrayBuffer;->backingArray:[J

    iget v3, p0, Ljava/nio/ReadWriteLongArrayBuffer;->offset:I

    invoke-virtual {p0}, Ljava/nio/ReadWriteLongArrayBuffer;->remaining()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->limit:I

    iget v1, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    iget v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->capacity:I

    iput v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->limit:I

    const/4 v0, -0x1

    iput v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->mark:I

    return-object p0
.end method

.method public duplicate()Ljava/nio/LongBuffer;
    .locals 1

    iget v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->mark:I

    invoke-static {p0, v0}, Ljava/nio/ReadWriteLongArrayBuffer;->copy(Ljava/nio/LongArrayBuffer;I)Ljava/nio/ReadWriteLongArrayBuffer;

    move-result-object v0

    return-object v0
.end method

.method public isReadOnly()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protectedArray()[J
    .locals 1

    iget-object v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->backingArray:[J

    return-object v0
.end method

.method protectedArrayOffset()I
    .locals 1

    iget v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->offset:I

    return v0
.end method

.method protectedHasArray()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public put(IJ)Ljava/nio/LongBuffer;
    .locals 2
    .param p1    # I
    .param p2    # J

    invoke-virtual {p0, p1}, Ljava/nio/ReadWriteLongArrayBuffer;->checkIndex(I)V

    iget-object v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->backingArray:[J

    iget v1, p0, Ljava/nio/ReadWriteLongArrayBuffer;->offset:I

    add-int/2addr v1, p1

    aput-wide p2, v0, v1

    return-object p0
.end method

.method public put(J)Ljava/nio/LongBuffer;
    .locals 4
    .param p1    # J

    iget v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    iget v1, p0, Ljava/nio/ReadWriteLongArrayBuffer;->limit:I

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/nio/BufferOverflowException;

    invoke-direct {v0}, Ljava/nio/BufferOverflowException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->backingArray:[J

    iget v1, p0, Ljava/nio/ReadWriteLongArrayBuffer;->offset:I

    iget v2, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    add-int/2addr v1, v2

    aput-wide p1, v0, v1

    return-object p0
.end method

.method public put([JII)Ljava/nio/LongBuffer;
    .locals 3
    .param p1    # [J
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Ljava/nio/ReadWriteLongArrayBuffer;->remaining()I

    move-result v0

    if-le p3, v0, :cond_0

    new-instance v0, Ljava/nio/BufferOverflowException;

    invoke-direct {v0}, Ljava/nio/BufferOverflowException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->backingArray:[J

    iget v1, p0, Ljava/nio/ReadWriteLongArrayBuffer;->offset:I

    iget v2, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    add-int/2addr v1, v2

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    add-int/2addr v0, p3

    iput v0, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    return-object p0
.end method

.method public slice()Ljava/nio/LongBuffer;
    .locals 5

    new-instance v0, Ljava/nio/ReadWriteLongArrayBuffer;

    invoke-virtual {p0}, Ljava/nio/ReadWriteLongArrayBuffer;->remaining()I

    move-result v1

    iget-object v2, p0, Ljava/nio/ReadWriteLongArrayBuffer;->backingArray:[J

    iget v3, p0, Ljava/nio/ReadWriteLongArrayBuffer;->offset:I

    iget v4, p0, Ljava/nio/ReadWriteLongArrayBuffer;->position:I

    add-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/nio/ReadWriteLongArrayBuffer;-><init>(I[JI)V

    return-object v0
.end method
