.class public Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;
.super Ljava/security/Signature;
.source "OpenSSLSignatureRawRSA.java"


# instance fields
.field private inputBuffer:[B

.field private inputIsTooLong:Z

.field private inputOffset:I

.field private key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    const-string v0, "NONEwithRSA"

    invoke-direct {p0, v0}, Ljava/security/Signature;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected engineGetParameter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method protected engineInitSign(Ljava/security/PrivateKey;)V
    .locals 4
    .param p1    # Ljava/security/PrivateKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    instance-of v2, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    if-eqz v2, :cond_0

    move-object v1, p1

    check-cast v1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;

    invoke-virtual {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;->getOpenSSLKey()Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    :goto_0
    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;->getPkeyContext()I

    move-result v2

    invoke-static {v2}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->RSA_size(I)I

    move-result v0

    new-array v2, v0, [B

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    return-void

    :cond_0
    instance-of v2, p1, Ljava/security/interfaces/RSAPrivateCrtKey;

    if-eqz v2, :cond_1

    move-object v1, p1

    check-cast v1, Ljava/security/interfaces/RSAPrivateCrtKey;

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateCrtKey;->getInstance(Ljava/security/interfaces/RSAPrivateCrtKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_1
    instance-of v2, p1, Ljava/security/interfaces/RSAPrivateKey;

    if-eqz v2, :cond_2

    move-object v1, p1

    check-cast v1, Ljava/security/interfaces/RSAPrivateKey;

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPrivateKey;->getInstance(Ljava/security/interfaces/RSAPrivateKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Need DSA or RSA private key"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected engineInitVerify(Ljava/security/PublicKey;)V
    .locals 4
    .param p1    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    instance-of v2, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;

    if-eqz v2, :cond_0

    move-object v1, p1

    check-cast v1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;

    invoke-virtual {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;->getOpenSSLKey()Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    :goto_0
    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v2}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;->getPkeyContext()I

    move-result v2

    invoke-static {v2}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->RSA_size(I)I

    move-result v0

    new-array v2, v0, [B

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    return-void

    :cond_0
    instance-of v2, p1, Ljava/security/interfaces/RSAPublicKey;

    if-eqz v2, :cond_1

    move-object v1, p1

    check-cast v1, Ljava/security/interfaces/RSAPublicKey;

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRSAPublicKey;->getInstance(Ljava/security/interfaces/RSAPublicKey;)Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/security/InvalidKeyException;

    const-string v3, "Need DSA or RSA public key"

    invoke-direct {v2, v3}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected engineSetParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    return-void
.end method

.method protected engineSign()[B
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    const/4 v6, 0x0

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    if-nez v2, :cond_0

    new-instance v2, Ljava/security/SignatureException;

    const-string v3, "Need RSA private key"

    invoke-direct {v2, v3}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-boolean v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputIsTooLong:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/security/SignatureException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "input length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (modulus size)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    array-length v2, v2

    new-array v1, v2, [B

    :try_start_0
    iget v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    iget-object v3, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    iget-object v4, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v4}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;->getPkeyContext()I

    move-result v4

    const/4 v5, 0x1

    invoke-static {v2, v3, v1, v4, v5}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->RSA_private_encrypt(I[B[BII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput v6, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    return-object v1

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Ljava/security/SignatureException;

    invoke-direct {v2, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    iput v6, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    throw v2
.end method

.method protected engineUpdate(B)V
    .locals 3
    .param p1    # B

    iget v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    iget v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    array-length v2, v2

    if-le v1, v2, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputIsTooLong:Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    aput-byte p1, v1, v0

    goto :goto_0
.end method

.method protected engineUpdate([BII)V
    .locals 3
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    iget v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    iget v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    add-int/2addr v1, p3

    iput v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    iget v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    iget-object v2, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    array-length v2, v2

    if-le v1, v2, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputIsTooLong:Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    invoke-static {p1, p2, v1, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method protected engineVerify([B)Z
    .locals 10
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v6, 0x0

    iget-object v7, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    if-nez v7, :cond_0

    new-instance v6, Ljava/security/SignatureException;

    const-string v7, "Need RSA public key"

    invoke-direct {v6, v7}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    iget-boolean v7, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputIsTooLong:Z

    if-eqz v7, :cond_1

    :goto_0
    return v6

    :cond_1
    iget-object v7, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    array-length v7, v7

    new-array v4, v7, [B

    :try_start_0
    array-length v7, p1

    iget-object v8, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v8}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;->getPkeyContext()I

    move-result v8

    const/4 v9, 0x1

    invoke-static {v7, p1, v4, v8, v9}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->RSA_public_decrypt(I[B[BII)I
    :try_end_0
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    :try_start_1
    iget v7, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    if-ne v5, v7, :cond_3

    :goto_1
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_4

    iget-object v7, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputBuffer:[B

    aget-byte v7, v7, v2

    aget-byte v8, v4, v2

    if-eq v7, v8, :cond_2

    const/4 v3, 0x0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_1
    move-exception v1

    :try_start_2
    new-instance v7, Ljava/security/SignatureException;

    invoke-direct {v7, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v7

    iput v6, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    throw v7

    :catch_2
    move-exception v0

    iput v6, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    goto :goto_0

    :cond_3
    move v3, v6

    goto :goto_1

    :cond_4
    iput v6, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLSignatureRawRSA;->inputOffset:I

    move v6, v3

    goto :goto_0
.end method
